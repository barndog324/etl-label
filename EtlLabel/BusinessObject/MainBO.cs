﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace KCC.OA.Etl
{
    class MainBO : MainDTO
    {
        public DataTable GetJobNumberList()
        {
            DataTable dt = new DataTable();

            try
            {
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["KCC.OA.Etl.Properties.Settings"].ConnectionString;
                var connectionString = GlobalKCCConnectionString.KCCConnectString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.EtlLabel_GetJobNumberList", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;                        
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);                                
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }

        public DataTable GetJobHeadData()
        {           
            DataTable dt = new DataTable();

            try
            {
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["KCC.OA.Etl.Properties.Settings"].ConnectionString;
                var connectionString = GlobalKCCConnectionString.KCCConnectString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.EtlLabel_GetJobHeadData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }

        public DataTable GetOrderHedData()
        {
            DataTable dt = new DataTable();

            try
            {
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["KCC.OA.Etl.Properties.Settings"].ConnectionString;
                var connectionString = GlobalKCCConnectionString.KCCConnectString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetOrderHedData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@OrderNum", Int32.Parse(OrderNum));
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }

        public DataTable GetUnitSerialNoData()
        {
            DataTable dt = new DataTable();

            try
            {
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["KCC.OA.Etl.Properties.Settings"].ConnectionString;
                var connectionString = GlobalKCCConnectionString.KCCConnectString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.VKG_GetUnitSerialNoData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }


        public bool GetEtlOAUData()
        {
            bool jobFound = false;

            DataTable dt = new DataTable();

            try
            {
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["KCC.OA.Etl.Properties.Settings"].ConnectionString;
                var connectionString = GlobalKCCConnectionString.KCCConnectString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetEtlData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);

                                if (dt.Rows.Count > 0)
                                {
                                    jobFound = true;
                                    DataRow dr = dt.Rows[0];
                                    JobNum = dr["JobNum"].ToString();
                                    OrderNum = dr["OrderNum"].ToString();
                                    OrderLine = dr["OrderLine"].ToString();
                                    ReleaseNum = dr["ReleaseNum"].ToString();                                    
                                    ModelNo = dr["ModelNo"].ToString();
                                    SerialNo = dr["SerialNo"].ToString();
                                    MfgDate = dr["MfgDate"].ToString();
                                    UnitType = dr["UnitType"].ToString();
                                    ElecRating = dr["ElecRating"].ToString();
                                    OperVoltage = dr["OperVoltage"].ToString();
                                    DualPointPower = dr["DualPointPower"].ToString();
                                    MinCKTAmp = dr["MinCKTAmp"].ToString();
                                    MinCKTAmp2 = dr["MinCKTAmp2"].ToString();
                                    MFSMCB = dr["MFSMCB"].ToString();
                                    MFSMCB2 = dr["MFSMCB2"].ToString();
                                    MOP = dr["MOP"].ToString();
                                    HeatingType = dr["HeatingType"].ToString();
                                    Voltage = dr["Voltage"].ToString();
                                    TestPressureHigh = dr["TestPressureHigh"].ToString();
                                    TestPressureLow = dr["TestPressureLow"].ToString();
                                    SecondaryHtgInput = dr["SecondaryHtgInput"].ToString();
                                    HeatingInputElectric = dr["HeatingInputElectric"].ToString();
                                    FuelType = dr["FuelType"].ToString();

                                    Comp1Qty = dr["Comp1Qty"].ToString();
                                    Comp2Qty = dr["Comp2Qty"].ToString();
                                    Comp3Qty = dr["Comp3Qty"].ToString();
                                    Comp4Qty = dr["Comp4Qty"].ToString();
                                    Comp5Qty = dr["Comp5Qty"].ToString();
                                    Comp6Qty = dr["Comp6Qty"].ToString();

                                    Comp1Phase = dr["Comp1Phase"].ToString();
                                    Comp2Phase = dr["Comp2Phase"].ToString();
                                    Comp3Phase = dr["Comp3Phase"].ToString();
                                    Comp4Phase = dr["Comp4Phase"].ToString();
                                    Comp5Phase = dr["Comp5Phase"].ToString();
                                    Comp6Phase = dr["Comp6Phase"].ToString();                                    
                                    
                                    Comp1RLA_Volts = dr["Comp1RLA_Volts"].ToString();
                                    Comp2RLA_Volts = dr["Comp2RLA_Volts"].ToString();
                                    Comp3RLA_Volts = dr["Comp3RLA_Volts"].ToString();
                                    Comp4RLA_Volts = dr["Comp4RLA_Volts"].ToString();
                                    Comp5RLA_Volts = dr["Comp5RLA_Volts"].ToString();
                                    Comp6RLA_Volts = dr["Comp6RLA_Volts"].ToString();
                                    
                                    Comp1LRA = dr["Comp1LRA"].ToString();
                                    Comp2LRA = dr["Comp2LRA"].ToString();
                                    Comp3LRA = dr["Comp3LRA"].ToString();
                                    Comp4LRA = dr["Comp4LRA"].ToString();
                                    Comp5LRA = dr["Comp5LRA"].ToString();
                                    Comp6LRA = dr["Comp6LRA"].ToString();

                                    Circuit1Charge = dr["Circuit1Charge"].ToString();
                                    Circuit2Charge = dr["Circuit2Charge"].ToString();

                                    FanCondQty = dr["FanCondQty"].ToString();
                                    FanEvapQty = dr["FanEvapQty"].ToString();
                                    FanErvQty = dr["FanErvQty"].ToString();
                                    FanPwrExhQty = dr["FanPwrExhQty"].ToString();

                                    FanCondPhase = dr["FanCondPhase"].ToString();
                                    FanEvapPhase = dr["FanEvapPhase"].ToString();
                                    FanErvPhase = dr["FanErvPhase"].ToString();
                                    FanPwrExhPhase = dr["FanPwrExhPhase"].ToString();

                                    FanCondFLA = dr["FanCondFLA"].ToString();
                                    FanEvapFLA = dr["FanEvapFLA"].ToString();
                                    FanErvFLA = dr["FanErvFLA"].ToString();
                                    FanPwrExhFLA = dr["FanPwrExhFLA"].ToString();

                                    FanCondHP = dr["FanCondHP"].ToString();
                                    FanEvapHP = dr["FanEvapHP"].ToString();
                                    FanErvHP = dr["FanErvHP"].ToString();
                                    FanPwrExhHP = dr["FanPwrExhHP"].ToString();

                                    FlowRate = dr["FlowRate"].ToString();
                                    EnteringTemp = dr["EnteringTemp"].ToString();
                                    WaterGlycol = dr["WaterGlycol"].ToString();
                                    OperatingPressure = dr["OperatingPressure"].ToString();

                                    DF_StaticPressure = dr["DF_StaticPressure"].ToString();
                                    DF_MaxHtgInputBTUH = dr["DF_MaxHtgInputBTUH"].ToString();
                                    DF_MinHeatingInput = dr["DF_MinHeatingInput"].ToString();
                                    DF_TempRise = dr["DF_TempRise"].ToString();
                                    DF_MaxGasPressure = dr["DF_MaxGasPressure"].ToString();
                                    DF_MinGasPressure = dr["DF_MinGasPressure"].ToString();
                                    DF_MinPressureDrop = dr["DF_MinPressureDrop"].ToString();
                                    DF_MaxPressureDrop = dr["DF_MaxPressureDrop"].ToString();
                                    DF_ManifoldPressure = dr["DF_ManifoldPressure"].ToString();
                                    DF_CutoutTemp = dr["DF_CutOutTemp"].ToString();

                                    IN_MaxHtgInputBTUH = dr["IN_MaxHtgInputBTUH"].ToString();
                                    IN_HeatingOutputBTUH = dr["IN_HeatingOutputBTUH"].ToString();
                                    IN_MinInputBTU = dr["IN_MinInputBTU"].ToString();
                                    IN_MaxExt = dr["IN_MaxExt"].ToString();
                                    IN_TempRise = dr["IN_TempRise"].ToString();
                                    IN_MaxOutAirTemp = dr["IN_MaxOutAirTemp"].ToString();
                                    IN_MaxGasPressure = dr["IN_MaxGasPressure"].ToString();
                                    IN_MinGasPressure = dr["IN_MinGasPressure"].ToString();
                                    IN_ManifoldPressure = dr["IN_ManifoldPressure"].ToString();

                                    ModelNoVerifiedBy = dr["ModelNoVerifiedBy"].ToString();
                                    French = dr["French"].ToString();
                                    WhseCode = dr["WarehouseCode"].ToString();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return jobFound;
        }

        public void InsertEtlOAU()
        {
            try
            {
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["KCC.OA.Etl.Properties.Settings"].ConnectionString;
                var connectionString = GlobalKCCConnectionString.KCCConnectString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_InsertEtlOAU", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@JobNum", JobNum);
                        command.Parameters.AddWithValue("@OrderNum", Int32.Parse(OrderNum));
                        command.Parameters.AddWithValue("@OrderLine", Int32.Parse(OrderLine));
                        command.Parameters.AddWithValue("@ReleaseNum", Int32.Parse(ReleaseNum));                                             
                        command.Parameters.AddWithValue("@ModelNo", ModelNo);
                        command.Parameters.AddWithValue("@SerialNo", SerialNo);
                        command.Parameters.AddWithValue("@MfgDate", DateTime.Parse(MfgDate));
                        command.Parameters.AddWithValue("@UnitType", UnitType);
                        command.Parameters.AddWithValue("@ElecRating", ElecRating);
                        command.Parameters.AddWithValue("@OperVoltage", OperVoltage);
                        command.Parameters.AddWithValue("@DualPointPower", Int16.Parse(DualPointPower));
                        command.Parameters.AddWithValue("@MinCKTAmp", MinCKTAmp);
                        command.Parameters.AddWithValue("@MinCKTAmp2", MinCKTAmp2);
                        command.Parameters.AddWithValue("@MFSMCB", MFSMCB);
                        command.Parameters.AddWithValue("@MFSMCB2", MFSMCB2);
                        command.Parameters.AddWithValue("@MOP", MOP);
                        command.Parameters.AddWithValue("@HeatingType", HeatingType);                        
                        command.Parameters.AddWithValue("@Voltage", Voltage);
                        command.Parameters.AddWithValue("@TestPressureHigh", TestPressureHigh);
                        command.Parameters.AddWithValue("@TestPressureLow", TestPressureLow);
                        command.Parameters.AddWithValue("@SecondaryHtgInput", SecondaryHtgInput);
                        command.Parameters.AddWithValue("@HeatingInputElectric", HeatingInputElectric);
                        command.Parameters.AddWithValue("@FuelType", FuelType);
                        command.Parameters.AddWithValue("@Comp1Qty", Comp1Qty);
                        command.Parameters.AddWithValue("@Comp2Qty", Comp2Qty);
                        command.Parameters.AddWithValue("@Comp3Qty", Comp3Qty);
                        command.Parameters.AddWithValue("@Comp4Qty", Comp4Qty);
                        command.Parameters.AddWithValue("@Comp5Qty", Comp5Qty);
                        command.Parameters.AddWithValue("@Comp6Qty", Comp6Qty);
                        command.Parameters.AddWithValue("@Comp1Phase", Comp1Phase);
                        command.Parameters.AddWithValue("@Comp2Phase", Comp2Phase);
                        command.Parameters.AddWithValue("@Comp3Phase", Comp3Phase);
                        command.Parameters.AddWithValue("@Comp4Phase", Comp4Phase);
                        command.Parameters.AddWithValue("@Comp5Phase", Comp5Phase);
                        command.Parameters.AddWithValue("@Comp6Phase", Comp6Phase);
                        command.Parameters.AddWithValue("@Comp1RLA_Volts", Comp1RLA_Volts);
                        command.Parameters.AddWithValue("@Comp2RLA_Volts", Comp2RLA_Volts);
                        command.Parameters.AddWithValue("@Comp3RLA_Volts", Comp3RLA_Volts);
                        command.Parameters.AddWithValue("@Comp4RLA_Volts", Comp4RLA_Volts);
                        command.Parameters.AddWithValue("@Comp5RLA_Volts", Comp5RLA_Volts);
                        command.Parameters.AddWithValue("@Comp6RLA_Volts", Comp6RLA_Volts);
                        command.Parameters.AddWithValue("@Comp1LRA", Comp1LRA);
                        command.Parameters.AddWithValue("@Comp2LRA", Comp2LRA);
                        command.Parameters.AddWithValue("@Comp3LRA", Comp3LRA);
                        command.Parameters.AddWithValue("@Comp4LRA", Comp4LRA);
                        command.Parameters.AddWithValue("@Comp5LRA", Comp5LRA);
                        command.Parameters.AddWithValue("@Comp6LRA", Comp6LRA);
                        command.Parameters.AddWithValue("@Circuit1Charge", Circuit1Charge);
                        command.Parameters.AddWithValue("@Circuit2Charge", Circuit2Charge);

                        command.Parameters.AddWithValue("@FanCondQty", FanCondQty);
                        command.Parameters.AddWithValue("@FanEvapQty", FanEvapQty);
                        command.Parameters.AddWithValue("@FanErvQty", FanErvQty);
                        command.Parameters.AddWithValue("@FanPwrExhQty", FanPwrExhQty);

                        command.Parameters.AddWithValue("@FanCondPhase", FanCondPhase);
                        command.Parameters.AddWithValue("@FanEvapPhase", FanEvapPhase);
                        command.Parameters.AddWithValue("@FanErvPhase", FanErvPhase);
                        command.Parameters.AddWithValue("@FanPwrExhPhase", FanPwrExhPhase);

                        command.Parameters.AddWithValue("@FanCondFLA", FanCondFLA);
                        command.Parameters.AddWithValue("@FanEvapFLA", FanEvapFLA);
                        command.Parameters.AddWithValue("@FanErvFLA", FanErvFLA);
                        command.Parameters.AddWithValue("@FanPwrExhFLA", FanPwrExhFLA);

                        command.Parameters.AddWithValue("@FanCondHP", FanCondHP);
                        command.Parameters.AddWithValue("@FanEvapHP", FanEvapHP);
                        command.Parameters.AddWithValue("@FanErvHP", FanErvHP);
                        command.Parameters.AddWithValue("@FanPwrExhHP", FanPwrExhHP);

                        command.Parameters.AddWithValue("@FlowRate", FlowRate);
                        command.Parameters.AddWithValue("@EnteringTemp", EnteringTemp);
                        command.Parameters.AddWithValue("@WaterGlycol", WaterGlycol);
                        command.Parameters.AddWithValue("@OperatingPressure", OperatingPressure);

                        command.Parameters.AddWithValue("@DF_StaticPressure", DF_StaticPressure);
                        command.Parameters.AddWithValue("@DF_MaxHtgInputBTUH", DF_MaxHtgInputBTUH);
                        command.Parameters.AddWithValue("@DF_MinHeatingInput", DF_MinHeatingInput);
                        command.Parameters.AddWithValue("@DF_MinPressureDrop", DF_MinPressureDrop);
                        command.Parameters.AddWithValue("@DF_MaxPressureDrop", DF_MaxPressureDrop);
                        command.Parameters.AddWithValue("@DF_ManifoldPressure", DF_ManifoldPressure);
                        command.Parameters.AddWithValue("@DF_MinGasPressure", DF_MinGasPressure);
                        command.Parameters.AddWithValue("@DF_MaxGasPressure", DF_MaxGasPressure);
                        command.Parameters.AddWithValue("@DF_CutoutTemp", DF_CutoutTemp);
                        command.Parameters.AddWithValue("@DF_TempRise", DF_TempRise);

                        command.Parameters.AddWithValue("@IN_MaxHtgInputBTUH", IN_MaxHtgInputBTUH);
                        command.Parameters.AddWithValue("@IN_HeatingOutputBTUH", IN_HeatingOutputBTUH);
                        command.Parameters.AddWithValue("@IN_MinInputBTU", IN_MinInputBTU);
                        command.Parameters.AddWithValue("@IN_MaxExt", IN_MaxExt);
                        command.Parameters.AddWithValue("@IN_TempRise", IN_TempRise);
                        command.Parameters.AddWithValue("@IN_MaxOutAirTemp", IN_MaxOutAirTemp);
                        command.Parameters.AddWithValue("@IN_MaxGasPressure", IN_MaxGasPressure);
                        command.Parameters.AddWithValue("@IN_MinGasPressure", IN_MinGasPressure);
                        command.Parameters.AddWithValue("@IN_ManifoldPressure", IN_ManifoldPressure);

                        command.Parameters.AddWithValue("@ModelNoVerifiedBy", ModelNoVerifiedBy);
                        command.Parameters.AddWithValue("@French", French);
                        command.Parameters.AddWithValue("@WhseCode", WhseCode);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateEtlOAU()
        {
            try
            {
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["KCC.OA.Etl.Properties.Settings"].ConnectionString;
                var connectionString = GlobalKCCConnectionString.KCCConnectString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_UpdateEtlOAU", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@JobNum", JobNum);
                        command.Parameters.AddWithValue("@OrderNum", Int32.Parse(OrderNum));
                        command.Parameters.AddWithValue("@OrderLine", Int32.Parse(OrderLine));
                        command.Parameters.AddWithValue("@ReleaseNum", Int32.Parse(ReleaseNum));
                        command.Parameters.AddWithValue("@ModelNo", ModelNo);
                        command.Parameters.AddWithValue("@SerialNo", SerialNo);
                        command.Parameters.AddWithValue("@MfgDate", DateTime.Parse(MfgDate));
                        command.Parameters.AddWithValue("@UnitType", UnitType);
                        command.Parameters.AddWithValue("@ElecRating", ElecRating);
                        command.Parameters.AddWithValue("@OperVoltage", OperVoltage);
                        command.Parameters.AddWithValue("@DualPointPower", Int16.Parse(DualPointPower));
                        command.Parameters.AddWithValue("@MinCKTAmp", MinCKTAmp);
                        command.Parameters.AddWithValue("@MinCKTAmp2", MinCKTAmp2);
                        command.Parameters.AddWithValue("@MfsMcb", MFSMCB);
                        command.Parameters.AddWithValue("@MfsMcb2", MFSMCB2);
                        command.Parameters.AddWithValue("@MOP", MOP);
                        command.Parameters.AddWithValue("@HeatingType", HeatingType);   
                        command.Parameters.AddWithValue("@Voltage", Voltage);
                        command.Parameters.AddWithValue("@TestPressureHigh", TestPressureHigh);
                        command.Parameters.AddWithValue("@TestPressureLow", TestPressureLow);
                        command.Parameters.AddWithValue("@SecondaryHtgInput", SecondaryHtgInput);
                        command.Parameters.AddWithValue("@HeatingInputElectric", HeatingInputElectric);
                        command.Parameters.AddWithValue("@FuelType", FuelType);
                        command.Parameters.AddWithValue("@Comp1Qty", Comp1Qty);
                        command.Parameters.AddWithValue("@Comp2Qty", Comp2Qty);
                        command.Parameters.AddWithValue("@Comp3Qty", Comp3Qty);
                        command.Parameters.AddWithValue("@Comp4Qty", Comp4Qty);
                        command.Parameters.AddWithValue("@Comp5Qty", Comp5Qty);
                        command.Parameters.AddWithValue("@Comp6Qty", Comp6Qty);
                        command.Parameters.AddWithValue("@Comp1Phase", Comp1Phase);
                        command.Parameters.AddWithValue("@Comp2Phase", Comp2Phase);
                        command.Parameters.AddWithValue("@Comp3Phase", Comp3Phase);
                        command.Parameters.AddWithValue("@Comp4Phase", Comp4Phase);
                        command.Parameters.AddWithValue("@Comp5Phase", Comp5Phase);
                        command.Parameters.AddWithValue("@Comp6Phase", Comp6Phase);
                        command.Parameters.AddWithValue("@Comp1RLA_Volts", Comp1RLA_Volts);
                        command.Parameters.AddWithValue("@Comp2RLA_Volts", Comp2RLA_Volts);
                        command.Parameters.AddWithValue("@Comp3RLA_Volts", Comp3RLA_Volts);
                        command.Parameters.AddWithValue("@Comp4RLA_Volts", Comp4RLA_Volts);
                        command.Parameters.AddWithValue("@Comp5RLA_Volts", Comp5RLA_Volts);
                        command.Parameters.AddWithValue("@Comp6RLA_Volts", Comp6RLA_Volts);
                        command.Parameters.AddWithValue("@Comp1LRA", Comp1LRA);
                        command.Parameters.AddWithValue("@Comp2LRA", Comp2LRA);
                        command.Parameters.AddWithValue("@Comp3LRA", Comp3LRA);
                        command.Parameters.AddWithValue("@Comp4LRA", Comp4LRA);
                        command.Parameters.AddWithValue("@Comp5LRA", Comp5LRA);
                        command.Parameters.AddWithValue("@Comp6LRA", Comp6LRA);
                        command.Parameters.AddWithValue("@Circuit1Charge", Circuit1Charge);
                        command.Parameters.AddWithValue("@Circuit2Charge", Circuit2Charge);

                        command.Parameters.AddWithValue("@FanCondQty", FanCondQty);
                        command.Parameters.AddWithValue("@FanEvapQty", FanEvapQty);
                        command.Parameters.AddWithValue("@FanErvQty", FanErvQty);
                        command.Parameters.AddWithValue("@FanPwrExhQty", FanPwrExhQty);

                        command.Parameters.AddWithValue("@FanCondPhase", FanCondPhase);
                        command.Parameters.AddWithValue("@FanEvapPhase", FanEvapPhase);
                        command.Parameters.AddWithValue("@FanErvPhase", FanErvPhase);
                        command.Parameters.AddWithValue("@FanPwrExhPhase", FanPwrExhPhase);

                        command.Parameters.AddWithValue("@FanCondFLA", FanCondFLA);
                        command.Parameters.AddWithValue("@FanEvapFLA", FanEvapFLA);
                        command.Parameters.AddWithValue("@FanErvFLA", FanErvFLA);
                        command.Parameters.AddWithValue("@FanPwrExhFLA", FanPwrExhFLA);

                        command.Parameters.AddWithValue("@FanCondHP", FanCondHP);
                        command.Parameters.AddWithValue("@FanEvapHP", FanEvapHP);
                        command.Parameters.AddWithValue("@FanErvHP", FanErvHP);
                        command.Parameters.AddWithValue("@FanPwrExhHP", FanPwrExhHP);

                        command.Parameters.AddWithValue("@FlowRate", FlowRate);
                        command.Parameters.AddWithValue("@EnteringTemp", EnteringTemp);
                        command.Parameters.AddWithValue("@WaterGlycol", WaterGlycol);
                        command.Parameters.AddWithValue("@OperatingPressure", OperatingPressure);

                        command.Parameters.AddWithValue("@DF_StaticPressure", DF_StaticPressure);
                        command.Parameters.AddWithValue("@DF_MaxHtgInputBTUH", DF_MaxHtgInputBTUH);
                        command.Parameters.AddWithValue("@DF_MinHeatingInput", DF_MinHeatingInput);
                        command.Parameters.AddWithValue("@DF_MinPressureDrop", DF_MinPressureDrop);
                        command.Parameters.AddWithValue("@DF_MaxPressureDrop", DF_MaxPressureDrop);
                        command.Parameters.AddWithValue("@DF_ManifoldPressure", DF_ManifoldPressure);
                        command.Parameters.AddWithValue("@DF_MinGasPressure", DF_MinGasPressure);
                        command.Parameters.AddWithValue("@DF_MaxGasPressure", DF_MaxGasPressure);
                        command.Parameters.AddWithValue("@DF_CutoutTemp", DF_CutoutTemp);
                        command.Parameters.AddWithValue("@DF_TempRise", DF_TempRise);

                        command.Parameters.AddWithValue("@IN_MaxHtgInputBTUH", IN_MaxHtgInputBTUH);
                        command.Parameters.AddWithValue("@IN_HeatingOutputBTUH", IN_HeatingOutputBTUH);
                        command.Parameters.AddWithValue("@IN_MinInputBTU", IN_MinInputBTU);
                        command.Parameters.AddWithValue("@IN_MaxExt", IN_MaxExt);
                        command.Parameters.AddWithValue("@IN_TempRise", IN_TempRise);
                        command.Parameters.AddWithValue("@IN_MaxOutAirTemp", IN_MaxOutAirTemp);
                        command.Parameters.AddWithValue("@IN_MaxGasPressure", IN_MaxGasPressure);
                        command.Parameters.AddWithValue("@IN_MinGasPressure", IN_MinGasPressure);
                        command.Parameters.AddWithValue("@IN_ManifoldPressure", IN_ManifoldPressure);

                        command.Parameters.AddWithValue("@ModelNoVerifiedBy", ModelNoVerifiedBy);
                        command.Parameters.AddWithValue("@French", French);
                        command.Parameters.AddWithValue("@WhseCode", WhseCode);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Monitor SQL

        public DataTable GetMonitorEtlData()
        {
            bool jobFound = false;

            DataTable dt = new DataTable();

            try
            {
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["KCC.OA.Etl.Properties.Settings"].ConnectionString;
                var connectionString = GlobalKCCConnectionString.KCCConnectString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MonitorGetEtlData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);                  
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }

        public void MonitorInsertEtl()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MonitorInsertEtl", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);
                        cmd.Parameters.AddWithValue("@OrderNum", OrderNum);
                        cmd.Parameters.AddWithValue("@OrderLine", OrderLine);
                        cmd.Parameters.AddWithValue("@ReleaseNum", ReleaseNum);
                        cmd.Parameters.AddWithValue("@ModelNo", ModelNo);
                        cmd.Parameters.AddWithValue("@SerialNo", SerialNo);
                        cmd.Parameters.AddWithValue("@MfgDate", MfgDate);
                        cmd.Parameters.AddWithValue("@ElecRating", ElecRating);
                        cmd.Parameters.AddWithValue("@OperVoltage", OperVoltage);
                        cmd.Parameters.AddWithValue("@MinCKTAmp", MinCKTAmp);
                        cmd.Parameters.AddWithValue("@MOP", MFSMCB);
                        cmd.Parameters.AddWithValue("@RawMOP", RawMOP);
                        cmd.Parameters.AddWithValue("@HeatingType", HeatingType);
                        cmd.Parameters.AddWithValue("@Voltage", Voltage);
                        cmd.Parameters.AddWithValue("@HeatingInputElectric", HeatingInputElectric);
                        cmd.Parameters.AddWithValue("@FuelType", FuelType);
                        cmd.Parameters.AddWithValue("@MaxOutletTemp", MaxOutAirTemp);
                        cmd.Parameters.AddWithValue("@Circuit1Charge", Circuit1Charge);
                        cmd.Parameters.AddWithValue("@Circuit2Charge", Circuit2Charge);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanStdQty", DD_SupplyFanStdQty);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanStdPhase", DD_SupplyFanStdPhase);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanStdFLA", DD_SupplyFanStdFLA);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanStdHP", DD_SupplyFanStdHP);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanOvrQty", DD_SupplyFanOvrQty);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanOvrPhase", DD_SupplyFanOvrPhase);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanOvrFLA", DD_SupplyFanOvrFLA);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanOvrHP", DD_SupplyFanOvrHP);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanStdQty", BeltSupplyFanStdQty);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanStdPhase", BeltSupplyFanStdPhase);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanStdFLA", BeltSupplyFanStdFLA);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanStdHP", BeltSupplyFanStdHP);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanOvrQty", BeltSupplyFanOvrQty);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanOvrPhase", BeltSupplyFanOvrPhase);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanOvrFLA", BeltSupplyFanOvrFLA);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanOvrHP", BeltSupplyFanOvrHP);
                        cmd.Parameters.AddWithValue("@CondenserFanStdQty", CondenserFanStdQty);
                        cmd.Parameters.AddWithValue("@CondenserFanStdPhase", CondenserFanStdPhase);
                        cmd.Parameters.AddWithValue("@CondenserFanStdFLA", CondenserFanStdFLA);
                        cmd.Parameters.AddWithValue("@CondenserFanStdHP", CondenserFanStdHP);
                        cmd.Parameters.AddWithValue("@CondenserFanHighQty", CondenserFanHighQty);
                        cmd.Parameters.AddWithValue("@CondenserFanHighPhase", CondenserFanHighPhase);
                        cmd.Parameters.AddWithValue("@CondenserFanHighFLA", CondenserFanHighFLA);
                        cmd.Parameters.AddWithValue("@CondenserFanHighHP", CondenserFanHighHP);
                        cmd.Parameters.AddWithValue("@ExhaustFanQty", ExhaustFanQty);
                        cmd.Parameters.AddWithValue("@ExhaustFanPhase", ExhaustFanPhase);
                        cmd.Parameters.AddWithValue("@ExhaustFanFLA", ExhaustFanFLA);
                        cmd.Parameters.AddWithValue("@ExhaustFanHP", ExhaustFanHP);
                        cmd.Parameters.AddWithValue("@SemcoWheelQty", SemcoWheelQty);
                        cmd.Parameters.AddWithValue("@SemcoWheelPhase", SemcoWheelPhase);
                        cmd.Parameters.AddWithValue("@SemcoWheelFLA", SemcoWheelFLA);
                        cmd.Parameters.AddWithValue("@SemcoWheelHP", SemcoWheelHP);
                        cmd.Parameters.AddWithValue("@AirXchangeWheelQty", AirXchangeWheelQty);
                        cmd.Parameters.AddWithValue("@AirXchangeWheelPhase", AirXchangeWheelPhase);
                        cmd.Parameters.AddWithValue("@AirXchangeWheelFLA", AirXchangeWheelFLA);
                        cmd.Parameters.AddWithValue("@AirXchangeWheelHP", AirXchangeWheelHP);
                        cmd.Parameters.AddWithValue("@ModelNoVerifiedBy", ModelNoVerifiedBy);
                        cmd.Parameters.AddWithValue("@French", French);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void MonitorUpdateEtl()
        {
            try
            {
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["KCC.OA.Etl.Properties.Settings"].ConnectionString;
                var connectionString = GlobalKCCConnectionString.KCCConnectString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MonitorUpdateEtl", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);
                        cmd.Parameters.AddWithValue("@OrderNum", OrderNum);
                        cmd.Parameters.AddWithValue("@OrderLine", OrderLine);
                        cmd.Parameters.AddWithValue("@ReleaseNum", ReleaseNum);
                        cmd.Parameters.AddWithValue("@ModelNo", ModelNo);
                        cmd.Parameters.AddWithValue("@SerialNo", SerialNo);
                        cmd.Parameters.AddWithValue("@MfgDate", MfgDate);
                        cmd.Parameters.AddWithValue("@ElecRating", ElecRating);
                        cmd.Parameters.AddWithValue("@OperVoltage", OperVoltage);
                        cmd.Parameters.AddWithValue("@MinCKTAmp", MinCKTAmp);
                        cmd.Parameters.AddWithValue("@MOP", MFSMCB);
                        cmd.Parameters.AddWithValue("@RawMOP", RawMOP);
                        cmd.Parameters.AddWithValue("@HeatingType", HeatingType);
                        cmd.Parameters.AddWithValue("@Voltage", Voltage);
                        cmd.Parameters.AddWithValue("@HeatingInputElectric", HeatingInputElectric);
                        cmd.Parameters.AddWithValue("@FuelType", FuelType);
                        cmd.Parameters.AddWithValue("@MaxOutletTemp", MaxOutAirTemp);
                        cmd.Parameters.AddWithValue("@Circuit1Charge", Circuit1Charge);
                        cmd.Parameters.AddWithValue("@Circuit2Charge", Circuit2Charge);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanStdQty", DD_SupplyFanStdQty);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanStdPhase", DD_SupplyFanStdPhase);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanStdFLA", DD_SupplyFanStdFLA);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanStdHP", DD_SupplyFanStdHP);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanOvrQty", DD_SupplyFanOvrQty);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanOvrPhase", DD_SupplyFanOvrPhase);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanOvrFLA", DD_SupplyFanOvrFLA);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanOvrHP", DD_SupplyFanOvrHP);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanStdQty", BeltSupplyFanStdQty);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanStdPhase", BeltSupplyFanStdPhase);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanStdFLA", BeltSupplyFanStdFLA);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanStdHP", BeltSupplyFanStdHP);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanOvrQty", BeltSupplyFanOvrQty);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanOvrPhase", BeltSupplyFanOvrPhase);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanOvrFLA", BeltSupplyFanOvrFLA);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanOvrHP", BeltSupplyFanOvrHP);
                        cmd.Parameters.AddWithValue("@CondenserFanStdQty", CondenserFanStdQty);
                        cmd.Parameters.AddWithValue("@CondenserFanStdPhase", CondenserFanStdPhase);
                        cmd.Parameters.AddWithValue("@CondenserFanStdFLA", CondenserFanStdFLA);
                        cmd.Parameters.AddWithValue("@CondenserFanStdHP", CondenserFanStdHP);
                        cmd.Parameters.AddWithValue("@CondenserFanHighQty", CondenserFanHighQty);
                        cmd.Parameters.AddWithValue("@CondenserFanHighPhase", CondenserFanHighPhase);
                        cmd.Parameters.AddWithValue("@CondenserFanHighFLA", CondenserFanHighFLA);
                        cmd.Parameters.AddWithValue("@CondenserFanHighHP", CondenserFanHighHP);
                        cmd.Parameters.AddWithValue("@ExhaustFanQty", ExhaustFanQty);
                        cmd.Parameters.AddWithValue("@ExhaustFanPhase", ExhaustFanPhase);
                        cmd.Parameters.AddWithValue("@ExhaustFanFLA", ExhaustFanFLA);
                        cmd.Parameters.AddWithValue("@ExhaustFanHP", ExhaustFanHP);
                        cmd.Parameters.AddWithValue("@SemcoWheelQty", SemcoWheelQty);
                        cmd.Parameters.AddWithValue("@SemcoWheelPhase", SemcoWheelPhase);
                        cmd.Parameters.AddWithValue("@SemcoWheelFLA", SemcoWheelFLA);
                        cmd.Parameters.AddWithValue("@SemcoWheelHP", SemcoWheelHP);
                        cmd.Parameters.AddWithValue("@AirXchangeWheelQty", AirXchangeWheelQty);
                        cmd.Parameters.AddWithValue("@AirXchangeWheelPhase", AirXchangeWheelPhase);
                        cmd.Parameters.AddWithValue("@AirXchangeWheelFLA", AirXchangeWheelFLA);
                        cmd.Parameters.AddWithValue("@AirXchangeWheelHP", AirXchangeWheelHP);
                        cmd.Parameters.AddWithValue("@ModelNoVerifiedBy", ModelNoVerifiedBy);
                        cmd.Parameters.AddWithValue("@French", French);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region MSP Sql

        public DataTable MSP_GetEtlData()
        {
            bool jobFound = false;

            DataTable dt = new DataTable();

            try
            {
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["KCC.OA.Etl.Properties.Settings"].ConnectionString;
                var connectionString = GlobalKCCConnectionString.KCCConnectString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MSP_GetEtlData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }

        public void MSP_InsertEtl()
        {
            try
            {
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;
                var connectionString = GlobalKCCConnectionString.KCCConnectString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MSP_InsertEtl", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);
                        cmd.Parameters.AddWithValue("@OrderNum", Int32.Parse(OrderNum));
                        cmd.Parameters.AddWithValue("@OrderLine", Int32.Parse(OrderLine));
                        cmd.Parameters.AddWithValue("@ReleaseNum", Int32.Parse(ReleaseNum));
                        cmd.Parameters.AddWithValue("@ModelNo", ModelNo);
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);
                        cmd.Parameters.AddWithValue("@SerialNum", SerialNo);
                        cmd.Parameters.AddWithValue("@MfgDate", DateTime.Parse(MfgDate));
                        cmd.Parameters.AddWithValue("@Designation", MSPDesignation);
                        cmd.Parameters.AddWithValue("@Filter", MSPFanFilter);
                        cmd.Parameters.AddWithValue("@Voltage", MSPFanVoltage);
                        cmd.Parameters.AddWithValue("@Power", MSPFanPower);
                        cmd.Parameters.AddWithValue("@Draw", MSPFanDraw);
                                   
                        cmd.Parameters.AddWithValue("@FanQty", int.Parse(MSPFanQty));
                        cmd.Parameters.AddWithValue("@FanAirVolume", MSPFanAirVolume);
                        cmd.Parameters.AddWithValue("@FanExternalSP", MSPFanExternalSP);
                        cmd.Parameters.AddWithValue("@FanTotalSP", MSPFanTotalSP);
                        cmd.Parameters.AddWithValue("@AmbientTemp", AmbientTemp);
                        cmd.Parameters.AddWithValue("@Refrigerant", Refrigerant);

                        cmd.Parameters.AddWithValue("@Dehumidifier_BTUH", Dehumidifier_BTUH);
                        cmd.Parameters.AddWithValue("@Dehumidifier_SST", Dehumidifier_SST);
                        cmd.Parameters.AddWithValue("@Dehumidifier_EWT", Dehumidifier_EWT);
                        cmd.Parameters.AddWithValue("@Dehumidifier_LWT", Dehumidifier_LWT);
                        cmd.Parameters.AddWithValue("@Dehumidifier_GPM", Dehumidifier_GPM);
                        cmd.Parameters.AddWithValue("@Dehumidifier_PD", Dehumidifier_PD);
                        cmd.Parameters.AddWithValue("@Dehumidifier_Glycol", Dehumidifier_Glycol);
                        cmd.Parameters.AddWithValue("@Dehumidifier_EAT", Dehumidifier_EAT);
                        cmd.Parameters.AddWithValue("@Dehumidifier_LAT", Dehumidifier_LAT);
                        cmd.Parameters.AddWithValue("@Dehumidifier_CAPACITY", Dehumidifier_CAPACITY);

                        cmd.Parameters.AddWithValue("@PostCooling_BTUH", PostCooling_BTUH);
                        cmd.Parameters.AddWithValue("@PostCooling_SST", PostCooling_SST);
                        cmd.Parameters.AddWithValue("@PostCooling_EWT", PostCooling_EWT);
                        cmd.Parameters.AddWithValue("@PostCooling_LWT", PostCooling_LWT);
                        cmd.Parameters.AddWithValue("@PostCooling_GPM", PostCooling_GPM);
                        cmd.Parameters.AddWithValue("@PostCooling_PD", PostCooling_PD);
                        cmd.Parameters.AddWithValue("@PostCooling_Glycol", PostCooling_Glycol);
                        cmd.Parameters.AddWithValue("@PostCooling_EAT", PostCooling_EAT);
                        cmd.Parameters.AddWithValue("@PostCooling_LAT", PostCooling_LAT);

                        cmd.Parameters.AddWithValue("@PostHeating_BTUH", PostHeating_BTUH);
                        cmd.Parameters.AddWithValue("@PostHeating_EWT", PostHeating_EWT);
                        cmd.Parameters.AddWithValue("@PostHeating_LWT", PostHeating_LWT);
                        cmd.Parameters.AddWithValue("@PostHeating_GPM", PostHeating_GPM);
                        cmd.Parameters.AddWithValue("@PostHeating_PD",  PostHeating_PD);
                        cmd.Parameters.AddWithValue("@PostHeating_Glycol", PostHeating_Glycol);
                        cmd.Parameters.AddWithValue("@PostHeating_EAT", PostHeating_EAT);
                        cmd.Parameters.AddWithValue("@PostHeating_LAT", PostHeating_LAT);

                        cmd.Parameters.AddWithValue("@VerifiedBy", ModelNoVerifiedBy);
                        cmd.Parameters.AddWithValue("@French", French);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void MSP_UpdateEtl()
        {
            try
            {
                //var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["KCC.OA.Etl.Properties.Settings"].ConnectionString;
                var connectionString = GlobalKCCConnectionString.KCCConnectString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MSP_UpdateEtl", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);
                        cmd.Parameters.AddWithValue("@OrderNum", Int32.Parse(OrderNum));
                        cmd.Parameters.AddWithValue("@OrderLine", Int32.Parse(OrderLine));
                        cmd.Parameters.AddWithValue("@ReleaseNum", Int32.Parse(ReleaseNum));
                        cmd.Parameters.AddWithValue("@ModelNo", ModelNo);
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);
                        cmd.Parameters.AddWithValue("@SerialNum", SerialNo);
                        cmd.Parameters.AddWithValue("@MfgDate", DateTime.Parse(MfgDate));
                        cmd.Parameters.AddWithValue("@Designation", MSPDesignation);
                        cmd.Parameters.AddWithValue("@Filter", MSPFanFilter);
                        cmd.Parameters.AddWithValue("@Voltage", MSPFanVoltage);
                        cmd.Parameters.AddWithValue("@Power", MSPFanPower);
                        cmd.Parameters.AddWithValue("@Draw", MSPFanDraw);
                       
                        cmd.Parameters.AddWithValue("@FanQty", int.Parse(MSPFanQty));
                        cmd.Parameters.AddWithValue("@FanAirVolume", MSPFanAirVolume);
                        cmd.Parameters.AddWithValue("@FanExternalSP", MSPFanExternalSP);
                        cmd.Parameters.AddWithValue("@FanTotalSP", MSPFanTotalSP);
                        cmd.Parameters.AddWithValue("@AmbientTemp", AmbientTemp);
                        cmd.Parameters.AddWithValue("@Refrigerant", Refrigerant);

                        cmd.Parameters.AddWithValue("@Dehumidifier_BTUH", Dehumidifier_BTUH);
                        cmd.Parameters.AddWithValue("@Dehumidifier_SST", Dehumidifier_SST);
                        cmd.Parameters.AddWithValue("@Dehumidifier_EWT", Dehumidifier_EWT);
                        cmd.Parameters.AddWithValue("@Dehumidifier_LWT", Dehumidifier_LWT);
                        cmd.Parameters.AddWithValue("@Dehumidifier_GPM", Dehumidifier_GPM);
                        cmd.Parameters.AddWithValue("@Dehumidifier_PD", Dehumidifier_PD);
                        cmd.Parameters.AddWithValue("@Dehumidifier_Glycol", Dehumidifier_Glycol);
                        cmd.Parameters.AddWithValue("@Dehumidifier_EAT", Dehumidifier_EAT);
                        cmd.Parameters.AddWithValue("@Dehumidifier_LAT", Dehumidifier_LAT);
                        cmd.Parameters.AddWithValue("@Dehumidifier_CAPACITY", Dehumidifier_CAPACITY);

                        cmd.Parameters.AddWithValue("@PostCooling_BTUH", PostCooling_BTUH);
                        cmd.Parameters.AddWithValue("@PostCooling_SST", PostCooling_SST);
                        cmd.Parameters.AddWithValue("@PostCooling_EWT", PostCooling_EWT);
                        cmd.Parameters.AddWithValue("@PostCooling_LWT", PostCooling_LWT);
                        cmd.Parameters.AddWithValue("@PostCooling_GPM", PostCooling_GPM);
                        cmd.Parameters.AddWithValue("@PostCooling_PD", PostCooling_PD);
                        cmd.Parameters.AddWithValue("@PostCooling_Glycol", PostCooling_Glycol);
                        cmd.Parameters.AddWithValue("@PostCooling_EAT", PostCooling_EAT);
                        cmd.Parameters.AddWithValue("@PostCooling_LAT", PostCooling_LAT);

                        cmd.Parameters.AddWithValue("@PostHeating_BTUH", PostHeating_BTUH);
                        cmd.Parameters.AddWithValue("@PostHeating_EWT", PostHeating_EWT);
                        cmd.Parameters.AddWithValue("@PostHeating_LWT", PostHeating_LWT);
                        cmd.Parameters.AddWithValue("@PostHeating_GPM", PostHeating_GPM);
                        cmd.Parameters.AddWithValue("@PostHeating_PD", PostHeating_PD);
                        cmd.Parameters.AddWithValue("@PostHeating_Glycol", PostHeating_Glycol);
                        cmd.Parameters.AddWithValue("@PostHeating_EAT", PostHeating_EAT);
                        cmd.Parameters.AddWithValue("@PostHeating_LAT", PostHeating_LAT);

                        cmd.Parameters.AddWithValue("@VerifiedBy", ModelNoVerifiedBy);
                        cmd.Parameters.AddWithValue("@French", French);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion
    }
}
