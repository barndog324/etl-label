﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KCC.OA.Etl
{
    public static class ETLHelper
    {
        public static int GetHeatingTypeIndex(string modelNum)
        {
            string heatingType = getHeatingType(modelNum).ToUpper();
            int heatingTypeIndex = -1;

            if (modelNum.Length == 39)
            {                
                switch (heatingType)
                {

                    case "0":  // No Heat
                        heatingTypeIndex = 2;
                        break;

                    case "A":  // Indirect                 
                        heatingTypeIndex = 0;
                        break;

                    case "B":  // Direct
                        heatingTypeIndex = 3;
                        break;

                    case "C":  // Electri Staged
                        heatingTypeIndex = 1;
                        break;

                    case "D":  // Electric - SCR Modulating
                        heatingTypeIndex = 1;
                        break;

                    case "E":  // Dual Fuel (PRI-IF/SEC-DF)
                       heatingTypeIndex = 11;
                        break;

                    case "F":    // Dual Fuel (PRI-ELEC SCR/SEC-DF)
                        heatingTypeIndex = 12;
                        break;

                    case "G":   // Dual Fuel (PRI-IF/SEC-ELEC STAGED)
                        heatingTypeIndex = 6;
                        break;

                    case "H":   // Dual Fuel (PRI-ELEC SCR/SEC-ELEC STAGED)
                        heatingTypeIndex = 7;
                        break;

                    case "J":   // Hot Water
                        heatingTypeIndex = 4;
                        break;

                    case "K":  // Steam
                        heatingTypeIndex = 5;
                        break;

                    case "L":   // NoPrimaryHeat/Elec-Staged
                        heatingTypeIndex = 10;
                        break;

                    case "M":  // Dual Fuel (PRI-ELEC-STAGED/SEC-DF)
                        heatingTypeIndex = 12;
                        break;

                    case "N":   // Dual Fuel (PRI-ELEC-STAGED/SEC-ELEC STAGED)
                        heatingTypeIndex = 7;
                        break;

                    case "P":  // Dual Fuel (PRI-HW/SEC-DF)
                        heatingTypeIndex = 8;
                        break;

                    case "Q":  // Dual Fuel (PRI-HW/SEC-ELEC STAGED)
                        heatingTypeIndex = 9;
                        break;

                    case "R": // Dual Fuel (PRI-STEAM/SEC-DF)
                        heatingTypeIndex = 13;
                        break;

                    case "S":      // Dual Fuel (PRI-STEAM/SEC-ELEC-STAGED)
                        heatingTypeIndex = 14;
                        break;

                    case "T":  // Dual Fuel (PRI-IF/SEC-ELEC SCR)
                        heatingTypeIndex = 6;
                        break;

                    case "U":  // Dual Fuel (PRI-ELEC SCR/SEC-ELEC SCR)
                        heatingTypeIndex = 7;
                        break;

                    case "W": // Dual Fuel (PRI-ELEC STAGED/SEC-ELEC SCR)
                        heatingTypeIndex = 7;
                        break;

                    case "Y":      // Dual Fuel (PRI-HW/SEC-ELEC-SCR)
                        heatingTypeIndex = 9;
                        break;

                    case "Z": // Dual Fuel (PRI-STEAM/SEC-DF)
                        heatingTypeIndex = 13;
                        break;

                    case "V":      // Dual Fuel (PRI-STEAM/SEC-ELEC-SCR)
                        heatingTypeIndex = 14;
                        break;

                    default:
                        break;
                }
            }             
            else if (modelNum.Length == 69)
            {
                switch (heatingType)
                {
                    case "0":   // No Heat
                        heatingTypeIndex = 2;
                        break;
                    case "A":   // Indirect Fired NG (IF) - Standard Efficiency (80%)
                        heatingTypeIndex = 0;
                        break;
                    case "B":   // Indirect Fired NG (IF) - High Efficiency (82%)
                        heatingTypeIndex = 0;
                        break;
                    case "C":   // Indirect Fired NG (IF) - Premium Efficiency (+90%)
                        heatingTypeIndex = 0;
                        break;
                    case "D":   // Indirect Fired LP (IF) - Standard Efficiency (80%)
                        heatingTypeIndex = 0;
                        break;
                    case "E":   // Indirect Fired LP (IF) - High Efficiency (82%)
                        heatingTypeIndex = 0;
                        break;
                    case "F":   // Indirect Fired LP (IF) - Premium Efficiency (+90%)
                        heatingTypeIndex = 0;
                        break;
                    case "G":   // Hot Water
                        heatingTypeIndex = 4;
                        break;
                    case "H":   // Electric - Staged
                        heatingTypeIndex = 1;
                        break;
                    case "J":   // Electric - SCR Modulating
                        heatingTypeIndex = 1;
                        break;
                    case "K":   // Steam
                        heatingTypeIndex = 5;
                        break;
                    case "X":   // Special 
                        //heatingTypeIndex = 9;
                        break;
                    default:
                        break;
                }
                if (heatingTypeIndex == 2 && (modelNum.Substring(17, 1) == "4" || modelNum.Substring(17, 1) == "5"))
                {
                    heatingTypeIndex = 1;
                }
            }
            else // For Monitor project
            {
                
            }
            return heatingTypeIndex;
        }

        private static string getHeatingType(string modelNum)
        {
            string retHeatType = "";
            if (modelNum.Length == 39)
            {
                retHeatType = modelNum.Substring(19, 1);
            }
            else if (modelNum.Length == 69)
            {
                retHeatType = modelNum.Substring(15, 1);
            }
            else
            {
                retHeatType = "";
            }

            return retHeatType;
        }
    }
}
