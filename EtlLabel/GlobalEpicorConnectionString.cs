using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace KCC.OA.Etl
{
    static class GlobalEpicorConnectionString
    {
        public static string DisplayMode { get; set; }

        public static string EnvironmentStr { get; set; }

        public static string EpicorServer { get; set; }

        public static Color EnvColor { get; set; }
      
        public static string EpicorConnectString { get; set; }

    }
}
