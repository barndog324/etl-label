using System;
using System.Collections.Generic;
using System.Text;

namespace KCC.OA.Etl
{
    class GlobalFunctions
    {
        public static void parse29DigitModelNo(string modelNumStr, frmNewLineOAU frmOAU)
        {
            string pwrExtDigitStr;
            string ervDigitStr;
            string grossCoolingStr;
            string digitStr;                      

            // Determine which Heating Type settings are used by passing the
            // 16th digit of the ModelNo to the getheatingType function. NOTE:
            // When you assign the return value to the SelectedIndex parameter
            // this will cause the SelectedIndexChanged event for this combobox
            // to fire. This event can be found in the frmNewLineOAU code.

            digitStr = modelNumStr.Substring(15, 1);

            if (digitStr == "0")  // Indicates No Heat
            {
                frmOAU.comboBoxNewOAUHeatingType.SelectedIndex = 2;
            }
            else if (digitStr == "A") // Indicates Indirect-Fired (IF)
            {
                frmOAU.comboBoxNewOAUHeatingType.SelectedIndex = 0;
            }
            else if (digitStr == "B") // Indicates Direct-Fired (DF)
            {
                frmOAU.comboBoxNewOAUHeatingType.SelectedIndex = 3;
            }
            else if (digitStr == "E") // Indicates Electric
            {
                frmOAU.comboBoxNewOAUHeatingType.SelectedIndex = 1;
            }
            else if (digitStr == "J") // Indicates Hot Water
            {
                frmOAU.comboBoxNewOAUHeatingType.SelectedIndex = 4;
            }
          

            // Determine which voltage settings are used by passing the
            // 9th digit of the ModelNo to the getVoltage function. NOTE:
            // When you assign the return value to the SelectedIndex parameter
            // this will cause the SelectedIndexChanged event for this combobox
            // to fire. This event can be found in the frmNewLineOAU code.
            frmOAU.comboBoxNewOAUVoltage.SelectedIndex = getVoltage(modelNumStr.Substring(8, 1));                        

            // Copy the 22nd digit of the model number. This value represents
            // Powered Exhaust.
            pwrExtDigitStr = modelNumStr.Substring(21, 1);

            // Copy the 23rd digit of the model number. This value represents ERV.
            ervDigitStr = modelNumStr.Substring(22, 1);

            grossCoolingStr = modelNumStr.Substring(4, 3);

            if (Int32.Parse(grossCoolingStr) > 0)
            {
                GlobalFunctions.populateFanCond_ComprData(modelNumStr.Substring(2, 1),
                    modelNumStr.Substring(4, 3), pwrExtDigitStr, ervDigitStr, frmOAU);              
                
                GlobalFunctions.getFanHP(modelNumStr.Substring(17, 1), modelNumStr.Substring(28, 1), frmOAU);                
            }

            if (frmOAU.comboBoxNewOAUHeatingType.SelectedIndex == 0)
            {
                frmOAU.textBoxNewOAUIndFireMaxHtgInput.Focus();
                frmOAU.textBoxNewOAUIndFireMaxHtgInput.Select();
            }
            else if (frmOAU.comboBoxNewOAUHeatingType.SelectedIndex == 1)
            {
                frmOAU.textBoxNewOAUElecHeatingInputElec.Focus();
                frmOAU.textBoxNewOAUElecHeatingInputElec.Select();
            }

        }

        public static void getHeatType(string digitStr, frmNewLineOAU frmOAU)
        {
                        

        }

        public static void getFanHP(string hpIndStr, string pwrExtHPIndStr, frmNewLineOAU frmNewOAU)
        {
            string hpStr = "";

            if ((hpIndStr == "A") || (hpIndStr == "B"))
            {
                hpStr = "0.5";
            }
            else if ((hpIndStr == "C") || (hpIndStr == "D"))
            {
                hpStr = "0.75";
            }
            else if ((hpIndStr == "E") || (hpIndStr == "F"))
            {
                hpStr = "1.0";
            }
            else if ((hpIndStr == "G") || (hpIndStr == "H"))
            {
                hpStr = "1.5";
            }
            else if ((hpIndStr == "J") || (hpIndStr == "K"))
            {
                hpStr = "2.0";
            }
            else if ((hpIndStr == "L") || (hpIndStr == "M"))
            {
                hpStr = "3.0";
            }
            else if ((hpIndStr == "N") || (hpIndStr == "P"))
            {
                hpStr = "5.0";
            }
            else if ((hpIndStr == "R") || (hpIndStr == "S"))
            {
                hpStr = "7.5";
            }
            else if ((hpIndStr == "T") || (hpIndStr == "U"))
            {
                hpStr = "10.0";
            }
            else if ((hpIndStr == "V") || (hpIndStr == "W"))
            {
                hpStr = "15.0";
            }

            frmNewOAU.textBoxNewOAUFanEvapHP.Text = hpStr;

            if (pwrExtHPIndStr != "0")
            {
                frmNewOAU.textBoxNewOAUFanPwrHP.Text = hpStr;
            }

        }

        private static int getVoltage(string digitStr)
        {
            int retValInt = -1;

            if (digitStr == "3")  // Indicates 208/230/60/3 voltage
            {
                retValInt = 0;
            }
            else if (digitStr == "4") // Indicates 208/230/60/3 voltage
            {
                retValInt = 1;
            }
            else if (digitStr == "5") // Indicates 208/230/60/3 voltage
            {
                retValInt = 2;
            }

            return retValInt;

        }


        public static void populateFanCond_ComprData(string cabSizeStr, string grossCoolingStr, string pwrExtStr,
                                                     string ervStr, frmNewLineOAU frmNewOAU)
        {
            if (cabSizeStr == "3")
            {
                frmNewOAU.textBoxNewOAUFanCondQty.Text = "4";
            }
            else if (cabSizeStr == "2")
            {
                frmNewOAU.textBoxNewOAUFanCondQty.Text = "3";
            }
            else if (cabSizeStr == "1")
            {
                if ((grossCoolingStr == "060") || (grossCoolingStr == "072") || (grossCoolingStr == "084"))
                {
                    frmNewOAU.textBoxNewOAUFanCondQty.Text = "1";
                }
                else
                {
                    frmNewOAU.textBoxNewOAUFanCondQty.Text = "2";
                }
            }

            if (ervStr != "0")
            {
                frmNewOAU.textBoxNewOAUFanEnergyQty.Text = "1";
            }

            frmNewOAU.textBoxNewOAUFanCondPH.Text = "3";
            frmNewOAU.textBoxNewOAUFanEvapPH.Text = "3";

            if (Int32.Parse(pwrExtStr) > 0)
            {
                frmNewOAU.textBoxNewOAUFanPwrPH.Text = "3";
            }

            if (frmNewOAU.comboBoxNewOAUVoltage.SelectedIndex == 1)
            {
                frmNewOAU.textBoxNewOAUFanCondFLA.Text = "1.7-460";
                //frmNewOAU.textBoxNewOAUFanCondHP.Text = "1.0";
            }
            else if (frmNewOAU.comboBoxNewOAUVoltage.SelectedIndex == 0)
            {
                if (cabSizeStr == "3") // Indicates unit is an OA3
                {
                    frmNewOAU.textBoxNewOAUFanCondFLA.Text = "3.6-208";
                    //frmNewOAU.textBoxNewOAUFanCondHP.Text = "1.0";
                }
                else
                {
                    frmNewOAU.textBoxNewOAUFanCondFLA.Text = "2.5-208";
                    //frmNewOAU.textBoxNewOAUFanCondHP.Text = "0.5";
                }
            }

            frmNewOAU.txtComp1Qty.Text = "1";
            frmNewOAU.txtComp1Phase.Text = "3";

            if (Int32.Parse(grossCoolingStr) > 84)
            {
                frmNewOAU.txtComp2Qty.Text = "1";
                frmNewOAU.txtComp2Phase.Text = "3";
            }

            if (Int32.Parse(grossCoolingStr) > 360)
            {
                frmNewOAU.txtComp3Qty.Text = "1";
                frmNewOAU.txtComp3Phase.Text = "3";
            }

            if (Int32.Parse(grossCoolingStr) > 540)
            {
                frmNewOAU.txtComp4Qty.Text = "1";
                frmNewOAU.txtComp4Phase.Text = "3";
            }
        }

        public static void parse39DigitModelNo(string modelNumStr, frmNewLineOAU frmOAU)
        {
            string digitStr;
            string pwrExtDigitStr;
            string ervDigitStr;
            string grossCoolingStr;

            // Determine which Heating Type settings are used by passing the
            // 20th digit of the ModelNo to the getheatingType function. NOTE:
            // When you assign the return value to the SelectedIndex parameter
            // this will cause the SelectedIndexChanged event for this combobox
            // to fire. This event can be found in the frmNewLineOAU code.     
            digitStr = modelNumStr.Substring(19, 1);

            if (digitStr == "0")  // Indicates No Heat
            {
                frmOAU.comboBoxNewOAUHeatingType.SelectedIndex = 2;
            }
            else if (digitStr == "A") // Indicates Indirect-Fired (IF)
            {
                frmOAU.comboBoxNewOAUHeatingType.SelectedIndex = 0;
            }
            else if (digitStr == "B") // Indicates Direct-Fired (DF)
            {
                frmOAU.comboBoxNewOAUHeatingType.SelectedIndex = 3;
            }
            else if ((digitStr == "C") || (digitStr == "D")) // Indicates Electric
            {
                frmOAU.comboBoxNewOAUHeatingType.SelectedIndex = 1;
            }
            else if (digitStr == "J") // Indicates Hot Water
            {
                frmOAU.comboBoxNewOAUHeatingType.SelectedIndex = 4;
            }
            else if (digitStr == "K") // Indicates Steam
            {
                frmOAU.comboBoxNewOAUHeatingType.SelectedIndex = 5;
            }
            else if (digitStr == "G") // Indicates Dual Heat Source Indirect Fired/Electric
            {
                frmOAU.comboBoxNewOAUHeatingType.SelectedIndex = 6;
            }
            else if (digitStr == "H") // Indicates Dual Heat Source Electric/Electric
            {
                frmOAU.comboBoxNewOAUHeatingType.SelectedIndex = 7;
            }
            else if (digitStr == "P") // Indicates Dual Heat Source Hot Water/Direct Fired
            {
                frmOAU.comboBoxNewOAUHeatingType.SelectedIndex = 8;
            }
            else if (digitStr == "Q") // Indicates Dual Heat Source Hot Water/Electric
            {
                frmOAU.comboBoxNewOAUHeatingType.SelectedIndex = 7;
            }   

            // Retrieve Voltage Selection.
            frmOAU.comboBoxNewOAUVoltage.SelectedIndex = getVoltage(modelNumStr.Substring(8, 1));

            // Copy the 27th digit of the model number. This value represents
            // Powered Exhaust.
            pwrExtDigitStr = modelNumStr.Substring(26, 1);

            // Copy the 31st digit of the model number. This value represents ERV.
            ervDigitStr = modelNumStr.Substring(30, 1);

            grossCoolingStr = modelNumStr.Substring(4, 3);

            if (Int32.Parse(grossCoolingStr) > 0)
            {
                GlobalFunctions.populateFanCond_ComprData(modelNumStr.Substring(2, 1),
                    modelNumStr.Substring(4, 3), pwrExtDigitStr, ervDigitStr, frmOAU);

                GlobalFunctions.getFanHP(modelNumStr.Substring(17, 1), modelNumStr.Substring(28, 1), frmOAU);
            }
            //if (frmOAU.comboBoxNewOAUHeatingType.SelectedIndex == 0)
            //{
            //    frmOAU.textBoxNewOAUIndFireMaxHtgInput.Focus();
            //    frmOAU.textBoxNewOAUIndFireMaxHtgInput.Select();
            //}
            //else if (frmOAU.comboBoxNewOAUHeatingType.SelectedIndex == 1)
            //{
            //    frmOAU.textBoxNewOAUElecHeatingInputElec.Focus();
            //    frmOAU.textBoxNewOAUElecHeatingInputElec.Select();
            //}


        }

    }
}
