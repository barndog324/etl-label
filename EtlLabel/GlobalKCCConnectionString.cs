using System;
using System.Collections.Generic;
using System.Text;

namespace KCC.OA.Etl
{
    static class GlobalKCCConnectionString
    {
        private static string m_globalKCCConnString = "";

        public static string KCCConnectString
        {
            get { return m_globalKCCConnString; }
            set { m_globalKCCConnString = value; }
        }        
       
    }
}
