using System;
using System.Collections.Generic;
using System.Text;

namespace KCC.OA.Etl
{
    static class GlobalModeType
    {
        private static string m_globalModeTypeStr = "";

        public static string ModeType
        {
            get { return m_globalModeTypeStr; }
            set { m_globalModeTypeStr = value; }
        }        
    }
}
