using System;
using System.Collections.Generic;
using System.Text;

namespace KCC.OA.Etl
{
    static class GlobalModelNoVerified
    {
        
        private static Boolean m_globalModelNoVerifiedBool = false;

        public static Boolean ModelNoVerified
        {
            get { return m_globalModelNoVerifiedBool; }
            set { m_globalModelNoVerifiedBool = value; }
        }        
    }
}
