using System;
using System.Collections.Generic;
using System.Text;

namespace KCC.OA.Etl
{
    static class GlobalRRUModelNo
    {
        private static int m_globalRRUModelNoSelInt = -1;

        public static int ModelNoSelInt
        {
            get { return m_globalRRUModelNoSelInt; }
            set { m_globalRRUModelNoSelInt = value; }
        }        
    }
}
