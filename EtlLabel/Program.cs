using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace KCC.OA.Etl
{
    static class Program
    {
        /// <summary>
        /// The man entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            GlobalEpicorConnectionString.EpicorConnectString =
                "Data Source=epicor905sql\\epicor905;Initial Catalog=epicor905;Integrated Security=True";
            GlobalKCCConnectionString.KCCConnectString =
                "Data Source=epicor905sql\\epicor905;Initial Catalog=KCC;Integrated Security=True";        

            //GlobalEpicorConnectionString.EpicorConnectString =
            //  "Data Source=kccwvpepic9sql;Initial Catalog=epicor905;Integrated Security=True";
            //GlobalKCCConnectionString.KCCConnectString =
            //    "Data Source=kccwvpepic9sql;Initial Catalog=KCC;Integrated Security=True";

#if Debug
            GlobalEpicorConnectionString.EpicorConnectString =
               "Data Source=DEVSVR01;Initial Catalog=epicor905;Integrated Security=True";
            GlobalKCCConnectionString.KCCConnectString =
                "Data Source=DEVSVR01;Initial Catalog=KCC;Integrated Security=True";
#endif

#if TEST2
            GlobalEpicorConnectionString.EpicorConnectString =
               "Data Source=KCCWVTEPICSQL01;Initial Catalog=epicor905;Integrated Security=True";
            GlobalKCCConnectionString.KCCConnectString =
                "Data Source=KCCWVTEPICSQL01;Initial Catalog=KCC;Integrated Security=True";
   
#endif

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmViperOpen());

        }
    }
}