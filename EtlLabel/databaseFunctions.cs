using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;

namespace KCC.OA.Etl
{
    class databaseFunctions
    {
        public string getCustomerName(int orderNumInt)
        {
            string customerNameStr = "";
            string custNumStr = "";

            VS2_DataSetTableAdapters.orderhedTableAdapter orderHedTA = 
                new VS2_DataSetTableAdapters.orderhedTableAdapter();
            
            orderHedTA.Connection.ConnectionString = GlobalEpicorConnectionString.EpicorConnectString;

            VS2_DataSetTableAdapters.customerTableAdapter custTA = 
                new VS2_DataSetTableAdapters.customerTableAdapter();
            custTA.Connection.ConnectionString = GlobalEpicorConnectionString.EpicorConnectString;

            try
            {
                VS2_DataSet.orderhedDataTable orderHedDT = orderHedTA.GetDataByOrderNum(orderNumInt);

                if (orderHedDT.Count > 0)
                {
                    DataRow dr = orderHedDT.Rows[0];
                    custNumStr = dr["custnum"].ToString();

                    try
                    {
                        VS2_DataSet.customerDataTable custDT = custTA.GetDataByCustNum(Int32.Parse(custNumStr));

                        if (custDT.Count > 0)
                        {
                            DataRow dr2 = custDT.Rows[0];
                            customerNameStr = dr2["name"].ToString();
                        }
                    }
                    catch (Exception f)
                    {
                        MessageBox.Show("ERROR - Attempting to read customer table for custnum = " +
                            custNumStr + " - " + f);
                    }
                }
            }
            catch (Exception f)
            {
                MessageBox.Show("ERROR - Attempting to read orderhed table for ordernum = " +
                    orderNumInt.ToString() + " - " + f);
            }
            return customerNameStr;
        }
    }
}
