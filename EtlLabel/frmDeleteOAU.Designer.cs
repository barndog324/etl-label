namespace KCC.OA.Etl
{
    partial class frmDeleteOAU
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelDelOrderNum = new System.Windows.Forms.Label();
            this.textBoxDelOrderNum = new System.Windows.Forms.TextBox();
            this.labelDelUnitType = new System.Windows.Forms.Label();
            this.comboBoxDelUnitType = new System.Windows.Forms.ComboBox();
            this.buttonDelDelete = new System.Windows.Forms.Button();
            this.buttonDelCancel = new System.Windows.Forms.Button();
            this.labelDelLineNumRelNum = new System.Windows.Forms.Label();
            this.textBoxDelOAULineNum = new System.Windows.Forms.TextBox();
            this.labelDelOAUSlash = new System.Windows.Forms.Label();
            this.textBoxDelOAURelNum = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.groupBox1.Controls.Add(this.textBoxDelOAURelNum);
            this.groupBox1.Controls.Add(this.labelDelOAUSlash);
            this.groupBox1.Controls.Add(this.textBoxDelOAULineNum);
            this.groupBox1.Controls.Add(this.labelDelLineNumRelNum);
            this.groupBox1.Controls.Add(this.buttonDelCancel);
            this.groupBox1.Controls.Add(this.buttonDelDelete);
            this.groupBox1.Controls.Add(this.comboBoxDelUnitType);
            this.groupBox1.Controls.Add(this.labelDelUnitType);
            this.groupBox1.Controls.Add(this.textBoxDelOrderNum);
            this.groupBox1.Controls.Add(this.labelDelOrderNum);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(268, 238);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Delete Order Num";
            // 
            // labelDelOrderNum
            // 
            this.labelDelOrderNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDelOrderNum.Location = new System.Drawing.Point(36, 54);
            this.labelDelOrderNum.Name = "labelDelOrderNum";
            this.labelDelOrderNum.Size = new System.Drawing.Size(90, 23);
            this.labelDelOrderNum.TabIndex = 0;
            this.labelDelOrderNum.Text = "Order Num:";
            this.labelDelOrderNum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxDelOrderNum
            // 
            this.textBoxDelOrderNum.BackColor = System.Drawing.Color.White;
            this.textBoxDelOrderNum.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDelOrderNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxDelOrderNum.Location = new System.Drawing.Point(132, 57);
            this.textBoxDelOrderNum.Name = "textBoxDelOrderNum";
            this.textBoxDelOrderNum.Size = new System.Drawing.Size(72, 23);
            this.textBoxDelOrderNum.TabIndex = 1;
            this.textBoxDelOrderNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelDelUnitType
            // 
            this.labelDelUnitType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDelUnitType.Location = new System.Drawing.Point(36, 120);
            this.labelDelUnitType.Name = "labelDelUnitType";
            this.labelDelUnitType.Size = new System.Drawing.Size(90, 23);
            this.labelDelUnitType.TabIndex = 2;
            this.labelDelUnitType.Text = "Unit Type:";
            this.labelDelUnitType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // comboBoxDelUnitType
            // 
            this.comboBoxDelUnitType.BackColor = System.Drawing.Color.White;
            this.comboBoxDelUnitType.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxDelUnitType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.comboBoxDelUnitType.FormattingEnabled = true;
            this.comboBoxDelUnitType.Items.AddRange(new object[] {
            "OAU",
            "RRU",
            "MUA",
            "FAN"});
            this.comboBoxDelUnitType.Location = new System.Drawing.Point(132, 120);
            this.comboBoxDelUnitType.Name = "comboBoxDelUnitType";
            this.comboBoxDelUnitType.Size = new System.Drawing.Size(72, 24);
            this.comboBoxDelUnitType.TabIndex = 4;
            // 
            // buttonDelDelete
            // 
            this.buttonDelDelete.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDelDelete.Location = new System.Drawing.Point(41, 170);
            this.buttonDelDelete.Name = "buttonDelDelete";
            this.buttonDelDelete.Size = new System.Drawing.Size(75, 30);
            this.buttonDelDelete.TabIndex = 5;
            this.buttonDelDelete.Text = "Delete";
            this.buttonDelDelete.UseVisualStyleBackColor = true;
            this.buttonDelDelete.Click += new System.EventHandler(this.buttonDelDelete_Click);
            // 
            // buttonDelCancel
            // 
            this.buttonDelCancel.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDelCancel.Location = new System.Drawing.Point(151, 170);
            this.buttonDelCancel.Name = "buttonDelCancel";
            this.buttonDelCancel.Size = new System.Drawing.Size(75, 30);
            this.buttonDelCancel.TabIndex = 6;
            this.buttonDelCancel.Text = "Cancel";
            this.buttonDelCancel.UseVisualStyleBackColor = true;
            this.buttonDelCancel.Click += new System.EventHandler(this.buttonDelCancel_Click);
            // 
            // labelDelLineNumRelNum
            // 
            this.labelDelLineNumRelNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDelLineNumRelNum.Location = new System.Drawing.Point(36, 88);
            this.labelDelLineNumRelNum.Name = "labelDelLineNumRelNum";
            this.labelDelLineNumRelNum.Size = new System.Drawing.Size(90, 23);
            this.labelDelLineNumRelNum.TabIndex = 6;
            this.labelDelLineNumRelNum.Text = "Line #/Rel #:";
            this.labelDelLineNumRelNum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxDelOAULineNum
            // 
            this.textBoxDelOAULineNum.BackColor = System.Drawing.Color.White;
            this.textBoxDelOAULineNum.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDelOAULineNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxDelOAULineNum.Location = new System.Drawing.Point(132, 88);
            this.textBoxDelOAULineNum.Name = "textBoxDelOAULineNum";
            this.textBoxDelOAULineNum.Size = new System.Drawing.Size(40, 23);
            this.textBoxDelOAULineNum.TabIndex = 2;
            this.textBoxDelOAULineNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelDelOAUSlash
            // 
            this.labelDelOAUSlash.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDelOAUSlash.Location = new System.Drawing.Point(178, 88);
            this.labelDelOAUSlash.Name = "labelDelOAUSlash";
            this.labelDelOAUSlash.Size = new System.Drawing.Size(15, 23);
            this.labelDelOAUSlash.TabIndex = 8;
            this.labelDelOAUSlash.Text = "/";
            this.labelDelOAUSlash.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxDelOAURelNum
            // 
            this.textBoxDelOAURelNum.BackColor = System.Drawing.Color.White;
            this.textBoxDelOAURelNum.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDelOAURelNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxDelOAURelNum.Location = new System.Drawing.Point(191, 88);
            this.textBoxDelOAURelNum.Name = "textBoxDelOAURelNum";
            this.textBoxDelOAURelNum.Size = new System.Drawing.Size(40, 23);
            this.textBoxDelOAURelNum.TabIndex = 3;
            this.textBoxDelOAURelNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // frmDeleteOAU
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(292, 262);
            this.Controls.Add(this.groupBox1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Name = "frmDeleteOAU";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Delete OAU";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelDelOrderNum;
        private System.Windows.Forms.Button buttonDelCancel;
        private System.Windows.Forms.Button buttonDelDelete;
        private System.Windows.Forms.Label labelDelUnitType;
        public System.Windows.Forms.TextBox textBoxDelOrderNum;
        public System.Windows.Forms.ComboBox comboBoxDelUnitType;
        public System.Windows.Forms.TextBox textBoxDelOAURelNum;
        private System.Windows.Forms.Label labelDelOAUSlash;
        public System.Windows.Forms.TextBox textBoxDelOAULineNum;
        private System.Windows.Forms.Label labelDelLineNumRelNum;
    }
}