using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace KCC.OA.Etl
{
    public partial class frmDeleteOAU : Form
    {
        public frmDeleteOAU()
        {
            InitializeComponent();
        }

        private void buttonDelCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonDelDelete_Click(object sender, EventArgs e)
        {
            string orderNumStr = "";
            string orderLineStr = "";
            string releaseNumStr = "";

            orderNumStr = this.textBoxDelOrderNum.Text.Trim();
            orderLineStr = this.textBoxDelOAULineNum.Text.Trim();
            releaseNumStr = this.textBoxDelOAURelNum.Text.Trim();

            // Create SqlDataAdapters for reading the VSOrderHed 
            // table using strongly typed datasets.
            VS_DataSetTableAdapters.VSOrderHedTableAdapter vsOrderHedTA =
                new VS_DataSetTableAdapters.VSOrderHedTableAdapter();

            vsOrderHedTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

            // Read the VSOrderHed table to see if the Sales Order Number entered by the
            // user exist in the table. If it does already exist display a message box
            // indicating that, Otherwise display the Unit Info form for the user to 
            // input the data in.
            try
            {
                vsOrderHedTA.DeleteByOrderNumOrderLineReleaseNum(orderNumStr, orderLineStr, releaseNumStr);
                MessageBox.Show("Order # " + orderNumStr + " successfully deleted from VSOrderHed");
                this.Close();
            }
            catch
            {
                MessageBox.Show("Error, Deleting row from VSOrderHed");
            }
        }
    }
}