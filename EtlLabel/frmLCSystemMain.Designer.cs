﻿namespace KCC.OA.Etl
{
    partial class frmLCSystemMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rbModelMCC3208 = new System.Windows.Forms.RadioButton();
            this.rbModelMCC6208 = new System.Windows.Forms.RadioButton();
            this.rbModelMCCV3208 = new System.Windows.Forms.RadioButton();
            this.rbModelMCC5208 = new System.Windows.Forms.RadioButton();
            this.rbModelMCC6208w_FLAs = new System.Windows.Forms.RadioButton();
            this.btnDisplay = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rbModelMCC3208
            // 
            this.rbModelMCC3208.AutoSize = true;
            this.rbModelMCC3208.Checked = true;
            this.rbModelMCC3208.Location = new System.Drawing.Point(78, 25);
            this.rbModelMCC3208.Name = "rbModelMCC3208";
            this.rbModelMCC3208.Size = new System.Drawing.Size(104, 17);
            this.rbModelMCC3208.TabIndex = 0;
            this.rbModelMCC3208.TabStop = true;
            this.rbModelMCC3208.Text = "Model MCC3208";
            this.rbModelMCC3208.UseVisualStyleBackColor = true;
            // 
            // rbModelMCC6208
            // 
            this.rbModelMCC6208.AutoSize = true;
            this.rbModelMCC6208.Location = new System.Drawing.Point(78, 115);
            this.rbModelMCC6208.Name = "rbModelMCC6208";
            this.rbModelMCC6208.Size = new System.Drawing.Size(104, 17);
            this.rbModelMCC6208.TabIndex = 1;
            this.rbModelMCC6208.TabStop = true;
            this.rbModelMCC6208.Text = "Model MCC6208";
            this.rbModelMCC6208.UseVisualStyleBackColor = true;
            // 
            // rbModelMCCV3208
            // 
            this.rbModelMCCV3208.AutoSize = true;
            this.rbModelMCCV3208.Location = new System.Drawing.Point(78, 55);
            this.rbModelMCCV3208.Name = "rbModelMCCV3208";
            this.rbModelMCCV3208.Size = new System.Drawing.Size(111, 17);
            this.rbModelMCCV3208.TabIndex = 2;
            this.rbModelMCCV3208.TabStop = true;
            this.rbModelMCCV3208.Text = "Model MCCV3208";
            this.rbModelMCCV3208.UseVisualStyleBackColor = true;
            // 
            // rbModelMCC5208
            // 
            this.rbModelMCC5208.AutoSize = true;
            this.rbModelMCC5208.Location = new System.Drawing.Point(78, 85);
            this.rbModelMCC5208.Name = "rbModelMCC5208";
            this.rbModelMCC5208.Size = new System.Drawing.Size(104, 17);
            this.rbModelMCC5208.TabIndex = 3;
            this.rbModelMCC5208.TabStop = true;
            this.rbModelMCC5208.Text = "Model MCC5208";
            this.rbModelMCC5208.UseVisualStyleBackColor = true;
            // 
            // rbModelMCC6208w_FLAs
            // 
            this.rbModelMCC6208w_FLAs.AutoSize = true;
            this.rbModelMCC6208w_FLAs.Location = new System.Drawing.Point(78, 145);
            this.rbModelMCC6208w_FLAs.Name = "rbModelMCC6208w_FLAs";
            this.rbModelMCC6208w_FLAs.Size = new System.Drawing.Size(144, 17);
            this.rbModelMCC6208w_FLAs.TabIndex = 4;
            this.rbModelMCC6208w_FLAs.TabStop = true;
            this.rbModelMCC6208w_FLAs.Text = "Model MCC6208 w/FLAs";
            this.rbModelMCC6208w_FLAs.UseVisualStyleBackColor = true;
            // 
            // btnDisplay
            // 
            this.btnDisplay.ForeColor = System.Drawing.Color.Blue;
            this.btnDisplay.Location = new System.Drawing.Point(56, 180);
            this.btnDisplay.Name = "btnDisplay";
            this.btnDisplay.Size = new System.Drawing.Size(75, 23);
            this.btnDisplay.TabIndex = 5;
            this.btnDisplay.Text = "Display";
            this.btnDisplay.UseVisualStyleBackColor = true;
            this.btnDisplay.Click += new System.EventHandler(this.btnDisplay_Click);
            // 
            // btnExit
            // 
            this.btnExit.ForeColor = System.Drawing.Color.Red;
            this.btnExit.Location = new System.Drawing.Point(159, 180);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 6;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // frmLCSystemMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 241);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnDisplay);
            this.Controls.Add(this.rbModelMCC6208w_FLAs);
            this.Controls.Add(this.rbModelMCC5208);
            this.Controls.Add(this.rbModelMCCV3208);
            this.Controls.Add(this.rbModelMCC6208);
            this.Controls.Add(this.rbModelMCC3208);
            this.Name = "frmLCSystemMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LC Systems Main";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbModelMCC3208;
        private System.Windows.Forms.RadioButton rbModelMCC6208;
        private System.Windows.Forms.RadioButton rbModelMCCV3208;
        private System.Windows.Forms.RadioButton rbModelMCC5208;
        private System.Windows.Forms.RadioButton rbModelMCC6208w_FLAs;
        private System.Windows.Forms.Button btnDisplay;
        private System.Windows.Forms.Button btnExit;
    }
}