﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KCC.OA.Etl
{
    public partial class frmLCSystemMain : Form
    {
        public frmLCSystemMain()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDisplay_Click(object sender, EventArgs e)
        {
            frmLCSystemsDataInput frmLC = new frmLCSystemsDataInput();

            if (rbModelMCC3208.Checked == true)
            {
                frmLC.lbModel.Text = "MCC3208";
                frmLC.lbFactoryID.Text = "KCCG1";
                frmLC.txtEnclosureNEMAType.Text = "1";
                frmLC.txtPowerSupply.Text = "";
                frmLC.txtVoltage.Text = "208";
                frmLC.txtMotorAmps.Text = "9.9";
                frmLC.txtMCA.Text = "15.9";
                frmLC.txtSupplyCircuit.Text = "";
                frmLC.txtVoltage2.Text = "120";
                frmLC.txtAmpLightInterlock.Text = "<1";
                frmLC.txtAmpFireSystemInterlock.Text = "<1";
                frmLC.txtSCCR.Text = "5";
                frmLC.txtMotorsToBeFieldConnected.Text = "3";
                frmLC.txtMotorHP.Text = "(2) 0.5 (1) 1.0";
                frmLC.txtMotorAmps.Text = "(2) 2.4 (1) 4.6";
                frmLC.txtLockedRotorAmps.Text = "(2) 22.1 (1) 33";               
            }
            else if (rbModelMCCV3208.Checked == true)
            {
                frmLC.lbModel.Text = "MCCV3208";
                frmLC.lbFactoryID.Text = "KCCG1";
                frmLC.txtEnclosureNEMAType.Text = "1";
                frmLC.txtPowerSupply.Text = "";
                frmLC.txtVoltage.Text = "208";
                frmLC.txtMotorAmps.Text = "13.8";
                frmLC.txtMCA.Text = "17.3";
                frmLC.txtSupplyCircuit.Text = "";
                frmLC.txtVoltage2.Text = "120";
                frmLC.txtAmpLightInterlock.Text = "<1";
                frmLC.txtAmpFireSystemInterlock.Text = "<1";
                frmLC.txtSCCR.Text = "5";
                frmLC.txtMotorsToBeFieldConnected.Text = "3";
                frmLC.txtMotorHP.Text = "(3) 1.0";
                frmLC.txtMotorAmps.Text = "4.6";
                frmLC.txtLockedRotorAmps.Text = "33";          
            }
            else if (rbModelMCC5208.Checked == true)
            {
                frmLC.lbModel.Text = "MCC5208";
                frmLC.lbFactoryID.Text = "KCCG1";
                frmLC.txtEnclosureNEMAType.Text = "1";
                frmLC.txtPowerSupply.Text = "";
                frmLC.txtVoltage.Text = "208";
                frmLC.txtMotorAmps.Text = "13.8";
                frmLC.txtMCA.Text = "17.3";
                frmLC.txtSupplyCircuit.Text = "";
                frmLC.txtVoltage2.Text = "";
                frmLC.txtAmpLightInterlock.Text = "<1";
                frmLC.txtAmpFireSystemInterlock.Text = "<1";
                frmLC.txtSCCR.Text = "5";
                frmLC.txtMotorsToBeFieldConnected.Text = "5";
                frmLC.txtMotorHP.Text = "1.5";
                frmLC.txtMotorAmps.Text = "6.6";
                frmLC.txtLockedRotorAmps.Text = "44";          
            }
            else if (rbModelMCC6208.Checked == true)
            {
                frmLC.lbModel.Text = "MCC6208";
                frmLC.lbFactoryID.Text = "KCCG1";
                frmLC.txtEnclosureNEMAType.Text = "1";
                frmLC.txtPowerSupply.Text = "";
                frmLC.txtVoltage.Text = "208";
                frmLC.txtMotorAmps.Text = "";
                frmLC.txtMCA.Text = "39.6";
                frmLC.txtSupplyCircuit.Text = "";
                frmLC.txtVoltage2.Text = "120";
                frmLC.txtAmpLightInterlock.Text = "";
                frmLC.txtAmpFireSystemInterlock.Text = "";
                frmLC.txtSCCR.Text = "5";
                frmLC.txtMotorsToBeFieldConnected.Text = "6";
                frmLC.txtMotorHP.Text = "1.5";
                frmLC.txtMotorAmps.Text = "";
                frmLC.txtLockedRotorAmps.Text = "44";        
            }
            else if (rbModelMCC6208w_FLAs.Checked == true)
            {
                frmLC.lbModel.Text = "MCC6208wFLAs";
                frmLC.lbFactoryID.Text = "KCCG1";
                frmLC.txtEnclosureNEMAType.Text = "1";
                frmLC.txtPowerSupply.Text = "";
                frmLC.txtVoltage.Text = "208";
                frmLC.txtMotorAmps.Text = "";
                frmLC.txtMCA.Text = "39.6";
                frmLC.txtSupplyCircuit.Text = "";
                frmLC.txtVoltage2.Text = "120";
                frmLC.txtAmpLightInterlock.Text = "<1";
                frmLC.txtAmpFireSystemInterlock.Text = "<1";
                frmLC.txtSCCR.Text = "5";
                frmLC.txtMotorsToBeFieldConnected.Text = "6";
                frmLC.txtMotorHP.Text = "";
                frmLC.txtMotorAmps.Text = "";
                frmLC.txtLockedRotorAmps.Text = "";
                frmLC.gbMotorFLAs.Visible = true;
                frmLC.txtMotor1FLA.Text = "(4) 1.5HP";
                frmLC.txtMotor2FLA.Text = "(1) 0.5HP";
                frmLC.txtMotor3FLA.Text = "(1) 5.0HP";
            }

            frmLC.ShowDialog();
        }
    }
}
