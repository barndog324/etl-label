﻿namespace KCC.OA.Etl
{
    partial class frmLCSystemsDataInput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbModel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtEnclosureNEMAType = new System.Windows.Forms.TextBox();
            this.txtPowerSupply = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtVoltage = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMCA = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSupplyCircuit = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtVoltage2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAmpLightInterlock = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtAmpFireSystemInterlock = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSCCR = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtMotorsToBeFieldConnected = new System.Windows.Forms.TextBox();
            this.lbFieldConnectedMotors = new System.Windows.Forms.Label();
            this.txtMotorHP = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMotorAmps = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtLockedRotorAmps = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.gbMotorFLAs = new System.Windows.Forms.GroupBox();
            this.txtMotor3FLA = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtMotor2FLA = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtMotor1FLA = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnDisplay = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.lbFactoryID = new System.Windows.Forms.Label();
            this.gbMotorFLAs.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbModel
            // 
            this.lbModel.ForeColor = System.Drawing.Color.Blue;
            this.lbModel.Location = new System.Drawing.Point(189, 13);
            this.lbModel.Name = "lbModel";
            this.lbModel.Size = new System.Drawing.Size(155, 23);
            this.lbModel.TabIndex = 0;
            this.lbModel.Text = "Model Type";
            this.lbModel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(84, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "Enclosure NEMA Type:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtEnclosureNEMAType
            // 
            this.txtEnclosureNEMAType.ForeColor = System.Drawing.Color.Blue;
            this.txtEnclosureNEMAType.Location = new System.Drawing.Point(263, 80);
            this.txtEnclosureNEMAType.Name = "txtEnclosureNEMAType";
            this.txtEnclosureNEMAType.Size = new System.Drawing.Size(100, 20);
            this.txtEnclosureNEMAType.TabIndex = 2;
            // 
            // txtPowerSupply
            // 
            this.txtPowerSupply.ForeColor = System.Drawing.Color.Blue;
            this.txtPowerSupply.Location = new System.Drawing.Point(263, 115);
            this.txtPowerSupply.Name = "txtPowerSupply";
            this.txtPowerSupply.Size = new System.Drawing.Size(100, 20);
            this.txtPowerSupply.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(84, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(175, 23);
            this.label2.TabIndex = 3;
            this.label2.Text = "Power Supply:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtVoltage
            // 
            this.txtVoltage.ForeColor = System.Drawing.Color.Blue;
            this.txtVoltage.Location = new System.Drawing.Point(263, 150);
            this.txtVoltage.Name = "txtVoltage";
            this.txtVoltage.Size = new System.Drawing.Size(100, 20);
            this.txtVoltage.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(84, 150);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(175, 23);
            this.label3.TabIndex = 5;
            this.label3.Text = "Voltage:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMCA
            // 
            this.txtMCA.ForeColor = System.Drawing.Color.Blue;
            this.txtMCA.Location = new System.Drawing.Point(263, 220);
            this.txtMCA.Name = "txtMCA";
            this.txtMCA.Size = new System.Drawing.Size(100, 20);
            this.txtMCA.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(84, 220);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(175, 23);
            this.label4.TabIndex = 7;
            this.label4.Text = "MCA:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSupplyCircuit
            // 
            this.txtSupplyCircuit.ForeColor = System.Drawing.Color.Blue;
            this.txtSupplyCircuit.Location = new System.Drawing.Point(263, 255);
            this.txtSupplyCircuit.Name = "txtSupplyCircuit";
            this.txtSupplyCircuit.Size = new System.Drawing.Size(100, 20);
            this.txtSupplyCircuit.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(84, 255);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(175, 23);
            this.label5.TabIndex = 9;
            this.label5.Text = "Supply Circuit:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtVoltage2
            // 
            this.txtVoltage2.ForeColor = System.Drawing.Color.Blue;
            this.txtVoltage2.Location = new System.Drawing.Point(263, 290);
            this.txtVoltage2.Name = "txtVoltage2";
            this.txtVoltage2.Size = new System.Drawing.Size(100, 20);
            this.txtVoltage2.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(84, 290);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(175, 23);
            this.label6.TabIndex = 11;
            this.label6.Text = "Voltage:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtAmpLightInterlock
            // 
            this.txtAmpLightInterlock.ForeColor = System.Drawing.Color.Blue;
            this.txtAmpLightInterlock.Location = new System.Drawing.Point(263, 325);
            this.txtAmpLightInterlock.Name = "txtAmpLightInterlock";
            this.txtAmpLightInterlock.Size = new System.Drawing.Size(100, 20);
            this.txtAmpLightInterlock.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(84, 325);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(175, 23);
            this.label7.TabIndex = 13;
            this.label7.Text = "Amperage Light Interlock:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtAmpFireSystemInterlock
            // 
            this.txtAmpFireSystemInterlock.ForeColor = System.Drawing.Color.Blue;
            this.txtAmpFireSystemInterlock.Location = new System.Drawing.Point(263, 360);
            this.txtAmpFireSystemInterlock.Name = "txtAmpFireSystemInterlock";
            this.txtAmpFireSystemInterlock.Size = new System.Drawing.Size(100, 20);
            this.txtAmpFireSystemInterlock.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(84, 360);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(175, 23);
            this.label8.TabIndex = 15;
            this.label8.Text = "Amp Fire System Interlock:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSCCR
            // 
            this.txtSCCR.ForeColor = System.Drawing.Color.Blue;
            this.txtSCCR.Location = new System.Drawing.Point(263, 395);
            this.txtSCCR.Name = "txtSCCR";
            this.txtSCCR.Size = new System.Drawing.Size(100, 20);
            this.txtSCCR.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(84, 395);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(175, 23);
            this.label9.TabIndex = 17;
            this.label9.Text = "SCCR:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMotorsToBeFieldConnected
            // 
            this.txtMotorsToBeFieldConnected.ForeColor = System.Drawing.Color.Blue;
            this.txtMotorsToBeFieldConnected.Location = new System.Drawing.Point(263, 430);
            this.txtMotorsToBeFieldConnected.Name = "txtMotorsToBeFieldConnected";
            this.txtMotorsToBeFieldConnected.Size = new System.Drawing.Size(100, 20);
            this.txtMotorsToBeFieldConnected.TabIndex = 20;
            // 
            // lbFieldConnectedMotors
            // 
            this.lbFieldConnectedMotors.ForeColor = System.Drawing.Color.Blue;
            this.lbFieldConnectedMotors.Location = new System.Drawing.Point(84, 430);
            this.lbFieldConnectedMotors.Name = "lbFieldConnectedMotors";
            this.lbFieldConnectedMotors.Size = new System.Drawing.Size(175, 23);
            this.lbFieldConnectedMotors.TabIndex = 19;
            this.lbFieldConnectedMotors.Text = "Motors To Be Field Connected:";
            this.lbFieldConnectedMotors.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMotorHP
            // 
            this.txtMotorHP.ForeColor = System.Drawing.Color.Blue;
            this.txtMotorHP.Location = new System.Drawing.Point(263, 465);
            this.txtMotorHP.Name = "txtMotorHP";
            this.txtMotorHP.Size = new System.Drawing.Size(100, 20);
            this.txtMotorHP.TabIndex = 22;
            // 
            // label10
            // 
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(104, 465);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(153, 23);
            this.label10.TabIndex = 21;
            this.label10.Text = "Motor HP:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMotorAmps
            // 
            this.txtMotorAmps.ForeColor = System.Drawing.Color.Blue;
            this.txtMotorAmps.Location = new System.Drawing.Point(263, 500);
            this.txtMotorAmps.Name = "txtMotorAmps";
            this.txtMotorAmps.Size = new System.Drawing.Size(100, 20);
            this.txtMotorAmps.TabIndex = 24;
            // 
            // label11
            // 
            this.label11.ForeColor = System.Drawing.Color.Blue;
            this.label11.Location = new System.Drawing.Point(104, 500);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(153, 23);
            this.label11.TabIndex = 23;
            this.label11.Text = "Motor Amperage MAX:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLockedRotorAmps
            // 
            this.txtLockedRotorAmps.ForeColor = System.Drawing.Color.Blue;
            this.txtLockedRotorAmps.Location = new System.Drawing.Point(263, 535);
            this.txtLockedRotorAmps.Name = "txtLockedRotorAmps";
            this.txtLockedRotorAmps.Size = new System.Drawing.Size(100, 20);
            this.txtLockedRotorAmps.TabIndex = 26;
            // 
            // label12
            // 
            this.label12.ForeColor = System.Drawing.Color.Blue;
            this.label12.Location = new System.Drawing.Point(104, 535);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(153, 23);
            this.label12.TabIndex = 25;
            this.label12.Text = "Locked Rotor Amps per Motor:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbMotorFLAs
            // 
            this.gbMotorFLAs.BackColor = System.Drawing.Color.DarkGray;
            this.gbMotorFLAs.Controls.Add(this.txtMotor3FLA);
            this.gbMotorFLAs.Controls.Add(this.label15);
            this.gbMotorFLAs.Controls.Add(this.txtMotor2FLA);
            this.gbMotorFLAs.Controls.Add(this.label14);
            this.gbMotorFLAs.Controls.Add(this.txtMotor1FLA);
            this.gbMotorFLAs.Controls.Add(this.label13);
            this.gbMotorFLAs.ForeColor = System.Drawing.Color.Blue;
            this.gbMotorFLAs.Location = new System.Drawing.Point(107, 466);
            this.gbMotorFLAs.Name = "gbMotorFLAs";
            this.gbMotorFLAs.Size = new System.Drawing.Size(291, 113);
            this.gbMotorFLAs.TabIndex = 27;
            this.gbMotorFLAs.TabStop = false;
            this.gbMotorFLAs.Text = "Motors FLA Data";
            this.gbMotorFLAs.Visible = false;
            // 
            // txtMotor3FLA
            // 
            this.txtMotor3FLA.ForeColor = System.Drawing.Color.Blue;
            this.txtMotor3FLA.Location = new System.Drawing.Point(93, 73);
            this.txtMotor3FLA.Name = "txtMotor3FLA";
            this.txtMotor3FLA.Size = new System.Drawing.Size(163, 20);
            this.txtMotor3FLA.TabIndex = 28;
            // 
            // label15
            // 
            this.label15.ForeColor = System.Drawing.Color.Blue;
            this.label15.Location = new System.Drawing.Point(24, 73);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(63, 23);
            this.label15.TabIndex = 27;
            this.label15.Text = "Motor 3:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMotor2FLA
            // 
            this.txtMotor2FLA.ForeColor = System.Drawing.Color.Blue;
            this.txtMotor2FLA.Location = new System.Drawing.Point(93, 47);
            this.txtMotor2FLA.Name = "txtMotor2FLA";
            this.txtMotor2FLA.Size = new System.Drawing.Size(163, 20);
            this.txtMotor2FLA.TabIndex = 26;
            // 
            // label14
            // 
            this.label14.ForeColor = System.Drawing.Color.Blue;
            this.label14.Location = new System.Drawing.Point(24, 47);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 23);
            this.label14.TabIndex = 25;
            this.label14.Text = "Motor 2:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMotor1FLA
            // 
            this.txtMotor1FLA.ForeColor = System.Drawing.Color.Blue;
            this.txtMotor1FLA.Location = new System.Drawing.Point(93, 21);
            this.txtMotor1FLA.Name = "txtMotor1FLA";
            this.txtMotor1FLA.Size = new System.Drawing.Size(163, 20);
            this.txtMotor1FLA.TabIndex = 24;
            // 
            // label13
            // 
            this.label13.ForeColor = System.Drawing.Color.Blue;
            this.label13.Location = new System.Drawing.Point(24, 21);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(63, 23);
            this.label13.TabIndex = 23;
            this.label13.Text = "Motor 1:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnDisplay
            // 
            this.btnDisplay.ForeColor = System.Drawing.Color.Blue;
            this.btnDisplay.Location = new System.Drawing.Point(107, 590);
            this.btnDisplay.Name = "btnDisplay";
            this.btnDisplay.Size = new System.Drawing.Size(135, 40);
            this.btnDisplay.TabIndex = 28;
            this.btnDisplay.Text = "Display Report";
            this.btnDisplay.UseVisualStyleBackColor = true;
            this.btnDisplay.Click += new System.EventHandler(this.btnDisplay_Click);
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.Color.Red;
            this.button1.Location = new System.Drawing.Point(263, 590);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(135, 40);
            this.button1.TabIndex = 29;
            this.button1.Text = "Exit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.ForeColor = System.Drawing.Color.Blue;
            this.textBox1.Location = new System.Drawing.Point(263, 185);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 31;
            // 
            // label16
            // 
            this.label16.ForeColor = System.Drawing.Color.Blue;
            this.label16.Location = new System.Drawing.Point(84, 180);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(175, 23);
            this.label16.TabIndex = 30;
            this.label16.Text = "Motor Amperage:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbFactoryID
            // 
            this.lbFactoryID.ForeColor = System.Drawing.Color.Blue;
            this.lbFactoryID.Location = new System.Drawing.Point(189, 36);
            this.lbFactoryID.Name = "lbFactoryID";
            this.lbFactoryID.Size = new System.Drawing.Size(155, 23);
            this.lbFactoryID.TabIndex = 32;
            this.lbFactoryID.Text = "Factory ID";
            this.lbFactoryID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmLCSystemsDataInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 651);
            this.Controls.Add(this.lbFactoryID);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnDisplay);
            this.Controls.Add(this.gbMotorFLAs);
            this.Controls.Add(this.txtLockedRotorAmps);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtMotorAmps);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtMotorHP);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtMotorsToBeFieldConnected);
            this.Controls.Add(this.lbFieldConnectedMotors);
            this.Controls.Add(this.txtSCCR);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtAmpFireSystemInterlock);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtAmpLightInterlock);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtVoltage2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtSupplyCircuit);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtMCA);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtVoltage);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtPowerSupply);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtEnclosureNEMAType);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbModel);
            this.Name = "frmLCSystemsDataInput";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LC Systems Data Input";
            this.gbMotorFLAs.ResumeLayout(false);
            this.gbMotorFLAs.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbFieldConnectedMotors;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnDisplay;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.TextBox txtEnclosureNEMAType;
        public System.Windows.Forms.TextBox txtPowerSupply;
        public System.Windows.Forms.TextBox txtVoltage;
        public System.Windows.Forms.TextBox txtMCA;
        public System.Windows.Forms.TextBox txtSupplyCircuit;
        public System.Windows.Forms.TextBox txtVoltage2;
        public System.Windows.Forms.TextBox txtAmpLightInterlock;
        public System.Windows.Forms.TextBox txtAmpFireSystemInterlock;
        public System.Windows.Forms.TextBox txtSCCR;
        public System.Windows.Forms.TextBox txtMotorsToBeFieldConnected;
        public System.Windows.Forms.TextBox txtMotorHP;
        public System.Windows.Forms.TextBox txtMotorAmps;
        public System.Windows.Forms.TextBox txtLockedRotorAmps;
        public System.Windows.Forms.GroupBox gbMotorFLAs;
        public System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.Label lbFactoryID;
        public System.Windows.Forms.Label lbModel;
        public System.Windows.Forms.TextBox txtMotor3FLA;
        public System.Windows.Forms.TextBox txtMotor2FLA;
        public System.Windows.Forms.TextBox txtMotor1FLA;
    }
}