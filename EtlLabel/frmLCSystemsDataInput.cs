﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KCC.OA.Etl
{
    public partial class frmLCSystemsDataInput : Form
    {
        private string EpicorServer = "";

        public frmLCSystemsDataInput()
        {
            InitializeComponent();
        }       

        private void button1_Click(object sender, EventArgs e)
        {
            this.gbMotorFLAs.Visible = false;
            this.Close();
        }

        private void btnDisplay_Click(object sender, EventArgs e)
        {
            frmPrintLCSystems frmPrint = new frmPrintLCSystems();

            VS_DataSet ds = new VS_DataSet();
            ds.LCSystemsReportDataTable.Rows.Add(txtEnclosureNEMAType.Text,
                                                 txtPowerSupply.Text,
                                                 txtVoltage.Text,
                                                 textBox1.Text,
                                                 txtMCA.Text,
                                                 txtVoltage2.Text,
                                                 txtAmpLightInterlock.Text,
                                                 txtAmpFireSystemInterlock.Text,
                                                 txtSCCR.Text,
                                                 txtMotorsToBeFieldConnected.Text,                                              
                                                 txtMotorHP.Text,
                                                 txtMotorAmps.Text,
                                                 txtLockedRotorAmps.Text,
                                                 txtMotor1FLA.Text,
                                                 txtMotor2FLA.Text,
                                                 txtMotor3FLA.Text,
                                                 lbModel.Text,
                                                 lbFactoryID.Text);

            ReportDocument cryRpt = new ReportDocument();

            EpicorServer = ConfigurationManager.AppSettings["EpicorAppServer"];

            string reportString = @"\\" + EpicorServer + "\\Apps\\OAU\\EtlLabel\\reports\\LCIndustrialControlPanelRpt.v1.rpt";
           
            //string reportString = @"\\KCCWVPEPIC9APP\Apps\OAU\EtlLabel\reports\LCIndustrialControlPanelRpt.v1.rpt";   // Default Crystal 
            
            cryRpt.Load(reportString);

            cryRpt.SetDataSource(ds.Tables["LCSystemsReportDataTable"]);

            frmPrint.crystalReportViewer1.ReportSource = cryRpt;

            frmPrint.ShowDialog();
            frmPrint.crystalReportViewer1.Refresh();  
        }
    }
}
