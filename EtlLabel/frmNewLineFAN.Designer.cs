namespace KCC.OA.Etl
{
    partial class frmNewLineFAN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxNewFANJobInfo = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxNewFANJobNum = new System.Windows.Forms.TextBox();
            this.textBoxNewFANEndConsumer = new System.Windows.Forms.TextBox();
            this.labelNewFANEndConsumer = new System.Windows.Forms.Label();
            this.textBoxNewFANReleaseNum = new System.Windows.Forms.TextBox();
            this.labelNewFANSlash = new System.Windows.Forms.Label();
            this.textBoxNewFANCustomer = new System.Windows.Forms.TextBox();
            this.textBoxNewFANOrderNum = new System.Windows.Forms.TextBox();
            this.labelNewFANCustomer = new System.Windows.Forms.Label();
            this.labelNewFANOrderNum = new System.Windows.Forms.Label();
            this.textBoxNewFANLineItem = new System.Windows.Forms.TextBox();
            this.labelNewFANLineItem = new System.Windows.Forms.Label();
            this.menuStripNewFAN = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.groupBoxNewFANUnitInfo = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTag = new System.Windows.Forms.TextBox();
            this.txtTagNum = new System.Windows.Forms.TextBox();
            this.labelNewFANPartNum = new System.Windows.Forms.Label();
            this.textBoxNewFANPartNum = new System.Windows.Forms.TextBox();
            this.checkBoxNewFANVerified = new System.Windows.Forms.CheckBox();
            this.textBoxNewFANVerifiedBy = new System.Windows.Forms.TextBox();
            this.labelMewFANVerifiedBy = new System.Windows.Forms.Label();
            this.labelNewFANTag = new System.Windows.Forms.Label();
            this.labelNewFANSerialNo = new System.Windows.Forms.Label();
            this.textBoxNewFANSerialNo = new System.Windows.Forms.TextBox();
            this.textBoxNewFANModelNo = new System.Windows.Forms.TextBox();
            this.labelNewFANModelNum = new System.Windows.Forms.Label();
            this.groupBoxNewFANVoltageInfo = new System.Windows.Forms.GroupBox();
            this.textBoxNewFANAirRPM = new System.Windows.Forms.TextBox();
            this.labelNewFANAirRPM = new System.Windows.Forms.Label();
            this.textBoxNewFANAirESP = new System.Windows.Forms.TextBox();
            this.labelNewFANAirESP = new System.Windows.Forms.Label();
            this.textBoxNewFANAirCFM = new System.Windows.Forms.TextBox();
            this.labelNewFANAirCFM = new System.Windows.Forms.Label();
            this.textBoxNewFANElecFLA = new System.Windows.Forms.TextBox();
            this.labelNewFANElecFLA = new System.Windows.Forms.Label();
            this.textBoxNewFANElecMaxVoltage = new System.Windows.Forms.TextBox();
            this.labelNewFANElecMaxVoltage = new System.Windows.Forms.Label();
            this.textBoxNewFANElecMaxVoltageHertz = new System.Windows.Forms.TextBox();
            this.labelNewFANElecMaxVoltageHertz = new System.Windows.Forms.Label();
            this.textBoxNewFANElecMinVoltage = new System.Windows.Forms.TextBox();
            this.labelNewFANElecMinVoltage = new System.Windows.Forms.Label();
            this.textBoxNewFANElecHertz = new System.Windows.Forms.TextBox();
            this.labelNewFANElecHertz = new System.Windows.Forms.Label();
            this.textBoxNewFANElecPhase = new System.Windows.Forms.TextBox();
            this.labelNewFANElecPhase = new System.Windows.Forms.Label();
            this.textBoxNewFANElecVolts = new System.Windows.Forms.TextBox();
            this.labelNewFANElecVolts = new System.Windows.Forms.Label();
            this.textBoxNewFANElecBlowerFan = new System.Windows.Forms.TextBox();
            this.labelNewFANElecBlowerFan = new System.Windows.Forms.Label();
            this.buttonNewFANPrint = new System.Windows.Forms.Button();
            this.buttonNewFANAccept = new System.Windows.Forms.Button();
            this.buttonNewFANCancel = new System.Windows.Forms.Button();
            this.labelNewFANMsg = new System.Windows.Forms.Label();
            this.labelNewFANMsg2 = new System.Windows.Forms.Label();
            this.groupBoxNewFANJobInfo.SuspendLayout();
            this.menuStripNewFAN.SuspendLayout();
            this.groupBoxNewFANUnitInfo.SuspendLayout();
            this.groupBoxNewFANVoltageInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxNewFANJobInfo
            // 
            this.groupBoxNewFANJobInfo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBoxNewFANJobInfo.Controls.Add(this.label1);
            this.groupBoxNewFANJobInfo.Controls.Add(this.textBoxNewFANJobNum);
            this.groupBoxNewFANJobInfo.Controls.Add(this.textBoxNewFANEndConsumer);
            this.groupBoxNewFANJobInfo.Controls.Add(this.labelNewFANEndConsumer);
            this.groupBoxNewFANJobInfo.Controls.Add(this.textBoxNewFANReleaseNum);
            this.groupBoxNewFANJobInfo.Controls.Add(this.labelNewFANSlash);
            this.groupBoxNewFANJobInfo.Controls.Add(this.textBoxNewFANCustomer);
            this.groupBoxNewFANJobInfo.Controls.Add(this.textBoxNewFANOrderNum);
            this.groupBoxNewFANJobInfo.Controls.Add(this.labelNewFANCustomer);
            this.groupBoxNewFANJobInfo.Controls.Add(this.labelNewFANOrderNum);
            this.groupBoxNewFANJobInfo.Controls.Add(this.textBoxNewFANLineItem);
            this.groupBoxNewFANJobInfo.Controls.Add(this.labelNewFANLineItem);
            this.groupBoxNewFANJobInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewFANJobInfo.Location = new System.Drawing.Point(12, 37);
            this.groupBoxNewFANJobInfo.Name = "groupBoxNewFANJobInfo";
            this.groupBoxNewFANJobInfo.Size = new System.Drawing.Size(311, 134);
            this.groupBoxNewFANJobInfo.TabIndex = 1;
            this.groupBoxNewFANJobInfo.TabStop = false;
            this.groupBoxNewFANJobInfo.Text = "Order Info";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 21);
            this.label1.TabIndex = 27;
            this.label1.Text = "Job #:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxNewFANJobNum
            // 
            this.textBoxNewFANJobNum.BackColor = System.Drawing.Color.White;
            this.textBoxNewFANJobNum.Enabled = false;
            this.textBoxNewFANJobNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFANJobNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFANJobNum.Location = new System.Drawing.Point(95, 21);
            this.textBoxNewFANJobNum.Name = "textBoxNewFANJobNum";
            this.textBoxNewFANJobNum.Size = new System.Drawing.Size(88, 26);
            this.textBoxNewFANJobNum.TabIndex = 26;
            this.textBoxNewFANJobNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxNewFANEndConsumer
            // 
            this.textBoxNewFANEndConsumer.BackColor = System.Drawing.Color.White;
            this.textBoxNewFANEndConsumer.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFANEndConsumer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFANEndConsumer.Location = new System.Drawing.Point(110, 104);
            this.textBoxNewFANEndConsumer.Name = "textBoxNewFANEndConsumer";
            this.textBoxNewFANEndConsumer.Size = new System.Drawing.Size(195, 26);
            this.textBoxNewFANEndConsumer.TabIndex = 8;
            // 
            // labelNewFANEndConsumer
            // 
            this.labelNewFANEndConsumer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANEndConsumer.Location = new System.Drawing.Point(4, 104);
            this.labelNewFANEndConsumer.Name = "labelNewFANEndConsumer";
            this.labelNewFANEndConsumer.Size = new System.Drawing.Size(104, 21);
            this.labelNewFANEndConsumer.TabIndex = 7;
            this.labelNewFANEndConsumer.Text = "End Consumer:";
            this.labelNewFANEndConsumer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewFANReleaseNum
            // 
            this.textBoxNewFANReleaseNum.BackColor = System.Drawing.Color.White;
            this.textBoxNewFANReleaseNum.Enabled = false;
            this.textBoxNewFANReleaseNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFANReleaseNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFANReleaseNum.Location = new System.Drawing.Point(284, 50);
            this.textBoxNewFANReleaseNum.Name = "textBoxNewFANReleaseNum";
            this.textBoxNewFANReleaseNum.Size = new System.Drawing.Size(21, 26);
            this.textBoxNewFANReleaseNum.TabIndex = 4;
            this.textBoxNewFANReleaseNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFANSlash
            // 
            this.labelNewFANSlash.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANSlash.Location = new System.Drawing.Point(270, 49);
            this.labelNewFANSlash.Name = "labelNewFANSlash";
            this.labelNewFANSlash.Size = new System.Drawing.Size(15, 21);
            this.labelNewFANSlash.TabIndex = 4;
            this.labelNewFANSlash.Text = "/";
            this.labelNewFANSlash.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxNewFANCustomer
            // 
            this.textBoxNewFANCustomer.BackColor = System.Drawing.Color.White;
            this.textBoxNewFANCustomer.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFANCustomer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFANCustomer.Location = new System.Drawing.Point(95, 77);
            this.textBoxNewFANCustomer.Name = "textBoxNewFANCustomer";
            this.textBoxNewFANCustomer.Size = new System.Drawing.Size(210, 26);
            this.textBoxNewFANCustomer.TabIndex = 2;
            // 
            // textBoxNewFANOrderNum
            // 
            this.textBoxNewFANOrderNum.BackColor = System.Drawing.Color.White;
            this.textBoxNewFANOrderNum.Enabled = false;
            this.textBoxNewFANOrderNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFANOrderNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFANOrderNum.Location = new System.Drawing.Point(95, 50);
            this.textBoxNewFANOrderNum.Name = "textBoxNewFANOrderNum";
            this.textBoxNewFANOrderNum.Size = new System.Drawing.Size(50, 26);
            this.textBoxNewFANOrderNum.TabIndex = 1;
            this.textBoxNewFANOrderNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelNewFANCustomer
            // 
            this.labelNewFANCustomer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANCustomer.Location = new System.Drawing.Point(4, 77);
            this.labelNewFANCustomer.Name = "labelNewFANCustomer";
            this.labelNewFANCustomer.Size = new System.Drawing.Size(85, 21);
            this.labelNewFANCustomer.TabIndex = 2;
            this.labelNewFANCustomer.Text = "Customer:";
            this.labelNewFANCustomer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelNewFANOrderNum
            // 
            this.labelNewFANOrderNum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANOrderNum.Location = new System.Drawing.Point(4, 51);
            this.labelNewFANOrderNum.Name = "labelNewFANOrderNum";
            this.labelNewFANOrderNum.Size = new System.Drawing.Size(85, 21);
            this.labelNewFANOrderNum.TabIndex = 1;
            this.labelNewFANOrderNum.Text = "Sales Order#:";
            this.labelNewFANOrderNum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxNewFANLineItem
            // 
            this.textBoxNewFANLineItem.BackColor = System.Drawing.Color.White;
            this.textBoxNewFANLineItem.Enabled = false;
            this.textBoxNewFANLineItem.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFANLineItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFANLineItem.Location = new System.Drawing.Point(246, 50);
            this.textBoxNewFANLineItem.Name = "textBoxNewFANLineItem";
            this.textBoxNewFANLineItem.Size = new System.Drawing.Size(21, 26);
            this.textBoxNewFANLineItem.TabIndex = 3;
            this.textBoxNewFANLineItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFANLineItem
            // 
            this.labelNewFANLineItem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANLineItem.Location = new System.Drawing.Point(156, 50);
            this.labelNewFANLineItem.Name = "labelNewFANLineItem";
            this.labelNewFANLineItem.Size = new System.Drawing.Size(85, 21);
            this.labelNewFANLineItem.TabIndex = 2;
            this.labelNewFANLineItem.Text = "Line #/Rel #:";
            this.labelNewFANLineItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // menuStripNewFAN
            // 
            this.menuStripNewFAN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.menuStripNewFAN.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStripNewFAN.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStripNewFAN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStripNewFAN.Location = new System.Drawing.Point(0, 0);
            this.menuStripNewFAN.Name = "menuStripNewFAN";
            this.menuStripNewFAN.Size = new System.Drawing.Size(471, 29);
            this.menuStripNewFAN.TabIndex = 2;
            this.menuStripNewFAN.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(52, 25);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(112, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(329, 46);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(141, 20);
            this.checkBox1.TabIndex = 20;
            this.checkBox1.Text = "French Canadian Order";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // groupBoxNewFANUnitInfo
            // 
            this.groupBoxNewFANUnitInfo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBoxNewFANUnitInfo.Controls.Add(this.label2);
            this.groupBoxNewFANUnitInfo.Controls.Add(this.txtTag);
            this.groupBoxNewFANUnitInfo.Controls.Add(this.txtTagNum);
            this.groupBoxNewFANUnitInfo.Controls.Add(this.labelNewFANPartNum);
            this.groupBoxNewFANUnitInfo.Controls.Add(this.textBoxNewFANPartNum);
            this.groupBoxNewFANUnitInfo.Controls.Add(this.checkBoxNewFANVerified);
            this.groupBoxNewFANUnitInfo.Controls.Add(this.textBoxNewFANVerifiedBy);
            this.groupBoxNewFANUnitInfo.Controls.Add(this.labelMewFANVerifiedBy);
            this.groupBoxNewFANUnitInfo.Controls.Add(this.labelNewFANTag);
            this.groupBoxNewFANUnitInfo.Controls.Add(this.labelNewFANSerialNo);
            this.groupBoxNewFANUnitInfo.Controls.Add(this.textBoxNewFANSerialNo);
            this.groupBoxNewFANUnitInfo.Controls.Add(this.textBoxNewFANModelNo);
            this.groupBoxNewFANUnitInfo.Controls.Add(this.labelNewFANModelNum);
            this.groupBoxNewFANUnitInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewFANUnitInfo.Location = new System.Drawing.Point(12, 177);
            this.groupBoxNewFANUnitInfo.Name = "groupBoxNewFANUnitInfo";
            this.groupBoxNewFANUnitInfo.Size = new System.Drawing.Size(311, 162);
            this.groupBoxNewFANUnitInfo.TabIndex = 2;
            this.groupBoxNewFANUnitInfo.TabStop = false;
            this.groupBoxNewFANUnitInfo.Text = "Unit Info";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 21);
            this.label2.TabIndex = 68;
            this.label2.Text = "Tag:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTag
            // 
            this.txtTag.BackColor = System.Drawing.Color.White;
            this.txtTag.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTag.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtTag.Location = new System.Drawing.Point(75, 129);
            this.txtTag.Name = "txtTag";
            this.txtTag.Size = new System.Drawing.Size(215, 26);
            this.txtTag.TabIndex = 67;
            this.txtTag.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtTagNum
            // 
            this.txtTagNum.BackColor = System.Drawing.Color.White;
            this.txtTagNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTagNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtTagNum.Location = new System.Drawing.Point(121, 100);
            this.txtTagNum.Name = "txtTagNum";
            this.txtTagNum.Size = new System.Drawing.Size(79, 26);
            this.txtTagNum.TabIndex = 66;
            this.txtTagNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelNewFANPartNum
            // 
            this.labelNewFANPartNum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANPartNum.Location = new System.Drawing.Point(7, 46);
            this.labelNewFANPartNum.Name = "labelNewFANPartNum";
            this.labelNewFANPartNum.Size = new System.Drawing.Size(65, 21);
            this.labelNewFANPartNum.TabIndex = 65;
            this.labelNewFANPartNum.Text = "Part Num:";
            this.labelNewFANPartNum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewFANPartNum
            // 
            this.textBoxNewFANPartNum.BackColor = System.Drawing.Color.White;
            this.textBoxNewFANPartNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFANPartNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFANPartNum.Location = new System.Drawing.Point(76, 46);
            this.textBoxNewFANPartNum.Name = "textBoxNewFANPartNum";
            this.textBoxNewFANPartNum.Size = new System.Drawing.Size(124, 26);
            this.textBoxNewFANPartNum.TabIndex = 2;
            this.textBoxNewFANPartNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBoxNewFANVerified
            // 
            this.checkBoxNewFANVerified.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBoxNewFANVerified.Location = new System.Drawing.Point(206, 44);
            this.checkBoxNewFANVerified.Name = "checkBoxNewFANVerified";
            this.checkBoxNewFANVerified.Size = new System.Drawing.Size(95, 32);
            this.checkBoxNewFANVerified.TabIndex = 62;
            this.checkBoxNewFANVerified.Text = "Model No \r\nVerified";
            this.checkBoxNewFANVerified.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxNewFANVerified.UseVisualStyleBackColor = true;
            this.checkBoxNewFANVerified.Visible = false;
            // 
            // textBoxNewFANVerifiedBy
            // 
            this.textBoxNewFANVerifiedBy.BackColor = System.Drawing.Color.White;
            this.textBoxNewFANVerifiedBy.Enabled = false;
            this.textBoxNewFANVerifiedBy.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFANVerifiedBy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFANVerifiedBy.Location = new System.Drawing.Point(223, 101);
            this.textBoxNewFANVerifiedBy.Name = "textBoxNewFANVerifiedBy";
            this.textBoxNewFANVerifiedBy.Size = new System.Drawing.Size(67, 26);
            this.textBoxNewFANVerifiedBy.TabIndex = 63;
            this.textBoxNewFANVerifiedBy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxNewFANVerifiedBy.Visible = false;
            // 
            // labelMewFANVerifiedBy
            // 
            this.labelMewFANVerifiedBy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMewFANVerifiedBy.Location = new System.Drawing.Point(221, 76);
            this.labelMewFANVerifiedBy.Name = "labelMewFANVerifiedBy";
            this.labelMewFANVerifiedBy.Size = new System.Drawing.Size(70, 20);
            this.labelMewFANVerifiedBy.TabIndex = 12;
            this.labelMewFANVerifiedBy.Text = "Verified By:";
            this.labelMewFANVerifiedBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelMewFANVerifiedBy.Visible = false;
            // 
            // labelNewFANTag
            // 
            this.labelNewFANTag.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANTag.Location = new System.Drawing.Point(7, 101);
            this.labelNewFANTag.Name = "labelNewFANTag";
            this.labelNewFANTag.Size = new System.Drawing.Size(108, 21);
            this.labelNewFANTag.TabIndex = 8;
            this.labelNewFANTag.Text = "Tag Num (eg EF1):";
            this.labelNewFANTag.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelNewFANSerialNo
            // 
            this.labelNewFANSerialNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANSerialNo.Location = new System.Drawing.Point(6, 72);
            this.labelNewFANSerialNo.Name = "labelNewFANSerialNo";
            this.labelNewFANSerialNo.Size = new System.Drawing.Size(64, 21);
            this.labelNewFANSerialNo.TabIndex = 7;
            this.labelNewFANSerialNo.Text = "Serial No:";
            this.labelNewFANSerialNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewFANSerialNo
            // 
            this.textBoxNewFANSerialNo.BackColor = System.Drawing.Color.White;
            this.textBoxNewFANSerialNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFANSerialNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFANSerialNo.Location = new System.Drawing.Point(76, 72);
            this.textBoxNewFANSerialNo.Name = "textBoxNewFANSerialNo";
            this.textBoxNewFANSerialNo.Size = new System.Drawing.Size(124, 26);
            this.textBoxNewFANSerialNo.TabIndex = 3;
            this.textBoxNewFANSerialNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxNewFANModelNo
            // 
            this.textBoxNewFANModelNo.BackColor = System.Drawing.Color.White;
            this.textBoxNewFANModelNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFANModelNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFANModelNo.Location = new System.Drawing.Point(76, 19);
            this.textBoxNewFANModelNo.Name = "textBoxNewFANModelNo";
            this.textBoxNewFANModelNo.Size = new System.Drawing.Size(227, 26);
            this.textBoxNewFANModelNo.TabIndex = 1;
            this.textBoxNewFANModelNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelNewFANModelNum
            // 
            this.labelNewFANModelNum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANModelNum.Location = new System.Drawing.Point(7, 19);
            this.labelNewFANModelNum.Name = "labelNewFANModelNum";
            this.labelNewFANModelNum.Size = new System.Drawing.Size(65, 21);
            this.labelNewFANModelNum.TabIndex = 4;
            this.labelNewFANModelNum.Text = "Model No:";
            this.labelNewFANModelNum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxNewFANVoltageInfo
            // 
            this.groupBoxNewFANVoltageInfo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.textBoxNewFANAirRPM);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.labelNewFANAirRPM);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.textBoxNewFANAirESP);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.labelNewFANAirESP);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.textBoxNewFANAirCFM);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.labelNewFANAirCFM);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.textBoxNewFANElecFLA);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.labelNewFANElecFLA);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.textBoxNewFANElecMaxVoltage);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.labelNewFANElecMaxVoltage);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.textBoxNewFANElecMaxVoltageHertz);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.labelNewFANElecMaxVoltageHertz);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.textBoxNewFANElecMinVoltage);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.labelNewFANElecMinVoltage);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.textBoxNewFANElecHertz);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.labelNewFANElecHertz);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.textBoxNewFANElecPhase);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.labelNewFANElecPhase);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.textBoxNewFANElecVolts);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.labelNewFANElecVolts);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.textBoxNewFANElecBlowerFan);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.labelNewFANElecBlowerFan);
            this.groupBoxNewFANVoltageInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewFANVoltageInfo.Location = new System.Drawing.Point(12, 345);
            this.groupBoxNewFANVoltageInfo.Name = "groupBoxNewFANVoltageInfo";
            this.groupBoxNewFANVoltageInfo.Size = new System.Drawing.Size(447, 240);
            this.groupBoxNewFANVoltageInfo.TabIndex = 3;
            this.groupBoxNewFANVoltageInfo.TabStop = false;
            this.groupBoxNewFANVoltageInfo.Text = "FAN";
            // 
            // textBoxNewFANAirRPM
            // 
            this.textBoxNewFANAirRPM.BackColor = System.Drawing.Color.White;
            this.textBoxNewFANAirRPM.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFANAirRPM.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFANAirRPM.Location = new System.Drawing.Point(352, 77);
            this.textBoxNewFANAirRPM.Name = "textBoxNewFANAirRPM";
            this.textBoxNewFANAirRPM.Size = new System.Drawing.Size(75, 26);
            this.textBoxNewFANAirRPM.TabIndex = 11;
            this.textBoxNewFANAirRPM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFANAirRPM
            // 
            this.labelNewFANAirRPM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANAirRPM.Location = new System.Drawing.Point(288, 77);
            this.labelNewFANAirRPM.Name = "labelNewFANAirRPM";
            this.labelNewFANAirRPM.Size = new System.Drawing.Size(61, 21);
            this.labelNewFANAirRPM.TabIndex = 60;
            this.labelNewFANAirRPM.Text = "Air RPM:";
            this.labelNewFANAirRPM.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewFANAirESP
            // 
            this.textBoxNewFANAirESP.BackColor = System.Drawing.Color.White;
            this.textBoxNewFANAirESP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFANAirESP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFANAirESP.Location = new System.Drawing.Point(352, 49);
            this.textBoxNewFANAirESP.Name = "textBoxNewFANAirESP";
            this.textBoxNewFANAirESP.Size = new System.Drawing.Size(75, 26);
            this.textBoxNewFANAirESP.TabIndex = 10;
            this.textBoxNewFANAirESP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFANAirESP
            // 
            this.labelNewFANAirESP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANAirESP.Location = new System.Drawing.Point(288, 49);
            this.labelNewFANAirESP.Name = "labelNewFANAirESP";
            this.labelNewFANAirESP.Size = new System.Drawing.Size(61, 21);
            this.labelNewFANAirESP.TabIndex = 58;
            this.labelNewFANAirESP.Text = "Air ESP:";
            this.labelNewFANAirESP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewFANAirCFM
            // 
            this.textBoxNewFANAirCFM.BackColor = System.Drawing.Color.White;
            this.textBoxNewFANAirCFM.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFANAirCFM.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFANAirCFM.Location = new System.Drawing.Point(352, 21);
            this.textBoxNewFANAirCFM.Name = "textBoxNewFANAirCFM";
            this.textBoxNewFANAirCFM.Size = new System.Drawing.Size(75, 26);
            this.textBoxNewFANAirCFM.TabIndex = 9;
            this.textBoxNewFANAirCFM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFANAirCFM
            // 
            this.labelNewFANAirCFM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANAirCFM.Location = new System.Drawing.Point(288, 21);
            this.labelNewFANAirCFM.Name = "labelNewFANAirCFM";
            this.labelNewFANAirCFM.Size = new System.Drawing.Size(61, 21);
            this.labelNewFANAirCFM.TabIndex = 56;
            this.labelNewFANAirCFM.Text = "Air CFM:";
            this.labelNewFANAirCFM.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewFANElecFLA
            // 
            this.textBoxNewFANElecFLA.BackColor = System.Drawing.Color.White;
            this.textBoxNewFANElecFLA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFANElecFLA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFANElecFLA.Location = new System.Drawing.Point(170, 127);
            this.textBoxNewFANElecFLA.Name = "textBoxNewFANElecFLA";
            this.textBoxNewFANElecFLA.Size = new System.Drawing.Size(90, 26);
            this.textBoxNewFANElecFLA.TabIndex = 5;
            this.textBoxNewFANElecFLA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFANElecFLA
            // 
            this.labelNewFANElecFLA.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANElecFLA.Location = new System.Drawing.Point(9, 127);
            this.labelNewFANElecFLA.Name = "labelNewFANElecFLA";
            this.labelNewFANElecFLA.Size = new System.Drawing.Size(158, 21);
            this.labelNewFANElecFLA.TabIndex = 54;
            this.labelNewFANElecFLA.Text = "Electric FLA:";
            this.labelNewFANElecFLA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewFANElecMaxVoltage
            // 
            this.textBoxNewFANElecMaxVoltage.BackColor = System.Drawing.Color.White;
            this.textBoxNewFANElecMaxVoltage.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFANElecMaxVoltage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFANElecMaxVoltage.Location = new System.Drawing.Point(170, 181);
            this.textBoxNewFANElecMaxVoltage.Name = "textBoxNewFANElecMaxVoltage";
            this.textBoxNewFANElecMaxVoltage.Size = new System.Drawing.Size(90, 26);
            this.textBoxNewFANElecMaxVoltage.TabIndex = 7;
            this.textBoxNewFANElecMaxVoltage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFANElecMaxVoltage
            // 
            this.labelNewFANElecMaxVoltage.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANElecMaxVoltage.Location = new System.Drawing.Point(9, 181);
            this.labelNewFANElecMaxVoltage.Name = "labelNewFANElecMaxVoltage";
            this.labelNewFANElecMaxVoltage.Size = new System.Drawing.Size(158, 21);
            this.labelNewFANElecMaxVoltage.TabIndex = 50;
            this.labelNewFANElecMaxVoltage.Text = "Electric Max Voltage:";
            this.labelNewFANElecMaxVoltage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewFANElecMaxVoltageHertz
            // 
            this.textBoxNewFANElecMaxVoltageHertz.BackColor = System.Drawing.Color.White;
            this.textBoxNewFANElecMaxVoltageHertz.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFANElecMaxVoltageHertz.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFANElecMaxVoltageHertz.Location = new System.Drawing.Point(170, 208);
            this.textBoxNewFANElecMaxVoltageHertz.Name = "textBoxNewFANElecMaxVoltageHertz";
            this.textBoxNewFANElecMaxVoltageHertz.Size = new System.Drawing.Size(90, 26);
            this.textBoxNewFANElecMaxVoltageHertz.TabIndex = 8;
            this.textBoxNewFANElecMaxVoltageHertz.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFANElecMaxVoltageHertz
            // 
            this.labelNewFANElecMaxVoltageHertz.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANElecMaxVoltageHertz.Location = new System.Drawing.Point(9, 208);
            this.labelNewFANElecMaxVoltageHertz.Name = "labelNewFANElecMaxVoltageHertz";
            this.labelNewFANElecMaxVoltageHertz.Size = new System.Drawing.Size(158, 21);
            this.labelNewFANElecMaxVoltageHertz.TabIndex = 48;
            this.labelNewFANElecMaxVoltageHertz.Text = "Electric Max Voltage Hertz:";
            this.labelNewFANElecMaxVoltageHertz.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewFANElecMinVoltage
            // 
            this.textBoxNewFANElecMinVoltage.BackColor = System.Drawing.Color.White;
            this.textBoxNewFANElecMinVoltage.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFANElecMinVoltage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFANElecMinVoltage.Location = new System.Drawing.Point(170, 154);
            this.textBoxNewFANElecMinVoltage.Name = "textBoxNewFANElecMinVoltage";
            this.textBoxNewFANElecMinVoltage.Size = new System.Drawing.Size(90, 26);
            this.textBoxNewFANElecMinVoltage.TabIndex = 6;
            this.textBoxNewFANElecMinVoltage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFANElecMinVoltage
            // 
            this.labelNewFANElecMinVoltage.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANElecMinVoltage.Location = new System.Drawing.Point(9, 154);
            this.labelNewFANElecMinVoltage.Name = "labelNewFANElecMinVoltage";
            this.labelNewFANElecMinVoltage.Size = new System.Drawing.Size(158, 21);
            this.labelNewFANElecMinVoltage.TabIndex = 46;
            this.labelNewFANElecMinVoltage.Text = "Electric Min Voltage:";
            this.labelNewFANElecMinVoltage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewFANElecHertz
            // 
            this.textBoxNewFANElecHertz.BackColor = System.Drawing.Color.White;
            this.textBoxNewFANElecHertz.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFANElecHertz.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFANElecHertz.Location = new System.Drawing.Point(170, 100);
            this.textBoxNewFANElecHertz.Name = "textBoxNewFANElecHertz";
            this.textBoxNewFANElecHertz.Size = new System.Drawing.Size(90, 26);
            this.textBoxNewFANElecHertz.TabIndex = 4;
            this.textBoxNewFANElecHertz.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFANElecHertz
            // 
            this.labelNewFANElecHertz.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANElecHertz.Location = new System.Drawing.Point(6, 100);
            this.labelNewFANElecHertz.Name = "labelNewFANElecHertz";
            this.labelNewFANElecHertz.Size = new System.Drawing.Size(158, 21);
            this.labelNewFANElecHertz.TabIndex = 44;
            this.labelNewFANElecHertz.Text = "Electric Hertz:";
            this.labelNewFANElecHertz.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewFANElecPhase
            // 
            this.textBoxNewFANElecPhase.BackColor = System.Drawing.Color.White;
            this.textBoxNewFANElecPhase.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFANElecPhase.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFANElecPhase.Location = new System.Drawing.Point(170, 73);
            this.textBoxNewFANElecPhase.Name = "textBoxNewFANElecPhase";
            this.textBoxNewFANElecPhase.Size = new System.Drawing.Size(90, 26);
            this.textBoxNewFANElecPhase.TabIndex = 3;
            this.textBoxNewFANElecPhase.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFANElecPhase
            // 
            this.labelNewFANElecPhase.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANElecPhase.Location = new System.Drawing.Point(9, 73);
            this.labelNewFANElecPhase.Name = "labelNewFANElecPhase";
            this.labelNewFANElecPhase.Size = new System.Drawing.Size(158, 21);
            this.labelNewFANElecPhase.TabIndex = 42;
            this.labelNewFANElecPhase.Text = "Electric Phase:";
            this.labelNewFANElecPhase.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewFANElecVolts
            // 
            this.textBoxNewFANElecVolts.BackColor = System.Drawing.Color.White;
            this.textBoxNewFANElecVolts.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFANElecVolts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFANElecVolts.Location = new System.Drawing.Point(170, 46);
            this.textBoxNewFANElecVolts.Name = "textBoxNewFANElecVolts";
            this.textBoxNewFANElecVolts.Size = new System.Drawing.Size(90, 26);
            this.textBoxNewFANElecVolts.TabIndex = 2;
            this.textBoxNewFANElecVolts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFANElecVolts
            // 
            this.labelNewFANElecVolts.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANElecVolts.Location = new System.Drawing.Point(9, 46);
            this.labelNewFANElecVolts.Name = "labelNewFANElecVolts";
            this.labelNewFANElecVolts.Size = new System.Drawing.Size(158, 21);
            this.labelNewFANElecVolts.TabIndex = 40;
            this.labelNewFANElecVolts.Text = "Electric Volts:";
            this.labelNewFANElecVolts.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewFANElecBlowerFan
            // 
            this.textBoxNewFANElecBlowerFan.BackColor = System.Drawing.Color.White;
            this.textBoxNewFANElecBlowerFan.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFANElecBlowerFan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFANElecBlowerFan.Location = new System.Drawing.Point(170, 19);
            this.textBoxNewFANElecBlowerFan.Name = "textBoxNewFANElecBlowerFan";
            this.textBoxNewFANElecBlowerFan.Size = new System.Drawing.Size(90, 26);
            this.textBoxNewFANElecBlowerFan.TabIndex = 1;
            this.textBoxNewFANElecBlowerFan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFANElecBlowerFan
            // 
            this.labelNewFANElecBlowerFan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANElecBlowerFan.Location = new System.Drawing.Point(9, 19);
            this.labelNewFANElecBlowerFan.Name = "labelNewFANElecBlowerFan";
            this.labelNewFANElecBlowerFan.Size = new System.Drawing.Size(158, 21);
            this.labelNewFANElecBlowerFan.TabIndex = 38;
            this.labelNewFANElecBlowerFan.Text = "Electric Blower Fan:";
            this.labelNewFANElecBlowerFan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonNewFANPrint
            // 
            this.buttonNewFANPrint.Enabled = false;
            this.buttonNewFANPrint.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewFANPrint.Location = new System.Drawing.Point(351, 199);
            this.buttonNewFANPrint.Name = "buttonNewFANPrint";
            this.buttonNewFANPrint.Size = new System.Drawing.Size(95, 37);
            this.buttonNewFANPrint.TabIndex = 23;
            this.buttonNewFANPrint.Text = "Print";
            this.buttonNewFANPrint.UseVisualStyleBackColor = true;
            this.buttonNewFANPrint.Click += new System.EventHandler(this.buttonNewFANPrint_Click);
            // 
            // buttonNewFANAccept
            // 
            this.buttonNewFANAccept.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewFANAccept.Location = new System.Drawing.Point(351, 151);
            this.buttonNewFANAccept.Name = "buttonNewFANAccept";
            this.buttonNewFANAccept.Size = new System.Drawing.Size(95, 37);
            this.buttonNewFANAccept.TabIndex = 22;
            this.buttonNewFANAccept.Text = "Accept";
            this.buttonNewFANAccept.UseVisualStyleBackColor = true;
            this.buttonNewFANAccept.Click += new System.EventHandler(this.buttonNewOAUAccept_Click);
            // 
            // buttonNewFANCancel
            // 
            this.buttonNewFANCancel.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewFANCancel.ForeColor = System.Drawing.Color.Red;
            this.buttonNewFANCancel.Location = new System.Drawing.Point(351, 102);
            this.buttonNewFANCancel.Name = "buttonNewFANCancel";
            this.buttonNewFANCancel.Size = new System.Drawing.Size(95, 37);
            this.buttonNewFANCancel.TabIndex = 21;
            this.buttonNewFANCancel.Text = "Cancel";
            this.buttonNewFANCancel.UseVisualStyleBackColor = true;
            this.buttonNewFANCancel.Click += new System.EventHandler(this.buttonNewOAUCancel_Click);
            // 
            // labelNewFANMsg
            // 
            this.labelNewFANMsg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.labelNewFANMsg.Location = new System.Drawing.Point(9, 588);
            this.labelNewFANMsg.Name = "labelNewFANMsg";
            this.labelNewFANMsg.Size = new System.Drawing.Size(447, 21);
            this.labelNewFANMsg.TabIndex = 24;
            this.labelNewFANMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelNewFANMsg2
            // 
            this.labelNewFANMsg2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.labelNewFANMsg2.Location = new System.Drawing.Point(9, 609);
            this.labelNewFANMsg2.Name = "labelNewFANMsg2";
            this.labelNewFANMsg2.Size = new System.Drawing.Size(447, 21);
            this.labelNewFANMsg2.TabIndex = 25;
            this.labelNewFANMsg2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmNewLineFAN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(471, 634);
            this.Controls.Add(this.labelNewFANMsg2);
            this.Controls.Add(this.labelNewFANMsg);
            this.Controls.Add(this.buttonNewFANPrint);
            this.Controls.Add(this.buttonNewFANAccept);
            this.Controls.Add(this.buttonNewFANCancel);
            this.Controls.Add(this.groupBoxNewFANVoltageInfo);
            this.Controls.Add(this.groupBoxNewFANUnitInfo);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.groupBoxNewFANJobInfo);
            this.Controls.Add(this.menuStripNewFAN);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Blue;
            this.MainMenuStrip = this.menuStripNewFAN;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmNewLineFAN";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New Line FAN";
            this.groupBoxNewFANJobInfo.ResumeLayout(false);
            this.groupBoxNewFANJobInfo.PerformLayout();
            this.menuStripNewFAN.ResumeLayout(false);
            this.menuStripNewFAN.PerformLayout();
            this.groupBoxNewFANUnitInfo.ResumeLayout(false);
            this.groupBoxNewFANUnitInfo.PerformLayout();
            this.groupBoxNewFANVoltageInfo.ResumeLayout(false);
            this.groupBoxNewFANVoltageInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxNewFANJobInfo;
        public System.Windows.Forms.TextBox textBoxNewFANReleaseNum;
        private System.Windows.Forms.Label labelNewFANSlash;
        public System.Windows.Forms.TextBox textBoxNewFANCustomer;
        public System.Windows.Forms.TextBox textBoxNewFANOrderNum;
        private System.Windows.Forms.Label labelNewFANCustomer;
        private System.Windows.Forms.Label labelNewFANOrderNum;
        public System.Windows.Forms.TextBox textBoxNewFANLineItem;
        private System.Windows.Forms.Label labelNewFANLineItem;
        private System.Windows.Forms.MenuStrip menuStripNewFAN;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.CheckBox checkBox1;
        public System.Windows.Forms.GroupBox groupBoxNewFANUnitInfo;
        public System.Windows.Forms.CheckBox checkBoxNewFANVerified;
        public System.Windows.Forms.TextBox textBoxNewFANVerifiedBy;
        private System.Windows.Forms.Label labelMewFANVerifiedBy;
        private System.Windows.Forms.Label labelNewFANTag;
        private System.Windows.Forms.Label labelNewFANSerialNo;
        public System.Windows.Forms.TextBox textBoxNewFANSerialNo;
        public System.Windows.Forms.TextBox textBoxNewFANModelNo;
        private System.Windows.Forms.Label labelNewFANModelNum;
        public System.Windows.Forms.GroupBox groupBoxNewFANVoltageInfo;
        public System.Windows.Forms.TextBox textBoxNewFANElecFLA;
        private System.Windows.Forms.Label labelNewFANElecFLA;
        public System.Windows.Forms.TextBox textBoxNewFANElecMaxVoltage;
        private System.Windows.Forms.Label labelNewFANElecMaxVoltage;
        public System.Windows.Forms.TextBox textBoxNewFANElecMaxVoltageHertz;
        private System.Windows.Forms.Label labelNewFANElecMaxVoltageHertz;
        public System.Windows.Forms.TextBox textBoxNewFANElecMinVoltage;
        private System.Windows.Forms.Label labelNewFANElecMinVoltage;
        public System.Windows.Forms.TextBox textBoxNewFANElecHertz;
        private System.Windows.Forms.Label labelNewFANElecHertz;
        public System.Windows.Forms.TextBox textBoxNewFANElecPhase;
        private System.Windows.Forms.Label labelNewFANElecPhase;
        public System.Windows.Forms.TextBox textBoxNewFANElecVolts;
        private System.Windows.Forms.Label labelNewFANElecVolts;
        public System.Windows.Forms.TextBox textBoxNewFANElecBlowerFan;
        private System.Windows.Forms.Label labelNewFANElecBlowerFan;
        public System.Windows.Forms.TextBox textBoxNewFANAirRPM;
        private System.Windows.Forms.Label labelNewFANAirRPM;
        public System.Windows.Forms.TextBox textBoxNewFANAirESP;
        private System.Windows.Forms.Label labelNewFANAirESP;
        public System.Windows.Forms.TextBox textBoxNewFANAirCFM;
        private System.Windows.Forms.Label labelNewFANAirCFM;
        public System.Windows.Forms.Button buttonNewFANPrint;
        public System.Windows.Forms.Button buttonNewFANAccept;
        public System.Windows.Forms.Button buttonNewFANCancel;
        private System.Windows.Forms.Label labelNewFANPartNum;
        public System.Windows.Forms.TextBox textBoxNewFANPartNum;
        public System.Windows.Forms.TextBox txtTagNum;
        public System.Windows.Forms.Label labelNewFANMsg;
        public System.Windows.Forms.Label labelNewFANMsg2;
        public System.Windows.Forms.TextBox textBoxNewFANEndConsumer;
        private System.Windows.Forms.Label labelNewFANEndConsumer;
        public System.Windows.Forms.TextBox textBoxNewFANJobNum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtTag;
    }
}