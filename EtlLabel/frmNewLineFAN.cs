using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;

namespace KCC.OA.Etl
{
    public partial class frmNewLineFAN : Form
    {
        public frmNewLineFAN()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonNewOAUCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonNewOAUAccept_Click(object sender, EventArgs e)
        {

            string tagNum = this.txtTagNum.Text.ToString();

            if (tagNum.Length > 4)
            {
                MessageBox.Show("TagNum field cannot be more than four characters.");
                return;
            }
            
            if (GlobalModeType.ModeType == "New")
            {
                addNewLineOrderDtlFAN();
            }
            else
            {
                updateOrderDtlFAN();
            }
        }

        private void updateOrderDtlFAN()
        {
            string jobNumStr;
            int orderNumInt;
            int orderLineInt;
            int releaseNumInt;

            if (this.textBoxNewFANSerialNo.Text == "")
            {
                MessageBox.Show("ERROR - Serial No is a required field.");
                this.textBoxNewFANSerialNo.Focus();
                this.textBoxNewFANSerialNo.Select();
                return;
            }

            //VS_DataSetTableAdapters.OrderDtlFANTableAdapter ordDtlFANTA =
            //                        new VS_DataSetTableAdapters.OrderDtlFANTableAdapter();

            VS_DataSetTableAdapters.EtlFANTableAdapter etlFANTA =
                new VS_DataSetTableAdapters.EtlFANTableAdapter();

            //ordDtlFANTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

            etlFANTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

            jobNumStr = this.textBoxNewFANJobNum.Text;
            orderNumInt = Int32.Parse(this.textBoxNewFANOrderNum.Text);
            orderLineInt = Int32.Parse(this.textBoxNewFANLineItem.Text);
            releaseNumInt = Int32.Parse(this.textBoxNewFANReleaseNum.Text);

            try
            {
                etlFANTA.UpdateByJobNum(
                    jobNumStr,
                    orderLineInt,
                    orderLineInt,
                    releaseNumInt,
                    this.textBoxNewFANPartNum.Text,   // Model No is always the Part Number so whenever the part num is changed the Model num is too.                                
                    this.textBoxNewFANPartNum.Text,
                    this.textBoxNewFANSerialNo.Text,
                    this.txtTag.Text,
                    this.textBoxNewFANElecBlowerFan.Text,
                    this.textBoxNewFANElecVolts.Text,
                    this.textBoxNewFANElecPhase.Text,
                    this.textBoxNewFANElecHertz.Text,
                    this.textBoxNewFANElecFLA.Text,
                    this.textBoxNewFANElecMinVoltage.Text,
                    this.textBoxNewFANElecMaxVoltage.Text,
                    this.textBoxNewFANElecMaxVoltageHertz.Text,
                    this.textBoxNewFANAirCFM.Text,
                    this.textBoxNewFANAirESP.Text,
                    this.textBoxNewFANAirRPM.Text,
                    this.txtTagNum.Text);

                MessageBox.Show("Job # " + jobNumStr + " Updated successfully.");

                this.buttonNewFANAccept.Text = "Update";
                GlobalModeType.ModeType = "Update";
                this.buttonNewFANPrint.Enabled = true;
                this.labelNewFANMsg.Text = jobNumStr +
                    " has been successfully updated in the EtlFAN table. " +
                    "Use the Update button for any additional modifications";
                this.textBoxNewFANModelNo.Text = this.textBoxNewFANPartNum.Text;
                this.Refresh();
            }
            catch
            {
                MessageBox.Show("ERROR - Updatinging data into EtlFAN");
            }
            
        }        

        private void addNewLineOrderDtlFAN()
        {
            string jobNumStr;
            int orderNumInt;
            int orderLineInt;
            int releaseNumInt;

            string modelNoVerifiedByStr = "";
            string customerStr = "";
            string endConsumerNameStr = "";

            VS_DataSet dsEtl = new VS_DataSet();

            jobNumStr = this.textBoxNewFANJobNum.Text;
            orderNumInt = Int32.Parse(this.textBoxNewFANOrderNum.Text);
            orderLineInt = Int32.Parse(this.textBoxNewFANLineItem.Text);
            releaseNumInt = Int32.Parse(this.textBoxNewFANReleaseNum.Text);
            customerStr = this.textBoxNewFANCustomer.Text;
            endConsumerNameStr = this.textBoxNewFANEndConsumer.Text;

            if (this.textBoxNewFANSerialNo.Text == "")
            {
                MessageBox.Show("ERROR - Serial No is a required field.");
                this.textBoxNewFANSerialNo.Focus();
                this.textBoxNewFANSerialNo.Select();
                return;
            }

            // Write record to the VSOrderHed table.                
            try
            {
                // Create SqlDataAdapters for reading the VSOrderHed 
                // table using strongly typed datasets.
                VS_DataSetTableAdapters.EtlJobHeadTableAdapter etlJobHeadTA =
                    new VS_DataSetTableAdapters.EtlJobHeadTableAdapter();

                etlJobHeadTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                //vsOrderHedTA.Insert(orderNumInt.ToString(), customerStr, "FAN",
                //    orderLineInt.ToString(), releaseNumInt.ToString(), endConsumerNameStr);


                etlJobHeadTA.Fill(dsEtl.EtlJobHead);


                DataRow drJobHead = dsEtl.Tables["EtlJobHead"].NewRow();

                
                drJobHead["JobNum"] = jobNumStr;
                drJobHead["OrderNum"] = orderNumInt.ToString();
                drJobHead["OrderLine"] = orderLineInt.ToString();
                drJobHead["ReleaseNum"] = releaseNumInt.ToString();
                drJobHead["Customer"] = customerStr;
                drJobHead["UnitType"] = "FAN";
                drJobHead["EndConsumerName"] = endConsumerNameStr;

                dsEtl.Tables["EtlJobHead"].Rows.Add(drJobHead);

                etlJobHeadTA.Update(dsEtl.EtlJobHead);

                //etlJobHeadTA.Update(jobNumStr, orderNumInt.ToString(), customerStr, "FAN",
                //    orderLineInt.ToString(), releaseNumInt.ToString(), endConsumerNameStr);

            }
            catch
            {
                MessageBox.Show("Error occurred while inserting a row to the EtlJobHead table.");
                return;
            }

            VS_DataSetTableAdapters.EtlFANTableAdapter etlFanTA =
                                    new VS_DataSetTableAdapters.EtlFANTableAdapter();

            etlFanTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

            //modelNoVerifiedByStr = this.textBoxNewFANVerifiedBy.Text.Trim();

            try
            {
                etlFanTA.Fill(dsEtl.EtlFAN);

                DataRow drEtlFAN = dsEtl.Tables["EtlFAN"].NewRow();

                drEtlFAN["JobNum"] = jobNumStr;
                drEtlFAN["OrderNum"] = orderNumInt;
                drEtlFAN["OrderLine"] = orderLineInt;
                drEtlFAN["ReleaseNum"] = releaseNumInt;
                drEtlFAN["ModelNum"] = this.textBoxNewFANModelNo.Text;
                drEtlFAN["PartNum"] = this.textBoxNewFANPartNum.Text;
                drEtlFAN["SerialNum"] = this.textBoxNewFANSerialNo.Text;

                drEtlFAN["TagNum"] = this.txtTagNum.Text;
                drEtlFAN["Tag"] = this.txtTag.Text;

                drEtlFAN["ElecBlowerFan"] = this.textBoxNewFANElecBlowerFan.Text;
                drEtlFAN["ElecVolts"] = this.textBoxNewFANElecVolts.Text;
                drEtlFAN["ElecPhase"] = this.textBoxNewFANElecPhase.Text;
                drEtlFAN["ElecHertz"] = this.textBoxNewFANElecHertz.Text;
                drEtlFAN["ElecFLA"] = this.textBoxNewFANElecFLA.Text;
                drEtlFAN["ElecMinVoltage"] = this.textBoxNewFANElecMinVoltage.Text;
                drEtlFAN["ElecMaxVoltage"] = this.textBoxNewFANElecMaxVoltage.Text;
                drEtlFAN["ElecMaxVoltageHertz"] = this.textBoxNewFANElecMaxVoltageHertz.Text;
                drEtlFAN["AirCFM"] = this.textBoxNewFANAirCFM.Text;
                drEtlFAN["AirESP"] = this.textBoxNewFANAirESP.Text;
                drEtlFAN["AirRPM"] = this.textBoxNewFANAirRPM.Text; 
                drEtlFAN["VerifiedBy"] = modelNoVerifiedByStr;
                drEtlFAN["French"] = "False";

                dsEtl.EtlFAN.Rows.Add(drEtlFAN);

                etlFanTA.Update(dsEtl.EtlFAN);

                MessageBox.Show("Job # " + jobNumStr + " added successfully.");

                this.buttonNewFANAccept.Text = "Update";
                GlobalModeType.ModeType = "Update";
                this.buttonNewFANPrint.Enabled = true;
                this.labelNewFANMsg.Text = this.textBoxNewFANSerialNo.Text +
                    " has been successfully written to the EtlFan table.";
                this.labelNewFANMsg2.Text = "Use the Update button to modify this unit's data if necessary.";
            }
            catch
            {
                MessageBox.Show("ERROR inserting data into EtlFan");
            }            

        }

        private void buttonNewFANPrint_Click(object sender, EventArgs e)
        {
            VS_DataSet ds = new VS_DataSet();

            frmPrintFAN frmPrint = new frmPrintFAN();

            ds.FANReportDataTable.Rows.Add(
                    this.textBoxNewFANOrderNum.Text,
                    this.textBoxNewFANLineItem.Text,
                    this.textBoxNewFANReleaseNum.Text,             
                    this.textBoxNewFANModelNo.Text,
                    this.textBoxNewFANPartNum.Text,
                    this.textBoxNewFANSerialNo.Text,
                    this.txtTagNum.Text,
                    this.txtTag.Text,
                    this.textBoxNewFANElecBlowerFan.Text,
                    this.textBoxNewFANElecVolts.Text,
                    this.textBoxNewFANElecPhase.Text,
                    this.textBoxNewFANElecHertz.Text,
                    this.textBoxNewFANElecFLA.Text,
                    this.textBoxNewFANElecMinVoltage.Text,
                    this.textBoxNewFANElecMaxVoltage.Text,
                    this.textBoxNewFANElecMaxVoltageHertz.Text,
                    this.textBoxNewFANAirCFM.Text,
                    this.textBoxNewFANAirESP.Text,
                    this.textBoxNewFANAirRPM.Text);

            ReportDocument cryRpt = new ReportDocument();

            //string reportString = @"\\epicor905app\Apps\OAU\EtlLabel\reports\FAN_Label_v002.rpt";
            string reportString = @"\\KCCWVPEPIC9APP\Apps\OAU\EtlLabel\reports\FAN_Label_v002.rpt";

#if DEBUG
                reportString = @"\\KCCWVTEPIC9APP2\Apps\OAU\EtlLabel\reports\FAN_Label_v002.rpt";
#endif

            //string reportString = @"reports\FAN_Label_v001.rpt";

            cryRpt.Load(reportString);

            cryRpt.SetDataSource(ds.Tables["FANReportDataTable"]);

            frmPrint.crystalReportViewer1.ReportSource = cryRpt;

            frmPrint.ShowDialog();
            frmPrint.crystalReportViewer1.Refresh();  

        }
    }
}