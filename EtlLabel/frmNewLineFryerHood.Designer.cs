namespace ViperSticker_rev4
{
    partial class frmNewLineFryerHood
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxNewFHPartNum = new System.Windows.Forms.TextBox();
            this.textBoxNewFHVerifiedBy = new System.Windows.Forms.TextBox();
            this.labelMewFHVerifiedBy = new System.Windows.Forms.Label();
            this.labelNewFHTag = new System.Windows.Forms.Label();
            this.labelNewFHSerialNo = new System.Windows.Forms.Label();
            this.textBoxNewFHSerialNo = new System.Windows.Forms.TextBox();
            this.textBoxNewFHModelNo = new System.Windows.Forms.TextBox();
            this.labelNewFANMsg = new System.Windows.Forms.Label();
            this.buttonNewFHAccept = new System.Windows.Forms.Button();
            this.buttonNewFHCancel = new System.Windows.Forms.Button();
            this.labelNewFHModelNum = new System.Windows.Forms.Label();
            this.buttonNewFHPrint = new System.Windows.Forms.Button();
            this.groupBoxNewFANVoltageInfo = new System.Windows.Forms.GroupBox();
            this.textBoxNewFHMinFrontOverhang = new System.Windows.Forms.TextBox();
            this.labelNewFHMinFrontOverhang = new System.Windows.Forms.Label();
            this.textBoxNewFHFilterSize = new System.Windows.Forms.TextBox();
            this.labelNewFHFilterSize = new System.Windows.Forms.Label();
            this.textBoxNewFANElecMaxVoltageHertz = new System.Windows.Forms.TextBox();
            this.labelNewFHMinOverallLength = new System.Windows.Forms.Label();
            this.textBoxNewFHMinSideOverhang = new System.Windows.Forms.TextBox();
            this.labelNewFHMinSideOverhang = new System.Windows.Forms.Label();
            this.textBoxNewFHMaxCFM = new System.Windows.Forms.TextBox();
            this.labelNewFHMaxCFM = new System.Windows.Forms.Label();
            this.textBoxNewFHMinCFM = new System.Windows.Forms.TextBox();
            this.labelNewFHMinCFM = new System.Windows.Forms.Label();
            this.textBoxNewFANElecVolts = new System.Windows.Forms.TextBox();
            this.labelNewFHMaxDimension = new System.Windows.Forms.Label();
            this.textBoxNewFHNewDimension = new System.Windows.Forms.TextBox();
            this.labelNewFHMinDimension = new System.Windows.Forms.Label();
            this.textBoxNewFHReleaseNum = new System.Windows.Forms.TextBox();
            this.labelNewFANSlash = new System.Windows.Forms.Label();
            this.groupBoxNewFHJobInfo = new System.Windows.Forms.GroupBox();
            this.textBoxNewFHCustomer = new System.Windows.Forms.TextBox();
            this.textBoxNewFHOrderNum = new System.Windows.Forms.TextBox();
            this.labelNewFHCustomer = new System.Windows.Forms.Label();
            this.labelNewFHOrderNum = new System.Windows.Forms.Label();
            this.textBoxNewFHLineItem = new System.Windows.Forms.TextBox();
            this.labelNewFHLineItem = new System.Windows.Forms.Label();
            this.textBoxNewFHTag = new System.Windows.Forms.TextBox();
            this.groupBoxNewFANUnitInfo = new System.Windows.Forms.GroupBox();
            this.labelNewFHPartNum = new System.Windows.Forms.Label();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStripNewFAN = new System.Windows.Forms.MenuStrip();
            this.textBoxNewFHMaxOverallLength = new System.Windows.Forms.TextBox();
            this.labelNewFHMaxOverallLength = new System.Windows.Forms.Label();
            this.groupBoxNewFANVoltageInfo.SuspendLayout();
            this.groupBoxNewFHJobInfo.SuspendLayout();
            this.groupBoxNewFANUnitInfo.SuspendLayout();
            this.menuStripNewFAN.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxNewFHPartNum
            // 
            this.textBoxNewFHPartNum.BackColor = System.Drawing.Color.White;
            this.textBoxNewFHPartNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFHPartNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFHPartNum.Location = new System.Drawing.Point(76, 46);
            this.textBoxNewFHPartNum.Name = "textBoxNewFHPartNum";
            this.textBoxNewFHPartNum.Size = new System.Drawing.Size(124, 22);
            this.textBoxNewFHPartNum.TabIndex = 2;
            this.textBoxNewFHPartNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxNewFHVerifiedBy
            // 
            this.textBoxNewFHVerifiedBy.BackColor = System.Drawing.Color.White;
            this.textBoxNewFHVerifiedBy.Enabled = false;
            this.textBoxNewFHVerifiedBy.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFHVerifiedBy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFHVerifiedBy.Location = new System.Drawing.Point(223, 101);
            this.textBoxNewFHVerifiedBy.Name = "textBoxNewFHVerifiedBy";
            this.textBoxNewFHVerifiedBy.Size = new System.Drawing.Size(67, 22);
            this.textBoxNewFHVerifiedBy.TabIndex = 63;
            this.textBoxNewFHVerifiedBy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxNewFHVerifiedBy.Visible = false;
            // 
            // labelMewFHVerifiedBy
            // 
            this.labelMewFHVerifiedBy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMewFHVerifiedBy.Location = new System.Drawing.Point(221, 76);
            this.labelMewFHVerifiedBy.Name = "labelMewFHVerifiedBy";
            this.labelMewFHVerifiedBy.Size = new System.Drawing.Size(70, 20);
            this.labelMewFHVerifiedBy.TabIndex = 12;
            this.labelMewFHVerifiedBy.Text = "Verified By:";
            this.labelMewFHVerifiedBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelMewFHVerifiedBy.Visible = false;
            // 
            // labelNewFHTag
            // 
            this.labelNewFHTag.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFHTag.Location = new System.Drawing.Point(7, 101);
            this.labelNewFHTag.Name = "labelNewFHTag";
            this.labelNewFHTag.Size = new System.Drawing.Size(65, 21);
            this.labelNewFHTag.TabIndex = 8;
            this.labelNewFHTag.Text = "Tag:";
            this.labelNewFHTag.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelNewFHSerialNo
            // 
            this.labelNewFHSerialNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFHSerialNo.Location = new System.Drawing.Point(7, 73);
            this.labelNewFHSerialNo.Name = "labelNewFHSerialNo";
            this.labelNewFHSerialNo.Size = new System.Drawing.Size(65, 21);
            this.labelNewFHSerialNo.TabIndex = 7;
            this.labelNewFHSerialNo.Text = "Serial No:";
            this.labelNewFHSerialNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewFHSerialNo
            // 
            this.textBoxNewFHSerialNo.BackColor = System.Drawing.Color.White;
            this.textBoxNewFHSerialNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFHSerialNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFHSerialNo.Location = new System.Drawing.Point(76, 73);
            this.textBoxNewFHSerialNo.Name = "textBoxNewFHSerialNo";
            this.textBoxNewFHSerialNo.Size = new System.Drawing.Size(108, 22);
            this.textBoxNewFHSerialNo.TabIndex = 3;
            this.textBoxNewFHSerialNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxNewFHModelNo
            // 
            this.textBoxNewFHModelNo.BackColor = System.Drawing.Color.White;
            this.textBoxNewFHModelNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFHModelNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFHModelNo.Location = new System.Drawing.Point(76, 19);
            this.textBoxNewFHModelNo.Name = "textBoxNewFHModelNo";
            this.textBoxNewFHModelNo.Size = new System.Drawing.Size(227, 22);
            this.textBoxNewFHModelNo.TabIndex = 1;
            this.textBoxNewFHModelNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelNewFANMsg
            // 
            this.labelNewFANMsg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.labelNewFANMsg.Location = new System.Drawing.Point(12, 565);
            this.labelNewFANMsg.Name = "labelNewFANMsg";
            this.labelNewFANMsg.Size = new System.Drawing.Size(447, 21);
            this.labelNewFANMsg.TabIndex = 34;
            this.labelNewFANMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonNewFHAccept
            // 
            this.buttonNewFHAccept.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewFHAccept.ForeColor = System.Drawing.Color.Blue;
            this.buttonNewFHAccept.Location = new System.Drawing.Point(351, 134);
            this.buttonNewFHAccept.Name = "buttonNewFHAccept";
            this.buttonNewFHAccept.Size = new System.Drawing.Size(95, 37);
            this.buttonNewFHAccept.TabIndex = 32;
            this.buttonNewFHAccept.Text = "Accept";
            this.buttonNewFHAccept.UseVisualStyleBackColor = true;
            // 
            // buttonNewFHCancel
            // 
            this.buttonNewFHCancel.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewFHCancel.ForeColor = System.Drawing.Color.Red;
            this.buttonNewFHCancel.Location = new System.Drawing.Point(351, 85);
            this.buttonNewFHCancel.Name = "buttonNewFHCancel";
            this.buttonNewFHCancel.Size = new System.Drawing.Size(95, 37);
            this.buttonNewFHCancel.TabIndex = 31;
            this.buttonNewFHCancel.Text = "Cancel";
            this.buttonNewFHCancel.UseVisualStyleBackColor = true;
            this.buttonNewFHCancel.Click += new System.EventHandler(this.buttonNewFHCancel_Click);
            // 
            // labelNewFHModelNum
            // 
            this.labelNewFHModelNum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFHModelNum.Location = new System.Drawing.Point(7, 19);
            this.labelNewFHModelNum.Name = "labelNewFHModelNum";
            this.labelNewFHModelNum.Size = new System.Drawing.Size(65, 21);
            this.labelNewFHModelNum.TabIndex = 4;
            this.labelNewFHModelNum.Text = "Model No:";
            this.labelNewFHModelNum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonNewFHPrint
            // 
            this.buttonNewFHPrint.Enabled = false;
            this.buttonNewFHPrint.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewFHPrint.ForeColor = System.Drawing.Color.Blue;
            this.buttonNewFHPrint.Location = new System.Drawing.Point(351, 182);
            this.buttonNewFHPrint.Name = "buttonNewFHPrint";
            this.buttonNewFHPrint.Size = new System.Drawing.Size(95, 37);
            this.buttonNewFHPrint.TabIndex = 33;
            this.buttonNewFHPrint.Text = "Print";
            this.buttonNewFHPrint.UseVisualStyleBackColor = true;
            // 
            // groupBoxNewFANVoltageInfo
            // 
            this.groupBoxNewFANVoltageInfo.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.textBoxNewFHMaxOverallLength);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.labelNewFHMaxOverallLength);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.textBoxNewFHMinFrontOverhang);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.labelNewFHMinFrontOverhang);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.textBoxNewFHFilterSize);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.labelNewFHFilterSize);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.textBoxNewFANElecMaxVoltageHertz);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.labelNewFHMinOverallLength);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.textBoxNewFHMinSideOverhang);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.labelNewFHMinSideOverhang);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.textBoxNewFHMaxCFM);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.labelNewFHMaxCFM);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.textBoxNewFHMinCFM);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.labelNewFHMinCFM);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.textBoxNewFANElecVolts);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.labelNewFHMaxDimension);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.textBoxNewFHNewDimension);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.labelNewFHMinDimension);
            this.groupBoxNewFANVoltageInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewFANVoltageInfo.Location = new System.Drawing.Point(12, 286);
            this.groupBoxNewFANVoltageInfo.Name = "groupBoxNewFANVoltageInfo";
            this.groupBoxNewFANVoltageInfo.Size = new System.Drawing.Size(447, 276);
            this.groupBoxNewFANVoltageInfo.TabIndex = 29;
            this.groupBoxNewFANVoltageInfo.TabStop = false;
            this.groupBoxNewFANVoltageInfo.Text = "FAN";
            // 
            // textBoxNewFHMinFrontOverhang
            // 
            this.textBoxNewFHMinFrontOverhang.BackColor = System.Drawing.Color.White;
            this.textBoxNewFHMinFrontOverhang.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFHMinFrontOverhang.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFHMinFrontOverhang.Location = new System.Drawing.Point(239, 127);
            this.textBoxNewFHMinFrontOverhang.Name = "textBoxNewFHMinFrontOverhang";
            this.textBoxNewFHMinFrontOverhang.Size = new System.Drawing.Size(90, 22);
            this.textBoxNewFHMinFrontOverhang.TabIndex = 5;
            this.textBoxNewFHMinFrontOverhang.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFHMinFrontOverhang
            // 
            this.labelNewFHMinFrontOverhang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFHMinFrontOverhang.Location = new System.Drawing.Point(78, 127);
            this.labelNewFHMinFrontOverhang.Name = "labelNewFHMinFrontOverhang";
            this.labelNewFHMinFrontOverhang.Size = new System.Drawing.Size(158, 21);
            this.labelNewFHMinFrontOverhang.TabIndex = 54;
            this.labelNewFHMinFrontOverhang.Text = "Minimum Front Overhang:";
            this.labelNewFHMinFrontOverhang.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewFHFilterSize
            // 
            this.textBoxNewFHFilterSize.BackColor = System.Drawing.Color.White;
            this.textBoxNewFHFilterSize.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFHFilterSize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFHFilterSize.Location = new System.Drawing.Point(239, 181);
            this.textBoxNewFHFilterSize.Name = "textBoxNewFHFilterSize";
            this.textBoxNewFHFilterSize.Size = new System.Drawing.Size(90, 22);
            this.textBoxNewFHFilterSize.TabIndex = 7;
            this.textBoxNewFHFilterSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFHFilterSize
            // 
            this.labelNewFHFilterSize.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFHFilterSize.Location = new System.Drawing.Point(78, 181);
            this.labelNewFHFilterSize.Name = "labelNewFHFilterSize";
            this.labelNewFHFilterSize.Size = new System.Drawing.Size(158, 21);
            this.labelNewFHFilterSize.TabIndex = 50;
            this.labelNewFHFilterSize.Text = "Filter Size:";
            this.labelNewFHFilterSize.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewFANElecMaxVoltageHertz
            // 
            this.textBoxNewFANElecMaxVoltageHertz.BackColor = System.Drawing.Color.White;
            this.textBoxNewFANElecMaxVoltageHertz.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFANElecMaxVoltageHertz.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFANElecMaxVoltageHertz.Location = new System.Drawing.Point(239, 208);
            this.textBoxNewFANElecMaxVoltageHertz.Name = "textBoxNewFANElecMaxVoltageHertz";
            this.textBoxNewFANElecMaxVoltageHertz.Size = new System.Drawing.Size(90, 22);
            this.textBoxNewFANElecMaxVoltageHertz.TabIndex = 8;
            this.textBoxNewFANElecMaxVoltageHertz.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFHMinOverallLength
            // 
            this.labelNewFHMinOverallLength.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFHMinOverallLength.Location = new System.Drawing.Point(78, 208);
            this.labelNewFHMinOverallLength.Name = "labelNewFHMinOverallLength";
            this.labelNewFHMinOverallLength.Size = new System.Drawing.Size(158, 21);
            this.labelNewFHMinOverallLength.TabIndex = 48;
            this.labelNewFHMinOverallLength.Text = "Minimum Overall Length:";
            this.labelNewFHMinOverallLength.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewFHMinSideOverhang
            // 
            this.textBoxNewFHMinSideOverhang.BackColor = System.Drawing.Color.White;
            this.textBoxNewFHMinSideOverhang.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFHMinSideOverhang.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFHMinSideOverhang.Location = new System.Drawing.Point(239, 154);
            this.textBoxNewFHMinSideOverhang.Name = "textBoxNewFHMinSideOverhang";
            this.textBoxNewFHMinSideOverhang.Size = new System.Drawing.Size(90, 22);
            this.textBoxNewFHMinSideOverhang.TabIndex = 6;
            this.textBoxNewFHMinSideOverhang.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFHMinSideOverhang
            // 
            this.labelNewFHMinSideOverhang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFHMinSideOverhang.Location = new System.Drawing.Point(78, 154);
            this.labelNewFHMinSideOverhang.Name = "labelNewFHMinSideOverhang";
            this.labelNewFHMinSideOverhang.Size = new System.Drawing.Size(158, 21);
            this.labelNewFHMinSideOverhang.TabIndex = 46;
            this.labelNewFHMinSideOverhang.Text = "Minimum Side Overhang:";
            this.labelNewFHMinSideOverhang.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewFHMaxCFM
            // 
            this.textBoxNewFHMaxCFM.BackColor = System.Drawing.Color.White;
            this.textBoxNewFHMaxCFM.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFHMaxCFM.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFHMaxCFM.Location = new System.Drawing.Point(239, 100);
            this.textBoxNewFHMaxCFM.Name = "textBoxNewFHMaxCFM";
            this.textBoxNewFHMaxCFM.Size = new System.Drawing.Size(90, 22);
            this.textBoxNewFHMaxCFM.TabIndex = 4;
            this.textBoxNewFHMaxCFM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFHMaxCFM
            // 
            this.labelNewFHMaxCFM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFHMaxCFM.Location = new System.Drawing.Point(75, 100);
            this.labelNewFHMaxCFM.Name = "labelNewFHMaxCFM";
            this.labelNewFHMaxCFM.Size = new System.Drawing.Size(158, 21);
            this.labelNewFHMaxCFM.TabIndex = 44;
            this.labelNewFHMaxCFM.Text = "Maximum CFM:";
            this.labelNewFHMaxCFM.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewFHMinCFM
            // 
            this.textBoxNewFHMinCFM.BackColor = System.Drawing.Color.White;
            this.textBoxNewFHMinCFM.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFHMinCFM.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFHMinCFM.Location = new System.Drawing.Point(239, 73);
            this.textBoxNewFHMinCFM.Name = "textBoxNewFHMinCFM";
            this.textBoxNewFHMinCFM.Size = new System.Drawing.Size(90, 22);
            this.textBoxNewFHMinCFM.TabIndex = 3;
            this.textBoxNewFHMinCFM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFHMinCFM
            // 
            this.labelNewFHMinCFM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFHMinCFM.Location = new System.Drawing.Point(78, 73);
            this.labelNewFHMinCFM.Name = "labelNewFHMinCFM";
            this.labelNewFHMinCFM.Size = new System.Drawing.Size(158, 21);
            this.labelNewFHMinCFM.TabIndex = 42;
            this.labelNewFHMinCFM.Text = "Minimum CFM:";
            this.labelNewFHMinCFM.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewFANElecVolts
            // 
            this.textBoxNewFANElecVolts.BackColor = System.Drawing.Color.White;
            this.textBoxNewFANElecVolts.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFANElecVolts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFANElecVolts.Location = new System.Drawing.Point(239, 46);
            this.textBoxNewFANElecVolts.Name = "textBoxNewFANElecVolts";
            this.textBoxNewFANElecVolts.Size = new System.Drawing.Size(90, 22);
            this.textBoxNewFANElecVolts.TabIndex = 2;
            this.textBoxNewFANElecVolts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFHMaxDimension
            // 
            this.labelNewFHMaxDimension.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFHMaxDimension.Location = new System.Drawing.Point(78, 46);
            this.labelNewFHMaxDimension.Name = "labelNewFHMaxDimension";
            this.labelNewFHMaxDimension.Size = new System.Drawing.Size(158, 21);
            this.labelNewFHMaxDimension.TabIndex = 40;
            this.labelNewFHMaxDimension.Text = "Maximum Dimension:";
            this.labelNewFHMaxDimension.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewFHNewDimension
            // 
            this.textBoxNewFHNewDimension.BackColor = System.Drawing.Color.White;
            this.textBoxNewFHNewDimension.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFHNewDimension.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFHNewDimension.Location = new System.Drawing.Point(239, 19);
            this.textBoxNewFHNewDimension.Name = "textBoxNewFHNewDimension";
            this.textBoxNewFHNewDimension.Size = new System.Drawing.Size(90, 22);
            this.textBoxNewFHNewDimension.TabIndex = 1;
            this.textBoxNewFHNewDimension.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFHMinDimension
            // 
            this.labelNewFHMinDimension.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFHMinDimension.Location = new System.Drawing.Point(78, 19);
            this.labelNewFHMinDimension.Name = "labelNewFHMinDimension";
            this.labelNewFHMinDimension.Size = new System.Drawing.Size(158, 21);
            this.labelNewFHMinDimension.TabIndex = 38;
            this.labelNewFHMinDimension.Text = "Minimum Dimension:";
            this.labelNewFHMinDimension.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewFHReleaseNum
            // 
            this.textBoxNewFHReleaseNum.BackColor = System.Drawing.Color.White;
            this.textBoxNewFHReleaseNum.Enabled = false;
            this.textBoxNewFHReleaseNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFHReleaseNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFHReleaseNum.Location = new System.Drawing.Point(149, 73);
            this.textBoxNewFHReleaseNum.Name = "textBoxNewFHReleaseNum";
            this.textBoxNewFHReleaseNum.Size = new System.Drawing.Size(31, 22);
            this.textBoxNewFHReleaseNum.TabIndex = 4;
            this.textBoxNewFHReleaseNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFANSlash
            // 
            this.labelNewFANSlash.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANSlash.Location = new System.Drawing.Point(132, 72);
            this.labelNewFANSlash.Name = "labelNewFANSlash";
            this.labelNewFANSlash.Size = new System.Drawing.Size(15, 21);
            this.labelNewFANSlash.TabIndex = 4;
            this.labelNewFANSlash.Text = "/";
            this.labelNewFANSlash.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBoxNewFHJobInfo
            // 
            this.groupBoxNewFHJobInfo.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.groupBoxNewFHJobInfo.Controls.Add(this.textBoxNewFHReleaseNum);
            this.groupBoxNewFHJobInfo.Controls.Add(this.labelNewFANSlash);
            this.groupBoxNewFHJobInfo.Controls.Add(this.textBoxNewFHCustomer);
            this.groupBoxNewFHJobInfo.Controls.Add(this.textBoxNewFHOrderNum);
            this.groupBoxNewFHJobInfo.Controls.Add(this.labelNewFHCustomer);
            this.groupBoxNewFHJobInfo.Controls.Add(this.labelNewFHOrderNum);
            this.groupBoxNewFHJobInfo.Controls.Add(this.textBoxNewFHLineItem);
            this.groupBoxNewFHJobInfo.Controls.Add(this.labelNewFHLineItem);
            this.groupBoxNewFHJobInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewFHJobInfo.Location = new System.Drawing.Point(12, 38);
            this.groupBoxNewFHJobInfo.Name = "groupBoxNewFHJobInfo";
            this.groupBoxNewFHJobInfo.Size = new System.Drawing.Size(311, 105);
            this.groupBoxNewFHJobInfo.TabIndex = 26;
            this.groupBoxNewFHJobInfo.TabStop = false;
            this.groupBoxNewFHJobInfo.Text = "Order Info";
            // 
            // textBoxNewFHCustomer
            // 
            this.textBoxNewFHCustomer.BackColor = System.Drawing.Color.White;
            this.textBoxNewFHCustomer.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFHCustomer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFHCustomer.Location = new System.Drawing.Point(95, 46);
            this.textBoxNewFHCustomer.Name = "textBoxNewFHCustomer";
            this.textBoxNewFHCustomer.Size = new System.Drawing.Size(210, 22);
            this.textBoxNewFHCustomer.TabIndex = 2;
            // 
            // textBoxNewFHOrderNum
            // 
            this.textBoxNewFHOrderNum.BackColor = System.Drawing.Color.White;
            this.textBoxNewFHOrderNum.Enabled = false;
            this.textBoxNewFHOrderNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFHOrderNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFHOrderNum.Location = new System.Drawing.Point(95, 19);
            this.textBoxNewFHOrderNum.Name = "textBoxNewFHOrderNum";
            this.textBoxNewFHOrderNum.Size = new System.Drawing.Size(65, 22);
            this.textBoxNewFHOrderNum.TabIndex = 1;
            this.textBoxNewFHOrderNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelNewFHCustomer
            // 
            this.labelNewFHCustomer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFHCustomer.Location = new System.Drawing.Point(4, 46);
            this.labelNewFHCustomer.Name = "labelNewFHCustomer";
            this.labelNewFHCustomer.Size = new System.Drawing.Size(85, 21);
            this.labelNewFHCustomer.TabIndex = 2;
            this.labelNewFHCustomer.Text = "Customer:";
            this.labelNewFHCustomer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelNewFHOrderNum
            // 
            this.labelNewFHOrderNum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFHOrderNum.Location = new System.Drawing.Point(4, 20);
            this.labelNewFHOrderNum.Name = "labelNewFHOrderNum";
            this.labelNewFHOrderNum.Size = new System.Drawing.Size(85, 21);
            this.labelNewFHOrderNum.TabIndex = 1;
            this.labelNewFHOrderNum.Text = "Sales Order#:";
            this.labelNewFHOrderNum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxNewFHLineItem
            // 
            this.textBoxNewFHLineItem.BackColor = System.Drawing.Color.White;
            this.textBoxNewFHLineItem.Enabled = false;
            this.textBoxNewFHLineItem.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFHLineItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFHLineItem.Location = new System.Drawing.Point(95, 73);
            this.textBoxNewFHLineItem.Name = "textBoxNewFHLineItem";
            this.textBoxNewFHLineItem.Size = new System.Drawing.Size(31, 22);
            this.textBoxNewFHLineItem.TabIndex = 3;
            this.textBoxNewFHLineItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFHLineItem
            // 
            this.labelNewFHLineItem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFHLineItem.Location = new System.Drawing.Point(4, 73);
            this.labelNewFHLineItem.Name = "labelNewFHLineItem";
            this.labelNewFHLineItem.Size = new System.Drawing.Size(85, 21);
            this.labelNewFHLineItem.TabIndex = 2;
            this.labelNewFHLineItem.Text = "Line #/Rel #:";
            this.labelNewFHLineItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewFHTag
            // 
            this.textBoxNewFHTag.BackColor = System.Drawing.Color.White;
            this.textBoxNewFHTag.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFHTag.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFHTag.Location = new System.Drawing.Point(76, 101);
            this.textBoxNewFHTag.Name = "textBoxNewFHTag";
            this.textBoxNewFHTag.Size = new System.Drawing.Size(108, 22);
            this.textBoxNewFHTag.TabIndex = 66;
            this.textBoxNewFHTag.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBoxNewFANUnitInfo
            // 
            this.groupBoxNewFANUnitInfo.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.groupBoxNewFANUnitInfo.Controls.Add(this.textBoxNewFHTag);
            this.groupBoxNewFANUnitInfo.Controls.Add(this.labelNewFHPartNum);
            this.groupBoxNewFANUnitInfo.Controls.Add(this.textBoxNewFHPartNum);
            this.groupBoxNewFANUnitInfo.Controls.Add(this.textBoxNewFHVerifiedBy);
            this.groupBoxNewFANUnitInfo.Controls.Add(this.labelMewFHVerifiedBy);
            this.groupBoxNewFANUnitInfo.Controls.Add(this.labelNewFHTag);
            this.groupBoxNewFANUnitInfo.Controls.Add(this.labelNewFHSerialNo);
            this.groupBoxNewFANUnitInfo.Controls.Add(this.textBoxNewFHSerialNo);
            this.groupBoxNewFANUnitInfo.Controls.Add(this.textBoxNewFHModelNo);
            this.groupBoxNewFANUnitInfo.Controls.Add(this.labelNewFHModelNum);
            this.groupBoxNewFANUnitInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewFANUnitInfo.Location = new System.Drawing.Point(12, 149);
            this.groupBoxNewFANUnitInfo.Name = "groupBoxNewFANUnitInfo";
            this.groupBoxNewFANUnitInfo.Size = new System.Drawing.Size(311, 131);
            this.groupBoxNewFANUnitInfo.TabIndex = 27;
            this.groupBoxNewFANUnitInfo.TabStop = false;
            this.groupBoxNewFANUnitInfo.Text = "Unit Info";
            // 
            // labelNewFHPartNum
            // 
            this.labelNewFHPartNum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFHPartNum.Location = new System.Drawing.Point(7, 46);
            this.labelNewFHPartNum.Name = "labelNewFHPartNum";
            this.labelNewFHPartNum.Size = new System.Drawing.Size(65, 21);
            this.labelNewFHPartNum.TabIndex = 65;
            this.labelNewFHPartNum.Text = "Part Num:";
            this.labelNewFHPartNum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // menuStripNewFAN
            // 
            this.menuStripNewFAN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.menuStripNewFAN.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStripNewFAN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStripNewFAN.Location = new System.Drawing.Point(0, 0);
            this.menuStripNewFAN.Name = "menuStripNewFAN";
            this.menuStripNewFAN.Size = new System.Drawing.Size(471, 24);
            this.menuStripNewFAN.TabIndex = 28;
            this.menuStripNewFAN.Text = "menuStrip1";
            // 
            // textBoxNewFHMaxOverallLength
            // 
            this.textBoxNewFHMaxOverallLength.BackColor = System.Drawing.Color.White;
            this.textBoxNewFHMaxOverallLength.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewFHMaxOverallLength.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewFHMaxOverallLength.Location = new System.Drawing.Point(239, 235);
            this.textBoxNewFHMaxOverallLength.Name = "textBoxNewFHMaxOverallLength";
            this.textBoxNewFHMaxOverallLength.Size = new System.Drawing.Size(90, 22);
            this.textBoxNewFHMaxOverallLength.TabIndex = 55;
            this.textBoxNewFHMaxOverallLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFHMaxOverallLength
            // 
            this.labelNewFHMaxOverallLength.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFHMaxOverallLength.Location = new System.Drawing.Point(78, 235);
            this.labelNewFHMaxOverallLength.Name = "labelNewFHMaxOverallLength";
            this.labelNewFHMaxOverallLength.Size = new System.Drawing.Size(158, 21);
            this.labelNewFHMaxOverallLength.TabIndex = 56;
            this.labelNewFHMaxOverallLength.Text = "Maximum Overall Length:";
            this.labelNewFHMaxOverallLength.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // frmNewLineFryerHood
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(471, 591);
            this.Controls.Add(this.labelNewFANMsg);
            this.Controls.Add(this.buttonNewFHAccept);
            this.Controls.Add(this.buttonNewFHCancel);
            this.Controls.Add(this.buttonNewFHPrint);
            this.Controls.Add(this.groupBoxNewFANVoltageInfo);
            this.Controls.Add(this.groupBoxNewFHJobInfo);
            this.Controls.Add(this.groupBoxNewFANUnitInfo);
            this.Controls.Add(this.menuStripNewFAN);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.Name = "frmNewLineFryerHood";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New Fryer Hood";
            this.groupBoxNewFANVoltageInfo.ResumeLayout(false);
            this.groupBoxNewFANVoltageInfo.PerformLayout();
            this.groupBoxNewFHJobInfo.ResumeLayout(false);
            this.groupBoxNewFHJobInfo.PerformLayout();
            this.groupBoxNewFANUnitInfo.ResumeLayout(false);
            this.groupBoxNewFANUnitInfo.PerformLayout();
            this.menuStripNewFAN.ResumeLayout(false);
            this.menuStripNewFAN.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox textBoxNewFHPartNum;
        public System.Windows.Forms.TextBox textBoxNewFHVerifiedBy;
        private System.Windows.Forms.Label labelMewFHVerifiedBy;
        private System.Windows.Forms.Label labelNewFHTag;
        private System.Windows.Forms.Label labelNewFHSerialNo;
        public System.Windows.Forms.TextBox textBoxNewFHSerialNo;
        public System.Windows.Forms.TextBox textBoxNewFHModelNo;
        public System.Windows.Forms.Label labelNewFANMsg;
        public System.Windows.Forms.Button buttonNewFHAccept;
        public System.Windows.Forms.Button buttonNewFHCancel;
        private System.Windows.Forms.Label labelNewFHModelNum;
        public System.Windows.Forms.Button buttonNewFHPrint;
        public System.Windows.Forms.GroupBox groupBoxNewFANVoltageInfo;
        public System.Windows.Forms.TextBox textBoxNewFHMinFrontOverhang;
        private System.Windows.Forms.Label labelNewFHMinFrontOverhang;
        public System.Windows.Forms.TextBox textBoxNewFHFilterSize;
        private System.Windows.Forms.Label labelNewFHFilterSize;
        public System.Windows.Forms.TextBox textBoxNewFANElecMaxVoltageHertz;
        private System.Windows.Forms.Label labelNewFHMinOverallLength;
        public System.Windows.Forms.TextBox textBoxNewFHMinSideOverhang;
        private System.Windows.Forms.Label labelNewFHMinSideOverhang;
        public System.Windows.Forms.TextBox textBoxNewFHMaxCFM;
        private System.Windows.Forms.Label labelNewFHMaxCFM;
        public System.Windows.Forms.TextBox textBoxNewFHMinCFM;
        private System.Windows.Forms.Label labelNewFHMinCFM;
        public System.Windows.Forms.TextBox textBoxNewFANElecVolts;
        private System.Windows.Forms.Label labelNewFHMaxDimension;
        public System.Windows.Forms.TextBox textBoxNewFHNewDimension;
        private System.Windows.Forms.Label labelNewFHMinDimension;
        public System.Windows.Forms.TextBox textBoxNewFHReleaseNum;
        private System.Windows.Forms.Label labelNewFANSlash;
        private System.Windows.Forms.GroupBox groupBoxNewFHJobInfo;
        public System.Windows.Forms.TextBox textBoxNewFHCustomer;
        public System.Windows.Forms.TextBox textBoxNewFHOrderNum;
        private System.Windows.Forms.Label labelNewFHCustomer;
        private System.Windows.Forms.Label labelNewFHOrderNum;
        public System.Windows.Forms.TextBox textBoxNewFHLineItem;
        private System.Windows.Forms.Label labelNewFHLineItem;
        public System.Windows.Forms.TextBox textBoxNewFHTag;
        public System.Windows.Forms.GroupBox groupBoxNewFANUnitInfo;
        private System.Windows.Forms.Label labelNewFHPartNum;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStripNewFAN;
        public System.Windows.Forms.TextBox textBoxNewFHMaxOverallLength;
        private System.Windows.Forms.Label labelNewFHMaxOverallLength;
    }
}