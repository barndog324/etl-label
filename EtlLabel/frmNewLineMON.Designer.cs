﻿namespace KCC.OA.Etl
{
    partial class frmNewLineMON
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxNewRRU = new System.Windows.Forms.GroupBox();
            this.groupBoxNewRRUFridgeType = new System.Windows.Forms.GroupBox();
            this.radioButtonNewRRUR410A = new System.Windows.Forms.RadioButton();
            this.groupBoxNewRRUElectricInfo = new System.Windows.Forms.GroupBox();
            this.label40 = new System.Windows.Forms.Label();
            this.txtHeatingInputElectric = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.txtVoltage = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFuelType = new System.Windows.Forms.TextBox();
            this.labelNewRRUHeatingTypeInfo = new System.Windows.Forms.Label();
            this.txtOperatingVolts = new System.Windows.Forms.TextBox();
            this.labelNewRRUOperatingVolts = new System.Windows.Forms.Label();
            this.txtElectricalRating = new System.Windows.Forms.TextBox();
            this.labelNewRRUElectricalRating = new System.Windows.Forms.Label();
            this.txtRawMOP = new System.Windows.Forms.TextBox();
            this.lbRawMOP = new System.Windows.Forms.Label();
            this.txtMFSMCB = new System.Windows.Forms.TextBox();
            this.labelNewRRUMFSMCB = new System.Windows.Forms.Label();
            this.txtMinCKTAmp = new System.Windows.Forms.TextBox();
            this.txtMaxOutAirTemp = new System.Windows.Forms.TextBox();
            this.labelNewRRUMinCKTAmp = new System.Windows.Forms.Label();
            this.cbMonFrench = new System.Windows.Forms.CheckBox();
            this.lbNewMONMsg = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnAccept = new System.Windows.Forms.Button();
            this.buttonNewRRUCancel = new System.Windows.Forms.Button();
            this.gbMonFanInfo = new System.Windows.Forms.GroupBox();
            this.gbAirXchange = new System.Windows.Forms.GroupBox();
            this.txtAirXchangeWheelQty = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.txtAirXchangeWheelPhase = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtAirXchangeWheelFLA = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.txtAirXchangeWheelHP = new System.Windows.Forms.TextBox();
            this.gbSemcoWheel = new System.Windows.Forms.GroupBox();
            this.txtSemcoWheelQty = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtSemcoWheelPhase = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtSemcoWheelFLA = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtSemcoWheelHP = new System.Windows.Forms.TextBox();
            this.gbExhaustFan = new System.Windows.Forms.GroupBox();
            this.txtExhaustFanQty = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtExhaustFanPhase = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtExhaustFanFLA = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtExhaustFanHP = new System.Windows.Forms.TextBox();
            this.groupBoxNewRRULineInfo = new System.Windows.Forms.GroupBox();
            this.txtHeatType = new System.Windows.Forms.TextBox();
            this.txtModelNo = new System.Windows.Forms.TextBox();
            this.labelNewRRUHeatingType = new System.Windows.Forms.Label();
            this.labelNewRRUPartNum = new System.Windows.Forms.Label();
            this.txtPartNum = new System.Windows.Forms.TextBox();
            this.txtVerifiedBy = new System.Windows.Forms.TextBox();
            this.labelMewRRUCreatedBy = new System.Windows.Forms.Label();
            this.labelNewRRUSerialNo = new System.Windows.Forms.Label();
            this.txtSerialNo = new System.Windows.Forms.TextBox();
            this.labelNewRRUModelNum = new System.Windows.Forms.Label();
            this.groupBoxNewRRUJobInfo = new System.Windows.Forms.GroupBox();
            this.txtJobNum = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtEndConsumer = new System.Windows.Forms.TextBox();
            this.labelNewRRUEndConsumer = new System.Windows.Forms.Label();
            this.txtReleaseNum = new System.Windows.Forms.TextBox();
            this.labelNewRRUSlash = new System.Windows.Forms.Label();
            this.txtCustomer = new System.Windows.Forms.TextBox();
            this.txtOrderNum = new System.Windows.Forms.TextBox();
            this.labelNewRRUCustomer = new System.Windows.Forms.Label();
            this.labelNewRRUOrderNum = new System.Windows.Forms.Label();
            this.txtLineItem = new System.Windows.Forms.TextBox();
            this.labelNewRRULineItem = new System.Windows.Forms.Label();
            this.groupBoxNewRRU.SuspendLayout();
            this.groupBoxNewRRUFridgeType.SuspendLayout();
            this.groupBoxNewRRUElectricInfo.SuspendLayout();
            this.gbMonFanInfo.SuspendLayout();
            this.gbAirXchange.SuspendLayout();
            this.gbSemcoWheel.SuspendLayout();
            this.gbExhaustFan.SuspendLayout();
            this.groupBoxNewRRULineInfo.SuspendLayout();
            this.groupBoxNewRRUJobInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxNewRRU
            // 
            this.groupBoxNewRRU.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBoxNewRRU.Controls.Add(this.groupBoxNewRRUFridgeType);
            this.groupBoxNewRRU.Controls.Add(this.groupBoxNewRRUElectricInfo);
            this.groupBoxNewRRU.Controls.Add(this.cbMonFrench);
            this.groupBoxNewRRU.Controls.Add(this.lbNewMONMsg);
            this.groupBoxNewRRU.Controls.Add(this.btnPrint);
            this.groupBoxNewRRU.Controls.Add(this.btnAccept);
            this.groupBoxNewRRU.Controls.Add(this.buttonNewRRUCancel);
            this.groupBoxNewRRU.Controls.Add(this.gbMonFanInfo);
            this.groupBoxNewRRU.Controls.Add(this.groupBoxNewRRULineInfo);
            this.groupBoxNewRRU.Controls.Add(this.groupBoxNewRRUJobInfo);
            this.groupBoxNewRRU.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewRRU.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.groupBoxNewRRU.Location = new System.Drawing.Point(12, 15);
            this.groupBoxNewRRU.Name = "groupBoxNewRRU";
            this.groupBoxNewRRU.Size = new System.Drawing.Size(853, 968);
            this.groupBoxNewRRU.TabIndex = 2;
            this.groupBoxNewRRU.TabStop = false;
            this.groupBoxNewRRU.Text = "RRU";
            // 
            // groupBoxNewRRUFridgeType
            // 
            this.groupBoxNewRRUFridgeType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewRRUFridgeType.Controls.Add(this.radioButtonNewRRUR410A);
            this.groupBoxNewRRUFridgeType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewRRUFridgeType.Location = new System.Drawing.Point(711, 17);
            this.groupBoxNewRRUFridgeType.Name = "groupBoxNewRRUFridgeType";
            this.groupBoxNewRRUFridgeType.Size = new System.Drawing.Size(132, 51);
            this.groupBoxNewRRUFridgeType.TabIndex = 63;
            this.groupBoxNewRRUFridgeType.TabStop = false;
            this.groupBoxNewRRUFridgeType.Text = "Fridge Type";
            // 
            // radioButtonNewRRUR410A
            // 
            this.radioButtonNewRRUR410A.AutoSize = true;
            this.radioButtonNewRRUR410A.Checked = true;
            this.radioButtonNewRRUR410A.Location = new System.Drawing.Point(47, 21);
            this.radioButtonNewRRUR410A.Name = "radioButtonNewRRUR410A";
            this.radioButtonNewRRUR410A.Size = new System.Drawing.Size(67, 18);
            this.radioButtonNewRRUR410A.TabIndex = 64;
            this.radioButtonNewRRUR410A.TabStop = true;
            this.radioButtonNewRRUR410A.Text = "R410A";
            this.radioButtonNewRRUR410A.UseVisualStyleBackColor = true;
            // 
            // groupBoxNewRRUElectricInfo
            // 
            this.groupBoxNewRRUElectricInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.label40);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.txtHeatingInputElectric);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.label39);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.txtVoltage);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.label3);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.txtFuelType);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.labelNewRRUHeatingTypeInfo);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.txtOperatingVolts);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.labelNewRRUOperatingVolts);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.txtElectricalRating);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.labelNewRRUElectricalRating);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.txtRawMOP);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.lbRawMOP);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.txtMFSMCB);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.labelNewRRUMFSMCB);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.txtMinCKTAmp);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.txtMaxOutAirTemp);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.labelNewRRUMinCKTAmp);
            this.groupBoxNewRRUElectricInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewRRUElectricInfo.Location = new System.Drawing.Point(425, 17);
            this.groupBoxNewRRUElectricInfo.Name = "groupBoxNewRRUElectricInfo";
            this.groupBoxNewRRUElectricInfo.Size = new System.Drawing.Size(278, 260);
            this.groupBoxNewRRUElectricInfo.TabIndex = 5;
            this.groupBoxNewRRUElectricInfo.TabStop = false;
            this.groupBoxNewRRUElectricInfo.Text = "Electric/Heating Info";
            // 
            // label40
            // 
            this.label40.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(15, 233);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(135, 21);
            this.label40.TabIndex = 66;
            this.label40.Text = "Heating Input Electric:";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtHeatingInputElectric
            // 
            this.txtHeatingInputElectric.BackColor = System.Drawing.Color.White;
            this.txtHeatingInputElectric.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHeatingInputElectric.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtHeatingInputElectric.Location = new System.Drawing.Point(156, 232);
            this.txtHeatingInputElectric.Name = "txtHeatingInputElectric";
            this.txtHeatingInputElectric.Size = new System.Drawing.Size(99, 22);
            this.txtHeatingInputElectric.TabIndex = 65;
            this.txtHeatingInputElectric.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label39
            // 
            this.label39.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(15, 206);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(135, 21);
            this.label39.TabIndex = 64;
            this.label39.Text = "Voltage:";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtVoltage
            // 
            this.txtVoltage.BackColor = System.Drawing.Color.White;
            this.txtVoltage.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVoltage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtVoltage.Location = new System.Drawing.Point(156, 205);
            this.txtVoltage.Name = "txtVoltage";
            this.txtVoltage.Size = new System.Drawing.Size(99, 22);
            this.txtVoltage.TabIndex = 63;
            this.txtVoltage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 178);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 21);
            this.label3.TabIndex = 62;
            this.label3.Text = "Max Air Outlet Temp:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtFuelType
            // 
            this.txtFuelType.BackColor = System.Drawing.Color.White;
            this.txtFuelType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFuelType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtFuelType.Location = new System.Drawing.Point(156, 151);
            this.txtFuelType.Name = "txtFuelType";
            this.txtFuelType.Size = new System.Drawing.Size(99, 22);
            this.txtFuelType.TabIndex = 6;
            this.txtFuelType.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUHeatingTypeInfo
            // 
            this.labelNewRRUHeatingTypeInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUHeatingTypeInfo.Location = new System.Drawing.Point(15, 151);
            this.labelNewRRUHeatingTypeInfo.Name = "labelNewRRUHeatingTypeInfo";
            this.labelNewRRUHeatingTypeInfo.Size = new System.Drawing.Size(135, 21);
            this.labelNewRRUHeatingTypeInfo.TabIndex = 61;
            this.labelNewRRUHeatingTypeInfo.Text = "Fuel Type:";
            this.labelNewRRUHeatingTypeInfo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtOperatingVolts
            // 
            this.txtOperatingVolts.BackColor = System.Drawing.Color.White;
            this.txtOperatingVolts.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOperatingVolts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtOperatingVolts.Location = new System.Drawing.Point(156, 124);
            this.txtOperatingVolts.Name = "txtOperatingVolts";
            this.txtOperatingVolts.Size = new System.Drawing.Size(99, 22);
            this.txtOperatingVolts.TabIndex = 5;
            this.txtOperatingVolts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUOperatingVolts
            // 
            this.labelNewRRUOperatingVolts.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUOperatingVolts.Location = new System.Drawing.Point(15, 124);
            this.labelNewRRUOperatingVolts.Name = "labelNewRRUOperatingVolts";
            this.labelNewRRUOperatingVolts.Size = new System.Drawing.Size(135, 21);
            this.labelNewRRUOperatingVolts.TabIndex = 14;
            this.labelNewRRUOperatingVolts.Text = "Operating Volts:";
            this.labelNewRRUOperatingVolts.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtElectricalRating
            // 
            this.txtElectricalRating.BackColor = System.Drawing.Color.White;
            this.txtElectricalRating.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtElectricalRating.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtElectricalRating.Location = new System.Drawing.Point(156, 97);
            this.txtElectricalRating.Name = "txtElectricalRating";
            this.txtElectricalRating.Size = new System.Drawing.Size(99, 22);
            this.txtElectricalRating.TabIndex = 4;
            this.txtElectricalRating.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUElectricalRating
            // 
            this.labelNewRRUElectricalRating.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUElectricalRating.Location = new System.Drawing.Point(15, 97);
            this.labelNewRRUElectricalRating.Name = "labelNewRRUElectricalRating";
            this.labelNewRRUElectricalRating.Size = new System.Drawing.Size(135, 21);
            this.labelNewRRUElectricalRating.TabIndex = 12;
            this.labelNewRRUElectricalRating.Text = "Electrical Rating:";
            this.labelNewRRUElectricalRating.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtRawMOP
            // 
            this.txtRawMOP.BackColor = System.Drawing.Color.White;
            this.txtRawMOP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRawMOP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtRawMOP.Location = new System.Drawing.Point(156, 70);
            this.txtRawMOP.Name = "txtRawMOP";
            this.txtRawMOP.Size = new System.Drawing.Size(99, 22);
            this.txtRawMOP.TabIndex = 3;
            this.txtRawMOP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbRawMOP
            // 
            this.lbRawMOP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbRawMOP.Location = new System.Drawing.Point(15, 70);
            this.lbRawMOP.Name = "lbRawMOP";
            this.lbRawMOP.Size = new System.Drawing.Size(135, 21);
            this.lbRawMOP.TabIndex = 10;
            this.lbRawMOP.Text = "Raw MOP:";
            this.lbRawMOP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMFSMCB
            // 
            this.txtMFSMCB.BackColor = System.Drawing.Color.White;
            this.txtMFSMCB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMFSMCB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtMFSMCB.Location = new System.Drawing.Point(156, 43);
            this.txtMFSMCB.Name = "txtMFSMCB";
            this.txtMFSMCB.Size = new System.Drawing.Size(99, 22);
            this.txtMFSMCB.TabIndex = 2;
            this.txtMFSMCB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUMFSMCB
            // 
            this.labelNewRRUMFSMCB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUMFSMCB.Location = new System.Drawing.Point(15, 43);
            this.labelNewRRUMFSMCB.Name = "labelNewRRUMFSMCB";
            this.labelNewRRUMFSMCB.Size = new System.Drawing.Size(135, 21);
            this.labelNewRRUMFSMCB.TabIndex = 8;
            this.labelNewRRUMFSMCB.Text = "MFS MCB:";
            this.labelNewRRUMFSMCB.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMinCKTAmp
            // 
            this.txtMinCKTAmp.BackColor = System.Drawing.Color.White;
            this.txtMinCKTAmp.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMinCKTAmp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtMinCKTAmp.Location = new System.Drawing.Point(156, 16);
            this.txtMinCKTAmp.Name = "txtMinCKTAmp";
            this.txtMinCKTAmp.Size = new System.Drawing.Size(99, 22);
            this.txtMinCKTAmp.TabIndex = 1;
            this.txtMinCKTAmp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtMaxOutAirTemp
            // 
            this.txtMaxOutAirTemp.BackColor = System.Drawing.Color.White;
            this.txtMaxOutAirTemp.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaxOutAirTemp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtMaxOutAirTemp.Location = new System.Drawing.Point(156, 178);
            this.txtMaxOutAirTemp.Name = "txtMaxOutAirTemp";
            this.txtMaxOutAirTemp.Size = new System.Drawing.Size(99, 22);
            this.txtMaxOutAirTemp.TabIndex = 6;
            this.txtMaxOutAirTemp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUMinCKTAmp
            // 
            this.labelNewRRUMinCKTAmp.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUMinCKTAmp.Location = new System.Drawing.Point(15, 16);
            this.labelNewRRUMinCKTAmp.Name = "labelNewRRUMinCKTAmp";
            this.labelNewRRUMinCKTAmp.Size = new System.Drawing.Size(135, 21);
            this.labelNewRRUMinCKTAmp.TabIndex = 6;
            this.labelNewRRUMinCKTAmp.Text = "Min CKT Amp:";
            this.labelNewRRUMinCKTAmp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbMonFrench
            // 
            this.cbMonFrench.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMonFrench.Location = new System.Drawing.Point(711, 77);
            this.cbMonFrench.Name = "cbMonFrench";
            this.cbMonFrench.Size = new System.Drawing.Size(139, 32);
            this.cbMonFrench.TabIndex = 7;
            this.cbMonFrench.Text = "French Canadian Order";
            this.cbMonFrench.UseVisualStyleBackColor = true;
            // 
            // lbNewMONMsg
            // 
            this.lbNewMONMsg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.lbNewMONMsg.Location = new System.Drawing.Point(6, 501);
            this.lbNewMONMsg.Name = "lbNewMONMsg";
            this.lbNewMONMsg.Size = new System.Drawing.Size(840, 21);
            this.lbNewMONMsg.TabIndex = 13;
            this.lbNewMONMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(731, 217);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(95, 37);
            this.btnPrint.TabIndex = 11;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnAccept
            // 
            this.btnAccept.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAccept.Location = new System.Drawing.Point(730, 170);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(95, 37);
            this.btnAccept.TabIndex = 10;
            this.btnAccept.Text = "Accept";
            this.btnAccept.UseVisualStyleBackColor = true;
            this.btnAccept.Click += new System.EventHandler(this.buttonNewRRUAccept_Click);
            // 
            // buttonNewRRUCancel
            // 
            this.buttonNewRRUCancel.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewRRUCancel.ForeColor = System.Drawing.Color.Red;
            this.buttonNewRRUCancel.Location = new System.Drawing.Point(730, 124);
            this.buttonNewRRUCancel.Name = "buttonNewRRUCancel";
            this.buttonNewRRUCancel.Size = new System.Drawing.Size(95, 37);
            this.buttonNewRRUCancel.TabIndex = 9;
            this.buttonNewRRUCancel.Text = "Cancel";
            this.buttonNewRRUCancel.UseVisualStyleBackColor = true;
            this.buttonNewRRUCancel.Click += new System.EventHandler(this.buttonNewRRUCancel_Click);
            // 
            // gbMonFanInfo
            // 
            this.gbMonFanInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gbMonFanInfo.Controls.Add(this.gbAirXchange);
            this.gbMonFanInfo.Controls.Add(this.gbSemcoWheel);
            this.gbMonFanInfo.Controls.Add(this.gbExhaustFan);
            this.gbMonFanInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbMonFanInfo.Location = new System.Drawing.Point(6, 283);
            this.gbMonFanInfo.Name = "gbMonFanInfo";
            this.gbMonFanInfo.Size = new System.Drawing.Size(840, 215);
            this.gbMonFanInfo.TabIndex = 6;
            this.gbMonFanInfo.TabStop = false;
            this.gbMonFanInfo.Text = "Fans";
            // 
            // gbAirXchange
            // 
            this.gbAirXchange.Controls.Add(this.txtAirXchangeWheelQty);
            this.gbAirXchange.Controls.Add(this.label35);
            this.gbAirXchange.Controls.Add(this.label36);
            this.gbAirXchange.Controls.Add(this.txtAirXchangeWheelPhase);
            this.gbAirXchange.Controls.Add(this.label37);
            this.gbAirXchange.Controls.Add(this.txtAirXchangeWheelFLA);
            this.gbAirXchange.Controls.Add(this.label38);
            this.gbAirXchange.Controls.Add(this.txtAirXchangeWheelHP);
            this.gbAirXchange.Location = new System.Drawing.Point(458, 33);
            this.gbAirXchange.Name = "gbAirXchange";
            this.gbAirXchange.Size = new System.Drawing.Size(200, 154);
            this.gbAirXchange.TabIndex = 38;
            this.gbAirXchange.TabStop = false;
            this.gbAirXchange.Text = "Air Xchange Wheel";
            // 
            // txtAirXchangeWheelQty
            // 
            this.txtAirXchangeWheelQty.BackColor = System.Drawing.Color.White;
            this.txtAirXchangeWheelQty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAirXchangeWheelQty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtAirXchangeWheelQty.Location = new System.Drawing.Point(140, 30);
            this.txtAirXchangeWheelQty.Name = "txtAirXchangeWheelQty";
            this.txtAirXchangeWheelQty.Size = new System.Drawing.Size(50, 22);
            this.txtAirXchangeWheelQty.TabIndex = 1;
            this.txtAirXchangeWheelQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label35
            // 
            this.label35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(3, 30);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(135, 21);
            this.label35.TabIndex = 6;
            this.label35.Text = "AirXchange Wheel Qty:";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label36
            // 
            this.label36.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(3, 57);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(135, 21);
            this.label36.TabIndex = 8;
            this.label36.Text = "AirXchange Wheel PH:";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtAirXchangeWheelPhase
            // 
            this.txtAirXchangeWheelPhase.BackColor = System.Drawing.Color.White;
            this.txtAirXchangeWheelPhase.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAirXchangeWheelPhase.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtAirXchangeWheelPhase.Location = new System.Drawing.Point(140, 57);
            this.txtAirXchangeWheelPhase.Name = "txtAirXchangeWheelPhase";
            this.txtAirXchangeWheelPhase.Size = new System.Drawing.Size(50, 22);
            this.txtAirXchangeWheelPhase.TabIndex = 2;
            this.txtAirXchangeWheelPhase.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label37
            // 
            this.label37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(3, 85);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(135, 21);
            this.label37.TabIndex = 10;
            this.label37.Text = "AirXchange Wheel FLA:";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtAirXchangeWheelFLA
            // 
            this.txtAirXchangeWheelFLA.BackColor = System.Drawing.Color.White;
            this.txtAirXchangeWheelFLA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAirXchangeWheelFLA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtAirXchangeWheelFLA.Location = new System.Drawing.Point(140, 85);
            this.txtAirXchangeWheelFLA.Name = "txtAirXchangeWheelFLA";
            this.txtAirXchangeWheelFLA.Size = new System.Drawing.Size(50, 22);
            this.txtAirXchangeWheelFLA.TabIndex = 3;
            this.txtAirXchangeWheelFLA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label38
            // 
            this.label38.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(3, 112);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(135, 21);
            this.label38.TabIndex = 12;
            this.label38.Text = "AirXchange Wheel HP:";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtAirXchangeWheelHP
            // 
            this.txtAirXchangeWheelHP.BackColor = System.Drawing.Color.White;
            this.txtAirXchangeWheelHP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAirXchangeWheelHP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtAirXchangeWheelHP.Location = new System.Drawing.Point(140, 112);
            this.txtAirXchangeWheelHP.Name = "txtAirXchangeWheelHP";
            this.txtAirXchangeWheelHP.Size = new System.Drawing.Size(50, 22);
            this.txtAirXchangeWheelHP.TabIndex = 4;
            this.txtAirXchangeWheelHP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // gbSemcoWheel
            // 
            this.gbSemcoWheel.Controls.Add(this.txtSemcoWheelQty);
            this.gbSemcoWheel.Controls.Add(this.label19);
            this.gbSemcoWheel.Controls.Add(this.label20);
            this.gbSemcoWheel.Controls.Add(this.txtSemcoWheelPhase);
            this.gbSemcoWheel.Controls.Add(this.label21);
            this.gbSemcoWheel.Controls.Add(this.txtSemcoWheelFLA);
            this.gbSemcoWheel.Controls.Add(this.label22);
            this.gbSemcoWheel.Controls.Add(this.txtSemcoWheelHP);
            this.gbSemcoWheel.Location = new System.Drawing.Point(237, 33);
            this.gbSemcoWheel.Name = "gbSemcoWheel";
            this.gbSemcoWheel.Size = new System.Drawing.Size(200, 154);
            this.gbSemcoWheel.TabIndex = 37;
            this.gbSemcoWheel.TabStop = false;
            this.gbSemcoWheel.Text = "Semco Wheel";
            // 
            // txtSemcoWheelQty
            // 
            this.txtSemcoWheelQty.BackColor = System.Drawing.Color.White;
            this.txtSemcoWheelQty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSemcoWheelQty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtSemcoWheelQty.Location = new System.Drawing.Point(140, 30);
            this.txtSemcoWheelQty.Name = "txtSemcoWheelQty";
            this.txtSemcoWheelQty.Size = new System.Drawing.Size(50, 22);
            this.txtSemcoWheelQty.TabIndex = 1;
            this.txtSemcoWheelQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(8, 30);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(130, 21);
            this.label19.TabIndex = 6;
            this.label19.Text = "Semco Wheel Qty:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(8, 57);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(130, 21);
            this.label20.TabIndex = 8;
            this.label20.Text = "Semco Wheel PH:";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSemcoWheelPhase
            // 
            this.txtSemcoWheelPhase.BackColor = System.Drawing.Color.White;
            this.txtSemcoWheelPhase.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSemcoWheelPhase.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtSemcoWheelPhase.Location = new System.Drawing.Point(140, 57);
            this.txtSemcoWheelPhase.Name = "txtSemcoWheelPhase";
            this.txtSemcoWheelPhase.Size = new System.Drawing.Size(50, 22);
            this.txtSemcoWheelPhase.TabIndex = 2;
            this.txtSemcoWheelPhase.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(8, 85);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(130, 21);
            this.label21.TabIndex = 10;
            this.label21.Text = "Semco Wheel FLA:";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSemcoWheelFLA
            // 
            this.txtSemcoWheelFLA.BackColor = System.Drawing.Color.White;
            this.txtSemcoWheelFLA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSemcoWheelFLA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtSemcoWheelFLA.Location = new System.Drawing.Point(140, 85);
            this.txtSemcoWheelFLA.Name = "txtSemcoWheelFLA";
            this.txtSemcoWheelFLA.Size = new System.Drawing.Size(50, 22);
            this.txtSemcoWheelFLA.TabIndex = 3;
            this.txtSemcoWheelFLA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(8, 112);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(130, 21);
            this.label22.TabIndex = 12;
            this.label22.Text = "Semco Wheel HP:";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSemcoWheelHP
            // 
            this.txtSemcoWheelHP.BackColor = System.Drawing.Color.White;
            this.txtSemcoWheelHP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSemcoWheelHP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtSemcoWheelHP.Location = new System.Drawing.Point(140, 112);
            this.txtSemcoWheelHP.Name = "txtSemcoWheelHP";
            this.txtSemcoWheelHP.Size = new System.Drawing.Size(50, 22);
            this.txtSemcoWheelHP.TabIndex = 4;
            this.txtSemcoWheelHP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // gbExhaustFan
            // 
            this.gbExhaustFan.Controls.Add(this.txtExhaustFanQty);
            this.gbExhaustFan.Controls.Add(this.label23);
            this.gbExhaustFan.Controls.Add(this.label24);
            this.gbExhaustFan.Controls.Add(this.txtExhaustFanPhase);
            this.gbExhaustFan.Controls.Add(this.label25);
            this.gbExhaustFan.Controls.Add(this.txtExhaustFanFLA);
            this.gbExhaustFan.Controls.Add(this.label26);
            this.gbExhaustFan.Controls.Add(this.txtExhaustFanHP);
            this.gbExhaustFan.Location = new System.Drawing.Point(16, 33);
            this.gbExhaustFan.Name = "gbExhaustFan";
            this.gbExhaustFan.Size = new System.Drawing.Size(200, 154);
            this.gbExhaustFan.TabIndex = 36;
            this.gbExhaustFan.TabStop = false;
            this.gbExhaustFan.Text = "Exhaust Fan";
            // 
            // txtExhaustFanQty
            // 
            this.txtExhaustFanQty.BackColor = System.Drawing.Color.White;
            this.txtExhaustFanQty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExhaustFanQty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtExhaustFanQty.Location = new System.Drawing.Point(140, 30);
            this.txtExhaustFanQty.Name = "txtExhaustFanQty";
            this.txtExhaustFanQty.Size = new System.Drawing.Size(50, 22);
            this.txtExhaustFanQty.TabIndex = 1;
            this.txtExhaustFanQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(8, 30);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(130, 21);
            this.label23.TabIndex = 6;
            this.label23.Text = "Exhaust Fan Qty:";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(8, 57);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(130, 21);
            this.label24.TabIndex = 8;
            this.label24.Text = "Exhaust Fan PH:";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtExhaustFanPhase
            // 
            this.txtExhaustFanPhase.BackColor = System.Drawing.Color.White;
            this.txtExhaustFanPhase.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExhaustFanPhase.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtExhaustFanPhase.Location = new System.Drawing.Point(140, 57);
            this.txtExhaustFanPhase.Name = "txtExhaustFanPhase";
            this.txtExhaustFanPhase.Size = new System.Drawing.Size(50, 22);
            this.txtExhaustFanPhase.TabIndex = 2;
            this.txtExhaustFanPhase.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(8, 85);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(130, 21);
            this.label25.TabIndex = 10;
            this.label25.Text = "ExhaustFan FLA:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtExhaustFanFLA
            // 
            this.txtExhaustFanFLA.BackColor = System.Drawing.Color.White;
            this.txtExhaustFanFLA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExhaustFanFLA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtExhaustFanFLA.Location = new System.Drawing.Point(140, 85);
            this.txtExhaustFanFLA.Name = "txtExhaustFanFLA";
            this.txtExhaustFanFLA.Size = new System.Drawing.Size(50, 22);
            this.txtExhaustFanFLA.TabIndex = 3;
            this.txtExhaustFanFLA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(8, 112);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(130, 21);
            this.label26.TabIndex = 12;
            this.label26.Text = "Exhaust Fan HP:";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtExhaustFanHP
            // 
            this.txtExhaustFanHP.BackColor = System.Drawing.Color.White;
            this.txtExhaustFanHP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExhaustFanHP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtExhaustFanHP.Location = new System.Drawing.Point(140, 112);
            this.txtExhaustFanHP.Name = "txtExhaustFanHP";
            this.txtExhaustFanHP.Size = new System.Drawing.Size(50, 22);
            this.txtExhaustFanHP.TabIndex = 4;
            this.txtExhaustFanHP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBoxNewRRULineInfo
            // 
            this.groupBoxNewRRULineInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewRRULineInfo.Controls.Add(this.txtHeatType);
            this.groupBoxNewRRULineInfo.Controls.Add(this.txtModelNo);
            this.groupBoxNewRRULineInfo.Controls.Add(this.labelNewRRUHeatingType);
            this.groupBoxNewRRULineInfo.Controls.Add(this.labelNewRRUPartNum);
            this.groupBoxNewRRULineInfo.Controls.Add(this.txtPartNum);
            this.groupBoxNewRRULineInfo.Controls.Add(this.txtVerifiedBy);
            this.groupBoxNewRRULineInfo.Controls.Add(this.labelMewRRUCreatedBy);
            this.groupBoxNewRRULineInfo.Controls.Add(this.labelNewRRUSerialNo);
            this.groupBoxNewRRULineInfo.Controls.Add(this.txtSerialNo);
            this.groupBoxNewRRULineInfo.Controls.Add(this.labelNewRRUModelNum);
            this.groupBoxNewRRULineInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewRRULineInfo.Location = new System.Drawing.Point(6, 131);
            this.groupBoxNewRRULineInfo.Name = "groupBoxNewRRULineInfo";
            this.groupBoxNewRRULineInfo.Size = new System.Drawing.Size(410, 145);
            this.groupBoxNewRRULineInfo.TabIndex = 1;
            this.groupBoxNewRRULineInfo.TabStop = false;
            this.groupBoxNewRRULineInfo.Text = "Unit Info";
            // 
            // txtHeatType
            // 
            this.txtHeatType.BackColor = System.Drawing.Color.White;
            this.txtHeatType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHeatType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtHeatType.Location = new System.Drawing.Point(97, 100);
            this.txtHeatType.Name = "txtHeatType";
            this.txtHeatType.Size = new System.Drawing.Size(306, 22);
            this.txtHeatType.TabIndex = 69;
            // 
            // txtModelNo
            // 
            this.txtModelNo.BackColor = System.Drawing.Color.White;
            this.txtModelNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtModelNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtModelNo.Location = new System.Drawing.Point(98, 19);
            this.txtModelNo.Name = "txtModelNo";
            this.txtModelNo.Size = new System.Drawing.Size(306, 22);
            this.txtModelNo.TabIndex = 68;
            // 
            // labelNewRRUHeatingType
            // 
            this.labelNewRRUHeatingType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUHeatingType.Location = new System.Drawing.Point(7, 101);
            this.labelNewRRUHeatingType.Name = "labelNewRRUHeatingType";
            this.labelNewRRUHeatingType.Size = new System.Drawing.Size(88, 21);
            this.labelNewRRUHeatingType.TabIndex = 67;
            this.labelNewRRUHeatingType.Text = "Heating Type:";
            this.labelNewRRUHeatingType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelNewRRUPartNum
            // 
            this.labelNewRRUPartNum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUPartNum.Location = new System.Drawing.Point(20, 73);
            this.labelNewRRUPartNum.Name = "labelNewRRUPartNum";
            this.labelNewRRUPartNum.Size = new System.Drawing.Size(75, 21);
            this.labelNewRRUPartNum.TabIndex = 65;
            this.labelNewRRUPartNum.Text = "Part Num:";
            this.labelNewRRUPartNum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPartNum
            // 
            this.txtPartNum.BackColor = System.Drawing.Color.White;
            this.txtPartNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPartNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtPartNum.Location = new System.Drawing.Point(98, 73);
            this.txtPartNum.Name = "txtPartNum";
            this.txtPartNum.Size = new System.Drawing.Size(142, 22);
            this.txtPartNum.TabIndex = 3;
            // 
            // txtVerifiedBy
            // 
            this.txtVerifiedBy.BackColor = System.Drawing.Color.White;
            this.txtVerifiedBy.Enabled = false;
            this.txtVerifiedBy.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVerifiedBy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtVerifiedBy.Location = new System.Drawing.Point(274, 73);
            this.txtVerifiedBy.Name = "txtVerifiedBy";
            this.txtVerifiedBy.Size = new System.Drawing.Size(129, 22);
            this.txtVerifiedBy.TabIndex = 4;
            this.txtVerifiedBy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtVerifiedBy.Visible = false;
            // 
            // labelMewRRUCreatedBy
            // 
            this.labelMewRRUCreatedBy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMewRRUCreatedBy.Location = new System.Drawing.Point(279, 48);
            this.labelMewRRUCreatedBy.Name = "labelMewRRUCreatedBy";
            this.labelMewRRUCreatedBy.Size = new System.Drawing.Size(75, 20);
            this.labelMewRRUCreatedBy.TabIndex = 12;
            this.labelMewRRUCreatedBy.Text = "Created By:";
            this.labelMewRRUCreatedBy.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelMewRRUCreatedBy.Visible = false;
            // 
            // labelNewRRUSerialNo
            // 
            this.labelNewRRUSerialNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUSerialNo.Location = new System.Drawing.Point(20, 46);
            this.labelNewRRUSerialNo.Name = "labelNewRRUSerialNo";
            this.labelNewRRUSerialNo.Size = new System.Drawing.Size(75, 21);
            this.labelNewRRUSerialNo.TabIndex = 7;
            this.labelNewRRUSerialNo.Text = "Serial No:";
            this.labelNewRRUSerialNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSerialNo
            // 
            this.txtSerialNo.BackColor = System.Drawing.Color.White;
            this.txtSerialNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSerialNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtSerialNo.Location = new System.Drawing.Point(98, 46);
            this.txtSerialNo.Name = "txtSerialNo";
            this.txtSerialNo.Size = new System.Drawing.Size(142, 22);
            this.txtSerialNo.TabIndex = 2;
            // 
            // labelNewRRUModelNum
            // 
            this.labelNewRRUModelNum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUModelNum.Location = new System.Drawing.Point(20, 19);
            this.labelNewRRUModelNum.Name = "labelNewRRUModelNum";
            this.labelNewRRUModelNum.Size = new System.Drawing.Size(75, 21);
            this.labelNewRRUModelNum.TabIndex = 4;
            this.labelNewRRUModelNum.Text = "Model No:";
            this.labelNewRRUModelNum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxNewRRUJobInfo
            // 
            this.groupBoxNewRRUJobInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewRRUJobInfo.Controls.Add(this.txtJobNum);
            this.groupBoxNewRRUJobInfo.Controls.Add(this.label2);
            this.groupBoxNewRRUJobInfo.Controls.Add(this.txtEndConsumer);
            this.groupBoxNewRRUJobInfo.Controls.Add(this.labelNewRRUEndConsumer);
            this.groupBoxNewRRUJobInfo.Controls.Add(this.txtReleaseNum);
            this.groupBoxNewRRUJobInfo.Controls.Add(this.labelNewRRUSlash);
            this.groupBoxNewRRUJobInfo.Controls.Add(this.txtCustomer);
            this.groupBoxNewRRUJobInfo.Controls.Add(this.txtOrderNum);
            this.groupBoxNewRRUJobInfo.Controls.Add(this.labelNewRRUCustomer);
            this.groupBoxNewRRUJobInfo.Controls.Add(this.labelNewRRUOrderNum);
            this.groupBoxNewRRUJobInfo.Controls.Add(this.txtLineItem);
            this.groupBoxNewRRUJobInfo.Controls.Add(this.labelNewRRULineItem);
            this.groupBoxNewRRUJobInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewRRUJobInfo.Location = new System.Drawing.Point(6, 17);
            this.groupBoxNewRRUJobInfo.Name = "groupBoxNewRRUJobInfo";
            this.groupBoxNewRRUJobInfo.Size = new System.Drawing.Size(410, 105);
            this.groupBoxNewRRUJobInfo.TabIndex = 0;
            this.groupBoxNewRRUJobInfo.TabStop = false;
            this.groupBoxNewRRUJobInfo.Text = "Order Info";
            // 
            // txtJobNum
            // 
            this.txtJobNum.BackColor = System.Drawing.Color.White;
            this.txtJobNum.Enabled = false;
            this.txtJobNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJobNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtJobNum.Location = new System.Drawing.Point(276, 72);
            this.txtJobNum.Name = "txtJobNum";
            this.txtJobNum.Size = new System.Drawing.Size(127, 22);
            this.txtJobNum.TabIndex = 6;
            this.txtJobNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(234, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 21);
            this.label2.TabIndex = 9;
            this.label2.Text = "Job #:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtEndConsumer
            // 
            this.txtEndConsumer.BackColor = System.Drawing.Color.White;
            this.txtEndConsumer.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEndConsumer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtEndConsumer.Location = new System.Drawing.Point(95, 72);
            this.txtEndConsumer.Name = "txtEndConsumer";
            this.txtEndConsumer.Size = new System.Drawing.Size(133, 22);
            this.txtEndConsumer.TabIndex = 5;
            // 
            // labelNewRRUEndConsumer
            // 
            this.labelNewRRUEndConsumer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUEndConsumer.Location = new System.Drawing.Point(-9, 73);
            this.labelNewRRUEndConsumer.Name = "labelNewRRUEndConsumer";
            this.labelNewRRUEndConsumer.Size = new System.Drawing.Size(104, 21);
            this.labelNewRRUEndConsumer.TabIndex = 7;
            this.labelNewRRUEndConsumer.Text = "End Consumer:";
            this.labelNewRRUEndConsumer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtReleaseNum
            // 
            this.txtReleaseNum.BackColor = System.Drawing.Color.White;
            this.txtReleaseNum.Enabled = false;
            this.txtReleaseNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReleaseNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtReleaseNum.Location = new System.Drawing.Point(284, 19);
            this.txtReleaseNum.Name = "txtReleaseNum";
            this.txtReleaseNum.Size = new System.Drawing.Size(21, 22);
            this.txtReleaseNum.TabIndex = 3;
            this.txtReleaseNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUSlash
            // 
            this.labelNewRRUSlash.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUSlash.Location = new System.Drawing.Point(270, 18);
            this.labelNewRRUSlash.Name = "labelNewRRUSlash";
            this.labelNewRRUSlash.Size = new System.Drawing.Size(15, 21);
            this.labelNewRRUSlash.TabIndex = 4;
            this.labelNewRRUSlash.Text = "/";
            this.labelNewRRUSlash.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCustomer
            // 
            this.txtCustomer.BackColor = System.Drawing.Color.White;
            this.txtCustomer.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtCustomer.Location = new System.Drawing.Point(95, 46);
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.Size = new System.Drawing.Size(309, 22);
            this.txtCustomer.TabIndex = 4;
            // 
            // txtOrderNum
            // 
            this.txtOrderNum.BackColor = System.Drawing.Color.White;
            this.txtOrderNum.Enabled = false;
            this.txtOrderNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrderNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtOrderNum.Location = new System.Drawing.Point(95, 19);
            this.txtOrderNum.Name = "txtOrderNum";
            this.txtOrderNum.Size = new System.Drawing.Size(50, 22);
            this.txtOrderNum.TabIndex = 1;
            this.txtOrderNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelNewRRUCustomer
            // 
            this.labelNewRRUCustomer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUCustomer.Location = new System.Drawing.Point(4, 46);
            this.labelNewRRUCustomer.Name = "labelNewRRUCustomer";
            this.labelNewRRUCustomer.Size = new System.Drawing.Size(85, 21);
            this.labelNewRRUCustomer.TabIndex = 2;
            this.labelNewRRUCustomer.Text = "Customer:";
            this.labelNewRRUCustomer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelNewRRUOrderNum
            // 
            this.labelNewRRUOrderNum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUOrderNum.Location = new System.Drawing.Point(4, 20);
            this.labelNewRRUOrderNum.Name = "labelNewRRUOrderNum";
            this.labelNewRRUOrderNum.Size = new System.Drawing.Size(85, 21);
            this.labelNewRRUOrderNum.TabIndex = 1;
            this.labelNewRRUOrderNum.Text = "Sales Order#:";
            this.labelNewRRUOrderNum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtLineItem
            // 
            this.txtLineItem.BackColor = System.Drawing.Color.White;
            this.txtLineItem.Enabled = false;
            this.txtLineItem.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLineItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtLineItem.Location = new System.Drawing.Point(246, 19);
            this.txtLineItem.Name = "txtLineItem";
            this.txtLineItem.Size = new System.Drawing.Size(21, 22);
            this.txtLineItem.TabIndex = 2;
            this.txtLineItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRULineItem
            // 
            this.labelNewRRULineItem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRULineItem.Location = new System.Drawing.Point(155, 19);
            this.labelNewRRULineItem.Name = "labelNewRRULineItem";
            this.labelNewRRULineItem.Size = new System.Drawing.Size(85, 21);
            this.labelNewRRULineItem.TabIndex = 2;
            this.labelNewRRULineItem.Text = "Line #/Rel #:";
            this.labelNewRRULineItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // frmNewLineMON
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(876, 539);
            this.Controls.Add(this.groupBoxNewRRU);
            this.Name = "frmNewLineMON";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmNewLineMON";
            this.groupBoxNewRRU.ResumeLayout(false);
            this.groupBoxNewRRUFridgeType.ResumeLayout(false);
            this.groupBoxNewRRUFridgeType.PerformLayout();
            this.groupBoxNewRRUElectricInfo.ResumeLayout(false);
            this.groupBoxNewRRUElectricInfo.PerformLayout();
            this.gbMonFanInfo.ResumeLayout(false);
            this.gbAirXchange.ResumeLayout(false);
            this.gbAirXchange.PerformLayout();
            this.gbSemcoWheel.ResumeLayout(false);
            this.gbSemcoWheel.PerformLayout();
            this.gbExhaustFan.ResumeLayout(false);
            this.gbExhaustFan.PerformLayout();
            this.groupBoxNewRRULineInfo.ResumeLayout(false);
            this.groupBoxNewRRULineInfo.PerformLayout();
            this.groupBoxNewRRUJobInfo.ResumeLayout(false);
            this.groupBoxNewRRUJobInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.GroupBox groupBoxNewRRU;
        private System.Windows.Forms.GroupBox groupBoxNewRRUFridgeType;
        public System.Windows.Forms.RadioButton radioButtonNewRRUR410A;
        public System.Windows.Forms.CheckBox cbMonFrench;
        public System.Windows.Forms.TextBox txtMaxOutAirTemp;
        public System.Windows.Forms.Label lbNewMONMsg;
        public System.Windows.Forms.Button btnPrint;
        public System.Windows.Forms.Button btnAccept;
        public System.Windows.Forms.Button buttonNewRRUCancel;
        private System.Windows.Forms.GroupBox gbMonFanInfo;
        private System.Windows.Forms.GroupBox groupBoxNewRRUElectricInfo;
        public System.Windows.Forms.TextBox txtFuelType;
        private System.Windows.Forms.Label labelNewRRUHeatingTypeInfo;
        public System.Windows.Forms.TextBox txtOperatingVolts;
        private System.Windows.Forms.Label labelNewRRUOperatingVolts;
        public System.Windows.Forms.TextBox txtElectricalRating;
        private System.Windows.Forms.Label labelNewRRUElectricalRating;
        public System.Windows.Forms.TextBox txtRawMOP;
        private System.Windows.Forms.Label lbRawMOP;
        public System.Windows.Forms.TextBox txtMFSMCB;
        private System.Windows.Forms.Label labelNewRRUMFSMCB;
        public System.Windows.Forms.TextBox txtMinCKTAmp;
        private System.Windows.Forms.Label labelNewRRUMinCKTAmp;
        public System.Windows.Forms.GroupBox groupBoxNewRRULineInfo;
        private System.Windows.Forms.Label labelNewRRUHeatingType;
        private System.Windows.Forms.Label labelNewRRUPartNum;
        public System.Windows.Forms.TextBox txtPartNum;
        public System.Windows.Forms.TextBox txtVerifiedBy;
        public System.Windows.Forms.Label labelMewRRUCreatedBy;
        private System.Windows.Forms.Label labelNewRRUSerialNo;
        public System.Windows.Forms.TextBox txtSerialNo;
        private System.Windows.Forms.Label labelNewRRUModelNum;
        private System.Windows.Forms.GroupBox groupBoxNewRRUJobInfo;
        public System.Windows.Forms.TextBox txtJobNum;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtEndConsumer;
        private System.Windows.Forms.Label labelNewRRUEndConsumer;
        public System.Windows.Forms.TextBox txtReleaseNum;
        private System.Windows.Forms.Label labelNewRRUSlash;
        public System.Windows.Forms.TextBox txtCustomer;
        public System.Windows.Forms.TextBox txtOrderNum;
        private System.Windows.Forms.Label labelNewRRUCustomer;
        private System.Windows.Forms.Label labelNewRRUOrderNum;
        public System.Windows.Forms.TextBox txtLineItem;
        private System.Windows.Forms.Label labelNewRRULineItem;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtModelNo;
        private System.Windows.Forms.GroupBox gbSemcoWheel;
        public System.Windows.Forms.TextBox txtSemcoWheelQty;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        public System.Windows.Forms.TextBox txtSemcoWheelPhase;
        private System.Windows.Forms.Label label21;
        public System.Windows.Forms.TextBox txtSemcoWheelFLA;
        private System.Windows.Forms.Label label22;
        public System.Windows.Forms.TextBox txtSemcoWheelHP;
        private System.Windows.Forms.GroupBox gbExhaustFan;
        public System.Windows.Forms.TextBox txtExhaustFanQty;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        public System.Windows.Forms.TextBox txtExhaustFanPhase;
        private System.Windows.Forms.Label label25;
        public System.Windows.Forms.TextBox txtExhaustFanFLA;
        private System.Windows.Forms.Label label26;
        public System.Windows.Forms.TextBox txtExhaustFanHP;
        private System.Windows.Forms.GroupBox gbAirXchange;
        public System.Windows.Forms.TextBox txtAirXchangeWheelQty;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        public System.Windows.Forms.TextBox txtAirXchangeWheelPhase;
        private System.Windows.Forms.Label label37;
        public System.Windows.Forms.TextBox txtAirXchangeWheelFLA;
        private System.Windows.Forms.Label label38;
        public System.Windows.Forms.TextBox txtAirXchangeWheelHP;
        public System.Windows.Forms.TextBox txtHeatType;
        private System.Windows.Forms.Label label39;
        public System.Windows.Forms.TextBox txtVoltage;
        private System.Windows.Forms.Label label40;
        public System.Windows.Forms.TextBox txtHeatingInputElectric;
    }
}