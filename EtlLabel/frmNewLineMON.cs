﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KCC.OA.Etl
{
    public partial class frmNewLineMON : Form
    {
        MainBO objMain = new MainBO();
        private string EpicorServer = "";

        public frmNewLineMON()
        {
            InitializeComponent();
        }       

        private void buttonNewRRUCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonNewRRUAccept_Click(object sender, EventArgs e)
        {
            bindDataToModel();

            if (btnAccept.Text == "Update")
            {                
                objMain.MonitorUpdateEtl();
            }
            else
            {
                objMain.MonitorInsertEtl();
            }
        }       

        private void bindDataToModel()
        {
            objMain.JobNum = txtJobNum.Text;
            objMain.OrderNum = this.txtOrderNum.Text;
            objMain.OrderLine =  this.txtLineItem.Text;
            objMain.ReleaseNum = this.txtReleaseNum.Text;
            objMain.Customer = this.txtCustomer.Text;
            objMain.EndConsumerName = this.txtEndConsumer.Text;

            objMain.ModelNo = this.txtModelNo.Text;
            objMain.SerialNo = this.txtSerialNo.Text;

            objMain.ModelNoVerifiedBy = this.txtVerifiedBy.Text;
            objMain.HeatingType = this.txtHeatType.Text;
            objMain.MinCKTAmp = this.txtMinCKTAmp.Text;
            objMain.MFSMCB = this.txtMFSMCB.Text;
            objMain.RawMOP = this.txtRawMOP.Text;
            objMain.ElecRating = this.txtElectricalRating.Text;
            objMain.OperVoltage = this.txtOperatingVolts.Text;
            objMain.FuelType = this.txtFuelType.Text;
            objMain.MaxOutAirTemp = this.txtMaxOutAirTemp.Text;
            objMain.ModelNo = this.txtModelNo.Text;
            objMain.MfgDate = DateTime.Now.ToShortDateString();
            objMain.Voltage = this.txtVoltage.Text;
            objMain.HeatingInputElectric = this.txtHeatingInputElectric.Text;
            //objMain.Circuit1Charge = this.txtCircuit1Charge.Text;
            //objMain.Circuit2Charge = this.txtCircuit2Charge.Text;

            //objMain.DD_SupplyFanStdQty = this.txtDD_SupplyFanStdQty.Text;
            //objMain.DD_SupplyFanStdPhase = this.txtDD_SupplyFanStdPhase.Text;
            //objMain.DD_SupplyFanStdFLA = this.txtDD_SupplyFanStdFLA.Text;
            //objMain.DD_SupplyFanStdHP = this.txtDD_SupplyFanStdHP.Text;

            //objMain.DD_SupplyFanOvrQty = this.txtDD_SupplyFanOvrQty.Text;
            //objMain.DD_SupplyFanOvrPhase = this.txtDD_SupplyFanOvrPhase.Text;
            //objMain.DD_SupplyFanOvrFLA = this.txtDD_SupplyFanOvrFLA.Text;
            //objMain.DD_SupplyFanOvrHP = this.txtDD_SupplyFanOvrHP.Text;

            //objMain.BeltSupplyFanStdQty = this.txtBelt_SupplyFanStdQty.Text;
            //objMain.BeltSupplyFanStdPhase = this.txtBelt_SupplyFanStdPhase.Text;
            //objMain.BeltSupplyFanStdFLA = this.txtBelt_SupplyFanStdFLA.Text;
            //objMain.BeltSupplyFanStdHP = this.txtBelt_SupplyFanStdHP.Text;

            //objMain.BeltSupplyFanOvrQty = this.txtBeltSupplyFanOvrQty.Text;
            //objMain.BeltSupplyFanOvrPhase = this.txtBeltSupplyFanOvrPhase.Text;
            //objMain.BeltSupplyFanOvrFLA = this.txtBeltSupplyFanOvrFLA.Text;
            //objMain.BeltSupplyFanOvrHP = this.txtBeltSupplyFanOvrHP.Text;

            //objMain.CondenserFanStdQty = this.txtCondFanStdQty.Text;
            //objMain.CondenserFanStdPhase = this.txtCondFanStdPhase.Text;
            //objMain.CondenserFanStdFLA = this.txtCondFanStdFLA.Text;
            //objMain.CondenserFanStdHP = this.txtCondFanStdHP.Text;

            //objMain.CondenserFanHighQty= this.txtCondFanHighQty.Text;
            //objMain.CondenserFanHighPhase = this.txtCondFanHighPhase.Text;
            //objMain.CondenserFanHighFLA = this.txtCondFanHighFLA.Text;
            //objMain.CondenserFanHighHP = this.txtCondFanHighHP.Text;

            objMain.ExhaustFanQty = this.txtExhaustFanQty.Text;
            objMain.ExhaustFanPhase = this.txtExhaustFanPhase.Text;
            objMain.ExhaustFanFLA = this.txtExhaustFanFLA.Text;
            objMain.ExhaustFanHP = this.txtExhaustFanHP.Text;

            objMain.SemcoWheelQty = this.txtSemcoWheelQty.Text;
            objMain.SemcoWheelPhase = this.txtSemcoWheelPhase.Text;
            objMain.SemcoWheelFLA = this.txtSemcoWheelFLA.Text;
            objMain.SemcoWheelHP = this.txtSemcoWheelHP.Text;

            objMain.AirXchangeWheelQty = this.txtAirXchangeWheelQty.Text;
            objMain.AirXchangeWheelPhase = this.txtAirXchangeWheelPhase.Text;
            objMain.AirXchangeWheelFLA = this.txtAirXchangeWheelFLA.Text;
            objMain.AirXchangeWheelHP = this.txtAirXchangeWheelHP.Text;    
       
            if (cbMonFrench.Checked == true)
            {
                objMain.French = "French";
            }
            else
            {
                objMain.French = "False";
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            string jobNumStr = this.txtJobNum.Text;
            string supplyFan1Qty = "";
            string supplyFan1Phase = "";
            string supplyFan1FLA = "";
            string supplyFan1HP = "";
            string supplyFan2Qty = "";
            string supplyFan2Phase = "";
            string supplyFan2FLA = "";
            string supplyFan2HP = "";
            string condFanQty = "";
            string condFanPhase = "";
            string condFanFLA = "";
            string condFanHP = "";
            string exhaustFanQty = "";
            string exhaustFanPhase = "";
            string exhaustFanFLA = "";
            string exhaustFanHP = "";
            string semcoWheelQty = "";
            string semcoWheelPhase = "";
            string semcoWheelFLA = "";
            string semcoWheelHP = "";
            string airXchangeWheelQty = "";
            string airXchangeWheelPhase = "";
            string airXchangeWheelFLA = "";
            string airXchangeWheelHP = "";
            string fuelType = "";
            string maxAirOutTemp = "";
            string heatingType = "";
            string circuit1Charge = "";
            string circuit2Charge = "";
            string voltage = "";

            frmPrintRRU frmPrint = new frmPrintRRU();
            frmPrint.Text = "Monitor Print ETL Label";
  
            ReportDocument cryRpt = new ReportDocument();
            
            voltage = this.txtElectricalRating.Text.Substring(0, 3);

            //if (this.txtDD_SupplyFanStdQty.Text != "NA")
            //{
            //    supplyFan1Qty = this.txtDD_SupplyFanStdQty.Text;
            //    supplyFan1Phase = this.txtDD_SupplyFanStdPhase.Text;
            //    supplyFan1FLA = this.txtDD_SupplyFanStdFLA.Text + " - " + voltage;
            //    supplyFan1HP = this.txtDD_SupplyFanStdHP.Text;
            //}

            //if (this.txtDD_SupplyFanOvrQty.Text != "NA")
            //{
            //    if (supplyFan1Qty == "")
            //    {
            //        supplyFan1Qty = this.txtDD_SupplyFanOvrQty.Text;
            //        supplyFan1Phase = this.txtDD_SupplyFanOvrPhase.Text;
            //        supplyFan1FLA = this.txtDD_SupplyFanOvrFLA.Text + " - " + voltage;
            //        supplyFan1HP = this.txtDD_SupplyFanOvrHP.Text;
            //    }
            //    else
            //    {
            //        supplyFan2Qty = this.txtDD_SupplyFanOvrQty.Text;
            //        supplyFan2Phase = this.txtDD_SupplyFanOvrPhase.Text;
            //        supplyFan2FLA = this.txtDD_SupplyFanOvrFLA.Text + " - " + voltage;
            //        supplyFan2HP = this.txtDD_SupplyFanOvrHP.Text;
            //    }
            //}

            //if (this.txtBelt_SupplyFanStdQty.Text != "NA")
            //{
            //    if (supplyFan1Qty == "")
            //    {
            //        supplyFan1Qty = this.txtBelt_SupplyFanStdQty.Text;
            //        supplyFan1Phase = this.txtBelt_SupplyFanStdPhase.Text;
            //        supplyFan1FLA = this.txtBelt_SupplyFanStdFLA.Text + " - " + voltage;
            //        supplyFan1HP = this.txtBelt_SupplyFanStdHP.Text;
            //    }
            //    else
            //    {
            //        supplyFan2Qty = this.txtBelt_SupplyFanStdQty.Text;
            //        supplyFan2Phase = this.txtBelt_SupplyFanStdPhase.Text;
            //        supplyFan2FLA = this.txtBelt_SupplyFanStdFLA.Text + " - " + voltage;
            //        supplyFan2HP = this.txtBelt_SupplyFanStdHP.Text;
            //    }
            //}

            //if (this.txtBeltSupplyFanOvrQty.Text != "NA")
            //{
            //    if (supplyFan1Qty == "")
            //    {
            //        supplyFan1Qty = this.txtBeltSupplyFanOvrQty.Text;
            //        supplyFan1Phase = this.txtBeltSupplyFanOvrPhase.Text;
            //        supplyFan1FLA = this.txtBeltSupplyFanOvrFLA.Text + " - " + voltage;
            //        supplyFan1HP = this.txtBeltSupplyFanOvrHP.Text;
            //    }
            //    else
            //    {
            //        supplyFan2Qty = this.txtBeltSupplyFanOvrQty.Text;
            //        supplyFan2Phase = this.txtBeltSupplyFanOvrPhase.Text;
            //        supplyFan2FLA = this.txtBeltSupplyFanOvrFLA.Text + " - " + voltage;
            //        supplyFan2HP = this.txtBeltSupplyFanOvrHP.Text;
            //    }
            //}

            //if (this.txtCondFanStdQty.Text != "NA")
            //{
            //    condFanQty = this.txtCondFanStdQty.Text;
            //    condFanPhase = this.txtCondFanStdPhase.Text;
            //    condFanFLA = this.txtCondFanStdFLA.Text + " - " + voltage;
            //    condFanHP = this.txtCondFanStdHP.Text;
            //}

            //if (this.txtCondFanHighQty.Text != "NA")
            //{
            //    condFanQty = this.txtCondFanHighQty.Text;
            //    condFanPhase = this.txtCondFanHighPhase.Text;
            //    condFanFLA = this.txtCondFanHighFLA.Text + " - " + voltage;
            //    condFanHP = this.txtCondFanHighHP.Text;
            //}

            if (this.txtExhaustFanQty.Text != "NA")
            {
                exhaustFanQty = this.txtExhaustFanQty.Text;
                exhaustFanPhase = this.txtExhaustFanPhase.Text;
                exhaustFanFLA = this.txtExhaustFanFLA.Text + " - " + voltage;
                exhaustFanHP = this.txtExhaustFanHP.Text;
            }

            if (this.txtSemcoWheelQty.Text != "NA")
            {
                semcoWheelQty = this.txtSemcoWheelQty.Text;
                semcoWheelPhase = this.txtSemcoWheelPhase.Text;
                semcoWheelFLA = this.txtSemcoWheelFLA.Text + " - " + voltage;
                semcoWheelHP = this.txtSemcoWheelHP.Text;
            }

            if (this.txtAirXchangeWheelQty.Text != "NA")
            {
                airXchangeWheelQty = this.txtAirXchangeWheelQty.Text;
                airXchangeWheelPhase = this.txtAirXchangeWheelPhase.Text;
                airXchangeWheelFLA = this.txtAirXchangeWheelFLA.Text + " - " + voltage;
                airXchangeWheelHP = this.txtAirXchangeWheelHP.Text;
            }

            VS_DataSet ds = new VS_DataSet();
            ds.MONReportDataTable.Rows.Add(jobNumStr,
                                           DateTime.Now.ToShortDateString(),
                                           this.txtModelNo.Text,
                                           this.txtSerialNo.Text,
                                           this.txtElectricalRating.Text,                                           
                                           this.txtHeatType.Text,
                                           this.txtHeatingInputElectric.Text,
                                           this.txtOperatingVolts.Text,
                                           this.txtMinCKTAmp.Text,
                                           this.txtMFSMCB.Text,
                                           supplyFan1Qty,
                                           supplyFan1Phase,
                                           supplyFan1FLA,
                                           supplyFan1HP,
                                           supplyFan2Qty,
                                           supplyFan2Phase,
                                           supplyFan2FLA,
                                           supplyFan2HP,
                                           condFanQty,
                                           condFanPhase,
                                           condFanFLA,
                                           condFanHP,
                                           exhaustFanQty,
                                           exhaustFanPhase,
                                           exhaustFanFLA,
                                           exhaustFanHP,
                                           semcoWheelQty,
                                           semcoWheelPhase,
                                           semcoWheelFLA,
                                           semcoWheelHP,
                                           airXchangeWheelQty,
                                           airXchangeWheelPhase,
                                           airXchangeWheelFLA,
                                           airXchangeWheelHP,
                                           fuelType,
                                           this.txtMaxOutAirTemp.Text,
                                           circuit1Charge,
                                           circuit2Charge
                                           );
            try
            {
                EpicorServer = ConfigurationManager.AppSettings["EpicorAppServer"];

                string reportString = @"\\" + EpicorServer + "\\Apps\\OAU\\EtlLabel\\reports\\MonitorEtlLabel.rpt";
                //string reportString = @"\\EPICOR905APP\Apps\OAU\EtlLabel\reports\MonitorEtlLabel.rpt"; 
               
#if DEBUG                              
                //cryRpt.Load(@"C:\Users\tonyt\Documents\Visual Studio 2013\Projects\EtlLabel\EtlLabel\reports\MonitorEtlLabel.rpt");               
                reportString = @"\\KCCWVTEPIC9APP2\Apps\OAU\EtlLabel\reports\MonitorEtlLabel.rpt"; 
#endif

                
                cryRpt.Load(reportString);
                cryRpt.SetDataSource(ds.Tables["MONReportDataTable"]);

                frmPrint.crystalReportViewer1.ReportSource = cryRpt;
                frmPrint.ShowDialog();
                frmPrint.crystalReportViewer1.Refresh();

                if (cbMonFrench.Checked == true)
                {
                    reportString = @"\\" + EpicorServer + "\\Apps\\OAU\\EtlLabel\\reports\\MonitorEtlLabel_French.rpt";
#if DEBUG            
                    reportString = @"\\KCCWVTEPIC9APP2\Apps\OAU\EtlLabel\reports\MonitorEtlLabel_French.rpt";
#endif
                    
                    cryRpt.Load(reportString);
                    cryRpt.SetDataSource(ds.Tables["MONReportDataTable"]);

                    frmPrint.crystalReportViewer1.ReportSource = cryRpt;

                    frmPrint.ShowDialog();
                    frmPrint.crystalReportViewer1.Refresh();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR - Displaying Monitor ETL Label - " + ex);
            }
            
        }
    }
}
