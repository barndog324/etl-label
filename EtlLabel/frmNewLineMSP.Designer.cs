﻿namespace KCC.OA.Etl
{
    partial class frmNewLineMSP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStripNewMSP = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBoxNewMSPJobInfo = new System.Windows.Forms.GroupBox();
            this.label31 = new System.Windows.Forms.Label();
            this.tbNewMSP_UnitType = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbNewMSPJobNum = new System.Windows.Forms.TextBox();
            this.tbNewMSPEndConsumer = new System.Windows.Forms.TextBox();
            this.labelNewFANEndConsumer = new System.Windows.Forms.Label();
            this.tbNewMSPReleaseNum = new System.Windows.Forms.TextBox();
            this.labelNewFANSlash = new System.Windows.Forms.Label();
            this.tbNewMSPCustomer = new System.Windows.Forms.TextBox();
            this.tbNewMSPOrderNum = new System.Windows.Forms.TextBox();
            this.labelNewFANCustomer = new System.Windows.Forms.Label();
            this.labelNewFANOrderNum = new System.Windows.Forms.Label();
            this.tbNewMSPLineItem = new System.Windows.Forms.TextBox();
            this.labelNewFANLineItem = new System.Windows.Forms.Label();
            this.btnNewMSPPrint = new System.Windows.Forms.Button();
            this.btnNewMSPAccept = new System.Windows.Forms.Button();
            this.btnNewMSPCancel = new System.Windows.Forms.Button();
            this.cbNewMSPFrench = new System.Windows.Forms.CheckBox();
            this.groupBoxNewMSPUnitInfo = new System.Windows.Forms.GroupBox();
            this.tbNewMSP_MfgDate = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.tbNewMSPRefrigerant = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbNewMSPFanFilter = new System.Windows.Forms.TextBox();
            this.labelNewFANPartNum = new System.Windows.Forms.Label();
            this.tbNewMSPDesignation = new System.Windows.Forms.TextBox();
            this.tbNewMSPPartNum = new System.Windows.Forms.TextBox();
            this.tbNewMSPVerifiedBy = new System.Windows.Forms.TextBox();
            this.labelMewFANVerifiedBy = new System.Windows.Forms.Label();
            this.labelNewMSPFiltere = new System.Windows.Forms.Label();
            this.labelNewFANSerialNo = new System.Windows.Forms.Label();
            this.tbNewMSPSerialNo = new System.Windows.Forms.TextBox();
            this.tbNewMSPModelNo = new System.Windows.Forms.TextBox();
            this.labelNewFANModelNum = new System.Windows.Forms.Label();
            this.groupBoxNewFANVoltageInfo = new System.Windows.Forms.GroupBox();
            this.tbNewMSPFanTotalSP = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.tbNewMSPFanQty = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.tbNewMSPFanExternalSP = new System.Windows.Forms.TextBox();
            this.labelNewMSPFanExternalSP = new System.Windows.Forms.Label();
            this.tbNewMSPFanAirVolume = new System.Windows.Forms.TextBox();
            this.labelNewMSPAirVolume = new System.Windows.Forms.Label();
            this.tbNewMSPFanVoltageDraw = new System.Windows.Forms.TextBox();
            this.lbNewFAN_VoltDraw = new System.Windows.Forms.Label();
            this.tbNewMSP_Power = new System.Windows.Forms.TextBox();
            this.labelNewFANElecVolts = new System.Windows.Forms.Label();
            this.lbNewMSPMsg = new System.Windows.Forms.Label();
            this.gbNewMSPDehumidifier = new System.Windows.Forms.GroupBox();
            this.tbNewMSPDehumidAmbient = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbNewMSPDehumidCapacity = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.tbNewMSPDehumidLAT = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbNewMSPDehumidEAT = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbNewMSPDehumidGlycol = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbNewMSPDehumidPD = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbNewMSPDehumidGPM_SST = new System.Windows.Forms.TextBox();
            this.lbDehumidGPM_SST = new System.Windows.Forms.Label();
            this.tbNewMSPDehumidLWT = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbNewMSPDehumidEWT = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbNewMSPDehumidBTUH = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.gbNewMSPPostCooling = new System.Windows.Forms.GroupBox();
            this.tbNewMSPPostCoolingLAT = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbNewMSPPostCoolingEAT = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tbNewMSPPostCoolingGlycol = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tbNewMSPPostCoolingPD = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tbNewMSPPostCoolingGPM_SST = new System.Windows.Forms.TextBox();
            this.lbPostCoolingGPM_SST = new System.Windows.Forms.Label();
            this.tbNewMSPPostCoolingLWT = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tbNewMSPPostCoolingEWT = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tbNewMSPPostCoolingBTUH = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.gbNewMSPPostHeating = new System.Windows.Forms.GroupBox();
            this.tbNewMSPPostHeatingLAT = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tbNewMSPPostHeatingEAT = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tbNewMSPPostHeatingGlycol = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.tbNewMSPPostHeatingPD = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.tbNewMSPPostHeatingGPM = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tbNewMSPPostHeatingLWT = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tbNewMSPPostHeatingEWT = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tbNewMSPPostHeatingBTUH = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.menuStripNewMSP.SuspendLayout();
            this.groupBoxNewMSPJobInfo.SuspendLayout();
            this.groupBoxNewMSPUnitInfo.SuspendLayout();
            this.groupBoxNewFANVoltageInfo.SuspendLayout();
            this.gbNewMSPDehumidifier.SuspendLayout();
            this.gbNewMSPPostCooling.SuspendLayout();
            this.gbNewMSPPostHeating.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStripNewMSP
            // 
            this.menuStripNewMSP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.menuStripNewMSP.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStripNewMSP.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStripNewMSP.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStripNewMSP.Location = new System.Drawing.Point(0, 0);
            this.menuStripNewMSP.Name = "menuStripNewMSP";
            this.menuStripNewMSP.Size = new System.Drawing.Size(844, 24);
            this.menuStripNewMSP.TabIndex = 3;
            this.menuStripNewMSP.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(99, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // groupBoxNewMSPJobInfo
            // 
            this.groupBoxNewMSPJobInfo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBoxNewMSPJobInfo.Controls.Add(this.label31);
            this.groupBoxNewMSPJobInfo.Controls.Add(this.tbNewMSP_UnitType);
            this.groupBoxNewMSPJobInfo.Controls.Add(this.label1);
            this.groupBoxNewMSPJobInfo.Controls.Add(this.tbNewMSPJobNum);
            this.groupBoxNewMSPJobInfo.Controls.Add(this.tbNewMSPEndConsumer);
            this.groupBoxNewMSPJobInfo.Controls.Add(this.labelNewFANEndConsumer);
            this.groupBoxNewMSPJobInfo.Controls.Add(this.tbNewMSPReleaseNum);
            this.groupBoxNewMSPJobInfo.Controls.Add(this.labelNewFANSlash);
            this.groupBoxNewMSPJobInfo.Controls.Add(this.tbNewMSPCustomer);
            this.groupBoxNewMSPJobInfo.Controls.Add(this.tbNewMSPOrderNum);
            this.groupBoxNewMSPJobInfo.Controls.Add(this.labelNewFANCustomer);
            this.groupBoxNewMSPJobInfo.Controls.Add(this.labelNewFANOrderNum);
            this.groupBoxNewMSPJobInfo.Controls.Add(this.tbNewMSPLineItem);
            this.groupBoxNewMSPJobInfo.Controls.Add(this.labelNewFANLineItem);
            this.groupBoxNewMSPJobInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewMSPJobInfo.Location = new System.Drawing.Point(5, 27);
            this.groupBoxNewMSPJobInfo.Name = "groupBoxNewMSPJobInfo";
            this.groupBoxNewMSPJobInfo.Size = new System.Drawing.Size(443, 145);
            this.groupBoxNewMSPJobInfo.TabIndex = 4;
            this.groupBoxNewMSPJobInfo.TabStop = false;
            this.groupBoxNewMSPJobInfo.Text = "Order Info";
            // 
            // label31
            // 
            this.label31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(240, 21);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(91, 21);
            this.label31.TabIndex = 29;
            this.label31.Text = "Unit Type:";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSP_UnitType
            // 
            this.tbNewMSP_UnitType.BackColor = System.Drawing.Color.White;
            this.tbNewMSP_UnitType.Enabled = false;
            this.tbNewMSP_UnitType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSP_UnitType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSP_UnitType.Location = new System.Drawing.Point(334, 21);
            this.tbNewMSP_UnitType.Name = "tbNewMSP_UnitType";
            this.tbNewMSP_UnitType.Size = new System.Drawing.Size(86, 22);
            this.tbNewMSP_UnitType.TabIndex = 28;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 21);
            this.label1.TabIndex = 27;
            this.label1.Text = "Job #:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPJobNum
            // 
            this.tbNewMSPJobNum.BackColor = System.Drawing.Color.White;
            this.tbNewMSPJobNum.Enabled = false;
            this.tbNewMSPJobNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPJobNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPJobNum.Location = new System.Drawing.Point(110, 21);
            this.tbNewMSPJobNum.Name = "tbNewMSPJobNum";
            this.tbNewMSPJobNum.Size = new System.Drawing.Size(124, 22);
            this.tbNewMSPJobNum.TabIndex = 1;
            // 
            // tbNewMSPEndConsumer
            // 
            this.tbNewMSPEndConsumer.BackColor = System.Drawing.Color.White;
            this.tbNewMSPEndConsumer.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPEndConsumer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPEndConsumer.Location = new System.Drawing.Point(110, 104);
            this.tbNewMSPEndConsumer.Name = "tbNewMSPEndConsumer";
            this.tbNewMSPEndConsumer.Size = new System.Drawing.Size(313, 22);
            this.tbNewMSPEndConsumer.TabIndex = 6;
            // 
            // labelNewFANEndConsumer
            // 
            this.labelNewFANEndConsumer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANEndConsumer.Location = new System.Drawing.Point(4, 104);
            this.labelNewFANEndConsumer.Name = "labelNewFANEndConsumer";
            this.labelNewFANEndConsumer.Size = new System.Drawing.Size(104, 21);
            this.labelNewFANEndConsumer.TabIndex = 7;
            this.labelNewFANEndConsumer.Text = "End Consumer:";
            this.labelNewFANEndConsumer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPReleaseNum
            // 
            this.tbNewMSPReleaseNum.BackColor = System.Drawing.Color.White;
            this.tbNewMSPReleaseNum.Enabled = false;
            this.tbNewMSPReleaseNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPReleaseNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPReleaseNum.Location = new System.Drawing.Point(399, 50);
            this.tbNewMSPReleaseNum.Name = "tbNewMSPReleaseNum";
            this.tbNewMSPReleaseNum.Size = new System.Drawing.Size(21, 22);
            this.tbNewMSPReleaseNum.TabIndex = 4;
            this.tbNewMSPReleaseNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFANSlash
            // 
            this.labelNewFANSlash.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANSlash.Location = new System.Drawing.Point(385, 49);
            this.labelNewFANSlash.Name = "labelNewFANSlash";
            this.labelNewFANSlash.Size = new System.Drawing.Size(15, 21);
            this.labelNewFANSlash.TabIndex = 4;
            this.labelNewFANSlash.Text = "/";
            this.labelNewFANSlash.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbNewMSPCustomer
            // 
            this.tbNewMSPCustomer.BackColor = System.Drawing.Color.White;
            this.tbNewMSPCustomer.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPCustomer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPCustomer.Location = new System.Drawing.Point(110, 77);
            this.tbNewMSPCustomer.Name = "tbNewMSPCustomer";
            this.tbNewMSPCustomer.Size = new System.Drawing.Size(313, 22);
            this.tbNewMSPCustomer.TabIndex = 5;
            // 
            // tbNewMSPOrderNum
            // 
            this.tbNewMSPOrderNum.BackColor = System.Drawing.Color.White;
            this.tbNewMSPOrderNum.Enabled = false;
            this.tbNewMSPOrderNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPOrderNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPOrderNum.Location = new System.Drawing.Point(110, 50);
            this.tbNewMSPOrderNum.Name = "tbNewMSPOrderNum";
            this.tbNewMSPOrderNum.Size = new System.Drawing.Size(124, 22);
            this.tbNewMSPOrderNum.TabIndex = 2;
            this.tbNewMSPOrderNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelNewFANCustomer
            // 
            this.labelNewFANCustomer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANCustomer.Location = new System.Drawing.Point(4, 77);
            this.labelNewFANCustomer.Name = "labelNewFANCustomer";
            this.labelNewFANCustomer.Size = new System.Drawing.Size(104, 21);
            this.labelNewFANCustomer.TabIndex = 2;
            this.labelNewFANCustomer.Text = "Customer:";
            this.labelNewFANCustomer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelNewFANOrderNum
            // 
            this.labelNewFANOrderNum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANOrderNum.Location = new System.Drawing.Point(4, 51);
            this.labelNewFANOrderNum.Name = "labelNewFANOrderNum";
            this.labelNewFANOrderNum.Size = new System.Drawing.Size(104, 21);
            this.labelNewFANOrderNum.TabIndex = 1;
            this.labelNewFANOrderNum.Text = "Sales Order#:";
            this.labelNewFANOrderNum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPLineItem
            // 
            this.tbNewMSPLineItem.BackColor = System.Drawing.Color.White;
            this.tbNewMSPLineItem.Enabled = false;
            this.tbNewMSPLineItem.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPLineItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPLineItem.Location = new System.Drawing.Point(361, 50);
            this.tbNewMSPLineItem.Name = "tbNewMSPLineItem";
            this.tbNewMSPLineItem.Size = new System.Drawing.Size(21, 22);
            this.tbNewMSPLineItem.TabIndex = 3;
            this.tbNewMSPLineItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewFANLineItem
            // 
            this.labelNewFANLineItem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANLineItem.Location = new System.Drawing.Point(271, 50);
            this.labelNewFANLineItem.Name = "labelNewFANLineItem";
            this.labelNewFANLineItem.Size = new System.Drawing.Size(85, 21);
            this.labelNewFANLineItem.TabIndex = 2;
            this.labelNewFANLineItem.Text = "Line #/Rel #:";
            this.labelNewFANLineItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnNewMSPPrint
            // 
            this.btnNewMSPPrint.Enabled = false;
            this.btnNewMSPPrint.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewMSPPrint.Location = new System.Drawing.Point(733, 522);
            this.btnNewMSPPrint.Name = "btnNewMSPPrint";
            this.btnNewMSPPrint.Size = new System.Drawing.Size(95, 37);
            this.btnNewMSPPrint.TabIndex = 53;
            this.btnNewMSPPrint.Text = "Print";
            this.btnNewMSPPrint.UseVisualStyleBackColor = true;
            this.btnNewMSPPrint.Click += new System.EventHandler(this.buttonNewMSPPrint_Click);
            // 
            // btnNewMSPAccept
            // 
            this.btnNewMSPAccept.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewMSPAccept.Location = new System.Drawing.Point(467, 522);
            this.btnNewMSPAccept.Name = "btnNewMSPAccept";
            this.btnNewMSPAccept.Size = new System.Drawing.Size(95, 37);
            this.btnNewMSPAccept.TabIndex = 51;
            this.btnNewMSPAccept.Text = "Accept";
            this.btnNewMSPAccept.UseVisualStyleBackColor = true;
            this.btnNewMSPAccept.Click += new System.EventHandler(this.buttonNewMSPAccept_Click);
            // 
            // btnNewMSPCancel
            // 
            this.btnNewMSPCancel.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewMSPCancel.ForeColor = System.Drawing.Color.Red;
            this.btnNewMSPCancel.Location = new System.Drawing.Point(601, 522);
            this.btnNewMSPCancel.Name = "btnNewMSPCancel";
            this.btnNewMSPCancel.Size = new System.Drawing.Size(95, 37);
            this.btnNewMSPCancel.TabIndex = 52;
            this.btnNewMSPCancel.Text = "Cancel";
            this.btnNewMSPCancel.UseVisualStyleBackColor = true;
            this.btnNewMSPCancel.Click += new System.EventHandler(this.btnNewMSPCancel_Click);
            // 
            // cbNewMSPFrench
            // 
            this.cbNewMSPFrench.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbNewMSPFrench.Location = new System.Drawing.Point(469, 488);
            this.cbNewMSPFrench.Name = "cbNewMSPFrench";
            this.cbNewMSPFrench.Size = new System.Drawing.Size(141, 20);
            this.cbNewMSPFrench.TabIndex = 50;
            this.cbNewMSPFrench.Text = "French Canadian Order";
            this.cbNewMSPFrench.UseVisualStyleBackColor = true;
            // 
            // groupBoxNewMSPUnitInfo
            // 
            this.groupBoxNewMSPUnitInfo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBoxNewMSPUnitInfo.Controls.Add(this.tbNewMSP_MfgDate);
            this.groupBoxNewMSPUnitInfo.Controls.Add(this.label29);
            this.groupBoxNewMSPUnitInfo.Controls.Add(this.tbNewMSPRefrigerant);
            this.groupBoxNewMSPUnitInfo.Controls.Add(this.label15);
            this.groupBoxNewMSPUnitInfo.Controls.Add(this.label2);
            this.groupBoxNewMSPUnitInfo.Controls.Add(this.tbNewMSPFanFilter);
            this.groupBoxNewMSPUnitInfo.Controls.Add(this.labelNewFANPartNum);
            this.groupBoxNewMSPUnitInfo.Controls.Add(this.tbNewMSPDesignation);
            this.groupBoxNewMSPUnitInfo.Controls.Add(this.tbNewMSPPartNum);
            this.groupBoxNewMSPUnitInfo.Controls.Add(this.tbNewMSPVerifiedBy);
            this.groupBoxNewMSPUnitInfo.Controls.Add(this.labelMewFANVerifiedBy);
            this.groupBoxNewMSPUnitInfo.Controls.Add(this.labelNewMSPFiltere);
            this.groupBoxNewMSPUnitInfo.Controls.Add(this.labelNewFANSerialNo);
            this.groupBoxNewMSPUnitInfo.Controls.Add(this.tbNewMSPSerialNo);
            this.groupBoxNewMSPUnitInfo.Controls.Add(this.tbNewMSPModelNo);
            this.groupBoxNewMSPUnitInfo.Controls.Add(this.labelNewFANModelNum);
            this.groupBoxNewMSPUnitInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewMSPUnitInfo.Location = new System.Drawing.Point(5, 178);
            this.groupBoxNewMSPUnitInfo.Name = "groupBoxNewMSPUnitInfo";
            this.groupBoxNewMSPUnitInfo.Size = new System.Drawing.Size(443, 185);
            this.groupBoxNewMSPUnitInfo.TabIndex = 28;
            this.groupBoxNewMSPUnitInfo.TabStop = false;
            this.groupBoxNewMSPUnitInfo.Text = "Unit Info";
            // 
            // tbNewMSP_MfgDate
            // 
            this.tbNewMSP_MfgDate.BackColor = System.Drawing.Color.White;
            this.tbNewMSP_MfgDate.Enabled = false;
            this.tbNewMSP_MfgDate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSP_MfgDate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSP_MfgDate.Location = new System.Drawing.Point(331, 105);
            this.tbNewMSP_MfgDate.Name = "tbNewMSP_MfgDate";
            this.tbNewMSP_MfgDate.Size = new System.Drawing.Size(92, 22);
            this.tbNewMSP_MfgDate.TabIndex = 13;
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(331, 82);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(70, 20);
            this.label29.TabIndex = 70;
            this.label29.Text = "Mfg Date:";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbNewMSPRefrigerant
            // 
            this.tbNewMSPRefrigerant.BackColor = System.Drawing.Color.White;
            this.tbNewMSPRefrigerant.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPRefrigerant.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPRefrigerant.Location = new System.Drawing.Point(110, 153);
            this.tbNewMSPRefrigerant.Name = "tbNewMSPRefrigerant";
            this.tbNewMSPRefrigerant.Size = new System.Drawing.Size(210, 22);
            this.tbNewMSPRefrigerant.TabIndex = 12;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(7, 154);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(90, 21);
            this.label15.TabIndex = 69;
            this.label15.Text = "Refrigerant:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 21);
            this.label2.TabIndex = 68;
            this.label2.Text = "Designation:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPFanFilter
            // 
            this.tbNewMSPFanFilter.BackColor = System.Drawing.Color.White;
            this.tbNewMSPFanFilter.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPFanFilter.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPFanFilter.Location = new System.Drawing.Point(110, 126);
            this.tbNewMSPFanFilter.Name = "tbNewMSPFanFilter";
            this.tbNewMSPFanFilter.Size = new System.Drawing.Size(210, 22);
            this.tbNewMSPFanFilter.TabIndex = 11;
            // 
            // labelNewFANPartNum
            // 
            this.labelNewFANPartNum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANPartNum.Location = new System.Drawing.Point(7, 72);
            this.labelNewFANPartNum.Name = "labelNewFANPartNum";
            this.labelNewFANPartNum.Size = new System.Drawing.Size(90, 21);
            this.labelNewFANPartNum.TabIndex = 65;
            this.labelNewFANPartNum.Text = "Part Num:";
            this.labelNewFANPartNum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPDesignation
            // 
            this.tbNewMSPDesignation.BackColor = System.Drawing.Color.White;
            this.tbNewMSPDesignation.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPDesignation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPDesignation.Location = new System.Drawing.Point(110, 18);
            this.tbNewMSPDesignation.Name = "tbNewMSPDesignation";
            this.tbNewMSPDesignation.Size = new System.Drawing.Size(313, 22);
            this.tbNewMSPDesignation.TabIndex = 7;
            // 
            // tbNewMSPPartNum
            // 
            this.tbNewMSPPartNum.BackColor = System.Drawing.Color.White;
            this.tbNewMSPPartNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPPartNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPPartNum.Location = new System.Drawing.Point(110, 72);
            this.tbNewMSPPartNum.Name = "tbNewMSPPartNum";
            this.tbNewMSPPartNum.Size = new System.Drawing.Size(210, 22);
            this.tbNewMSPPartNum.TabIndex = 9;
           
            // 
            // tbNewMSPVerifiedBy
            // 
            this.tbNewMSPVerifiedBy.BackColor = System.Drawing.Color.White;
            this.tbNewMSPVerifiedBy.Enabled = false;
            this.tbNewMSPVerifiedBy.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPVerifiedBy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPVerifiedBy.Location = new System.Drawing.Point(331, 153);
            this.tbNewMSPVerifiedBy.Name = "tbNewMSPVerifiedBy";
            this.tbNewMSPVerifiedBy.Size = new System.Drawing.Size(92, 22);
            this.tbNewMSPVerifiedBy.TabIndex = 14;
            // 
            // labelMewFANVerifiedBy
            // 
            this.labelMewFANVerifiedBy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMewFANVerifiedBy.Location = new System.Drawing.Point(331, 130);
            this.labelMewFANVerifiedBy.Name = "labelMewFANVerifiedBy";
            this.labelMewFANVerifiedBy.Size = new System.Drawing.Size(92, 20);
            this.labelMewFANVerifiedBy.TabIndex = 12;
            this.labelMewFANVerifiedBy.Text = "Modified By:";
            this.labelMewFANVerifiedBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelNewMSPFiltere
            // 
            this.labelNewMSPFiltere.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMSPFiltere.Location = new System.Drawing.Point(7, 127);
            this.labelNewMSPFiltere.Name = "labelNewMSPFiltere";
            this.labelNewMSPFiltere.Size = new System.Drawing.Size(90, 21);
            this.labelNewMSPFiltere.TabIndex = 8;
            this.labelNewMSPFiltere.Text = "Filter:";
            this.labelNewMSPFiltere.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelNewFANSerialNo
            // 
            this.labelNewFANSerialNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANSerialNo.Location = new System.Drawing.Point(6, 99);
            this.labelNewFANSerialNo.Name = "labelNewFANSerialNo";
            this.labelNewFANSerialNo.Size = new System.Drawing.Size(90, 21);
            this.labelNewFANSerialNo.TabIndex = 7;
            this.labelNewFANSerialNo.Text = "Serial No:";
            this.labelNewFANSerialNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPSerialNo
            // 
            this.tbNewMSPSerialNo.BackColor = System.Drawing.Color.White;
            this.tbNewMSPSerialNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPSerialNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPSerialNo.Location = new System.Drawing.Point(110, 99);
            this.tbNewMSPSerialNo.Name = "tbNewMSPSerialNo";
            this.tbNewMSPSerialNo.Size = new System.Drawing.Size(210, 22);
            this.tbNewMSPSerialNo.TabIndex = 10;
            // 
            // tbNewMSPModelNo
            // 
            this.tbNewMSPModelNo.BackColor = System.Drawing.Color.White;
            this.tbNewMSPModelNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPModelNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPModelNo.Location = new System.Drawing.Point(110, 45);
            this.tbNewMSPModelNo.Name = "tbNewMSPModelNo";
            this.tbNewMSPModelNo.Size = new System.Drawing.Size(313, 22);
            this.tbNewMSPModelNo.TabIndex = 8;
            // 
            // labelNewFANModelNum
            // 
            this.labelNewFANModelNum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANModelNum.Location = new System.Drawing.Point(7, 45);
            this.labelNewFANModelNum.Name = "labelNewFANModelNum";
            this.labelNewFANModelNum.Size = new System.Drawing.Size(90, 21);
            this.labelNewFANModelNum.TabIndex = 4;
            this.labelNewFANModelNum.Text = "Model No:";
            this.labelNewFANModelNum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxNewFANVoltageInfo
            // 
            this.groupBoxNewFANVoltageInfo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.tbNewMSPFanTotalSP);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.label30);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.tbNewMSPFanQty);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.label28);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.tbNewMSPFanExternalSP);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.labelNewMSPFanExternalSP);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.tbNewMSPFanAirVolume);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.labelNewMSPAirVolume);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.tbNewMSPFanVoltageDraw);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.lbNewFAN_VoltDraw);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.tbNewMSP_Power);
            this.groupBoxNewFANVoltageInfo.Controls.Add(this.labelNewFANElecVolts);
            this.groupBoxNewFANVoltageInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewFANVoltageInfo.Location = new System.Drawing.Point(5, 369);
            this.groupBoxNewFANVoltageInfo.Name = "groupBoxNewFANVoltageInfo";
            this.groupBoxNewFANVoltageInfo.Size = new System.Drawing.Size(443, 190);
            this.groupBoxNewFANVoltageInfo.TabIndex = 29;
            this.groupBoxNewFANVoltageInfo.TabStop = false;
            this.groupBoxNewFANVoltageInfo.Text = "Fan";
            // 
            // tbNewMSPFanTotalSP
            // 
            this.tbNewMSPFanTotalSP.BackColor = System.Drawing.Color.White;
            this.tbNewMSPFanTotalSP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPFanTotalSP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPFanTotalSP.Location = new System.Drawing.Point(110, 127);
            this.tbNewMSPFanTotalSP.Name = "tbNewMSPFanTotalSP";
            this.tbNewMSPFanTotalSP.Size = new System.Drawing.Size(313, 22);
            this.tbNewMSPFanTotalSP.TabIndex = 21;
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(9, 127);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(90, 21);
            this.label30.TabIndex = 57;
            this.label30.Text = "Total SP:";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPFanQty
            // 
            this.tbNewMSPFanQty.BackColor = System.Drawing.Color.White;
            this.tbNewMSPFanQty.Enabled = false;
            this.tbNewMSPFanQty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPFanQty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPFanQty.Location = new System.Drawing.Point(110, 154);
            this.tbNewMSPFanQty.Name = "tbNewMSPFanQty";
            this.tbNewMSPFanQty.Size = new System.Drawing.Size(124, 22);
            this.tbNewMSPFanQty.TabIndex = 22;
            this.tbNewMSPFanQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(59, 155);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(49, 21);
            this.label28.TabIndex = 55;
            this.label28.Text = "Qty#:";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPFanExternalSP
            // 
            this.tbNewMSPFanExternalSP.BackColor = System.Drawing.Color.White;
            this.tbNewMSPFanExternalSP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPFanExternalSP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPFanExternalSP.Location = new System.Drawing.Point(110, 100);
            this.tbNewMSPFanExternalSP.Name = "tbNewMSPFanExternalSP";
            this.tbNewMSPFanExternalSP.Size = new System.Drawing.Size(313, 22);
            this.tbNewMSPFanExternalSP.TabIndex = 20;
            // 
            // labelNewMSPFanExternalSP
            // 
            this.labelNewMSPFanExternalSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMSPFanExternalSP.Location = new System.Drawing.Point(9, 100);
            this.labelNewMSPFanExternalSP.Name = "labelNewMSPFanExternalSP";
            this.labelNewMSPFanExternalSP.Size = new System.Drawing.Size(90, 21);
            this.labelNewMSPFanExternalSP.TabIndex = 50;
            this.labelNewMSPFanExternalSP.Text = "External SP:";
            this.labelNewMSPFanExternalSP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPFanAirVolume
            // 
            this.tbNewMSPFanAirVolume.BackColor = System.Drawing.Color.White;
            this.tbNewMSPFanAirVolume.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPFanAirVolume.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPFanAirVolume.Location = new System.Drawing.Point(110, 73);
            this.tbNewMSPFanAirVolume.Name = "tbNewMSPFanAirVolume";
            this.tbNewMSPFanAirVolume.Size = new System.Drawing.Size(313, 22);
            this.tbNewMSPFanAirVolume.TabIndex = 19;
            // 
            // labelNewMSPAirVolume
            // 
            this.labelNewMSPAirVolume.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMSPAirVolume.Location = new System.Drawing.Point(9, 73);
            this.labelNewMSPAirVolume.Name = "labelNewMSPAirVolume";
            this.labelNewMSPAirVolume.Size = new System.Drawing.Size(90, 21);
            this.labelNewMSPAirVolume.TabIndex = 46;
            this.labelNewMSPAirVolume.Text = "Air Volume:";
            this.labelNewMSPAirVolume.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPFanVoltageDraw
            // 
            this.tbNewMSPFanVoltageDraw.BackColor = System.Drawing.Color.White;
            this.tbNewMSPFanVoltageDraw.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPFanVoltageDraw.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPFanVoltageDraw.Location = new System.Drawing.Point(110, 46);
            this.tbNewMSPFanVoltageDraw.Name = "tbNewMSPFanVoltageDraw";
            this.tbNewMSPFanVoltageDraw.Size = new System.Drawing.Size(313, 22);
            this.tbNewMSPFanVoltageDraw.TabIndex = 16;
            // 
            // lbNewFAN_VoltDraw
            // 
            this.lbNewFAN_VoltDraw.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNewFAN_VoltDraw.Location = new System.Drawing.Point(9, 46);
            this.lbNewFAN_VoltDraw.Name = "lbNewFAN_VoltDraw";
            this.lbNewFAN_VoltDraw.Size = new System.Drawing.Size(90, 21);
            this.lbNewFAN_VoltDraw.TabIndex = 42;
            this.lbNewFAN_VoltDraw.Text = "Draw/Voltage:";
            this.lbNewFAN_VoltDraw.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSP_Power
            // 
            this.tbNewMSP_Power.BackColor = System.Drawing.Color.White;
            this.tbNewMSP_Power.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSP_Power.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSP_Power.Location = new System.Drawing.Point(110, 19);
            this.tbNewMSP_Power.Name = "tbNewMSP_Power";
            this.tbNewMSP_Power.Size = new System.Drawing.Size(313, 22);
            this.tbNewMSP_Power.TabIndex = 15;
            // 
            // labelNewFANElecVolts
            // 
            this.labelNewFANElecVolts.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewFANElecVolts.Location = new System.Drawing.Point(9, 19);
            this.labelNewFANElecVolts.Name = "labelNewFANElecVolts";
            this.labelNewFANElecVolts.Size = new System.Drawing.Size(90, 21);
            this.labelNewFANElecVolts.TabIndex = 40;
            this.labelNewFANElecVolts.Text = "Power:";
            this.labelNewFANElecVolts.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbNewMSPMsg
            // 
            this.lbNewMSPMsg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.lbNewMSPMsg.Location = new System.Drawing.Point(8, 567);
            this.lbNewMSPMsg.Name = "lbNewMSPMsg";
            this.lbNewMSPMsg.Size = new System.Drawing.Size(829, 21);
            this.lbNewMSPMsg.TabIndex = 30;
            this.lbNewMSPMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbNewMSPDehumidifier
            // 
            this.gbNewMSPDehumidifier.BackColor = System.Drawing.Color.LightSteelBlue;
            this.gbNewMSPDehumidifier.Controls.Add(this.tbNewMSPDehumidAmbient);
            this.gbNewMSPDehumidifier.Controls.Add(this.label6);
            this.gbNewMSPDehumidifier.Controls.Add(this.tbNewMSPDehumidCapacity);
            this.gbNewMSPDehumidifier.Controls.Add(this.label27);
            this.gbNewMSPDehumidifier.Controls.Add(this.tbNewMSPDehumidLAT);
            this.gbNewMSPDehumidifier.Controls.Add(this.label3);
            this.gbNewMSPDehumidifier.Controls.Add(this.tbNewMSPDehumidEAT);
            this.gbNewMSPDehumidifier.Controls.Add(this.label4);
            this.gbNewMSPDehumidifier.Controls.Add(this.tbNewMSPDehumidGlycol);
            this.gbNewMSPDehumidifier.Controls.Add(this.label5);
            this.gbNewMSPDehumidifier.Controls.Add(this.tbNewMSPDehumidPD);
            this.gbNewMSPDehumidifier.Controls.Add(this.label10);
            this.gbNewMSPDehumidifier.Controls.Add(this.tbNewMSPDehumidGPM_SST);
            this.gbNewMSPDehumidifier.Controls.Add(this.lbDehumidGPM_SST);
            this.gbNewMSPDehumidifier.Controls.Add(this.tbNewMSPDehumidLWT);
            this.gbNewMSPDehumidifier.Controls.Add(this.label7);
            this.gbNewMSPDehumidifier.Controls.Add(this.tbNewMSPDehumidEWT);
            this.gbNewMSPDehumidifier.Controls.Add(this.label9);
            this.gbNewMSPDehumidifier.Controls.Add(this.tbNewMSPDehumidBTUH);
            this.gbNewMSPDehumidifier.Controls.Add(this.label8);
            this.gbNewMSPDehumidifier.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbNewMSPDehumidifier.Location = new System.Drawing.Point(454, 27);
            this.gbNewMSPDehumidifier.Name = "gbNewMSPDehumidifier";
            this.gbNewMSPDehumidifier.Size = new System.Drawing.Size(383, 166);
            this.gbNewMSPDehumidifier.TabIndex = 31;
            this.gbNewMSPDehumidifier.TabStop = false;
            this.gbNewMSPDehumidifier.Text = "Dehumidifier/Summer Rated Conditions";
            // 
            // tbNewMSPDehumidAmbient
            // 
            this.tbNewMSPDehumidAmbient.BackColor = System.Drawing.Color.White;
            this.tbNewMSPDehumidAmbient.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPDehumidAmbient.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPDehumidAmbient.Location = new System.Drawing.Point(85, 129);
            this.tbNewMSPDehumidAmbient.Name = "tbNewMSPDehumidAmbient";
            this.tbNewMSPDehumidAmbient.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPDehumidAmbient.TabIndex = 27;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(10, 129);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 21);
            this.label6.TabIndex = 72;
            this.label6.Text = "Ambient:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPDehumidCapacity
            // 
            this.tbNewMSPDehumidCapacity.BackColor = System.Drawing.Color.White;
            this.tbNewMSPDehumidCapacity.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPDehumidCapacity.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPDehumidCapacity.Location = new System.Drawing.Point(270, 129);
            this.tbNewMSPDehumidCapacity.Name = "tbNewMSPDehumidCapacity";
            this.tbNewMSPDehumidCapacity.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPDehumidCapacity.TabIndex = 32;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(190, 130);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(75, 21);
            this.label27.TabIndex = 70;
            this.label27.Text = "CAPACITY:";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPDehumidLAT
            // 
            this.tbNewMSPDehumidLAT.BackColor = System.Drawing.Color.White;
            this.tbNewMSPDehumidLAT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPDehumidLAT.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPDehumidLAT.Location = new System.Drawing.Point(85, 48);
            this.tbNewMSPDehumidLAT.Name = "tbNewMSPDehumidLAT";
            this.tbNewMSPDehumidLAT.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPDehumidLAT.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(190, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 21);
            this.label3.TabIndex = 68;
            this.label3.Text = "LWT:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPDehumidEAT
            // 
            this.tbNewMSPDehumidEAT.BackColor = System.Drawing.Color.White;
            this.tbNewMSPDehumidEAT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPDehumidEAT.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPDehumidEAT.Location = new System.Drawing.Point(85, 21);
            this.tbNewMSPDehumidEAT.Name = "tbNewMSPDehumidEAT";
            this.tbNewMSPDehumidEAT.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPDehumidEAT.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(190, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 21);
            this.label4.TabIndex = 66;
            this.label4.Text = "EWT:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPDehumidGlycol
            // 
            this.tbNewMSPDehumidGlycol.BackColor = System.Drawing.Color.White;
            this.tbNewMSPDehumidGlycol.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPDehumidGlycol.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPDehumidGlycol.Location = new System.Drawing.Point(270, 48);
            this.tbNewMSPDehumidGlycol.Name = "tbNewMSPDehumidGlycol";
            this.tbNewMSPDehumidGlycol.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPDehumidGlycol.TabIndex = 29;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(190, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 21);
            this.label5.TabIndex = 64;
            this.label5.Text = "Glycol:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPDehumidPD
            // 
            this.tbNewMSPDehumidPD.BackColor = System.Drawing.Color.White;
            this.tbNewMSPDehumidPD.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPDehumidPD.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPDehumidPD.Location = new System.Drawing.Point(270, 21);
            this.tbNewMSPDehumidPD.Name = "tbNewMSPDehumidPD";
            this.tbNewMSPDehumidPD.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPDehumidPD.TabIndex = 28;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(190, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 21);
            this.label10.TabIndex = 62;
            this.label10.Text = "PD:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPDehumidGPM_SST
            // 
            this.tbNewMSPDehumidGPM_SST.BackColor = System.Drawing.Color.White;
            this.tbNewMSPDehumidGPM_SST.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPDehumidGPM_SST.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPDehumidGPM_SST.Location = new System.Drawing.Point(85, 102);
            this.tbNewMSPDehumidGPM_SST.Name = "tbNewMSPDehumidGPM_SST";
            this.tbNewMSPDehumidGPM_SST.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPDehumidGPM_SST.TabIndex = 26;
            // 
            // lbDehumidGPM_SST
            // 
            this.lbDehumidGPM_SST.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDehumidGPM_SST.Location = new System.Drawing.Point(10, 102);
            this.lbDehumidGPM_SST.Name = "lbDehumidGPM_SST";
            this.lbDehumidGPM_SST.Size = new System.Drawing.Size(70, 21);
            this.lbDehumidGPM_SST.TabIndex = 60;
            this.lbDehumidGPM_SST.Text = "GPM";
            this.lbDehumidGPM_SST.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPDehumidLWT
            // 
            this.tbNewMSPDehumidLWT.BackColor = System.Drawing.Color.White;
            this.tbNewMSPDehumidLWT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPDehumidLWT.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPDehumidLWT.Location = new System.Drawing.Point(270, 102);
            this.tbNewMSPDehumidLWT.Name = "tbNewMSPDehumidLWT";
            this.tbNewMSPDehumidLWT.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPDehumidLWT.TabIndex = 31;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(10, 75);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 21);
            this.label7.TabIndex = 58;
            this.label7.Text = "BTUH:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPDehumidEWT
            // 
            this.tbNewMSPDehumidEWT.BackColor = System.Drawing.Color.White;
            this.tbNewMSPDehumidEWT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPDehumidEWT.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPDehumidEWT.Location = new System.Drawing.Point(270, 75);
            this.tbNewMSPDehumidEWT.Name = "tbNewMSPDehumidEWT";
            this.tbNewMSPDehumidEWT.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPDehumidEWT.TabIndex = 30;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(10, 48);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 21);
            this.label9.TabIndex = 56;
            this.label9.Text = "LAT:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPDehumidBTUH
            // 
            this.tbNewMSPDehumidBTUH.BackColor = System.Drawing.Color.White;
            this.tbNewMSPDehumidBTUH.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPDehumidBTUH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPDehumidBTUH.Location = new System.Drawing.Point(85, 75);
            this.tbNewMSPDehumidBTUH.Name = "tbNewMSPDehumidBTUH";
            this.tbNewMSPDehumidBTUH.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPDehumidBTUH.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(10, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 21);
            this.label8.TabIndex = 40;
            this.label8.Text = "EAT:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbNewMSPPostCooling
            // 
            this.gbNewMSPPostCooling.BackColor = System.Drawing.Color.LightSteelBlue;
            this.gbNewMSPPostCooling.Controls.Add(this.tbNewMSPPostCoolingLAT);
            this.gbNewMSPPostCooling.Controls.Add(this.label11);
            this.gbNewMSPPostCooling.Controls.Add(this.tbNewMSPPostCoolingEAT);
            this.gbNewMSPPostCooling.Controls.Add(this.label12);
            this.gbNewMSPPostCooling.Controls.Add(this.tbNewMSPPostCoolingGlycol);
            this.gbNewMSPPostCooling.Controls.Add(this.label13);
            this.gbNewMSPPostCooling.Controls.Add(this.tbNewMSPPostCoolingPD);
            this.gbNewMSPPostCooling.Controls.Add(this.label14);
            this.gbNewMSPPostCooling.Controls.Add(this.tbNewMSPPostCoolingGPM_SST);
            this.gbNewMSPPostCooling.Controls.Add(this.lbPostCoolingGPM_SST);
            this.gbNewMSPPostCooling.Controls.Add(this.tbNewMSPPostCoolingLWT);
            this.gbNewMSPPostCooling.Controls.Add(this.label16);
            this.gbNewMSPPostCooling.Controls.Add(this.tbNewMSPPostCoolingEWT);
            this.gbNewMSPPostCooling.Controls.Add(this.label17);
            this.gbNewMSPPostCooling.Controls.Add(this.tbNewMSPPostCoolingBTUH);
            this.gbNewMSPPostCooling.Controls.Add(this.label18);
            this.gbNewMSPPostCooling.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbNewMSPPostCooling.Location = new System.Drawing.Point(454, 199);
            this.gbNewMSPPostCooling.Name = "gbNewMSPPostCooling";
            this.gbNewMSPPostCooling.Size = new System.Drawing.Size(383, 134);
            this.gbNewMSPPostCooling.TabIndex = 32;
            this.gbNewMSPPostCooling.TabStop = false;
            this.gbNewMSPPostCooling.Text = "Post Cooling";
            // 
            // tbNewMSPPostCoolingLAT
            // 
            this.tbNewMSPPostCoolingLAT.BackColor = System.Drawing.Color.White;
            this.tbNewMSPPostCoolingLAT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPPostCoolingLAT.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPPostCoolingLAT.Location = new System.Drawing.Point(85, 48);
            this.tbNewMSPPostCoolingLAT.Name = "tbNewMSPPostCoolingLAT";
            this.tbNewMSPPostCoolingLAT.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPPostCoolingLAT.TabIndex = 34;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(211, 102);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 21);
            this.label11.TabIndex = 68;
            this.label11.Text = "LWT:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPPostCoolingEAT
            // 
            this.tbNewMSPPostCoolingEAT.BackColor = System.Drawing.Color.White;
            this.tbNewMSPPostCoolingEAT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPPostCoolingEAT.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPPostCoolingEAT.Location = new System.Drawing.Point(85, 21);
            this.tbNewMSPPostCoolingEAT.Name = "tbNewMSPPostCoolingEAT";
            this.tbNewMSPPostCoolingEAT.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPPostCoolingEAT.TabIndex = 33;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(211, 75);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 21);
            this.label12.TabIndex = 66;
            this.label12.Text = "EWT:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPPostCoolingGlycol
            // 
            this.tbNewMSPPostCoolingGlycol.BackColor = System.Drawing.Color.White;
            this.tbNewMSPPostCoolingGlycol.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPPostCoolingGlycol.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPPostCoolingGlycol.Location = new System.Drawing.Point(270, 48);
            this.tbNewMSPPostCoolingGlycol.Name = "tbNewMSPPostCoolingGlycol";
            this.tbNewMSPPostCoolingGlycol.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPPostCoolingGlycol.TabIndex = 38;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(211, 48);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 21);
            this.label13.TabIndex = 64;
            this.label13.Text = "Glycol:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPPostCoolingPD
            // 
            this.tbNewMSPPostCoolingPD.BackColor = System.Drawing.Color.White;
            this.tbNewMSPPostCoolingPD.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPPostCoolingPD.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPPostCoolingPD.Location = new System.Drawing.Point(270, 21);
            this.tbNewMSPPostCoolingPD.Name = "tbNewMSPPostCoolingPD";
            this.tbNewMSPPostCoolingPD.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPPostCoolingPD.TabIndex = 37;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(211, 21);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 21);
            this.label14.TabIndex = 62;
            this.label14.Text = "PD:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPPostCoolingGPM_SST
            // 
            this.tbNewMSPPostCoolingGPM_SST.BackColor = System.Drawing.Color.White;
            this.tbNewMSPPostCoolingGPM_SST.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPPostCoolingGPM_SST.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPPostCoolingGPM_SST.Location = new System.Drawing.Point(85, 102);
            this.tbNewMSPPostCoolingGPM_SST.Name = "tbNewMSPPostCoolingGPM_SST";
            this.tbNewMSPPostCoolingGPM_SST.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPPostCoolingGPM_SST.TabIndex = 36;
            // 
            // lbPostCoolingGPM_SST
            // 
            this.lbPostCoolingGPM_SST.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPostCoolingGPM_SST.Location = new System.Drawing.Point(26, 102);
            this.lbPostCoolingGPM_SST.Name = "lbPostCoolingGPM_SST";
            this.lbPostCoolingGPM_SST.Size = new System.Drawing.Size(55, 21);
            this.lbPostCoolingGPM_SST.TabIndex = 60;
            this.lbPostCoolingGPM_SST.Text = "GPM";
            this.lbPostCoolingGPM_SST.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPPostCoolingLWT
            // 
            this.tbNewMSPPostCoolingLWT.BackColor = System.Drawing.Color.White;
            this.tbNewMSPPostCoolingLWT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPPostCoolingLWT.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPPostCoolingLWT.Location = new System.Drawing.Point(270, 102);
            this.tbNewMSPPostCoolingLWT.Name = "tbNewMSPPostCoolingLWT";
            this.tbNewMSPPostCoolingLWT.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPPostCoolingLWT.TabIndex = 40;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(26, 75);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(55, 21);
            this.label16.TabIndex = 58;
            this.label16.Text = "BTUH:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPPostCoolingEWT
            // 
            this.tbNewMSPPostCoolingEWT.BackColor = System.Drawing.Color.White;
            this.tbNewMSPPostCoolingEWT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPPostCoolingEWT.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPPostCoolingEWT.Location = new System.Drawing.Point(270, 75);
            this.tbNewMSPPostCoolingEWT.Name = "tbNewMSPPostCoolingEWT";
            this.tbNewMSPPostCoolingEWT.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPPostCoolingEWT.TabIndex = 39;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(26, 48);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(55, 21);
            this.label17.TabIndex = 56;
            this.label17.Text = "LAT:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPPostCoolingBTUH
            // 
            this.tbNewMSPPostCoolingBTUH.BackColor = System.Drawing.Color.White;
            this.tbNewMSPPostCoolingBTUH.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPPostCoolingBTUH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPPostCoolingBTUH.Location = new System.Drawing.Point(85, 75);
            this.tbNewMSPPostCoolingBTUH.Name = "tbNewMSPPostCoolingBTUH";
            this.tbNewMSPPostCoolingBTUH.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPPostCoolingBTUH.TabIndex = 35;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(26, 21);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 21);
            this.label18.TabIndex = 40;
            this.label18.Text = "EAT:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbNewMSPPostHeating
            // 
            this.gbNewMSPPostHeating.BackColor = System.Drawing.Color.LightSteelBlue;
            this.gbNewMSPPostHeating.Controls.Add(this.tbNewMSPPostHeatingLAT);
            this.gbNewMSPPostHeating.Controls.Add(this.label19);
            this.gbNewMSPPostHeating.Controls.Add(this.tbNewMSPPostHeatingEAT);
            this.gbNewMSPPostHeating.Controls.Add(this.label20);
            this.gbNewMSPPostHeating.Controls.Add(this.tbNewMSPPostHeatingGlycol);
            this.gbNewMSPPostHeating.Controls.Add(this.label21);
            this.gbNewMSPPostHeating.Controls.Add(this.tbNewMSPPostHeatingPD);
            this.gbNewMSPPostHeating.Controls.Add(this.label22);
            this.gbNewMSPPostHeating.Controls.Add(this.tbNewMSPPostHeatingGPM);
            this.gbNewMSPPostHeating.Controls.Add(this.label23);
            this.gbNewMSPPostHeating.Controls.Add(this.tbNewMSPPostHeatingLWT);
            this.gbNewMSPPostHeating.Controls.Add(this.label24);
            this.gbNewMSPPostHeating.Controls.Add(this.tbNewMSPPostHeatingEWT);
            this.gbNewMSPPostHeating.Controls.Add(this.label25);
            this.gbNewMSPPostHeating.Controls.Add(this.tbNewMSPPostHeatingBTUH);
            this.gbNewMSPPostHeating.Controls.Add(this.label26);
            this.gbNewMSPPostHeating.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbNewMSPPostHeating.Location = new System.Drawing.Point(454, 339);
            this.gbNewMSPPostHeating.Name = "gbNewMSPPostHeating";
            this.gbNewMSPPostHeating.Size = new System.Drawing.Size(383, 134);
            this.gbNewMSPPostHeating.TabIndex = 33;
            this.gbNewMSPPostHeating.TabStop = false;
            this.gbNewMSPPostHeating.Text = "Post Heating";
            // 
            // tbNewMSPPostHeatingLAT
            // 
            this.tbNewMSPPostHeatingLAT.BackColor = System.Drawing.Color.White;
            this.tbNewMSPPostHeatingLAT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPPostHeatingLAT.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPPostHeatingLAT.Location = new System.Drawing.Point(85, 48);
            this.tbNewMSPPostHeatingLAT.Name = "tbNewMSPPostHeatingLAT";
            this.tbNewMSPPostHeatingLAT.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPPostHeatingLAT.TabIndex = 42;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(211, 101);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(55, 21);
            this.label19.TabIndex = 68;
            this.label19.Text = "LWT:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPPostHeatingEAT
            // 
            this.tbNewMSPPostHeatingEAT.BackColor = System.Drawing.Color.White;
            this.tbNewMSPPostHeatingEAT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPPostHeatingEAT.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPPostHeatingEAT.Location = new System.Drawing.Point(85, 21);
            this.tbNewMSPPostHeatingEAT.Name = "tbNewMSPPostHeatingEAT";
            this.tbNewMSPPostHeatingEAT.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPPostHeatingEAT.TabIndex = 41;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(211, 74);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(55, 21);
            this.label20.TabIndex = 66;
            this.label20.Text = "EWT:";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPPostHeatingGlycol
            // 
            this.tbNewMSPPostHeatingGlycol.BackColor = System.Drawing.Color.White;
            this.tbNewMSPPostHeatingGlycol.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPPostHeatingGlycol.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPPostHeatingGlycol.Location = new System.Drawing.Point(270, 48);
            this.tbNewMSPPostHeatingGlycol.Name = "tbNewMSPPostHeatingGlycol";
            this.tbNewMSPPostHeatingGlycol.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPPostHeatingGlycol.TabIndex = 46;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(211, 47);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(55, 21);
            this.label21.TabIndex = 64;
            this.label21.Text = "Glycol:";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPPostHeatingPD
            // 
            this.tbNewMSPPostHeatingPD.BackColor = System.Drawing.Color.White;
            this.tbNewMSPPostHeatingPD.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPPostHeatingPD.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPPostHeatingPD.Location = new System.Drawing.Point(270, 21);
            this.tbNewMSPPostHeatingPD.Name = "tbNewMSPPostHeatingPD";
            this.tbNewMSPPostHeatingPD.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPPostHeatingPD.TabIndex = 45;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(211, 20);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(55, 21);
            this.label22.TabIndex = 62;
            this.label22.Text = "PD:";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPPostHeatingGPM
            // 
            this.tbNewMSPPostHeatingGPM.BackColor = System.Drawing.Color.White;
            this.tbNewMSPPostHeatingGPM.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPPostHeatingGPM.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPPostHeatingGPM.Location = new System.Drawing.Point(85, 102);
            this.tbNewMSPPostHeatingGPM.Name = "tbNewMSPPostHeatingGPM";
            this.tbNewMSPPostHeatingGPM.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPPostHeatingGPM.TabIndex = 44;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(26, 102);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(55, 21);
            this.label23.TabIndex = 60;
            this.label23.Text = "GPM:";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPPostHeatingLWT
            // 
            this.tbNewMSPPostHeatingLWT.BackColor = System.Drawing.Color.White;
            this.tbNewMSPPostHeatingLWT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPPostHeatingLWT.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPPostHeatingLWT.Location = new System.Drawing.Point(270, 102);
            this.tbNewMSPPostHeatingLWT.Name = "tbNewMSPPostHeatingLWT";
            this.tbNewMSPPostHeatingLWT.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPPostHeatingLWT.TabIndex = 49;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(26, 75);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(55, 21);
            this.label24.TabIndex = 58;
            this.label24.Text = "BTUH:";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPPostHeatingEWT
            // 
            this.tbNewMSPPostHeatingEWT.BackColor = System.Drawing.Color.White;
            this.tbNewMSPPostHeatingEWT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPPostHeatingEWT.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPPostHeatingEWT.Location = new System.Drawing.Point(270, 75);
            this.tbNewMSPPostHeatingEWT.Name = "tbNewMSPPostHeatingEWT";
            this.tbNewMSPPostHeatingEWT.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPPostHeatingEWT.TabIndex = 47;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(26, 48);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(55, 21);
            this.label25.TabIndex = 56;
            this.label25.Text = "LAT:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbNewMSPPostHeatingBTUH
            // 
            this.tbNewMSPPostHeatingBTUH.BackColor = System.Drawing.Color.White;
            this.tbNewMSPPostHeatingBTUH.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNewMSPPostHeatingBTUH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tbNewMSPPostHeatingBTUH.Location = new System.Drawing.Point(85, 75);
            this.tbNewMSPPostHeatingBTUH.Name = "tbNewMSPPostHeatingBTUH";
            this.tbNewMSPPostHeatingBTUH.Size = new System.Drawing.Size(98, 22);
            this.tbNewMSPPostHeatingBTUH.TabIndex = 43;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(26, 21);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(55, 21);
            this.label26.TabIndex = 40;
            this.label26.Text = "EAT:";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // frmNewLineMSP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 592);
            this.Controls.Add(this.gbNewMSPPostHeating);
            this.Controls.Add(this.gbNewMSPPostCooling);
            this.Controls.Add(this.gbNewMSPDehumidifier);
            this.Controls.Add(this.lbNewMSPMsg);
            this.Controls.Add(this.groupBoxNewFANVoltageInfo);
            this.Controls.Add(this.groupBoxNewMSPUnitInfo);
            this.Controls.Add(this.btnNewMSPPrint);
            this.Controls.Add(this.btnNewMSPAccept);
            this.Controls.Add(this.btnNewMSPCancel);
            this.Controls.Add(this.cbNewMSPFrench);
            this.Controls.Add(this.groupBoxNewMSPJobInfo);
            this.Controls.Add(this.menuStripNewMSP);
            this.Name = "frmNewLineMSP";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "New Line MSP";
            this.menuStripNewMSP.ResumeLayout(false);
            this.menuStripNewMSP.PerformLayout();
            this.groupBoxNewMSPJobInfo.ResumeLayout(false);
            this.groupBoxNewMSPJobInfo.PerformLayout();
            this.groupBoxNewMSPUnitInfo.ResumeLayout(false);
            this.groupBoxNewMSPUnitInfo.PerformLayout();
            this.groupBoxNewFANVoltageInfo.ResumeLayout(false);
            this.groupBoxNewFANVoltageInfo.PerformLayout();
            this.gbNewMSPDehumidifier.ResumeLayout(false);
            this.gbNewMSPDehumidifier.PerformLayout();
            this.gbNewMSPPostCooling.ResumeLayout(false);
            this.gbNewMSPPostCooling.PerformLayout();
            this.gbNewMSPPostHeating.ResumeLayout(false);
            this.gbNewMSPPostHeating.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStripNewMSP;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBoxNewMSPJobInfo;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox tbNewMSPJobNum;
        public System.Windows.Forms.TextBox tbNewMSPEndConsumer;
        private System.Windows.Forms.Label labelNewFANEndConsumer;
        public System.Windows.Forms.TextBox tbNewMSPReleaseNum;
        private System.Windows.Forms.Label labelNewFANSlash;
        public System.Windows.Forms.TextBox tbNewMSPCustomer;
        public System.Windows.Forms.TextBox tbNewMSPOrderNum;
        private System.Windows.Forms.Label labelNewFANCustomer;
        private System.Windows.Forms.Label labelNewFANOrderNum;
        public System.Windows.Forms.TextBox tbNewMSPLineItem;
        private System.Windows.Forms.Label labelNewFANLineItem;
        public System.Windows.Forms.Button btnNewMSPPrint;
        public System.Windows.Forms.Button btnNewMSPAccept;
        public System.Windows.Forms.Button btnNewMSPCancel;
        private System.Windows.Forms.CheckBox cbNewMSPFrench;
        public System.Windows.Forms.GroupBox groupBoxNewMSPUnitInfo;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox tbNewMSPDesignation;
        public System.Windows.Forms.TextBox tbNewMSPFanFilter;
        private System.Windows.Forms.Label labelNewFANPartNum;
        public System.Windows.Forms.TextBox tbNewMSPPartNum;
        public System.Windows.Forms.TextBox tbNewMSPVerifiedBy;
        private System.Windows.Forms.Label labelMewFANVerifiedBy;
        private System.Windows.Forms.Label labelNewMSPFiltere;
        private System.Windows.Forms.Label labelNewFANSerialNo;
        public System.Windows.Forms.TextBox tbNewMSPSerialNo;
        public System.Windows.Forms.TextBox tbNewMSPModelNo;
        private System.Windows.Forms.Label labelNewFANModelNum;
        public System.Windows.Forms.GroupBox groupBoxNewFANVoltageInfo;
        public System.Windows.Forms.TextBox tbNewMSPFanExternalSP;
        private System.Windows.Forms.Label labelNewMSPFanExternalSP;
        public System.Windows.Forms.TextBox tbNewMSPFanAirVolume;
        private System.Windows.Forms.Label labelNewMSPAirVolume;
        public System.Windows.Forms.TextBox tbNewMSPFanVoltageDraw;
        public System.Windows.Forms.TextBox tbNewMSP_Power;
        private System.Windows.Forms.Label labelNewFANElecVolts;
        public System.Windows.Forms.Label lbNewMSPMsg;
        public System.Windows.Forms.GroupBox gbNewMSPDehumidifier;
        public System.Windows.Forms.TextBox tbNewMSPDehumidLAT;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox tbNewMSPDehumidEAT;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox tbNewMSPDehumidGlycol;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox tbNewMSPDehumidPD;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox tbNewMSPDehumidGPM_SST;
        public System.Windows.Forms.TextBox tbNewMSPDehumidLWT;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox tbNewMSPDehumidEWT;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox tbNewMSPDehumidBTUH;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.GroupBox gbNewMSPPostCooling;
        public System.Windows.Forms.TextBox tbNewMSPPostCoolingLAT;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.TextBox tbNewMSPPostCoolingEAT;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.TextBox tbNewMSPPostCoolingGlycol;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.TextBox tbNewMSPPostCoolingPD;
        private System.Windows.Forms.Label label14;
        public System.Windows.Forms.TextBox tbNewMSPPostCoolingGPM_SST;
        public System.Windows.Forms.TextBox tbNewMSPPostCoolingLWT;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.TextBox tbNewMSPPostCoolingEWT;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.TextBox tbNewMSPPostCoolingBTUH;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.GroupBox gbNewMSPPostHeating;
        public System.Windows.Forms.TextBox tbNewMSPPostHeatingLAT;
        private System.Windows.Forms.Label label19;
        public System.Windows.Forms.TextBox tbNewMSPPostHeatingEAT;
        private System.Windows.Forms.Label label20;
        public System.Windows.Forms.TextBox tbNewMSPPostHeatingGlycol;
        private System.Windows.Forms.Label label21;
        public System.Windows.Forms.TextBox tbNewMSPPostHeatingPD;
        private System.Windows.Forms.Label label22;
        public System.Windows.Forms.TextBox tbNewMSPPostHeatingGPM;
        private System.Windows.Forms.Label label23;
        public System.Windows.Forms.TextBox tbNewMSPPostHeatingLWT;
        private System.Windows.Forms.Label label24;
        public System.Windows.Forms.TextBox tbNewMSPPostHeatingEWT;
        private System.Windows.Forms.Label label25;
        public System.Windows.Forms.TextBox tbNewMSPPostHeatingBTUH;
        private System.Windows.Forms.Label label26;
        public System.Windows.Forms.TextBox tbNewMSPDehumidCapacity;
        private System.Windows.Forms.Label label27;
        public System.Windows.Forms.TextBox tbNewMSPFanQty;
        private System.Windows.Forms.Label label28;
        public System.Windows.Forms.Label lbPostCoolingGPM_SST;
        public System.Windows.Forms.TextBox tbNewMSPDehumidAmbient;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox tbNewMSPRefrigerant;
        private System.Windows.Forms.Label label15;
        public System.Windows.Forms.Label lbDehumidGPM_SST;
        public System.Windows.Forms.TextBox tbNewMSP_MfgDate;
        private System.Windows.Forms.Label label29;
        public System.Windows.Forms.TextBox tbNewMSPFanTotalSP;
        private System.Windows.Forms.Label label30;
        public System.Windows.Forms.Label lbNewFAN_VoltDraw;
        private System.Windows.Forms.Label label31;
        public System.Windows.Forms.TextBox tbNewMSP_UnitType;
    }
}