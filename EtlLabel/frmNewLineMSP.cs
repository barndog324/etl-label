﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KCC.OA.Etl
{
    public partial class frmNewLineMSP : Form
    {
        MainBO objMain = new MainBO();
        private string EpicorServer = "";

        public frmNewLineMSP()
        {
            InitializeComponent();
        }

        private void buttonNewMSPAccept_Click(object sender, EventArgs e)
        {
            bindDataToModel();

            if (objMain.MfgDate == null)
            {
                objMain.MfgDate = DateTime.Today.ToShortDateString();
            }

            if (btnNewMSPAccept.Text == "Update")
            {
                try
                {
                    objMain.MSP_UpdateEtl();
                    lbNewMSPMsg.Text = "Job# " + objMain.JobNum + " has been updated into the MSP_Etl table.";
                }
                catch
                {
                    MessageBox.Show("Error occurred while updating a row to the MSP_Etl table.");
                    return;
                }
                
            }
            else
            {
                VS_DataSet dsEtl = new VS_DataSet();

                // Write record to the VSOrderHed table.                
                try
                {
                    VS_DataSetTableAdapters.EtlJobHeadTableAdapter etlJobHeadTA =
                        new VS_DataSetTableAdapters.EtlJobHeadTableAdapter();

                    etlJobHeadTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                    etlJobHeadTA.Fill(dsEtl.EtlJobHead);

                    DataRow drJobHead = dsEtl.EtlJobHead.NewRow();

                    drJobHead["JobNum"] = objMain.JobNum;
                    drJobHead["OrderNum"] = objMain.OrderNum;
                    drJobHead["OrderLine"] = objMain.OrderLine;
                    drJobHead["ReleaseNum"] = objMain.ReleaseNum;
                    drJobHead["UnitType"] = "MSP";
                    drJobHead["Customer"] = objMain.Customer;
                    drJobHead["EndConsumerName"] = objMain.EndConsumerName;

                    dsEtl.EtlJobHead.Rows.Add(drJobHead);
                    etlJobHeadTA.Update(dsEtl.EtlJobHead);                   
                }
                catch
                {
                    MessageBox.Show("Error occurred while inserting a row to the EtlJobHead table.");
                    return;
                }

                try
                {
                    objMain.MSP_InsertEtl();
                    btnNewMSPPrint.Enabled = true;
                    lbNewMSPMsg.Text = "Job# " + objMain.JobNum + " has been inserted into the MSP_Etl table.";
                }
                catch
                {
                    MessageBox.Show("Error occurred while updating a row to the MSP_Etl table.");
                    return;
                }
               
            }
        }

        private void bindDataToModel()
        {
            string modelNoStr = this.tbNewMSPModelNo.Text;
          
            objMain.JobNum = tbNewMSPJobNum.Text;
            objMain.OrderNum = this.tbNewMSPOrderNum.Text;
            objMain.OrderLine = this.tbNewMSPLineItem.Text;
            objMain.ReleaseNum = this.tbNewMSPReleaseNum.Text;
            objMain.Customer = this.tbNewMSPCustomer.Text;
            objMain.EndConsumerName = this.tbNewMSPEndConsumer.Text;

            objMain.MSPDesignation = this.tbNewMSPDesignation.Text;
            objMain.ModelNo = modelNoStr;
            objMain.PartNum = this.tbNewMSPPartNum.Text;
            objMain.SerialNo = this.tbNewMSPSerialNo.Text;
            objMain.MfgDate = this.tbNewMSP_MfgDate.Text;
            objMain.MSPFanFilter = this.tbNewMSPFanFilter.Text;
            objMain.ModelNoVerifiedBy = this.tbNewMSPVerifiedBy.Text;

            objMain.MSPFanPower = this.tbNewMSP_Power.Text;
            if (modelNoStr.Substring(1, 1) == "U")
            {
                objMain.MSPFanDraw = "";
                objMain.MSPFanVoltage = this.tbNewMSPFanVoltageDraw.Text; 
            }
            else
            {
                objMain.MSPFanDraw = this.tbNewMSPFanVoltageDraw.Text;
                objMain.MSPFanVoltage = ""; 
            }

            objMain.Refrigerant = tbNewMSPRefrigerant.Text;
            objMain.AmbientTemp = this.tbNewMSPDehumidAmbient.Text;
                       
            objMain.MSPFanAirVolume = this.tbNewMSPFanAirVolume.Text;
            objMain.MSPFanExternalSP = this.tbNewMSPFanExternalSP.Text;
            objMain.MSPFanTotalSP = this.tbNewMSPFanTotalSP.Text;
            
            if (tbNewMSPFanQty.Text.Length == 0 )
            {
                objMain.MSPFanQty = "1";
            }
            else
            {
                objMain.MSPFanQty = tbNewMSPFanQty.Text;
            }

            objMain.Dehumidifier_BTUH = this.tbNewMSPDehumidBTUH.Text;
            objMain.Dehumidifier_EWT = this.tbNewMSPDehumidEWT.Text;
            objMain.Dehumidifier_LWT = this.tbNewMSPDehumidLWT.Text;

            if (this.lbDehumidGPM_SST.Text == "GPM")
            {
                objMain.Dehumidifier_GPM = this.tbNewMSPDehumidGPM_SST.Text;
                objMain.Dehumidifier_SST = "";
            }
            else
            {
                objMain.Dehumidifier_SST = this.tbNewMSPDehumidGPM_SST.Text;
                objMain.Dehumidifier_GPM = "";
            }

            objMain.Dehumidifier_PD = this.tbNewMSPDehumidPD.Text;
            objMain.Dehumidifier_Glycol = this.tbNewMSPDehumidGlycol.Text;
            objMain.Dehumidifier_EAT = this.tbNewMSPDehumidEAT.Text;
            objMain.Dehumidifier_LAT = this.tbNewMSPDehumidLAT.Text;
            objMain.Dehumidifier_CAPACITY = this.tbNewMSPDehumidCapacity.Text;

            objMain.PostCooling_BTUH = this.tbNewMSPPostCoolingBTUH.Text;
            objMain.PostCooling_EWT = this.tbNewMSPPostCoolingEWT.Text;
            objMain.PostCooling_LWT = this.tbNewMSPPostCoolingLWT.Text;

            if (this.lbPostCoolingGPM_SST.Text == "GPM")
            {
                objMain.PostCooling_GPM = this.tbNewMSPPostCoolingGPM_SST.Text;
                objMain.PostCooling_SST = "";
            }
            else
            {
                objMain.PostCooling_SST = this.tbNewMSPPostCoolingGPM_SST.Text;
                objMain.PostCooling_GPM = "";
            }
           
            objMain.PostCooling_PD = this.tbNewMSPPostCoolingPD.Text;
            objMain.PostCooling_Glycol = this.tbNewMSPPostCoolingGlycol.Text;
            objMain.PostCooling_EAT = this.tbNewMSPPostCoolingEAT.Text;
            objMain.PostCooling_LAT = this.tbNewMSPPostCoolingLAT.Text;

            objMain.PostHeating_BTUH = this.tbNewMSPPostHeatingBTUH.Text;
            objMain.PostHeating_EWT = this.tbNewMSPPostHeatingEWT.Text;
            objMain.PostHeating_LWT = this.tbNewMSPPostHeatingLWT.Text;
            objMain.PostHeating_GPM = this.tbNewMSPPostHeatingGPM.Text;
            objMain.PostHeating_PD = this.tbNewMSPPostHeatingPD.Text;
            objMain.PostHeating_Glycol = this.tbNewMSPPostHeatingGlycol.Text;
            objMain.PostHeating_EAT = this.tbNewMSPPostHeatingEAT.Text;
            objMain.PostHeating_LAT = this.tbNewMSPPostHeatingLAT.Text;

            objMain.ModelNoVerifiedBy = this.tbNewMSPVerifiedBy.Text;

            if (cbNewMSPFrench.Checked == true)
            {
                objMain.French = "French";
            }
            else
            {
                objMain.French = "False";
            }
        }

        private void buttonNewMSPPrint_Click(object sender, EventArgs e)
        {
            string jobNumStr = this.tbNewMSPJobNum.Text;
            string modelNoStr = this.tbNewMSPModelNo.Text;
            string unitTypeStr = "";
            
            frmPrintRRU frmPrint = new frmPrintRRU();
            frmPrint.Text = "MSP Print ETL Label";                     

            try
            {
                if (modelNoStr.Substring(1,1) == "U")
                {
                    if (modelNoStr.Substring(10,1) == "A" || modelNoStr.Substring(10,1) == "B")
                    {
                        unitTypeStr = "DUCW";
                    }
                    else
                    {
                        unitTypeStr = "DUDX";
                    }
                }
                else
                {
                    if (modelNoStr.Substring(10, 1) == "A" || modelNoStr.Substring(10, 1) == "B")
                    {
                        unitTypeStr = "DVCW";
                    }
                    else
                    {
                        unitTypeStr = "DVDX";
                    }
                }

                ReportDocument cryRpt = new ReportDocument();

                ParameterValues curPV = new ParameterValues();

                ParameterFields paramFields = new ParameterFields();

                ParameterField pf1 = new ParameterField();
                ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
                pf1.Name = "@JobNum";
                pdv1.Value = jobNumStr;
                pf1.CurrentValues.Add(pdv1);

                paramFields.Add(pf1);

                ParameterField pf2 = new ParameterField();
                ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
                pf2.Name = "@UnitType";
                pdv2.Value = unitTypeStr;
                pf2.CurrentValues.Add(pdv2);

                paramFields.Add(pf2);

                frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

                EpicorServer = ConfigurationManager.AppSettings["EpicorAppServer"];

                string reportString = @"\\" + EpicorServer + "\\Apps\\OAU\\EtlLabel\\reports\\MSP_ProductLabel.v1.rpt";

#if DEBUG
                //cryRpt.Load(@"C:\Users\tonyt\Documents\Visual Studio 2013\Projects\EtlLabel\EtlLabel\reports\MSP_ProductLabel.v1.rpt");
                reportString = @"\\KCCWVTEPIC9APP2\Apps\OAU\EtlLabel\Reports\MSP_ProductLabel.v1.rpt";                              
#endif

                cryRpt.Load(reportString);

                frmPrint.crystalReportViewer1.ReportSource = cryRpt;
                frmPrint.ShowDialog();
                frmPrint.crystalReportViewer1.Refresh();

                if (cbNewMSPFrench.Checked == true)
                {
                    reportString = @"\\" + EpicorServer + "\\Apps\\OAU\\EtlLabel\\reports\\MSP_ProductLabel.v1.rpt";
#if DEBUG
                    reportString = @"\\KCCWVTEPIC9APP2\Apps\OAU\EtlLabel\Reports\MSP_ProductLabel_French.rpt";                     
#endif

                    cryRpt.Load(reportString);

                    frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

                    frmPrint.crystalReportViewer1.ReportSource = cryRpt;

                    frmPrint.ShowDialog();
                    frmPrint.crystalReportViewer1.Refresh();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR - Displaying MSP ETL Label - " + ex);
            }
            
        }       

        private void btnNewMSPCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }              
    }
}
