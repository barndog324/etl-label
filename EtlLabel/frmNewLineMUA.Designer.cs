namespace KCC.OA.Etl
{
    partial class frmNewLineMUA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxNewMUAPartNum = new System.Windows.Forms.TextBox();
            this.textBoxNewMUAVerifiedBy = new System.Windows.Forms.TextBox();
            this.labelNewMUAVerifiedBy = new System.Windows.Forms.Label();
            this.labelNewMUATag = new System.Windows.Forms.Label();
            this.labelNewMUASerialNo = new System.Windows.Forms.Label();
            this.textBoxNewMUASerialNo = new System.Windows.Forms.TextBox();
            this.textBoxNewMUAModelNo = new System.Windows.Forms.TextBox();
            this.labelNewMUAMsg2 = new System.Windows.Forms.Label();
            this.labelNewMUAMsg = new System.Windows.Forms.Label();
            this.buttonNewMUAAccept = new System.Windows.Forms.Button();
            this.buttonNewMUACancel = new System.Windows.Forms.Button();
            this.labelNewMUAModelNum = new System.Windows.Forms.Label();
            this.textBoxNewMUAAirMaxExtPressure = new System.Windows.Forms.TextBox();
            this.buttonNewMUAPrint = new System.Windows.Forms.Button();
            this.groupBoxNewMUAVoltageInfo = new System.Windows.Forms.GroupBox();
            this.textBoxNewMUAGasOutput = new System.Windows.Forms.TextBox();
            this.labelNewMUAGasOutput = new System.Windows.Forms.Label();
            this.textBoxNewMUAGasInput = new System.Windows.Forms.TextBox();
            this.labelNewMUAGasInput = new System.Windows.Forms.Label();
            this.textBoxNewMUAAirTempRise = new System.Windows.Forms.TextBox();
            this.labelNewMUAAirTempRise = new System.Windows.Forms.Label();
            this.labelNewMUAAirMaxExtPressure = new System.Windows.Forms.Label();
            this.textBoxNewMUAAirMaxTotalPressure = new System.Windows.Forms.TextBox();
            this.labelNewMUAAirMaxTotalPressure = new System.Windows.Forms.Label();
            this.textBoxNewMUAAirSCFM = new System.Windows.Forms.TextBox();
            this.labelNewMUAAirSCFM = new System.Windows.Forms.Label();
            this.textBoxNewMUAElecFLA = new System.Windows.Forms.TextBox();
            this.labelNewMUAElecFLA = new System.Windows.Forms.Label();
            this.textBoxNewMUAElecMaxVoltage = new System.Windows.Forms.TextBox();
            this.labelNewMUAElecMaxVoltage = new System.Windows.Forms.Label();
            this.textBoxNewMUAElecMaxVoltageHertz = new System.Windows.Forms.TextBox();
            this.labelNewMUAElecMaxVoltageHertz = new System.Windows.Forms.Label();
            this.textBoxNewMUAElecMinVoltage = new System.Windows.Forms.TextBox();
            this.labelNewMUAElecMinVoltage = new System.Windows.Forms.Label();
            this.textBoxNewMUAElecHertz = new System.Windows.Forms.TextBox();
            this.labelNewMUAElecHertz = new System.Windows.Forms.Label();
            this.textBoxNewMUAElecPhase = new System.Windows.Forms.TextBox();
            this.labelNewMUAElecPhase = new System.Windows.Forms.Label();
            this.textBoxNewMUAElecVolts = new System.Windows.Forms.TextBox();
            this.labelNewMUAElecVolts = new System.Windows.Forms.Label();
            this.textBoxNewMUAElecBlowerFan = new System.Windows.Forms.TextBox();
            this.labelNewMUAElecBlowerFan = new System.Windows.Forms.Label();
            this.textBoxNewMUAReleaseNum = new System.Windows.Forms.TextBox();
            this.labelNewMUASlash = new System.Windows.Forms.Label();
            this.groupBoxNewMUAJobInfo = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxNewMUAJobNum = new System.Windows.Forms.TextBox();
            this.textBoxNewMUAEndConsumer = new System.Windows.Forms.TextBox();
            this.labelNewMUAEndConsumer = new System.Windows.Forms.Label();
            this.textBoxNewMUACustomer = new System.Windows.Forms.TextBox();
            this.textBoxNewMUAOrderNum = new System.Windows.Forms.TextBox();
            this.labelNewMUACustomer = new System.Windows.Forms.Label();
            this.labelNewMUAOrderNum = new System.Windows.Forms.Label();
            this.textBoxNewMUALineItem = new System.Windows.Forms.TextBox();
            this.labelNewMUALineItem = new System.Windows.Forms.Label();
            this.textBoxNewMUATag = new System.Windows.Forms.TextBox();
            this.groupBoxNewMUAUnitInfo = new System.Windows.Forms.GroupBox();
            this.labelNewMUAPartNum = new System.Windows.Forms.Label();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkBoxNewMUAFrench = new System.Windows.Forms.CheckBox();
            this.menuStripNewFAN = new System.Windows.Forms.MenuStrip();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBoxNewMUAVoltageInfo.SuspendLayout();
            this.groupBoxNewMUAJobInfo.SuspendLayout();
            this.groupBoxNewMUAUnitInfo.SuspendLayout();
            this.menuStripNewFAN.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxNewMUAPartNum
            // 
            this.textBoxNewMUAPartNum.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUAPartNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUAPartNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUAPartNum.Location = new System.Drawing.Point(76, 46);
            this.textBoxNewMUAPartNum.Name = "textBoxNewMUAPartNum";
            this.textBoxNewMUAPartNum.Size = new System.Drawing.Size(124, 26);
            this.textBoxNewMUAPartNum.TabIndex = 2;
            this.textBoxNewMUAPartNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxNewMUAVerifiedBy
            // 
            this.textBoxNewMUAVerifiedBy.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUAVerifiedBy.Enabled = false;
            this.textBoxNewMUAVerifiedBy.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUAVerifiedBy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUAVerifiedBy.Location = new System.Drawing.Point(235, 100);
            this.textBoxNewMUAVerifiedBy.Name = "textBoxNewMUAVerifiedBy";
            this.textBoxNewMUAVerifiedBy.Size = new System.Drawing.Size(67, 26);
            this.textBoxNewMUAVerifiedBy.TabIndex = 63;
            this.textBoxNewMUAVerifiedBy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxNewMUAVerifiedBy.Visible = false;
            // 
            // labelNewMUAVerifiedBy
            // 
            this.labelNewMUAVerifiedBy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUAVerifiedBy.Location = new System.Drawing.Point(233, 76);
            this.labelNewMUAVerifiedBy.Name = "labelNewMUAVerifiedBy";
            this.labelNewMUAVerifiedBy.Size = new System.Drawing.Size(75, 20);
            this.labelNewMUAVerifiedBy.TabIndex = 12;
            this.labelNewMUAVerifiedBy.Text = "Created By:";
            this.labelNewMUAVerifiedBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelNewMUAVerifiedBy.Visible = false;
            // 
            // labelNewMUATag
            // 
            this.labelNewMUATag.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUATag.Location = new System.Drawing.Point(7, 100);
            this.labelNewMUATag.Name = "labelNewMUATag";
            this.labelNewMUATag.Size = new System.Drawing.Size(65, 21);
            this.labelNewMUATag.TabIndex = 8;
            this.labelNewMUATag.Text = "Tag:";
            this.labelNewMUATag.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelNewMUASerialNo
            // 
            this.labelNewMUASerialNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUASerialNo.Location = new System.Drawing.Point(7, 73);
            this.labelNewMUASerialNo.Name = "labelNewMUASerialNo";
            this.labelNewMUASerialNo.Size = new System.Drawing.Size(65, 21);
            this.labelNewMUASerialNo.TabIndex = 7;
            this.labelNewMUASerialNo.Text = "Serial No:";
            this.labelNewMUASerialNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewMUASerialNo
            // 
            this.textBoxNewMUASerialNo.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUASerialNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUASerialNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUASerialNo.Location = new System.Drawing.Point(76, 73);
            this.textBoxNewMUASerialNo.Name = "textBoxNewMUASerialNo";
            this.textBoxNewMUASerialNo.Size = new System.Drawing.Size(108, 26);
            this.textBoxNewMUASerialNo.TabIndex = 3;
            this.textBoxNewMUASerialNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxNewMUAModelNo
            // 
            this.textBoxNewMUAModelNo.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUAModelNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUAModelNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUAModelNo.Location = new System.Drawing.Point(76, 19);
            this.textBoxNewMUAModelNo.Name = "textBoxNewMUAModelNo";
            this.textBoxNewMUAModelNo.Size = new System.Drawing.Size(227, 26);
            this.textBoxNewMUAModelNo.TabIndex = 1;
            this.textBoxNewMUAModelNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelNewMUAMsg2
            // 
            this.labelNewMUAMsg2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.labelNewMUAMsg2.Location = new System.Drawing.Point(9, 558);
            this.labelNewMUAMsg2.Name = "labelNewMUAMsg2";
            this.labelNewMUAMsg2.Size = new System.Drawing.Size(491, 21);
            this.labelNewMUAMsg2.TabIndex = 35;
            this.labelNewMUAMsg2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelNewMUAMsg
            // 
            this.labelNewMUAMsg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.labelNewMUAMsg.Location = new System.Drawing.Point(9, 536);
            this.labelNewMUAMsg.Name = "labelNewMUAMsg";
            this.labelNewMUAMsg.Size = new System.Drawing.Size(491, 21);
            this.labelNewMUAMsg.TabIndex = 34;
            this.labelNewMUAMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonNewMUAAccept
            // 
            this.buttonNewMUAAccept.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewMUAAccept.Location = new System.Drawing.Point(366, 158);
            this.buttonNewMUAAccept.Name = "buttonNewMUAAccept";
            this.buttonNewMUAAccept.Size = new System.Drawing.Size(95, 37);
            this.buttonNewMUAAccept.TabIndex = 32;
            this.buttonNewMUAAccept.Text = "Accept";
            this.buttonNewMUAAccept.UseVisualStyleBackColor = true;
            this.buttonNewMUAAccept.Click += new System.EventHandler(this.buttonNewMUAAccept_Click);
            // 
            // buttonNewMUACancel
            // 
            this.buttonNewMUACancel.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewMUACancel.ForeColor = System.Drawing.Color.Red;
            this.buttonNewMUACancel.Location = new System.Drawing.Point(366, 109);
            this.buttonNewMUACancel.Name = "buttonNewMUACancel";
            this.buttonNewMUACancel.Size = new System.Drawing.Size(95, 37);
            this.buttonNewMUACancel.TabIndex = 31;
            this.buttonNewMUACancel.Text = "Cancel";
            this.buttonNewMUACancel.UseVisualStyleBackColor = true;
            this.buttonNewMUACancel.Click += new System.EventHandler(this.buttonNewMUACancel_Click);
            // 
            // labelNewMUAModelNum
            // 
            this.labelNewMUAModelNum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUAModelNum.Location = new System.Drawing.Point(7, 19);
            this.labelNewMUAModelNum.Name = "labelNewMUAModelNum";
            this.labelNewMUAModelNum.Size = new System.Drawing.Size(65, 21);
            this.labelNewMUAModelNum.TabIndex = 4;
            this.labelNewMUAModelNum.Text = "Model No:";
            this.labelNewMUAModelNum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewMUAAirMaxExtPressure
            // 
            this.textBoxNewMUAAirMaxExtPressure.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUAAirMaxExtPressure.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUAAirMaxExtPressure.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUAAirMaxExtPressure.Location = new System.Drawing.Point(407, 73);
            this.textBoxNewMUAAirMaxExtPressure.Name = "textBoxNewMUAAirMaxExtPressure";
            this.textBoxNewMUAAirMaxExtPressure.Size = new System.Drawing.Size(75, 26);
            this.textBoxNewMUAAirMaxExtPressure.TabIndex = 11;
            this.textBoxNewMUAAirMaxExtPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // buttonNewMUAPrint
            // 
            this.buttonNewMUAPrint.Enabled = false;
            this.buttonNewMUAPrint.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewMUAPrint.Location = new System.Drawing.Point(366, 206);
            this.buttonNewMUAPrint.Name = "buttonNewMUAPrint";
            this.buttonNewMUAPrint.Size = new System.Drawing.Size(95, 37);
            this.buttonNewMUAPrint.TabIndex = 33;
            this.buttonNewMUAPrint.Text = "Print";
            this.buttonNewMUAPrint.UseVisualStyleBackColor = true;
            this.buttonNewMUAPrint.Click += new System.EventHandler(this.buttonNewMUAPrint_Click);
            // 
            // groupBoxNewMUAVoltageInfo
            // 
            this.groupBoxNewMUAVoltageInfo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.textBoxNewMUAGasOutput);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.labelNewMUAGasOutput);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.textBoxNewMUAGasInput);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.labelNewMUAGasInput);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.textBoxNewMUAAirTempRise);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.labelNewMUAAirTempRise);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.textBoxNewMUAAirMaxExtPressure);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.labelNewMUAAirMaxExtPressure);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.textBoxNewMUAAirMaxTotalPressure);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.labelNewMUAAirMaxTotalPressure);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.textBoxNewMUAAirSCFM);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.labelNewMUAAirSCFM);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.textBoxNewMUAElecFLA);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.labelNewMUAElecFLA);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.textBoxNewMUAElecMaxVoltage);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.labelNewMUAElecMaxVoltage);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.textBoxNewMUAElecMaxVoltageHertz);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.labelNewMUAElecMaxVoltageHertz);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.textBoxNewMUAElecMinVoltage);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.labelNewMUAElecMinVoltage);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.textBoxNewMUAElecHertz);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.labelNewMUAElecHertz);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.textBoxNewMUAElecPhase);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.labelNewMUAElecPhase);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.textBoxNewMUAElecVolts);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.labelNewMUAElecVolts);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.textBoxNewMUAElecBlowerFan);
            this.groupBoxNewMUAVoltageInfo.Controls.Add(this.labelNewMUAElecBlowerFan);
            this.groupBoxNewMUAVoltageInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewMUAVoltageInfo.Location = new System.Drawing.Point(6, 302);
            this.groupBoxNewMUAVoltageInfo.Name = "groupBoxNewMUAVoltageInfo";
            this.groupBoxNewMUAVoltageInfo.Size = new System.Drawing.Size(491, 240);
            this.groupBoxNewMUAVoltageInfo.TabIndex = 29;
            this.groupBoxNewMUAVoltageInfo.TabStop = false;
            this.groupBoxNewMUAVoltageInfo.Text = "FAN";
            // 
            // textBoxNewMUAGasOutput
            // 
            this.textBoxNewMUAGasOutput.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUAGasOutput.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUAGasOutput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUAGasOutput.Location = new System.Drawing.Point(407, 181);
            this.textBoxNewMUAGasOutput.Name = "textBoxNewMUAGasOutput";
            this.textBoxNewMUAGasOutput.Size = new System.Drawing.Size(75, 26);
            this.textBoxNewMUAGasOutput.TabIndex = 65;
            this.textBoxNewMUAGasOutput.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewMUAGasOutput
            // 
            this.labelNewMUAGasOutput.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUAGasOutput.Location = new System.Drawing.Point(266, 181);
            this.labelNewMUAGasOutput.Name = "labelNewMUAGasOutput";
            this.labelNewMUAGasOutput.Size = new System.Drawing.Size(138, 21);
            this.labelNewMUAGasOutput.TabIndex = 66;
            this.labelNewMUAGasOutput.Text = "Gas Output:";
            this.labelNewMUAGasOutput.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewMUAGasInput
            // 
            this.textBoxNewMUAGasInput.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUAGasInput.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUAGasInput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUAGasInput.Location = new System.Drawing.Point(407, 154);
            this.textBoxNewMUAGasInput.Name = "textBoxNewMUAGasInput";
            this.textBoxNewMUAGasInput.Size = new System.Drawing.Size(75, 26);
            this.textBoxNewMUAGasInput.TabIndex = 63;
            this.textBoxNewMUAGasInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewMUAGasInput
            // 
            this.labelNewMUAGasInput.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUAGasInput.Location = new System.Drawing.Point(266, 154);
            this.labelNewMUAGasInput.Name = "labelNewMUAGasInput";
            this.labelNewMUAGasInput.Size = new System.Drawing.Size(138, 21);
            this.labelNewMUAGasInput.TabIndex = 64;
            this.labelNewMUAGasInput.Text = "Gas Input:";
            this.labelNewMUAGasInput.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewMUAAirTempRise
            // 
            this.textBoxNewMUAAirTempRise.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUAAirTempRise.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUAAirTempRise.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUAAirTempRise.Location = new System.Drawing.Point(407, 100);
            this.textBoxNewMUAAirTempRise.Name = "textBoxNewMUAAirTempRise";
            this.textBoxNewMUAAirTempRise.Size = new System.Drawing.Size(75, 26);
            this.textBoxNewMUAAirTempRise.TabIndex = 61;
            this.textBoxNewMUAAirTempRise.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewMUAAirTempRise
            // 
            this.labelNewMUAAirTempRise.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUAAirTempRise.Location = new System.Drawing.Point(266, 100);
            this.labelNewMUAAirTempRise.Name = "labelNewMUAAirTempRise";
            this.labelNewMUAAirTempRise.Size = new System.Drawing.Size(138, 21);
            this.labelNewMUAAirTempRise.TabIndex = 62;
            this.labelNewMUAAirTempRise.Text = "Air Temp Rise:";
            this.labelNewMUAAirTempRise.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelNewMUAAirMaxExtPressure
            // 
            this.labelNewMUAAirMaxExtPressure.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUAAirMaxExtPressure.Location = new System.Drawing.Point(266, 73);
            this.labelNewMUAAirMaxExtPressure.Name = "labelNewMUAAirMaxExtPressure";
            this.labelNewMUAAirMaxExtPressure.Size = new System.Drawing.Size(138, 21);
            this.labelNewMUAAirMaxExtPressure.TabIndex = 60;
            this.labelNewMUAAirMaxExtPressure.Text = "Air Max Ext Pressure:";
            this.labelNewMUAAirMaxExtPressure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewMUAAirMaxTotalPressure
            // 
            this.textBoxNewMUAAirMaxTotalPressure.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUAAirMaxTotalPressure.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUAAirMaxTotalPressure.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUAAirMaxTotalPressure.Location = new System.Drawing.Point(407, 46);
            this.textBoxNewMUAAirMaxTotalPressure.Name = "textBoxNewMUAAirMaxTotalPressure";
            this.textBoxNewMUAAirMaxTotalPressure.Size = new System.Drawing.Size(75, 26);
            this.textBoxNewMUAAirMaxTotalPressure.TabIndex = 10;
            this.textBoxNewMUAAirMaxTotalPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewMUAAirMaxTotalPressure
            // 
            this.labelNewMUAAirMaxTotalPressure.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUAAirMaxTotalPressure.Location = new System.Drawing.Point(266, 46);
            this.labelNewMUAAirMaxTotalPressure.Name = "labelNewMUAAirMaxTotalPressure";
            this.labelNewMUAAirMaxTotalPressure.Size = new System.Drawing.Size(138, 21);
            this.labelNewMUAAirMaxTotalPressure.TabIndex = 58;
            this.labelNewMUAAirMaxTotalPressure.Text = "Air Max Total Pressure:";
            this.labelNewMUAAirMaxTotalPressure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewMUAAirSCFM
            // 
            this.textBoxNewMUAAirSCFM.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUAAirSCFM.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUAAirSCFM.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUAAirSCFM.Location = new System.Drawing.Point(407, 19);
            this.textBoxNewMUAAirSCFM.Name = "textBoxNewMUAAirSCFM";
            this.textBoxNewMUAAirSCFM.Size = new System.Drawing.Size(75, 26);
            this.textBoxNewMUAAirSCFM.TabIndex = 9;
            this.textBoxNewMUAAirSCFM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewMUAAirSCFM
            // 
            this.labelNewMUAAirSCFM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUAAirSCFM.Location = new System.Drawing.Point(266, 19);
            this.labelNewMUAAirSCFM.Name = "labelNewMUAAirSCFM";
            this.labelNewMUAAirSCFM.Size = new System.Drawing.Size(138, 21);
            this.labelNewMUAAirSCFM.TabIndex = 56;
            this.labelNewMUAAirSCFM.Text = "Air SCFM:";
            this.labelNewMUAAirSCFM.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewMUAElecFLA
            // 
            this.textBoxNewMUAElecFLA.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUAElecFLA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUAElecFLA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUAElecFLA.Location = new System.Drawing.Point(170, 127);
            this.textBoxNewMUAElecFLA.Name = "textBoxNewMUAElecFLA";
            this.textBoxNewMUAElecFLA.Size = new System.Drawing.Size(90, 26);
            this.textBoxNewMUAElecFLA.TabIndex = 5;
            this.textBoxNewMUAElecFLA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewMUAElecFLA
            // 
            this.labelNewMUAElecFLA.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUAElecFLA.Location = new System.Drawing.Point(9, 127);
            this.labelNewMUAElecFLA.Name = "labelNewMUAElecFLA";
            this.labelNewMUAElecFLA.Size = new System.Drawing.Size(158, 21);
            this.labelNewMUAElecFLA.TabIndex = 54;
            this.labelNewMUAElecFLA.Text = "Electric FLA:";
            this.labelNewMUAElecFLA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewMUAElecMaxVoltage
            // 
            this.textBoxNewMUAElecMaxVoltage.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUAElecMaxVoltage.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUAElecMaxVoltage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUAElecMaxVoltage.Location = new System.Drawing.Point(170, 181);
            this.textBoxNewMUAElecMaxVoltage.Name = "textBoxNewMUAElecMaxVoltage";
            this.textBoxNewMUAElecMaxVoltage.Size = new System.Drawing.Size(90, 26);
            this.textBoxNewMUAElecMaxVoltage.TabIndex = 7;
            this.textBoxNewMUAElecMaxVoltage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewMUAElecMaxVoltage
            // 
            this.labelNewMUAElecMaxVoltage.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUAElecMaxVoltage.Location = new System.Drawing.Point(9, 181);
            this.labelNewMUAElecMaxVoltage.Name = "labelNewMUAElecMaxVoltage";
            this.labelNewMUAElecMaxVoltage.Size = new System.Drawing.Size(158, 21);
            this.labelNewMUAElecMaxVoltage.TabIndex = 50;
            this.labelNewMUAElecMaxVoltage.Text = "Electric Max Voltage:";
            this.labelNewMUAElecMaxVoltage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewMUAElecMaxVoltageHertz
            // 
            this.textBoxNewMUAElecMaxVoltageHertz.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUAElecMaxVoltageHertz.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUAElecMaxVoltageHertz.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUAElecMaxVoltageHertz.Location = new System.Drawing.Point(170, 208);
            this.textBoxNewMUAElecMaxVoltageHertz.Name = "textBoxNewMUAElecMaxVoltageHertz";
            this.textBoxNewMUAElecMaxVoltageHertz.Size = new System.Drawing.Size(90, 26);
            this.textBoxNewMUAElecMaxVoltageHertz.TabIndex = 8;
            this.textBoxNewMUAElecMaxVoltageHertz.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewMUAElecMaxVoltageHertz
            // 
            this.labelNewMUAElecMaxVoltageHertz.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUAElecMaxVoltageHertz.Location = new System.Drawing.Point(9, 208);
            this.labelNewMUAElecMaxVoltageHertz.Name = "labelNewMUAElecMaxVoltageHertz";
            this.labelNewMUAElecMaxVoltageHertz.Size = new System.Drawing.Size(158, 21);
            this.labelNewMUAElecMaxVoltageHertz.TabIndex = 48;
            this.labelNewMUAElecMaxVoltageHertz.Text = "Electric Max Voltage Hertz:";
            this.labelNewMUAElecMaxVoltageHertz.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewMUAElecMinVoltage
            // 
            this.textBoxNewMUAElecMinVoltage.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUAElecMinVoltage.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUAElecMinVoltage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUAElecMinVoltage.Location = new System.Drawing.Point(170, 154);
            this.textBoxNewMUAElecMinVoltage.Name = "textBoxNewMUAElecMinVoltage";
            this.textBoxNewMUAElecMinVoltage.Size = new System.Drawing.Size(90, 26);
            this.textBoxNewMUAElecMinVoltage.TabIndex = 6;
            this.textBoxNewMUAElecMinVoltage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewMUAElecMinVoltage
            // 
            this.labelNewMUAElecMinVoltage.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUAElecMinVoltage.Location = new System.Drawing.Point(9, 154);
            this.labelNewMUAElecMinVoltage.Name = "labelNewMUAElecMinVoltage";
            this.labelNewMUAElecMinVoltage.Size = new System.Drawing.Size(158, 21);
            this.labelNewMUAElecMinVoltage.TabIndex = 46;
            this.labelNewMUAElecMinVoltage.Text = "Electric Min Voltage:";
            this.labelNewMUAElecMinVoltage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewMUAElecHertz
            // 
            this.textBoxNewMUAElecHertz.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUAElecHertz.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUAElecHertz.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUAElecHertz.Location = new System.Drawing.Point(170, 100);
            this.textBoxNewMUAElecHertz.Name = "textBoxNewMUAElecHertz";
            this.textBoxNewMUAElecHertz.Size = new System.Drawing.Size(90, 26);
            this.textBoxNewMUAElecHertz.TabIndex = 4;
            this.textBoxNewMUAElecHertz.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewMUAElecHertz
            // 
            this.labelNewMUAElecHertz.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUAElecHertz.Location = new System.Drawing.Point(6, 100);
            this.labelNewMUAElecHertz.Name = "labelNewMUAElecHertz";
            this.labelNewMUAElecHertz.Size = new System.Drawing.Size(158, 21);
            this.labelNewMUAElecHertz.TabIndex = 44;
            this.labelNewMUAElecHertz.Text = "Electric Hertz:";
            this.labelNewMUAElecHertz.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewMUAElecPhase
            // 
            this.textBoxNewMUAElecPhase.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUAElecPhase.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUAElecPhase.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUAElecPhase.Location = new System.Drawing.Point(170, 73);
            this.textBoxNewMUAElecPhase.Name = "textBoxNewMUAElecPhase";
            this.textBoxNewMUAElecPhase.Size = new System.Drawing.Size(90, 26);
            this.textBoxNewMUAElecPhase.TabIndex = 3;
            this.textBoxNewMUAElecPhase.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewMUAElecPhase
            // 
            this.labelNewMUAElecPhase.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUAElecPhase.Location = new System.Drawing.Point(9, 73);
            this.labelNewMUAElecPhase.Name = "labelNewMUAElecPhase";
            this.labelNewMUAElecPhase.Size = new System.Drawing.Size(158, 21);
            this.labelNewMUAElecPhase.TabIndex = 42;
            this.labelNewMUAElecPhase.Text = "Electric Phase:";
            this.labelNewMUAElecPhase.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewMUAElecVolts
            // 
            this.textBoxNewMUAElecVolts.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUAElecVolts.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUAElecVolts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUAElecVolts.Location = new System.Drawing.Point(170, 46);
            this.textBoxNewMUAElecVolts.Name = "textBoxNewMUAElecVolts";
            this.textBoxNewMUAElecVolts.Size = new System.Drawing.Size(90, 26);
            this.textBoxNewMUAElecVolts.TabIndex = 2;
            this.textBoxNewMUAElecVolts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewMUAElecVolts
            // 
            this.labelNewMUAElecVolts.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUAElecVolts.Location = new System.Drawing.Point(9, 46);
            this.labelNewMUAElecVolts.Name = "labelNewMUAElecVolts";
            this.labelNewMUAElecVolts.Size = new System.Drawing.Size(158, 21);
            this.labelNewMUAElecVolts.TabIndex = 40;
            this.labelNewMUAElecVolts.Text = "Electric Volts:";
            this.labelNewMUAElecVolts.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewMUAElecBlowerFan
            // 
            this.textBoxNewMUAElecBlowerFan.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUAElecBlowerFan.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUAElecBlowerFan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUAElecBlowerFan.Location = new System.Drawing.Point(170, 19);
            this.textBoxNewMUAElecBlowerFan.Name = "textBoxNewMUAElecBlowerFan";
            this.textBoxNewMUAElecBlowerFan.Size = new System.Drawing.Size(90, 26);
            this.textBoxNewMUAElecBlowerFan.TabIndex = 1;
            this.textBoxNewMUAElecBlowerFan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewMUAElecBlowerFan
            // 
            this.labelNewMUAElecBlowerFan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUAElecBlowerFan.Location = new System.Drawing.Point(9, 19);
            this.labelNewMUAElecBlowerFan.Name = "labelNewMUAElecBlowerFan";
            this.labelNewMUAElecBlowerFan.Size = new System.Drawing.Size(158, 21);
            this.labelNewMUAElecBlowerFan.TabIndex = 38;
            this.labelNewMUAElecBlowerFan.Text = "Electric Blower Fan:";
            this.labelNewMUAElecBlowerFan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewMUAReleaseNum
            // 
            this.textBoxNewMUAReleaseNum.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUAReleaseNum.Enabled = false;
            this.textBoxNewMUAReleaseNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUAReleaseNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUAReleaseNum.Location = new System.Drawing.Point(284, 49);
            this.textBoxNewMUAReleaseNum.Name = "textBoxNewMUAReleaseNum";
            this.textBoxNewMUAReleaseNum.Size = new System.Drawing.Size(21, 26);
            this.textBoxNewMUAReleaseNum.TabIndex = 4;
            this.textBoxNewMUAReleaseNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewMUASlash
            // 
            this.labelNewMUASlash.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUASlash.Location = new System.Drawing.Point(269, 49);
            this.labelNewMUASlash.Name = "labelNewMUASlash";
            this.labelNewMUASlash.Size = new System.Drawing.Size(15, 21);
            this.labelNewMUASlash.TabIndex = 4;
            this.labelNewMUASlash.Text = "/";
            this.labelNewMUASlash.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBoxNewMUAJobInfo
            // 
            this.groupBoxNewMUAJobInfo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBoxNewMUAJobInfo.Controls.Add(this.label1);
            this.groupBoxNewMUAJobInfo.Controls.Add(this.textBoxNewMUAJobNum);
            this.groupBoxNewMUAJobInfo.Controls.Add(this.textBoxNewMUAEndConsumer);
            this.groupBoxNewMUAJobInfo.Controls.Add(this.labelNewMUAEndConsumer);
            this.groupBoxNewMUAJobInfo.Controls.Add(this.textBoxNewMUAReleaseNum);
            this.groupBoxNewMUAJobInfo.Controls.Add(this.labelNewMUASlash);
            this.groupBoxNewMUAJobInfo.Controls.Add(this.textBoxNewMUACustomer);
            this.groupBoxNewMUAJobInfo.Controls.Add(this.textBoxNewMUAOrderNum);
            this.groupBoxNewMUAJobInfo.Controls.Add(this.labelNewMUACustomer);
            this.groupBoxNewMUAJobInfo.Controls.Add(this.labelNewMUAOrderNum);
            this.groupBoxNewMUAJobInfo.Controls.Add(this.textBoxNewMUALineItem);
            this.groupBoxNewMUAJobInfo.Controls.Add(this.labelNewMUALineItem);
            this.groupBoxNewMUAJobInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewMUAJobInfo.Location = new System.Drawing.Point(6, 22);
            this.groupBoxNewMUAJobInfo.Name = "groupBoxNewMUAJobInfo";
            this.groupBoxNewMUAJobInfo.Size = new System.Drawing.Size(311, 135);
            this.groupBoxNewMUAJobInfo.TabIndex = 26;
            this.groupBoxNewMUAJobInfo.TabStop = false;
            this.groupBoxNewMUAJobInfo.Text = "Order Info";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 21);
            this.label1.TabIndex = 67;
            this.label1.Text = "Job #:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxNewMUAJobNum
            // 
            this.textBoxNewMUAJobNum.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUAJobNum.Enabled = false;
            this.textBoxNewMUAJobNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUAJobNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUAJobNum.Location = new System.Drawing.Point(95, 21);
            this.textBoxNewMUAJobNum.Name = "textBoxNewMUAJobNum";
            this.textBoxNewMUAJobNum.Size = new System.Drawing.Size(97, 26);
            this.textBoxNewMUAJobNum.TabIndex = 66;
            this.textBoxNewMUAJobNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxNewMUAEndConsumer
            // 
            this.textBoxNewMUAEndConsumer.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUAEndConsumer.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUAEndConsumer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUAEndConsumer.Location = new System.Drawing.Point(110, 103);
            this.textBoxNewMUAEndConsumer.Name = "textBoxNewMUAEndConsumer";
            this.textBoxNewMUAEndConsumer.Size = new System.Drawing.Size(195, 26);
            this.textBoxNewMUAEndConsumer.TabIndex = 8;
            // 
            // labelNewMUAEndConsumer
            // 
            this.labelNewMUAEndConsumer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUAEndConsumer.Location = new System.Drawing.Point(4, 103);
            this.labelNewMUAEndConsumer.Name = "labelNewMUAEndConsumer";
            this.labelNewMUAEndConsumer.Size = new System.Drawing.Size(104, 21);
            this.labelNewMUAEndConsumer.TabIndex = 7;
            this.labelNewMUAEndConsumer.Text = "End Consumer:";
            this.labelNewMUAEndConsumer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewMUACustomer
            // 
            this.textBoxNewMUACustomer.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUACustomer.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUACustomer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUACustomer.Location = new System.Drawing.Point(95, 76);
            this.textBoxNewMUACustomer.Name = "textBoxNewMUACustomer";
            this.textBoxNewMUACustomer.Size = new System.Drawing.Size(210, 26);
            this.textBoxNewMUACustomer.TabIndex = 2;
            // 
            // textBoxNewMUAOrderNum
            // 
            this.textBoxNewMUAOrderNum.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUAOrderNum.Enabled = false;
            this.textBoxNewMUAOrderNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUAOrderNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUAOrderNum.Location = new System.Drawing.Point(95, 49);
            this.textBoxNewMUAOrderNum.Name = "textBoxNewMUAOrderNum";
            this.textBoxNewMUAOrderNum.Size = new System.Drawing.Size(50, 26);
            this.textBoxNewMUAOrderNum.TabIndex = 1;
            this.textBoxNewMUAOrderNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelNewMUACustomer
            // 
            this.labelNewMUACustomer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUACustomer.Location = new System.Drawing.Point(4, 76);
            this.labelNewMUACustomer.Name = "labelNewMUACustomer";
            this.labelNewMUACustomer.Size = new System.Drawing.Size(85, 21);
            this.labelNewMUACustomer.TabIndex = 2;
            this.labelNewMUACustomer.Text = "Customer:";
            this.labelNewMUACustomer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelNewMUAOrderNum
            // 
            this.labelNewMUAOrderNum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUAOrderNum.Location = new System.Drawing.Point(4, 49);
            this.labelNewMUAOrderNum.Name = "labelNewMUAOrderNum";
            this.labelNewMUAOrderNum.Size = new System.Drawing.Size(85, 21);
            this.labelNewMUAOrderNum.TabIndex = 1;
            this.labelNewMUAOrderNum.Text = "Sales Order#:";
            this.labelNewMUAOrderNum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxNewMUALineItem
            // 
            this.textBoxNewMUALineItem.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUALineItem.Enabled = false;
            this.textBoxNewMUALineItem.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUALineItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUALineItem.Location = new System.Drawing.Point(243, 49);
            this.textBoxNewMUALineItem.Name = "textBoxNewMUALineItem";
            this.textBoxNewMUALineItem.Size = new System.Drawing.Size(21, 26);
            this.textBoxNewMUALineItem.TabIndex = 3;
            this.textBoxNewMUALineItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewMUALineItem
            // 
            this.labelNewMUALineItem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUALineItem.Location = new System.Drawing.Point(154, 49);
            this.labelNewMUALineItem.Name = "labelNewMUALineItem";
            this.labelNewMUALineItem.Size = new System.Drawing.Size(85, 21);
            this.labelNewMUALineItem.TabIndex = 2;
            this.labelNewMUALineItem.Text = "Line #/Rel #:";
            this.labelNewMUALineItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewMUATag
            // 
            this.textBoxNewMUATag.BackColor = System.Drawing.Color.White;
            this.textBoxNewMUATag.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewMUATag.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewMUATag.Location = new System.Drawing.Point(76, 100);
            this.textBoxNewMUATag.Name = "textBoxNewMUATag";
            this.textBoxNewMUATag.Size = new System.Drawing.Size(108, 26);
            this.textBoxNewMUATag.TabIndex = 66;
            this.textBoxNewMUATag.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBoxNewMUAUnitInfo
            // 
            this.groupBoxNewMUAUnitInfo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBoxNewMUAUnitInfo.Controls.Add(this.textBoxNewMUATag);
            this.groupBoxNewMUAUnitInfo.Controls.Add(this.labelNewMUAPartNum);
            this.groupBoxNewMUAUnitInfo.Controls.Add(this.textBoxNewMUAPartNum);
            this.groupBoxNewMUAUnitInfo.Controls.Add(this.textBoxNewMUAVerifiedBy);
            this.groupBoxNewMUAUnitInfo.Controls.Add(this.labelNewMUAVerifiedBy);
            this.groupBoxNewMUAUnitInfo.Controls.Add(this.labelNewMUATag);
            this.groupBoxNewMUAUnitInfo.Controls.Add(this.labelNewMUASerialNo);
            this.groupBoxNewMUAUnitInfo.Controls.Add(this.textBoxNewMUASerialNo);
            this.groupBoxNewMUAUnitInfo.Controls.Add(this.textBoxNewMUAModelNo);
            this.groupBoxNewMUAUnitInfo.Controls.Add(this.labelNewMUAModelNum);
            this.groupBoxNewMUAUnitInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewMUAUnitInfo.Location = new System.Drawing.Point(6, 166);
            this.groupBoxNewMUAUnitInfo.Name = "groupBoxNewMUAUnitInfo";
            this.groupBoxNewMUAUnitInfo.Size = new System.Drawing.Size(311, 131);
            this.groupBoxNewMUAUnitInfo.TabIndex = 27;
            this.groupBoxNewMUAUnitInfo.TabStop = false;
            this.groupBoxNewMUAUnitInfo.Text = "Unit Info";
            // 
            // labelNewMUAPartNum
            // 
            this.labelNewMUAPartNum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewMUAPartNum.Location = new System.Drawing.Point(7, 46);
            this.labelNewMUAPartNum.Name = "labelNewMUAPartNum";
            this.labelNewMUAPartNum.Size = new System.Drawing.Size(65, 21);
            this.labelNewMUAPartNum.TabIndex = 65;
            this.labelNewMUAPartNum.Text = "Part Num:";
            this.labelNewMUAPartNum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(52, 25);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(112, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // checkBoxNewMUAFrench
            // 
            this.checkBoxNewMUAFrench.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxNewMUAFrench.Location = new System.Drawing.Point(338, 22);
            this.checkBoxNewMUAFrench.Name = "checkBoxNewMUAFrench";
            this.checkBoxNewMUAFrench.Size = new System.Drawing.Size(141, 20);
            this.checkBoxNewMUAFrench.TabIndex = 30;
            this.checkBoxNewMUAFrench.Text = "French Canadian Order";
            this.checkBoxNewMUAFrench.UseVisualStyleBackColor = true;
            // 
            // menuStripNewFAN
            // 
            this.menuStripNewFAN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.menuStripNewFAN.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStripNewFAN.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStripNewFAN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStripNewFAN.Location = new System.Drawing.Point(0, 0);
            this.menuStripNewFAN.Name = "menuStripNewFAN";
            this.menuStripNewFAN.Size = new System.Drawing.Size(509, 29);
            this.menuStripNewFAN.TabIndex = 28;
            this.menuStripNewFAN.Text = "menuStrip1";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBoxNewMUAVoltageInfo);
            this.groupBox1.Controls.Add(this.groupBoxNewMUAUnitInfo);
            this.groupBox1.Controls.Add(this.groupBoxNewMUAJobInfo);
            this.groupBox1.Controls.Add(this.checkBoxNewMUAFrench);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(3, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(502, 561);
            this.groupBox1.TabIndex = 36;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "MUA";
            // 
            // frmNewLineMUA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(509, 586);
            this.Controls.Add(this.labelNewMUAMsg2);
            this.Controls.Add(this.labelNewMUAMsg);
            this.Controls.Add(this.buttonNewMUAAccept);
            this.Controls.Add(this.buttonNewMUACancel);
            this.Controls.Add(this.buttonNewMUAPrint);
            this.Controls.Add(this.menuStripNewFAN);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Blue;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmNewLineMUA";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New Line MUA";
            this.groupBoxNewMUAVoltageInfo.ResumeLayout(false);
            this.groupBoxNewMUAVoltageInfo.PerformLayout();
            this.groupBoxNewMUAJobInfo.ResumeLayout(false);
            this.groupBoxNewMUAJobInfo.PerformLayout();
            this.groupBoxNewMUAUnitInfo.ResumeLayout(false);
            this.groupBoxNewMUAUnitInfo.PerformLayout();
            this.menuStripNewFAN.ResumeLayout(false);
            this.menuStripNewFAN.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox textBoxNewMUAPartNum;
        public System.Windows.Forms.TextBox textBoxNewMUAVerifiedBy;
        private System.Windows.Forms.Label labelNewMUATag;
        private System.Windows.Forms.Label labelNewMUASerialNo;
        public System.Windows.Forms.TextBox textBoxNewMUASerialNo;
        public System.Windows.Forms.TextBox textBoxNewMUAModelNo;
        public System.Windows.Forms.Label labelNewMUAMsg2;
        public System.Windows.Forms.Label labelNewMUAMsg;
        public System.Windows.Forms.Button buttonNewMUAAccept;
        public System.Windows.Forms.Button buttonNewMUACancel;
        private System.Windows.Forms.Label labelNewMUAModelNum;
        public System.Windows.Forms.TextBox textBoxNewMUAAirMaxExtPressure;
        public System.Windows.Forms.Button buttonNewMUAPrint;
        public System.Windows.Forms.GroupBox groupBoxNewMUAVoltageInfo;
        private System.Windows.Forms.Label labelNewMUAAirMaxExtPressure;
        public System.Windows.Forms.TextBox textBoxNewMUAAirMaxTotalPressure;
        private System.Windows.Forms.Label labelNewMUAAirMaxTotalPressure;
        public System.Windows.Forms.TextBox textBoxNewMUAAirSCFM;
        private System.Windows.Forms.Label labelNewMUAAirSCFM;
        public System.Windows.Forms.TextBox textBoxNewMUAElecFLA;
        private System.Windows.Forms.Label labelNewMUAElecFLA;
        public System.Windows.Forms.TextBox textBoxNewMUAElecMaxVoltage;
        private System.Windows.Forms.Label labelNewMUAElecMaxVoltage;
        public System.Windows.Forms.TextBox textBoxNewMUAElecMaxVoltageHertz;
        private System.Windows.Forms.Label labelNewMUAElecMaxVoltageHertz;
        public System.Windows.Forms.TextBox textBoxNewMUAElecMinVoltage;
        private System.Windows.Forms.Label labelNewMUAElecMinVoltage;
        public System.Windows.Forms.TextBox textBoxNewMUAElecHertz;
        private System.Windows.Forms.Label labelNewMUAElecHertz;
        public System.Windows.Forms.TextBox textBoxNewMUAElecPhase;
        private System.Windows.Forms.Label labelNewMUAElecPhase;
        public System.Windows.Forms.TextBox textBoxNewMUAElecVolts;
        private System.Windows.Forms.Label labelNewMUAElecVolts;
        public System.Windows.Forms.TextBox textBoxNewMUAElecBlowerFan;
        private System.Windows.Forms.Label labelNewMUAElecBlowerFan;
        public System.Windows.Forms.TextBox textBoxNewMUAReleaseNum;
        private System.Windows.Forms.Label labelNewMUASlash;
        private System.Windows.Forms.GroupBox groupBoxNewMUAJobInfo;
        public System.Windows.Forms.TextBox textBoxNewMUACustomer;
        public System.Windows.Forms.TextBox textBoxNewMUAOrderNum;
        private System.Windows.Forms.Label labelNewMUACustomer;
        private System.Windows.Forms.Label labelNewMUAOrderNum;
        public System.Windows.Forms.TextBox textBoxNewMUALineItem;
        private System.Windows.Forms.Label labelNewMUALineItem;
        public System.Windows.Forms.TextBox textBoxNewMUATag;
        public System.Windows.Forms.GroupBox groupBoxNewMUAUnitInfo;
        private System.Windows.Forms.Label labelNewMUAPartNum;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStripNewFAN;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.TextBox textBoxNewMUAGasOutput;
        private System.Windows.Forms.Label labelNewMUAGasOutput;
        public System.Windows.Forms.TextBox textBoxNewMUAGasInput;
        private System.Windows.Forms.Label labelNewMUAGasInput;
        public System.Windows.Forms.TextBox textBoxNewMUAAirTempRise;
        private System.Windows.Forms.Label labelNewMUAAirTempRise;
        public System.Windows.Forms.Label labelNewMUAVerifiedBy;
        public System.Windows.Forms.CheckBox checkBoxNewMUAFrench;
        public System.Windows.Forms.TextBox textBoxNewMUAEndConsumer;
        private System.Windows.Forms.Label labelNewMUAEndConsumer;
        public System.Windows.Forms.TextBox textBoxNewMUAJobNum;
        private System.Windows.Forms.Label label1;
    }
}