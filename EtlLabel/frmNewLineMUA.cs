using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;

namespace KCC.OA.Etl
{
    public partial class frmNewLineMUA : Form
    {
        public frmNewLineMUA()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonNewMUACancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonNewMUAAccept_Click(object sender, EventArgs e)
        {
            if (GlobalModeType.ModeType == "New")
            {
                addNewLineOrderDtlMUA();
            }
            else
            {
                updateOrderDtlMUA();
            }
        }

        private void updateOrderDtlMUA()
        {
            string jobNumStr;
            int orderNumInt;
            int orderLineInt;
            int releaseNumInt;

            string frenchStr = "False";

            VS_DataSetTableAdapters.EtlMUATableAdapter etlMUATA =
                new VS_DataSetTableAdapters.EtlMUATableAdapter();

            etlMUATA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

            jobNumStr = this.textBoxNewMUAJobNum.Text;
            orderNumInt = Int32.Parse(this.textBoxNewMUAOrderNum.Text);
            orderLineInt = Int32.Parse(this.textBoxNewMUALineItem.Text);
            releaseNumInt = Int32.Parse(this.textBoxNewMUAReleaseNum.Text);

            if (this.checkBoxNewMUAFrench.Checked == true)
            {
                frenchStr = "True";
            }

            try
            {
                etlMUATA.UpdateByJobNum(
                    this.textBoxNewMUAJobNum.Text,
                    orderNumInt,
                    orderLineInt,
                    releaseNumInt,
                    this.textBoxNewMUAModelNo.Text,
                    this.textBoxNewMUAPartNum.Text,
                    this.textBoxNewMUASerialNo.Text,
                    this.textBoxNewMUAElecBlowerFan.Text,
                    this.textBoxNewMUAElecVolts.Text,
                    this.textBoxNewMUAElecPhase.Text,
                    this.textBoxNewMUAElecHertz.Text,
                    this.textBoxNewMUAElecFLA.Text,
                    this.textBoxNewMUAElecMinVoltage.Text,
                    this.textBoxNewMUAElecMaxVoltage.Text,
                    this.textBoxNewMUAElecMaxVoltageHertz.Text,
                    this.textBoxNewMUAAirSCFM.Text,
                    this.textBoxNewMUAAirMaxTotalPressure.Text,
                    this.textBoxNewMUAAirMaxExtPressure.Text,
                    this.textBoxNewMUAAirTempRise.Text,
                    this.textBoxNewMUAGasInput.Text,
                    this.textBoxNewMUAGasOutput.Text,
                    this.textBoxNewMUATag.Text,
                    frenchStr);

                MessageBox.Show("Job # " + jobNumStr + " Updated successfully.");

                this.buttonNewMUAAccept.Text = "Update";
                GlobalModeType.ModeType = "Update";
                this.buttonNewMUAPrint.Enabled = true;
                this.labelNewMUAMsg.Text = jobNumStr +
                    " has been successfully updated in the EtlMUA table. " +
                    "Use the Update button for any additional modifications";
            }
            catch
            {
                MessageBox.Show("ERROR - Updatinging data into EtlMUA");
            }

        }        

        private void addNewLineOrderDtlMUA()
        {
            string jobNumStr;
            int orderNumInt;
            int orderLineInt;
            int releaseNumInt;
            int slashLocInt;

            string createdByStr = "";
            string frenchStr = "False";
            string customerStr;
            string endConsumerNameStr = "";

            jobNumStr = this.textBoxNewMUAJobNum.Text;
            orderNumInt = Int32.Parse(this.textBoxNewMUAOrderNum.Text);
            orderLineInt = Int32.Parse(this.textBoxNewMUALineItem.Text);
            releaseNumInt = Int32.Parse(this.textBoxNewMUAReleaseNum.Text);         
            customerStr = this.textBoxNewMUACustomer.Text;
            endConsumerNameStr = this.textBoxNewMUAEndConsumer.Text;

            if (this.textBoxNewMUASerialNo.Text == "")
            {
                MessageBox.Show("ERROR - Serial No is a required field.");
                this.textBoxNewMUASerialNo.Focus();
                this.textBoxNewMUASerialNo.Select();
                return;
            }

            VS_DataSet dsEtl = new VS_DataSet();

            // Write record to the VSOrderHed table.                
            try
            {
                // Create SqlDataAdapters for reading the VSOrderHed 
                // table using strongly typed datasets.
               
                VS_DataSetTableAdapters.EtlJobHeadTableAdapter etlJobHeadTA =
                    new VS_DataSetTableAdapters.EtlJobHeadTableAdapter();

                etlJobHeadTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                etlJobHeadTA.Fill(dsEtl.EtlJobHead);

                DataRow drJobHead = dsEtl.EtlJobHead.NewRow();

                drJobHead["JobNum"] = jobNumStr;
                drJobHead["OrderNum"] = orderNumInt.ToString();
                drJobHead["OrderLine"] = orderLineInt.ToString();
                drJobHead["ReleaseNum"] = releaseNumInt.ToString();
                drJobHead["UnitType"] = "MUA";
                drJobHead["Customer"] = customerStr;
                drJobHead["EndConsumerName"] = endConsumerNameStr;

                dsEtl.EtlJobHead.Rows.Add(drJobHead);

                etlJobHeadTA.Update(dsEtl.EtlJobHead);

            }
            catch
            {
                MessageBox.Show("Error occurred while inserting a row to the EtlJobHead table.");
                return;
            }

            VS_DataSetTableAdapters.EtlMUATableAdapter etlMUATA =
                new VS_DataSetTableAdapters.EtlMUATableAdapter();

            etlMUATA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

            createdByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
            if (createdByStr != "")
            {
                slashLocInt = createdByStr.IndexOf('\\');
                if (slashLocInt != -1)
                {
                    ++slashLocInt;
                    createdByStr = createdByStr.Substring(slashLocInt, (createdByStr.Length - slashLocInt));
                }
            }

            if (this.checkBoxNewMUAFrench.Checked == true)
            {
                frenchStr = "True";
            }

            try
            {
                etlMUATA.Fill(dsEtl.EtlMUA);

                DataRow drEtlMUA = dsEtl.EtlMUA.NewRow();

                drEtlMUA["JobNum"] = jobNumStr;
                drEtlMUA["OrderNum"] = orderNumInt;
                drEtlMUA["OrderLine"] = orderLineInt;
                drEtlMUA["ReleaseNum"] = releaseNumInt;
                drEtlMUA["ModelNum"] = this.textBoxNewMUAModelNo.Text;
                drEtlMUA["PartNum"] = this.textBoxNewMUAPartNum.Text;
                drEtlMUA["SerialNum"] = this.textBoxNewMUASerialNo.Text;                    
                drEtlMUA["ElecBlowerFan"] = this.textBoxNewMUAElecBlowerFan.Text;
                drEtlMUA["ElecVolts"] = this.textBoxNewMUAElecVolts.Text;
                drEtlMUA["ElecPhase"] = this.textBoxNewMUAElecPhase.Text;
                drEtlMUA["ElecHertz"] = this.textBoxNewMUAElecHertz.Text;
                drEtlMUA["ElecFLA"] = this.textBoxNewMUAElecFLA.Text;
                drEtlMUA["ElecMinVoltage"] = this.textBoxNewMUAElecMinVoltage.Text;
                drEtlMUA["ElecMaxVoltage"] = this.textBoxNewMUAElecMaxVoltage.Text;
                drEtlMUA["ElecMaxVoltageHertz"] = this.textBoxNewMUAElecMaxVoltageHertz.Text;
                drEtlMUA["AirSCFM"] = this.textBoxNewMUAAirSCFM.Text;
                drEtlMUA["AirMaxTotalPressure"] = this.textBoxNewMUAAirMaxTotalPressure.Text;
                drEtlMUA["AirMaxExtPressure"] = this.textBoxNewMUAAirMaxExtPressure.Text;
                drEtlMUA["AirTempRise"] = this.textBoxNewMUAAirTempRise.Text;
                drEtlMUA["GasInput"] = this.textBoxNewMUAGasInput.Text;
                drEtlMUA["GasOutput"] = this.textBoxNewMUAGasOutput.Text;
                drEtlMUA["Tag"] = this.textBoxNewMUATag.Text;
                drEtlMUA["VerifiedBy"] = createdByStr;
                drEtlMUA["French"] = frenchStr;

                dsEtl.EtlMUA.Rows.Add(drEtlMUA);

                etlMUATA.Update(dsEtl.EtlMUA);

                MessageBox.Show("Job # " + jobNumStr + " added successfully.");

                this.buttonNewMUAAccept.Text = "Update";
                GlobalModeType.ModeType = "Update";
                this.buttonNewMUAPrint.Enabled = true;
                this.labelNewMUAMsg.Text = this.textBoxNewMUASerialNo.Text +
                    " has been successfully written to the EtlMUA table.";
                this.labelNewMUAMsg2.Text = "Use the Update button to modify this unit's data if necessary.";
            }
            catch
            {
                MessageBox.Show("ERROR inserting data into EtlMUA");
            }

        }

        private void buttonNewMUAPrint_Click(object sender, EventArgs e)
        {
            string frenchStr = "False";

            VS_DataSet ds = new VS_DataSet();

            frmPrintMUA frmPrint = new frmPrintMUA();

            if (this.textBoxNewMUASerialNo.Text == "")
            {
                MessageBox.Show("ERROR - Serial No is a required field.");
                this.textBoxNewMUASerialNo.Focus();
                this.textBoxNewMUASerialNo.Select();
                return;
            }

            if (this.checkBoxNewMUAFrench.Checked == true)
            {
                frenchStr = "True";
            }

            ds.MUAReportDataTable.Rows.Add(
                    this.textBoxNewMUAOrderNum.Text,
                    this.textBoxNewMUALineItem.Text,
                    this.textBoxNewMUAReleaseNum.Text,
                    this.textBoxNewMUAModelNo.Text,
                    this.textBoxNewMUAPartNum.Text,
                    this.textBoxNewMUASerialNo.Text,           
                    this.textBoxNewMUAElecBlowerFan.Text,
                    this.textBoxNewMUAElecVolts.Text,
                    this.textBoxNewMUAElecPhase.Text,
                    this.textBoxNewMUAElecHertz.Text,
                    this.textBoxNewMUAElecFLA.Text,
                    this.textBoxNewMUAElecMinVoltage.Text,
                    this.textBoxNewMUAElecMaxVoltage.Text,
                    this.textBoxNewMUAElecMaxVoltageHertz.Text,
                    this.textBoxNewMUAAirSCFM.Text,
                    this.textBoxNewMUAAirMaxTotalPressure.Text,
                    this.textBoxNewMUAAirMaxExtPressure.Text,
                    this.textBoxNewMUAAirTempRise.Text,
                    this.textBoxNewMUAGasInput.Text,
                    this.textBoxNewMUAGasOutput.Text,
                    this.textBoxNewMUATag.Text,
                    frenchStr);

            ReportDocument cryRpt = new ReportDocument();

            //string reportString = @"\\epicor905app\Apps\OAU\EtlLabel\reports\MUA_Label_v002.rpt";
            string reportString = @"\\KCCWVPEPIC9APP\Apps\OAU\EtlLabel\reports\MUA_Label_v002.rpt";

#if DEBUG
                reportString = @"\\KCCWVTEPIC9APP2\Apps\OAU\EtlLabel\reports\MUA_Label_v002.rpt";
#endif

            cryRpt.Load(reportString);

            cryRpt.SetDataSource(ds.Tables["MUAReportDataTable"]);

            frmPrint.crystalReportViewer2.ReportSource = cryRpt;

            frmPrint.ShowDialog();
            frmPrint.crystalReportViewer2.Refresh();  

        }

    }
}