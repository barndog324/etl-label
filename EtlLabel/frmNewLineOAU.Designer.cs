namespace KCC.OA.Etl
{
    partial class frmNewLineOAU
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxNewOAU = new System.Windows.Forms.GroupBox();
            this.labelNewOAUMsg = new System.Windows.Forms.Label();
            this.groupBoxNewOAUDirectFired = new System.Windows.Forms.GroupBox();
            this.txtNewOAU_DF_MinPressureDrop = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtNewOAU_DF_MaxPressureDrop = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtNewOAU_DF_CutoutTemp = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtNewOAU_DF_AirFlowRate = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtNewOAU_DF_StaticPressure = new System.Windows.Forms.TextBox();
            this.txtNewOAU_DF_MinGasPressure = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtNewOAU_DF_ManifoldPressure = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtNewOAU_DF_MaxGasPressure = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtNewOAU_DF_TempRise = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtNewOAU_DF_MinHtgInputBTU = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtNewOAU_DF_MaxHtgInputBTU = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtCircuit2Charge = new System.Windows.Forms.TextBox();
            this.labelNewOAUComp2Charge = new System.Windows.Forms.Label();
            this.txtCircuit1Charge = new System.Windows.Forms.TextBox();
            this.labelNewOAUComp1Charge = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtComp6LRA = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtComp6RLA_Volts = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtComp6Phase = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtComp6Qty = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.gbComp2_2 = new System.Windows.Forms.GroupBox();
            this.txtComp5LRA = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtComp5RLA_Volts = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtComp5Phase = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtComp5Qty = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBoxNewOAULanguage = new System.Windows.Forms.GroupBox();
            this.checkBoxLangFrench = new System.Windows.Forms.CheckBox();
            this.groupBoxNewOAUWater_Glycol = new System.Windows.Forms.GroupBox();
            this.radioButtonGlycol = new System.Windows.Forms.RadioButton();
            this.radioButtonWater = new System.Windows.Forms.RadioButton();
            this.groupBoxNewOAUSteam = new System.Windows.Forms.GroupBox();
            this.textBoxNewOAUSteamCondensateFlowRate = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxNewOAUSteamOperatingPressure = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBoxNewOAUHotWater = new System.Windows.Forms.GroupBox();
            this.textBoxHotWaterEnteringWaterTemp = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxHotWaterFlowRate = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxParseModelNo = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonModelNoCancel = new System.Windows.Forms.Button();
            this.buttonModelNoEdit = new System.Windows.Forms.Button();
            this.buttonModelNoParse = new System.Windows.Forms.Button();
            this.textBoxModifyModelNo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonNewOAUPrint = new System.Windows.Forms.Button();
            this.buttonNewOAUAccept = new System.Windows.Forms.Button();
            this.buttonNewOAUCancel = new System.Windows.Forms.Button();
            this.groupBoxNewOAUFanInfo = new System.Windows.Forms.GroupBox();
            this.textBoxNewOAUFanPwrHP = new System.Windows.Forms.TextBox();
            this.labelNewOAUFanPwrHP = new System.Windows.Forms.Label();
            this.textBoxNewOAUFanPwrFLA = new System.Windows.Forms.TextBox();
            this.labelNewOAUFanPwrFLA = new System.Windows.Forms.Label();
            this.textBoxNewOAUFanPwrPH = new System.Windows.Forms.TextBox();
            this.labelNewOAUFanPwrPH = new System.Windows.Forms.Label();
            this.textBoxNewOAUFanPwrQty = new System.Windows.Forms.TextBox();
            this.labelNewOAUFanPwrQty = new System.Windows.Forms.Label();
            this.textBoxNewOAUFanEvapHP = new System.Windows.Forms.TextBox();
            this.labelNewOAUFanEvapHP = new System.Windows.Forms.Label();
            this.textBoxNewOAUFanEvapFLA = new System.Windows.Forms.TextBox();
            this.labelNewOAUFanEvapFLA = new System.Windows.Forms.Label();
            this.textBoxNewOAUFanEvapPH = new System.Windows.Forms.TextBox();
            this.labelNewOAUFanEvapPH = new System.Windows.Forms.Label();
            this.textBoxNewOAUFanEvapQty = new System.Windows.Forms.TextBox();
            this.labelNewOAUFanEvapQty = new System.Windows.Forms.Label();
            this.textBoxNewOAUFanEnergyHP = new System.Windows.Forms.TextBox();
            this.labelNewOAUFanEnergyHP = new System.Windows.Forms.Label();
            this.textBoxNewOAUFanEnergyFLA = new System.Windows.Forms.TextBox();
            this.labelNewOAUFanEnergyFLA = new System.Windows.Forms.Label();
            this.textBoxNewOAUFanEnergyPH = new System.Windows.Forms.TextBox();
            this.labelNewOAUFanEnergyPH = new System.Windows.Forms.Label();
            this.textBoxNewOAUFanEnergyQty = new System.Windows.Forms.TextBox();
            this.labelNewOAUFanEnergyQty = new System.Windows.Forms.Label();
            this.textBoxNewOAUFanCondHP = new System.Windows.Forms.TextBox();
            this.labelNewOAUFanCondHP = new System.Windows.Forms.Label();
            this.textBoxNewOAUFanCondFLA = new System.Windows.Forms.TextBox();
            this.labelNewOAUFanCondFLA = new System.Windows.Forms.Label();
            this.textBoxNewOAUFanCondPH = new System.Windows.Forms.TextBox();
            this.labelNewOAUFanCondPH = new System.Windows.Forms.Label();
            this.textBoxNewOAUFanCondQty = new System.Windows.Forms.TextBox();
            this.labelNewOAUFanCondQty = new System.Windows.Forms.Label();
            this.groupBoxNewOAUCompressor4 = new System.Windows.Forms.GroupBox();
            this.txtComp4LRA = new System.Windows.Forms.TextBox();
            this.labelNewOAUComp4LRA = new System.Windows.Forms.Label();
            this.txtComp4RLA_Volts = new System.Windows.Forms.TextBox();
            this.labelNewOAUComp4RLAVolts = new System.Windows.Forms.Label();
            this.txtComp4Phase = new System.Windows.Forms.TextBox();
            this.labelNewOAUComp4PH = new System.Windows.Forms.Label();
            this.txtComp4Qty = new System.Windows.Forms.TextBox();
            this.labelNewOAUComp4Qty = new System.Windows.Forms.Label();
            this.groupBoxNewOAUCompressor3 = new System.Windows.Forms.GroupBox();
            this.txtComp3LRA = new System.Windows.Forms.TextBox();
            this.labelNewOAUComp3LRA = new System.Windows.Forms.Label();
            this.txtComp3RLA_Volts = new System.Windows.Forms.TextBox();
            this.labelNewOAUComp3RLAVolts = new System.Windows.Forms.Label();
            this.txtComp3Phase = new System.Windows.Forms.TextBox();
            this.labelNewOAUComp3PH = new System.Windows.Forms.Label();
            this.txtComp3Qty = new System.Windows.Forms.TextBox();
            this.labelNewOAUComp3Qty = new System.Windows.Forms.Label();
            this.groupBoxNewOAUCompressor2 = new System.Windows.Forms.GroupBox();
            this.txtComp2LRA = new System.Windows.Forms.TextBox();
            this.labelNewOAUNewOAUComp2LRA = new System.Windows.Forms.Label();
            this.txtComp2RLA_Volts = new System.Windows.Forms.TextBox();
            this.labelNewOAUComp2RLAVolts = new System.Windows.Forms.Label();
            this.txtComp2Phase = new System.Windows.Forms.TextBox();
            this.labelNewOAUComp2PH = new System.Windows.Forms.Label();
            this.txtComp2Qty = new System.Windows.Forms.TextBox();
            this.labelNewOAUComp2Qty = new System.Windows.Forms.Label();
            this.groupBoxNewOAUCompressor1 = new System.Windows.Forms.GroupBox();
            this.txtComp1LRA = new System.Windows.Forms.TextBox();
            this.labelNewOAUComp1LRA = new System.Windows.Forms.Label();
            this.txtComp1RLA_Volts = new System.Windows.Forms.TextBox();
            this.labelNewOAUComp1RLAVolts = new System.Windows.Forms.Label();
            this.txtComp1Phase = new System.Windows.Forms.TextBox();
            this.labelNewOAUComp1PH = new System.Windows.Forms.Label();
            this.txtComp1Qty = new System.Windows.Forms.TextBox();
            this.labelNewOAUComp1Qty = new System.Windows.Forms.Label();
            this.groupBoxNewOauLineInfo = new System.Windows.Forms.GroupBox();
            this.cbWhseCode = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.dateTimePickerOAUMfgDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBoxNewOAUVerified = new System.Windows.Forms.CheckBox();
            this.textBoxNewOAUVerifiedBy = new System.Windows.Forms.TextBox();
            this.labelMewOAUVerifiedBy = new System.Windows.Forms.Label();
            this.comboBoxNewOAUVoltage = new System.Windows.Forms.ComboBox();
            this.labelNewOAUVoltage = new System.Windows.Forms.Label();
            this.comboBoxNewOAUHeatingType = new System.Windows.Forms.ComboBox();
            this.labelNewOAUHeatingType = new System.Windows.Forms.Label();
            this.labelNewOAUSerialNo = new System.Windows.Forms.Label();
            this.textBoxNewOAUSerialNo = new System.Windows.Forms.TextBox();
            this.textBoxNewOAUModelNo = new System.Windows.Forms.TextBox();
            this.labelNewOAUModelNum = new System.Windows.Forms.Label();
            this.groupBoxNewOAUJobInfo = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxNewOAUJobNum = new System.Windows.Forms.TextBox();
            this.textBoxNewOAUEndConsumer = new System.Windows.Forms.TextBox();
            this.labelNewOAUEndConsumer = new System.Windows.Forms.Label();
            this.textBoxNewOAUReleaseNum = new System.Windows.Forms.TextBox();
            this.labelNewOAUSlash = new System.Windows.Forms.Label();
            this.textBoxNewOAUCustomer = new System.Windows.Forms.TextBox();
            this.textBoxNewOAUOrderNum = new System.Windows.Forms.TextBox();
            this.labelNewOAUCustomer = new System.Windows.Forms.Label();
            this.labelNewOAUOrderNum = new System.Windows.Forms.Label();
            this.textBoxNewOAULineItem = new System.Windows.Forms.TextBox();
            this.labelNewOAULineItem = new System.Windows.Forms.Label();
            this.groupBoxNewOAUElectricInfo = new System.Windows.Forms.GroupBox();
            this.txtSCCR = new System.Windows.Forms.TextBox();
            this.lblSCCR = new System.Windows.Forms.Label();
            this.textBoxNewOAUMFSMCB2 = new System.Windows.Forms.TextBox();
            this.textBoxNewOAUMinCKTAmp2 = new System.Windows.Forms.TextBox();
            this.labelNewOAUDPP2 = new System.Windows.Forms.Label();
            this.labelNewOAUDPP1 = new System.Windows.Forms.Label();
            this.checkBoxNewOAUDPP = new System.Windows.Forms.CheckBox();
            this.textBoxNewOAUTestPressureLow = new System.Windows.Forms.TextBox();
            this.labelNewOAUTestPressureLow = new System.Windows.Forms.Label();
            this.textBoxNewOAUTestPressureHigh = new System.Windows.Forms.TextBox();
            this.labelNewOAUTestPressureHigh = new System.Windows.Forms.Label();
            this.textBoxNewOAUOperatingVolts = new System.Windows.Forms.TextBox();
            this.labelNewOAUOperatingVolts = new System.Windows.Forms.Label();
            this.textBoxNewOAUElectricalRating = new System.Windows.Forms.TextBox();
            this.labelNewOAUElectricalRating = new System.Windows.Forms.Label();
            this.textBoxNewOAUMOP = new System.Windows.Forms.TextBox();
            this.labelNewOAUMOP = new System.Windows.Forms.Label();
            this.textBoxNewOAUMFSMCB = new System.Windows.Forms.TextBox();
            this.labelNewOAUMFSMCB = new System.Windows.Forms.Label();
            this.textBoxNewOAUMinCKTAmp = new System.Windows.Forms.TextBox();
            this.labelNewOAUMinCKTAmp = new System.Windows.Forms.Label();
            this.groupBoxNewOAUElectric = new System.Windows.Forms.GroupBox();
            this.txtNewOAUElecSecHtgInput = new System.Windows.Forms.TextBox();
            this.lblNewOAUElecSecHtgInput = new System.Windows.Forms.Label();
            this.textBoxNewOAUElecHeatingInputElec = new System.Windows.Forms.TextBox();
            this.labelNewOAUElecHeatingInput = new System.Windows.Forms.Label();
            this.groupBoxNewOAUIndirectFire = new System.Windows.Forms.GroupBox();
            this.labelNewOAUIndFireManifoldPres = new System.Windows.Forms.Label();
            this.textBoxNewOAUIndFireSecHtgInput = new System.Windows.Forms.TextBox();
            this.lblNewOAUIndFireHtgInput = new System.Windows.Forms.Label();
            this.comboBoxNewOAUIFFuelType = new System.Windows.Forms.ComboBox();
            this.labelNewOAUIFFuelType = new System.Windows.Forms.Label();
            this.textBoxNewOAUIndFireTempRise = new System.Windows.Forms.TextBox();
            this.labelNewOAUIndFireTempRise = new System.Windows.Forms.Label();
            this.textBoxNewOAUIndFireManifoldPressure = new System.Windows.Forms.TextBox();
            this.textBoxNewOAUIndFireMaxGasPressure = new System.Windows.Forms.TextBox();
            this.labelNewOAUIndFireMaxGasPressure = new System.Windows.Forms.Label();
            this.textBoxNewOAUIndFireMinGasPressure = new System.Windows.Forms.TextBox();
            this.labelNewOAUIndFireMinGasPressure = new System.Windows.Forms.Label();
            this.textBoxNewOAUIndFireMaxOutAirTemp = new System.Windows.Forms.TextBox();
            this.labelNewOAUIndFireMaxOutAirTemp = new System.Windows.Forms.Label();
            this.textBoxNewOauIndFireMaxExt = new System.Windows.Forms.TextBox();
            this.textBoxNewOauIndFireMinInputBTU = new System.Windows.Forms.TextBox();
            this.labelNewOauIndFireMinHtgInput = new System.Windows.Forms.Label();
            this.textBoxNewOAUIndFireHeatingOutput = new System.Windows.Forms.TextBox();
            this.labelNewOauIndFireHtgOutput = new System.Windows.Forms.Label();
            this.textBoxNewOAUIndFireMaxHtgInput = new System.Windows.Forms.TextBox();
            this.labelNewOauIndFireMaxHtg = new System.Windows.Forms.Label();
            this.labelNewOauIndFireMaxEst = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblNewOAUElecMaxAirOutletTemp = new System.Windows.Forms.Label();
            this.textBoxNewOAUElecMaxAirOutletTemp = new System.Windows.Forms.TextBox();
            this.groupBoxNewOAU.SuspendLayout();
            this.groupBoxNewOAUDirectFired.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbComp2_2.SuspendLayout();
            this.groupBoxNewOAULanguage.SuspendLayout();
            this.groupBoxNewOAUWater_Glycol.SuspendLayout();
            this.groupBoxNewOAUSteam.SuspendLayout();
            this.groupBoxNewOAUHotWater.SuspendLayout();
            this.groupBoxParseModelNo.SuspendLayout();
            this.groupBoxNewOAUFanInfo.SuspendLayout();
            this.groupBoxNewOAUCompressor4.SuspendLayout();
            this.groupBoxNewOAUCompressor3.SuspendLayout();
            this.groupBoxNewOAUCompressor2.SuspendLayout();
            this.groupBoxNewOAUCompressor1.SuspendLayout();
            this.groupBoxNewOauLineInfo.SuspendLayout();
            this.groupBoxNewOAUJobInfo.SuspendLayout();
            this.groupBoxNewOAUElectricInfo.SuspendLayout();
            this.groupBoxNewOAUElectric.SuspendLayout();
            this.groupBoxNewOAUIndirectFire.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxNewOAU
            // 
            this.groupBoxNewOAU.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBoxNewOAU.Controls.Add(this.groupBoxNewOAUElectric);
            this.groupBoxNewOAU.Controls.Add(this.labelNewOAUMsg);
            this.groupBoxNewOAU.Controls.Add(this.groupBox2);
            this.groupBoxNewOAU.Controls.Add(this.groupBox1);
            this.groupBoxNewOAU.Controls.Add(this.gbComp2_2);
            this.groupBoxNewOAU.Controls.Add(this.groupBoxNewOAULanguage);
            this.groupBoxNewOAU.Controls.Add(this.groupBoxNewOAUWater_Glycol);
            this.groupBoxNewOAU.Controls.Add(this.groupBoxNewOAUHotWater);
            this.groupBoxNewOAU.Controls.Add(this.groupBoxParseModelNo);
            this.groupBoxNewOAU.Controls.Add(this.buttonNewOAUPrint);
            this.groupBoxNewOAU.Controls.Add(this.buttonNewOAUAccept);
            this.groupBoxNewOAU.Controls.Add(this.buttonNewOAUCancel);
            this.groupBoxNewOAU.Controls.Add(this.groupBoxNewOAUFanInfo);
            this.groupBoxNewOAU.Controls.Add(this.groupBoxNewOAUCompressor4);
            this.groupBoxNewOAU.Controls.Add(this.groupBoxNewOAUCompressor3);
            this.groupBoxNewOAU.Controls.Add(this.groupBoxNewOAUCompressor2);
            this.groupBoxNewOAU.Controls.Add(this.groupBoxNewOAUCompressor1);
            this.groupBoxNewOAU.Controls.Add(this.groupBoxNewOauLineInfo);
            this.groupBoxNewOAU.Controls.Add(this.groupBoxNewOAUJobInfo);
            this.groupBoxNewOAU.Controls.Add(this.groupBoxNewOAUElectricInfo);
            this.groupBoxNewOAU.Controls.Add(this.groupBoxNewOAUIndirectFire);
            this.groupBoxNewOAU.Controls.Add(this.groupBoxNewOAUDirectFired);
            this.groupBoxNewOAU.Controls.Add(this.groupBoxNewOAUSteam);
            this.groupBoxNewOAU.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewOAU.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.groupBoxNewOAU.Location = new System.Drawing.Point(12, 29);
            this.groupBoxNewOAU.Name = "groupBoxNewOAU";
            this.groupBoxNewOAU.Size = new System.Drawing.Size(1079, 691);
            this.groupBoxNewOAU.TabIndex = 0;
            this.groupBoxNewOAU.TabStop = false;
            this.groupBoxNewOAU.Text = "OAU";
            // 
            // labelNewOAUMsg
            // 
            this.labelNewOAUMsg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.labelNewOAUMsg.Location = new System.Drawing.Point(6, 693);
            this.labelNewOAUMsg.Name = "labelNewOAUMsg";
            this.labelNewOAUMsg.Size = new System.Drawing.Size(1059, 21);
            this.labelNewOAUMsg.TabIndex = 13;
            this.labelNewOAUMsg.Text = "Press the Accept Button to Verify OAU Sticker data.";
            this.labelNewOAUMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBoxNewOAUDirectFired
            // 
            this.groupBoxNewOAUDirectFired.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewOAUDirectFired.Controls.Add(this.txtNewOAU_DF_MinPressureDrop);
            this.groupBoxNewOAUDirectFired.Controls.Add(this.label18);
            this.groupBoxNewOAUDirectFired.Controls.Add(this.txtNewOAU_DF_MaxPressureDrop);
            this.groupBoxNewOAUDirectFired.Controls.Add(this.label19);
            this.groupBoxNewOAUDirectFired.Controls.Add(this.label17);
            this.groupBoxNewOAUDirectFired.Controls.Add(this.txtNewOAU_DF_CutoutTemp);
            this.groupBoxNewOAUDirectFired.Controls.Add(this.label16);
            this.groupBoxNewOAUDirectFired.Controls.Add(this.txtNewOAU_DF_AirFlowRate);
            this.groupBoxNewOAUDirectFired.Controls.Add(this.label10);
            this.groupBoxNewOAUDirectFired.Controls.Add(this.txtNewOAU_DF_StaticPressure);
            this.groupBoxNewOAUDirectFired.Controls.Add(this.txtNewOAU_DF_MinGasPressure);
            this.groupBoxNewOAUDirectFired.Controls.Add(this.label7);
            this.groupBoxNewOAUDirectFired.Controls.Add(this.txtNewOAU_DF_ManifoldPressure);
            this.groupBoxNewOAUDirectFired.Controls.Add(this.label11);
            this.groupBoxNewOAUDirectFired.Controls.Add(this.txtNewOAU_DF_MaxGasPressure);
            this.groupBoxNewOAUDirectFired.Controls.Add(this.label12);
            this.groupBoxNewOAUDirectFired.Controls.Add(this.txtNewOAU_DF_TempRise);
            this.groupBoxNewOAUDirectFired.Controls.Add(this.txtNewOAU_DF_MinHtgInputBTU);
            this.groupBoxNewOAUDirectFired.Controls.Add(this.label14);
            this.groupBoxNewOAUDirectFired.Controls.Add(this.txtNewOAU_DF_MaxHtgInputBTU);
            this.groupBoxNewOAUDirectFired.Controls.Add(this.label15);
            this.groupBoxNewOAUDirectFired.Controls.Add(this.label13);
            this.groupBoxNewOAUDirectFired.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewOAUDirectFired.Location = new System.Drawing.Point(4, 365);
            this.groupBoxNewOAUDirectFired.Name = "groupBoxNewOAUDirectFired";
            this.groupBoxNewOAUDirectFired.Size = new System.Drawing.Size(311, 320);
            this.groupBoxNewOAUDirectFired.TabIndex = 57;
            this.groupBoxNewOAUDirectFired.TabStop = false;
            this.groupBoxNewOAUDirectFired.Text = "Direct Fire";
            this.groupBoxNewOAUDirectFired.Visible = false;
            // 
            // txtNewOAU_DF_MinPressureDrop
            // 
            this.txtNewOAU_DF_MinPressureDrop.BackColor = System.Drawing.Color.White;
            this.txtNewOAU_DF_MinPressureDrop.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewOAU_DF_MinPressureDrop.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtNewOAU_DF_MinPressureDrop.Location = new System.Drawing.Point(186, 288);
            this.txtNewOAU_DF_MinPressureDrop.Name = "txtNewOAU_DF_MinPressureDrop";
            this.txtNewOAU_DF_MinPressureDrop.Size = new System.Drawing.Size(118, 22);
            this.txtNewOAU_DF_MinPressureDrop.TabIndex = 62;
            this.txtNewOAU_DF_MinPressureDrop.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(8, 288);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(175, 21);
            this.label18.TabIndex = 64;
            this.label18.Text = "Min Pressure Drop:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtNewOAU_DF_MaxPressureDrop
            // 
            this.txtNewOAU_DF_MaxPressureDrop.BackColor = System.Drawing.Color.White;
            this.txtNewOAU_DF_MaxPressureDrop.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewOAU_DF_MaxPressureDrop.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtNewOAU_DF_MaxPressureDrop.Location = new System.Drawing.Point(186, 261);
            this.txtNewOAU_DF_MaxPressureDrop.Name = "txtNewOAU_DF_MaxPressureDrop";
            this.txtNewOAU_DF_MaxPressureDrop.Size = new System.Drawing.Size(118, 22);
            this.txtNewOAU_DF_MaxPressureDrop.TabIndex = 61;
            this.txtNewOAU_DF_MaxPressureDrop.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(5, 261);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(175, 21);
            this.label19.TabIndex = 63;
            this.label19.Text = "Max Pressure Drop:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(8, 235);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(175, 21);
            this.label17.TabIndex = 60;
            this.label17.Text = "Cutout Temp:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtNewOAU_DF_CutoutTemp
            // 
            this.txtNewOAU_DF_CutoutTemp.BackColor = System.Drawing.Color.White;
            this.txtNewOAU_DF_CutoutTemp.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewOAU_DF_CutoutTemp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtNewOAU_DF_CutoutTemp.Location = new System.Drawing.Point(186, 235);
            this.txtNewOAU_DF_CutoutTemp.Name = "txtNewOAU_DF_CutoutTemp";
            this.txtNewOAU_DF_CutoutTemp.Size = new System.Drawing.Size(118, 22);
            this.txtNewOAU_DF_CutoutTemp.TabIndex = 59;
            this.txtNewOAU_DF_CutoutTemp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(9, 208);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(175, 21);
            this.label16.TabIndex = 58;
            this.label16.Text = "Static Pressure:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtNewOAU_DF_AirFlowRate
            // 
            this.txtNewOAU_DF_AirFlowRate.BackColor = System.Drawing.Color.White;
            this.txtNewOAU_DF_AirFlowRate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewOAU_DF_AirFlowRate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtNewOAU_DF_AirFlowRate.Location = new System.Drawing.Point(187, 181);
            this.txtNewOAU_DF_AirFlowRate.Name = "txtNewOAU_DF_AirFlowRate";
            this.txtNewOAU_DF_AirFlowRate.Size = new System.Drawing.Size(118, 22);
            this.txtNewOAU_DF_AirFlowRate.TabIndex = 57;
            this.txtNewOAU_DF_AirFlowRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(9, 183);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(175, 21);
            this.label10.TabIndex = 56;
            this.label10.Text = "Airflow Rate:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtNewOAU_DF_StaticPressure
            // 
            this.txtNewOAU_DF_StaticPressure.BackColor = System.Drawing.Color.White;
            this.txtNewOAU_DF_StaticPressure.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewOAU_DF_StaticPressure.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtNewOAU_DF_StaticPressure.Location = new System.Drawing.Point(187, 208);
            this.txtNewOAU_DF_StaticPressure.Name = "txtNewOAU_DF_StaticPressure";
            this.txtNewOAU_DF_StaticPressure.Size = new System.Drawing.Size(118, 22);
            this.txtNewOAU_DF_StaticPressure.TabIndex = 55;
            this.txtNewOAU_DF_StaticPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtNewOAU_DF_MinGasPressure
            // 
            this.txtNewOAU_DF_MinGasPressure.BackColor = System.Drawing.Color.White;
            this.txtNewOAU_DF_MinGasPressure.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewOAU_DF_MinGasPressure.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtNewOAU_DF_MinGasPressure.Location = new System.Drawing.Point(187, 127);
            this.txtNewOAU_DF_MinGasPressure.Name = "txtNewOAU_DF_MinGasPressure";
            this.txtNewOAU_DF_MinGasPressure.Size = new System.Drawing.Size(118, 22);
            this.txtNewOAU_DF_MinGasPressure.TabIndex = 13;
            this.txtNewOAU_DF_MinGasPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(9, 127);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(175, 21);
            this.label7.TabIndex = 54;
            this.label7.Text = "Min Gas Pressure:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtNewOAU_DF_ManifoldPressure
            // 
            this.txtNewOAU_DF_ManifoldPressure.BackColor = System.Drawing.Color.White;
            this.txtNewOAU_DF_ManifoldPressure.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewOAU_DF_ManifoldPressure.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtNewOAU_DF_ManifoldPressure.Location = new System.Drawing.Point(187, 154);
            this.txtNewOAU_DF_ManifoldPressure.Name = "txtNewOAU_DF_ManifoldPressure";
            this.txtNewOAU_DF_ManifoldPressure.Size = new System.Drawing.Size(118, 22);
            this.txtNewOAU_DF_ManifoldPressure.TabIndex = 14;
            this.txtNewOAU_DF_ManifoldPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(9, 154);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(175, 21);
            this.label11.TabIndex = 46;
            this.label11.Text = "Manifold Pressure:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtNewOAU_DF_MaxGasPressure
            // 
            this.txtNewOAU_DF_MaxGasPressure.BackColor = System.Drawing.Color.White;
            this.txtNewOAU_DF_MaxGasPressure.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewOAU_DF_MaxGasPressure.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtNewOAU_DF_MaxGasPressure.Location = new System.Drawing.Point(187, 100);
            this.txtNewOAU_DF_MaxGasPressure.Name = "txtNewOAU_DF_MaxGasPressure";
            this.txtNewOAU_DF_MaxGasPressure.Size = new System.Drawing.Size(118, 22);
            this.txtNewOAU_DF_MaxGasPressure.TabIndex = 12;
            this.txtNewOAU_DF_MaxGasPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 100);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(175, 21);
            this.label12.TabIndex = 44;
            this.label12.Text = "Max Gas Pressure:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtNewOAU_DF_TempRise
            // 
            this.txtNewOAU_DF_TempRise.BackColor = System.Drawing.Color.White;
            this.txtNewOAU_DF_TempRise.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewOAU_DF_TempRise.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtNewOAU_DF_TempRise.Location = new System.Drawing.Point(187, 73);
            this.txtNewOAU_DF_TempRise.Name = "txtNewOAU_DF_TempRise";
            this.txtNewOAU_DF_TempRise.Size = new System.Drawing.Size(118, 22);
            this.txtNewOAU_DF_TempRise.TabIndex = 11;
            this.txtNewOAU_DF_TempRise.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(9, 73);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(175, 21);
            this.label13.TabIndex = 42;
            this.label13.Text = "Temp Rise:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtNewOAU_DF_MinHtgInputBTU
            // 
            this.txtNewOAU_DF_MinHtgInputBTU.BackColor = System.Drawing.Color.White;
            this.txtNewOAU_DF_MinHtgInputBTU.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewOAU_DF_MinHtgInputBTU.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtNewOAU_DF_MinHtgInputBTU.Location = new System.Drawing.Point(187, 46);
            this.txtNewOAU_DF_MinHtgInputBTU.Name = "txtNewOAU_DF_MinHtgInputBTU";
            this.txtNewOAU_DF_MinHtgInputBTU.Size = new System.Drawing.Size(118, 22);
            this.txtNewOAU_DF_MinHtgInputBTU.TabIndex = 10;
            this.txtNewOAU_DF_MinHtgInputBTU.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(9, 46);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(175, 21);
            this.label14.TabIndex = 40;
            this.label14.Text = "Min Heating Input BTU:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtNewOAU_DF_MaxHtgInputBTU
            // 
            this.txtNewOAU_DF_MaxHtgInputBTU.BackColor = System.Drawing.Color.White;
            this.txtNewOAU_DF_MaxHtgInputBTU.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewOAU_DF_MaxHtgInputBTU.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtNewOAU_DF_MaxHtgInputBTU.Location = new System.Drawing.Point(187, 19);
            this.txtNewOAU_DF_MaxHtgInputBTU.Name = "txtNewOAU_DF_MaxHtgInputBTU";
            this.txtNewOAU_DF_MaxHtgInputBTU.Size = new System.Drawing.Size(118, 22);
            this.txtNewOAU_DF_MaxHtgInputBTU.TabIndex = 9;
            this.txtNewOAU_DF_MaxHtgInputBTU.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(9, 19);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(175, 21);
            this.label15.TabIndex = 38;
            this.label15.Text = "Max Heating Input BTU:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBox2.Controls.Add(this.txtCircuit2Charge);
            this.groupBox2.Controls.Add(this.labelNewOAUComp2Charge);
            this.groupBox2.Controls.Add(this.txtCircuit1Charge);
            this.groupBox2.Controls.Add(this.labelNewOAUComp1Charge);
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(321, 439);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(392, 67);
            this.groupBox2.TabIndex = 69;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Compressor Charge Data";
            // 
            // txtCircuit2Charge
            // 
            this.txtCircuit2Charge.BackColor = System.Drawing.Color.White;
            this.txtCircuit2Charge.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCircuit2Charge.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtCircuit2Charge.Location = new System.Drawing.Point(324, 29);
            this.txtCircuit2Charge.Name = "txtCircuit2Charge";
            this.txtCircuit2Charge.Size = new System.Drawing.Size(60, 22);
            this.txtCircuit2Charge.TabIndex = 27;
            this.txtCircuit2Charge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUComp2Charge
            // 
            this.labelNewOAUComp2Charge.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUComp2Charge.Location = new System.Drawing.Point(210, 29);
            this.labelNewOAUComp2Charge.Name = "labelNewOAUComp2Charge";
            this.labelNewOAUComp2Charge.Size = new System.Drawing.Size(110, 21);
            this.labelNewOAUComp2Charge.TabIndex = 12;
            this.labelNewOAUComp2Charge.Text = "Circuit 2 Charge:";
            this.labelNewOAUComp2Charge.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCircuit1Charge
            // 
            this.txtCircuit1Charge.BackColor = System.Drawing.Color.White;
            this.txtCircuit1Charge.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCircuit1Charge.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtCircuit1Charge.Location = new System.Drawing.Point(126, 28);
            this.txtCircuit1Charge.Name = "txtCircuit1Charge";
            this.txtCircuit1Charge.Size = new System.Drawing.Size(60, 22);
            this.txtCircuit1Charge.TabIndex = 22;
            this.txtCircuit1Charge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUComp1Charge
            // 
            this.labelNewOAUComp1Charge.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUComp1Charge.Location = new System.Drawing.Point(12, 28);
            this.labelNewOAUComp1Charge.Name = "labelNewOAUComp1Charge";
            this.labelNewOAUComp1Charge.Size = new System.Drawing.Size(110, 21);
            this.labelNewOAUComp1Charge.TabIndex = 12;
            this.labelNewOAUComp1Charge.Text = "Circuit 1 Charge:";
            this.labelNewOAUComp1Charge.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBox1.Controls.Add(this.txtComp6LRA);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.txtComp6RLA_Volts);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.txtComp6Phase);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.txtComp6Qty);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(519, 300);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(194, 134);
            this.groupBox1.TabIndex = 68;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Compressor 2-3";
            // 
            // txtComp6LRA
            // 
            this.txtComp6LRA.BackColor = System.Drawing.Color.White;
            this.txtComp6LRA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp6LRA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp6LRA.Location = new System.Drawing.Point(126, 101);
            this.txtComp6LRA.Name = "txtComp6LRA";
            this.txtComp6LRA.Size = new System.Drawing.Size(60, 22);
            this.txtComp6LRA.TabIndex = 35;
            this.txtComp6LRA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(13, 101);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(110, 21);
            this.label24.TabIndex = 10;
            this.label24.Text = "LRA:";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtComp6RLA_Volts
            // 
            this.txtComp6RLA_Volts.BackColor = System.Drawing.Color.White;
            this.txtComp6RLA_Volts.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp6RLA_Volts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp6RLA_Volts.Location = new System.Drawing.Point(126, 73);
            this.txtComp6RLA_Volts.Name = "txtComp6RLA_Volts";
            this.txtComp6RLA_Volts.Size = new System.Drawing.Size(60, 22);
            this.txtComp6RLA_Volts.TabIndex = 34;
            this.txtComp6RLA_Volts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(12, 73);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(110, 21);
            this.label25.TabIndex = 8;
            this.label25.Text = "RLA-Volts:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtComp6Phase
            // 
            this.txtComp6Phase.BackColor = System.Drawing.Color.White;
            this.txtComp6Phase.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp6Phase.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp6Phase.Location = new System.Drawing.Point(126, 46);
            this.txtComp6Phase.Name = "txtComp6Phase";
            this.txtComp6Phase.Size = new System.Drawing.Size(60, 22);
            this.txtComp6Phase.TabIndex = 33;
            this.txtComp6Phase.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(12, 46);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(110, 21);
            this.label26.TabIndex = 6;
            this.label26.Text = "Phase:";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtComp6Qty
            // 
            this.txtComp6Qty.BackColor = System.Drawing.Color.White;
            this.txtComp6Qty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp6Qty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp6Qty.Location = new System.Drawing.Point(126, 19);
            this.txtComp6Qty.Name = "txtComp6Qty";
            this.txtComp6Qty.Size = new System.Drawing.Size(60, 22);
            this.txtComp6Qty.TabIndex = 32;
            this.txtComp6Qty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(12, 19);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(110, 21);
            this.label27.TabIndex = 4;
            this.label27.Text = "Qty:";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbComp2_2
            // 
            this.gbComp2_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gbComp2_2.Controls.Add(this.txtComp5LRA);
            this.gbComp2_2.Controls.Add(this.label20);
            this.gbComp2_2.Controls.Add(this.txtComp5RLA_Volts);
            this.gbComp2_2.Controls.Add(this.label21);
            this.gbComp2_2.Controls.Add(this.txtComp5Phase);
            this.gbComp2_2.Controls.Add(this.label22);
            this.gbComp2_2.Controls.Add(this.txtComp5Qty);
            this.gbComp2_2.Controls.Add(this.label23);
            this.gbComp2_2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbComp2_2.Location = new System.Drawing.Point(519, 161);
            this.gbComp2_2.Name = "gbComp2_2";
            this.gbComp2_2.Size = new System.Drawing.Size(194, 134);
            this.gbComp2_2.TabIndex = 67;
            this.gbComp2_2.TabStop = false;
            this.gbComp2_2.Text = "Compressor 2-2";
            // 
            // txtComp5LRA
            // 
            this.txtComp5LRA.BackColor = System.Drawing.Color.White;
            this.txtComp5LRA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp5LRA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp5LRA.Location = new System.Drawing.Point(126, 101);
            this.txtComp5LRA.Name = "txtComp5LRA";
            this.txtComp5LRA.Size = new System.Drawing.Size(60, 22);
            this.txtComp5LRA.TabIndex = 35;
            this.txtComp5LRA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(13, 101);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(110, 21);
            this.label20.TabIndex = 10;
            this.label20.Text = "LRA:";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtComp5RLA_Volts
            // 
            this.txtComp5RLA_Volts.BackColor = System.Drawing.Color.White;
            this.txtComp5RLA_Volts.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp5RLA_Volts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp5RLA_Volts.Location = new System.Drawing.Point(126, 73);
            this.txtComp5RLA_Volts.Name = "txtComp5RLA_Volts";
            this.txtComp5RLA_Volts.Size = new System.Drawing.Size(60, 22);
            this.txtComp5RLA_Volts.TabIndex = 34;
            this.txtComp5RLA_Volts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(12, 73);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(110, 21);
            this.label21.TabIndex = 8;
            this.label21.Text = "RLA-Volts:";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtComp5Phase
            // 
            this.txtComp5Phase.BackColor = System.Drawing.Color.White;
            this.txtComp5Phase.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp5Phase.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp5Phase.Location = new System.Drawing.Point(126, 46);
            this.txtComp5Phase.Name = "txtComp5Phase";
            this.txtComp5Phase.Size = new System.Drawing.Size(60, 22);
            this.txtComp5Phase.TabIndex = 33;
            this.txtComp5Phase.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(12, 46);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(110, 21);
            this.label22.TabIndex = 6;
            this.label22.Text = "Phase:";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtComp5Qty
            // 
            this.txtComp5Qty.BackColor = System.Drawing.Color.White;
            this.txtComp5Qty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp5Qty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp5Qty.Location = new System.Drawing.Point(126, 19);
            this.txtComp5Qty.Name = "txtComp5Qty";
            this.txtComp5Qty.Size = new System.Drawing.Size(60, 22);
            this.txtComp5Qty.TabIndex = 32;
            this.txtComp5Qty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(12, 19);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(110, 21);
            this.label23.TabIndex = 4;
            this.label23.Text = "Qty:";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxNewOAULanguage
            // 
            this.groupBoxNewOAULanguage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewOAULanguage.Controls.Add(this.checkBoxLangFrench);
            this.groupBoxNewOAULanguage.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewOAULanguage.Location = new System.Drawing.Point(961, 353);
            this.groupBoxNewOAULanguage.Name = "groupBoxNewOAULanguage";
            this.groupBoxNewOAULanguage.Size = new System.Drawing.Size(110, 50);
            this.groupBoxNewOAULanguage.TabIndex = 66;
            this.groupBoxNewOAULanguage.TabStop = false;
            this.groupBoxNewOAULanguage.Text = "Language";
            // 
            // checkBoxLangFrench
            // 
            this.checkBoxLangFrench.Location = new System.Drawing.Point(14, 19);
            this.checkBoxLangFrench.Name = "checkBoxLangFrench";
            this.checkBoxLangFrench.Size = new System.Drawing.Size(84, 23);
            this.checkBoxLangFrench.TabIndex = 63;
            this.checkBoxLangFrench.Text = "French";
            this.checkBoxLangFrench.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxLangFrench.UseVisualStyleBackColor = true;
            // 
            // groupBoxNewOAUWater_Glycol
            // 
            this.groupBoxNewOAUWater_Glycol.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewOAUWater_Glycol.Controls.Add(this.radioButtonGlycol);
            this.groupBoxNewOAUWater_Glycol.Controls.Add(this.radioButtonWater);
            this.groupBoxNewOAUWater_Glycol.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewOAUWater_Glycol.Location = new System.Drawing.Point(961, 408);
            this.groupBoxNewOAUWater_Glycol.Name = "groupBoxNewOAUWater_Glycol";
            this.groupBoxNewOAUWater_Glycol.Size = new System.Drawing.Size(110, 78);
            this.groupBoxNewOAUWater_Glycol.TabIndex = 65;
            this.groupBoxNewOAUWater_Glycol.TabStop = false;
            this.groupBoxNewOAUWater_Glycol.Text = "Water/Glycol";
            this.groupBoxNewOAUWater_Glycol.Visible = false;
            // 
            // radioButtonGlycol
            // 
            this.radioButtonGlycol.AutoSize = true;
            this.radioButtonGlycol.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonGlycol.Location = new System.Drawing.Point(23, 50);
            this.radioButtonGlycol.Name = "radioButtonGlycol";
            this.radioButtonGlycol.Size = new System.Drawing.Size(60, 18);
            this.radioButtonGlycol.TabIndex = 1;
            this.radioButtonGlycol.Text = "Glycol";
            this.radioButtonGlycol.UseVisualStyleBackColor = true;
            // 
            // radioButtonWater
            // 
            this.radioButtonWater.AutoSize = true;
            this.radioButtonWater.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonWater.Location = new System.Drawing.Point(23, 21);
            this.radioButtonWater.Name = "radioButtonWater";
            this.radioButtonWater.Size = new System.Drawing.Size(62, 18);
            this.radioButtonWater.TabIndex = 0;
            this.radioButtonWater.Text = "Water";
            this.radioButtonWater.UseVisualStyleBackColor = true;
            // 
            // groupBoxNewOAUSteam
            // 
            this.groupBoxNewOAUSteam.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewOAUSteam.Controls.Add(this.textBoxNewOAUSteamCondensateFlowRate);
            this.groupBoxNewOAUSteam.Controls.Add(this.label8);
            this.groupBoxNewOAUSteam.Controls.Add(this.textBoxNewOAUSteamOperatingPressure);
            this.groupBoxNewOAUSteam.Controls.Add(this.label9);
            this.groupBoxNewOAUSteam.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewOAUSteam.Location = new System.Drawing.Point(2, 361);
            this.groupBoxNewOAUSteam.Name = "groupBoxNewOAUSteam";
            this.groupBoxNewOAUSteam.Size = new System.Drawing.Size(311, 81);
            this.groupBoxNewOAUSteam.TabIndex = 63;
            this.groupBoxNewOAUSteam.TabStop = false;
            this.groupBoxNewOAUSteam.Text = "Steam";
            this.groupBoxNewOAUSteam.Visible = false;
            // 
            // textBoxNewOAUSteamCondensateFlowRate
            // 
            this.textBoxNewOAUSteamCondensateFlowRate.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUSteamCondensateFlowRate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUSteamCondensateFlowRate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUSteamCondensateFlowRate.Location = new System.Drawing.Point(187, 46);
            this.textBoxNewOAUSteamCondensateFlowRate.Name = "textBoxNewOAUSteamCondensateFlowRate";
            this.textBoxNewOAUSteamCondensateFlowRate.Size = new System.Drawing.Size(61, 22);
            this.textBoxNewOAUSteamCondensateFlowRate.TabIndex = 11;
            this.textBoxNewOAUSteamCondensateFlowRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(41, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(140, 22);
            this.label8.TabIndex = 10;
            this.label8.Text = "Condensate Flow Rate:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUSteamOperatingPressure
            // 
            this.textBoxNewOAUSteamOperatingPressure.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUSteamOperatingPressure.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUSteamOperatingPressure.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUSteamOperatingPressure.Location = new System.Drawing.Point(187, 19);
            this.textBoxNewOAUSteamOperatingPressure.Name = "textBoxNewOAUSteamOperatingPressure";
            this.textBoxNewOAUSteamOperatingPressure.Size = new System.Drawing.Size(61, 22);
            this.textBoxNewOAUSteamOperatingPressure.TabIndex = 9;
            this.textBoxNewOAUSteamOperatingPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(41, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(140, 22);
            this.label9.TabIndex = 8;
            this.label9.Text = "Operating Pressure:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxNewOAUHotWater
            // 
            this.groupBoxNewOAUHotWater.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewOAUHotWater.Controls.Add(this.textBoxHotWaterEnteringWaterTemp);
            this.groupBoxNewOAUHotWater.Controls.Add(this.label2);
            this.groupBoxNewOAUHotWater.Controls.Add(this.textBoxHotWaterFlowRate);
            this.groupBoxNewOAUHotWater.Controls.Add(this.label1);
            this.groupBoxNewOAUHotWater.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewOAUHotWater.Location = new System.Drawing.Point(321, 512);
            this.groupBoxNewOAUHotWater.Name = "groupBoxNewOAUHotWater";
            this.groupBoxNewOAUHotWater.Size = new System.Drawing.Size(311, 81);
            this.groupBoxNewOAUHotWater.TabIndex = 62;
            this.groupBoxNewOAUHotWater.TabStop = false;
            this.groupBoxNewOAUHotWater.Text = "Hot Water";
            // 
            // textBoxHotWaterEnteringWaterTemp
            // 
            this.textBoxHotWaterEnteringWaterTemp.BackColor = System.Drawing.Color.White;
            this.textBoxHotWaterEnteringWaterTemp.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxHotWaterEnteringWaterTemp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxHotWaterEnteringWaterTemp.Location = new System.Drawing.Point(187, 46);
            this.textBoxHotWaterEnteringWaterTemp.Name = "textBoxHotWaterEnteringWaterTemp";
            this.textBoxHotWaterEnteringWaterTemp.Size = new System.Drawing.Size(95, 22);
            this.textBoxHotWaterEnteringWaterTemp.TabIndex = 11;
            this.textBoxHotWaterEnteringWaterTemp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(41, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 22);
            this.label2.TabIndex = 10;
            this.label2.Text = "Entering Water Temp:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxHotWaterFlowRate
            // 
            this.textBoxHotWaterFlowRate.BackColor = System.Drawing.Color.White;
            this.textBoxHotWaterFlowRate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxHotWaterFlowRate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxHotWaterFlowRate.Location = new System.Drawing.Point(187, 19);
            this.textBoxHotWaterFlowRate.Name = "textBoxHotWaterFlowRate";
            this.textBoxHotWaterFlowRate.Size = new System.Drawing.Size(95, 22);
            this.textBoxHotWaterFlowRate.TabIndex = 9;
            this.textBoxHotWaterFlowRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(41, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 22);
            this.label1.TabIndex = 8;
            this.label1.Text = "Hot Water Flow Rate:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxParseModelNo
            // 
            this.groupBoxParseModelNo.BackColor = System.Drawing.Color.LightSkyBlue;
            this.groupBoxParseModelNo.Controls.Add(this.label6);
            this.groupBoxParseModelNo.Controls.Add(this.buttonModelNoCancel);
            this.groupBoxParseModelNo.Controls.Add(this.buttonModelNoEdit);
            this.groupBoxParseModelNo.Controls.Add(this.buttonModelNoParse);
            this.groupBoxParseModelNo.Controls.Add(this.textBoxModifyModelNo);
            this.groupBoxParseModelNo.Controls.Add(this.label5);
            this.groupBoxParseModelNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxParseModelNo.ForeColor = System.Drawing.Color.Blue;
            this.groupBoxParseModelNo.Location = new System.Drawing.Point(323, 552);
            this.groupBoxParseModelNo.Name = "groupBoxParseModelNo";
            this.groupBoxParseModelNo.Size = new System.Drawing.Size(311, 100);
            this.groupBoxParseModelNo.TabIndex = 64;
            this.groupBoxParseModelNo.TabStop = false;
            this.groupBoxParseModelNo.Text = "Modify ModelNo";
            this.groupBoxParseModelNo.Visible = false;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(175, 21);
            this.label6.TabIndex = 60;
            this.label6.Text = "Secondary Heating Input:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label6.Visible = false;
            // 
            // buttonModelNoCancel
            // 
            this.buttonModelNoCancel.ForeColor = System.Drawing.Color.Red;
            this.buttonModelNoCancel.Location = new System.Drawing.Point(223, 66);
            this.buttonModelNoCancel.Name = "buttonModelNoCancel";
            this.buttonModelNoCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonModelNoCancel.TabIndex = 8;
            this.buttonModelNoCancel.Text = "Cancel";
            this.buttonModelNoCancel.UseVisualStyleBackColor = true;
            this.buttonModelNoCancel.Click += new System.EventHandler(this.buttonModelNoCancel_Click);
            // 
            // buttonModelNoEdit
            // 
            this.buttonModelNoEdit.Location = new System.Drawing.Point(120, 66);
            this.buttonModelNoEdit.Name = "buttonModelNoEdit";
            this.buttonModelNoEdit.Size = new System.Drawing.Size(75, 23);
            this.buttonModelNoEdit.TabIndex = 7;
            this.buttonModelNoEdit.Text = "Accept";
            this.buttonModelNoEdit.UseVisualStyleBackColor = true;
            this.buttonModelNoEdit.Click += new System.EventHandler(this.buttonModelNoEdit_Click);
            // 
            // buttonModelNoParse
            // 
            this.buttonModelNoParse.Location = new System.Drawing.Point(15, 66);
            this.buttonModelNoParse.Name = "buttonModelNoParse";
            this.buttonModelNoParse.Size = new System.Drawing.Size(75, 23);
            this.buttonModelNoParse.TabIndex = 6;
            this.buttonModelNoParse.Text = "Parse";
            this.buttonModelNoParse.UseVisualStyleBackColor = true;
            this.buttonModelNoParse.Click += new System.EventHandler(this.buttonModelNoParse_Click);
            // 
            // textBoxModifyModelNo
            // 
            this.textBoxModifyModelNo.BackColor = System.Drawing.Color.White;
            this.textBoxModifyModelNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxModifyModelNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxModifyModelNo.Location = new System.Drawing.Point(15, 35);
            this.textBoxModifyModelNo.Name = "textBoxModifyModelNo";
            this.textBoxModifyModelNo.Size = new System.Drawing.Size(283, 22);
            this.textBoxModifyModelNo.TabIndex = 5;
            this.textBoxModifyModelNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 21);
            this.label5.TabIndex = 4;
            this.label5.Text = "Model No:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonNewOAUPrint
            // 
            this.buttonNewOAUPrint.Enabled = false;
            this.buttonNewOAUPrint.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewOAUPrint.Location = new System.Drawing.Point(970, 592);
            this.buttonNewOAUPrint.Name = "buttonNewOAUPrint";
            this.buttonNewOAUPrint.Size = new System.Drawing.Size(95, 37);
            this.buttonNewOAUPrint.TabIndex = 61;
            this.buttonNewOAUPrint.Text = "Print";
            this.buttonNewOAUPrint.UseVisualStyleBackColor = true;
            this.buttonNewOAUPrint.Click += new System.EventHandler(this.buttonNewOAUPrint_Click);
            // 
            // buttonNewOAUAccept
            // 
            this.buttonNewOAUAccept.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewOAUAccept.Location = new System.Drawing.Point(970, 544);
            this.buttonNewOAUAccept.Name = "buttonNewOAUAccept";
            this.buttonNewOAUAccept.Size = new System.Drawing.Size(95, 37);
            this.buttonNewOAUAccept.TabIndex = 60;
            this.buttonNewOAUAccept.Text = "Accept";
            this.buttonNewOAUAccept.UseVisualStyleBackColor = true;
            this.buttonNewOAUAccept.Click += new System.EventHandler(this.buttonNewOAUAccept_Click);
            // 
            // buttonNewOAUCancel
            // 
            this.buttonNewOAUCancel.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewOAUCancel.ForeColor = System.Drawing.Color.Red;
            this.buttonNewOAUCancel.Location = new System.Drawing.Point(970, 496);
            this.buttonNewOAUCancel.Name = "buttonNewOAUCancel";
            this.buttonNewOAUCancel.Size = new System.Drawing.Size(95, 37);
            this.buttonNewOAUCancel.TabIndex = 59;
            this.buttonNewOAUCancel.Text = "Cancel";
            this.buttonNewOAUCancel.UseVisualStyleBackColor = true;
            this.buttonNewOAUCancel.Click += new System.EventHandler(this.buttonNewOAUCancel_Click);
            // 
            // groupBoxNewOAUFanInfo
            // 
            this.groupBoxNewOAUFanInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewOAUFanInfo.Controls.Add(this.textBoxNewOAUFanPwrHP);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.labelNewOAUFanPwrHP);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.textBoxNewOAUFanPwrFLA);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.labelNewOAUFanPwrFLA);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.textBoxNewOAUFanPwrPH);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.labelNewOAUFanPwrPH);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.textBoxNewOAUFanPwrQty);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.labelNewOAUFanPwrQty);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.textBoxNewOAUFanEvapHP);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.labelNewOAUFanEvapHP);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.textBoxNewOAUFanEvapFLA);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.labelNewOAUFanEvapFLA);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.textBoxNewOAUFanEvapPH);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.labelNewOAUFanEvapPH);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.textBoxNewOAUFanEvapQty);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.labelNewOAUFanEvapQty);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.textBoxNewOAUFanEnergyHP);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.labelNewOAUFanEnergyHP);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.textBoxNewOAUFanEnergyFLA);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.labelNewOAUFanEnergyFLA);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.textBoxNewOAUFanEnergyPH);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.labelNewOAUFanEnergyPH);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.textBoxNewOAUFanEnergyQty);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.labelNewOAUFanEnergyQty);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.textBoxNewOAUFanCondHP);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.labelNewOAUFanCondHP);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.textBoxNewOAUFanCondFLA);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.labelNewOAUFanCondFLA);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.textBoxNewOAUFanCondPH);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.labelNewOAUFanCondPH);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.textBoxNewOAUFanCondQty);
            this.groupBoxNewOAUFanInfo.Controls.Add(this.labelNewOAUFanCondQty);
            this.groupBoxNewOAUFanInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewOAUFanInfo.Location = new System.Drawing.Point(717, 22);
            this.groupBoxNewOAUFanInfo.Name = "groupBoxNewOAUFanInfo";
            this.groupBoxNewOAUFanInfo.Size = new System.Drawing.Size(354, 325);
            this.groupBoxNewOAUFanInfo.TabIndex = 9;
            this.groupBoxNewOAUFanInfo.TabStop = false;
            this.groupBoxNewOAUFanInfo.Text = "Fans";
            // 
            // textBoxNewOAUFanPwrHP
            // 
            this.textBoxNewOAUFanPwrHP.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUFanPwrHP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUFanPwrHP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUFanPwrHP.Location = new System.Drawing.Point(292, 254);
            this.textBoxNewOAUFanPwrHP.Name = "textBoxNewOAUFanPwrHP";
            this.textBoxNewOAUFanPwrHP.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewOAUFanPwrHP.TabIndex = 51;
            this.textBoxNewOAUFanPwrHP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUFanPwrHP
            // 
            this.labelNewOAUFanPwrHP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUFanPwrHP.Location = new System.Drawing.Point(188, 254);
            this.labelNewOAUFanPwrHP.Name = "labelNewOAUFanPwrHP";
            this.labelNewOAUFanPwrHP.Size = new System.Drawing.Size(100, 21);
            this.labelNewOAUFanPwrHP.TabIndex = 36;
            this.labelNewOAUFanPwrHP.Text = "Fan EXH HP:";
            this.labelNewOAUFanPwrHP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUFanPwrFLA
            // 
            this.textBoxNewOAUFanPwrFLA.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUFanPwrFLA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUFanPwrFLA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUFanPwrFLA.Location = new System.Drawing.Point(292, 226);
            this.textBoxNewOAUFanPwrFLA.Name = "textBoxNewOAUFanPwrFLA";
            this.textBoxNewOAUFanPwrFLA.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewOAUFanPwrFLA.TabIndex = 50;
            this.textBoxNewOAUFanPwrFLA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxNewOAUFanPwrFLA.Leave += new System.EventHandler(this.textBoxNewOAUFanPwrFLA_Leave);
            // 
            // labelNewOAUFanPwrFLA
            // 
            this.labelNewOAUFanPwrFLA.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUFanPwrFLA.Location = new System.Drawing.Point(188, 226);
            this.labelNewOAUFanPwrFLA.Name = "labelNewOAUFanPwrFLA";
            this.labelNewOAUFanPwrFLA.Size = new System.Drawing.Size(100, 21);
            this.labelNewOAUFanPwrFLA.TabIndex = 34;
            this.labelNewOAUFanPwrFLA.Text = "Fan EXH FLA:";
            this.labelNewOAUFanPwrFLA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUFanPwrPH
            // 
            this.textBoxNewOAUFanPwrPH.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUFanPwrPH.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUFanPwrPH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUFanPwrPH.Location = new System.Drawing.Point(292, 198);
            this.textBoxNewOAUFanPwrPH.Name = "textBoxNewOAUFanPwrPH";
            this.textBoxNewOAUFanPwrPH.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewOAUFanPwrPH.TabIndex = 49;
            this.textBoxNewOAUFanPwrPH.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUFanPwrPH
            // 
            this.labelNewOAUFanPwrPH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUFanPwrPH.Location = new System.Drawing.Point(188, 198);
            this.labelNewOAUFanPwrPH.Name = "labelNewOAUFanPwrPH";
            this.labelNewOAUFanPwrPH.Size = new System.Drawing.Size(100, 21);
            this.labelNewOAUFanPwrPH.TabIndex = 32;
            this.labelNewOAUFanPwrPH.Text = "Fan EXH PH:";
            this.labelNewOAUFanPwrPH.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUFanPwrQty
            // 
            this.textBoxNewOAUFanPwrQty.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUFanPwrQty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUFanPwrQty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUFanPwrQty.Location = new System.Drawing.Point(292, 171);
            this.textBoxNewOAUFanPwrQty.Name = "textBoxNewOAUFanPwrQty";
            this.textBoxNewOAUFanPwrQty.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewOAUFanPwrQty.TabIndex = 48;
            this.textBoxNewOAUFanPwrQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUFanPwrQty
            // 
            this.labelNewOAUFanPwrQty.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUFanPwrQty.Location = new System.Drawing.Point(188, 171);
            this.labelNewOAUFanPwrQty.Name = "labelNewOAUFanPwrQty";
            this.labelNewOAUFanPwrQty.Size = new System.Drawing.Size(100, 21);
            this.labelNewOAUFanPwrQty.TabIndex = 30;
            this.labelNewOAUFanPwrQty.Text = "Fan EXH Qty:";
            this.labelNewOAUFanPwrQty.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUFanEvapHP
            // 
            this.textBoxNewOAUFanEvapHP.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUFanEvapHP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUFanEvapHP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUFanEvapHP.Location = new System.Drawing.Point(292, 104);
            this.textBoxNewOAUFanEvapHP.Name = "textBoxNewOAUFanEvapHP";
            this.textBoxNewOAUFanEvapHP.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewOAUFanEvapHP.TabIndex = 47;
            this.textBoxNewOAUFanEvapHP.Text = " ";
            this.textBoxNewOAUFanEvapHP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUFanEvapHP
            // 
            this.labelNewOAUFanEvapHP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUFanEvapHP.Location = new System.Drawing.Point(181, 104);
            this.labelNewOAUFanEvapHP.Name = "labelNewOAUFanEvapHP";
            this.labelNewOAUFanEvapHP.Size = new System.Drawing.Size(107, 21);
            this.labelNewOAUFanEvapHP.TabIndex = 28;
            this.labelNewOAUFanEvapHP.Text = "Fan Evap HP:";
            this.labelNewOAUFanEvapHP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUFanEvapFLA
            // 
            this.textBoxNewOAUFanEvapFLA.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUFanEvapFLA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUFanEvapFLA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUFanEvapFLA.Location = new System.Drawing.Point(292, 76);
            this.textBoxNewOAUFanEvapFLA.Name = "textBoxNewOAUFanEvapFLA";
            this.textBoxNewOAUFanEvapFLA.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewOAUFanEvapFLA.TabIndex = 46;
            this.textBoxNewOAUFanEvapFLA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxNewOAUFanEvapFLA.Leave += new System.EventHandler(this.textBoxNewOAUFanEvapFLA_Leave);
            // 
            // labelNewOAUFanEvapFLA
            // 
            this.labelNewOAUFanEvapFLA.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUFanEvapFLA.Location = new System.Drawing.Point(188, 76);
            this.labelNewOAUFanEvapFLA.Name = "labelNewOAUFanEvapFLA";
            this.labelNewOAUFanEvapFLA.Size = new System.Drawing.Size(100, 21);
            this.labelNewOAUFanEvapFLA.TabIndex = 26;
            this.labelNewOAUFanEvapFLA.Text = "Fan Evap FLA:";
            this.labelNewOAUFanEvapFLA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUFanEvapPH
            // 
            this.textBoxNewOAUFanEvapPH.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUFanEvapPH.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUFanEvapPH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUFanEvapPH.Location = new System.Drawing.Point(292, 48);
            this.textBoxNewOAUFanEvapPH.Name = "textBoxNewOAUFanEvapPH";
            this.textBoxNewOAUFanEvapPH.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewOAUFanEvapPH.TabIndex = 45;
            this.textBoxNewOAUFanEvapPH.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUFanEvapPH
            // 
            this.labelNewOAUFanEvapPH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUFanEvapPH.Location = new System.Drawing.Point(188, 48);
            this.labelNewOAUFanEvapPH.Name = "labelNewOAUFanEvapPH";
            this.labelNewOAUFanEvapPH.Size = new System.Drawing.Size(100, 21);
            this.labelNewOAUFanEvapPH.TabIndex = 24;
            this.labelNewOAUFanEvapPH.Text = "Fan Evap PH:";
            this.labelNewOAUFanEvapPH.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUFanEvapQty
            // 
            this.textBoxNewOAUFanEvapQty.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUFanEvapQty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUFanEvapQty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUFanEvapQty.Location = new System.Drawing.Point(292, 21);
            this.textBoxNewOAUFanEvapQty.Name = "textBoxNewOAUFanEvapQty";
            this.textBoxNewOAUFanEvapQty.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewOAUFanEvapQty.TabIndex = 44;
            this.textBoxNewOAUFanEvapQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUFanEvapQty
            // 
            this.labelNewOAUFanEvapQty.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUFanEvapQty.Location = new System.Drawing.Point(188, 21);
            this.labelNewOAUFanEvapQty.Name = "labelNewOAUFanEvapQty";
            this.labelNewOAUFanEvapQty.Size = new System.Drawing.Size(100, 21);
            this.labelNewOAUFanEvapQty.TabIndex = 22;
            this.labelNewOAUFanEvapQty.Text = "Fan Evap Qty:";
            this.labelNewOAUFanEvapQty.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUFanEnergyHP
            // 
            this.textBoxNewOAUFanEnergyHP.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUFanEnergyHP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUFanEnergyHP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUFanEnergyHP.Location = new System.Drawing.Point(114, 254);
            this.textBoxNewOAUFanEnergyHP.Name = "textBoxNewOAUFanEnergyHP";
            this.textBoxNewOAUFanEnergyHP.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewOAUFanEnergyHP.TabIndex = 43;
            this.textBoxNewOAUFanEnergyHP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUFanEnergyHP
            // 
            this.labelNewOAUFanEnergyHP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUFanEnergyHP.Location = new System.Drawing.Point(10, 254);
            this.labelNewOAUFanEnergyHP.Name = "labelNewOAUFanEnergyHP";
            this.labelNewOAUFanEnergyHP.Size = new System.Drawing.Size(100, 21);
            this.labelNewOAUFanEnergyHP.TabIndex = 20;
            this.labelNewOAUFanEnergyHP.Text = "Fan ERV HP:";
            this.labelNewOAUFanEnergyHP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUFanEnergyFLA
            // 
            this.textBoxNewOAUFanEnergyFLA.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUFanEnergyFLA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUFanEnergyFLA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUFanEnergyFLA.Location = new System.Drawing.Point(114, 226);
            this.textBoxNewOAUFanEnergyFLA.Name = "textBoxNewOAUFanEnergyFLA";
            this.textBoxNewOAUFanEnergyFLA.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewOAUFanEnergyFLA.TabIndex = 42;
            this.textBoxNewOAUFanEnergyFLA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxNewOAUFanEnergyFLA.Leave += new System.EventHandler(this.textBoxNewOAUFanEnergyFLA_Leave);
            // 
            // labelNewOAUFanEnergyFLA
            // 
            this.labelNewOAUFanEnergyFLA.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUFanEnergyFLA.Location = new System.Drawing.Point(10, 226);
            this.labelNewOAUFanEnergyFLA.Name = "labelNewOAUFanEnergyFLA";
            this.labelNewOAUFanEnergyFLA.Size = new System.Drawing.Size(100, 21);
            this.labelNewOAUFanEnergyFLA.TabIndex = 18;
            this.labelNewOAUFanEnergyFLA.Text = "Fan ERV FLA:";
            this.labelNewOAUFanEnergyFLA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUFanEnergyPH
            // 
            this.textBoxNewOAUFanEnergyPH.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUFanEnergyPH.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUFanEnergyPH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUFanEnergyPH.Location = new System.Drawing.Point(114, 198);
            this.textBoxNewOAUFanEnergyPH.Name = "textBoxNewOAUFanEnergyPH";
            this.textBoxNewOAUFanEnergyPH.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewOAUFanEnergyPH.TabIndex = 41;
            this.textBoxNewOAUFanEnergyPH.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUFanEnergyPH
            // 
            this.labelNewOAUFanEnergyPH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUFanEnergyPH.Location = new System.Drawing.Point(10, 198);
            this.labelNewOAUFanEnergyPH.Name = "labelNewOAUFanEnergyPH";
            this.labelNewOAUFanEnergyPH.Size = new System.Drawing.Size(100, 21);
            this.labelNewOAUFanEnergyPH.TabIndex = 16;
            this.labelNewOAUFanEnergyPH.Text = "Fan ERV PH:";
            this.labelNewOAUFanEnergyPH.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUFanEnergyQty
            // 
            this.textBoxNewOAUFanEnergyQty.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUFanEnergyQty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUFanEnergyQty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUFanEnergyQty.Location = new System.Drawing.Point(114, 171);
            this.textBoxNewOAUFanEnergyQty.Name = "textBoxNewOAUFanEnergyQty";
            this.textBoxNewOAUFanEnergyQty.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewOAUFanEnergyQty.TabIndex = 40;
            this.textBoxNewOAUFanEnergyQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUFanEnergyQty
            // 
            this.labelNewOAUFanEnergyQty.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUFanEnergyQty.Location = new System.Drawing.Point(10, 171);
            this.labelNewOAUFanEnergyQty.Name = "labelNewOAUFanEnergyQty";
            this.labelNewOAUFanEnergyQty.Size = new System.Drawing.Size(100, 21);
            this.labelNewOAUFanEnergyQty.TabIndex = 14;
            this.labelNewOAUFanEnergyQty.Text = "Fan ERV Qty:";
            this.labelNewOAUFanEnergyQty.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUFanCondHP
            // 
            this.textBoxNewOAUFanCondHP.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUFanCondHP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUFanCondHP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUFanCondHP.Location = new System.Drawing.Point(114, 102);
            this.textBoxNewOAUFanCondHP.Name = "textBoxNewOAUFanCondHP";
            this.textBoxNewOAUFanCondHP.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewOAUFanCondHP.TabIndex = 39;
            this.textBoxNewOAUFanCondHP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUFanCondHP
            // 
            this.labelNewOAUFanCondHP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUFanCondHP.Location = new System.Drawing.Point(10, 102);
            this.labelNewOAUFanCondHP.Name = "labelNewOAUFanCondHP";
            this.labelNewOAUFanCondHP.Size = new System.Drawing.Size(100, 21);
            this.labelNewOAUFanCondHP.TabIndex = 12;
            this.labelNewOAUFanCondHP.Text = "Fan Cond HP:";
            this.labelNewOAUFanCondHP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUFanCondFLA
            // 
            this.textBoxNewOAUFanCondFLA.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUFanCondFLA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUFanCondFLA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUFanCondFLA.Location = new System.Drawing.Point(114, 75);
            this.textBoxNewOAUFanCondFLA.Name = "textBoxNewOAUFanCondFLA";
            this.textBoxNewOAUFanCondFLA.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewOAUFanCondFLA.TabIndex = 38;
            this.textBoxNewOAUFanCondFLA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxNewOAUFanCondFLA.Leave += new System.EventHandler(this.textBoxNewOAUFanCondFLA_Leave);
            // 
            // labelNewOAUFanCondFLA
            // 
            this.labelNewOAUFanCondFLA.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUFanCondFLA.Location = new System.Drawing.Point(10, 75);
            this.labelNewOAUFanCondFLA.Name = "labelNewOAUFanCondFLA";
            this.labelNewOAUFanCondFLA.Size = new System.Drawing.Size(100, 21);
            this.labelNewOAUFanCondFLA.TabIndex = 10;
            this.labelNewOAUFanCondFLA.Text = "Fan Cond FLA:";
            this.labelNewOAUFanCondFLA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUFanCondPH
            // 
            this.textBoxNewOAUFanCondPH.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUFanCondPH.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUFanCondPH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUFanCondPH.Location = new System.Drawing.Point(114, 47);
            this.textBoxNewOAUFanCondPH.Name = "textBoxNewOAUFanCondPH";
            this.textBoxNewOAUFanCondPH.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewOAUFanCondPH.TabIndex = 37;
            this.textBoxNewOAUFanCondPH.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUFanCondPH
            // 
            this.labelNewOAUFanCondPH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUFanCondPH.Location = new System.Drawing.Point(10, 47);
            this.labelNewOAUFanCondPH.Name = "labelNewOAUFanCondPH";
            this.labelNewOAUFanCondPH.Size = new System.Drawing.Size(100, 21);
            this.labelNewOAUFanCondPH.TabIndex = 8;
            this.labelNewOAUFanCondPH.Text = "Fan Cond PH:";
            this.labelNewOAUFanCondPH.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUFanCondQty
            // 
            this.textBoxNewOAUFanCondQty.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUFanCondQty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUFanCondQty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUFanCondQty.Location = new System.Drawing.Point(114, 20);
            this.textBoxNewOAUFanCondQty.Name = "textBoxNewOAUFanCondQty";
            this.textBoxNewOAUFanCondQty.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewOAUFanCondQty.TabIndex = 36;
            this.textBoxNewOAUFanCondQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUFanCondQty
            // 
            this.labelNewOAUFanCondQty.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUFanCondQty.Location = new System.Drawing.Point(10, 20);
            this.labelNewOAUFanCondQty.Name = "labelNewOAUFanCondQty";
            this.labelNewOAUFanCondQty.Size = new System.Drawing.Size(100, 21);
            this.labelNewOAUFanCondQty.TabIndex = 6;
            this.labelNewOAUFanCondQty.Text = "Fan Cond Qty:";
            this.labelNewOAUFanCondQty.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxNewOAUCompressor4
            // 
            this.groupBoxNewOAUCompressor4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewOAUCompressor4.Controls.Add(this.txtComp4LRA);
            this.groupBoxNewOAUCompressor4.Controls.Add(this.labelNewOAUComp4LRA);
            this.groupBoxNewOAUCompressor4.Controls.Add(this.txtComp4RLA_Volts);
            this.groupBoxNewOAUCompressor4.Controls.Add(this.labelNewOAUComp4RLAVolts);
            this.groupBoxNewOAUCompressor4.Controls.Add(this.txtComp4Phase);
            this.groupBoxNewOAUCompressor4.Controls.Add(this.labelNewOAUComp4PH);
            this.groupBoxNewOAUCompressor4.Controls.Add(this.txtComp4Qty);
            this.groupBoxNewOAUCompressor4.Controls.Add(this.labelNewOAUComp4Qty);
            this.groupBoxNewOAUCompressor4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewOAUCompressor4.Location = new System.Drawing.Point(519, 22);
            this.groupBoxNewOAUCompressor4.Name = "groupBoxNewOAUCompressor4";
            this.groupBoxNewOAUCompressor4.Size = new System.Drawing.Size(194, 134);
            this.groupBoxNewOAUCompressor4.TabIndex = 8;
            this.groupBoxNewOAUCompressor4.TabStop = false;
            this.groupBoxNewOAUCompressor4.Text = "Compressor 2-1";
            // 
            // txtComp4LRA
            // 
            this.txtComp4LRA.BackColor = System.Drawing.Color.White;
            this.txtComp4LRA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp4LRA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp4LRA.Location = new System.Drawing.Point(126, 101);
            this.txtComp4LRA.Name = "txtComp4LRA";
            this.txtComp4LRA.Size = new System.Drawing.Size(60, 22);
            this.txtComp4LRA.TabIndex = 35;
            this.txtComp4LRA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUComp4LRA
            // 
            this.labelNewOAUComp4LRA.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUComp4LRA.Location = new System.Drawing.Point(13, 101);
            this.labelNewOAUComp4LRA.Name = "labelNewOAUComp4LRA";
            this.labelNewOAUComp4LRA.Size = new System.Drawing.Size(110, 21);
            this.labelNewOAUComp4LRA.TabIndex = 10;
            this.labelNewOAUComp4LRA.Text = "LRA:";
            this.labelNewOAUComp4LRA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtComp4RLA_Volts
            // 
            this.txtComp4RLA_Volts.BackColor = System.Drawing.Color.White;
            this.txtComp4RLA_Volts.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp4RLA_Volts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp4RLA_Volts.Location = new System.Drawing.Point(126, 73);
            this.txtComp4RLA_Volts.Name = "txtComp4RLA_Volts";
            this.txtComp4RLA_Volts.Size = new System.Drawing.Size(60, 22);
            this.txtComp4RLA_Volts.TabIndex = 34;
            this.txtComp4RLA_Volts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtComp4RLA_Volts.Leave += new System.EventHandler(this.textBoxNewOAUComp4RLAVolts_Leave);
            // 
            // labelNewOAUComp4RLAVolts
            // 
            this.labelNewOAUComp4RLAVolts.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUComp4RLAVolts.Location = new System.Drawing.Point(12, 73);
            this.labelNewOAUComp4RLAVolts.Name = "labelNewOAUComp4RLAVolts";
            this.labelNewOAUComp4RLAVolts.Size = new System.Drawing.Size(110, 21);
            this.labelNewOAUComp4RLAVolts.TabIndex = 8;
            this.labelNewOAUComp4RLAVolts.Text = "RLA-Volts:";
            this.labelNewOAUComp4RLAVolts.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtComp4Phase
            // 
            this.txtComp4Phase.BackColor = System.Drawing.Color.White;
            this.txtComp4Phase.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp4Phase.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp4Phase.Location = new System.Drawing.Point(126, 46);
            this.txtComp4Phase.Name = "txtComp4Phase";
            this.txtComp4Phase.Size = new System.Drawing.Size(60, 22);
            this.txtComp4Phase.TabIndex = 33;
            this.txtComp4Phase.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUComp4PH
            // 
            this.labelNewOAUComp4PH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUComp4PH.Location = new System.Drawing.Point(12, 46);
            this.labelNewOAUComp4PH.Name = "labelNewOAUComp4PH";
            this.labelNewOAUComp4PH.Size = new System.Drawing.Size(110, 21);
            this.labelNewOAUComp4PH.TabIndex = 6;
            this.labelNewOAUComp4PH.Text = "Phase:";
            this.labelNewOAUComp4PH.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtComp4Qty
            // 
            this.txtComp4Qty.BackColor = System.Drawing.Color.White;
            this.txtComp4Qty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp4Qty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp4Qty.Location = new System.Drawing.Point(126, 19);
            this.txtComp4Qty.Name = "txtComp4Qty";
            this.txtComp4Qty.Size = new System.Drawing.Size(60, 22);
            this.txtComp4Qty.TabIndex = 32;
            this.txtComp4Qty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUComp4Qty
            // 
            this.labelNewOAUComp4Qty.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUComp4Qty.Location = new System.Drawing.Point(12, 19);
            this.labelNewOAUComp4Qty.Name = "labelNewOAUComp4Qty";
            this.labelNewOAUComp4Qty.Size = new System.Drawing.Size(110, 21);
            this.labelNewOAUComp4Qty.TabIndex = 4;
            this.labelNewOAUComp4Qty.Text = "Qty:";
            this.labelNewOAUComp4Qty.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxNewOAUCompressor3
            // 
            this.groupBoxNewOAUCompressor3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewOAUCompressor3.Controls.Add(this.txtComp3LRA);
            this.groupBoxNewOAUCompressor3.Controls.Add(this.labelNewOAUComp3LRA);
            this.groupBoxNewOAUCompressor3.Controls.Add(this.txtComp3RLA_Volts);
            this.groupBoxNewOAUCompressor3.Controls.Add(this.labelNewOAUComp3RLAVolts);
            this.groupBoxNewOAUCompressor3.Controls.Add(this.txtComp3Phase);
            this.groupBoxNewOAUCompressor3.Controls.Add(this.labelNewOAUComp3PH);
            this.groupBoxNewOAUCompressor3.Controls.Add(this.txtComp3Qty);
            this.groupBoxNewOAUCompressor3.Controls.Add(this.labelNewOAUComp3Qty);
            this.groupBoxNewOAUCompressor3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewOAUCompressor3.Location = new System.Drawing.Point(321, 300);
            this.groupBoxNewOAUCompressor3.Name = "groupBoxNewOAUCompressor3";
            this.groupBoxNewOAUCompressor3.Size = new System.Drawing.Size(194, 134);
            this.groupBoxNewOAUCompressor3.TabIndex = 7;
            this.groupBoxNewOAUCompressor3.TabStop = false;
            this.groupBoxNewOAUCompressor3.Text = "Compressor 1-3";
            // 
            // txtComp3LRA
            // 
            this.txtComp3LRA.BackColor = System.Drawing.Color.White;
            this.txtComp3LRA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp3LRA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp3LRA.Location = new System.Drawing.Point(126, 101);
            this.txtComp3LRA.Name = "txtComp3LRA";
            this.txtComp3LRA.Size = new System.Drawing.Size(60, 22);
            this.txtComp3LRA.TabIndex = 31;
            this.txtComp3LRA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUComp3LRA
            // 
            this.labelNewOAUComp3LRA.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUComp3LRA.Location = new System.Drawing.Point(13, 101);
            this.labelNewOAUComp3LRA.Name = "labelNewOAUComp3LRA";
            this.labelNewOAUComp3LRA.Size = new System.Drawing.Size(110, 21);
            this.labelNewOAUComp3LRA.TabIndex = 10;
            this.labelNewOAUComp3LRA.Text = "LRA:";
            this.labelNewOAUComp3LRA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtComp3RLA_Volts
            // 
            this.txtComp3RLA_Volts.BackColor = System.Drawing.Color.White;
            this.txtComp3RLA_Volts.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp3RLA_Volts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp3RLA_Volts.Location = new System.Drawing.Point(126, 73);
            this.txtComp3RLA_Volts.Name = "txtComp3RLA_Volts";
            this.txtComp3RLA_Volts.Size = new System.Drawing.Size(60, 22);
            this.txtComp3RLA_Volts.TabIndex = 30;
            this.txtComp3RLA_Volts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtComp3RLA_Volts.Leave += new System.EventHandler(this.textBoxNewOAUComp3RLAVolts_Leave);
            // 
            // labelNewOAUComp3RLAVolts
            // 
            this.labelNewOAUComp3RLAVolts.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUComp3RLAVolts.Location = new System.Drawing.Point(12, 73);
            this.labelNewOAUComp3RLAVolts.Name = "labelNewOAUComp3RLAVolts";
            this.labelNewOAUComp3RLAVolts.Size = new System.Drawing.Size(110, 21);
            this.labelNewOAUComp3RLAVolts.TabIndex = 8;
            this.labelNewOAUComp3RLAVolts.Text = "RLA-Volts:";
            this.labelNewOAUComp3RLAVolts.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtComp3Phase
            // 
            this.txtComp3Phase.BackColor = System.Drawing.Color.White;
            this.txtComp3Phase.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp3Phase.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp3Phase.Location = new System.Drawing.Point(126, 46);
            this.txtComp3Phase.Name = "txtComp3Phase";
            this.txtComp3Phase.Size = new System.Drawing.Size(60, 22);
            this.txtComp3Phase.TabIndex = 29;
            this.txtComp3Phase.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUComp3PH
            // 
            this.labelNewOAUComp3PH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUComp3PH.Location = new System.Drawing.Point(12, 46);
            this.labelNewOAUComp3PH.Name = "labelNewOAUComp3PH";
            this.labelNewOAUComp3PH.Size = new System.Drawing.Size(110, 21);
            this.labelNewOAUComp3PH.TabIndex = 6;
            this.labelNewOAUComp3PH.Text = "Phase:";
            this.labelNewOAUComp3PH.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtComp3Qty
            // 
            this.txtComp3Qty.BackColor = System.Drawing.Color.White;
            this.txtComp3Qty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp3Qty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp3Qty.Location = new System.Drawing.Point(126, 19);
            this.txtComp3Qty.Name = "txtComp3Qty";
            this.txtComp3Qty.Size = new System.Drawing.Size(60, 22);
            this.txtComp3Qty.TabIndex = 28;
            this.txtComp3Qty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUComp3Qty
            // 
            this.labelNewOAUComp3Qty.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUComp3Qty.Location = new System.Drawing.Point(12, 19);
            this.labelNewOAUComp3Qty.Name = "labelNewOAUComp3Qty";
            this.labelNewOAUComp3Qty.Size = new System.Drawing.Size(110, 21);
            this.labelNewOAUComp3Qty.TabIndex = 4;
            this.labelNewOAUComp3Qty.Text = "Qty:";
            this.labelNewOAUComp3Qty.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxNewOAUCompressor2
            // 
            this.groupBoxNewOAUCompressor2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewOAUCompressor2.Controls.Add(this.txtComp2LRA);
            this.groupBoxNewOAUCompressor2.Controls.Add(this.labelNewOAUNewOAUComp2LRA);
            this.groupBoxNewOAUCompressor2.Controls.Add(this.txtComp2RLA_Volts);
            this.groupBoxNewOAUCompressor2.Controls.Add(this.labelNewOAUComp2RLAVolts);
            this.groupBoxNewOAUCompressor2.Controls.Add(this.txtComp2Phase);
            this.groupBoxNewOAUCompressor2.Controls.Add(this.labelNewOAUComp2PH);
            this.groupBoxNewOAUCompressor2.Controls.Add(this.txtComp2Qty);
            this.groupBoxNewOAUCompressor2.Controls.Add(this.labelNewOAUComp2Qty);
            this.groupBoxNewOAUCompressor2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewOAUCompressor2.Location = new System.Drawing.Point(321, 161);
            this.groupBoxNewOAUCompressor2.Name = "groupBoxNewOAUCompressor2";
            this.groupBoxNewOAUCompressor2.Size = new System.Drawing.Size(194, 134);
            this.groupBoxNewOAUCompressor2.TabIndex = 6;
            this.groupBoxNewOAUCompressor2.TabStop = false;
            this.groupBoxNewOAUCompressor2.Text = "Compressor 1-2";
            // 
            // txtComp2LRA
            // 
            this.txtComp2LRA.BackColor = System.Drawing.Color.White;
            this.txtComp2LRA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp2LRA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp2LRA.Location = new System.Drawing.Point(126, 101);
            this.txtComp2LRA.Name = "txtComp2LRA";
            this.txtComp2LRA.Size = new System.Drawing.Size(60, 22);
            this.txtComp2LRA.TabIndex = 26;
            this.txtComp2LRA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUNewOAUComp2LRA
            // 
            this.labelNewOAUNewOAUComp2LRA.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUNewOAUComp2LRA.Location = new System.Drawing.Point(13, 101);
            this.labelNewOAUNewOAUComp2LRA.Name = "labelNewOAUNewOAUComp2LRA";
            this.labelNewOAUNewOAUComp2LRA.Size = new System.Drawing.Size(110, 21);
            this.labelNewOAUNewOAUComp2LRA.TabIndex = 10;
            this.labelNewOAUNewOAUComp2LRA.Text = "LRA:";
            this.labelNewOAUNewOAUComp2LRA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtComp2RLA_Volts
            // 
            this.txtComp2RLA_Volts.BackColor = System.Drawing.Color.White;
            this.txtComp2RLA_Volts.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp2RLA_Volts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp2RLA_Volts.Location = new System.Drawing.Point(126, 73);
            this.txtComp2RLA_Volts.Name = "txtComp2RLA_Volts";
            this.txtComp2RLA_Volts.Size = new System.Drawing.Size(60, 22);
            this.txtComp2RLA_Volts.TabIndex = 25;
            this.txtComp2RLA_Volts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtComp2RLA_Volts.Leave += new System.EventHandler(this.textBoxNewOAUComp2RLAVolts_Leave);
            // 
            // labelNewOAUComp2RLAVolts
            // 
            this.labelNewOAUComp2RLAVolts.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUComp2RLAVolts.Location = new System.Drawing.Point(12, 73);
            this.labelNewOAUComp2RLAVolts.Name = "labelNewOAUComp2RLAVolts";
            this.labelNewOAUComp2RLAVolts.Size = new System.Drawing.Size(110, 21);
            this.labelNewOAUComp2RLAVolts.TabIndex = 8;
            this.labelNewOAUComp2RLAVolts.Text = "RLA-Volts:";
            this.labelNewOAUComp2RLAVolts.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtComp2Phase
            // 
            this.txtComp2Phase.BackColor = System.Drawing.Color.White;
            this.txtComp2Phase.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp2Phase.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp2Phase.Location = new System.Drawing.Point(126, 46);
            this.txtComp2Phase.Name = "txtComp2Phase";
            this.txtComp2Phase.Size = new System.Drawing.Size(60, 22);
            this.txtComp2Phase.TabIndex = 24;
            this.txtComp2Phase.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUComp2PH
            // 
            this.labelNewOAUComp2PH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUComp2PH.Location = new System.Drawing.Point(12, 46);
            this.labelNewOAUComp2PH.Name = "labelNewOAUComp2PH";
            this.labelNewOAUComp2PH.Size = new System.Drawing.Size(110, 21);
            this.labelNewOAUComp2PH.TabIndex = 6;
            this.labelNewOAUComp2PH.Text = "Phase:";
            this.labelNewOAUComp2PH.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtComp2Qty
            // 
            this.txtComp2Qty.BackColor = System.Drawing.Color.White;
            this.txtComp2Qty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp2Qty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp2Qty.Location = new System.Drawing.Point(126, 19);
            this.txtComp2Qty.Name = "txtComp2Qty";
            this.txtComp2Qty.Size = new System.Drawing.Size(60, 22);
            this.txtComp2Qty.TabIndex = 23;
            this.txtComp2Qty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUComp2Qty
            // 
            this.labelNewOAUComp2Qty.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUComp2Qty.Location = new System.Drawing.Point(12, 19);
            this.labelNewOAUComp2Qty.Name = "labelNewOAUComp2Qty";
            this.labelNewOAUComp2Qty.Size = new System.Drawing.Size(110, 21);
            this.labelNewOAUComp2Qty.TabIndex = 4;
            this.labelNewOAUComp2Qty.Text = "Qty:";
            this.labelNewOAUComp2Qty.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxNewOAUCompressor1
            // 
            this.groupBoxNewOAUCompressor1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewOAUCompressor1.Controls.Add(this.txtComp1LRA);
            this.groupBoxNewOAUCompressor1.Controls.Add(this.labelNewOAUComp1LRA);
            this.groupBoxNewOAUCompressor1.Controls.Add(this.txtComp1RLA_Volts);
            this.groupBoxNewOAUCompressor1.Controls.Add(this.labelNewOAUComp1RLAVolts);
            this.groupBoxNewOAUCompressor1.Controls.Add(this.txtComp1Phase);
            this.groupBoxNewOAUCompressor1.Controls.Add(this.labelNewOAUComp1PH);
            this.groupBoxNewOAUCompressor1.Controls.Add(this.txtComp1Qty);
            this.groupBoxNewOAUCompressor1.Controls.Add(this.labelNewOAUComp1Qty);
            this.groupBoxNewOAUCompressor1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewOAUCompressor1.Location = new System.Drawing.Point(321, 22);
            this.groupBoxNewOAUCompressor1.Name = "groupBoxNewOAUCompressor1";
            this.groupBoxNewOAUCompressor1.Size = new System.Drawing.Size(194, 134);
            this.groupBoxNewOAUCompressor1.TabIndex = 5;
            this.groupBoxNewOAUCompressor1.TabStop = false;
            this.groupBoxNewOAUCompressor1.Text = "Compressor 1-1";
            // 
            // txtComp1LRA
            // 
            this.txtComp1LRA.BackColor = System.Drawing.Color.White;
            this.txtComp1LRA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp1LRA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp1LRA.Location = new System.Drawing.Point(126, 101);
            this.txtComp1LRA.Name = "txtComp1LRA";
            this.txtComp1LRA.Size = new System.Drawing.Size(60, 22);
            this.txtComp1LRA.TabIndex = 21;
            this.txtComp1LRA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUComp1LRA
            // 
            this.labelNewOAUComp1LRA.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUComp1LRA.Location = new System.Drawing.Point(13, 101);
            this.labelNewOAUComp1LRA.Name = "labelNewOAUComp1LRA";
            this.labelNewOAUComp1LRA.Size = new System.Drawing.Size(110, 21);
            this.labelNewOAUComp1LRA.TabIndex = 10;
            this.labelNewOAUComp1LRA.Text = "LRA:";
            this.labelNewOAUComp1LRA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtComp1RLA_Volts
            // 
            this.txtComp1RLA_Volts.BackColor = System.Drawing.Color.White;
            this.txtComp1RLA_Volts.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp1RLA_Volts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp1RLA_Volts.Location = new System.Drawing.Point(126, 73);
            this.txtComp1RLA_Volts.Name = "txtComp1RLA_Volts";
            this.txtComp1RLA_Volts.Size = new System.Drawing.Size(60, 22);
            this.txtComp1RLA_Volts.TabIndex = 20;
            this.txtComp1RLA_Volts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtComp1RLA_Volts.Leave += new System.EventHandler(this.textBoxNewOAUComp1RLAVolts_Leave);
            // 
            // labelNewOAUComp1RLAVolts
            // 
            this.labelNewOAUComp1RLAVolts.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUComp1RLAVolts.Location = new System.Drawing.Point(12, 73);
            this.labelNewOAUComp1RLAVolts.Name = "labelNewOAUComp1RLAVolts";
            this.labelNewOAUComp1RLAVolts.Size = new System.Drawing.Size(110, 21);
            this.labelNewOAUComp1RLAVolts.TabIndex = 8;
            this.labelNewOAUComp1RLAVolts.Text = "RLA-Volts:";
            this.labelNewOAUComp1RLAVolts.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtComp1Phase
            // 
            this.txtComp1Phase.BackColor = System.Drawing.Color.White;
            this.txtComp1Phase.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp1Phase.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp1Phase.Location = new System.Drawing.Point(126, 46);
            this.txtComp1Phase.Name = "txtComp1Phase";
            this.txtComp1Phase.Size = new System.Drawing.Size(60, 22);
            this.txtComp1Phase.TabIndex = 19;
            this.txtComp1Phase.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUComp1PH
            // 
            this.labelNewOAUComp1PH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUComp1PH.Location = new System.Drawing.Point(12, 46);
            this.labelNewOAUComp1PH.Name = "labelNewOAUComp1PH";
            this.labelNewOAUComp1PH.Size = new System.Drawing.Size(110, 21);
            this.labelNewOAUComp1PH.TabIndex = 6;
            this.labelNewOAUComp1PH.Text = "Phase:";
            this.labelNewOAUComp1PH.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtComp1Qty
            // 
            this.txtComp1Qty.BackColor = System.Drawing.Color.White;
            this.txtComp1Qty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp1Qty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtComp1Qty.Location = new System.Drawing.Point(126, 19);
            this.txtComp1Qty.Name = "txtComp1Qty";
            this.txtComp1Qty.Size = new System.Drawing.Size(60, 22);
            this.txtComp1Qty.TabIndex = 18;
            this.txtComp1Qty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUComp1Qty
            // 
            this.labelNewOAUComp1Qty.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUComp1Qty.Location = new System.Drawing.Point(12, 19);
            this.labelNewOAUComp1Qty.Name = "labelNewOAUComp1Qty";
            this.labelNewOAUComp1Qty.Size = new System.Drawing.Size(110, 21);
            this.labelNewOAUComp1Qty.TabIndex = 4;
            this.labelNewOAUComp1Qty.Text = "Qty:";
            this.labelNewOAUComp1Qty.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxNewOauLineInfo
            // 
            this.groupBoxNewOauLineInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewOauLineInfo.Controls.Add(this.cbWhseCode);
            this.groupBoxNewOauLineInfo.Controls.Add(this.label28);
            this.groupBoxNewOauLineInfo.Controls.Add(this.dateTimePickerOAUMfgDate);
            this.groupBoxNewOauLineInfo.Controls.Add(this.label3);
            this.groupBoxNewOauLineInfo.Controls.Add(this.checkBoxNewOAUVerified);
            this.groupBoxNewOauLineInfo.Controls.Add(this.textBoxNewOAUVerifiedBy);
            this.groupBoxNewOauLineInfo.Controls.Add(this.labelMewOAUVerifiedBy);
            this.groupBoxNewOauLineInfo.Controls.Add(this.comboBoxNewOAUVoltage);
            this.groupBoxNewOauLineInfo.Controls.Add(this.labelNewOAUVoltage);
            this.groupBoxNewOauLineInfo.Controls.Add(this.comboBoxNewOAUHeatingType);
            this.groupBoxNewOauLineInfo.Controls.Add(this.labelNewOAUHeatingType);
            this.groupBoxNewOauLineInfo.Controls.Add(this.labelNewOAUSerialNo);
            this.groupBoxNewOauLineInfo.Controls.Add(this.textBoxNewOAUSerialNo);
            this.groupBoxNewOauLineInfo.Controls.Add(this.textBoxNewOAUModelNo);
            this.groupBoxNewOauLineInfo.Controls.Add(this.labelNewOAUModelNum);
            this.groupBoxNewOauLineInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewOauLineInfo.Location = new System.Drawing.Point(6, 132);
            this.groupBoxNewOauLineInfo.Name = "groupBoxNewOauLineInfo";
            this.groupBoxNewOauLineInfo.Size = new System.Drawing.Size(311, 209);
            this.groupBoxNewOauLineInfo.TabIndex = 1;
            this.groupBoxNewOauLineInfo.TabStop = false;
            this.groupBoxNewOauLineInfo.Text = "Unit Info";
            // 
            // cbWhseCode
            // 
            this.cbWhseCode.BackColor = System.Drawing.Color.White;
            this.cbWhseCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbWhseCode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.cbWhseCode.FormattingEnabled = true;
            this.cbWhseCode.Items.AddRange(new object[] {
            "LWH2 - Grassland Drive",
            "LWH5 - Technology Drive"});
            this.cbWhseCode.Location = new System.Drawing.Point(95, 174);
            this.cbWhseCode.Name = "cbWhseCode";
            this.cbWhseCode.Size = new System.Drawing.Size(196, 22);
            this.cbWhseCode.TabIndex = 66;
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(7, 175);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(80, 21);
            this.label28.TabIndex = 67;
            this.label28.Text = "Whse Code:";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dateTimePickerOAUMfgDate
            // 
            this.dateTimePickerOAUMfgDate.CalendarForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.dateTimePickerOAUMfgDate.CalendarMonthBackground = System.Drawing.Color.White;
            this.dateTimePickerOAUMfgDate.CalendarTitleBackColor = System.Drawing.Color.LightSteelBlue;
            this.dateTimePickerOAUMfgDate.CalendarTitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.dateTimePickerOAUMfgDate.CalendarTrailingForeColor = System.Drawing.Color.Gray;
            this.dateTimePickerOAUMfgDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerOAUMfgDate.Location = new System.Drawing.Point(95, 89);
            this.dateTimePickerOAUMfgDate.Name = "dateTimePickerOAUMfgDate";
            this.dateTimePickerOAUMfgDate.Size = new System.Drawing.Size(107, 22);
            this.dateTimePickerOAUMfgDate.TabIndex = 65;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 21);
            this.label3.TabIndex = 64;
            this.label3.Text = "Mfg Date:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // checkBoxNewOAUVerified
            // 
            this.checkBoxNewOAUVerified.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBoxNewOAUVerified.Location = new System.Drawing.Point(206, 60);
            this.checkBoxNewOAUVerified.Name = "checkBoxNewOAUVerified";
            this.checkBoxNewOAUVerified.Size = new System.Drawing.Size(95, 32);
            this.checkBoxNewOAUVerified.TabIndex = 62;
            this.checkBoxNewOAUVerified.Text = "Model No \r\nVerified";
            this.checkBoxNewOAUVerified.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxNewOAUVerified.UseVisualStyleBackColor = true;
            this.checkBoxNewOAUVerified.Visible = false;
            this.checkBoxNewOAUVerified.CheckedChanged += new System.EventHandler(this.checkBoxNewOAUVerified_CheckedChanged);
            // 
            // textBoxNewOAUVerifiedBy
            // 
            this.textBoxNewOAUVerifiedBy.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUVerifiedBy.Enabled = false;
            this.textBoxNewOAUVerifiedBy.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUVerifiedBy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUVerifiedBy.Location = new System.Drawing.Point(223, 117);
            this.textBoxNewOAUVerifiedBy.Name = "textBoxNewOAUVerifiedBy";
            this.textBoxNewOAUVerifiedBy.Size = new System.Drawing.Size(67, 22);
            this.textBoxNewOAUVerifiedBy.TabIndex = 63;
            this.textBoxNewOAUVerifiedBy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxNewOAUVerifiedBy.Visible = false;
            // 
            // labelMewOAUVerifiedBy
            // 
            this.labelMewOAUVerifiedBy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMewOAUVerifiedBy.Location = new System.Drawing.Point(221, 92);
            this.labelMewOAUVerifiedBy.Name = "labelMewOAUVerifiedBy";
            this.labelMewOAUVerifiedBy.Size = new System.Drawing.Size(70, 20);
            this.labelMewOAUVerifiedBy.TabIndex = 12;
            this.labelMewOAUVerifiedBy.Text = "Verified By:";
            this.labelMewOAUVerifiedBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelMewOAUVerifiedBy.Visible = false;
            // 
            // comboBoxNewOAUVoltage
            // 
            this.comboBoxNewOAUVoltage.BackColor = System.Drawing.Color.White;
            this.comboBoxNewOAUVoltage.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxNewOAUVoltage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.comboBoxNewOAUVoltage.FormattingEnabled = true;
            this.comboBoxNewOAUVoltage.Items.AddRange(new object[] {
            "208",
            "230",
            "460",
            "575"});
            this.comboBoxNewOAUVoltage.Location = new System.Drawing.Point(95, 117);
            this.comboBoxNewOAUVoltage.Name = "comboBoxNewOAUVoltage";
            this.comboBoxNewOAUVoltage.Size = new System.Drawing.Size(107, 22);
            this.comboBoxNewOAUVoltage.TabIndex = 8;
            this.comboBoxNewOAUVoltage.SelectedIndexChanged += new System.EventHandler(this.comboBoxNewOAUVoltage_SelectedIndexChanged);
            // 
            // labelNewOAUVoltage
            // 
            this.labelNewOAUVoltage.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUVoltage.Location = new System.Drawing.Point(7, 117);
            this.labelNewOAUVoltage.Name = "labelNewOAUVoltage";
            this.labelNewOAUVoltage.Size = new System.Drawing.Size(80, 21);
            this.labelNewOAUVoltage.TabIndex = 10;
            this.labelNewOAUVoltage.Text = "Voltage:";
            this.labelNewOAUVoltage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // comboBoxNewOAUHeatingType
            // 
            this.comboBoxNewOAUHeatingType.BackColor = System.Drawing.Color.White;
            this.comboBoxNewOAUHeatingType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxNewOAUHeatingType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.comboBoxNewOAUHeatingType.FormattingEnabled = true;
            this.comboBoxNewOAUHeatingType.Items.AddRange(new object[] {
            "Indirect-Fired",
            "Electric",
            "No Heat",
            "Direct-Fired",
            "HotWater",
            "Steam",
            "Dual-Fuel(Indirect/Elec)",
            "Dual-Fuel(Elec/Elec)",
            "Dual-Fuel(HotWater/Direct-Fired)",
            "Dual-Fuel(HotWater/Elec)",
            "No Primary Heat/Elec-Staged",
            "Dual-Fuel(Indirect/Direct-Fired)",
            "Dual-Fuel(Elec/Direct-Fired)",
            "Dual-Fuel(Steam/Direct-Fired)",
            "Dual-Fuel(Steam/Elec)"});
            this.comboBoxNewOAUHeatingType.Location = new System.Drawing.Point(95, 145);
            this.comboBoxNewOAUHeatingType.Name = "comboBoxNewOAUHeatingType";
            this.comboBoxNewOAUHeatingType.Size = new System.Drawing.Size(196, 22);
            this.comboBoxNewOAUHeatingType.TabIndex = 7;
            this.comboBoxNewOAUHeatingType.SelectedIndexChanged += new System.EventHandler(this.comboBoxNewOAUHeatingType_SelectedIndexChanged);
            // 
            // labelNewOAUHeatingType
            // 
            this.labelNewOAUHeatingType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUHeatingType.Location = new System.Drawing.Point(7, 146);
            this.labelNewOAUHeatingType.Name = "labelNewOAUHeatingType";
            this.labelNewOAUHeatingType.Size = new System.Drawing.Size(80, 21);
            this.labelNewOAUHeatingType.TabIndex = 8;
            this.labelNewOAUHeatingType.Text = "Htg Type:";
            this.labelNewOAUHeatingType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelNewOAUSerialNo
            // 
            this.labelNewOAUSerialNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUSerialNo.Location = new System.Drawing.Point(7, 62);
            this.labelNewOAUSerialNo.Name = "labelNewOAUSerialNo";
            this.labelNewOAUSerialNo.Size = new System.Drawing.Size(80, 21);
            this.labelNewOAUSerialNo.TabIndex = 7;
            this.labelNewOAUSerialNo.Text = "Serial No:";
            this.labelNewOAUSerialNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUSerialNo
            // 
            this.textBoxNewOAUSerialNo.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUSerialNo.Enabled = false;
            this.textBoxNewOAUSerialNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUSerialNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUSerialNo.Location = new System.Drawing.Point(95, 62);
            this.textBoxNewOAUSerialNo.Name = "textBoxNewOAUSerialNo";
            this.textBoxNewOAUSerialNo.Size = new System.Drawing.Size(108, 22);
            this.textBoxNewOAUSerialNo.TabIndex = 6;
            this.textBoxNewOAUSerialNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxNewOAUModelNo
            // 
            this.textBoxNewOAUModelNo.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUModelNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUModelNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUModelNo.Location = new System.Drawing.Point(15, 35);
            this.textBoxNewOAUModelNo.Name = "textBoxNewOAUModelNo";
            this.textBoxNewOAUModelNo.Size = new System.Drawing.Size(283, 22);
            this.textBoxNewOAUModelNo.TabIndex = 5;
            this.textBoxNewOAUModelNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxNewOAUModelNo.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.textBoxNewOAUModelNo_MouseDoubleClick);
            // 
            // labelNewOAUModelNum
            // 
            this.labelNewOAUModelNum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUModelNum.Location = new System.Drawing.Point(7, 14);
            this.labelNewOAUModelNum.Name = "labelNewOAUModelNum";
            this.labelNewOAUModelNum.Size = new System.Drawing.Size(65, 21);
            this.labelNewOAUModelNum.TabIndex = 4;
            this.labelNewOAUModelNum.Text = "Model No:";
            this.labelNewOAUModelNum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxNewOAUJobInfo
            // 
            this.groupBoxNewOAUJobInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewOAUJobInfo.Controls.Add(this.label4);
            this.groupBoxNewOAUJobInfo.Controls.Add(this.textBoxNewOAUJobNum);
            this.groupBoxNewOAUJobInfo.Controls.Add(this.textBoxNewOAUEndConsumer);
            this.groupBoxNewOAUJobInfo.Controls.Add(this.labelNewOAUEndConsumer);
            this.groupBoxNewOAUJobInfo.Controls.Add(this.textBoxNewOAUReleaseNum);
            this.groupBoxNewOAUJobInfo.Controls.Add(this.labelNewOAUSlash);
            this.groupBoxNewOAUJobInfo.Controls.Add(this.textBoxNewOAUCustomer);
            this.groupBoxNewOAUJobInfo.Controls.Add(this.textBoxNewOAUOrderNum);
            this.groupBoxNewOAUJobInfo.Controls.Add(this.labelNewOAUCustomer);
            this.groupBoxNewOAUJobInfo.Controls.Add(this.labelNewOAUOrderNum);
            this.groupBoxNewOAUJobInfo.Controls.Add(this.textBoxNewOAULineItem);
            this.groupBoxNewOAUJobInfo.Controls.Add(this.labelNewOAULineItem);
            this.groupBoxNewOAUJobInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewOAUJobInfo.Location = new System.Drawing.Point(6, 22);
            this.groupBoxNewOAUJobInfo.Name = "groupBoxNewOAUJobInfo";
            this.groupBoxNewOAUJobInfo.Size = new System.Drawing.Size(311, 105);
            this.groupBoxNewOAUJobInfo.TabIndex = 0;
            this.groupBoxNewOAUJobInfo.TabStop = false;
            this.groupBoxNewOAUJobInfo.Text = "Order Info";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(166, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 21);
            this.label4.TabIndex = 66;
            this.label4.Text = "Job #:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxNewOAUJobNum
            // 
            this.textBoxNewOAUJobNum.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUJobNum.Enabled = false;
            this.textBoxNewOAUJobNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUJobNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUJobNum.Location = new System.Drawing.Point(208, 73);
            this.textBoxNewOAUJobNum.Name = "textBoxNewOAUJobNum";
            this.textBoxNewOAUJobNum.Size = new System.Drawing.Size(97, 22);
            this.textBoxNewOAUJobNum.TabIndex = 65;
            this.textBoxNewOAUJobNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxNewOAUEndConsumer
            // 
            this.textBoxNewOAUEndConsumer.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUEndConsumer.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUEndConsumer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUEndConsumer.Location = new System.Drawing.Point(96, 74);
            this.textBoxNewOAUEndConsumer.Name = "textBoxNewOAUEndConsumer";
            this.textBoxNewOAUEndConsumer.Size = new System.Drawing.Size(72, 22);
            this.textBoxNewOAUEndConsumer.TabIndex = 6;
            // 
            // labelNewOAUEndConsumer
            // 
            this.labelNewOAUEndConsumer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUEndConsumer.Location = new System.Drawing.Point(4, 73);
            this.labelNewOAUEndConsumer.Name = "labelNewOAUEndConsumer";
            this.labelNewOAUEndConsumer.Size = new System.Drawing.Size(93, 21);
            this.labelNewOAUEndConsumer.TabIndex = 5;
            this.labelNewOAUEndConsumer.Text = "End Consumer:";
            this.labelNewOAUEndConsumer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUReleaseNum
            // 
            this.textBoxNewOAUReleaseNum.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUReleaseNum.Enabled = false;
            this.textBoxNewOAUReleaseNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUReleaseNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUReleaseNum.Location = new System.Drawing.Point(284, 19);
            this.textBoxNewOAUReleaseNum.Name = "textBoxNewOAUReleaseNum";
            this.textBoxNewOAUReleaseNum.Size = new System.Drawing.Size(21, 22);
            this.textBoxNewOAUReleaseNum.TabIndex = 4;
            this.textBoxNewOAUReleaseNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUSlash
            // 
            this.labelNewOAUSlash.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUSlash.Location = new System.Drawing.Point(271, 18);
            this.labelNewOAUSlash.Name = "labelNewOAUSlash";
            this.labelNewOAUSlash.Size = new System.Drawing.Size(15, 21);
            this.labelNewOAUSlash.TabIndex = 4;
            this.labelNewOAUSlash.Text = "/";
            this.labelNewOAUSlash.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxNewOAUCustomer
            // 
            this.textBoxNewOAUCustomer.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUCustomer.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUCustomer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUCustomer.Location = new System.Drawing.Point(110, 46);
            this.textBoxNewOAUCustomer.Name = "textBoxNewOAUCustomer";
            this.textBoxNewOAUCustomer.Size = new System.Drawing.Size(195, 22);
            this.textBoxNewOAUCustomer.TabIndex = 2;
            // 
            // textBoxNewOAUOrderNum
            // 
            this.textBoxNewOAUOrderNum.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUOrderNum.Enabled = false;
            this.textBoxNewOAUOrderNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUOrderNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUOrderNum.Location = new System.Drawing.Point(95, 19);
            this.textBoxNewOAUOrderNum.Name = "textBoxNewOAUOrderNum";
            this.textBoxNewOAUOrderNum.Size = new System.Drawing.Size(65, 22);
            this.textBoxNewOAUOrderNum.TabIndex = 1;
            this.textBoxNewOAUOrderNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelNewOAUCustomer
            // 
            this.labelNewOAUCustomer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUCustomer.Location = new System.Drawing.Point(4, 46);
            this.labelNewOAUCustomer.Name = "labelNewOAUCustomer";
            this.labelNewOAUCustomer.Size = new System.Drawing.Size(104, 21);
            this.labelNewOAUCustomer.TabIndex = 2;
            this.labelNewOAUCustomer.Text = "Customer:";
            this.labelNewOAUCustomer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelNewOAUOrderNum
            // 
            this.labelNewOAUOrderNum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUOrderNum.Location = new System.Drawing.Point(4, 20);
            this.labelNewOAUOrderNum.Name = "labelNewOAUOrderNum";
            this.labelNewOAUOrderNum.Size = new System.Drawing.Size(85, 21);
            this.labelNewOAUOrderNum.TabIndex = 1;
            this.labelNewOAUOrderNum.Text = "Sales Order#:";
            this.labelNewOAUOrderNum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxNewOAULineItem
            // 
            this.textBoxNewOAULineItem.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAULineItem.Enabled = false;
            this.textBoxNewOAULineItem.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAULineItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAULineItem.Location = new System.Drawing.Point(249, 19);
            this.textBoxNewOAULineItem.Name = "textBoxNewOAULineItem";
            this.textBoxNewOAULineItem.Size = new System.Drawing.Size(21, 22);
            this.textBoxNewOAULineItem.TabIndex = 3;
            this.textBoxNewOAULineItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAULineItem
            // 
            this.labelNewOAULineItem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAULineItem.Location = new System.Drawing.Point(166, 19);
            this.labelNewOAULineItem.Name = "labelNewOAULineItem";
            this.labelNewOAULineItem.Size = new System.Drawing.Size(81, 21);
            this.labelNewOAULineItem.TabIndex = 2;
            this.labelNewOAULineItem.Text = "Line #/Rel #:";
            this.labelNewOAULineItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxNewOAUElectricInfo
            // 
            this.groupBoxNewOAUElectricInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewOAUElectricInfo.Controls.Add(this.txtSCCR);
            this.groupBoxNewOAUElectricInfo.Controls.Add(this.lblSCCR);
            this.groupBoxNewOAUElectricInfo.Controls.Add(this.textBoxNewOAUMFSMCB2);
            this.groupBoxNewOAUElectricInfo.Controls.Add(this.textBoxNewOAUMinCKTAmp2);
            this.groupBoxNewOAUElectricInfo.Controls.Add(this.labelNewOAUDPP2);
            this.groupBoxNewOAUElectricInfo.Controls.Add(this.labelNewOAUDPP1);
            this.groupBoxNewOAUElectricInfo.Controls.Add(this.checkBoxNewOAUDPP);
            this.groupBoxNewOAUElectricInfo.Controls.Add(this.textBoxNewOAUTestPressureLow);
            this.groupBoxNewOAUElectricInfo.Controls.Add(this.labelNewOAUTestPressureLow);
            this.groupBoxNewOAUElectricInfo.Controls.Add(this.textBoxNewOAUTestPressureHigh);
            this.groupBoxNewOAUElectricInfo.Controls.Add(this.labelNewOAUTestPressureHigh);
            this.groupBoxNewOAUElectricInfo.Controls.Add(this.textBoxNewOAUOperatingVolts);
            this.groupBoxNewOAUElectricInfo.Controls.Add(this.labelNewOAUOperatingVolts);
            this.groupBoxNewOAUElectricInfo.Controls.Add(this.textBoxNewOAUElectricalRating);
            this.groupBoxNewOAUElectricInfo.Controls.Add(this.labelNewOAUElectricalRating);
            this.groupBoxNewOAUElectricInfo.Controls.Add(this.textBoxNewOAUMOP);
            this.groupBoxNewOAUElectricInfo.Controls.Add(this.labelNewOAUMOP);
            this.groupBoxNewOAUElectricInfo.Controls.Add(this.textBoxNewOAUMFSMCB);
            this.groupBoxNewOAUElectricInfo.Controls.Add(this.labelNewOAUMFSMCB);
            this.groupBoxNewOAUElectricInfo.Controls.Add(this.textBoxNewOAUMinCKTAmp);
            this.groupBoxNewOAUElectricInfo.Controls.Add(this.labelNewOAUMinCKTAmp);
            this.groupBoxNewOAUElectricInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewOAUElectricInfo.Location = new System.Drawing.Point(717, 353);
            this.groupBoxNewOAUElectricInfo.Name = "groupBoxNewOAUElectricInfo";
            this.groupBoxNewOAUElectricInfo.Size = new System.Drawing.Size(239, 274);
            this.groupBoxNewOAUElectricInfo.TabIndex = 10;
            this.groupBoxNewOAUElectricInfo.TabStop = false;
            this.groupBoxNewOAUElectricInfo.Text = "Electric Info";
            // 
            // txtSCCR
            // 
            this.txtSCCR.BackColor = System.Drawing.Color.White;
            this.txtSCCR.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSCCR.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtSCCR.Location = new System.Drawing.Point(134, 130);
            this.txtSCCR.Name = "txtSCCR";
            this.txtSCCR.Size = new System.Drawing.Size(99, 22);
            this.txtSCCR.TabIndex = 66;
            this.txtSCCR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblSCCR
            // 
            this.lblSCCR.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSCCR.Location = new System.Drawing.Point(12, 130);
            this.lblSCCR.Name = "lblSCCR";
            this.lblSCCR.Size = new System.Drawing.Size(116, 21);
            this.lblSCCR.TabIndex = 64;
            this.lblSCCR.Text = "SCCR:";
            this.lblSCCR.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUMFSMCB2
            // 
            this.textBoxNewOAUMFSMCB2.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUMFSMCB2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUMFSMCB2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUMFSMCB2.Location = new System.Drawing.Point(187, 75);
            this.textBoxNewOAUMFSMCB2.Name = "textBoxNewOAUMFSMCB2";
            this.textBoxNewOAUMFSMCB2.Size = new System.Drawing.Size(46, 22);
            this.textBoxNewOAUMFSMCB2.TabIndex = 63;
            this.textBoxNewOAUMFSMCB2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxNewOAUMinCKTAmp2
            // 
            this.textBoxNewOAUMinCKTAmp2.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUMinCKTAmp2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUMinCKTAmp2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUMinCKTAmp2.Location = new System.Drawing.Point(187, 47);
            this.textBoxNewOAUMinCKTAmp2.Name = "textBoxNewOAUMinCKTAmp2";
            this.textBoxNewOAUMinCKTAmp2.Size = new System.Drawing.Size(46, 22);
            this.textBoxNewOAUMinCKTAmp2.TabIndex = 62;
            this.textBoxNewOAUMinCKTAmp2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUDPP2
            // 
            this.labelNewOAUDPP2.AutoSize = true;
            this.labelNewOAUDPP2.Location = new System.Drawing.Point(203, 28);
            this.labelNewOAUDPP2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelNewOAUDPP2.Name = "labelNewOAUDPP2";
            this.labelNewOAUDPP2.Size = new System.Drawing.Size(15, 14);
            this.labelNewOAUDPP2.TabIndex = 61;
            this.labelNewOAUDPP2.Text = "2";
            // 
            // labelNewOAUDPP1
            // 
            this.labelNewOAUDPP1.AutoSize = true;
            this.labelNewOAUDPP1.Location = new System.Drawing.Point(151, 28);
            this.labelNewOAUDPP1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelNewOAUDPP1.Name = "labelNewOAUDPP1";
            this.labelNewOAUDPP1.Size = new System.Drawing.Size(15, 14);
            this.labelNewOAUDPP1.TabIndex = 60;
            this.labelNewOAUDPP1.Text = "1";
            // 
            // checkBoxNewOAUDPP
            // 
            this.checkBoxNewOAUDPP.AutoSize = true;
            this.checkBoxNewOAUDPP.Location = new System.Drawing.Point(106, 13);
            this.checkBoxNewOAUDPP.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxNewOAUDPP.Name = "checkBoxNewOAUDPP";
            this.checkBoxNewOAUDPP.Size = new System.Drawing.Size(133, 18);
            this.checkBoxNewOAUDPP.TabIndex = 59;
            this.checkBoxNewOAUDPP.Text = "Dual Point Power";
            this.checkBoxNewOAUDPP.UseVisualStyleBackColor = true;
            this.checkBoxNewOAUDPP.CheckedChanged += new System.EventHandler(this.checkBoxNewOAUDPP_CheckedChanged);
            // 
            // textBoxNewOAUTestPressureLow
            // 
            this.textBoxNewOAUTestPressureLow.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUTestPressureLow.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUTestPressureLow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUTestPressureLow.Location = new System.Drawing.Point(134, 240);
            this.textBoxNewOAUTestPressureLow.Name = "textBoxNewOAUTestPressureLow";
            this.textBoxNewOAUTestPressureLow.Size = new System.Drawing.Size(99, 22);
            this.textBoxNewOAUTestPressureLow.TabIndex = 58;
            this.textBoxNewOAUTestPressureLow.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUTestPressureLow
            // 
            this.labelNewOAUTestPressureLow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUTestPressureLow.Location = new System.Drawing.Point(12, 240);
            this.labelNewOAUTestPressureLow.Name = "labelNewOAUTestPressureLow";
            this.labelNewOAUTestPressureLow.Size = new System.Drawing.Size(116, 21);
            this.labelNewOAUTestPressureLow.TabIndex = 18;
            this.labelNewOAUTestPressureLow.Text = "Test Pressure Low:";
            this.labelNewOAUTestPressureLow.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUTestPressureHigh
            // 
            this.textBoxNewOAUTestPressureHigh.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUTestPressureHigh.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUTestPressureHigh.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUTestPressureHigh.Location = new System.Drawing.Point(134, 213);
            this.textBoxNewOAUTestPressureHigh.Name = "textBoxNewOAUTestPressureHigh";
            this.textBoxNewOAUTestPressureHigh.Size = new System.Drawing.Size(99, 22);
            this.textBoxNewOAUTestPressureHigh.TabIndex = 57;
            this.textBoxNewOAUTestPressureHigh.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUTestPressureHigh
            // 
            this.labelNewOAUTestPressureHigh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUTestPressureHigh.Location = new System.Drawing.Point(12, 213);
            this.labelNewOAUTestPressureHigh.Name = "labelNewOAUTestPressureHigh";
            this.labelNewOAUTestPressureHigh.Size = new System.Drawing.Size(116, 21);
            this.labelNewOAUTestPressureHigh.TabIndex = 16;
            this.labelNewOAUTestPressureHigh.Text = "Test Pressure High:";
            this.labelNewOAUTestPressureHigh.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUOperatingVolts
            // 
            this.textBoxNewOAUOperatingVolts.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUOperatingVolts.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUOperatingVolts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUOperatingVolts.Location = new System.Drawing.Point(134, 185);
            this.textBoxNewOAUOperatingVolts.Name = "textBoxNewOAUOperatingVolts";
            this.textBoxNewOAUOperatingVolts.Size = new System.Drawing.Size(99, 22);
            this.textBoxNewOAUOperatingVolts.TabIndex = 56;
            this.textBoxNewOAUOperatingVolts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUOperatingVolts
            // 
            this.labelNewOAUOperatingVolts.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUOperatingVolts.Location = new System.Drawing.Point(12, 185);
            this.labelNewOAUOperatingVolts.Name = "labelNewOAUOperatingVolts";
            this.labelNewOAUOperatingVolts.Size = new System.Drawing.Size(116, 21);
            this.labelNewOAUOperatingVolts.TabIndex = 14;
            this.labelNewOAUOperatingVolts.Text = "Operating Volts:";
            this.labelNewOAUOperatingVolts.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUElectricalRating
            // 
            this.textBoxNewOAUElectricalRating.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUElectricalRating.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUElectricalRating.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUElectricalRating.Location = new System.Drawing.Point(134, 158);
            this.textBoxNewOAUElectricalRating.Name = "textBoxNewOAUElectricalRating";
            this.textBoxNewOAUElectricalRating.Size = new System.Drawing.Size(99, 22);
            this.textBoxNewOAUElectricalRating.TabIndex = 55;
            this.textBoxNewOAUElectricalRating.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUElectricalRating
            // 
            this.labelNewOAUElectricalRating.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUElectricalRating.Location = new System.Drawing.Point(12, 158);
            this.labelNewOAUElectricalRating.Name = "labelNewOAUElectricalRating";
            this.labelNewOAUElectricalRating.Size = new System.Drawing.Size(116, 21);
            this.labelNewOAUElectricalRating.TabIndex = 12;
            this.labelNewOAUElectricalRating.Text = "Electrical Rating:";
            this.labelNewOAUElectricalRating.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUMOP
            // 
            this.textBoxNewOAUMOP.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUMOP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUMOP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUMOP.Location = new System.Drawing.Point(134, 103);
            this.textBoxNewOAUMOP.Name = "textBoxNewOAUMOP";
            this.textBoxNewOAUMOP.Size = new System.Drawing.Size(99, 22);
            this.textBoxNewOAUMOP.TabIndex = 54;
            this.textBoxNewOAUMOP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUMOP
            // 
            this.labelNewOAUMOP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUMOP.Location = new System.Drawing.Point(12, 103);
            this.labelNewOAUMOP.Name = "labelNewOAUMOP";
            this.labelNewOAUMOP.Size = new System.Drawing.Size(116, 21);
            this.labelNewOAUMOP.TabIndex = 10;
            this.labelNewOAUMOP.Text = "MOP:";
            this.labelNewOAUMOP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUMFSMCB
            // 
            this.textBoxNewOAUMFSMCB.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUMFSMCB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUMFSMCB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUMFSMCB.Location = new System.Drawing.Point(134, 75);
            this.textBoxNewOAUMFSMCB.Name = "textBoxNewOAUMFSMCB";
            this.textBoxNewOAUMFSMCB.Size = new System.Drawing.Size(46, 22);
            this.textBoxNewOAUMFSMCB.TabIndex = 53;
            this.textBoxNewOAUMFSMCB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUMFSMCB
            // 
            this.labelNewOAUMFSMCB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUMFSMCB.Location = new System.Drawing.Point(13, 75);
            this.labelNewOAUMFSMCB.Name = "labelNewOAUMFSMCB";
            this.labelNewOAUMFSMCB.Size = new System.Drawing.Size(116, 21);
            this.labelNewOAUMFSMCB.TabIndex = 8;
            this.labelNewOAUMFSMCB.Text = "MFS MCB:";
            this.labelNewOAUMFSMCB.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUMinCKTAmp
            // 
            this.textBoxNewOAUMinCKTAmp.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUMinCKTAmp.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUMinCKTAmp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUMinCKTAmp.Location = new System.Drawing.Point(134, 47);
            this.textBoxNewOAUMinCKTAmp.Name = "textBoxNewOAUMinCKTAmp";
            this.textBoxNewOAUMinCKTAmp.Size = new System.Drawing.Size(46, 22);
            this.textBoxNewOAUMinCKTAmp.TabIndex = 52;
            this.textBoxNewOAUMinCKTAmp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUMinCKTAmp
            // 
            this.labelNewOAUMinCKTAmp.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUMinCKTAmp.Location = new System.Drawing.Point(12, 47);
            this.labelNewOAUMinCKTAmp.Name = "labelNewOAUMinCKTAmp";
            this.labelNewOAUMinCKTAmp.Size = new System.Drawing.Size(116, 21);
            this.labelNewOAUMinCKTAmp.TabIndex = 6;
            this.labelNewOAUMinCKTAmp.Text = "Min CKT Amp:";
            this.labelNewOAUMinCKTAmp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxNewOAUElectric
            // 
            this.groupBoxNewOAUElectric.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewOAUElectric.Controls.Add(this.textBoxNewOAUElecMaxAirOutletTemp);
            this.groupBoxNewOAUElectric.Controls.Add(this.lblNewOAUElecMaxAirOutletTemp);
            this.groupBoxNewOAUElectric.Controls.Add(this.txtNewOAUElecSecHtgInput);
            this.groupBoxNewOAUElectric.Controls.Add(this.lblNewOAUElecSecHtgInput);
            this.groupBoxNewOAUElectric.Controls.Add(this.textBoxNewOAUElecHeatingInputElec);
            this.groupBoxNewOAUElectric.Controls.Add(this.labelNewOAUElecHeatingInput);
            this.groupBoxNewOAUElectric.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewOAUElectric.Location = new System.Drawing.Point(6, 345);
            this.groupBoxNewOAUElectric.Name = "groupBoxNewOAUElectric";
            this.groupBoxNewOAUElectric.Size = new System.Drawing.Size(311, 122);
            this.groupBoxNewOAUElectric.TabIndex = 4;
            this.groupBoxNewOAUElectric.TabStop = false;
            this.groupBoxNewOAUElectric.Text = "Electric";
            this.groupBoxNewOAUElectric.Visible = false;
            // 
            // txtNewOAUElecSecHtgInput
            // 
            this.txtNewOAUElecSecHtgInput.BackColor = System.Drawing.Color.White;
            this.txtNewOAUElecSecHtgInput.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewOAUElecSecHtgInput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtNewOAUElecSecHtgInput.Location = new System.Drawing.Point(187, 81);
            this.txtNewOAUElecSecHtgInput.Name = "txtNewOAUElecSecHtgInput";
            this.txtNewOAUElecSecHtgInput.Size = new System.Drawing.Size(118, 22);
            this.txtNewOAUElecSecHtgInput.TabIndex = 59;
            this.txtNewOAUElecSecHtgInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNewOAUElecSecHtgInput.Visible = false;
            // 
            // lblNewOAUElecSecHtgInput
            // 
            this.lblNewOAUElecSecHtgInput.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNewOAUElecSecHtgInput.Location = new System.Drawing.Point(9, 81);
            this.lblNewOAUElecSecHtgInput.Name = "lblNewOAUElecSecHtgInput";
            this.lblNewOAUElecSecHtgInput.Size = new System.Drawing.Size(175, 21);
            this.lblNewOAUElecSecHtgInput.TabIndex = 60;
            this.lblNewOAUElecSecHtgInput.Text = "Secondary Heating Input:";
            this.lblNewOAUElecSecHtgInput.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblNewOAUElecSecHtgInput.Visible = false;
            // 
            // textBoxNewOAUElecHeatingInputElec
            // 
            this.textBoxNewOAUElecHeatingInputElec.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUElecHeatingInputElec.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUElecHeatingInputElec.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUElecHeatingInputElec.Location = new System.Drawing.Point(187, 27);
            this.textBoxNewOAUElecHeatingInputElec.Name = "textBoxNewOAUElecHeatingInputElec";
            this.textBoxNewOAUElecHeatingInputElec.Size = new System.Drawing.Size(118, 22);
            this.textBoxNewOAUElecHeatingInputElec.TabIndex = 9;
            this.textBoxNewOAUElecHeatingInputElec.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUElecHeatingInput
            // 
            this.labelNewOAUElecHeatingInput.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUElecHeatingInput.Location = new System.Drawing.Point(9, 27);
            this.labelNewOAUElecHeatingInput.Name = "labelNewOAUElecHeatingInput";
            this.labelNewOAUElecHeatingInput.Size = new System.Drawing.Size(175, 21);
            this.labelNewOAUElecHeatingInput.TabIndex = 8;
            this.labelNewOAUElecHeatingInput.Text = "Heating Input Electric:";
            this.labelNewOAUElecHeatingInput.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxNewOAUIndirectFire
            // 
            this.groupBoxNewOAUIndirectFire.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewOAUIndirectFire.Controls.Add(this.labelNewOAUIndFireManifoldPres);
            this.groupBoxNewOAUIndirectFire.Controls.Add(this.textBoxNewOAUIndFireSecHtgInput);
            this.groupBoxNewOAUIndirectFire.Controls.Add(this.lblNewOAUIndFireHtgInput);
            this.groupBoxNewOAUIndirectFire.Controls.Add(this.comboBoxNewOAUIFFuelType);
            this.groupBoxNewOAUIndirectFire.Controls.Add(this.labelNewOAUIFFuelType);
            this.groupBoxNewOAUIndirectFire.Controls.Add(this.textBoxNewOAUIndFireTempRise);
            this.groupBoxNewOAUIndirectFire.Controls.Add(this.labelNewOAUIndFireTempRise);
            this.groupBoxNewOAUIndirectFire.Controls.Add(this.textBoxNewOAUIndFireManifoldPressure);
            this.groupBoxNewOAUIndirectFire.Controls.Add(this.textBoxNewOAUIndFireMaxGasPressure);
            this.groupBoxNewOAUIndirectFire.Controls.Add(this.labelNewOAUIndFireMaxGasPressure);
            this.groupBoxNewOAUIndirectFire.Controls.Add(this.textBoxNewOAUIndFireMinGasPressure);
            this.groupBoxNewOAUIndirectFire.Controls.Add(this.labelNewOAUIndFireMinGasPressure);
            this.groupBoxNewOAUIndirectFire.Controls.Add(this.textBoxNewOAUIndFireMaxOutAirTemp);
            this.groupBoxNewOAUIndirectFire.Controls.Add(this.labelNewOAUIndFireMaxOutAirTemp);
            this.groupBoxNewOAUIndirectFire.Controls.Add(this.textBoxNewOauIndFireMaxExt);
            this.groupBoxNewOAUIndirectFire.Controls.Add(this.textBoxNewOauIndFireMinInputBTU);
            this.groupBoxNewOAUIndirectFire.Controls.Add(this.labelNewOauIndFireMinHtgInput);
            this.groupBoxNewOAUIndirectFire.Controls.Add(this.textBoxNewOAUIndFireHeatingOutput);
            this.groupBoxNewOAUIndirectFire.Controls.Add(this.labelNewOauIndFireHtgOutput);
            this.groupBoxNewOAUIndirectFire.Controls.Add(this.textBoxNewOAUIndFireMaxHtgInput);
            this.groupBoxNewOAUIndirectFire.Controls.Add(this.labelNewOauIndFireMaxHtg);
            this.groupBoxNewOAUIndirectFire.Controls.Add(this.labelNewOauIndFireMaxEst);
            this.groupBoxNewOAUIndirectFire.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewOAUIndirectFire.Location = new System.Drawing.Point(6, 346);
            this.groupBoxNewOAUIndirectFire.Name = "groupBoxNewOAUIndirectFire";
            this.groupBoxNewOAUIndirectFire.Size = new System.Drawing.Size(311, 319);
            this.groupBoxNewOAUIndirectFire.TabIndex = 3;
            this.groupBoxNewOAUIndirectFire.TabStop = false;
            this.groupBoxNewOAUIndirectFire.Text = "Indirect Fire";
            this.groupBoxNewOAUIndirectFire.Visible = false;
            // 
            // labelNewOAUIndFireManifoldPres
            // 
            this.labelNewOAUIndFireManifoldPres.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUIndFireManifoldPres.Location = new System.Drawing.Point(9, 235);
            this.labelNewOAUIndFireManifoldPres.Name = "labelNewOAUIndFireManifoldPres";
            this.labelNewOAUIndFireManifoldPres.Size = new System.Drawing.Size(175, 21);
            this.labelNewOAUIndFireManifoldPres.TabIndex = 52;
            this.labelNewOAUIndFireManifoldPres.Text = "Manifold Pressure:";
            this.labelNewOAUIndFireManifoldPres.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUIndFireSecHtgInput
            // 
            this.textBoxNewOAUIndFireSecHtgInput.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUIndFireSecHtgInput.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUIndFireSecHtgInput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUIndFireSecHtgInput.Location = new System.Drawing.Point(185, 290);
            this.textBoxNewOAUIndFireSecHtgInput.Name = "textBoxNewOAUIndFireSecHtgInput";
            this.textBoxNewOAUIndFireSecHtgInput.Size = new System.Drawing.Size(118, 22);
            this.textBoxNewOAUIndFireSecHtgInput.TabIndex = 57;
            this.textBoxNewOAUIndFireSecHtgInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxNewOAUIndFireSecHtgInput.Visible = false;
            // 
            // lblNewOAUIndFireHtgInput
            // 
            this.lblNewOAUIndFireHtgInput.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNewOAUIndFireHtgInput.Location = new System.Drawing.Point(7, 290);
            this.lblNewOAUIndFireHtgInput.Name = "lblNewOAUIndFireHtgInput";
            this.lblNewOAUIndFireHtgInput.Size = new System.Drawing.Size(175, 21);
            this.lblNewOAUIndFireHtgInput.TabIndex = 58;
            this.lblNewOAUIndFireHtgInput.Text = "Secondary Heating Input:";
            this.lblNewOAUIndFireHtgInput.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblNewOAUIndFireHtgInput.Visible = false;
            // 
            // comboBoxNewOAUIFFuelType
            // 
            this.comboBoxNewOAUIFFuelType.BackColor = System.Drawing.Color.White;
            this.comboBoxNewOAUIFFuelType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxNewOAUIFFuelType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.comboBoxNewOAUIFFuelType.FormattingEnabled = true;
            this.comboBoxNewOAUIFFuelType.Items.AddRange(new object[] {
            "N/A",
            "Natural Gas",
            "Liquid Propane"});
            this.comboBoxNewOAUIFFuelType.Location = new System.Drawing.Point(187, 262);
            this.comboBoxNewOAUIFFuelType.Name = "comboBoxNewOAUIFFuelType";
            this.comboBoxNewOAUIFFuelType.Size = new System.Drawing.Size(118, 22);
            this.comboBoxNewOAUIFFuelType.TabIndex = 56;
            // 
            // labelNewOAUIFFuelType
            // 
            this.labelNewOAUIFFuelType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUIFFuelType.Location = new System.Drawing.Point(9, 262);
            this.labelNewOAUIFFuelType.Name = "labelNewOAUIFFuelType";
            this.labelNewOAUIFFuelType.Size = new System.Drawing.Size(175, 21);
            this.labelNewOAUIFFuelType.TabIndex = 55;
            this.labelNewOAUIFFuelType.Text = "Fuel Type:";
            this.labelNewOAUIFFuelType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUIndFireTempRise
            // 
            this.textBoxNewOAUIndFireTempRise.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUIndFireTempRise.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUIndFireTempRise.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUIndFireTempRise.Location = new System.Drawing.Point(187, 127);
            this.textBoxNewOAUIndFireTempRise.Name = "textBoxNewOAUIndFireTempRise";
            this.textBoxNewOAUIndFireTempRise.Size = new System.Drawing.Size(118, 22);
            this.textBoxNewOAUIndFireTempRise.TabIndex = 13;
            this.textBoxNewOAUIndFireTempRise.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUIndFireTempRise
            // 
            this.labelNewOAUIndFireTempRise.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUIndFireTempRise.Location = new System.Drawing.Point(9, 127);
            this.labelNewOAUIndFireTempRise.Name = "labelNewOAUIndFireTempRise";
            this.labelNewOAUIndFireTempRise.Size = new System.Drawing.Size(175, 21);
            this.labelNewOAUIndFireTempRise.TabIndex = 54;
            this.labelNewOAUIndFireTempRise.Text = "Temp Rise:";
            this.labelNewOAUIndFireTempRise.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUIndFireManifoldPressure
            // 
            this.textBoxNewOAUIndFireManifoldPressure.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUIndFireManifoldPressure.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUIndFireManifoldPressure.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUIndFireManifoldPressure.Location = new System.Drawing.Point(187, 235);
            this.textBoxNewOAUIndFireManifoldPressure.Name = "textBoxNewOAUIndFireManifoldPressure";
            this.textBoxNewOAUIndFireManifoldPressure.Size = new System.Drawing.Size(118, 22);
            this.textBoxNewOAUIndFireManifoldPressure.TabIndex = 17;
            this.textBoxNewOAUIndFireManifoldPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxNewOAUIndFireMaxGasPressure
            // 
            this.textBoxNewOAUIndFireMaxGasPressure.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUIndFireMaxGasPressure.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUIndFireMaxGasPressure.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUIndFireMaxGasPressure.Location = new System.Drawing.Point(187, 181);
            this.textBoxNewOAUIndFireMaxGasPressure.Name = "textBoxNewOAUIndFireMaxGasPressure";
            this.textBoxNewOAUIndFireMaxGasPressure.Size = new System.Drawing.Size(118, 22);
            this.textBoxNewOAUIndFireMaxGasPressure.TabIndex = 15;
            this.textBoxNewOAUIndFireMaxGasPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUIndFireMaxGasPressure
            // 
            this.labelNewOAUIndFireMaxGasPressure.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUIndFireMaxGasPressure.Location = new System.Drawing.Point(9, 181);
            this.labelNewOAUIndFireMaxGasPressure.Name = "labelNewOAUIndFireMaxGasPressure";
            this.labelNewOAUIndFireMaxGasPressure.Size = new System.Drawing.Size(175, 21);
            this.labelNewOAUIndFireMaxGasPressure.TabIndex = 50;
            this.labelNewOAUIndFireMaxGasPressure.Text = "Max Gas Pressure:";
            this.labelNewOAUIndFireMaxGasPressure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUIndFireMinGasPressure
            // 
            this.textBoxNewOAUIndFireMinGasPressure.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUIndFireMinGasPressure.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUIndFireMinGasPressure.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUIndFireMinGasPressure.Location = new System.Drawing.Point(187, 208);
            this.textBoxNewOAUIndFireMinGasPressure.Name = "textBoxNewOAUIndFireMinGasPressure";
            this.textBoxNewOAUIndFireMinGasPressure.Size = new System.Drawing.Size(118, 22);
            this.textBoxNewOAUIndFireMinGasPressure.TabIndex = 16;
            this.textBoxNewOAUIndFireMinGasPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUIndFireMinGasPressure
            // 
            this.labelNewOAUIndFireMinGasPressure.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUIndFireMinGasPressure.Location = new System.Drawing.Point(9, 208);
            this.labelNewOAUIndFireMinGasPressure.Name = "labelNewOAUIndFireMinGasPressure";
            this.labelNewOAUIndFireMinGasPressure.Size = new System.Drawing.Size(175, 21);
            this.labelNewOAUIndFireMinGasPressure.TabIndex = 48;
            this.labelNewOAUIndFireMinGasPressure.Text = "Min Gas Pressure:";
            this.labelNewOAUIndFireMinGasPressure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUIndFireMaxOutAirTemp
            // 
            this.textBoxNewOAUIndFireMaxOutAirTemp.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUIndFireMaxOutAirTemp.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUIndFireMaxOutAirTemp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUIndFireMaxOutAirTemp.Location = new System.Drawing.Point(187, 154);
            this.textBoxNewOAUIndFireMaxOutAirTemp.Name = "textBoxNewOAUIndFireMaxOutAirTemp";
            this.textBoxNewOAUIndFireMaxOutAirTemp.Size = new System.Drawing.Size(118, 22);
            this.textBoxNewOAUIndFireMaxOutAirTemp.TabIndex = 14;
            this.textBoxNewOAUIndFireMaxOutAirTemp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOAUIndFireMaxOutAirTemp
            // 
            this.labelNewOAUIndFireMaxOutAirTemp.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOAUIndFireMaxOutAirTemp.Location = new System.Drawing.Point(9, 154);
            this.labelNewOAUIndFireMaxOutAirTemp.Name = "labelNewOAUIndFireMaxOutAirTemp";
            this.labelNewOAUIndFireMaxOutAirTemp.Size = new System.Drawing.Size(175, 21);
            this.labelNewOAUIndFireMaxOutAirTemp.TabIndex = 46;
            this.labelNewOAUIndFireMaxOutAirTemp.Text = "Max Outlet Air Temp:";
            this.labelNewOAUIndFireMaxOutAirTemp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOauIndFireMaxExt
            // 
            this.textBoxNewOauIndFireMaxExt.BackColor = System.Drawing.Color.White;
            this.textBoxNewOauIndFireMaxExt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOauIndFireMaxExt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOauIndFireMaxExt.Location = new System.Drawing.Point(187, 100);
            this.textBoxNewOauIndFireMaxExt.Name = "textBoxNewOauIndFireMaxExt";
            this.textBoxNewOauIndFireMaxExt.Size = new System.Drawing.Size(118, 22);
            this.textBoxNewOauIndFireMaxExt.TabIndex = 12;
            this.textBoxNewOauIndFireMaxExt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxNewOauIndFireMinInputBTU
            // 
            this.textBoxNewOauIndFireMinInputBTU.BackColor = System.Drawing.Color.White;
            this.textBoxNewOauIndFireMinInputBTU.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOauIndFireMinInputBTU.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOauIndFireMinInputBTU.Location = new System.Drawing.Point(187, 73);
            this.textBoxNewOauIndFireMinInputBTU.Name = "textBoxNewOauIndFireMinInputBTU";
            this.textBoxNewOauIndFireMinInputBTU.Size = new System.Drawing.Size(118, 22);
            this.textBoxNewOauIndFireMinInputBTU.TabIndex = 11;
            this.textBoxNewOauIndFireMinInputBTU.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOauIndFireMinHtgInput
            // 
            this.labelNewOauIndFireMinHtgInput.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOauIndFireMinHtgInput.Location = new System.Drawing.Point(9, 73);
            this.labelNewOauIndFireMinHtgInput.Name = "labelNewOauIndFireMinHtgInput";
            this.labelNewOauIndFireMinHtgInput.Size = new System.Drawing.Size(175, 21);
            this.labelNewOauIndFireMinHtgInput.TabIndex = 42;
            this.labelNewOauIndFireMinHtgInput.Text = "Min Input BTU:";
            this.labelNewOauIndFireMinHtgInput.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUIndFireHeatingOutput
            // 
            this.textBoxNewOAUIndFireHeatingOutput.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUIndFireHeatingOutput.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUIndFireHeatingOutput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUIndFireHeatingOutput.Location = new System.Drawing.Point(187, 46);
            this.textBoxNewOAUIndFireHeatingOutput.Name = "textBoxNewOAUIndFireHeatingOutput";
            this.textBoxNewOAUIndFireHeatingOutput.Size = new System.Drawing.Size(118, 22);
            this.textBoxNewOAUIndFireHeatingOutput.TabIndex = 10;
            this.textBoxNewOAUIndFireHeatingOutput.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOauIndFireHtgOutput
            // 
            this.labelNewOauIndFireHtgOutput.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOauIndFireHtgOutput.Location = new System.Drawing.Point(9, 46);
            this.labelNewOauIndFireHtgOutput.Name = "labelNewOauIndFireHtgOutput";
            this.labelNewOauIndFireHtgOutput.Size = new System.Drawing.Size(175, 21);
            this.labelNewOauIndFireHtgOutput.TabIndex = 40;
            this.labelNewOauIndFireHtgOutput.Text = "Heating Output BTUH:";
            this.labelNewOauIndFireHtgOutput.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUIndFireMaxHtgInput
            // 
            this.textBoxNewOAUIndFireMaxHtgInput.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUIndFireMaxHtgInput.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUIndFireMaxHtgInput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUIndFireMaxHtgInput.Location = new System.Drawing.Point(187, 19);
            this.textBoxNewOAUIndFireMaxHtgInput.Name = "textBoxNewOAUIndFireMaxHtgInput";
            this.textBoxNewOAUIndFireMaxHtgInput.Size = new System.Drawing.Size(118, 22);
            this.textBoxNewOAUIndFireMaxHtgInput.TabIndex = 9;
            this.textBoxNewOAUIndFireMaxHtgInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewOauIndFireMaxHtg
            // 
            this.labelNewOauIndFireMaxHtg.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOauIndFireMaxHtg.Location = new System.Drawing.Point(9, 19);
            this.labelNewOauIndFireMaxHtg.Name = "labelNewOauIndFireMaxHtg";
            this.labelNewOauIndFireMaxHtg.Size = new System.Drawing.Size(175, 21);
            this.labelNewOauIndFireMaxHtg.TabIndex = 38;
            this.labelNewOauIndFireMaxHtg.Text = "Max Heating Input BTUH:";
            this.labelNewOauIndFireMaxHtg.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelNewOauIndFireMaxEst
            // 
            this.labelNewOauIndFireMaxEst.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewOauIndFireMaxEst.Location = new System.Drawing.Point(6, 100);
            this.labelNewOauIndFireMaxEst.Name = "labelNewOauIndFireMaxEst";
            this.labelNewOauIndFireMaxEst.Size = new System.Drawing.Size(175, 21);
            this.labelNewOauIndFireMaxEst.TabIndex = 44;
            this.labelNewOauIndFireMaxEst.Text = "Max Ext:";
            this.labelNewOauIndFireMaxEst.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1101, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(99, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // lblNewOAUElecMaxAirOutletTemp
            // 
            this.lblNewOAUElecMaxAirOutletTemp.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNewOAUElecMaxAirOutletTemp.Location = new System.Drawing.Point(8, 54);
            this.lblNewOAUElecMaxAirOutletTemp.Name = "lblNewOAUElecMaxAirOutletTemp";
            this.lblNewOAUElecMaxAirOutletTemp.Size = new System.Drawing.Size(175, 21);
            this.lblNewOAUElecMaxAirOutletTemp.TabIndex = 61;
            this.lblNewOAUElecMaxAirOutletTemp.Text = "Max Air Outlet Temp:";
            this.lblNewOAUElecMaxAirOutletTemp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewOAUElecMaxAirOutletTemp
            // 
            this.textBoxNewOAUElecMaxAirOutletTemp.BackColor = System.Drawing.Color.White;
            this.textBoxNewOAUElecMaxAirOutletTemp.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewOAUElecMaxAirOutletTemp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewOAUElecMaxAirOutletTemp.Location = new System.Drawing.Point(187, 54);
            this.textBoxNewOAUElecMaxAirOutletTemp.Name = "textBoxNewOAUElecMaxAirOutletTemp";
            this.textBoxNewOAUElecMaxAirOutletTemp.Size = new System.Drawing.Size(118, 22);
            this.textBoxNewOAUElecMaxAirOutletTemp.TabIndex = 62;
            this.textBoxNewOAUElecMaxAirOutletTemp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // frmNewLineOAU
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(1101, 748);
            this.Controls.Add(this.groupBoxNewOAU);
            this.Controls.Add(this.menuStrip1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmNewLineOAU";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New OAU Order";
            this.groupBoxNewOAU.ResumeLayout(false);
            this.groupBoxNewOAUDirectFired.ResumeLayout(false);
            this.groupBoxNewOAUDirectFired.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbComp2_2.ResumeLayout(false);
            this.gbComp2_2.PerformLayout();
            this.groupBoxNewOAULanguage.ResumeLayout(false);
            this.groupBoxNewOAUWater_Glycol.ResumeLayout(false);
            this.groupBoxNewOAUWater_Glycol.PerformLayout();
            this.groupBoxNewOAUSteam.ResumeLayout(false);
            this.groupBoxNewOAUSteam.PerformLayout();
            this.groupBoxNewOAUHotWater.ResumeLayout(false);
            this.groupBoxNewOAUHotWater.PerformLayout();
            this.groupBoxParseModelNo.ResumeLayout(false);
            this.groupBoxParseModelNo.PerformLayout();
            this.groupBoxNewOAUFanInfo.ResumeLayout(false);
            this.groupBoxNewOAUFanInfo.PerformLayout();
            this.groupBoxNewOAUCompressor4.ResumeLayout(false);
            this.groupBoxNewOAUCompressor4.PerformLayout();
            this.groupBoxNewOAUCompressor3.ResumeLayout(false);
            this.groupBoxNewOAUCompressor3.PerformLayout();
            this.groupBoxNewOAUCompressor2.ResumeLayout(false);
            this.groupBoxNewOAUCompressor2.PerformLayout();
            this.groupBoxNewOAUCompressor1.ResumeLayout(false);
            this.groupBoxNewOAUCompressor1.PerformLayout();
            this.groupBoxNewOauLineInfo.ResumeLayout(false);
            this.groupBoxNewOauLineInfo.PerformLayout();
            this.groupBoxNewOAUJobInfo.ResumeLayout(false);
            this.groupBoxNewOAUJobInfo.PerformLayout();
            this.groupBoxNewOAUElectricInfo.ResumeLayout(false);
            this.groupBoxNewOAUElectricInfo.PerformLayout();
            this.groupBoxNewOAUElectric.ResumeLayout(false);
            this.groupBoxNewOAUElectric.PerformLayout();
            this.groupBoxNewOAUIndirectFire.ResumeLayout(false);
            this.groupBoxNewOAUIndirectFire.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxNewOAU;
        private System.Windows.Forms.GroupBox groupBoxNewOAUJobInfo;
        private System.Windows.Forms.Label labelNewOAUOrderNum;
        private System.Windows.Forms.Label labelNewOAUCustomer;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Label labelNewOAULineItem;
        private System.Windows.Forms.Label labelNewOAUModelNum;
        private System.Windows.Forms.Label labelNewOAUSerialNo;
        private System.Windows.Forms.Label labelNewOAUHeatingType;
        private System.Windows.Forms.Label labelNewOAUVoltage;
        private System.Windows.Forms.GroupBox groupBoxNewOAUElectricInfo;
        private System.Windows.Forms.Label labelNewOAUMFSMCB;
        private System.Windows.Forms.Label labelNewOAUMinCKTAmp;
        public System.Windows.Forms.TextBox textBoxNewOAULineItem;
        public System.Windows.Forms.TextBox textBoxNewOAUModelNo;
        public System.Windows.Forms.TextBox textBoxNewOAUSerialNo;
        public System.Windows.Forms.ComboBox comboBoxNewOAUHeatingType;
        public System.Windows.Forms.ComboBox comboBoxNewOAUVoltage;
        public System.Windows.Forms.TextBox textBoxNewOAUMFSMCB;
        public System.Windows.Forms.TextBox textBoxNewOAUMinCKTAmp;
        public System.Windows.Forms.TextBox textBoxNewOAUCustomer;
        public System.Windows.Forms.TextBox textBoxNewOAUOrderNum;
        public System.Windows.Forms.TextBox textBoxNewOAUMOP;
        private System.Windows.Forms.Label labelNewOAUMOP;
        public System.Windows.Forms.TextBox textBoxNewOAUElectricalRating;
        private System.Windows.Forms.Label labelNewOAUElectricalRating;
        private System.Windows.Forms.Label labelNewOAUTestPressureLow;
        public System.Windows.Forms.TextBox textBoxNewOAUTestPressureHigh;
        private System.Windows.Forms.Label labelNewOAUTestPressureHigh;
        public System.Windows.Forms.TextBox textBoxNewOAUOperatingVolts;
        private System.Windows.Forms.Label labelNewOAUOperatingVolts;
        public System.Windows.Forms.TextBox textBoxNewOAUTestPressureLow;
        private System.Windows.Forms.GroupBox groupBoxNewOAUCompressor1;
        public System.Windows.Forms.TextBox txtComp1Qty;
        private System.Windows.Forms.Label labelNewOAUComp1Qty;
        public System.Windows.Forms.TextBox txtComp1Phase;
        private System.Windows.Forms.Label labelNewOAUComp1PH;
        public System.Windows.Forms.TextBox txtCircuit1Charge;
        private System.Windows.Forms.Label labelNewOAUComp1Charge;
        public System.Windows.Forms.TextBox txtComp1LRA;
        private System.Windows.Forms.Label labelNewOAUComp1LRA;
        public System.Windows.Forms.TextBox txtComp1RLA_Volts;
        private System.Windows.Forms.Label labelNewOAUComp1RLAVolts;
        private System.Windows.Forms.GroupBox groupBoxNewOAUCompressor2;
        public System.Windows.Forms.TextBox txtCircuit2Charge;
        private System.Windows.Forms.Label labelNewOAUComp2Charge;
        public System.Windows.Forms.TextBox txtComp2LRA;
        private System.Windows.Forms.Label labelNewOAUNewOAUComp2LRA;
        public System.Windows.Forms.TextBox txtComp2RLA_Volts;
        private System.Windows.Forms.Label labelNewOAUComp2RLAVolts;
        public System.Windows.Forms.TextBox txtComp2Phase;
        private System.Windows.Forms.Label labelNewOAUComp2PH;
        public System.Windows.Forms.TextBox txtComp2Qty;
        private System.Windows.Forms.Label labelNewOAUComp2Qty;
        private System.Windows.Forms.GroupBox groupBoxNewOAUCompressor3;
        public System.Windows.Forms.TextBox txtComp3LRA;
        private System.Windows.Forms.Label labelNewOAUComp3LRA;
        public System.Windows.Forms.TextBox txtComp3RLA_Volts;
        private System.Windows.Forms.Label labelNewOAUComp3RLAVolts;
        public System.Windows.Forms.TextBox txtComp3Phase;
        private System.Windows.Forms.Label labelNewOAUComp3PH;
        public System.Windows.Forms.TextBox txtComp3Qty;
        private System.Windows.Forms.Label labelNewOAUComp3Qty;
        private System.Windows.Forms.GroupBox groupBoxNewOAUCompressor4;
        public System.Windows.Forms.TextBox txtComp4LRA;
        private System.Windows.Forms.Label labelNewOAUComp4LRA;
        public System.Windows.Forms.TextBox txtComp4RLA_Volts;
        private System.Windows.Forms.Label labelNewOAUComp4RLAVolts;
        public System.Windows.Forms.TextBox txtComp4Phase;
        private System.Windows.Forms.Label labelNewOAUComp4PH;
        public System.Windows.Forms.TextBox txtComp4Qty;
        private System.Windows.Forms.Label labelNewOAUComp4Qty;
        private System.Windows.Forms.GroupBox groupBoxNewOAUFanInfo;
        public System.Windows.Forms.TextBox textBoxNewOAUFanPwrHP;
        public System.Windows.Forms.Label labelNewOAUFanPwrHP;
        public System.Windows.Forms.TextBox textBoxNewOAUFanPwrFLA;
        private System.Windows.Forms.Label labelNewOAUFanPwrFLA;
        public System.Windows.Forms.TextBox textBoxNewOAUFanPwrPH;
        private System.Windows.Forms.Label labelNewOAUFanPwrPH;
        public System.Windows.Forms.TextBox textBoxNewOAUFanPwrQty;
        private System.Windows.Forms.Label labelNewOAUFanPwrQty;
        public System.Windows.Forms.TextBox textBoxNewOAUFanEvapHP;
        public System.Windows.Forms.Label labelNewOAUFanEvapHP;
        public System.Windows.Forms.TextBox textBoxNewOAUFanEvapFLA;
        private System.Windows.Forms.Label labelNewOAUFanEvapFLA;
        public System.Windows.Forms.TextBox textBoxNewOAUFanEvapPH;
        private System.Windows.Forms.Label labelNewOAUFanEvapPH;
        public System.Windows.Forms.TextBox textBoxNewOAUFanEvapQty;
        private System.Windows.Forms.Label labelNewOAUFanEvapQty;
        public System.Windows.Forms.TextBox textBoxNewOAUFanEnergyHP;
        public System.Windows.Forms.Label labelNewOAUFanEnergyHP;
        public System.Windows.Forms.TextBox textBoxNewOAUFanEnergyFLA;
        private System.Windows.Forms.Label labelNewOAUFanEnergyFLA;
        public System.Windows.Forms.TextBox textBoxNewOAUFanEnergyPH;
        private System.Windows.Forms.Label labelNewOAUFanEnergyPH;
        public System.Windows.Forms.TextBox textBoxNewOAUFanEnergyQty;
        private System.Windows.Forms.Label labelNewOAUFanEnergyQty;
        public System.Windows.Forms.TextBox textBoxNewOAUFanCondHP;
        public System.Windows.Forms.Label labelNewOAUFanCondHP;
        public System.Windows.Forms.TextBox textBoxNewOAUFanCondFLA;
        private System.Windows.Forms.Label labelNewOAUFanCondFLA;
        public System.Windows.Forms.TextBox textBoxNewOAUFanCondPH;
        private System.Windows.Forms.Label labelNewOAUFanCondPH;
        public System.Windows.Forms.TextBox textBoxNewOAUFanCondQty;
        private System.Windows.Forms.Label labelNewOAUFanCondQty;
        public System.Windows.Forms.TextBox textBoxNewOAUIndFireHeatingOutput;
        private System.Windows.Forms.Label labelNewOauIndFireHtgOutput;
        public System.Windows.Forms.TextBox textBoxNewOAUIndFireMaxHtgInput;
        private System.Windows.Forms.Label labelNewOauIndFireMaxHtg;
        public System.Windows.Forms.TextBox textBoxNewOAUIndFireMinGasPressure;
        private System.Windows.Forms.Label labelNewOAUIndFireMinGasPressure;
        public System.Windows.Forms.TextBox textBoxNewOAUIndFireMaxOutAirTemp;
        private System.Windows.Forms.Label labelNewOAUIndFireMaxOutAirTemp;
        public System.Windows.Forms.TextBox textBoxNewOauIndFireMaxExt;
        private System.Windows.Forms.Label labelNewOauIndFireMaxEst;
        public System.Windows.Forms.TextBox textBoxNewOauIndFireMinInputBTU;
        private System.Windows.Forms.Label labelNewOauIndFireMinHtgInput;
        public System.Windows.Forms.TextBox textBoxNewOAUIndFireTempRise;
        private System.Windows.Forms.Label labelNewOAUIndFireTempRise;
        public System.Windows.Forms.TextBox textBoxNewOAUIndFireManifoldPressure;
        private System.Windows.Forms.Label labelNewOAUIndFireManifoldPres;
        public System.Windows.Forms.TextBox textBoxNewOAUIndFireMaxGasPressure;
        private System.Windows.Forms.Label labelNewOAUIndFireMaxGasPressure;
        public System.Windows.Forms.GroupBox groupBoxNewOAUElectric;
        public System.Windows.Forms.GroupBox groupBoxNewOAUIndirectFire;
        public System.Windows.Forms.TextBox textBoxNewOAUVerifiedBy;
        private System.Windows.Forms.Label labelMewOAUVerifiedBy;
        public System.Windows.Forms.CheckBox checkBoxNewOAUVerified;
        private System.Windows.Forms.Label labelNewOAUSlash;
        public System.Windows.Forms.TextBox textBoxNewOAUReleaseNum;
        public System.Windows.Forms.Button buttonNewOAUAccept;
        public System.Windows.Forms.Button buttonNewOAUCancel;
        public System.Windows.Forms.Button buttonNewOAUPrint;
        public System.Windows.Forms.Label labelNewOAUMsg;
        public System.Windows.Forms.GroupBox groupBoxNewOauLineInfo;
        private System.Windows.Forms.Label labelNewOAUIFFuelType;
        public System.Windows.Forms.ComboBox comboBoxNewOAUIFFuelType;
        public System.Windows.Forms.GroupBox groupBoxParseModelNo;
        public System.Windows.Forms.TextBox textBoxModifyModelNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonModelNoCancel;
        private System.Windows.Forms.Button buttonModelNoEdit;
        private System.Windows.Forms.Button buttonModelNoParse;
        public System.Windows.Forms.GroupBox groupBoxNewOAUHotWater;
        public System.Windows.Forms.TextBox textBoxHotWaterEnteringWaterTemp;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox textBoxHotWaterFlowRate;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox textBoxNewOAUEndConsumer;
        private System.Windows.Forms.Label labelNewOAUEndConsumer;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.DateTimePicker dateTimePickerOAUMfgDate;
        public System.Windows.Forms.TextBox textBoxNewOAUJobNum;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.CheckBox checkBoxNewOAUDPP;
        public System.Windows.Forms.Label labelNewOAUDPP2;
        public System.Windows.Forms.Label labelNewOAUDPP1;
        public System.Windows.Forms.TextBox textBoxNewOAUMFSMCB2;
        public System.Windows.Forms.TextBox textBoxNewOAUMinCKTAmp2;
        private System.Windows.Forms.Label lblSCCR;
        public System.Windows.Forms.GroupBox groupBoxNewOAUDirectFired;
        public System.Windows.Forms.TextBox txtNewOAU_DF_MinGasPressure;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox txtNewOAU_DF_ManifoldPressure;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.TextBox txtNewOAU_DF_MaxGasPressure;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.TextBox txtNewOAU_DF_TempRise;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.TextBox txtNewOAU_DF_MinHtgInputBTU;
        private System.Windows.Forms.Label label14;
        public System.Windows.Forms.TextBox txtNewOAU_DF_MaxHtgInputBTU;
        private System.Windows.Forms.Label label15;
        public System.Windows.Forms.TextBox textBoxNewOAUElecHeatingInputElec;
        public System.Windows.Forms.TextBox textBoxNewOAUIndFireSecHtgInput;
        public System.Windows.Forms.Label lblNewOAUIndFireHtgInput;
        public System.Windows.Forms.TextBox txtNewOAUElecSecHtgInput;
        public System.Windows.Forms.Label lblNewOAUElecSecHtgInput;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.GroupBox groupBoxNewOAUSteam;
        public System.Windows.Forms.TextBox textBoxNewOAUSteamCondensateFlowRate;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox textBoxNewOAUSteamOperatingPressure;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.GroupBox groupBoxNewOAUWater_Glycol;
        public System.Windows.Forms.RadioButton radioButtonGlycol;
        public System.Windows.Forms.RadioButton radioButtonWater;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.TextBox txtNewOAU_DF_AirFlowRate;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox txtNewOAU_DF_StaticPressure;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.TextBox txtNewOAU_DF_CutoutTemp;
        public System.Windows.Forms.TextBox txtNewOAU_DF_MinPressureDrop;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.TextBox txtNewOAU_DF_MaxPressureDrop;
        private System.Windows.Forms.Label label19;
        public System.Windows.Forms.Label labelNewOAUElecHeatingInput;
        public System.Windows.Forms.GroupBox groupBoxNewOAULanguage;
        public System.Windows.Forms.CheckBox checkBoxLangFrench;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.TextBox txtComp6LRA;
        private System.Windows.Forms.Label label24;
        public System.Windows.Forms.TextBox txtComp6RLA_Volts;
        private System.Windows.Forms.Label label25;
        public System.Windows.Forms.TextBox txtComp6Phase;
        private System.Windows.Forms.Label label26;
        public System.Windows.Forms.TextBox txtComp6Qty;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox gbComp2_2;
        public System.Windows.Forms.TextBox txtComp5LRA;
        private System.Windows.Forms.Label label20;
        public System.Windows.Forms.TextBox txtComp5RLA_Volts;
        private System.Windows.Forms.Label label21;
        public System.Windows.Forms.TextBox txtComp5Phase;
        private System.Windows.Forms.Label label22;
        public System.Windows.Forms.TextBox txtComp5Qty;
        private System.Windows.Forms.Label label23;
        public System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.ComboBox cbWhseCode;
        private System.Windows.Forms.Label label28;
        public System.Windows.Forms.TextBox txtSCCR;
        public System.Windows.Forms.TextBox textBoxNewOAUElecMaxAirOutletTemp;
        public System.Windows.Forms.Label lblNewOAUElecMaxAirOutletTemp;
    }
}