using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace KCC.OA.Etl
{
    public partial class frmNewLineOAU : Form
    {
        MainBO objMain = new MainBO();

        public string EpicorServer = "";

        public frmNewLineOAU()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBoxNewOAUHeatingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.groupBoxNewOAUIndirectFire.Visible = false;
            //.this.groupBoxNewOAUElectric.Visible = false;
            this.groupBoxNewOAUHotWater.Visible = false;
            this.groupBoxNewOAUDirectFired.Visible = false;

            if (this.comboBoxNewOAUHeatingType.SelectedIndex == 0 || this.comboBoxNewOAUHeatingType.SelectedIndex == 6 || 
                this.comboBoxNewOAUHeatingType.SelectedIndex == 11)  // Indirect Fired or Dual Fuel (Indirect Fired/Electic)
            {
                this.textBoxNewOAUIndFireMaxOutAirTemp.Text = "125 Deg F";
                this.lblNewOAUIndFireHtgInput.Visible = false;
                this.textBoxNewOAUIndFireSecHtgInput.Visible = false;
                this.groupBoxNewOAUIndirectFire.Visible = true;                 
                this.groupBoxNewOAUIndirectFire.BringToFront();

                if (this.comboBoxNewOAUHeatingType.SelectedIndex == 6)
                {                    
                    this.textBoxNewOAUIndFireSecHtgInput.Visible = true;
                    this.lblNewOAUIndFireHtgInput.Visible = true;
                }
            }
            else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 1 || this.comboBoxNewOAUHeatingType.SelectedIndex == 7 ||
                     this.comboBoxNewOAUHeatingType.SelectedIndex == 12) // Electric or Dual Fuel (Electric/Electic)
            {
                this.textBoxNewOAUIndFireMaxOutAirTemp.Text = "90 Deg F";
                this.txtNewOAUElecSecHtgInput.Visible = false;
                this.lblNewOAUElecSecHtgInput.Visible = false;

                if (this.comboBoxNewOAUHeatingType.SelectedIndex == 7)
                {
                    this.txtNewOAUElecSecHtgInput.Visible = true;
                    this.lblNewOAUElecSecHtgInput.Visible = true;
                }

                this.groupBoxNewOAUElectric.Location = new Point(6, 346);
                this.groupBoxNewOAUElectric.Visible = true;                
                this.groupBoxNewOAUElectric.BringToFront();                                  
            }
            else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 4 || this.comboBoxNewOAUHeatingType.SelectedIndex == 8 || 
                     this.comboBoxNewOAUHeatingType.SelectedIndex == 9)  // Hot Water or Dual Fuel (HotWater/Electric)
            {
                this.groupBoxNewOAUHotWater.Location = new Point(6, 346);
                this.groupBoxNewOAUHotWater.Visible = true;

                if (this.comboBoxNewOAUHeatingType.SelectedIndex == 9)
                {
                    this.textBoxNewOAUElecHeatingInputElec.Visible = false;
                    this.labelNewOAUElecHeatingInput.Visible = false;
                    this.txtNewOAUElecSecHtgInput.Visible = true;
                    this.lblNewOAUElecSecHtgInput.Visible = true;

                    this.groupBoxNewOAUElectric.Location = new Point(6, 436);
                    this.groupBoxNewOAUElectric.Visible = true;
                    this.groupBoxNewOAUElectric.BringToFront();           
                }
                
                this.groupBoxNewOAUHotWater.BringToFront();
                this.textBoxHotWaterFlowRate.Focus();
                this.textBoxHotWaterFlowRate.Select();
            }
            else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 3) // Direct Fired
            {
                this.textBoxNewOAUIndFireMaxOutAirTemp.Text = "90 Deg F";

                this.groupBoxNewOAUDirectFired.Location = new Point(6, 346);
                this.groupBoxNewOAUDirectFired.Visible = true;               
                this.groupBoxNewOAUDirectFired.BringToFront();
            }
            else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 5 || this.comboBoxNewOAUHeatingType.SelectedIndex == 13) // Steam
            {
                this.groupBoxNewOAUSteam.Location = new Point(6, 346);
                this.groupBoxNewOAUSteam.Visible = true;

                this.groupBoxNewOAUSteam.BringToFront();
                this.textBoxNewOAUSteamOperatingPressure.Focus();
                this.textBoxNewOAUSteamOperatingPressure.Select();
            }
            else // No Heat
            {                
               
            }

        }

        private void buttonNewOAUCancel_Click(object sender, EventArgs e)
        {
            GlobalModeType.ModeType = "";
            GlobalModelNoVerified.ModelNoVerified = false;
            this.Close();
        }

        private void comboBoxNewOAUVoltage_SelectedIndexChanged(object sender, EventArgs e)
        {

            // Populate these fields in the Electrical Info groupbox
            if (this.comboBoxNewOAUVoltage.SelectedIndex == 0) // 208 voltage.
            {                
                this.textBoxNewOAUElectricalRating.Text = "208-230/60/3";
                this.textBoxNewOAUOperatingVolts.Text = "187 / 253";     
                this.textBoxNewOAUMOP.Text = "N/A";
            }
            else if (this.comboBoxNewOAUVoltage.SelectedIndex == 1) // 460 voltage.
            {               
                this.textBoxNewOAUElectricalRating.Text = "460/60/3";
                this.textBoxNewOAUOperatingVolts.Text = "414 / 506";
                this.textBoxNewOAUMOP.Text = "N/A";
            }
            else if (this.comboBoxNewOAUVoltage.SelectedIndex == 2) // 575 voltage.
            {
                this.textBoxNewOAUElectricalRating.Text = "575/60/3";
                this.textBoxNewOAUOperatingVolts.Text = "517 / 633";
                this.textBoxNewOAUMOP.Text = "N/A";                
            }
            else
            {
                // Set these fields to blank for now (NO 575 voltage info yet).
                this.textBoxNewOAUElectricalRating.Text = "";
                this.textBoxNewOAUOperatingVolts.Text = "";
                this.textBoxNewOAUMOP.Text = "";
                this.textBoxNewOAUTestPressureHigh.Text = "";
                this.textBoxNewOAUTestPressureLow.Text = "";
            }

            //Populate these fields in the Electric Info groupbox
            this.textBoxNewOAUTestPressureHigh.Text = "446";
            this.textBoxNewOAUTestPressureLow.Text = "238";

            // Populate these fields in the Indirect Fire groupbox
            //this.textBoxNewOauIndFireMaxExt.Text = "2.0";               // Commented out because values are saved in the database. 10/17/2021 TonyT
            //this.textBoxNewOAUIndFireTempRise.Text = "10 TO 80 Deg F";
            //this.textBoxNewOAUIndFireTempRise.Text = "10 TO 100 Deg F";
            //this.textBoxNewOAUIndFireMaxOutAirTemp.Text = "125 Deg F";
            //this.textBoxNewOAUIndFireMaxGasPressure.Text = "14";
            //this.textBoxNewOAUIndFireMinGasPressure.Text = "7";
            //this.textBoxNewOAUIndFireManifoldPressure.Text = "3.5";    
        }       

        private void textBoxNewOAUFanCondFLA_Leave(object sender, EventArgs e)
        {
            string tmpStr;

            tmpStr = this.textBoxNewOAUFanCondFLA.Text.Trim();
            if (tmpStr.Length > 0)
            {
                if (tmpStr.IndexOf('-') == -1)
                {
                    //if (tmpStr.Length > 2)
                    //{
                    //    tmpStr = tmpStr.Substring(0, 3);
                    //}
                    //else
                    //{
                    //    tmpStr = tmpStr.Substring(0, tmpStr.Length);                        
                    //}                   
                }
                else
                {
                    tmpStr = tmpStr.Substring(0, tmpStr.IndexOf('-'));
                }
                tmpStr = evalNumAppendZero(tmpStr);
                tmpStr = tmpStr + "-" + this.comboBoxNewOAUVoltage.SelectedItem.ToString();
                this.textBoxNewOAUFanCondFLA.Text = tmpStr;
            }
        }

        private void textBoxNewOAUFanEvapFLA_Leave(object sender, EventArgs e)
        {
            string tmpStr;

            tmpStr = this.textBoxNewOAUFanEvapFLA.Text.Trim();
            if (tmpStr.Length > 0)
            {
                if (tmpStr.IndexOf('-') == -1)
                {
                    //if (tmpStr.Length > 2)
                    //{                       
                    //    tmpStr = tmpStr.Substring(0, 3);
                    //}
                    //else
                    //{
                    //    tmpStr = tmpStr.Substring(0, tmpStr.Length);
                    //}

                }
                else
                {
                    tmpStr = tmpStr.Substring(0, tmpStr.IndexOf('-'));
                }
                tmpStr = evalNumAppendZero(tmpStr);
                tmpStr = tmpStr + "-" + this.comboBoxNewOAUVoltage.SelectedItem.ToString();
                this.textBoxNewOAUFanEvapFLA.Text = tmpStr;
            }
        }

        private void textBoxNewOAUFanEnergyFLA_Leave(object sender, EventArgs e)
        {
            string tmpStr;
            string voltageStr;

            tmpStr = this.textBoxNewOAUFanEnergyFLA.Text.Trim();
            if (tmpStr.Length > 0)
            {
                if (tmpStr.IndexOf('-') == -1)
                {
                    //if (tmpStr.Length > 2)
                    //{
                    //    tmpStr = tmpStr.Substring(0, 3);
                    //}
                    //else
                    //{
                    //    tmpStr = tmpStr.Substring(0, tmpStr.Length);
                    //}
                    tmpStr = evalNumAppendZero(tmpStr);
                    tmpStr = tmpStr + "-" + this.comboBoxNewOAUVoltage.SelectedItem.ToString();
                    
                }
                else
                {
                    voltageStr = tmpStr.Substring(tmpStr.IndexOf('-'), (tmpStr.Length-tmpStr.IndexOf('-')));
                    tmpStr = tmpStr.Substring(0, tmpStr.IndexOf('-'));
                    tmpStr = evalNumAppendZero(tmpStr);
                    tmpStr = tmpStr + voltageStr;
                }
                
                this.textBoxNewOAUFanEnergyFLA.Text = tmpStr;
            }
        }

        private void textBoxNewOAUFanPwrFLA_Leave(object sender, EventArgs e)
        {
            string tmpStr;

            tmpStr = this.textBoxNewOAUFanPwrFLA.Text.Trim();
            if (tmpStr.Length > 0)
            {
                if (tmpStr.IndexOf('-') == -1)
                {
                    //if (tmpStr.Length > 2)
                    //{
                    //    tmpStr = tmpStr.Substring(0, 3);
                    //}
                    //else
                    //{
                    //    tmpStr = tmpStr.Substring(0, tmpStr.Length);
                    //}
                }
                else
                {
                    tmpStr = tmpStr.Substring(0, tmpStr.IndexOf('-'));
                }
                tmpStr = evalNumAppendZero(tmpStr);
                tmpStr = tmpStr + "-" + this.comboBoxNewOAUVoltage.SelectedItem.ToString();
                this.textBoxNewOAUFanPwrFLA.Text = tmpStr;
            }
        }

        private void buttonNewOAUAccept_Click(object sender, EventArgs e)
        {
            if (GlobalModeType.ModeType == "New")
            {
                addNewLineOrderDtlOAU();
            }
            else
            {
                updateOrderDtlOAU();
            }
        }

        private void addNewLineOrderDtlOAU()
        {
            string jobNumStr;
            int orderNumInt;
            int orderLineInt;
            int releaseNumInt;

            string modelNoVerifiedByStr = "";
            string heatingInputStr = "";
            string voltageStr = "";
            string customerStr = "";
            string heatingInputElecStr = "";
            string maxInputStr = "";
            string heatOutputStr = "";
            string endConsumerNameStr = "";
            string flowRateStr = "";
            string enteringTempStr = "";
            string operatingPressureStr = "";
            string secondaryHtgInputStr = "";
            string modelNoStr = "";
            string fluidTypeStr = "";
            string unitRevStr = "";

            byte dualPointPower = 0;

            DateTime mfgDateDate;

            if (this.textBoxNewOAUEndConsumer.Text == "")
            {
                MessageBox.Show("ERROR - End Consumer name is a required field.");
                this.textBoxNewOAUEndConsumer.Focus();
                this.textBoxNewOAUEndConsumer.Select();
                return;
            }

            if (this.textBoxNewOAUSerialNo.Text == "")
            {
                MessageBox.Show("ERROR - Serial No is a required field.");
                this.textBoxNewOAUSerialNo.Focus();
                this.textBoxNewOAUSerialNo.Select();
                return;
            }

            if (GlobalModelNoVerified.ModelNoVerified == false)
            {
                if ((this.textBoxNewOAUModelNo.Text.Length == 29) || (this.textBoxNewOAUModelNo.Text.Length == 39) || (this.textBoxNewOAUModelNo.Text.Length == 69))
                {
                    MessageBox.Show("Please verify that the ModelNo contains the correct value for this unit. " +
                        "If the ModelNo is correct check the box next the ModelNo field to indicate that you " +
                        "have verified the information.");

                    this.checkBoxNewOAUVerified.Visible = true;
                    this.labelNewOAUMsg.Text = "Please verify that the ModelNo is correct and then Check the box once completed.";                                         
                }
                else
                {
                    MessageBox.Show("ERROR - ModelNo contains an Invalid value. ModelNo is either 29 or 39 char long!");
                    GlobalModelNoVerified.ModelNoVerified = false;
                }
            }
            else
            {
                if (this.textBoxNewOAUModelNo.Text.Length == 39)
                {
                    unitRevStr = "Rev5";
                }
                else if (this.textBoxNewOAUModelNo.Text.Length == 69)
                {
                    unitRevStr = "Rev6";
                }               

                jobNumStr = this.textBoxNewOAUJobNum.Text;
                orderNumInt = Int32.Parse(this.textBoxNewOAUOrderNum.Text);
                orderLineInt = Int32.Parse(this.textBoxNewOAULineItem.Text);
                releaseNumInt = Int32.Parse(this.textBoxNewOAUReleaseNum.Text);
                customerStr = this.textBoxNewOAUCustomer.Text;
                endConsumerNameStr = this.textBoxNewOAUEndConsumer.Text;
                modelNoStr = this.textBoxNewOAUModelNo.Text;

                VS_DataSet dsEtl = new VS_DataSet();

                // Write record to the VSOrderHed table.                
                try
                {
                    VS_DataSetTableAdapters.EtlJobHeadTableAdapter etlJobHeadTA =
                        new VS_DataSetTableAdapters.EtlJobHeadTableAdapter();

                    etlJobHeadTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                    etlJobHeadTA.Fill(dsEtl.EtlJobHead);

                    DataRow drJobHead = dsEtl.EtlJobHead.NewRow();

                    drJobHead["JobNum"] = jobNumStr;
                    drJobHead["OrderNum"] = orderNumInt.ToString();
                    drJobHead["OrderLine"] = orderLineInt.ToString();
                    drJobHead["ReleaseNum"] = releaseNumInt.ToString();
                    drJobHead["UnitType"] = "OAU";
                    drJobHead["Customer"] = customerStr;
                    drJobHead["EndConsumerName"] = endConsumerNameStr;

                    dsEtl.EtlJobHead.Rows.Add(drJobHead);

                    etlJobHeadTA.Update(dsEtl.EtlJobHead);
                }
                catch
                {
                    MessageBox.Show("Error occurred while inserting a row to the EtlJobHead table.");
                    return;
                }

                VS_DataSetTableAdapters.EtlOAUTableAdapter etlOAUTA =
                    new VS_DataSetTableAdapters.EtlOAUTableAdapter();

                etlOAUTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                modelNoVerifiedByStr = this.textBoxNewOAUVerifiedBy.Text.Trim();

                if (this.comboBoxNewOAUHeatingType.SelectedIndex == 0)
                {
                    heatingInputStr = "Indirect";
                    heatingInputElecStr = this.comboBoxNewOAUIFFuelType.Text;
                    maxInputStr = this.textBoxNewOAUIndFireMaxHtgInput.Text;
                    heatOutputStr = this.textBoxNewOAUIndFireHeatingOutput.Text;
                }
                else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 1)
                {
                    heatingInputStr = "Electric";
                    this.textBoxNewOAUIndFireMaxOutAirTemp.Text = "90 Deg F";
                }
                else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 2)
                {
                    heatingInputStr = "NoHeat";
                }
                else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 3)
                {
                    heatingInputStr = "Direct";
                }
                else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 4)// Heating type equals Hot Water
                {
                    heatingInputStr = "HotWater";
                    enteringTempStr = this.textBoxHotWaterEnteringWaterTemp.Text;
                    flowRateStr = this.textBoxHotWaterFlowRate.Text;
                }
                else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 5)// Heating type equals Steam
                {
                    heatingInputStr = "Steam";
                    operatingPressureStr = this.textBoxNewOAUSteamOperatingPressure.Text;
                    flowRateStr = this.textBoxNewOAUSteamCondensateFlowRate.Text;
                }

                if (this.comboBoxNewOAUVoltage.SelectedIndex == 0)
                {
                    voltageStr = "208";
                }
                else if (this.comboBoxNewOAUVoltage.SelectedIndex == 1)
                {
                    voltageStr = "460";
                }
                else
                {
                    voltageStr = "575";
                }

                mfgDateDate = this.dateTimePickerOAUMfgDate.Value;

                if (this.checkBoxNewOAUDPP.Checked == true)
                {
                    dualPointPower = 1;
                }

                try
                {
                    etlOAUTA.Fill(dsEtl.EtlOAU);

                    DataRow drEtlOAU = dsEtl.EtlOAU.NewRow();

                    if (unitRevStr == "Rev5")
                    {
                        if (modelNoStr.Substring(13, 1) == "3" || modelNoStr.Substring(13, 1) == "8")
                        {
                            if (radioButtonGlycol.Checked == false && radioButtonWater.Checked == false)
                            {
                                MessageBox.Show("WARNING: No fluid type has been selected.");
                            }
                            else if (radioButtonGlycol.Checked == true)
                            {
                                fluidTypeStr = "Glycol";
                            }
                            else
                            {
                                fluidTypeStr = "Water";
                            }
                        }
                    }
                    else
                    {
                        if (modelNoStr.Substring(13, 1) == "3" || modelNoStr.Substring(13, 1) == "4")
                        {
                            if (radioButtonGlycol.Checked == false && radioButtonWater.Checked == false)
                            {
                                MessageBox.Show("WARNING: No fluid type has been selected.");
                            }
                            else if (radioButtonGlycol.Checked == true)
                            {
                                fluidTypeStr = "Glycol";
                            }
                            else
                            {
                                fluidTypeStr = "Water";
                            }
                        }
                    }

                    objMain.JobNum = jobNumStr;
                    objMain.OrderNum = orderNumInt.ToString();
                    objMain.OrderLine = orderLineInt.ToString();
                    objMain.ReleaseNum = releaseNumInt.ToString();
                    objMain.ModelNo = this.textBoxNewOAUModelNo.Text;
                    objMain.SerialNo = this.textBoxNewOAUSerialNo.Text;
                    objMain.MfgDate = mfgDateDate.ToShortDateString();
                    objMain.UnitType = "OAU";
                    objMain.ElecRating = this.textBoxNewOAUElectricalRating.Text;
                    objMain.OperVoltage = this.textBoxNewOAUOperatingVolts.Text;
                    objMain.DualPointPower = dualPointPower.ToString();
                    objMain.MinCKTAmp = this.textBoxNewOAUMinCKTAmp.Text;
                    objMain.MinCKTAmp2 = this.textBoxNewOAUMinCKTAmp2.Text;
                    objMain.MFSMCB = this.textBoxNewOAUMFSMCB.Text;
                    objMain.MFSMCB2 = this.textBoxNewOAUMFSMCB2.Text;
                    objMain.MOP = this.textBoxNewOAUMOP.Text;
                    objMain.HeatingType = heatingInputStr;
                    objMain.Voltage = voltageStr;
                    objMain.TestPressureHigh = this.textBoxNewOAUTestPressureHigh.Text;
                    objMain.TestPressureLow = this.textBoxNewOAUTestPressureLow.Text;
                    objMain.SecondaryHtgInput = secondaryHtgInputStr;
                    objMain.HeatingInputElectric = heatingInputElecStr;
                    objMain.FuelType = "";

                    objMain.Comp1Qty = this.txtComp1Qty.Text;
                    objMain.Comp2Qty = this.txtComp2Qty.Text; 
                    objMain.Comp3Qty = this.txtComp3Qty.Text;
                    objMain.Comp4Qty = this.txtComp4Qty.Text;
                    objMain.Comp5Qty = this.txtComp5Qty.Text;
                    objMain.Comp6Qty = this.txtComp6Qty.Text; 
                    
                    objMain.Comp1Phase = this.txtComp1Phase.Text;
                    objMain.Comp2Phase = this.txtComp2Phase.Text;
                    objMain.Comp3Phase = this.txtComp3Phase.Text;
                    objMain.Comp4Phase = this.txtComp4Phase.Text;
                    objMain.Comp5Phase = this.txtComp5Phase.Text;
                    objMain.Comp6Phase = this.txtComp6Phase.Text;                                  
                    
                    objMain.Comp1RLA_Volts = this.txtComp1RLA_Volts.Text;
                    objMain.Comp2RLA_Volts = this.txtComp2RLA_Volts.Text;
                    objMain.Comp3RLA_Volts = this.txtComp3RLA_Volts.Text;
                    objMain.Comp4RLA_Volts = this.txtComp4RLA_Volts.Text;
                    objMain.Comp5RLA_Volts = this.txtComp5RLA_Volts.Text;
                    objMain.Comp6RLA_Volts = this.txtComp6RLA_Volts.Text;
                    
                    objMain.Comp1LRA = this.txtComp1LRA.Text;
                    objMain.Comp2LRA = this.txtComp2LRA.Text;
                    objMain.Comp3LRA = this.txtComp3LRA.Text;
                    objMain.Comp4LRA = this.txtComp4LRA.Text;
                    objMain.Comp5LRA = this.txtComp5LRA.Text;
                    objMain.Comp6LRA = this.txtComp6LRA.Text;
                    
                    objMain.Circuit1Charge = this.txtCircuit1Charge.Text;
                    objMain.Circuit2Charge = this.txtCircuit2Charge.Text;
                   
                    objMain.FanCondQty = this.textBoxNewOAUFanCondQty.Text;
                    objMain.FanEvapQty = this.textBoxNewOAUFanEvapQty.Text;
                    objMain.FanErvQty = this.textBoxNewOAUFanEnergyQty.Text;
                    objMain.FanPwrExhQty = this.textBoxNewOAUFanPwrQty.Text;
                    
                    objMain.FanCondPhase = this.textBoxNewOAUFanPwrQty.Text;
                    objMain.FanEvapPhase = this.textBoxNewOAUFanCondPH.Text;
                    objMain.FanErvPhase = this.textBoxNewOAUFanEvapPH.Text;
                    objMain.FanPwrExhPhase = this.textBoxNewOAUFanEnergyPH.Text;
                    
                    objMain.FanCondFLA = this.textBoxNewOAUFanCondFLA.Text;
                    objMain.FanEvapFLA = this.textBoxNewOAUFanEvapFLA.Text;
                    objMain.FanErvFLA = this.textBoxNewOAUFanEnergyFLA.Text;
                    objMain.FanPwrExhFLA = this.textBoxNewOAUFanPwrFLA.Text;
                    
                    objMain.FanCondHP = this.textBoxNewOAUFanCondHP.Text;
                    objMain.FanEvapHP = this.textBoxNewOAUFanEvapHP.Text;
                    objMain.FanErvHP = this.textBoxNewOAUFanEnergyHP.Text;
                    objMain.FanPwrExhHP = this.textBoxNewOAUFanPwrHP.Text;
                    
                    objMain.FlowRate = flowRateStr;
                    objMain.EnteringTemp = enteringTempStr;
                    objMain.WaterGlycol = fluidTypeStr;
                    objMain.OperatingPressure = this.textBoxNewOAUSteamOperatingPressure.Text; 
                                        
                    objMain.DF_StaticPressure = this.txtNewOAU_DF_StaticPressure.Text;
                    objMain.DF_MaxHtgInputBTUH = this.txtNewOAU_DF_MaxHtgInputBTU.Text;
                    objMain.DF_MinHeatingInput = this.txtNewOAU_DF_MinHtgInputBTU.Text;
                    objMain.DF_TempRise = this.txtNewOAU_DF_TempRise.Text;
                    objMain.DF_MaxGasPressure = this.txtNewOAU_DF_MaxGasPressure.Text;
                    objMain.DF_MinGasPressure = this.txtNewOAU_DF_MinGasPressure.Text;
                    objMain.DF_MinPressureDrop = this.txtNewOAU_DF_MinPressureDrop.Text;
                    objMain.DF_MaxPressureDrop = this.txtNewOAU_DF_MaxPressureDrop.Text;
                    objMain.DF_ManifoldPressure = this.txtNewOAU_DF_ManifoldPressure.Text;
                    objMain.DF_CutoutTemp = this.txtNewOAU_DF_CutoutTemp.Text;     
              
                    objMain.IN_MaxHtgInputBTUH = maxInputStr;  
                    objMain.IN_HeatingOutputBTUH = heatOutputStr;
                    objMain.IN_MinInputBTU = this.textBoxNewOauIndFireMinInputBTU.Text;
                    objMain.IN_MaxExt = this.textBoxNewOauIndFireMaxExt.Text;
                    objMain.IN_TempRise = this.textBoxNewOAUIndFireTempRise.Text;
                    objMain.IN_MaxOutAirTemp = this.textBoxNewOAUIndFireMaxOutAirTemp.Text;
                    objMain.IN_MaxGasPressure = this.textBoxNewOAUIndFireMaxGasPressure.Text;
                    objMain.IN_MinGasPressure = this.textBoxNewOAUIndFireMinGasPressure.Text;
                    objMain.IN_ManifoldPressure = this.textBoxNewOAUIndFireManifoldPressure.Text;
                    
                    objMain.ModelNoVerifiedBy = modelNoVerifiedByStr;

                    if (checkBoxLangFrench.Checked == true)
                    {
                        objMain.French = "True";
                    }
                    else
                    {
                        objMain.French = "False";
                    }
                    
                   
                    // Dual Point Power Option
                    if (checkBoxNewOAUDPP.Checked == true)
                    {
                        objMain.DualPointPower = "1";
                    }
                    else
                    {
                        objMain.DualPointPower = "0";
                    }

                    if (cbWhseCode.SelectedIndex == 0)
                    {
                        objMain.WhseCode = "LWH2";
                    }
                    else
                    {
                        objMain.WhseCode = "LWH5";
                    }

                    //dsEtl.EtlOAU.Rows.Add(drEtlOAU);

                    //etlOAUTA.Update(dsEtl.EtlOAU);

                    objMain.InsertEtlOAU();

                    MessageBox.Show("Job # " + jobNumStr + " added successfully.");

                    this.buttonNewOAUAccept.Text = "Update";
                    GlobalModeType.ModeType = "Update";
                    this.buttonNewOAUPrint.Enabled = true;
                    this.labelNewOAUMsg.Text = this.textBoxNewOAUSerialNo.Text +
                        " has been successfully written to the EtlOAU table. " +
                        "Use the Update button to modify this unit.";
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR inserting data into R6_OA_EtlOAU: " + ex);
                }
                
            }
           
        }

        private void updateOrderDtlOAU()
        {
            string jobNumStr;
            int orderNumInt;
            int orderLineInt;
            int releaseNumInt;

            string modelNoVerifiedByStr = "";
            string heatingInputStr = "";
            string heatingInputElecStr = "";
            string voltageStr = "";
            string maxInputStr = "";
            string heatOutputStr = "";
            string flowRateStr = "";
            string operatingPressureStr = "";
            string secondaryHtgInputStr = "";
            string enteringTempStr = "";
            string fluidTypeStr = "";
            string modelNoStr = "";
            string serialNoStr = "";
            string languageFrench = "False";

            string fanCondHP = "";
            string fanEvapHP = "";
            string fanEnergyHP = "";
            string fanPwrHP = "";           

            // Dual Point Power Option Variables
            byte dualPointPower = 0;

            DateTime mfgDateDate;

            if (this.textBoxNewOAUSerialNo.Text == "")
            {
                MessageBox.Show("ERROR - Serial No is a required field.");
                this.textBoxNewOAUSerialNo.Focus();
                this.textBoxNewOAUSerialNo.Select();
                return;
            }

            if (GlobalModelNoVerified.ModelNoVerified == false)
            {
                if (this.textBoxNewOAUModelNo.Text.Length == 29)
                {
                    MessageBox.Show("Please verify that the ModelNo contains the correct value for this unit. " +
                        "If the ModelNo is correct check the box next the ModelNo field to indicate that you " +
                        "have verified the information.");

                    this.checkBoxNewOAUVerified.Visible = true;
                    this.labelNewOAUMsg.Text = "Please verify that the ModelNo is correct and then Check the box once completed.";
                }
                else
                {
                    MessageBox.Show("ERROR - ModelNo contains an Invalid value. ModelNo is always 29 char long!");
                    GlobalModelNoVerified.ModelNoVerified = false;
                }
            }
            else
            {
                VS_DataSetTableAdapters.EtlOAUTableAdapter etlOAUTA =
                    new VS_DataSetTableAdapters.EtlOAUTableAdapter();

                etlOAUTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                jobNumStr = textBoxNewOAUJobNum.Text;
                orderNumInt = Int32.Parse(this.textBoxNewOAUOrderNum.Text);
                orderLineInt = Int32.Parse(this.textBoxNewOAULineItem.Text);
                releaseNumInt = Int32.Parse(this.textBoxNewOAUReleaseNum.Text);
                modelNoStr = this.textBoxNewOAUModelNo.Text;
                serialNoStr = this.textBoxNewOAUSerialNo.Text;


                if (this.comboBoxNewOAUHeatingType.SelectedIndex == 0 || this.comboBoxNewOAUHeatingType.SelectedIndex == 6)  // Heating type equals Indirect Fired or Dual Fuel Indirect/Electric
                {
                    heatingInputStr = "Indirect";
                    heatingInputElecStr = this.comboBoxNewOAUIFFuelType.Text;
                    maxInputStr = this.textBoxNewOAUIndFireMaxHtgInput.Text;
                    heatOutputStr = this.textBoxNewOAUIndFireHeatingOutput.Text;

                     if (this.comboBoxNewOAUHeatingType.SelectedIndex == 6)
                     {
                         heatingInputStr = "Dual Fuel (PRI-IF/SEC-ELEC)";
                         secondaryHtgInputStr = this.textBoxNewOAUIndFireSecHtgInput.Text;
                     }
                }
                else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 1 || this.comboBoxNewOAUHeatingType.SelectedIndex == 7)
                {
                    heatingInputStr = "Electric";
                    heatingInputElecStr = this.textBoxNewOAUElecHeatingInputElec.Text;

                    if (this.comboBoxNewOAUHeatingType.SelectedIndex == 7)
                    {
                        heatingInputStr = "Dual Fuel (PRI-ELEC/SEC-ELEC)";
                        secondaryHtgInputStr = this.txtNewOAUElecSecHtgInput.Text;
                    }
                }
                else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 2)
                {
                    heatingInputStr = "NoHeat";
                }
                else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 3)
                {
                    heatingInputStr = "Direct";
                    flowRateStr = this.txtNewOAU_DF_AirFlowRate.Text;
                    fluidTypeStr = this.txtNewOAU_DF_CutoutTemp.Text;

                }
                else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 4) // Heating type equals Hot Water
                {
                    heatingInputStr = "HotWater";
                    enteringTempStr = this.textBoxHotWaterEnteringWaterTemp.Text;
                    flowRateStr = this.textBoxHotWaterFlowRate.Text;
                }
                else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 5) // Heating type equals Steam
                {
                    heatingInputStr = "Steam";
                    operatingPressureStr = this.textBoxNewOAUSteamOperatingPressure.Text;
                    flowRateStr = this.textBoxNewOAUSteamCondensateFlowRate.Text;
                }
                else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 8) // Heating type equals Hot Water
                {
                    heatingInputStr = "Dual Fuel (PRI-HotWater/SEC-DF)";
                    enteringTempStr = this.textBoxHotWaterEnteringWaterTemp.Text;
                    flowRateStr = this.textBoxHotWaterFlowRate.Text;
                }
                else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 9) // Heating type equals Hot Water
                {
                    heatingInputStr = "Dual Fuel (PRI-HotWater/SEC-ELEC)";
                    enteringTempStr = this.textBoxHotWaterEnteringWaterTemp.Text;
                    flowRateStr = this.textBoxHotWaterFlowRate.Text;
                    secondaryHtgInputStr = this.txtNewOAUElecSecHtgInput.Text;
                }
                else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 10) // Heating type equals NoPrimaryHeat/Elec-Staged Secondary
                {
                    heatingInputStr = "NoPrimHeat/Elec-Staged";                    
                    secondaryHtgInputStr = this.txtNewOAUElecSecHtgInput.Text;
                }

                if (this.comboBoxNewOAUVoltage.SelectedIndex == 0)
                {
                    voltageStr = "208";
                }
                else if (this.comboBoxNewOAUVoltage.SelectedIndex == 1)
                {
                    voltageStr = "230";
                }
                else if (this.comboBoxNewOAUVoltage.SelectedIndex == 2)
                {
                    voltageStr = "460";
                }
                else
                {
                    voltageStr = "575";
                }

                if (modelNoStr.Substring(13, 1) == "3" || modelNoStr.Substring(13, 1) == "8")
                {
                    if (radioButtonGlycol.Checked == false && radioButtonWater.Checked == false)
                    {
                        MessageBox.Show("WARNING: No fluid type has been selected.");
                    }
                    else if (radioButtonGlycol.Checked == true)
                    {
                        fluidTypeStr = "Glycol";
                    }
                    else
                    {
                        fluidTypeStr = "Water";
                    }
                }

                /*
                fanEvapKW = textBoxNewOAUFanEvapKW.Text;
                fanEnergyKW = textBoxNewOAUFanEnergyKW.Text;
                fanPwrKW = textBoxNewOAUFanPwrKW.Text;
                */
                fanCondHP = textBoxNewOAUFanCondHP.Text;
                fanEvapHP = textBoxNewOAUFanEvapHP.Text;
                fanEnergyHP = textBoxNewOAUFanEnergyHP.Text;
                fanPwrHP = textBoxNewOAUFanPwrHP.Text;

                if (checkBoxNewOAUDPP.Checked == true)
                {
                    dualPointPower = 1;
                }

                if (checkBoxLangFrench.Checked == true)
                {
                    languageFrench = "True";
                }
                else
                {
                    languageFrench = "False";
                }               

                mfgDateDate = this.dateTimePickerOAUMfgDate.Value;                

                updateCustomerName(jobNumStr);

                try
                {                           
                    objMain.JobNum = jobNumStr;
                    objMain.OrderNum = orderNumInt.ToString();
                    objMain.OrderLine = orderLineInt.ToString();
                    objMain.ReleaseNum = releaseNumInt.ToString();
                    objMain.ModelNo = this.textBoxNewOAUModelNo.Text;
                    objMain.SerialNo = this.textBoxNewOAUSerialNo.Text;
                    objMain.MfgDate = mfgDateDate.ToShortDateString();
                    objMain.UnitType = "OAU";
                    objMain.ElecRating = this.textBoxNewOAUElectricalRating.Text;
                    objMain.OperVoltage = this.textBoxNewOAUOperatingVolts.Text;
                    objMain.DualPointPower = dualPointPower.ToString();
                    objMain.MinCKTAmp = this.textBoxNewOAUMinCKTAmp.Text;
                    objMain.MinCKTAmp2 = this.textBoxNewOAUMinCKTAmp2.Text;
                    objMain.MFSMCB = this.textBoxNewOAUMFSMCB.Text;
                    objMain.MFSMCB2 = this.textBoxNewOAUMFSMCB2.Text;
                    objMain.MOP = this.textBoxNewOAUMOP.Text;
                    objMain.HeatingType = heatingInputStr;
                    objMain.Voltage = voltageStr;
                    objMain.TestPressureHigh = this.textBoxNewOAUTestPressureHigh.Text;
                    objMain.TestPressureLow = this.textBoxNewOAUTestPressureLow.Text;
                    objMain.SecondaryHtgInput = secondaryHtgInputStr;
                    objMain.HeatingInputElectric = heatingInputElecStr;
                    objMain.FuelType = "";

                    objMain.Comp1Qty = this.txtComp1Qty.Text;
                    objMain.Comp2Qty = this.txtComp2Qty.Text;
                    objMain.Comp3Qty = this.txtComp3Qty.Text;
                    objMain.Comp4Qty = this.txtComp4Qty.Text;
                    objMain.Comp5Qty = this.txtComp5Qty.Text;
                    objMain.Comp6Qty = this.txtComp6Qty.Text;

                    objMain.Comp1Phase = this.txtComp1Phase.Text;
                    objMain.Comp2Phase = this.txtComp2Phase.Text;
                    objMain.Comp3Phase = this.txtComp3Phase.Text;
                    objMain.Comp4Phase = this.txtComp4Phase.Text;
                    objMain.Comp5Phase = this.txtComp5Phase.Text;
                    objMain.Comp6Phase = this.txtComp6Phase.Text;

                    objMain.Comp1RLA_Volts = this.txtComp1RLA_Volts.Text;
                    objMain.Comp2RLA_Volts = this.txtComp2RLA_Volts.Text;
                    objMain.Comp3RLA_Volts = this.txtComp3RLA_Volts.Text;
                    objMain.Comp4RLA_Volts = this.txtComp4RLA_Volts.Text;
                    objMain.Comp5RLA_Volts = this.txtComp5RLA_Volts.Text;
                    objMain.Comp6RLA_Volts = this.txtComp6RLA_Volts.Text;

                    objMain.Comp1LRA = this.txtComp1LRA.Text;
                    objMain.Comp2LRA = this.txtComp2LRA.Text;
                    objMain.Comp3LRA = this.txtComp3LRA.Text;
                    objMain.Comp4LRA = this.txtComp4LRA.Text;
                    objMain.Comp5LRA = this.txtComp5LRA.Text;
                    objMain.Comp6LRA = this.txtComp6LRA.Text;

                    objMain.Circuit1Charge = this.txtCircuit1Charge.Text;
                    objMain.Circuit2Charge = this.txtCircuit2Charge.Text;

                    objMain.FanCondQty = this.textBoxNewOAUFanCondQty.Text;
                    objMain.FanEvapQty = this.textBoxNewOAUFanEvapQty.Text;
                    objMain.FanErvQty = this.textBoxNewOAUFanEnergyQty.Text;
                    objMain.FanPwrExhQty = this.textBoxNewOAUFanPwrQty.Text;

                    objMain.FanCondPhase = this.textBoxNewOAUFanCondPH.Text;
                    objMain.FanEvapPhase = this.textBoxNewOAUFanEvapPH.Text;
                    objMain.FanErvPhase = this.textBoxNewOAUFanEnergyPH.Text;
                    objMain.FanPwrExhPhase = this.textBoxNewOAUFanPwrPH.Text;

                    objMain.FanCondFLA = this.textBoxNewOAUFanCondFLA.Text;
                    objMain.FanEvapFLA = this.textBoxNewOAUFanEvapFLA.Text;
                    objMain.FanErvFLA = this.textBoxNewOAUFanEnergyFLA.Text;
                    objMain.FanPwrExhFLA = this.textBoxNewOAUFanPwrFLA.Text;

                    objMain.FanCondHP = this.textBoxNewOAUFanCondHP.Text;
                    objMain.FanEvapHP = this.textBoxNewOAUFanEvapHP.Text;
                    objMain.FanErvHP = this.textBoxNewOAUFanEnergyHP.Text;
                    objMain.FanPwrExhHP = this.textBoxNewOAUFanPwrHP.Text;

                    objMain.FlowRate = flowRateStr;
                    objMain.EnteringTemp = enteringTempStr;
                    objMain.WaterGlycol = fluidTypeStr;
                    objMain.OperatingPressure = this.textBoxNewOAUSteamOperatingPressure.Text;

                    objMain.DF_StaticPressure = this.txtNewOAU_DF_StaticPressure.Text;
                    objMain.DF_MaxHtgInputBTUH = this.txtNewOAU_DF_MaxHtgInputBTU.Text;
                    objMain.DF_MinHeatingInput = this.txtNewOAU_DF_MinHtgInputBTU.Text;
                    objMain.DF_TempRise = this.txtNewOAU_DF_TempRise.Text;
                    objMain.DF_MaxGasPressure = this.txtNewOAU_DF_MaxGasPressure.Text;
                    objMain.DF_MinGasPressure = this.txtNewOAU_DF_MinGasPressure.Text;
                    objMain.DF_MinPressureDrop = this.txtNewOAU_DF_MinPressureDrop.Text;
                    objMain.DF_MaxPressureDrop = this.txtNewOAU_DF_MaxPressureDrop.Text;
                    objMain.DF_ManifoldPressure = this.txtNewOAU_DF_ManifoldPressure.Text;
                    objMain.DF_CutoutTemp = this.txtNewOAU_DF_CutoutTemp.Text;

                    objMain.IN_MaxHtgInputBTUH = maxInputStr;
                    objMain.IN_HeatingOutputBTUH = heatOutputStr;
                    objMain.IN_MinInputBTU = this.textBoxNewOauIndFireMinInputBTU.Text;
                    objMain.IN_MaxExt = this.textBoxNewOauIndFireMaxExt.Text;
                    objMain.IN_TempRise = this.textBoxNewOAUIndFireTempRise.Text;

                    if (heatingInputStr == "Electric")
                    {
                        objMain.IN_MaxOutAirTemp = this.textBoxNewOAUElecMaxAirOutletTemp.Text;
                    }
                    else
                    {
                        objMain.IN_MaxOutAirTemp = this.textBoxNewOAUIndFireMaxOutAirTemp.Text;
                    }
                    objMain.IN_MaxGasPressure = this.textBoxNewOAUIndFireMaxGasPressure.Text;
                    objMain.IN_MinGasPressure = this.textBoxNewOAUIndFireMinGasPressure.Text;
                    objMain.IN_ManifoldPressure = this.textBoxNewOAUIndFireManifoldPressure.Text;

                    objMain.ModelNoVerifiedBy = modelNoVerifiedByStr;

                    if (checkBoxLangFrench.Checked == true)
                    {
                        objMain.French = "True";
                    }
                    else
                    {
                        objMain.French = "False";
                    }


                    // Dual Point Power Option
                    if (checkBoxNewOAUDPP.Checked == true)
                    {
                        objMain.DualPointPower = "1";
                    }
                    else
                    {
                        objMain.DualPointPower = "0";
                    }

                    if (cbWhseCode.SelectedIndex == 0)
                    {
                        objMain.WhseCode = "LWH2";
                    }
                    else
                    {
                        objMain.WhseCode = "LWH5";
                    }

                    objMain.UpdateEtlOAU();

                    MessageBox.Show("Job # " + jobNumStr + " Updated successfully.");

                    this.buttonNewOAUAccept.Text = "Update";
                    GlobalModeType.ModeType = "Update";
                    this.buttonNewOAUPrint.Enabled = true;
                    this.labelNewOAUMsg.Text = orderNumInt.ToString() + "-" + orderLineInt.ToString() + "-" +
                        releaseNumInt.ToString() +
                        " has been successfully updated in the OrderDtlOAU table. " +
                        "Use the Update button for any additional modifications";
                }
                catch  (Exception f)
                {
                    MessageBox.Show("ERROR - Updating data in EtlOAU for Job # " + 
                        jobNumStr + " - " + f);
                }
            }
        }         
     
        private void updateCustomerName(string jobNumStr)
        {
            string custNameStr;

            VS_DataSetTableAdapters.EtlJobHeadTableAdapter etlJobHeadTA =
                new VS_DataSetTableAdapters.EtlJobHeadTableAdapter();

            etlJobHeadTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

            custNameStr = this.textBoxNewOAUCustomer.Text;

            try
            {
                etlJobHeadTA.UpdateQueryCustomer(custNameStr, jobNumStr);
            }
            catch (Exception f)
            {
                MessageBox.Show("ERROR - Updating Customer in EtlJobHead Table - " + f);
            }
        }

        private void checkBoxNewOAUVerified_CheckedChanged(object sender, EventArgs e)
        {
            string userNameStr = "";

            int slashLocInt;

            if (this.checkBoxNewOAUVerified.Checked == true)
            {
                if ((GlobalModeType.ModeType == "PreUpdate") || (GlobalModeType.ModeType == "New"))
                {
                    userNameStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
                    if (userNameStr != "")
                    {
                        slashLocInt = userNameStr.IndexOf('\\');
                        if (slashLocInt != -1)
                        {
                            ++slashLocInt;
                            userNameStr = userNameStr.Substring(slashLocInt, (userNameStr.Length - slashLocInt));
                        }
                        this.labelMewOAUVerifiedBy.Visible = true;
                        this.textBoxNewOAUVerifiedBy.Text = userNameStr;
                        this.textBoxNewOAUVerifiedBy.Visible = true;
                        GlobalModelNoVerified.ModelNoVerified = true;
                        if (GlobalModeType.ModeType != "PreUpdate")
                        {
                            this.buttonNewOAUAccept.Text = "OK";
                            this.labelNewOAUMsg.Text = "Press the OK button to commit the data to the OrderDtlOAU table.";
                        }
                    }
                    //this.buttonNewOAUPrint.Enabled = true;
                }
            }
            else
            {
                GlobalModeType.ModeType = "PreUpdate";
                this.textBoxNewOAUModelNo.Enabled = true;
                this.buttonNewOAUPrint.Enabled = false;
                this.textBoxNewOAUVerifiedBy.Text = "";
            }
            
        }

        private void buttonNewOAUPrint_Click(object sender, EventArgs e)
        {
            string secHeatingInputStr = "";
            string heatingTypeStr = "";
            string heatTypeStr = "";
            string fuelTypeStr = "";
            string maxInputStr = "";
            string maxOutAirTempStr = "";
            string heatOutputStr = "";
            string mfgDateStr = "";
            string reportString = "";
            string modelNo ="";
            string unitManual = "";
            string manifoldPressure = "";
            string tempRise = "";
            string maxGasSupplyPressure = "";
            string minGasSupplyPressure = "";
            string maxHeatingInputBTUStr = "";
            string minHeatingInputBTUStr = "";
            string operatingPressureStr = "";
            string flowRateStr = "";
            string enteringTempStr = "";
            string fluidType_CutoutTempStr = "";
            string maxPressureDrop = "";
            string minPressureDrop = "";
            string staticPressure = "";
            string warehouseCode = "";
            string comp1RLA_VoltsStr = "";
            string comp2RLA_VoltsStr = "";
            string comp3RLA_VoltsStr = "";
            string comp4RLA_VoltsStr = "";
            string comp5RLA_VoltsStr = "";
            string comp6RLA_VoltsStr = "";
            string comp1QtyStr = "";
            string comp2QtyStr = "";
            string comp3QtyStr = "";
            string comp4QtyStr = "";
            string comp5QtyStr = "";
            string comp6QtyStr = "";
            string comp1PhaseStr = "";
            string comp2PhaseStr = "";
            string comp3PhaseStr = "";
            string comp4PhaseStr = "";
            string comp5PhaseStr = "";
            string comp6PhaseStr = "";
            string comp1LRAStr = "";
            string comp2LRAStr = "";
            string comp3LRAStr = "";
            string comp4LRAStr = "";
            string comp5LRAStr = "";
            string comp6LRAStr = "";
            string comp1CirPos = "";
            string comp2CirPos = "";
            string comp3CirPos = "";
            string comp4CirPos = "";
            string comp5CirPos = "";
            string comp6CirPos = "";

            bool cirPosComplete = false;
           
            byte dualPointPower = 0;

            DateTime mfgDateDate;

            frmPrintOAU frmPrint = new frmPrintOAU();
            modelNo = this.textBoxNewOAUModelNo.Text;

            if (this.comboBoxNewOAUHeatingType.SelectedIndex == 0 || this.comboBoxNewOAUHeatingType.SelectedIndex == 11) 
            {
                heatingTypeStr = "Indirect";
                heatTypeStr = "Indirect";
                maxHeatingInputBTUStr = this.textBoxNewOAUIndFireMaxHtgInput.Text;
                fuelTypeStr = this.comboBoxNewOAUIFFuelType.Text;
                minHeatingInputBTUStr = this.textBoxNewOauIndFireMinInputBTU.Text;
                heatOutputStr = this.textBoxNewOAUIndFireHeatingOutput.Text;
                manifoldPressure = this.textBoxNewOAUIndFireManifoldPressure.Text;
                tempRise = this.textBoxNewOAUIndFireTempRise.Text;
                maxGasSupplyPressure = this.textBoxNewOAUIndFireMaxGasPressure.Text;
                minGasSupplyPressure = this.textBoxNewOAUIndFireMinGasPressure.Text;
                maxOutAirTempStr = textBoxNewOAUIndFireMaxOutAirTemp.Text;
            }
            else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 1 || this.comboBoxNewOAUHeatingType.SelectedIndex == 12)
            {
                heatingTypeStr = "Electric";
                heatTypeStr = "Electric";
                maxHeatingInputBTUStr = this.textBoxNewOAUElecHeatingInputElec.Text;
                maxOutAirTempStr = this.textBoxNewOAUElecMaxAirOutletTemp.Text;
                //maxOutAirTempStr = "90 F/32 C";
                if (modelNo.Substring(13, 1) == "3" || modelNo.Substring(13, 1) == "8")
                {
                    heatTypeStr = "ElectricWaterSource";
                    fuelTypeStr = "WaterSource";
                }
            }
            else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 2) 
            {
                heatingTypeStr = "NoHeat";
                heatTypeStr = "NoHeat";
                if (modelNo.Substring(13, 1) == "3" || modelNo.Substring(13, 1) == "8")
                {
                    fuelTypeStr = "WaterSource";
                }
            }
            else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 3)
            {
                heatingTypeStr = "Direct";
                heatTypeStr = "Direct";
                maxHeatingInputBTUStr = this.txtNewOAU_DF_MaxHtgInputBTU.Text;
                minHeatingInputBTUStr = this.txtNewOAU_DF_MinHtgInputBTU.Text;
                manifoldPressure = this.txtNewOAU_DF_ManifoldPressure.Text;
                tempRise = this.txtNewOAU_DF_TempRise.Text;
                maxGasSupplyPressure = this.txtNewOAU_DF_MaxGasPressure.Text;
                minGasSupplyPressure = this.txtNewOAU_DF_MinGasPressure.Text;
                maxOutAirTempStr = this.txtNewOAU_DF_TempRise.Text;
                flowRateStr = this.txtNewOAU_DF_AirFlowRate.Text;
                fluidType_CutoutTempStr = this.txtNewOAU_DF_CutoutTemp.Text;
                maxPressureDrop = this.txtNewOAU_DF_MaxPressureDrop.Text;
                minPressureDrop = this.txtNewOAU_DF_MinPressureDrop.Text;
                staticPressure = this.txtNewOAU_DF_StaticPressure.Text;
            }
            else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 4)
            {
                heatingTypeStr = "HotWater";
                heatTypeStr = "HotWater";
                enteringTempStr = this.textBoxHotWaterEnteringWaterTemp.Text;
                flowRateStr = this.textBoxHotWaterFlowRate.Text;               
            }
            else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 5 || this.comboBoxNewOAUHeatingType.SelectedIndex == 13 || 
                     this.comboBoxNewOAUHeatingType.SelectedIndex == 14)
            {
                heatingTypeStr = "Steam";
                heatTypeStr = "Steam";
                flowRateStr = this.textBoxNewOAUSteamCondensateFlowRate.Text;
                operatingPressureStr = this.textBoxNewOAUSteamOperatingPressure.Text;
            }
            else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 6)
            {
                heatingTypeStr = "DualFuel(IF/Elec)";
                heatTypeStr = "DualFuel(IF/Elec)";
                maxInputStr = this.textBoxNewOAUIndFireMaxHtgInput.Text;                                                                                          
                maxHeatingInputBTUStr = this.textBoxNewOAUIndFireMaxHtgInput.Text;
                fuelTypeStr = this.comboBoxNewOAUIFFuelType.Text;
                minHeatingInputBTUStr = this.textBoxNewOauIndFireMinInputBTU.Text;
                heatOutputStr = this.textBoxNewOAUIndFireHeatingOutput.Text;
                manifoldPressure = this.textBoxNewOAUIndFireManifoldPressure.Text;
                tempRise = this.textBoxNewOAUIndFireTempRise.Text;
                maxGasSupplyPressure = this.textBoxNewOAUIndFireMaxGasPressure.Text;
                minGasSupplyPressure = this.textBoxNewOAUIndFireMinGasPressure.Text;
                secHeatingInputStr = this.textBoxNewOAUIndFireSecHtgInput.Text;
                //maxOutAirTempStr = "125 F/52 C";
            }
            else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 7)
            {
                heatingTypeStr = "DualFuel(Elec/Elec)";
                heatTypeStr = "DualFuel(Elec/Elec)";
                
                maxHeatingInputBTUStr = this.textBoxNewOAUElecHeatingInputElec.Text;
                secHeatingInputStr = this.txtNewOAUElecSecHtgInput.Text;
                maxOutAirTempStr = this.textBoxNewOAUElecMaxAirOutletTemp.Text;
                //if (secHeatingInputStr != "")   //Commented out per Eric League 09/17/2019
                //{
                //    maxHeatingInputBTUStr = (Convert.ToDouble(maxHeatingInputBTUStr) - Convert.ToDouble(secHeatingInputStr)).ToString("f2");
                //}
               
            }
            else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 8) // DualFuel (HotWater/Direct-Fired)
            {
                heatingTypeStr = "HotWater";
                heatTypeStr = "HotWater";
                enteringTempStr = this.textBoxHotWaterEnteringWaterTemp.Text;
                flowRateStr = this.textBoxHotWaterFlowRate.Text;        
            }
            else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 9)
            {
                heatingTypeStr = "DualFuel(HotWater/Elec)";
                heatTypeStr = "DualFuel(HotWater/Elec)";

                maxHeatingInputBTUStr = this.textBoxNewOAUElecHeatingInputElec.Text;
                secHeatingInputStr = this.txtNewOAUElecSecHtgInput.Text;
                enteringTempStr = this.textBoxHotWaterEnteringWaterTemp.Text;
                flowRateStr = this.textBoxHotWaterFlowRate.Text;        
                //maxOutAirTempStr = "32 Deg C";
                if (secHeatingInputStr != "")
                {
                    maxHeatingInputBTUStr = Convert.ToDouble(secHeatingInputStr).ToString("f2");
                }
            }
            else if (this.comboBoxNewOAUHeatingType.SelectedIndex == 10)
            {
                heatingTypeStr = "NoHeat";
                heatTypeStr = "NoHeat";

                maxHeatingInputBTUStr = this.textBoxNewOAUElecHeatingInputElec.Text;
                secHeatingInputStr = this.txtNewOAUElecSecHtgInput.Text;
                //enteringTempStr = this.textBoxHotWaterEnteringWaterTemp.Text;
                //flowRateStr = this.textBoxHotWaterFlowRate.Text;
                //maxOutAirTempStr = "32 Deg C";
                if (secHeatingInputStr != "")
                {
                    maxHeatingInputBTUStr = Convert.ToDouble(secHeatingInputStr).ToString("f2");
                }
                if (modelNo.Substring(13, 1) == "3" || modelNo.Substring(13, 1) == "8")
                {
                    fuelTypeStr = "WaterSource";
                }
            }

            if (modelNo.Substring(13, 1) == "3" || modelNo.Substring(13, 1) == "8")
            {
                if (radioButtonGlycol.Checked == false && radioButtonWater.Checked == false)
                {
                    MessageBox.Show("WARNING: No fluid type has been selected.");
                }
                else if (radioButtonGlycol.Checked == true)
                {
                    fluidType_CutoutTempStr = "Glycol";
                }
                else
                {
                    fluidType_CutoutTempStr = "Water";
                }
            }

            mfgDateDate = this.dateTimePickerOAUMfgDate.Value;
            mfgDateStr = mfgDateDate.Month.ToString() + "/" + mfgDateDate.Year.ToString();

            warehouseCode = this.cbWhseCode.Text.Substring(0,4);

            if (checkBoxNewOAUDPP.Checked == true)
            {
                dualPointPower = 1;
            }
            else
            {
                dualPointPower = 0;
            }          
                                             
            if ((modelNo.Substring(2, 1) == "B" || modelNo.Substring(2, 1) == "D" || modelNo.Substring(2, 1) == "G" ||modelNo.Substring(2, 1) == "K" || modelNo.Substring(2, 1) == "N" ) &&                    
                     (modelNo.Substring(13, 1) == "3" || modelNo.Substring(13, 1) == "8")  &&
                     (modelNo.Substring(19, 1) == "0" || modelNo.Substring(19, 1) == "A" || modelNo.Substring(19, 1) == "C" || modelNo.Substring(19, 1) == "D" || modelNo.Substring(19, 1) == "G" ||
                      modelNo.Substring(19, 1) == "H" || modelNo.Substring(19, 1) == "J" || modelNo.Substring(19, 1) == "K" || modelNo.Substring(19, 1) == "L" || modelNo.Substring(19, 1) == "N" ||
                      modelNo.Substring(19, 1) == "Q" || modelNo.Substring(19, 1) == "S"))
            {
                unitManual = "OAU-SVX004B-EN";
            }
            else if ((modelNo.Substring(2, 1) == "G") &&
                     (modelNo.Substring(13, 1) == "0" ||  modelNo.Substring(13, 1) == "1" || modelNo.Substring(13, 1) == "2" || modelNo.Substring(13, 1) == "4" ||
                      modelNo.Substring(13, 1) == "5" || modelNo.Substring(13, 1) == "6" || modelNo.Substring(13, 1) == "7") &&
                     (modelNo.Substring(19, 1) == "B" || modelNo.Substring(19, 1) == "E" || modelNo.Substring(19, 1) == "F" || modelNo.Substring(19, 1) == "P"))
            {
                unitManual = "OAU-SVX005A-EN";
            }
            else if ((modelNo.Substring(2, 1) == "B" || modelNo.Substring(2, 1) == "G") &&
                    (modelNo.Substring(13, 1) == "0" || modelNo.Substring(13, 1) == "1" || modelNo.Substring(13, 1) == "2" || modelNo.Substring(13, 1) == "4" ||
                     modelNo.Substring(13, 1) == "5" || modelNo.Substring(13, 1) == "6" || modelNo.Substring(13, 1) == "7") &&
                    (modelNo.Substring(19, 1) == "0" || modelNo.Substring(19, 1) == "A" || modelNo.Substring(19, 1) == "C" || modelNo.Substring(19, 1) == "D" ||
                     modelNo.Substring(19, 1) == "G" || modelNo.Substring(19, 1) == "H" || modelNo.Substring(19, 1) == "J" || modelNo.Substring(19, 1) == "K" ||
                     modelNo.Substring(19, 1) == "L" || modelNo.Substring(19, 1) == "N" || modelNo.Substring(19, 1) == "Q" || modelNo.Substring(19, 1) == "S"))
            {
                unitManual = "OAU-SVX02C-EN";
            }
            else  if (modelNo.Substring(2, 1) == "D" || modelNo.Substring(2, 1) == "K" || modelNo.Substring(2, 1) == "N")
            {
                unitManual = "OAU-SVX01F-EN";
            }
            else if (modelNo.Substring(0, 2) == "HA")
            {
                unitManual = "RTSXX064A-XX";
            }

            if (this.txtComp1Qty.Text != "" && Int32.Parse(this.txtComp1Qty.Text) > 0)
            {
                comp1QtyStr = this.txtComp1Qty.Text;
                comp1PhaseStr = this.txtComp1Phase.Text;
                comp1RLA_VoltsStr = this.txtComp1RLA_Volts.Text;
                comp1LRAStr = this.txtComp1LRA.Text;
                comp1CirPos = "1-1";
            }

            if (this.txtComp2Qty.Text != "" && Int32.Parse(this.txtComp2Qty.Text) > 0)
            {
                comp2QtyStr = this.txtComp2Qty.Text;
                comp2PhaseStr = this.txtComp2Phase.Text;
                comp2RLA_VoltsStr = this.txtComp2RLA_Volts.Text;
                comp2LRAStr = this.txtComp2LRA.Text;
                comp2CirPos = "1-2";
            }
            else
            {
                cirPosComplete = true;

                if (this.txtComp4Qty.Text != "" && Int32.Parse(this.txtComp4Qty.Text) > 0)
                {
                    comp2QtyStr = this.txtComp4Qty.Text;
                    comp2PhaseStr = this.txtComp4Phase.Text;
                    comp2RLA_VoltsStr = this.txtComp4RLA_Volts.Text;
                    comp2LRAStr = this.txtComp4LRA.Text;
                    comp2CirPos = "2-1";
                }

                if (this.txtComp5Qty.Text != "" && Int32.Parse(this.txtComp5Qty.Text) > 0)
                {
                    comp3QtyStr = this.txtComp5Qty.Text;
                    comp3PhaseStr = this.txtComp5Phase.Text;
                    comp3RLA_VoltsStr = this.txtComp5RLA_Volts.Text;
                    comp3LRAStr = this.txtComp5LRA.Text;
                    comp3CirPos = "2-2";
                }

                if (this.txtComp6Qty.Text != "" && Int32.Parse(this.txtComp6Qty.Text) > 0)
                {
                    comp4QtyStr = this.txtComp6Qty.Text;
                    comp4PhaseStr = this.txtComp6Phase.Text;
                    comp4RLA_VoltsStr = this.txtComp6RLA_Volts.Text;
                    comp4LRAStr = this.txtComp6LRA.Text;
                    comp4CirPos = "2-3";
                }

            }

            if (this.txtComp3Qty.Text != "" && Int32.Parse(this.txtComp3Qty.Text) > 0)
            {
                comp3QtyStr = this.txtComp3Qty.Text;
                comp3PhaseStr = this.txtComp3Phase.Text;
                comp3RLA_VoltsStr = this.txtComp3RLA_Volts.Text;
                comp3LRAStr = this.txtComp3LRA.Text;
                comp3CirPos = "1-3";
            }
            else
            {
                if (cirPosComplete == false)
                {
                    if (this.txtComp4Qty.Text != "" && Int32.Parse(this.txtComp4Qty.Text) > 0)
                    {
                        comp3QtyStr = this.txtComp4Qty.Text;
                        comp3PhaseStr = this.txtComp4Phase.Text;
                        comp3RLA_VoltsStr = this.txtComp4RLA_Volts.Text;
                        comp3LRAStr = this.txtComp4LRA.Text;
                        comp3CirPos = "2-1";
                    }

                    if (this.txtComp5Qty.Text != "" && Int32.Parse(this.txtComp5Qty.Text) > 0)
                    {
                        comp4QtyStr = this.txtComp5Qty.Text;
                        comp4PhaseStr = this.txtComp5Phase.Text;
                        comp4RLA_VoltsStr = this.txtComp5RLA_Volts.Text;
                        comp4LRAStr = this.txtComp5LRA.Text;
                        comp4CirPos = "2-2";
                    }

                    if (this.txtComp6Qty.Text != "" && Int32.Parse(this.txtComp6Qty.Text) > 0)
                    {
                        comp5QtyStr = this.txtComp6Qty.Text;
                        comp5PhaseStr = this.txtComp6Phase.Text;
                        comp5RLA_VoltsStr = this.txtComp6RLA_Volts.Text;
                        comp5LRAStr = this.txtComp6LRA.Text;
                        comp5CirPos = "2-3";
                    }
                }
            }

            if ((this.txtComp1Qty.Text != "" && Int32.Parse(this.txtComp1Qty.Text) > 0) && 
                (this.txtComp2Qty.Text != "" && Int32.Parse(this.txtComp2Qty.Text) > 0) && 
                (this.txtComp3Qty.Text != ""  && Int32.Parse(this.txtComp3Qty.Text) > 0))
            {
                if (this.txtComp4Qty.Text != "" && Int32.Parse(this.txtComp4Qty.Text) > 0)
                {
                    comp4QtyStr = this.txtComp4Qty.Text;
                    comp4PhaseStr = this.txtComp4Phase.Text;
                    comp4RLA_VoltsStr = this.txtComp4RLA_Volts.Text;
                    comp4LRAStr = this.txtComp4LRA.Text;
                    comp4CirPos = "2-1";
                }
                if (this.txtComp5Qty.Text != "" && Int32.Parse(this.txtComp5Qty.Text) > 0)
                {
                    comp5QtyStr = this.txtComp5Qty.Text;
                    comp5PhaseStr = this.txtComp5Phase.Text;
                    comp5RLA_VoltsStr = this.txtComp5RLA_Volts.Text;
                    comp5LRAStr = this.txtComp5LRA.Text;
                    comp5CirPos = "2-2";
                }
                if (this.txtComp6Qty.Text != "" && Int32.Parse(this.txtComp6Qty.Text) > 0)
                {
                    comp6QtyStr = this.txtComp6Qty.Text;
                    comp6PhaseStr = this.txtComp6Phase.Text;
                    comp6RLA_VoltsStr = this.txtComp6RLA_Volts.Text;
                    comp6LRAStr = this.txtComp6LRA.Text;
                    comp6CirPos = "2-3";
                }
            }

            if (this.txtCircuit1Charge.Text != "" && this.txtCircuit2Charge.Text != "")
            {
                if (comp2CirPos != "2-1" && comp3CirPos != "2-1" & comp4CirPos != "2-1")
                {
                    if (comp3CirPos == "")
                    {
                        comp2CirPos = "2-1";
                    }
                    else
                    {
                        comp3CirPos = "2-1";
                    }
                }
            }
                       
            VS_DataSet ds = new VS_DataSet();
            ds.OAUReportDataTable.Rows.Add(this.textBoxNewOAUOrderNum.Text,
                        this.textBoxNewOAULineItem.Text,
                        this.textBoxNewOAUReleaseNum.Text,
                        this.textBoxNewOAUModelNo.Text,
                        this.textBoxNewOAUSerialNo.Text,
                        this.textBoxNewOAUElectricalRating.Text,
                        this.textBoxNewOAUOperatingVolts.Text,
                        this.textBoxNewOAUMinCKTAmp.Text,
                        this.textBoxNewOAUMFSMCB.Text,
                        "N/A", //this.textBoxNewOAUMOP.Text,
                        this.txtSCCR.Text,
                        heatTypeStr,
                        this.textBoxNewOAUTestPressureHigh.Text,
                        this.textBoxNewOAUTestPressureLow.Text,
                        comp1QtyStr,
                        comp2QtyStr,
                        comp3QtyStr,
                        comp4QtyStr,
                        comp1PhaseStr,
                        comp2PhaseStr,
                        comp3PhaseStr,
                        comp4PhaseStr,
                        comp1RLA_VoltsStr,
                        comp2RLA_VoltsStr,
                        comp3RLA_VoltsStr,
                        comp4RLA_VoltsStr,
                        comp1LRAStr,
                        comp2LRAStr,
                        comp3LRAStr,
                        comp4LRAStr,
                        this.txtCircuit1Charge.Text,
                        this.txtCircuit2Charge.Text,
                        this.textBoxNewOAUFanCondQty.Text,
                        this.textBoxNewOAUFanEvapQty.Text,
                        this.textBoxNewOAUFanEnergyQty.Text,
                        this.textBoxNewOAUFanPwrQty.Text,
                        this.textBoxNewOAUFanCondPH.Text,
                        this.textBoxNewOAUFanEvapPH.Text,
                        this.textBoxNewOAUFanEnergyPH.Text,
                        this.textBoxNewOAUFanPwrPH.Text,
                        this.textBoxNewOAUFanCondFLA.Text,
                        this.textBoxNewOAUFanEvapFLA.Text,
                        this.textBoxNewOAUFanEnergyFLA.Text,
                        this.textBoxNewOAUFanPwrFLA.Text,
                        this.textBoxNewOAUFanCondHP.Text,
                        this.textBoxNewOAUFanEvapHP.Text,
                        this.textBoxNewOAUFanEnergyHP.Text,
                        this.textBoxNewOAUFanPwrHP.Text,
                        flowRateStr,                         
                        operatingPressureStr,
                        maxHeatingInputBTUStr,                           // Max Htg Input BTU
                        heatOutputStr,                                   // Heating Output BTU
                        minHeatingInputBTUStr,                           // Min Htg Input BTU
                        this.textBoxNewOauIndFireMaxExt.Text,            // Max Ext
                        tempRise,                                        // Temp Rise
                        maxOutAirTempStr,                                // Max Outlet Air Temp
                        maxGasSupplyPressure,                            // Max Gas Supply Pressure
                        minGasSupplyPressure,                            // Min Gas Supply Pressure
                        manifoldPressure,                                // Manifold Pressure
                        maxPressureDrop,                                 // Direct Fired Max Pressure Drop
                        minPressureDrop,                                 // Direct Fired Min Pressure Drop
                        staticPressure,                                  // Direct Fired Static Pressure
                        "",  // Place Holder for future use.
                        fluidType_CutoutTempStr,  // Water or Glycol for Water Source or Cutout Temp for Direct Fired
                        fuelTypeStr,
                        mfgDateStr,                       
                        this.textBoxNewOAUMinCKTAmp2.Text,
                        this.textBoxNewOAUMFSMCB2.Text,
                        dualPointPower,
                        unitManual,
                        secHeatingInputStr,
                        "", // Place holder for future use
                        enteringTempStr,
                        warehouseCode,
                        comp1CirPos,
                        comp2CirPos,
                        comp3CirPos,
                        comp4CirPos,
                        comp5CirPos,
                        comp6CirPos,
                        comp5QtyStr,
                        comp6QtyStr,
                        comp5PhaseStr,
                        comp6PhaseStr,
                        comp5RLA_VoltsStr, 
                        comp6RLA_VoltsStr,
                        comp5LRAStr,
                        comp6LRAStr );           
            

            ReportDocument cryRpt = new ReportDocument();

            EpicorServer = ConfigurationManager.AppSettings["EpicorAppServer"];            

            if (modelNo.StartsWith("OA") == true)
            {
                reportString = @"\\" + EpicorServer + "\\Apps\\OAU\\EtlLabel\\reports\\OA_Label_v010.rpt";
//                //reportString = @"\\epicor905app\Apps\OAU\EtlLabel\reports\OA_Label_v010.rpt";                 
//                //reportString = @"\\KCCWVPEPIC9APP\Apps\OAU\EtlLabel\reports\OA_Label_test.rpt"; 
//                reportString = @"\\epicor905app\Apps\OAU\EtlLabel\reports\OA_Label_v010.rpt"; 
//#if DEBUG
//                reportString = @"\\KCCWVTEPIC9APP2\Apps\OAU\EtlLabel\reports\OA_Label_v010.rpt";   // Default Crystal report for Indirect, Direct, Electric & No Heat

//#endif
            }
            else // MAU Label
            {
                reportString = @"\\" + EpicorServer + "\\Apps\\OAU\\EtlLabel\\reports\\Viking_v001.rpt";
//                reportString = @"\\epicor905app\Apps\OAU\EtlLabel\reports\Viking_v001.rpt";
//#if DEBUG
//                reportString = @"\\KCCWVTEPIC9APP2\Apps\OAU\EtlLabel\reports\Viking_v001.rpt";   // Default Crystal report for Indirect, Direct, Electric & No Heat

//#endif

            }
           
            //reportString = @"\\epicor905app\Apps\OAU\EtlLabel\reports\OA_Label_v008_Glycol.rpt";  
            //if (heatingTypeStr == "Electric")
            //{               
            //    if (modelNo.Substring(13, 1) == "3" || modelNo.Substring(13, 1) == "8")
            //    {
            //        reportString = @"\\epicor905app\Apps\OAU\EtlLabel\reports\OA_Label_v007ElectricLineForWaterSource.rpt";
            //    }
            //}
            //else if (heatingTypeStr == "Steam")
            //{
            //    reportString = @"\\epicor905app\Apps\OAU\EtlLabel\reports\OA_Label_v006steam.rpt";
            //}
            //else if (heatingTypeStr == "DuelFuel(IF/Elec)")
            //{
            //    reportString = @"\\epicor905app\Apps\OAU\EtlLabel\reports\OA_Label_v007electricLineforgas.rpt";
            //}
            //else if (heatingTypeStr == "DuelFuel(Elec/Elec)")
            //{
            //    reportString = @"\\epicor905app\Apps\OAU\EtlLabel\reports\OA_Label_v006electricLineforelectrical.rpt";
            //}
            //else if (heatingTypeStr == "NoHeat")
            //{
            //    if (modelNo.Substring(13, 1) == "3" || modelNo.Substring(13, 1) == "8")
            //    {
            //        reportString = @"\\epicor905app\Apps\OAU\EtlLabel\reports\OA_Label_v006WaterSource.rpt";
            //    }
            //}
           
            cryRpt.Load(reportString);
            
            cryRpt.SetDataSource(ds.Tables["OAUReportDataTable"]);           

            frmPrint.crystalReportViewer2.ReportSource = cryRpt;
            
            frmPrint.ShowDialog();
            frmPrint.crystalReportViewer2.Refresh();


            if (checkBoxLangFrench.Checked == true)
            {
                if (modelNo.StartsWith("OA") == true)
                {
                    reportString = @"\\" + EpicorServer + "\\Apps\\OAU\\EtlLabel\\reports\\OA_Label_v010_French.rpt";
                    //reportString = @"\\epicor905app\Apps\OAU\EtlLabel\reports\OA_Label_v010_French.rpt"; 
                    //reportString = @"\\epicor905app\Apps\OAU\EtlLabel\reports\OA_Label_v011.rpt"; 
                    //reportString = @"\\KCCWVPEPIC9APP\Apps\OAU\EtlLabel\reports\OA_Label_v010_French.rpt";
//#if DEBUG
//                    reportString = @"\\KCCWVTEPIC9APP2\Apps\OAU\EtlLabel\reports\OA_Label_v010_French.rpt";   // Default Crystal report for Indirect, Direct, Electric & No Heat

//#endif
                }
                else // MAU Label
                {
                    reportString = @"\\" + EpicorServer + "\\Apps\\OAU\\EtlLabel\\reports\\Viking_v001_French.rpt";
                    //reportString = @"\\epicor905app\Apps\OAU\EtlLabel\reports\OA_Label_v010.rpt";
                    //reportString = @"\\epicor905app\Apps\OAU\EtlLabel\reports\OA_Label_test.rpt";
//                    reportString = @"\\KCCWVPEPIC9APP\Apps\OAU\EtlLabel\reports\Viking_v001_French.rpt";                    
//#if DEBUG
//                    reportString = @"\\KCCWVTEPIC9APP2\Apps\OAU\EtlLabel\reports\Viking_v001_French.rpt";   // Default Crystal report for Indirect, Direct, Electric & No Heat

//#endif
                }
                cryRpt.Load(reportString);

                cryRpt.SetDataSource(ds.Tables["OAUReportDataTable"]);

                frmPrint.crystalReportViewer2.ReportSource = cryRpt;

                frmPrint.ShowDialog();
                frmPrint.crystalReportViewer2.Refresh(); 
            }
                      
        }

        private void textBoxNewOAUComp1RLAVolts_Leave(object sender, EventArgs e)
        {
            string tmpStr;
            string numStr;

            tmpStr = this.txtComp1RLA_Volts.Text.Trim();
            if (tmpStr.Length > 0)
            {
                if (tmpStr.IndexOf('-') == -1)
                {
                    numStr = evalNumAppendZero(tmpStr);
                    tmpStr = tmpStr.Substring(0, tmpStr.Length);                    
                }
                else
                {
                    tmpStr = tmpStr.Substring(0, tmpStr.IndexOf('-'));
                    numStr = evalNumAppendZero(tmpStr);
                }
                numStr = numStr + "-" + this.comboBoxNewOAUVoltage.SelectedItem.ToString();
                this.txtComp1RLA_Volts.Text = numStr;
            }
        }        

        private void textBoxNewOAUComp2RLAVolts_Leave(object sender, EventArgs e)
        {
            string tmpStr;
            string numStr;

            tmpStr = this.txtComp2RLA_Volts.Text.Trim();
            if (tmpStr.Length > 0)
            {
                if (tmpStr.IndexOf('-') == -1)
                {
                    numStr = evalNumAppendZero(tmpStr);
                    tmpStr = tmpStr.Substring(0, tmpStr.Length);
                }
                else
                {
                    tmpStr = tmpStr.Substring(0, tmpStr.IndexOf('-'));
                    numStr = evalNumAppendZero(tmpStr);
                }
                numStr = numStr + "-" + this.comboBoxNewOAUVoltage.SelectedItem.ToString();
                this.txtComp2RLA_Volts.Text = numStr;
            }
        }

        private void textBoxNewOAUComp3RLAVolts_Leave(object sender, EventArgs e)
        {
            string tmpStr;
            string numStr;

            tmpStr = this.txtComp3RLA_Volts.Text.Trim();
            if (tmpStr.Length > 0)
            {
                if (tmpStr.IndexOf('-') == -1)
                {
                    numStr = evalNumAppendZero(tmpStr);
                    tmpStr = tmpStr.Substring(0, tmpStr.Length);
                }
                else
                {
                    tmpStr = tmpStr.Substring(0, tmpStr.IndexOf('-'));
                    numStr = evalNumAppendZero(tmpStr);
                }
                numStr = numStr + "-" + this.comboBoxNewOAUVoltage.SelectedItem.ToString();
                this.txtComp3RLA_Volts.Text = numStr;
            }
        }

        private void textBoxNewOAUComp4RLAVolts_Leave(object sender, EventArgs e)
        {
            string tmpStr;
            string numStr;

            tmpStr = this.txtComp4RLA_Volts.Text.Trim();
            if (tmpStr.Length > 0)
            {
                if (tmpStr.IndexOf('-') == -1)
                {
                    numStr = evalNumAppendZero(tmpStr);
                    tmpStr = tmpStr.Substring(0, tmpStr.Length);
                }
                else
                {
                    tmpStr = tmpStr.Substring(0, tmpStr.IndexOf('-'));
                    numStr = evalNumAppendZero(tmpStr);
                }
                numStr = numStr + "-" + this.comboBoxNewOAUVoltage.SelectedItem.ToString();
                this.txtComp4RLA_Volts.Text = numStr;
            }
        }

        private string evalNumAppendZero(string numStr)
        {
            string retNumStr = "";

            int dotPosInt;

            if (numStr.IndexOf('.') == -1)
            {
                retNumStr = numStr + ".0";
            }
            else
            {
                dotPosInt = numStr.IndexOf('.');
                if (dotPosInt == (numStr.Length - 1))
                {
                    retNumStr = numStr + "0";
                }
                else
                {
                    retNumStr = numStr;
                }
            }
            return retNumStr;
        }

        private void textBoxNewOAUModelNo_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.groupBoxParseModelNo.Location = new Point(6, 152);
            this.groupBoxParseModelNo.Visible = true;
            this.labelNewOAUMsg.Text = "Please enter Model No and press the Parse button.";
            this.buttonNewOAUAccept.Enabled = false;
            this.buttonNewOAUCancel.Enabled = false;
        }

        private void buttonModelNoParse_Click(object sender, EventArgs e)
        {
            string modelNoStr;

            modelNoStr = this.textBoxModifyModelNo.Text;            
            modelNoStr = modelNoStr.ToUpper();
            this.textBoxModifyModelNo.Text = modelNoStr;

            if (modelNoStr.Length == 29)
            {
                GlobalFunctions.parse29DigitModelNo(modelNoStr, this);
                this.textBoxNewOAUModelNo.Text = this.textBoxModifyModelNo.Text;
                this.groupBoxParseModelNo.Visible = false;
                this.labelNewOAUMsg.Text = "Enter respective info, then Press Accept Button to Verify OAU Sticker data.";
                this.buttonNewOAUAccept.Enabled = true;
                this.buttonNewOAUCancel.Enabled = true;
            }
            else if (modelNoStr.Length == 39)
            {
                GlobalFunctions.parse39DigitModelNo(modelNoStr, this);
                this.textBoxNewOAUModelNo.Text = this.textBoxModifyModelNo.Text;
                this.groupBoxParseModelNo.Visible = false;
                this.labelNewOAUMsg.Text = "Enter respective info, then Press Accept Button to Verify OAU Sticker data.";
                this.buttonNewOAUAccept.Enabled = true;
                this.buttonNewOAUCancel.Enabled = true;

            }
            else
            {
                MessageBox.Show("ERROR - Invalid ModelNo, (" + modelNoStr.Length.ToString() + " char entered). " +
                                "ModelNo can be 29 or 39 characters in length.");
            }
            
        }

        private void buttonModelNoCancel_Click(object sender, EventArgs e)
        {
            this.groupBoxParseModelNo.Visible = false;
            this.buttonNewOAUAccept.Enabled = true;
            this.buttonNewOAUCancel.Enabled = true;
        }

        private void buttonModelNoEdit_Click(object sender, EventArgs e)
        {
            this.textBoxNewOAUModelNo.Text = this.textBoxModifyModelNo.Text;
            this.groupBoxParseModelNo.Visible = false;
            this.buttonNewOAUAccept.Enabled = true;
            this.buttonNewOAUCancel.Enabled = true;
            this.labelNewOAUMsg.Text = "Enter respective info, then Press Accept Button to Verify OAU Sticker data.";
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }


        private void checkBoxNewOAUDPP_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxNewOAUDPP.Checked == true)
            {
                labelNewOAUDPP2.ForeColor = Color.Blue;

                textBoxNewOAUMinCKTAmp2.BackColor = Color.White;
                textBoxNewOAUMFSMCB2.BackColor = Color.White;

                textBoxNewOAUMinCKTAmp2.ReadOnly = false;
                textBoxNewOAUMFSMCB2.ReadOnly = false;
            }
            else
            {
                labelNewOAUDPP2.ForeColor = Color.Pink;

                textBoxNewOAUMinCKTAmp2.BackColor = Color.Pink;
                textBoxNewOAUMFSMCB2.BackColor = Color.Pink;

                textBoxNewOAUMinCKTAmp2.ReadOnly = true;
                textBoxNewOAUMFSMCB2.ReadOnly = true;
            }
        }       
    }
}
        