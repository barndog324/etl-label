namespace KCC.OA.Etl
{
    partial class frmNewLinePCO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStripNewPCO = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBoxNewPCOJobInfo = new System.Windows.Forms.GroupBox();
            this.textBoxNewPCOEndConsumer = new System.Windows.Forms.TextBox();
            this.labelNewPCOEndConsumer = new System.Windows.Forms.Label();
            this.textBoxNewPCOPONum = new System.Windows.Forms.TextBox();
            this.labelNewPCOPONum = new System.Windows.Forms.Label();
            this.labelNewPCOJobNo = new System.Windows.Forms.Label();
            this.textBoxNewPCOReleaseNum = new System.Windows.Forms.TextBox();
            this.textBoxNewPCOJobNo = new System.Windows.Forms.TextBox();
            this.labelNewPCOSlash = new System.Windows.Forms.Label();
            this.textBoxNewPCOCustomer = new System.Windows.Forms.TextBox();
            this.textBoxNewPCOOrderNum = new System.Windows.Forms.TextBox();
            this.labelNewPCOCustomer = new System.Windows.Forms.Label();
            this.labelNewPCOOrderNum = new System.Windows.Forms.Label();
            this.textBoxNewPCOLineItem = new System.Windows.Forms.TextBox();
            this.labelNewPCOLineItem = new System.Windows.Forms.Label();
            this.groupBoxNewPCOUnitInfo = new System.Windows.Forms.GroupBox();
            this.textBoxNewPCOBulbPartNum = new System.Windows.Forms.TextBox();
            this.labelNewPCOBulbPartNum = new System.Windows.Forms.Label();
            this.textBoxNewPCODateOfMfg = new System.Windows.Forms.TextBox();
            this.labelNewPCODateOfMfg = new System.Windows.Forms.Label();
            this.textBoxNewPCOTag = new System.Windows.Forms.TextBox();
            this.labelNewPCOTag = new System.Windows.Forms.Label();
            this.textBoxNewPCOModelNo = new System.Windows.Forms.TextBox();
            this.labelNewPCOModelNum = new System.Windows.Forms.Label();
            this.labelNewPCOMsg = new System.Windows.Forms.Label();
            this.buttonNewPCOPrint = new System.Windows.Forms.Button();
            this.buttonNewPCOCancel = new System.Windows.Forms.Button();
            this.buttonNewPCOAccept = new System.Windows.Forms.Button();
            this.menuStripNewPCO.SuspendLayout();
            this.groupBoxNewPCOJobInfo.SuspendLayout();
            this.groupBoxNewPCOUnitInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStripNewPCO
            // 
            this.menuStripNewPCO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.menuStripNewPCO.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStripNewPCO.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStripNewPCO.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStripNewPCO.Location = new System.Drawing.Point(0, 0);
            this.menuStripNewPCO.Name = "menuStripNewPCO";
            this.menuStripNewPCO.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStripNewPCO.Size = new System.Drawing.Size(593, 29);
            this.menuStripNewPCO.TabIndex = 3;
            this.menuStripNewPCO.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(52, 25);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(112, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // groupBoxNewPCOJobInfo
            // 
            this.groupBoxNewPCOJobInfo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBoxNewPCOJobInfo.Controls.Add(this.textBoxNewPCOEndConsumer);
            this.groupBoxNewPCOJobInfo.Controls.Add(this.labelNewPCOEndConsumer);
            this.groupBoxNewPCOJobInfo.Controls.Add(this.textBoxNewPCOPONum);
            this.groupBoxNewPCOJobInfo.Controls.Add(this.labelNewPCOPONum);
            this.groupBoxNewPCOJobInfo.Controls.Add(this.labelNewPCOJobNo);
            this.groupBoxNewPCOJobInfo.Controls.Add(this.textBoxNewPCOReleaseNum);
            this.groupBoxNewPCOJobInfo.Controls.Add(this.textBoxNewPCOJobNo);
            this.groupBoxNewPCOJobInfo.Controls.Add(this.labelNewPCOSlash);
            this.groupBoxNewPCOJobInfo.Controls.Add(this.textBoxNewPCOCustomer);
            this.groupBoxNewPCOJobInfo.Controls.Add(this.textBoxNewPCOOrderNum);
            this.groupBoxNewPCOJobInfo.Controls.Add(this.labelNewPCOCustomer);
            this.groupBoxNewPCOJobInfo.Controls.Add(this.labelNewPCOOrderNum);
            this.groupBoxNewPCOJobInfo.Controls.Add(this.textBoxNewPCOLineItem);
            this.groupBoxNewPCOJobInfo.Controls.Add(this.labelNewPCOLineItem);
            this.groupBoxNewPCOJobInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewPCOJobInfo.Location = new System.Drawing.Point(16, 46);
            this.groupBoxNewPCOJobInfo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxNewPCOJobInfo.Name = "groupBoxNewPCOJobInfo";
            this.groupBoxNewPCOJobInfo.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxNewPCOJobInfo.Size = new System.Drawing.Size(420, 198);
            this.groupBoxNewPCOJobInfo.TabIndex = 1;
            this.groupBoxNewPCOJobInfo.TabStop = false;
            this.groupBoxNewPCOJobInfo.Text = "Order Info";
            // 
            // textBoxNewPCOEndConsumer
            // 
            this.textBoxNewPCOEndConsumer.BackColor = System.Drawing.Color.White;
            this.textBoxNewPCOEndConsumer.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewPCOEndConsumer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewPCOEndConsumer.Location = new System.Drawing.Point(127, 123);
            this.textBoxNewPCOEndConsumer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxNewPCOEndConsumer.Name = "textBoxNewPCOEndConsumer";
            this.textBoxNewPCOEndConsumer.Size = new System.Drawing.Size(279, 26);
            this.textBoxNewPCOEndConsumer.TabIndex = 10;
            // 
            // labelNewPCOEndConsumer
            // 
            this.labelNewPCOEndConsumer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewPCOEndConsumer.Location = new System.Drawing.Point(1, 123);
            this.labelNewPCOEndConsumer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNewPCOEndConsumer.Name = "labelNewPCOEndConsumer";
            this.labelNewPCOEndConsumer.Size = new System.Drawing.Size(120, 26);
            this.labelNewPCOEndConsumer.TabIndex = 9;
            this.labelNewPCOEndConsumer.Text = "End Consumer:";
            this.labelNewPCOEndConsumer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewPCOPONum
            // 
            this.textBoxNewPCOPONum.BackColor = System.Drawing.Color.White;
            this.textBoxNewPCOPONum.Enabled = false;
            this.textBoxNewPCOPONum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewPCOPONum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewPCOPONum.Location = new System.Drawing.Point(127, 156);
            this.textBoxNewPCOPONum.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxNewPCOPONum.Name = "textBoxNewPCOPONum";
            this.textBoxNewPCOPONum.Size = new System.Drawing.Size(112, 26);
            this.textBoxNewPCOPONum.TabIndex = 5;
            this.textBoxNewPCOPONum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelNewPCOPONum
            // 
            this.labelNewPCOPONum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewPCOPONum.Location = new System.Drawing.Point(5, 156);
            this.labelNewPCOPONum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNewPCOPONum.Name = "labelNewPCOPONum";
            this.labelNewPCOPONum.Size = new System.Drawing.Size(113, 26);
            this.labelNewPCOPONum.TabIndex = 5;
            this.labelNewPCOPONum.Text = "P.O.#:";
            this.labelNewPCOPONum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelNewPCOJobNo
            // 
            this.labelNewPCOJobNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewPCOJobNo.Location = new System.Drawing.Point(244, 156);
            this.labelNewPCOJobNo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNewPCOJobNo.Name = "labelNewPCOJobNo";
            this.labelNewPCOJobNo.Size = new System.Drawing.Size(63, 26);
            this.labelNewPCOJobNo.TabIndex = 7;
            this.labelNewPCOJobNo.Text = "Job No:";
            this.labelNewPCOJobNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewPCOReleaseNum
            // 
            this.textBoxNewPCOReleaseNum.BackColor = System.Drawing.Color.White;
            this.textBoxNewPCOReleaseNum.Enabled = false;
            this.textBoxNewPCOReleaseNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewPCOReleaseNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewPCOReleaseNum.Location = new System.Drawing.Point(199, 57);
            this.textBoxNewPCOReleaseNum.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxNewPCOReleaseNum.Name = "textBoxNewPCOReleaseNum";
            this.textBoxNewPCOReleaseNum.Size = new System.Drawing.Size(40, 26);
            this.textBoxNewPCOReleaseNum.TabIndex = 4;
            this.textBoxNewPCOReleaseNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxNewPCOJobNo
            // 
            this.textBoxNewPCOJobNo.BackColor = System.Drawing.Color.White;
            this.textBoxNewPCOJobNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewPCOJobNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewPCOJobNo.Location = new System.Drawing.Point(312, 156);
            this.textBoxNewPCOJobNo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxNewPCOJobNo.Name = "textBoxNewPCOJobNo";
            this.textBoxNewPCOJobNo.Size = new System.Drawing.Size(92, 26);
            this.textBoxNewPCOJobNo.TabIndex = 6;
            // 
            // labelNewPCOSlash
            // 
            this.labelNewPCOSlash.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewPCOSlash.Location = new System.Drawing.Point(176, 55);
            this.labelNewPCOSlash.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNewPCOSlash.Name = "labelNewPCOSlash";
            this.labelNewPCOSlash.Size = new System.Drawing.Size(20, 26);
            this.labelNewPCOSlash.TabIndex = 4;
            this.labelNewPCOSlash.Text = "/";
            this.labelNewPCOSlash.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxNewPCOCustomer
            // 
            this.textBoxNewPCOCustomer.BackColor = System.Drawing.Color.White;
            this.textBoxNewPCOCustomer.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewPCOCustomer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewPCOCustomer.Location = new System.Drawing.Point(127, 90);
            this.textBoxNewPCOCustomer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxNewPCOCustomer.Name = "textBoxNewPCOCustomer";
            this.textBoxNewPCOCustomer.Size = new System.Drawing.Size(279, 26);
            this.textBoxNewPCOCustomer.TabIndex = 2;
            // 
            // textBoxNewPCOOrderNum
            // 
            this.textBoxNewPCOOrderNum.BackColor = System.Drawing.Color.White;
            this.textBoxNewPCOOrderNum.Enabled = false;
            this.textBoxNewPCOOrderNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewPCOOrderNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewPCOOrderNum.Location = new System.Drawing.Point(127, 23);
            this.textBoxNewPCOOrderNum.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxNewPCOOrderNum.Name = "textBoxNewPCOOrderNum";
            this.textBoxNewPCOOrderNum.Size = new System.Drawing.Size(85, 26);
            this.textBoxNewPCOOrderNum.TabIndex = 1;
            this.textBoxNewPCOOrderNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelNewPCOCustomer
            // 
            this.labelNewPCOCustomer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewPCOCustomer.Location = new System.Drawing.Point(5, 90);
            this.labelNewPCOCustomer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNewPCOCustomer.Name = "labelNewPCOCustomer";
            this.labelNewPCOCustomer.Size = new System.Drawing.Size(113, 26);
            this.labelNewPCOCustomer.TabIndex = 2;
            this.labelNewPCOCustomer.Text = "Customer:";
            this.labelNewPCOCustomer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelNewPCOOrderNum
            // 
            this.labelNewPCOOrderNum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewPCOOrderNum.Location = new System.Drawing.Point(5, 25);
            this.labelNewPCOOrderNum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNewPCOOrderNum.Name = "labelNewPCOOrderNum";
            this.labelNewPCOOrderNum.Size = new System.Drawing.Size(113, 26);
            this.labelNewPCOOrderNum.TabIndex = 1;
            this.labelNewPCOOrderNum.Text = "Sales Order#:";
            this.labelNewPCOOrderNum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxNewPCOLineItem
            // 
            this.textBoxNewPCOLineItem.BackColor = System.Drawing.Color.White;
            this.textBoxNewPCOLineItem.Enabled = false;
            this.textBoxNewPCOLineItem.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewPCOLineItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewPCOLineItem.Location = new System.Drawing.Point(127, 57);
            this.textBoxNewPCOLineItem.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxNewPCOLineItem.Name = "textBoxNewPCOLineItem";
            this.textBoxNewPCOLineItem.Size = new System.Drawing.Size(40, 26);
            this.textBoxNewPCOLineItem.TabIndex = 3;
            this.textBoxNewPCOLineItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewPCOLineItem
            // 
            this.labelNewPCOLineItem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewPCOLineItem.Location = new System.Drawing.Point(5, 57);
            this.labelNewPCOLineItem.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNewPCOLineItem.Name = "labelNewPCOLineItem";
            this.labelNewPCOLineItem.Size = new System.Drawing.Size(113, 26);
            this.labelNewPCOLineItem.TabIndex = 2;
            this.labelNewPCOLineItem.Text = "Line #/Rel #:";
            this.labelNewPCOLineItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxNewPCOUnitInfo
            // 
            this.groupBoxNewPCOUnitInfo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBoxNewPCOUnitInfo.Controls.Add(this.textBoxNewPCOBulbPartNum);
            this.groupBoxNewPCOUnitInfo.Controls.Add(this.labelNewPCOBulbPartNum);
            this.groupBoxNewPCOUnitInfo.Controls.Add(this.textBoxNewPCODateOfMfg);
            this.groupBoxNewPCOUnitInfo.Controls.Add(this.labelNewPCODateOfMfg);
            this.groupBoxNewPCOUnitInfo.Controls.Add(this.textBoxNewPCOTag);
            this.groupBoxNewPCOUnitInfo.Controls.Add(this.labelNewPCOTag);
            this.groupBoxNewPCOUnitInfo.Controls.Add(this.textBoxNewPCOModelNo);
            this.groupBoxNewPCOUnitInfo.Controls.Add(this.labelNewPCOModelNum);
            this.groupBoxNewPCOUnitInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewPCOUnitInfo.Location = new System.Drawing.Point(16, 254);
            this.groupBoxNewPCOUnitInfo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxNewPCOUnitInfo.Name = "groupBoxNewPCOUnitInfo";
            this.groupBoxNewPCOUnitInfo.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxNewPCOUnitInfo.Size = new System.Drawing.Size(560, 134);
            this.groupBoxNewPCOUnitInfo.TabIndex = 2;
            this.groupBoxNewPCOUnitInfo.TabStop = false;
            this.groupBoxNewPCOUnitInfo.Text = "Unit Info";
            // 
            // textBoxNewPCOBulbPartNum
            // 
            this.textBoxNewPCOBulbPartNum.BackColor = System.Drawing.Color.White;
            this.textBoxNewPCOBulbPartNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewPCOBulbPartNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewPCOBulbPartNum.Location = new System.Drawing.Point(133, 90);
            this.textBoxNewPCOBulbPartNum.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxNewPCOBulbPartNum.Name = "textBoxNewPCOBulbPartNum";
            this.textBoxNewPCOBulbPartNum.Size = new System.Drawing.Size(136, 26);
            this.textBoxNewPCOBulbPartNum.TabIndex = 4;
            // 
            // labelNewPCOBulbPartNum
            // 
            this.labelNewPCOBulbPartNum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewPCOBulbPartNum.Location = new System.Drawing.Point(9, 90);
            this.labelNewPCOBulbPartNum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNewPCOBulbPartNum.Name = "labelNewPCOBulbPartNum";
            this.labelNewPCOBulbPartNum.Size = new System.Drawing.Size(113, 26);
            this.labelNewPCOBulbPartNum.TabIndex = 69;
            this.labelNewPCOBulbPartNum.Text = "Bulb Part No:";
            this.labelNewPCOBulbPartNum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewPCODateOfMfg
            // 
            this.textBoxNewPCODateOfMfg.BackColor = System.Drawing.Color.White;
            this.textBoxNewPCODateOfMfg.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewPCODateOfMfg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewPCODateOfMfg.Location = new System.Drawing.Point(415, 22);
            this.textBoxNewPCODateOfMfg.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxNewPCODateOfMfg.Name = "textBoxNewPCODateOfMfg";
            this.textBoxNewPCODateOfMfg.Size = new System.Drawing.Size(136, 26);
            this.textBoxNewPCODateOfMfg.TabIndex = 2;
            this.textBoxNewPCODateOfMfg.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelNewPCODateOfMfg
            // 
            this.labelNewPCODateOfMfg.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewPCODateOfMfg.Location = new System.Drawing.Point(291, 22);
            this.labelNewPCODateOfMfg.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNewPCODateOfMfg.Name = "labelNewPCODateOfMfg";
            this.labelNewPCODateOfMfg.Size = new System.Drawing.Size(113, 26);
            this.labelNewPCODateOfMfg.TabIndex = 67;
            this.labelNewPCODateOfMfg.Text = "Date Of Mfg:";
            this.labelNewPCODateOfMfg.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewPCOTag
            // 
            this.textBoxNewPCOTag.BackColor = System.Drawing.Color.White;
            this.textBoxNewPCOTag.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewPCOTag.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewPCOTag.Location = new System.Drawing.Point(133, 57);
            this.textBoxNewPCOTag.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxNewPCOTag.Name = "textBoxNewPCOTag";
            this.textBoxNewPCOTag.Size = new System.Drawing.Size(417, 26);
            this.textBoxNewPCOTag.TabIndex = 3;
            // 
            // labelNewPCOTag
            // 
            this.labelNewPCOTag.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewPCOTag.Location = new System.Drawing.Point(9, 57);
            this.labelNewPCOTag.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNewPCOTag.Name = "labelNewPCOTag";
            this.labelNewPCOTag.Size = new System.Drawing.Size(113, 26);
            this.labelNewPCOTag.TabIndex = 8;
            this.labelNewPCOTag.Text = "Tag:";
            this.labelNewPCOTag.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewPCOModelNo
            // 
            this.textBoxNewPCOModelNo.BackColor = System.Drawing.Color.White;
            this.textBoxNewPCOModelNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewPCOModelNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewPCOModelNo.Location = new System.Drawing.Point(133, 23);
            this.textBoxNewPCOModelNo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxNewPCOModelNo.Name = "textBoxNewPCOModelNo";
            this.textBoxNewPCOModelNo.Size = new System.Drawing.Size(136, 26);
            this.textBoxNewPCOModelNo.TabIndex = 1;
            // 
            // labelNewPCOModelNum
            // 
            this.labelNewPCOModelNum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewPCOModelNum.Location = new System.Drawing.Point(9, 23);
            this.labelNewPCOModelNum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNewPCOModelNum.Name = "labelNewPCOModelNum";
            this.labelNewPCOModelNum.Size = new System.Drawing.Size(113, 26);
            this.labelNewPCOModelNum.TabIndex = 4;
            this.labelNewPCOModelNum.Text = "Model No:";
            this.labelNewPCOModelNum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelNewPCOMsg
            // 
            this.labelNewPCOMsg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.labelNewPCOMsg.Location = new System.Drawing.Point(0, 404);
            this.labelNewPCOMsg.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNewPCOMsg.Name = "labelNewPCOMsg";
            this.labelNewPCOMsg.Size = new System.Drawing.Size(593, 30);
            this.labelNewPCOMsg.TabIndex = 25;
            this.labelNewPCOMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonNewPCOPrint
            // 
            this.buttonNewPCOPrint.Enabled = false;
            this.buttonNewPCOPrint.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewPCOPrint.ForeColor = System.Drawing.Color.Blue;
            this.buttonNewPCOPrint.Location = new System.Drawing.Point(449, 180);
            this.buttonNewPCOPrint.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonNewPCOPrint.Name = "buttonNewPCOPrint";
            this.buttonNewPCOPrint.Size = new System.Drawing.Size(127, 46);
            this.buttonNewPCOPrint.TabIndex = 28;
            this.buttonNewPCOPrint.Text = "Print";
            this.buttonNewPCOPrint.UseVisualStyleBackColor = true;
            this.buttonNewPCOPrint.Click += new System.EventHandler(this.buttonNewPCOPrint_Click);
            // 
            // buttonNewPCOCancel
            // 
            this.buttonNewPCOCancel.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewPCOCancel.ForeColor = System.Drawing.Color.Red;
            this.buttonNewPCOCancel.Location = new System.Drawing.Point(449, 71);
            this.buttonNewPCOCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonNewPCOCancel.Name = "buttonNewPCOCancel";
            this.buttonNewPCOCancel.Size = new System.Drawing.Size(127, 46);
            this.buttonNewPCOCancel.TabIndex = 4;
            this.buttonNewPCOCancel.Text = "Cancel";
            this.buttonNewPCOCancel.UseVisualStyleBackColor = true;
            this.buttonNewPCOCancel.Click += new System.EventHandler(this.buttonNewPCOCancel_Click);
            // 
            // buttonNewPCOAccept
            // 
            this.buttonNewPCOAccept.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewPCOAccept.ForeColor = System.Drawing.Color.Blue;
            this.buttonNewPCOAccept.Location = new System.Drawing.Point(451, 126);
            this.buttonNewPCOAccept.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonNewPCOAccept.Name = "buttonNewPCOAccept";
            this.buttonNewPCOAccept.Size = new System.Drawing.Size(127, 46);
            this.buttonNewPCOAccept.TabIndex = 3;
            this.buttonNewPCOAccept.Text = "Accept";
            this.buttonNewPCOAccept.UseVisualStyleBackColor = true;
            this.buttonNewPCOAccept.Click += new System.EventHandler(this.buttonNewPCOAccept_Click);
            // 
            // frmNewLinePCO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(593, 432);
            this.Controls.Add(this.buttonNewPCOAccept);
            this.Controls.Add(this.buttonNewPCOPrint);
            this.Controls.Add(this.buttonNewPCOCancel);
            this.Controls.Add(this.labelNewPCOMsg);
            this.Controls.Add(this.groupBoxNewPCOUnitInfo);
            this.Controls.Add(this.groupBoxNewPCOJobInfo);
            this.Controls.Add(this.menuStripNewPCO);
            this.ForeColor = System.Drawing.Color.Black;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmNewLinePCO";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New PCO";
            this.menuStripNewPCO.ResumeLayout(false);
            this.menuStripNewPCO.PerformLayout();
            this.groupBoxNewPCOJobInfo.ResumeLayout(false);
            this.groupBoxNewPCOJobInfo.PerformLayout();
            this.groupBoxNewPCOUnitInfo.ResumeLayout(false);
            this.groupBoxNewPCOUnitInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStripNewPCO;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBoxNewPCOJobInfo;
        public System.Windows.Forms.TextBox textBoxNewPCOReleaseNum;
        private System.Windows.Forms.Label labelNewPCOSlash;
        public System.Windows.Forms.TextBox textBoxNewPCOCustomer;
        public System.Windows.Forms.TextBox textBoxNewPCOOrderNum;
        private System.Windows.Forms.Label labelNewPCOCustomer;
        private System.Windows.Forms.Label labelNewPCOOrderNum;
        public System.Windows.Forms.TextBox textBoxNewPCOLineItem;
        private System.Windows.Forms.Label labelNewPCOLineItem;
        public System.Windows.Forms.GroupBox groupBoxNewPCOUnitInfo;
        public System.Windows.Forms.TextBox textBoxNewPCOTag;
        private System.Windows.Forms.Label labelNewPCOTag;
        private System.Windows.Forms.Label labelNewPCOJobNo;
        public System.Windows.Forms.TextBox textBoxNewPCOJobNo;
        public System.Windows.Forms.TextBox textBoxNewPCOModelNo;
        private System.Windows.Forms.Label labelNewPCOModelNum;
        public System.Windows.Forms.Label labelNewPCOMsg;
        public System.Windows.Forms.Button buttonNewPCOPrint;
        public System.Windows.Forms.Button buttonNewPCOCancel;
        public System.Windows.Forms.TextBox textBoxNewPCOPONum;
        private System.Windows.Forms.Label labelNewPCOPONum;
        private System.Windows.Forms.Label labelNewPCODateOfMfg;
        public System.Windows.Forms.TextBox textBoxNewPCODateOfMfg;
        public System.Windows.Forms.TextBox textBoxNewPCOBulbPartNum;
        private System.Windows.Forms.Label labelNewPCOBulbPartNum;
        public System.Windows.Forms.Button buttonNewPCOAccept;
        public System.Windows.Forms.TextBox textBoxNewPCOEndConsumer;
        private System.Windows.Forms.Label labelNewPCOEndConsumer;

    }
}