using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;

namespace KCC.OA.Etl
{
    public partial class frmNewLinePCO : Form
    {
        public frmNewLinePCO()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonNewPCOPrint_Click(object sender, EventArgs e)
        {
            VS_DataSet ds = new VS_DataSet();

            frmPrintPCO frmPrint = new frmPrintPCO();

            ds.PCOReportDataTable.Rows.Add(
                    this.textBoxNewPCOOrderNum.Text,
                    this.textBoxNewPCOLineItem.Text,
                    this.textBoxNewPCOReleaseNum.Text,
                    this.textBoxNewPCOJobNo.Text,
                    this.textBoxNewPCOModelNo.Text,
                    this.textBoxNewPCOPONum.Text,
                    this.textBoxNewPCOTag.Text,
                    this.textBoxNewPCOCustomer.Text,
                    this.textBoxNewPCODateOfMfg.Text,
                    this.textBoxNewPCOBulbPartNum.Text);

            ReportDocument cryRpt = new ReportDocument();

            //string reportString = @"\\epicor905app\Apps\OAU\EtlLabel\reports\PCO_Adapter_Label_v001.rpt";
            string reportString = @"\\KCCWVPEPIC9APP\Apps\OAU\EtlLabel\reports\PCO_Adapter_Label_v001.rpt";

#if DEBUG
                reportString = @"\\KCCWVTEPIC9APP2\Apps\OAU\EtlLabel\reports\PCO_Adapter_Label_v001.rpt";
#endif

            cryRpt.Load(reportString);

            cryRpt.SetDataSource(ds.Tables["PCOReportDataTable"]);

            frmPrint.crystalReportViewer1.ReportSource = cryRpt;

            frmPrint.ShowDialog();
            frmPrint.crystalReportViewer1.Refresh();  
        }

        private void buttonNewPCOCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonNewPCOAccept_Click(object sender, EventArgs e)
        {
            if (GlobalModeType.ModeType == "New")
            {
                addNewLineOrderDtlPCO();
            }
            else
            {
                updateOrderDtlPCO();
            }
        }

        private void updateOrderDtlPCO()
        {
            string jobNumStr;
            int orderNumInt;
            int orderLineInt;
            int releaseNumInt;

            VS_DataSetTableAdapters.EtlPCOTableAdapter etlPCOTA =
                new VS_DataSetTableAdapters.EtlPCOTableAdapter();

            etlPCOTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

            jobNumStr = this.textBoxNewPCOJobNo.Text;
            orderNumInt = Int32.Parse(this.textBoxNewPCOOrderNum.Text);
            orderLineInt = Int32.Parse(this.textBoxNewPCOLineItem.Text);
            releaseNumInt = Int32.Parse(this.textBoxNewPCOReleaseNum.Text);

            try
            {
                etlPCOTA.UpdateByJobNum(
                    orderNumInt,
                    orderLineInt,
                    releaseNumInt,
                    jobNumStr,
                    this.textBoxNewPCOModelNo.Text,
                    this.textBoxNewPCOBulbPartNum.Text,
                    this.textBoxNewPCOTag.Text,
                    this.textBoxNewPCOCustomer.Text,
                    this.textBoxNewPCODateOfMfg.Text,
                    this.textBoxNewPCOPONum.Text);

                MessageBox.Show("Job # " + jobNumStr + " Updated successfully.");

                this.buttonNewPCOAccept.Text = "Update";
                GlobalModeType.ModeType = "Update";
                this.buttonNewPCOPrint.Enabled = true;
                this.labelNewPCOMsg.Text = jobNumStr +
                    " has been successfully updated in the EtlPCO table.";
            }
            catch
            {
                MessageBox.Show("ERROR - Updatinging data into EtlPCO");
            }

        }

        private void addNewLineOrderDtlPCO()
        {
            string jobNumStr;
            int orderNumInt;
            int orderLineInt;
            int releaseNumInt;

            string customerStr = "";
            string endConsumerNameStr = "";

            jobNumStr = this.textBoxNewPCOJobNo.Text;
            orderNumInt = Int32.Parse(this.textBoxNewPCOOrderNum.Text);
            orderLineInt = Int32.Parse(this.textBoxNewPCOLineItem.Text);
            releaseNumInt = Int32.Parse(this.textBoxNewPCOReleaseNum.Text);
            customerStr = this.textBoxNewPCOCustomer.Text;
            endConsumerNameStr = this.textBoxNewPCOEndConsumer.Text;

            VS_DataSet dsEtl = new VS_DataSet();

            // Write record to the VSOrderHed table.                
            try
            {
                // Create SqlDataAdapters for reading the VSOrderHed 
                // table using strongly typed datasets.
                
                VS_DataSetTableAdapters.EtlJobHeadTableAdapter etlJobHeadTA =
                    new VS_DataSetTableAdapters.EtlJobHeadTableAdapter();

                etlJobHeadTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                etlJobHeadTA.Fill(dsEtl.EtlJobHead);

                DataRow drJobHead = dsEtl.EtlJobHead.NewRow();

                drJobHead["JobNum"] = jobNumStr;
                drJobHead["OrderNum"] = orderNumInt.ToString();
                drJobHead["OrderLine"] = orderLineInt.ToString();
                drJobHead["ReleaseNum"] = releaseNumInt.ToString();
                drJobHead["UnitType"] = "PCO";
                drJobHead["Customer"] = customerStr;
                drJobHead["EndConsumerName"] = endConsumerNameStr;

                dsEtl.EtlJobHead.Rows.Add(drJobHead);

                etlJobHeadTA.Update(dsEtl.EtlJobHead);
            }
            catch
            {
                MessageBox.Show("Error occurred while inserting a row to the EtlJobHead table.");
                return;
            }

            VS_DataSetTableAdapters.EtlPCOTableAdapter etlPCOTA =
                new VS_DataSetTableAdapters.EtlPCOTableAdapter();

            etlPCOTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

            try
            {
                etlPCOTA.Fill(dsEtl.EtlPCO);

                DataRow drEtlPCO = dsEtl.EtlPCO.NewRow();

                drEtlPCO["JobNum"] = jobNumStr;
                drEtlPCO["OrderNum"] = orderNumInt;
                drEtlPCO["OrderLine"] = orderLineInt;
                drEtlPCO["ReleaseNum"] = releaseNumInt;
                drEtlPCO["ModelNum"] = this.textBoxNewPCOModelNo.Text;
                drEtlPCO["BulbPartNum"] = this.textBoxNewPCOBulbPartNum.Text;
                drEtlPCO["Tag"] = this.textBoxNewPCOTag.Text;
                drEtlPCO["Customer"] = this.textBoxNewPCOCustomer.Text;
                drEtlPCO["DateOfMfg"] = this.textBoxNewPCODateOfMfg.Text;
                drEtlPCO["PONum"] = this.textBoxNewPCOPONum.Text;

                dsEtl.EtlPCO.Rows.Add(drEtlPCO);

                etlPCOTA.Update(dsEtl.EtlPCO);

                MessageBox.Show("Job # " + jobNumStr + " added successfully.");

                this.buttonNewPCOAccept.Text = "Update";
                GlobalModeType.ModeType = "Update";
                this.buttonNewPCOPrint.Enabled = true;
                this.labelNewPCOMsg.Text = this.textBoxNewPCOJobNo +
                    " has been successfully written to the EtlPCO table.";
            }
            catch (Exception f)
            {
                MessageBox.Show("ERROR inserting data into EtlPCO " + f);
            }
        }        
    }
}