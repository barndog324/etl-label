namespace KCC.OA.Etl
{
    partial class frmNewLineRRU
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxNewRRU = new System.Windows.Forms.GroupBox();
            this.groupBoxNewRRUFridgeType = new System.Windows.Forms.GroupBox();
            this.radioButtonNewRRUR410A = new System.Windows.Forms.RadioButton();
            this.checkBoxModelTypeOverride = new System.Windows.Forms.CheckBox();
            this.checkBoxNewRRUFrench = new System.Windows.Forms.CheckBox();
            this.groupBoxNewRRUDirectFire = new System.Windows.Forms.GroupBox();
            this.textBoxNewRRUDFCutOutTemp = new System.Windows.Forms.TextBox();
            this.labelNewRRUDFCutOutTemp = new System.Windows.Forms.Label();
            this.textBoxNewRRUDFManifoldPressure = new System.Windows.Forms.TextBox();
            this.labelNewRRUDFManifoldPressure = new System.Windows.Forms.Label();
            this.textBoxNewRRUDFTempRise = new System.Windows.Forms.TextBox();
            this.labelNewRRUDFTempRise = new System.Windows.Forms.Label();
            this.textBoxNewRRUDFMinPressureDrop = new System.Windows.Forms.TextBox();
            this.labelNewRRUDFMinPressureDrop = new System.Windows.Forms.Label();
            this.textBoxNewRRUDFMinGasPressure = new System.Windows.Forms.TextBox();
            this.labelNewRRUDFMinGasPressure = new System.Windows.Forms.Label();
            this.textBoxNewRRUDFMaxPressureDrop = new System.Windows.Forms.TextBox();
            this.labelNewRRUDFMaxPressureDrop = new System.Windows.Forms.Label();
            this.textBoxNewRRUDFMaxGasPressure = new System.Windows.Forms.TextBox();
            this.labelNewRRUDFMaxGasPressure = new System.Windows.Forms.Label();
            this.textboxNewRRUDFMinHtgInput = new System.Windows.Forms.TextBox();
            this.labelNewRRUDFMinHtgInput = new System.Windows.Forms.Label();
            this.textBoxNewRRUDFMaxHtgInputBTUH = new System.Windows.Forms.TextBox();
            this.labelNewRRUDFMaxHtgInputBTUH = new System.Windows.Forms.Label();
            this.textBoxNewRRUDFStaticPressure = new System.Windows.Forms.TextBox();
            this.labelNewRRUDFStaticPressure = new System.Windows.Forms.Label();
            this.textBoxNewRRUDFEquiptedAirflow = new System.Windows.Forms.TextBox();
            this.labelNewRRUDFEquiptedAirflow = new System.Windows.Forms.Label();
            this.groupBoxNewRRUIndirectFire = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxNewRRUIFHeatingFuelType = new System.Windows.Forms.ComboBox();
            this.textBoxNewRRUIFMaxHtgInputBTUH = new System.Windows.Forms.TextBox();
            this.labelNewRRUIndirectFireMaxHtgInputBTUH = new System.Windows.Forms.Label();
            this.textBoxNewRRUIFTempRise = new System.Windows.Forms.TextBox();
            this.labelNewRRUIndFireTempRise = new System.Windows.Forms.Label();
            this.textBoxNewRRUIFManifoldPressure = new System.Windows.Forms.TextBox();
            this.labelNewRRUIndFireManifoldPres = new System.Windows.Forms.Label();
            this.textBoxNewRRUIFMaxGasPressure = new System.Windows.Forms.TextBox();
            this.labelNewRRUIndFireMaxGasPressure = new System.Windows.Forms.Label();
            this.textBoxNewRRUIFMinGasPressure = new System.Windows.Forms.TextBox();
            this.labelNewRRUIndFireMinGasPressure = new System.Windows.Forms.Label();
            this.textBoxNewRRUIFMaxOutAirTemp = new System.Windows.Forms.TextBox();
            this.labelNewRRUIndFireMaxOutAirTemp = new System.Windows.Forms.Label();
            this.textBoxNewRRUIFMaxExt = new System.Windows.Forms.TextBox();
            this.labelNewRRUIndFireMaxEst = new System.Windows.Forms.Label();
            this.textBoxNewRRUIFMinInputBTU = new System.Windows.Forms.TextBox();
            this.labelNewRRUIndFireMinHtgInput = new System.Windows.Forms.Label();
            this.textBoxNewRRUIFHeatingOutput = new System.Windows.Forms.TextBox();
            this.labelNewRRUIndFireHeatingOutputBTUH = new System.Windows.Forms.Label();
            this.textBoxNewRRUIFHeatingInputBTUH = new System.Windows.Forms.TextBox();
            this.labelNewRRUIndFireHHeatingInput = new System.Windows.Forms.Label();
            this.labelNewRRUMsg = new System.Windows.Forms.Label();
            this.buttonNewRRUPrint = new System.Windows.Forms.Button();
            this.buttonNewRRUAccept = new System.Windows.Forms.Button();
            this.buttonNewRRUCancel = new System.Windows.Forms.Button();
            this.groupBoxNewRRUFanInfo = new System.Windows.Forms.GroupBox();
            this.textBoxNewRRUFanOvrHP = new System.Windows.Forms.TextBox();
            this.labelNewRRUFanOvrHP = new System.Windows.Forms.Label();
            this.textBoxNewRRUFanOvrFLA = new System.Windows.Forms.TextBox();
            this.labelNewRRUFanOvrFLA = new System.Windows.Forms.Label();
            this.textBoxNewRRUFanOvrPH = new System.Windows.Forms.TextBox();
            this.labelNewRRUFanOvrPH = new System.Windows.Forms.Label();
            this.textBoxNewRRUFanOvrQty = new System.Windows.Forms.TextBox();
            this.labelNewRRUFanOvrQty = new System.Windows.Forms.Label();
            this.textBoxNewRRUFanStdHP = new System.Windows.Forms.TextBox();
            this.labelNewRRUFanStdHP = new System.Windows.Forms.Label();
            this.textBoxNewRRUFanStdFLA = new System.Windows.Forms.TextBox();
            this.labelNewRRUFanStdFLA = new System.Windows.Forms.Label();
            this.textBoxNewRRUFanStdPH = new System.Windows.Forms.TextBox();
            this.labelNewRRUFanStdPH = new System.Windows.Forms.Label();
            this.textBoxNewRRUFanStdQty = new System.Windows.Forms.TextBox();
            this.labelNewRRUFanStdQty = new System.Windows.Forms.Label();
            this.textBoxNewRRUFanCondHP = new System.Windows.Forms.TextBox();
            this.labelNewRRUFanCondHP = new System.Windows.Forms.Label();
            this.textBoxNewRRUFanCondFLA = new System.Windows.Forms.TextBox();
            this.labelNewRRUFanCondFLA = new System.Windows.Forms.Label();
            this.textBoxNewRRUFanCondPH = new System.Windows.Forms.TextBox();
            this.labelNewRRUFanCondPH = new System.Windows.Forms.Label();
            this.textBoxNewRRUFanCondQty = new System.Windows.Forms.TextBox();
            this.labelNewRRUFanCondQty = new System.Windows.Forms.Label();
            this.groupBoxNewRRUElectricInfo = new System.Windows.Forms.GroupBox();
            this.textBoxNewRRUHeatingType = new System.Windows.Forms.TextBox();
            this.labelNewRRUHeatingTypeInfo = new System.Windows.Forms.Label();
            this.textBoxNewRRUHeatingInput = new System.Windows.Forms.TextBox();
            this.labelNeRRUHeatingInput = new System.Windows.Forms.Label();
            this.textBoxNewRRUTestPressureLow = new System.Windows.Forms.TextBox();
            this.labelNewRRUTestPressureLow = new System.Windows.Forms.Label();
            this.textBoxNewRRUTestPressureHigh = new System.Windows.Forms.TextBox();
            this.labelNewRRUTestPressureHigh = new System.Windows.Forms.Label();
            this.textBoxNewRRUOperatingVolts = new System.Windows.Forms.TextBox();
            this.labelNewRRUOperatingVolts = new System.Windows.Forms.Label();
            this.textBoxNewRRUElectricalRating = new System.Windows.Forms.TextBox();
            this.labelNewRRUElectricalRating = new System.Windows.Forms.Label();
            this.textBoxNewRRUMOP = new System.Windows.Forms.TextBox();
            this.labelNewRRUMOP = new System.Windows.Forms.Label();
            this.textBoxNewRRUMFSMCB = new System.Windows.Forms.TextBox();
            this.labelNewRRUMFSMCB = new System.Windows.Forms.Label();
            this.textBoxNewRRUMinCKTAmp = new System.Windows.Forms.TextBox();
            this.labelNewRRUMinCKTAmp = new System.Windows.Forms.Label();
            this.groupBoxNewRRUCompressor1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxNewRRUComp3Charge = new System.Windows.Forms.TextBox();
            this.textBoxNewRRUComp3LRA = new System.Windows.Forms.TextBox();
            this.textBoxNewRRUComp3RLA = new System.Windows.Forms.TextBox();
            this.textBoxNewRRUComp3PH = new System.Windows.Forms.TextBox();
            this.textBoxNewRRUComp3Qty = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxNewRRUComp2Charge = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxNewRRUComp1Charge = new System.Windows.Forms.TextBox();
            this.textBoxNewRRUComp2LRA = new System.Windows.Forms.TextBox();
            this.labelNewRRUComp1Charge = new System.Windows.Forms.Label();
            this.textBoxNewRRUComp1LRA = new System.Windows.Forms.TextBox();
            this.textBoxNewRRUComp2RLAVolts = new System.Windows.Forms.TextBox();
            this.labelNewRRUComp1LRA = new System.Windows.Forms.Label();
            this.textBoxNewRRUComp1RLAVolts = new System.Windows.Forms.TextBox();
            this.textBoxNewRRUComp2PH = new System.Windows.Forms.TextBox();
            this.labelNewRRUComp1RLAVolts = new System.Windows.Forms.Label();
            this.textboxNewRRUComp1PH = new System.Windows.Forms.TextBox();
            this.textBoxNewRRUComp2Qty = new System.Windows.Forms.TextBox();
            this.labelNewRRUComp1PH = new System.Windows.Forms.Label();
            this.textBoxNewRRUComp1Qty = new System.Windows.Forms.TextBox();
            this.labelNewRRUComp1Qty = new System.Windows.Forms.Label();
            this.groupBoxNewRRULineInfo = new System.Windows.Forms.GroupBox();
            this.comboBoxNewRRUHeatingType = new System.Windows.Forms.ComboBox();
            this.labelNewRRUHeatingType = new System.Windows.Forms.Label();
            this.labelNewRRUPartNum = new System.Windows.Forms.Label();
            this.textBoxNewRRUPartNum = new System.Windows.Forms.TextBox();
            this.textBoxNewRRUVerifiedBy = new System.Windows.Forms.TextBox();
            this.labelMewRRUCreatedBy = new System.Windows.Forms.Label();
            this.comboBoxNewRRUModelNo = new System.Windows.Forms.ComboBox();
            this.labelNewRRUSerialNo = new System.Windows.Forms.Label();
            this.textBoxNewRRUSerialNo = new System.Windows.Forms.TextBox();
            this.labelNewRRUModelNum = new System.Windows.Forms.Label();
            this.groupBoxNewRRUJobInfo = new System.Windows.Forms.GroupBox();
            this.textBoxNewRRUJobNum = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxNewRRUEndConsumer = new System.Windows.Forms.TextBox();
            this.labelNewRRUEndConsumer = new System.Windows.Forms.Label();
            this.textBoxNewRRUReleaseNum = new System.Windows.Forms.TextBox();
            this.labelNewRRUSlash = new System.Windows.Forms.Label();
            this.textBoxNewRRUCustomer = new System.Windows.Forms.TextBox();
            this.textBoxNewRRUOrderNum = new System.Windows.Forms.TextBox();
            this.labelNewRRUCustomer = new System.Windows.Forms.Label();
            this.labelNewRRUOrderNum = new System.Windows.Forms.Label();
            this.textBoxNewRRULineItem = new System.Windows.Forms.TextBox();
            this.labelNewRRULineItem = new System.Windows.Forms.Label();
            this.menuStripRRUFile = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItemRRU = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBoxNewRRU.SuspendLayout();
            this.groupBoxNewRRUFridgeType.SuspendLayout();
            this.groupBoxNewRRUDirectFire.SuspendLayout();
            this.groupBoxNewRRUIndirectFire.SuspendLayout();
            this.groupBoxNewRRUFanInfo.SuspendLayout();
            this.groupBoxNewRRUElectricInfo.SuspendLayout();
            this.groupBoxNewRRUCompressor1.SuspendLayout();
            this.groupBoxNewRRULineInfo.SuspendLayout();
            this.groupBoxNewRRUJobInfo.SuspendLayout();
            this.menuStripRRUFile.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxNewRRU
            // 
            this.groupBoxNewRRU.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBoxNewRRU.Controls.Add(this.groupBoxNewRRUFridgeType);
            this.groupBoxNewRRU.Controls.Add(this.checkBoxModelTypeOverride);
            this.groupBoxNewRRU.Controls.Add(this.checkBoxNewRRUFrench);
            this.groupBoxNewRRU.Controls.Add(this.groupBoxNewRRUDirectFire);
            this.groupBoxNewRRU.Controls.Add(this.groupBoxNewRRUIndirectFire);
            this.groupBoxNewRRU.Controls.Add(this.labelNewRRUMsg);
            this.groupBoxNewRRU.Controls.Add(this.buttonNewRRUPrint);
            this.groupBoxNewRRU.Controls.Add(this.buttonNewRRUAccept);
            this.groupBoxNewRRU.Controls.Add(this.buttonNewRRUCancel);
            this.groupBoxNewRRU.Controls.Add(this.groupBoxNewRRUFanInfo);
            this.groupBoxNewRRU.Controls.Add(this.groupBoxNewRRUElectricInfo);
            this.groupBoxNewRRU.Controls.Add(this.groupBoxNewRRUCompressor1);
            this.groupBoxNewRRU.Controls.Add(this.groupBoxNewRRULineInfo);
            this.groupBoxNewRRU.Controls.Add(this.groupBoxNewRRUJobInfo);
            this.groupBoxNewRRU.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewRRU.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.groupBoxNewRRU.Location = new System.Drawing.Point(12, 30);
            this.groupBoxNewRRU.Name = "groupBoxNewRRU";
            this.groupBoxNewRRU.Size = new System.Drawing.Size(822, 968);
            this.groupBoxNewRRU.TabIndex = 1;
            this.groupBoxNewRRU.TabStop = false;
            this.groupBoxNewRRU.Text = "RRU";
            // 
            // groupBoxNewRRUFridgeType
            // 
            this.groupBoxNewRRUFridgeType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewRRUFridgeType.Controls.Add(this.radioButtonNewRRUR410A);
            this.groupBoxNewRRUFridgeType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewRRUFridgeType.Location = new System.Drawing.Point(653, 22);
            this.groupBoxNewRRUFridgeType.Name = "groupBoxNewRRUFridgeType";
            this.groupBoxNewRRUFridgeType.Size = new System.Drawing.Size(162, 51);
            this.groupBoxNewRRUFridgeType.TabIndex = 63;
            this.groupBoxNewRRUFridgeType.TabStop = false;
            this.groupBoxNewRRUFridgeType.Text = "Fridge Type";
            // 
            // radioButtonNewRRUR410A
            // 
            this.radioButtonNewRRUR410A.AutoSize = true;
            this.radioButtonNewRRUR410A.Checked = true;
            this.radioButtonNewRRUR410A.Location = new System.Drawing.Point(47, 21);
            this.radioButtonNewRRUR410A.Name = "radioButtonNewRRUR410A";
            this.radioButtonNewRRUR410A.Size = new System.Drawing.Size(67, 18);
            this.radioButtonNewRRUR410A.TabIndex = 64;
            this.radioButtonNewRRUR410A.TabStop = true;
            this.radioButtonNewRRUR410A.Text = "R410A";
            this.radioButtonNewRRUR410A.UseVisualStyleBackColor = true;
            // 
            // checkBoxModelTypeOverride
            // 
            this.checkBoxModelTypeOverride.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxModelTypeOverride.Location = new System.Drawing.Point(664, 107);
            this.checkBoxModelTypeOverride.Name = "checkBoxModelTypeOverride";
            this.checkBoxModelTypeOverride.Size = new System.Drawing.Size(139, 32);
            this.checkBoxModelTypeOverride.TabIndex = 8;
            this.checkBoxModelTypeOverride.Text = "Model Type Override";
            this.checkBoxModelTypeOverride.UseVisualStyleBackColor = true;
            this.checkBoxModelTypeOverride.CheckedChanged += new System.EventHandler(this.checkBoxModelTypeOverride_CheckedChanged);
            // 
            // checkBoxNewRRUFrench
            // 
            this.checkBoxNewRRUFrench.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxNewRRUFrench.Location = new System.Drawing.Point(664, 77);
            this.checkBoxNewRRUFrench.Name = "checkBoxNewRRUFrench";
            this.checkBoxNewRRUFrench.Size = new System.Drawing.Size(139, 32);
            this.checkBoxNewRRUFrench.TabIndex = 7;
            this.checkBoxNewRRUFrench.Text = "French Canadian Order";
            this.checkBoxNewRRUFrench.UseVisualStyleBackColor = true;
            // 
            // groupBoxNewRRUDirectFire
            // 
            this.groupBoxNewRRUDirectFire.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewRRUDirectFire.Controls.Add(this.textBoxNewRRUDFCutOutTemp);
            this.groupBoxNewRRUDirectFire.Controls.Add(this.labelNewRRUDFCutOutTemp);
            this.groupBoxNewRRUDirectFire.Controls.Add(this.textBoxNewRRUDFManifoldPressure);
            this.groupBoxNewRRUDirectFire.Controls.Add(this.labelNewRRUDFManifoldPressure);
            this.groupBoxNewRRUDirectFire.Controls.Add(this.textBoxNewRRUDFTempRise);
            this.groupBoxNewRRUDirectFire.Controls.Add(this.labelNewRRUDFTempRise);
            this.groupBoxNewRRUDirectFire.Controls.Add(this.textBoxNewRRUDFMinPressureDrop);
            this.groupBoxNewRRUDirectFire.Controls.Add(this.labelNewRRUDFMinPressureDrop);
            this.groupBoxNewRRUDirectFire.Controls.Add(this.textBoxNewRRUDFMinGasPressure);
            this.groupBoxNewRRUDirectFire.Controls.Add(this.labelNewRRUDFMinGasPressure);
            this.groupBoxNewRRUDirectFire.Controls.Add(this.textBoxNewRRUDFMaxPressureDrop);
            this.groupBoxNewRRUDirectFire.Controls.Add(this.labelNewRRUDFMaxPressureDrop);
            this.groupBoxNewRRUDirectFire.Controls.Add(this.textBoxNewRRUDFMaxGasPressure);
            this.groupBoxNewRRUDirectFire.Controls.Add(this.labelNewRRUDFMaxGasPressure);
            this.groupBoxNewRRUDirectFire.Controls.Add(this.textboxNewRRUDFMinHtgInput);
            this.groupBoxNewRRUDirectFire.Controls.Add(this.labelNewRRUDFMinHtgInput);
            this.groupBoxNewRRUDirectFire.Controls.Add(this.textBoxNewRRUDFMaxHtgInputBTUH);
            this.groupBoxNewRRUDirectFire.Controls.Add(this.labelNewRRUDFMaxHtgInputBTUH);
            this.groupBoxNewRRUDirectFire.Controls.Add(this.textBoxNewRRUDFStaticPressure);
            this.groupBoxNewRRUDirectFire.Controls.Add(this.labelNewRRUDFStaticPressure);
            this.groupBoxNewRRUDirectFire.Controls.Add(this.textBoxNewRRUDFEquiptedAirflow);
            this.groupBoxNewRRUDirectFire.Controls.Add(this.labelNewRRUDFEquiptedAirflow);
            this.groupBoxNewRRUDirectFire.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewRRUDirectFire.Location = new System.Drawing.Point(6, 283);
            this.groupBoxNewRRUDirectFire.Name = "groupBoxNewRRUDirectFire";
            this.groupBoxNewRRUDirectFire.Size = new System.Drawing.Size(293, 319);
            this.groupBoxNewRRUDirectFire.TabIndex = 2;
            this.groupBoxNewRRUDirectFire.TabStop = false;
            this.groupBoxNewRRUDirectFire.Text = "Direct Fire";
            this.groupBoxNewRRUDirectFire.Visible = false;
            // 
            // textBoxNewRRUDFCutOutTemp
            // 
            this.textBoxNewRRUDFCutOutTemp.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUDFCutOutTemp.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUDFCutOutTemp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUDFCutOutTemp.Location = new System.Drawing.Point(175, 289);
            this.textBoxNewRRUDFCutOutTemp.Name = "textBoxNewRRUDFCutOutTemp";
            this.textBoxNewRRUDFCutOutTemp.Size = new System.Drawing.Size(103, 22);
            this.textBoxNewRRUDFCutOutTemp.TabIndex = 11;
            this.textBoxNewRRUDFCutOutTemp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUDFCutOutTemp
            // 
            this.labelNewRRUDFCutOutTemp.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUDFCutOutTemp.Location = new System.Drawing.Point(11, 289);
            this.labelNewRRUDFCutOutTemp.Name = "labelNewRRUDFCutOutTemp";
            this.labelNewRRUDFCutOutTemp.Size = new System.Drawing.Size(157, 21);
            this.labelNewRRUDFCutOutTemp.TabIndex = 78;
            this.labelNewRRUDFCutOutTemp.Text = "Cut-Out Temp:";
            this.labelNewRRUDFCutOutTemp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUDFManifoldPressure
            // 
            this.textBoxNewRRUDFManifoldPressure.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUDFManifoldPressure.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUDFManifoldPressure.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUDFManifoldPressure.Location = new System.Drawing.Point(175, 262);
            this.textBoxNewRRUDFManifoldPressure.Name = "textBoxNewRRUDFManifoldPressure";
            this.textBoxNewRRUDFManifoldPressure.Size = new System.Drawing.Size(103, 22);
            this.textBoxNewRRUDFManifoldPressure.TabIndex = 10;
            this.textBoxNewRRUDFManifoldPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUDFManifoldPressure
            // 
            this.labelNewRRUDFManifoldPressure.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUDFManifoldPressure.Location = new System.Drawing.Point(11, 262);
            this.labelNewRRUDFManifoldPressure.Name = "labelNewRRUDFManifoldPressure";
            this.labelNewRRUDFManifoldPressure.Size = new System.Drawing.Size(157, 21);
            this.labelNewRRUDFManifoldPressure.TabIndex = 76;
            this.labelNewRRUDFManifoldPressure.Text = "Manifold Pressure:";
            this.labelNewRRUDFManifoldPressure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUDFTempRise
            // 
            this.textBoxNewRRUDFTempRise.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUDFTempRise.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUDFTempRise.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUDFTempRise.Location = new System.Drawing.Point(175, 127);
            this.textBoxNewRRUDFTempRise.Name = "textBoxNewRRUDFTempRise";
            this.textBoxNewRRUDFTempRise.Size = new System.Drawing.Size(103, 22);
            this.textBoxNewRRUDFTempRise.TabIndex = 5;
            this.textBoxNewRRUDFTempRise.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUDFTempRise
            // 
            this.labelNewRRUDFTempRise.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUDFTempRise.Location = new System.Drawing.Point(11, 127);
            this.labelNewRRUDFTempRise.Name = "labelNewRRUDFTempRise";
            this.labelNewRRUDFTempRise.Size = new System.Drawing.Size(157, 21);
            this.labelNewRRUDFTempRise.TabIndex = 74;
            this.labelNewRRUDFTempRise.Text = "Temp Rise:";
            this.labelNewRRUDFTempRise.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUDFMinPressureDrop
            // 
            this.textBoxNewRRUDFMinPressureDrop.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUDFMinPressureDrop.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUDFMinPressureDrop.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUDFMinPressureDrop.Location = new System.Drawing.Point(175, 235);
            this.textBoxNewRRUDFMinPressureDrop.Name = "textBoxNewRRUDFMinPressureDrop";
            this.textBoxNewRRUDFMinPressureDrop.Size = new System.Drawing.Size(103, 22);
            this.textBoxNewRRUDFMinPressureDrop.TabIndex = 9;
            this.textBoxNewRRUDFMinPressureDrop.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUDFMinPressureDrop
            // 
            this.labelNewRRUDFMinPressureDrop.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUDFMinPressureDrop.Location = new System.Drawing.Point(11, 235);
            this.labelNewRRUDFMinPressureDrop.Name = "labelNewRRUDFMinPressureDrop";
            this.labelNewRRUDFMinPressureDrop.Size = new System.Drawing.Size(157, 21);
            this.labelNewRRUDFMinPressureDrop.TabIndex = 73;
            this.labelNewRRUDFMinPressureDrop.Text = "Min Pressure Drop:";
            this.labelNewRRUDFMinPressureDrop.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUDFMinGasPressure
            // 
            this.textBoxNewRRUDFMinGasPressure.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUDFMinGasPressure.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUDFMinGasPressure.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUDFMinGasPressure.Location = new System.Drawing.Point(175, 181);
            this.textBoxNewRRUDFMinGasPressure.Name = "textBoxNewRRUDFMinGasPressure";
            this.textBoxNewRRUDFMinGasPressure.Size = new System.Drawing.Size(103, 22);
            this.textBoxNewRRUDFMinGasPressure.TabIndex = 7;
            this.textBoxNewRRUDFMinGasPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUDFMinGasPressure
            // 
            this.labelNewRRUDFMinGasPressure.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUDFMinGasPressure.Location = new System.Drawing.Point(11, 181);
            this.labelNewRRUDFMinGasPressure.Name = "labelNewRRUDFMinGasPressure";
            this.labelNewRRUDFMinGasPressure.Size = new System.Drawing.Size(157, 21);
            this.labelNewRRUDFMinGasPressure.TabIndex = 72;
            this.labelNewRRUDFMinGasPressure.Text = "Min Gas Pressure:";
            this.labelNewRRUDFMinGasPressure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUDFMaxPressureDrop
            // 
            this.textBoxNewRRUDFMaxPressureDrop.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUDFMaxPressureDrop.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUDFMaxPressureDrop.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUDFMaxPressureDrop.Location = new System.Drawing.Point(175, 208);
            this.textBoxNewRRUDFMaxPressureDrop.Name = "textBoxNewRRUDFMaxPressureDrop";
            this.textBoxNewRRUDFMaxPressureDrop.Size = new System.Drawing.Size(103, 22);
            this.textBoxNewRRUDFMaxPressureDrop.TabIndex = 8;
            this.textBoxNewRRUDFMaxPressureDrop.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUDFMaxPressureDrop
            // 
            this.labelNewRRUDFMaxPressureDrop.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUDFMaxPressureDrop.Location = new System.Drawing.Point(11, 208);
            this.labelNewRRUDFMaxPressureDrop.Name = "labelNewRRUDFMaxPressureDrop";
            this.labelNewRRUDFMaxPressureDrop.Size = new System.Drawing.Size(157, 21);
            this.labelNewRRUDFMaxPressureDrop.TabIndex = 71;
            this.labelNewRRUDFMaxPressureDrop.Text = "Max Pressure Drop:";
            this.labelNewRRUDFMaxPressureDrop.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUDFMaxGasPressure
            // 
            this.textBoxNewRRUDFMaxGasPressure.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUDFMaxGasPressure.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUDFMaxGasPressure.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUDFMaxGasPressure.Location = new System.Drawing.Point(175, 154);
            this.textBoxNewRRUDFMaxGasPressure.Name = "textBoxNewRRUDFMaxGasPressure";
            this.textBoxNewRRUDFMaxGasPressure.Size = new System.Drawing.Size(103, 22);
            this.textBoxNewRRUDFMaxGasPressure.TabIndex = 6;
            this.textBoxNewRRUDFMaxGasPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUDFMaxGasPressure
            // 
            this.labelNewRRUDFMaxGasPressure.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUDFMaxGasPressure.Location = new System.Drawing.Point(11, 154);
            this.labelNewRRUDFMaxGasPressure.Name = "labelNewRRUDFMaxGasPressure";
            this.labelNewRRUDFMaxGasPressure.Size = new System.Drawing.Size(157, 21);
            this.labelNewRRUDFMaxGasPressure.TabIndex = 70;
            this.labelNewRRUDFMaxGasPressure.Text = "Max Gas Pressure:";
            this.labelNewRRUDFMaxGasPressure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textboxNewRRUDFMinHtgInput
            // 
            this.textboxNewRRUDFMinHtgInput.BackColor = System.Drawing.Color.White;
            this.textboxNewRRUDFMinHtgInput.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textboxNewRRUDFMinHtgInput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textboxNewRRUDFMinHtgInput.Location = new System.Drawing.Point(175, 100);
            this.textboxNewRRUDFMinHtgInput.Name = "textboxNewRRUDFMinHtgInput";
            this.textboxNewRRUDFMinHtgInput.Size = new System.Drawing.Size(103, 22);
            this.textboxNewRRUDFMinHtgInput.TabIndex = 4;
            this.textboxNewRRUDFMinHtgInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUDFMinHtgInput
            // 
            this.labelNewRRUDFMinHtgInput.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUDFMinHtgInput.Location = new System.Drawing.Point(8, 100);
            this.labelNewRRUDFMinHtgInput.Name = "labelNewRRUDFMinHtgInput";
            this.labelNewRRUDFMinHtgInput.Size = new System.Drawing.Size(157, 21);
            this.labelNewRRUDFMinHtgInput.TabIndex = 69;
            this.labelNewRRUDFMinHtgInput.Text = "Min Heating Input:";
            this.labelNewRRUDFMinHtgInput.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUDFMaxHtgInputBTUH
            // 
            this.textBoxNewRRUDFMaxHtgInputBTUH.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUDFMaxHtgInputBTUH.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUDFMaxHtgInputBTUH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUDFMaxHtgInputBTUH.Location = new System.Drawing.Point(175, 73);
            this.textBoxNewRRUDFMaxHtgInputBTUH.Name = "textBoxNewRRUDFMaxHtgInputBTUH";
            this.textBoxNewRRUDFMaxHtgInputBTUH.Size = new System.Drawing.Size(103, 22);
            this.textBoxNewRRUDFMaxHtgInputBTUH.TabIndex = 3;
            this.textBoxNewRRUDFMaxHtgInputBTUH.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUDFMaxHtgInputBTUH
            // 
            this.labelNewRRUDFMaxHtgInputBTUH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUDFMaxHtgInputBTUH.Location = new System.Drawing.Point(11, 73);
            this.labelNewRRUDFMaxHtgInputBTUH.Name = "labelNewRRUDFMaxHtgInputBTUH";
            this.labelNewRRUDFMaxHtgInputBTUH.Size = new System.Drawing.Size(157, 21);
            this.labelNewRRUDFMaxHtgInputBTUH.TabIndex = 68;
            this.labelNewRRUDFMaxHtgInputBTUH.Text = "Max Heating Input BTUH:";
            this.labelNewRRUDFMaxHtgInputBTUH.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUDFStaticPressure
            // 
            this.textBoxNewRRUDFStaticPressure.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUDFStaticPressure.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUDFStaticPressure.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUDFStaticPressure.Location = new System.Drawing.Point(175, 46);
            this.textBoxNewRRUDFStaticPressure.Name = "textBoxNewRRUDFStaticPressure";
            this.textBoxNewRRUDFStaticPressure.Size = new System.Drawing.Size(103, 22);
            this.textBoxNewRRUDFStaticPressure.TabIndex = 2;
            this.textBoxNewRRUDFStaticPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUDFStaticPressure
            // 
            this.labelNewRRUDFStaticPressure.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUDFStaticPressure.Location = new System.Drawing.Point(11, 46);
            this.labelNewRRUDFStaticPressure.Name = "labelNewRRUDFStaticPressure";
            this.labelNewRRUDFStaticPressure.Size = new System.Drawing.Size(157, 21);
            this.labelNewRRUDFStaticPressure.TabIndex = 67;
            this.labelNewRRUDFStaticPressure.Text = "Static Pressure:";
            this.labelNewRRUDFStaticPressure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUDFEquiptedAirflow
            // 
            this.textBoxNewRRUDFEquiptedAirflow.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUDFEquiptedAirflow.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUDFEquiptedAirflow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUDFEquiptedAirflow.Location = new System.Drawing.Point(175, 19);
            this.textBoxNewRRUDFEquiptedAirflow.Name = "textBoxNewRRUDFEquiptedAirflow";
            this.textBoxNewRRUDFEquiptedAirflow.Size = new System.Drawing.Size(103, 22);
            this.textBoxNewRRUDFEquiptedAirflow.TabIndex = 1;
            this.textBoxNewRRUDFEquiptedAirflow.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUDFEquiptedAirflow
            // 
            this.labelNewRRUDFEquiptedAirflow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUDFEquiptedAirflow.Location = new System.Drawing.Point(11, 19);
            this.labelNewRRUDFEquiptedAirflow.Name = "labelNewRRUDFEquiptedAirflow";
            this.labelNewRRUDFEquiptedAirflow.Size = new System.Drawing.Size(157, 21);
            this.labelNewRRUDFEquiptedAirflow.TabIndex = 66;
            this.labelNewRRUDFEquiptedAirflow.Text = "Equipted Airflow:";
            this.labelNewRRUDFEquiptedAirflow.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxNewRRUIndirectFire
            // 
            this.groupBoxNewRRUIndirectFire.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewRRUIndirectFire.Controls.Add(this.label1);
            this.groupBoxNewRRUIndirectFire.Controls.Add(this.comboBoxNewRRUIFHeatingFuelType);
            this.groupBoxNewRRUIndirectFire.Controls.Add(this.textBoxNewRRUIFMaxHtgInputBTUH);
            this.groupBoxNewRRUIndirectFire.Controls.Add(this.labelNewRRUIndirectFireMaxHtgInputBTUH);
            this.groupBoxNewRRUIndirectFire.Controls.Add(this.textBoxNewRRUIFTempRise);
            this.groupBoxNewRRUIndirectFire.Controls.Add(this.labelNewRRUIndFireTempRise);
            this.groupBoxNewRRUIndirectFire.Controls.Add(this.textBoxNewRRUIFManifoldPressure);
            this.groupBoxNewRRUIndirectFire.Controls.Add(this.labelNewRRUIndFireManifoldPres);
            this.groupBoxNewRRUIndirectFire.Controls.Add(this.textBoxNewRRUIFMaxGasPressure);
            this.groupBoxNewRRUIndirectFire.Controls.Add(this.labelNewRRUIndFireMaxGasPressure);
            this.groupBoxNewRRUIndirectFire.Controls.Add(this.textBoxNewRRUIFMinGasPressure);
            this.groupBoxNewRRUIndirectFire.Controls.Add(this.labelNewRRUIndFireMinGasPressure);
            this.groupBoxNewRRUIndirectFire.Controls.Add(this.textBoxNewRRUIFMaxOutAirTemp);
            this.groupBoxNewRRUIndirectFire.Controls.Add(this.labelNewRRUIndFireMaxOutAirTemp);
            this.groupBoxNewRRUIndirectFire.Controls.Add(this.textBoxNewRRUIFMaxExt);
            this.groupBoxNewRRUIndirectFire.Controls.Add(this.labelNewRRUIndFireMaxEst);
            this.groupBoxNewRRUIndirectFire.Controls.Add(this.textBoxNewRRUIFMinInputBTU);
            this.groupBoxNewRRUIndirectFire.Controls.Add(this.labelNewRRUIndFireMinHtgInput);
            this.groupBoxNewRRUIndirectFire.Controls.Add(this.textBoxNewRRUIFHeatingOutput);
            this.groupBoxNewRRUIndirectFire.Controls.Add(this.labelNewRRUIndFireHeatingOutputBTUH);
            this.groupBoxNewRRUIndirectFire.Controls.Add(this.textBoxNewRRUIFHeatingInputBTUH);
            this.groupBoxNewRRUIndirectFire.Controls.Add(this.labelNewRRUIndFireHHeatingInput);
            this.groupBoxNewRRUIndirectFire.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewRRUIndirectFire.Location = new System.Drawing.Point(6, 608);
            this.groupBoxNewRRUIndirectFire.Name = "groupBoxNewRRUIndirectFire";
            this.groupBoxNewRRUIndirectFire.Size = new System.Drawing.Size(290, 327);
            this.groupBoxNewRRUIndirectFire.TabIndex = 3;
            this.groupBoxNewRRUIndirectFire.TabStop = false;
            this.groupBoxNewRRUIndirectFire.Text = "Indirect Fire";
            this.groupBoxNewRRUIndirectFire.Visible = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 291);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 21);
            this.label1.TabIndex = 58;
            this.label1.Text = "Fuel Type:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // comboBoxNewRRUIFHeatingFuelType
            // 
            this.comboBoxNewRRUIFHeatingFuelType.BackColor = System.Drawing.Color.White;
            this.comboBoxNewRRUIFHeatingFuelType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxNewRRUIFHeatingFuelType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.comboBoxNewRRUIFHeatingFuelType.FormattingEnabled = true;
            this.comboBoxNewRRUIFHeatingFuelType.Items.AddRange(new object[] {
            "Natural Gas",
            "Liquid Propane"});
            this.comboBoxNewRRUIFHeatingFuelType.Location = new System.Drawing.Point(171, 290);
            this.comboBoxNewRRUIFHeatingFuelType.Name = "comboBoxNewRRUIFHeatingFuelType";
            this.comboBoxNewRRUIFHeatingFuelType.Size = new System.Drawing.Size(103, 22);
            this.comboBoxNewRRUIFHeatingFuelType.TabIndex = 57;
            // 
            // textBoxNewRRUIFMaxHtgInputBTUH
            // 
            this.textBoxNewRRUIFMaxHtgInputBTUH.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUIFMaxHtgInputBTUH.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUIFMaxHtgInputBTUH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUIFMaxHtgInputBTUH.Location = new System.Drawing.Point(171, 262);
            this.textBoxNewRRUIFMaxHtgInputBTUH.Name = "textBoxNewRRUIFMaxHtgInputBTUH";
            this.textBoxNewRRUIFMaxHtgInputBTUH.Size = new System.Drawing.Size(103, 22);
            this.textBoxNewRRUIFMaxHtgInputBTUH.TabIndex = 10;
            this.textBoxNewRRUIFMaxHtgInputBTUH.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUIndirectFireMaxHtgInputBTUH
            // 
            this.labelNewRRUIndirectFireMaxHtgInputBTUH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUIndirectFireMaxHtgInputBTUH.Location = new System.Drawing.Point(7, 262);
            this.labelNewRRUIndirectFireMaxHtgInputBTUH.Name = "labelNewRRUIndirectFireMaxHtgInputBTUH";
            this.labelNewRRUIndirectFireMaxHtgInputBTUH.Size = new System.Drawing.Size(157, 21);
            this.labelNewRRUIndirectFireMaxHtgInputBTUH.TabIndex = 56;
            this.labelNewRRUIndirectFireMaxHtgInputBTUH.Text = "Max Heating Input BTUH:";
            this.labelNewRRUIndirectFireMaxHtgInputBTUH.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUIFTempRise
            // 
            this.textBoxNewRRUIFTempRise.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUIFTempRise.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUIFTempRise.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUIFTempRise.Location = new System.Drawing.Point(171, 127);
            this.textBoxNewRRUIFTempRise.Name = "textBoxNewRRUIFTempRise";
            this.textBoxNewRRUIFTempRise.Size = new System.Drawing.Size(103, 22);
            this.textBoxNewRRUIFTempRise.TabIndex = 5;
            this.textBoxNewRRUIFTempRise.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUIndFireTempRise
            // 
            this.labelNewRRUIndFireTempRise.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUIndFireTempRise.Location = new System.Drawing.Point(7, 127);
            this.labelNewRRUIndFireTempRise.Name = "labelNewRRUIndFireTempRise";
            this.labelNewRRUIndFireTempRise.Size = new System.Drawing.Size(157, 21);
            this.labelNewRRUIndFireTempRise.TabIndex = 54;
            this.labelNewRRUIndFireTempRise.Text = "Temp Rise:";
            this.labelNewRRUIndFireTempRise.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUIFManifoldPressure
            // 
            this.textBoxNewRRUIFManifoldPressure.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUIFManifoldPressure.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUIFManifoldPressure.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUIFManifoldPressure.Location = new System.Drawing.Point(171, 235);
            this.textBoxNewRRUIFManifoldPressure.Name = "textBoxNewRRUIFManifoldPressure";
            this.textBoxNewRRUIFManifoldPressure.Size = new System.Drawing.Size(103, 22);
            this.textBoxNewRRUIFManifoldPressure.TabIndex = 9;
            this.textBoxNewRRUIFManifoldPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUIndFireManifoldPres
            // 
            this.labelNewRRUIndFireManifoldPres.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUIndFireManifoldPres.Location = new System.Drawing.Point(7, 235);
            this.labelNewRRUIndFireManifoldPres.Name = "labelNewRRUIndFireManifoldPres";
            this.labelNewRRUIndFireManifoldPres.Size = new System.Drawing.Size(157, 21);
            this.labelNewRRUIndFireManifoldPres.TabIndex = 52;
            this.labelNewRRUIndFireManifoldPres.Text = "Manifold Pressure:";
            this.labelNewRRUIndFireManifoldPres.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUIFMaxGasPressure
            // 
            this.textBoxNewRRUIFMaxGasPressure.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUIFMaxGasPressure.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUIFMaxGasPressure.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUIFMaxGasPressure.Location = new System.Drawing.Point(171, 181);
            this.textBoxNewRRUIFMaxGasPressure.Name = "textBoxNewRRUIFMaxGasPressure";
            this.textBoxNewRRUIFMaxGasPressure.Size = new System.Drawing.Size(103, 22);
            this.textBoxNewRRUIFMaxGasPressure.TabIndex = 7;
            this.textBoxNewRRUIFMaxGasPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUIndFireMaxGasPressure
            // 
            this.labelNewRRUIndFireMaxGasPressure.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUIndFireMaxGasPressure.Location = new System.Drawing.Point(7, 181);
            this.labelNewRRUIndFireMaxGasPressure.Name = "labelNewRRUIndFireMaxGasPressure";
            this.labelNewRRUIndFireMaxGasPressure.Size = new System.Drawing.Size(157, 21);
            this.labelNewRRUIndFireMaxGasPressure.TabIndex = 50;
            this.labelNewRRUIndFireMaxGasPressure.Text = "Max Gas Pressure:";
            this.labelNewRRUIndFireMaxGasPressure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUIFMinGasPressure
            // 
            this.textBoxNewRRUIFMinGasPressure.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUIFMinGasPressure.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUIFMinGasPressure.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUIFMinGasPressure.Location = new System.Drawing.Point(171, 208);
            this.textBoxNewRRUIFMinGasPressure.Name = "textBoxNewRRUIFMinGasPressure";
            this.textBoxNewRRUIFMinGasPressure.Size = new System.Drawing.Size(103, 22);
            this.textBoxNewRRUIFMinGasPressure.TabIndex = 8;
            this.textBoxNewRRUIFMinGasPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUIndFireMinGasPressure
            // 
            this.labelNewRRUIndFireMinGasPressure.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUIndFireMinGasPressure.Location = new System.Drawing.Point(7, 208);
            this.labelNewRRUIndFireMinGasPressure.Name = "labelNewRRUIndFireMinGasPressure";
            this.labelNewRRUIndFireMinGasPressure.Size = new System.Drawing.Size(157, 21);
            this.labelNewRRUIndFireMinGasPressure.TabIndex = 48;
            this.labelNewRRUIndFireMinGasPressure.Text = "Min Gas Pressure:";
            this.labelNewRRUIndFireMinGasPressure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUIFMaxOutAirTemp
            // 
            this.textBoxNewRRUIFMaxOutAirTemp.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUIFMaxOutAirTemp.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUIFMaxOutAirTemp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUIFMaxOutAirTemp.Location = new System.Drawing.Point(171, 154);
            this.textBoxNewRRUIFMaxOutAirTemp.Name = "textBoxNewRRUIFMaxOutAirTemp";
            this.textBoxNewRRUIFMaxOutAirTemp.Size = new System.Drawing.Size(103, 22);
            this.textBoxNewRRUIFMaxOutAirTemp.TabIndex = 6;
            this.textBoxNewRRUIFMaxOutAirTemp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUIndFireMaxOutAirTemp
            // 
            this.labelNewRRUIndFireMaxOutAirTemp.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUIndFireMaxOutAirTemp.Location = new System.Drawing.Point(7, 154);
            this.labelNewRRUIndFireMaxOutAirTemp.Name = "labelNewRRUIndFireMaxOutAirTemp";
            this.labelNewRRUIndFireMaxOutAirTemp.Size = new System.Drawing.Size(157, 21);
            this.labelNewRRUIndFireMaxOutAirTemp.TabIndex = 46;
            this.labelNewRRUIndFireMaxOutAirTemp.Text = "Max Outlet Air Temp:";
            this.labelNewRRUIndFireMaxOutAirTemp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUIFMaxExt
            // 
            this.textBoxNewRRUIFMaxExt.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUIFMaxExt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUIFMaxExt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUIFMaxExt.Location = new System.Drawing.Point(171, 100);
            this.textBoxNewRRUIFMaxExt.Name = "textBoxNewRRUIFMaxExt";
            this.textBoxNewRRUIFMaxExt.Size = new System.Drawing.Size(103, 22);
            this.textBoxNewRRUIFMaxExt.TabIndex = 4;
            this.textBoxNewRRUIFMaxExt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUIndFireMaxEst
            // 
            this.labelNewRRUIndFireMaxEst.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUIndFireMaxEst.Location = new System.Drawing.Point(4, 100);
            this.labelNewRRUIndFireMaxEst.Name = "labelNewRRUIndFireMaxEst";
            this.labelNewRRUIndFireMaxEst.Size = new System.Drawing.Size(157, 21);
            this.labelNewRRUIndFireMaxEst.TabIndex = 44;
            this.labelNewRRUIndFireMaxEst.Text = "Max Ext:";
            this.labelNewRRUIndFireMaxEst.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUIFMinInputBTU
            // 
            this.textBoxNewRRUIFMinInputBTU.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUIFMinInputBTU.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUIFMinInputBTU.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUIFMinInputBTU.Location = new System.Drawing.Point(171, 73);
            this.textBoxNewRRUIFMinInputBTU.Name = "textBoxNewRRUIFMinInputBTU";
            this.textBoxNewRRUIFMinInputBTU.Size = new System.Drawing.Size(103, 22);
            this.textBoxNewRRUIFMinInputBTU.TabIndex = 3;
            this.textBoxNewRRUIFMinInputBTU.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUIndFireMinHtgInput
            // 
            this.labelNewRRUIndFireMinHtgInput.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUIndFireMinHtgInput.Location = new System.Drawing.Point(7, 73);
            this.labelNewRRUIndFireMinHtgInput.Name = "labelNewRRUIndFireMinHtgInput";
            this.labelNewRRUIndFireMinHtgInput.Size = new System.Drawing.Size(157, 21);
            this.labelNewRRUIndFireMinHtgInput.TabIndex = 42;
            this.labelNewRRUIndFireMinHtgInput.Text = "Min Input BTU:";
            this.labelNewRRUIndFireMinHtgInput.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUIFHeatingOutput
            // 
            this.textBoxNewRRUIFHeatingOutput.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUIFHeatingOutput.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUIFHeatingOutput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUIFHeatingOutput.Location = new System.Drawing.Point(171, 46);
            this.textBoxNewRRUIFHeatingOutput.Name = "textBoxNewRRUIFHeatingOutput";
            this.textBoxNewRRUIFHeatingOutput.Size = new System.Drawing.Size(103, 22);
            this.textBoxNewRRUIFHeatingOutput.TabIndex = 2;
            this.textBoxNewRRUIFHeatingOutput.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUIndFireHeatingOutputBTUH
            // 
            this.labelNewRRUIndFireHeatingOutputBTUH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUIndFireHeatingOutputBTUH.Location = new System.Drawing.Point(7, 46);
            this.labelNewRRUIndFireHeatingOutputBTUH.Name = "labelNewRRUIndFireHeatingOutputBTUH";
            this.labelNewRRUIndFireHeatingOutputBTUH.Size = new System.Drawing.Size(157, 21);
            this.labelNewRRUIndFireHeatingOutputBTUH.TabIndex = 40;
            this.labelNewRRUIndFireHeatingOutputBTUH.Text = "Heating Output BTUH:";
            this.labelNewRRUIndFireHeatingOutputBTUH.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUIFHeatingInputBTUH
            // 
            this.textBoxNewRRUIFHeatingInputBTUH.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUIFHeatingInputBTUH.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUIFHeatingInputBTUH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUIFHeatingInputBTUH.Location = new System.Drawing.Point(171, 19);
            this.textBoxNewRRUIFHeatingInputBTUH.Name = "textBoxNewRRUIFHeatingInputBTUH";
            this.textBoxNewRRUIFHeatingInputBTUH.Size = new System.Drawing.Size(103, 22);
            this.textBoxNewRRUIFHeatingInputBTUH.TabIndex = 1;
            this.textBoxNewRRUIFHeatingInputBTUH.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUIndFireHHeatingInput
            // 
            this.labelNewRRUIndFireHHeatingInput.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUIndFireHHeatingInput.Location = new System.Drawing.Point(7, 19);
            this.labelNewRRUIndFireHHeatingInput.Name = "labelNewRRUIndFireHHeatingInput";
            this.labelNewRRUIndFireHHeatingInput.Size = new System.Drawing.Size(157, 21);
            this.labelNewRRUIndFireHHeatingInput.TabIndex = 38;
            this.labelNewRRUIndFireHHeatingInput.Text = "Heating Input BTUH:";
            this.labelNewRRUIndFireHHeatingInput.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelNewRRUMsg
            // 
            this.labelNewRRUMsg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.labelNewRRUMsg.Location = new System.Drawing.Point(6, 606);
            this.labelNewRRUMsg.Name = "labelNewRRUMsg";
            this.labelNewRRUMsg.Size = new System.Drawing.Size(810, 21);
            this.labelNewRRUMsg.TabIndex = 13;
            this.labelNewRRUMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonNewRRUPrint
            // 
            this.buttonNewRRUPrint.Enabled = false;
            this.buttonNewRRUPrint.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewRRUPrint.Location = new System.Drawing.Point(688, 231);
            this.buttonNewRRUPrint.Name = "buttonNewRRUPrint";
            this.buttonNewRRUPrint.Size = new System.Drawing.Size(95, 37);
            this.buttonNewRRUPrint.TabIndex = 11;
            this.buttonNewRRUPrint.Text = "Print";
            this.buttonNewRRUPrint.UseVisualStyleBackColor = true;
            this.buttonNewRRUPrint.Click += new System.EventHandler(this.buttonNewRRUPrint_Click);
            // 
            // buttonNewRRUAccept
            // 
            this.buttonNewRRUAccept.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewRRUAccept.Location = new System.Drawing.Point(688, 188);
            this.buttonNewRRUAccept.Name = "buttonNewRRUAccept";
            this.buttonNewRRUAccept.Size = new System.Drawing.Size(95, 37);
            this.buttonNewRRUAccept.TabIndex = 10;
            this.buttonNewRRUAccept.Text = "Accept";
            this.buttonNewRRUAccept.UseVisualStyleBackColor = true;
            this.buttonNewRRUAccept.Click += new System.EventHandler(this.buttonNewRRUAccept_Click);
            // 
            // buttonNewRRUCancel
            // 
            this.buttonNewRRUCancel.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewRRUCancel.ForeColor = System.Drawing.Color.Red;
            this.buttonNewRRUCancel.Location = new System.Drawing.Point(688, 145);
            this.buttonNewRRUCancel.Name = "buttonNewRRUCancel";
            this.buttonNewRRUCancel.Size = new System.Drawing.Size(95, 37);
            this.buttonNewRRUCancel.TabIndex = 9;
            this.buttonNewRRUCancel.Text = "Cancel";
            this.buttonNewRRUCancel.UseVisualStyleBackColor = true;
            this.buttonNewRRUCancel.Click += new System.EventHandler(this.buttonNewRRUCancel_Click);
            // 
            // groupBoxNewRRUFanInfo
            // 
            this.groupBoxNewRRUFanInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewRRUFanInfo.Controls.Add(this.textBoxNewRRUFanOvrHP);
            this.groupBoxNewRRUFanInfo.Controls.Add(this.labelNewRRUFanOvrHP);
            this.groupBoxNewRRUFanInfo.Controls.Add(this.textBoxNewRRUFanOvrFLA);
            this.groupBoxNewRRUFanInfo.Controls.Add(this.labelNewRRUFanOvrFLA);
            this.groupBoxNewRRUFanInfo.Controls.Add(this.textBoxNewRRUFanOvrPH);
            this.groupBoxNewRRUFanInfo.Controls.Add(this.labelNewRRUFanOvrPH);
            this.groupBoxNewRRUFanInfo.Controls.Add(this.textBoxNewRRUFanOvrQty);
            this.groupBoxNewRRUFanInfo.Controls.Add(this.labelNewRRUFanOvrQty);
            this.groupBoxNewRRUFanInfo.Controls.Add(this.textBoxNewRRUFanStdHP);
            this.groupBoxNewRRUFanInfo.Controls.Add(this.labelNewRRUFanStdHP);
            this.groupBoxNewRRUFanInfo.Controls.Add(this.textBoxNewRRUFanStdFLA);
            this.groupBoxNewRRUFanInfo.Controls.Add(this.labelNewRRUFanStdFLA);
            this.groupBoxNewRRUFanInfo.Controls.Add(this.textBoxNewRRUFanStdPH);
            this.groupBoxNewRRUFanInfo.Controls.Add(this.labelNewRRUFanStdPH);
            this.groupBoxNewRRUFanInfo.Controls.Add(this.textBoxNewRRUFanStdQty);
            this.groupBoxNewRRUFanInfo.Controls.Add(this.labelNewRRUFanStdQty);
            this.groupBoxNewRRUFanInfo.Controls.Add(this.textBoxNewRRUFanCondHP);
            this.groupBoxNewRRUFanInfo.Controls.Add(this.labelNewRRUFanCondHP);
            this.groupBoxNewRRUFanInfo.Controls.Add(this.textBoxNewRRUFanCondFLA);
            this.groupBoxNewRRUFanInfo.Controls.Add(this.labelNewRRUFanCondFLA);
            this.groupBoxNewRRUFanInfo.Controls.Add(this.textBoxNewRRUFanCondPH);
            this.groupBoxNewRRUFanInfo.Controls.Add(this.labelNewRRUFanCondPH);
            this.groupBoxNewRRUFanInfo.Controls.Add(this.textBoxNewRRUFanCondQty);
            this.groupBoxNewRRUFanInfo.Controls.Add(this.labelNewRRUFanCondQty);
            this.groupBoxNewRRUFanInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewRRUFanInfo.Location = new System.Drawing.Point(302, 466);
            this.groupBoxNewRRUFanInfo.Name = "groupBoxNewRRUFanInfo";
            this.groupBoxNewRRUFanInfo.Size = new System.Drawing.Size(513, 137);
            this.groupBoxNewRRUFanInfo.TabIndex = 6;
            this.groupBoxNewRRUFanInfo.TabStop = false;
            this.groupBoxNewRRUFanInfo.Text = "Fans";
            // 
            // textBoxNewRRUFanOvrHP
            // 
            this.textBoxNewRRUFanOvrHP.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUFanOvrHP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUFanOvrHP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUFanOvrHP.Location = new System.Drawing.Point(425, 102);
            this.textBoxNewRRUFanOvrHP.Name = "textBoxNewRRUFanOvrHP";
            this.textBoxNewRRUFanOvrHP.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUFanOvrHP.TabIndex = 12;
            this.textBoxNewRRUFanOvrHP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUFanOvrHP
            // 
            this.labelNewRRUFanOvrHP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUFanOvrHP.Location = new System.Drawing.Point(338, 102);
            this.labelNewRRUFanOvrHP.Name = "labelNewRRUFanOvrHP";
            this.labelNewRRUFanOvrHP.Size = new System.Drawing.Size(83, 21);
            this.labelNewRRUFanOvrHP.TabIndex = 28;
            this.labelNewRRUFanOvrHP.Text = "Fan Ovr HP:";
            this.labelNewRRUFanOvrHP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUFanOvrFLA
            // 
            this.textBoxNewRRUFanOvrFLA.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUFanOvrFLA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUFanOvrFLA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUFanOvrFLA.Location = new System.Drawing.Point(425, 75);
            this.textBoxNewRRUFanOvrFLA.Name = "textBoxNewRRUFanOvrFLA";
            this.textBoxNewRRUFanOvrFLA.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUFanOvrFLA.TabIndex = 11;
            this.textBoxNewRRUFanOvrFLA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUFanOvrFLA
            // 
            this.labelNewRRUFanOvrFLA.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUFanOvrFLA.Location = new System.Drawing.Point(338, 75);
            this.labelNewRRUFanOvrFLA.Name = "labelNewRRUFanOvrFLA";
            this.labelNewRRUFanOvrFLA.Size = new System.Drawing.Size(83, 21);
            this.labelNewRRUFanOvrFLA.TabIndex = 26;
            this.labelNewRRUFanOvrFLA.Text = "Fan Ovr FLA:";
            this.labelNewRRUFanOvrFLA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUFanOvrPH
            // 
            this.textBoxNewRRUFanOvrPH.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUFanOvrPH.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUFanOvrPH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUFanOvrPH.Location = new System.Drawing.Point(425, 48);
            this.textBoxNewRRUFanOvrPH.Name = "textBoxNewRRUFanOvrPH";
            this.textBoxNewRRUFanOvrPH.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUFanOvrPH.TabIndex = 10;
            this.textBoxNewRRUFanOvrPH.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUFanOvrPH
            // 
            this.labelNewRRUFanOvrPH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUFanOvrPH.Location = new System.Drawing.Point(338, 48);
            this.labelNewRRUFanOvrPH.Name = "labelNewRRUFanOvrPH";
            this.labelNewRRUFanOvrPH.Size = new System.Drawing.Size(83, 21);
            this.labelNewRRUFanOvrPH.TabIndex = 24;
            this.labelNewRRUFanOvrPH.Text = "Fan Ovr PH:";
            this.labelNewRRUFanOvrPH.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUFanOvrQty
            // 
            this.textBoxNewRRUFanOvrQty.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUFanOvrQty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUFanOvrQty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUFanOvrQty.Location = new System.Drawing.Point(425, 21);
            this.textBoxNewRRUFanOvrQty.Name = "textBoxNewRRUFanOvrQty";
            this.textBoxNewRRUFanOvrQty.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUFanOvrQty.TabIndex = 9;
            this.textBoxNewRRUFanOvrQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUFanOvrQty
            // 
            this.labelNewRRUFanOvrQty.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUFanOvrQty.Location = new System.Drawing.Point(338, 21);
            this.labelNewRRUFanOvrQty.Name = "labelNewRRUFanOvrQty";
            this.labelNewRRUFanOvrQty.Size = new System.Drawing.Size(83, 21);
            this.labelNewRRUFanOvrQty.TabIndex = 22;
            this.labelNewRRUFanOvrQty.Text = "Fan Ovr Qty:";
            this.labelNewRRUFanOvrQty.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUFanStdHP
            // 
            this.textBoxNewRRUFanStdHP.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUFanStdHP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUFanStdHP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUFanStdHP.Location = new System.Drawing.Point(263, 101);
            this.textBoxNewRRUFanStdHP.Name = "textBoxNewRRUFanStdHP";
            this.textBoxNewRRUFanStdHP.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUFanStdHP.TabIndex = 8;
            this.textBoxNewRRUFanStdHP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUFanStdHP
            // 
            this.labelNewRRUFanStdHP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUFanStdHP.Location = new System.Drawing.Point(174, 101);
            this.labelNewRRUFanStdHP.Name = "labelNewRRUFanStdHP";
            this.labelNewRRUFanStdHP.Size = new System.Drawing.Size(85, 21);
            this.labelNewRRUFanStdHP.TabIndex = 20;
            this.labelNewRRUFanStdHP.Text = "Fan Std HP:";
            this.labelNewRRUFanStdHP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUFanStdFLA
            // 
            this.textBoxNewRRUFanStdFLA.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUFanStdFLA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUFanStdFLA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUFanStdFLA.Location = new System.Drawing.Point(263, 74);
            this.textBoxNewRRUFanStdFLA.Name = "textBoxNewRRUFanStdFLA";
            this.textBoxNewRRUFanStdFLA.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUFanStdFLA.TabIndex = 7;
            this.textBoxNewRRUFanStdFLA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUFanStdFLA
            // 
            this.labelNewRRUFanStdFLA.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUFanStdFLA.Location = new System.Drawing.Point(174, 74);
            this.labelNewRRUFanStdFLA.Name = "labelNewRRUFanStdFLA";
            this.labelNewRRUFanStdFLA.Size = new System.Drawing.Size(85, 21);
            this.labelNewRRUFanStdFLA.TabIndex = 18;
            this.labelNewRRUFanStdFLA.Text = "Fan Std FLA:";
            this.labelNewRRUFanStdFLA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUFanStdPH
            // 
            this.textBoxNewRRUFanStdPH.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUFanStdPH.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUFanStdPH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUFanStdPH.Location = new System.Drawing.Point(263, 47);
            this.textBoxNewRRUFanStdPH.Name = "textBoxNewRRUFanStdPH";
            this.textBoxNewRRUFanStdPH.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUFanStdPH.TabIndex = 6;
            this.textBoxNewRRUFanStdPH.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUFanStdPH
            // 
            this.labelNewRRUFanStdPH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUFanStdPH.Location = new System.Drawing.Point(174, 47);
            this.labelNewRRUFanStdPH.Name = "labelNewRRUFanStdPH";
            this.labelNewRRUFanStdPH.Size = new System.Drawing.Size(85, 21);
            this.labelNewRRUFanStdPH.TabIndex = 16;
            this.labelNewRRUFanStdPH.Text = "Fan Std PH:";
            this.labelNewRRUFanStdPH.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUFanStdQty
            // 
            this.textBoxNewRRUFanStdQty.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUFanStdQty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUFanStdQty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUFanStdQty.Location = new System.Drawing.Point(263, 20);
            this.textBoxNewRRUFanStdQty.Name = "textBoxNewRRUFanStdQty";
            this.textBoxNewRRUFanStdQty.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUFanStdQty.TabIndex = 5;
            this.textBoxNewRRUFanStdQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUFanStdQty
            // 
            this.labelNewRRUFanStdQty.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUFanStdQty.Location = new System.Drawing.Point(174, 20);
            this.labelNewRRUFanStdQty.Name = "labelNewRRUFanStdQty";
            this.labelNewRRUFanStdQty.Size = new System.Drawing.Size(85, 21);
            this.labelNewRRUFanStdQty.TabIndex = 14;
            this.labelNewRRUFanStdQty.Text = "Fan Std Qty:";
            this.labelNewRRUFanStdQty.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUFanCondHP
            // 
            this.textBoxNewRRUFanCondHP.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUFanCondHP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUFanCondHP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUFanCondHP.Location = new System.Drawing.Point(98, 101);
            this.textBoxNewRRUFanCondHP.Name = "textBoxNewRRUFanCondHP";
            this.textBoxNewRRUFanCondHP.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUFanCondHP.TabIndex = 4;
            this.textBoxNewRRUFanCondHP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUFanCondHP
            // 
            this.labelNewRRUFanCondHP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUFanCondHP.Location = new System.Drawing.Point(9, 101);
            this.labelNewRRUFanCondHP.Name = "labelNewRRUFanCondHP";
            this.labelNewRRUFanCondHP.Size = new System.Drawing.Size(85, 21);
            this.labelNewRRUFanCondHP.TabIndex = 12;
            this.labelNewRRUFanCondHP.Text = "Fan Cond HP:";
            this.labelNewRRUFanCondHP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUFanCondFLA
            // 
            this.textBoxNewRRUFanCondFLA.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUFanCondFLA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUFanCondFLA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUFanCondFLA.Location = new System.Drawing.Point(98, 74);
            this.textBoxNewRRUFanCondFLA.Name = "textBoxNewRRUFanCondFLA";
            this.textBoxNewRRUFanCondFLA.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUFanCondFLA.TabIndex = 3;
            this.textBoxNewRRUFanCondFLA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUFanCondFLA
            // 
            this.labelNewRRUFanCondFLA.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUFanCondFLA.Location = new System.Drawing.Point(9, 74);
            this.labelNewRRUFanCondFLA.Name = "labelNewRRUFanCondFLA";
            this.labelNewRRUFanCondFLA.Size = new System.Drawing.Size(85, 21);
            this.labelNewRRUFanCondFLA.TabIndex = 10;
            this.labelNewRRUFanCondFLA.Text = "Fan Cond FLA:";
            this.labelNewRRUFanCondFLA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUFanCondPH
            // 
            this.textBoxNewRRUFanCondPH.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUFanCondPH.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUFanCondPH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUFanCondPH.Location = new System.Drawing.Point(98, 46);
            this.textBoxNewRRUFanCondPH.Name = "textBoxNewRRUFanCondPH";
            this.textBoxNewRRUFanCondPH.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUFanCondPH.TabIndex = 2;
            this.textBoxNewRRUFanCondPH.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUFanCondPH
            // 
            this.labelNewRRUFanCondPH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUFanCondPH.Location = new System.Drawing.Point(9, 46);
            this.labelNewRRUFanCondPH.Name = "labelNewRRUFanCondPH";
            this.labelNewRRUFanCondPH.Size = new System.Drawing.Size(85, 21);
            this.labelNewRRUFanCondPH.TabIndex = 8;
            this.labelNewRRUFanCondPH.Text = "Fan Cond PH:";
            this.labelNewRRUFanCondPH.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUFanCondQty
            // 
            this.textBoxNewRRUFanCondQty.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUFanCondQty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUFanCondQty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUFanCondQty.Location = new System.Drawing.Point(98, 19);
            this.textBoxNewRRUFanCondQty.Name = "textBoxNewRRUFanCondQty";
            this.textBoxNewRRUFanCondQty.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUFanCondQty.TabIndex = 1;
            this.textBoxNewRRUFanCondQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUFanCondQty
            // 
            this.labelNewRRUFanCondQty.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUFanCondQty.Location = new System.Drawing.Point(9, 19);
            this.labelNewRRUFanCondQty.Name = "labelNewRRUFanCondQty";
            this.labelNewRRUFanCondQty.Size = new System.Drawing.Size(85, 21);
            this.labelNewRRUFanCondQty.TabIndex = 6;
            this.labelNewRRUFanCondQty.Text = "Fan Cond Qty:";
            this.labelNewRRUFanCondQty.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxNewRRUElectricInfo
            // 
            this.groupBoxNewRRUElectricInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.textBoxNewRRUHeatingType);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.labelNewRRUHeatingTypeInfo);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.textBoxNewRRUHeatingInput);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.labelNeRRUHeatingInput);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.textBoxNewRRUTestPressureLow);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.labelNewRRUTestPressureLow);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.textBoxNewRRUTestPressureHigh);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.labelNewRRUTestPressureHigh);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.textBoxNewRRUOperatingVolts);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.labelNewRRUOperatingVolts);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.textBoxNewRRUElectricalRating);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.labelNewRRUElectricalRating);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.textBoxNewRRUMOP);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.labelNewRRUMOP);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.textBoxNewRRUMFSMCB);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.labelNewRRUMFSMCB);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.textBoxNewRRUMinCKTAmp);
            this.groupBoxNewRRUElectricInfo.Controls.Add(this.labelNewRRUMinCKTAmp);
            this.groupBoxNewRRUElectricInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewRRUElectricInfo.Location = new System.Drawing.Point(302, 283);
            this.groupBoxNewRRUElectricInfo.Name = "groupBoxNewRRUElectricInfo";
            this.groupBoxNewRRUElectricInfo.Size = new System.Drawing.Size(513, 177);
            this.groupBoxNewRRUElectricInfo.TabIndex = 5;
            this.groupBoxNewRRUElectricInfo.TabStop = false;
            this.groupBoxNewRRUElectricInfo.Text = "Electric/Heating Info";
            // 
            // textBoxNewRRUHeatingType
            // 
            this.textBoxNewRRUHeatingType.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUHeatingType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUHeatingType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUHeatingType.Location = new System.Drawing.Point(406, 31);
            this.textBoxNewRRUHeatingType.Name = "textBoxNewRRUHeatingType";
            this.textBoxNewRRUHeatingType.Size = new System.Drawing.Size(99, 22);
            this.textBoxNewRRUHeatingType.TabIndex = 6;
            this.textBoxNewRRUHeatingType.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUHeatingTypeInfo
            // 
            this.labelNewRRUHeatingTypeInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUHeatingTypeInfo.Location = new System.Drawing.Point(284, 31);
            this.labelNewRRUHeatingTypeInfo.Name = "labelNewRRUHeatingTypeInfo";
            this.labelNewRRUHeatingTypeInfo.Size = new System.Drawing.Size(116, 21);
            this.labelNewRRUHeatingTypeInfo.TabIndex = 61;
            this.labelNewRRUHeatingTypeInfo.Text = "Heating Type:";
            this.labelNewRRUHeatingTypeInfo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUHeatingInput
            // 
            this.textBoxNewRRUHeatingInput.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUHeatingInput.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUHeatingInput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUHeatingInput.Location = new System.Drawing.Point(406, 59);
            this.textBoxNewRRUHeatingInput.Name = "textBoxNewRRUHeatingInput";
            this.textBoxNewRRUHeatingInput.Size = new System.Drawing.Size(99, 22);
            this.textBoxNewRRUHeatingInput.TabIndex = 7;
            this.textBoxNewRRUHeatingInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNeRRUHeatingInput
            // 
            this.labelNeRRUHeatingInput.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNeRRUHeatingInput.Location = new System.Drawing.Point(284, 59);
            this.labelNeRRUHeatingInput.Name = "labelNeRRUHeatingInput";
            this.labelNeRRUHeatingInput.Size = new System.Drawing.Size(116, 21);
            this.labelNeRRUHeatingInput.TabIndex = 59;
            this.labelNeRRUHeatingInput.Text = "Heating Input:";
            this.labelNeRRUHeatingInput.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUTestPressureLow
            // 
            this.textBoxNewRRUTestPressureLow.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUTestPressureLow.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUTestPressureLow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUTestPressureLow.Location = new System.Drawing.Point(406, 114);
            this.textBoxNewRRUTestPressureLow.Name = "textBoxNewRRUTestPressureLow";
            this.textBoxNewRRUTestPressureLow.Size = new System.Drawing.Size(99, 22);
            this.textBoxNewRRUTestPressureLow.TabIndex = 9;
            this.textBoxNewRRUTestPressureLow.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUTestPressureLow
            // 
            this.labelNewRRUTestPressureLow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUTestPressureLow.Location = new System.Drawing.Point(284, 114);
            this.labelNewRRUTestPressureLow.Name = "labelNewRRUTestPressureLow";
            this.labelNewRRUTestPressureLow.Size = new System.Drawing.Size(116, 21);
            this.labelNewRRUTestPressureLow.TabIndex = 18;
            this.labelNewRRUTestPressureLow.Text = "Test Pressure Low:";
            this.labelNewRRUTestPressureLow.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUTestPressureHigh
            // 
            this.textBoxNewRRUTestPressureHigh.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUTestPressureHigh.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUTestPressureHigh.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUTestPressureHigh.Location = new System.Drawing.Point(406, 87);
            this.textBoxNewRRUTestPressureHigh.Name = "textBoxNewRRUTestPressureHigh";
            this.textBoxNewRRUTestPressureHigh.Size = new System.Drawing.Size(99, 22);
            this.textBoxNewRRUTestPressureHigh.TabIndex = 8;
            this.textBoxNewRRUTestPressureHigh.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUTestPressureHigh
            // 
            this.labelNewRRUTestPressureHigh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUTestPressureHigh.Location = new System.Drawing.Point(284, 87);
            this.labelNewRRUTestPressureHigh.Name = "labelNewRRUTestPressureHigh";
            this.labelNewRRUTestPressureHigh.Size = new System.Drawing.Size(116, 21);
            this.labelNewRRUTestPressureHigh.TabIndex = 16;
            this.labelNewRRUTestPressureHigh.Text = "Test Pressure High:";
            this.labelNewRRUTestPressureHigh.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUOperatingVolts
            // 
            this.textBoxNewRRUOperatingVolts.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUOperatingVolts.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUOperatingVolts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUOperatingVolts.Location = new System.Drawing.Point(156, 136);
            this.textBoxNewRRUOperatingVolts.Name = "textBoxNewRRUOperatingVolts";
            this.textBoxNewRRUOperatingVolts.Size = new System.Drawing.Size(99, 22);
            this.textBoxNewRRUOperatingVolts.TabIndex = 5;
            this.textBoxNewRRUOperatingVolts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUOperatingVolts
            // 
            this.labelNewRRUOperatingVolts.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUOperatingVolts.Location = new System.Drawing.Point(34, 136);
            this.labelNewRRUOperatingVolts.Name = "labelNewRRUOperatingVolts";
            this.labelNewRRUOperatingVolts.Size = new System.Drawing.Size(116, 21);
            this.labelNewRRUOperatingVolts.TabIndex = 14;
            this.labelNewRRUOperatingVolts.Text = "Operating Volts:";
            this.labelNewRRUOperatingVolts.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUElectricalRating
            // 
            this.textBoxNewRRUElectricalRating.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUElectricalRating.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUElectricalRating.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUElectricalRating.Location = new System.Drawing.Point(156, 109);
            this.textBoxNewRRUElectricalRating.Name = "textBoxNewRRUElectricalRating";
            this.textBoxNewRRUElectricalRating.Size = new System.Drawing.Size(99, 22);
            this.textBoxNewRRUElectricalRating.TabIndex = 4;
            this.textBoxNewRRUElectricalRating.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUElectricalRating
            // 
            this.labelNewRRUElectricalRating.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUElectricalRating.Location = new System.Drawing.Point(34, 109);
            this.labelNewRRUElectricalRating.Name = "labelNewRRUElectricalRating";
            this.labelNewRRUElectricalRating.Size = new System.Drawing.Size(116, 21);
            this.labelNewRRUElectricalRating.TabIndex = 12;
            this.labelNewRRUElectricalRating.Text = "Electrical Rating:";
            this.labelNewRRUElectricalRating.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUMOP
            // 
            this.textBoxNewRRUMOP.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUMOP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUMOP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUMOP.Location = new System.Drawing.Point(156, 82);
            this.textBoxNewRRUMOP.Name = "textBoxNewRRUMOP";
            this.textBoxNewRRUMOP.Size = new System.Drawing.Size(99, 22);
            this.textBoxNewRRUMOP.TabIndex = 3;
            this.textBoxNewRRUMOP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUMOP
            // 
            this.labelNewRRUMOP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUMOP.Location = new System.Drawing.Point(34, 82);
            this.labelNewRRUMOP.Name = "labelNewRRUMOP";
            this.labelNewRRUMOP.Size = new System.Drawing.Size(116, 21);
            this.labelNewRRUMOP.TabIndex = 10;
            this.labelNewRRUMOP.Text = "MOP:";
            this.labelNewRRUMOP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUMFSMCB
            // 
            this.textBoxNewRRUMFSMCB.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUMFSMCB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUMFSMCB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUMFSMCB.Location = new System.Drawing.Point(156, 55);
            this.textBoxNewRRUMFSMCB.Name = "textBoxNewRRUMFSMCB";
            this.textBoxNewRRUMFSMCB.Size = new System.Drawing.Size(99, 22);
            this.textBoxNewRRUMFSMCB.TabIndex = 2;
            this.textBoxNewRRUMFSMCB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUMFSMCB
            // 
            this.labelNewRRUMFSMCB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUMFSMCB.Location = new System.Drawing.Point(34, 55);
            this.labelNewRRUMFSMCB.Name = "labelNewRRUMFSMCB";
            this.labelNewRRUMFSMCB.Size = new System.Drawing.Size(116, 21);
            this.labelNewRRUMFSMCB.TabIndex = 8;
            this.labelNewRRUMFSMCB.Text = "MFS MCB:";
            this.labelNewRRUMFSMCB.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUMinCKTAmp
            // 
            this.textBoxNewRRUMinCKTAmp.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUMinCKTAmp.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUMinCKTAmp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUMinCKTAmp.Location = new System.Drawing.Point(156, 28);
            this.textBoxNewRRUMinCKTAmp.Name = "textBoxNewRRUMinCKTAmp";
            this.textBoxNewRRUMinCKTAmp.Size = new System.Drawing.Size(99, 22);
            this.textBoxNewRRUMinCKTAmp.TabIndex = 1;
            this.textBoxNewRRUMinCKTAmp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUMinCKTAmp
            // 
            this.labelNewRRUMinCKTAmp.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUMinCKTAmp.Location = new System.Drawing.Point(34, 28);
            this.labelNewRRUMinCKTAmp.Name = "labelNewRRUMinCKTAmp";
            this.labelNewRRUMinCKTAmp.Size = new System.Drawing.Size(116, 21);
            this.labelNewRRUMinCKTAmp.TabIndex = 6;
            this.labelNewRRUMinCKTAmp.Text = "Min CKT Amp:";
            this.labelNewRRUMinCKTAmp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxNewRRUCompressor1
            // 
            this.groupBoxNewRRUCompressor1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewRRUCompressor1.Controls.Add(this.label5);
            this.groupBoxNewRRUCompressor1.Controls.Add(this.textBoxNewRRUComp3Charge);
            this.groupBoxNewRRUCompressor1.Controls.Add(this.textBoxNewRRUComp3LRA);
            this.groupBoxNewRRUCompressor1.Controls.Add(this.textBoxNewRRUComp3RLA);
            this.groupBoxNewRRUCompressor1.Controls.Add(this.textBoxNewRRUComp3PH);
            this.groupBoxNewRRUCompressor1.Controls.Add(this.textBoxNewRRUComp3Qty);
            this.groupBoxNewRRUCompressor1.Controls.Add(this.label4);
            this.groupBoxNewRRUCompressor1.Controls.Add(this.textBoxNewRRUComp2Charge);
            this.groupBoxNewRRUCompressor1.Controls.Add(this.label3);
            this.groupBoxNewRRUCompressor1.Controls.Add(this.textBoxNewRRUComp1Charge);
            this.groupBoxNewRRUCompressor1.Controls.Add(this.textBoxNewRRUComp2LRA);
            this.groupBoxNewRRUCompressor1.Controls.Add(this.labelNewRRUComp1Charge);
            this.groupBoxNewRRUCompressor1.Controls.Add(this.textBoxNewRRUComp1LRA);
            this.groupBoxNewRRUCompressor1.Controls.Add(this.textBoxNewRRUComp2RLAVolts);
            this.groupBoxNewRRUCompressor1.Controls.Add(this.labelNewRRUComp1LRA);
            this.groupBoxNewRRUCompressor1.Controls.Add(this.textBoxNewRRUComp1RLAVolts);
            this.groupBoxNewRRUCompressor1.Controls.Add(this.textBoxNewRRUComp2PH);
            this.groupBoxNewRRUCompressor1.Controls.Add(this.labelNewRRUComp1RLAVolts);
            this.groupBoxNewRRUCompressor1.Controls.Add(this.textboxNewRRUComp1PH);
            this.groupBoxNewRRUCompressor1.Controls.Add(this.textBoxNewRRUComp2Qty);
            this.groupBoxNewRRUCompressor1.Controls.Add(this.labelNewRRUComp1PH);
            this.groupBoxNewRRUCompressor1.Controls.Add(this.textBoxNewRRUComp1Qty);
            this.groupBoxNewRRUCompressor1.Controls.Add(this.labelNewRRUComp1Qty);
            this.groupBoxNewRRUCompressor1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewRRUCompressor1.Location = new System.Drawing.Point(323, 21);
            this.groupBoxNewRRUCompressor1.Name = "groupBoxNewRRUCompressor1";
            this.groupBoxNewRRUCompressor1.Size = new System.Drawing.Size(324, 256);
            this.groupBoxNewRRUCompressor1.TabIndex = 4;
            this.groupBoxNewRRUCompressor1.TabStop = false;
            this.groupBoxNewRRUCompressor1.Text = "Compressors";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(233, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 21);
            this.label5.TabIndex = 20;
            this.label5.Text = "Comp 3";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxNewRRUComp3Charge
            // 
            this.textBoxNewRRUComp3Charge.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUComp3Charge.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUComp3Charge.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUComp3Charge.Location = new System.Drawing.Point(233, 179);
            this.textBoxNewRRUComp3Charge.Name = "textBoxNewRRUComp3Charge";
            this.textBoxNewRRUComp3Charge.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUComp3Charge.TabIndex = 115;
            this.textBoxNewRRUComp3Charge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxNewRRUComp3LRA
            // 
            this.textBoxNewRRUComp3LRA.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUComp3LRA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUComp3LRA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUComp3LRA.Location = new System.Drawing.Point(233, 152);
            this.textBoxNewRRUComp3LRA.Name = "textBoxNewRRUComp3LRA";
            this.textBoxNewRRUComp3LRA.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUComp3LRA.TabIndex = 14;
            this.textBoxNewRRUComp3LRA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxNewRRUComp3RLA
            // 
            this.textBoxNewRRUComp3RLA.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUComp3RLA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUComp3RLA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUComp3RLA.Location = new System.Drawing.Point(233, 125);
            this.textBoxNewRRUComp3RLA.Name = "textBoxNewRRUComp3RLA";
            this.textBoxNewRRUComp3RLA.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUComp3RLA.TabIndex = 13;
            this.textBoxNewRRUComp3RLA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxNewRRUComp3PH
            // 
            this.textBoxNewRRUComp3PH.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUComp3PH.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUComp3PH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUComp3PH.Location = new System.Drawing.Point(233, 98);
            this.textBoxNewRRUComp3PH.Name = "textBoxNewRRUComp3PH";
            this.textBoxNewRRUComp3PH.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUComp3PH.TabIndex = 12;
            this.textBoxNewRRUComp3PH.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxNewRRUComp3Qty
            // 
            this.textBoxNewRRUComp3Qty.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUComp3Qty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUComp3Qty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUComp3Qty.Location = new System.Drawing.Point(233, 71);
            this.textBoxNewRRUComp3Qty.Name = "textBoxNewRRUComp3Qty";
            this.textBoxNewRRUComp3Qty.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUComp3Qty.TabIndex = 11;
            this.textBoxNewRRUComp3Qty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(156, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 21);
            this.label4.TabIndex = 14;
            this.label4.Text = "Comp 2";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxNewRRUComp2Charge
            // 
            this.textBoxNewRRUComp2Charge.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUComp2Charge.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUComp2Charge.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUComp2Charge.Location = new System.Drawing.Point(156, 179);
            this.textBoxNewRRUComp2Charge.Name = "textBoxNewRRUComp2Charge";
            this.textBoxNewRRUComp2Charge.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUComp2Charge.TabIndex = 10;
            this.textBoxNewRRUComp2Charge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(77, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 21);
            this.label3.TabIndex = 13;
            this.label3.Text = "Comp 1";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxNewRRUComp1Charge
            // 
            this.textBoxNewRRUComp1Charge.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUComp1Charge.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUComp1Charge.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUComp1Charge.Location = new System.Drawing.Point(78, 179);
            this.textBoxNewRRUComp1Charge.Name = "textBoxNewRRUComp1Charge";
            this.textBoxNewRRUComp1Charge.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUComp1Charge.TabIndex = 5;
            this.textBoxNewRRUComp1Charge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxNewRRUComp2LRA
            // 
            this.textBoxNewRRUComp2LRA.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUComp2LRA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUComp2LRA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUComp2LRA.Location = new System.Drawing.Point(156, 152);
            this.textBoxNewRRUComp2LRA.Name = "textBoxNewRRUComp2LRA";
            this.textBoxNewRRUComp2LRA.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUComp2LRA.TabIndex = 9;
            this.textBoxNewRRUComp2LRA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUComp1Charge
            // 
            this.labelNewRRUComp1Charge.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUComp1Charge.Location = new System.Drawing.Point(12, 180);
            this.labelNewRRUComp1Charge.Name = "labelNewRRUComp1Charge";
            this.labelNewRRUComp1Charge.Size = new System.Drawing.Size(60, 21);
            this.labelNewRRUComp1Charge.TabIndex = 12;
            this.labelNewRRUComp1Charge.Text = "Charge:";
            this.labelNewRRUComp1Charge.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUComp1LRA
            // 
            this.textBoxNewRRUComp1LRA.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUComp1LRA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUComp1LRA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUComp1LRA.Location = new System.Drawing.Point(78, 152);
            this.textBoxNewRRUComp1LRA.Name = "textBoxNewRRUComp1LRA";
            this.textBoxNewRRUComp1LRA.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUComp1LRA.TabIndex = 4;
            this.textBoxNewRRUComp1LRA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxNewRRUComp2RLAVolts
            // 
            this.textBoxNewRRUComp2RLAVolts.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUComp2RLAVolts.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUComp2RLAVolts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUComp2RLAVolts.Location = new System.Drawing.Point(156, 125);
            this.textBoxNewRRUComp2RLAVolts.Name = "textBoxNewRRUComp2RLAVolts";
            this.textBoxNewRRUComp2RLAVolts.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUComp2RLAVolts.TabIndex = 8;
            this.textBoxNewRRUComp2RLAVolts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUComp1LRA
            // 
            this.labelNewRRUComp1LRA.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUComp1LRA.Location = new System.Drawing.Point(13, 153);
            this.labelNewRRUComp1LRA.Name = "labelNewRRUComp1LRA";
            this.labelNewRRUComp1LRA.Size = new System.Drawing.Size(60, 21);
            this.labelNewRRUComp1LRA.TabIndex = 10;
            this.labelNewRRUComp1LRA.Text = "LRA:";
            this.labelNewRRUComp1LRA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUComp1RLAVolts
            // 
            this.textBoxNewRRUComp1RLAVolts.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUComp1RLAVolts.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUComp1RLAVolts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUComp1RLAVolts.Location = new System.Drawing.Point(78, 125);
            this.textBoxNewRRUComp1RLAVolts.Name = "textBoxNewRRUComp1RLAVolts";
            this.textBoxNewRRUComp1RLAVolts.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUComp1RLAVolts.TabIndex = 3;
            this.textBoxNewRRUComp1RLAVolts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxNewRRUComp2PH
            // 
            this.textBoxNewRRUComp2PH.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUComp2PH.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUComp2PH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUComp2PH.Location = new System.Drawing.Point(156, 98);
            this.textBoxNewRRUComp2PH.Name = "textBoxNewRRUComp2PH";
            this.textBoxNewRRUComp2PH.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUComp2PH.TabIndex = 7;
            this.textBoxNewRRUComp2PH.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUComp1RLAVolts
            // 
            this.labelNewRRUComp1RLAVolts.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUComp1RLAVolts.Location = new System.Drawing.Point(12, 125);
            this.labelNewRRUComp1RLAVolts.Name = "labelNewRRUComp1RLAVolts";
            this.labelNewRRUComp1RLAVolts.Size = new System.Drawing.Size(60, 21);
            this.labelNewRRUComp1RLAVolts.TabIndex = 8;
            this.labelNewRRUComp1RLAVolts.Text = "RLA-Volts:";
            this.labelNewRRUComp1RLAVolts.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textboxNewRRUComp1PH
            // 
            this.textboxNewRRUComp1PH.BackColor = System.Drawing.Color.White;
            this.textboxNewRRUComp1PH.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textboxNewRRUComp1PH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textboxNewRRUComp1PH.Location = new System.Drawing.Point(78, 98);
            this.textboxNewRRUComp1PH.Name = "textboxNewRRUComp1PH";
            this.textboxNewRRUComp1PH.Size = new System.Drawing.Size(60, 22);
            this.textboxNewRRUComp1PH.TabIndex = 2;
            this.textboxNewRRUComp1PH.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxNewRRUComp2Qty
            // 
            this.textBoxNewRRUComp2Qty.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUComp2Qty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUComp2Qty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUComp2Qty.Location = new System.Drawing.Point(156, 71);
            this.textBoxNewRRUComp2Qty.Name = "textBoxNewRRUComp2Qty";
            this.textBoxNewRRUComp2Qty.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUComp2Qty.TabIndex = 6;
            this.textBoxNewRRUComp2Qty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUComp1PH
            // 
            this.labelNewRRUComp1PH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUComp1PH.Location = new System.Drawing.Point(12, 98);
            this.labelNewRRUComp1PH.Name = "labelNewRRUComp1PH";
            this.labelNewRRUComp1PH.Size = new System.Drawing.Size(60, 21);
            this.labelNewRRUComp1PH.TabIndex = 6;
            this.labelNewRRUComp1PH.Text = "PH:";
            this.labelNewRRUComp1PH.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUComp1Qty
            // 
            this.textBoxNewRRUComp1Qty.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUComp1Qty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUComp1Qty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUComp1Qty.Location = new System.Drawing.Point(78, 71);
            this.textBoxNewRRUComp1Qty.Name = "textBoxNewRRUComp1Qty";
            this.textBoxNewRRUComp1Qty.Size = new System.Drawing.Size(60, 22);
            this.textBoxNewRRUComp1Qty.TabIndex = 1;
            this.textBoxNewRRUComp1Qty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUComp1Qty
            // 
            this.labelNewRRUComp1Qty.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUComp1Qty.Location = new System.Drawing.Point(12, 71);
            this.labelNewRRUComp1Qty.Name = "labelNewRRUComp1Qty";
            this.labelNewRRUComp1Qty.Size = new System.Drawing.Size(60, 21);
            this.labelNewRRUComp1Qty.TabIndex = 4;
            this.labelNewRRUComp1Qty.Text = "Qty:";
            this.labelNewRRUComp1Qty.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxNewRRULineInfo
            // 
            this.groupBoxNewRRULineInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewRRULineInfo.Controls.Add(this.comboBoxNewRRUHeatingType);
            this.groupBoxNewRRULineInfo.Controls.Add(this.labelNewRRUHeatingType);
            this.groupBoxNewRRULineInfo.Controls.Add(this.labelNewRRUPartNum);
            this.groupBoxNewRRULineInfo.Controls.Add(this.textBoxNewRRUPartNum);
            this.groupBoxNewRRULineInfo.Controls.Add(this.textBoxNewRRUVerifiedBy);
            this.groupBoxNewRRULineInfo.Controls.Add(this.labelMewRRUCreatedBy);
            this.groupBoxNewRRULineInfo.Controls.Add(this.comboBoxNewRRUModelNo);
            this.groupBoxNewRRULineInfo.Controls.Add(this.labelNewRRUSerialNo);
            this.groupBoxNewRRULineInfo.Controls.Add(this.textBoxNewRRUSerialNo);
            this.groupBoxNewRRULineInfo.Controls.Add(this.labelNewRRUModelNum);
            this.groupBoxNewRRULineInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewRRULineInfo.Location = new System.Drawing.Point(6, 132);
            this.groupBoxNewRRULineInfo.Name = "groupBoxNewRRULineInfo";
            this.groupBoxNewRRULineInfo.Size = new System.Drawing.Size(311, 145);
            this.groupBoxNewRRULineInfo.TabIndex = 1;
            this.groupBoxNewRRULineInfo.TabStop = false;
            this.groupBoxNewRRULineInfo.Text = "Unit Info";
            // 
            // comboBoxNewRRUHeatingType
            // 
            this.comboBoxNewRRUHeatingType.BackColor = System.Drawing.Color.White;
            this.comboBoxNewRRUHeatingType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxNewRRUHeatingType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.comboBoxNewRRUHeatingType.FormattingEnabled = true;
            this.comboBoxNewRRUHeatingType.Items.AddRange(new object[] {
            "Direct Fire",
            "Indirect Fire",
            "Electric",
            "NoHeat"});
            this.comboBoxNewRRUHeatingType.Location = new System.Drawing.Point(98, 100);
            this.comboBoxNewRRUHeatingType.Name = "comboBoxNewRRUHeatingType";
            this.comboBoxNewRRUHeatingType.Size = new System.Drawing.Size(190, 22);
            this.comboBoxNewRRUHeatingType.TabIndex = 5;
            this.comboBoxNewRRUHeatingType.SelectedIndexChanged += new System.EventHandler(this.comboBoxNewRRUHeatingType_SelectedIndexChanged);
            // 
            // labelNewRRUHeatingType
            // 
            this.labelNewRRUHeatingType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUHeatingType.Location = new System.Drawing.Point(7, 101);
            this.labelNewRRUHeatingType.Name = "labelNewRRUHeatingType";
            this.labelNewRRUHeatingType.Size = new System.Drawing.Size(88, 21);
            this.labelNewRRUHeatingType.TabIndex = 67;
            this.labelNewRRUHeatingType.Text = "Heating Type:";
            this.labelNewRRUHeatingType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelNewRRUPartNum
            // 
            this.labelNewRRUPartNum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUPartNum.Location = new System.Drawing.Point(20, 73);
            this.labelNewRRUPartNum.Name = "labelNewRRUPartNum";
            this.labelNewRRUPartNum.Size = new System.Drawing.Size(75, 21);
            this.labelNewRRUPartNum.TabIndex = 65;
            this.labelNewRRUPartNum.Text = "Part Num:";
            this.labelNewRRUPartNum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUPartNum
            // 
            this.textBoxNewRRUPartNum.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUPartNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUPartNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUPartNum.Location = new System.Drawing.Point(98, 73);
            this.textBoxNewRRUPartNum.Name = "textBoxNewRRUPartNum";
            this.textBoxNewRRUPartNum.Size = new System.Drawing.Size(108, 22);
            this.textBoxNewRRUPartNum.TabIndex = 3;
            this.textBoxNewRRUPartNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxNewRRUVerifiedBy
            // 
            this.textBoxNewRRUVerifiedBy.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUVerifiedBy.Enabled = false;
            this.textBoxNewRRUVerifiedBy.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUVerifiedBy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUVerifiedBy.Location = new System.Drawing.Point(221, 73);
            this.textBoxNewRRUVerifiedBy.Name = "textBoxNewRRUVerifiedBy";
            this.textBoxNewRRUVerifiedBy.Size = new System.Drawing.Size(67, 22);
            this.textBoxNewRRUVerifiedBy.TabIndex = 4;
            this.textBoxNewRRUVerifiedBy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxNewRRUVerifiedBy.Visible = false;
            // 
            // labelMewRRUCreatedBy
            // 
            this.labelMewRRUCreatedBy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMewRRUCreatedBy.Location = new System.Drawing.Point(218, 48);
            this.labelMewRRUCreatedBy.Name = "labelMewRRUCreatedBy";
            this.labelMewRRUCreatedBy.Size = new System.Drawing.Size(75, 20);
            this.labelMewRRUCreatedBy.TabIndex = 12;
            this.labelMewRRUCreatedBy.Text = "Created By:";
            this.labelMewRRUCreatedBy.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelMewRRUCreatedBy.Visible = false;
            // 
            // comboBoxNewRRUModelNo
            // 
            this.comboBoxNewRRUModelNo.BackColor = System.Drawing.Color.White;
            this.comboBoxNewRRUModelNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxNewRRUModelNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.comboBoxNewRRUModelNo.FormattingEnabled = true;
            this.comboBoxNewRRUModelNo.ItemHeight = 14;
            this.comboBoxNewRRUModelNo.Location = new System.Drawing.Point(98, 18);
            this.comboBoxNewRRUModelNo.Name = "comboBoxNewRRUModelNo";
            this.comboBoxNewRRUModelNo.Size = new System.Drawing.Size(183, 22);
            this.comboBoxNewRRUModelNo.TabIndex = 1;
            this.comboBoxNewRRUModelNo.SelectedIndexChanged += new System.EventHandler(this.comboBoxNewRRUModelNo_SelectedIndexChanged);
            // 
            // labelNewRRUSerialNo
            // 
            this.labelNewRRUSerialNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUSerialNo.Location = new System.Drawing.Point(20, 46);
            this.labelNewRRUSerialNo.Name = "labelNewRRUSerialNo";
            this.labelNewRRUSerialNo.Size = new System.Drawing.Size(75, 21);
            this.labelNewRRUSerialNo.TabIndex = 7;
            this.labelNewRRUSerialNo.Text = "Serial No:";
            this.labelNewRRUSerialNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUSerialNo
            // 
            this.textBoxNewRRUSerialNo.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUSerialNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUSerialNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUSerialNo.Location = new System.Drawing.Point(98, 46);
            this.textBoxNewRRUSerialNo.Name = "textBoxNewRRUSerialNo";
            this.textBoxNewRRUSerialNo.Size = new System.Drawing.Size(108, 22);
            this.textBoxNewRRUSerialNo.TabIndex = 2;
            this.textBoxNewRRUSerialNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelNewRRUModelNum
            // 
            this.labelNewRRUModelNum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUModelNum.Location = new System.Drawing.Point(20, 19);
            this.labelNewRRUModelNum.Name = "labelNewRRUModelNum";
            this.labelNewRRUModelNum.Size = new System.Drawing.Size(75, 21);
            this.labelNewRRUModelNum.TabIndex = 4;
            this.labelNewRRUModelNum.Text = "Model Type:";
            this.labelNewRRUModelNum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxNewRRUJobInfo
            // 
            this.groupBoxNewRRUJobInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxNewRRUJobInfo.Controls.Add(this.textBoxNewRRUJobNum);
            this.groupBoxNewRRUJobInfo.Controls.Add(this.label2);
            this.groupBoxNewRRUJobInfo.Controls.Add(this.textBoxNewRRUEndConsumer);
            this.groupBoxNewRRUJobInfo.Controls.Add(this.labelNewRRUEndConsumer);
            this.groupBoxNewRRUJobInfo.Controls.Add(this.textBoxNewRRUReleaseNum);
            this.groupBoxNewRRUJobInfo.Controls.Add(this.labelNewRRUSlash);
            this.groupBoxNewRRUJobInfo.Controls.Add(this.textBoxNewRRUCustomer);
            this.groupBoxNewRRUJobInfo.Controls.Add(this.textBoxNewRRUOrderNum);
            this.groupBoxNewRRUJobInfo.Controls.Add(this.labelNewRRUCustomer);
            this.groupBoxNewRRUJobInfo.Controls.Add(this.labelNewRRUOrderNum);
            this.groupBoxNewRRUJobInfo.Controls.Add(this.textBoxNewRRULineItem);
            this.groupBoxNewRRUJobInfo.Controls.Add(this.labelNewRRULineItem);
            this.groupBoxNewRRUJobInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNewRRUJobInfo.Location = new System.Drawing.Point(6, 22);
            this.groupBoxNewRRUJobInfo.Name = "groupBoxNewRRUJobInfo";
            this.groupBoxNewRRUJobInfo.Size = new System.Drawing.Size(311, 105);
            this.groupBoxNewRRUJobInfo.TabIndex = 0;
            this.groupBoxNewRRUJobInfo.TabStop = false;
            this.groupBoxNewRRUJobInfo.Text = "Order Info";
            // 
            // textBoxNewRRUJobNum
            // 
            this.textBoxNewRRUJobNum.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUJobNum.Enabled = false;
            this.textBoxNewRRUJobNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUJobNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUJobNum.Location = new System.Drawing.Point(208, 72);
            this.textBoxNewRRUJobNum.Name = "textBoxNewRRUJobNum";
            this.textBoxNewRRUJobNum.Size = new System.Drawing.Size(97, 22);
            this.textBoxNewRRUJobNum.TabIndex = 6;
            this.textBoxNewRRUJobNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(168, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 21);
            this.label2.TabIndex = 9;
            this.label2.Text = "Job #:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxNewRRUEndConsumer
            // 
            this.textBoxNewRRUEndConsumer.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUEndConsumer.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUEndConsumer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUEndConsumer.Location = new System.Drawing.Point(95, 72);
            this.textBoxNewRRUEndConsumer.Name = "textBoxNewRRUEndConsumer";
            this.textBoxNewRRUEndConsumer.Size = new System.Drawing.Size(73, 22);
            this.textBoxNewRRUEndConsumer.TabIndex = 5;
            // 
            // labelNewRRUEndConsumer
            // 
            this.labelNewRRUEndConsumer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUEndConsumer.Location = new System.Drawing.Point(-9, 73);
            this.labelNewRRUEndConsumer.Name = "labelNewRRUEndConsumer";
            this.labelNewRRUEndConsumer.Size = new System.Drawing.Size(104, 21);
            this.labelNewRRUEndConsumer.TabIndex = 7;
            this.labelNewRRUEndConsumer.Text = "End Consumer:";
            this.labelNewRRUEndConsumer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxNewRRUReleaseNum
            // 
            this.textBoxNewRRUReleaseNum.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUReleaseNum.Enabled = false;
            this.textBoxNewRRUReleaseNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUReleaseNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUReleaseNum.Location = new System.Drawing.Point(284, 19);
            this.textBoxNewRRUReleaseNum.Name = "textBoxNewRRUReleaseNum";
            this.textBoxNewRRUReleaseNum.Size = new System.Drawing.Size(21, 22);
            this.textBoxNewRRUReleaseNum.TabIndex = 3;
            this.textBoxNewRRUReleaseNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRUSlash
            // 
            this.labelNewRRUSlash.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUSlash.Location = new System.Drawing.Point(270, 18);
            this.labelNewRRUSlash.Name = "labelNewRRUSlash";
            this.labelNewRRUSlash.Size = new System.Drawing.Size(15, 21);
            this.labelNewRRUSlash.TabIndex = 4;
            this.labelNewRRUSlash.Text = "/";
            this.labelNewRRUSlash.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxNewRRUCustomer
            // 
            this.textBoxNewRRUCustomer.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUCustomer.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUCustomer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUCustomer.Location = new System.Drawing.Point(95, 46);
            this.textBoxNewRRUCustomer.Name = "textBoxNewRRUCustomer";
            this.textBoxNewRRUCustomer.Size = new System.Drawing.Size(210, 22);
            this.textBoxNewRRUCustomer.TabIndex = 4;
            // 
            // textBoxNewRRUOrderNum
            // 
            this.textBoxNewRRUOrderNum.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRUOrderNum.Enabled = false;
            this.textBoxNewRRUOrderNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRUOrderNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRUOrderNum.Location = new System.Drawing.Point(95, 19);
            this.textBoxNewRRUOrderNum.Name = "textBoxNewRRUOrderNum";
            this.textBoxNewRRUOrderNum.Size = new System.Drawing.Size(50, 22);
            this.textBoxNewRRUOrderNum.TabIndex = 1;
            this.textBoxNewRRUOrderNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelNewRRUCustomer
            // 
            this.labelNewRRUCustomer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUCustomer.Location = new System.Drawing.Point(4, 46);
            this.labelNewRRUCustomer.Name = "labelNewRRUCustomer";
            this.labelNewRRUCustomer.Size = new System.Drawing.Size(85, 21);
            this.labelNewRRUCustomer.TabIndex = 2;
            this.labelNewRRUCustomer.Text = "Customer:";
            this.labelNewRRUCustomer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelNewRRUOrderNum
            // 
            this.labelNewRRUOrderNum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRUOrderNum.Location = new System.Drawing.Point(4, 20);
            this.labelNewRRUOrderNum.Name = "labelNewRRUOrderNum";
            this.labelNewRRUOrderNum.Size = new System.Drawing.Size(85, 21);
            this.labelNewRRUOrderNum.TabIndex = 1;
            this.labelNewRRUOrderNum.Text = "Sales Order#:";
            this.labelNewRRUOrderNum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxNewRRULineItem
            // 
            this.textBoxNewRRULineItem.BackColor = System.Drawing.Color.White;
            this.textBoxNewRRULineItem.Enabled = false;
            this.textBoxNewRRULineItem.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewRRULineItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewRRULineItem.Location = new System.Drawing.Point(246, 19);
            this.textBoxNewRRULineItem.Name = "textBoxNewRRULineItem";
            this.textBoxNewRRULineItem.Size = new System.Drawing.Size(21, 22);
            this.textBoxNewRRULineItem.TabIndex = 2;
            this.textBoxNewRRULineItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelNewRRULineItem
            // 
            this.labelNewRRULineItem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewRRULineItem.Location = new System.Drawing.Point(155, 19);
            this.labelNewRRULineItem.Name = "labelNewRRULineItem";
            this.labelNewRRULineItem.Size = new System.Drawing.Size(85, 21);
            this.labelNewRRULineItem.TabIndex = 2;
            this.labelNewRRULineItem.Text = "Line #/Rel #:";
            this.labelNewRRULineItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // menuStripRRUFile
            // 
            this.menuStripRRUFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.menuStripRRUFile.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStripRRUFile.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItemRRU});
            this.menuStripRRUFile.Location = new System.Drawing.Point(0, 0);
            this.menuStripRRUFile.Name = "menuStripRRUFile";
            this.menuStripRRUFile.Size = new System.Drawing.Size(847, 24);
            this.menuStripRRUFile.TabIndex = 2;
            // 
            // fileToolStripMenuItemRRU
            // 
            this.fileToolStripMenuItemRRU.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.fileToolStripMenuItemRRU.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItemRRU.Name = "fileToolStripMenuItemRRU";
            this.fileToolStripMenuItemRRU.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItemRRU.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // frmNewLineRRU
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(847, 998);
            this.Controls.Add(this.groupBoxNewRRU);
            this.Controls.Add(this.menuStripRRUFile);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.MainMenuStrip = this.menuStripRRUFile;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmNewLineRRU";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New RRU Sales Order#";
            this.groupBoxNewRRU.ResumeLayout(false);
            this.groupBoxNewRRUFridgeType.ResumeLayout(false);
            this.groupBoxNewRRUFridgeType.PerformLayout();
            this.groupBoxNewRRUDirectFire.ResumeLayout(false);
            this.groupBoxNewRRUDirectFire.PerformLayout();
            this.groupBoxNewRRUIndirectFire.ResumeLayout(false);
            this.groupBoxNewRRUIndirectFire.PerformLayout();
            this.groupBoxNewRRUFanInfo.ResumeLayout(false);
            this.groupBoxNewRRUFanInfo.PerformLayout();
            this.groupBoxNewRRUElectricInfo.ResumeLayout(false);
            this.groupBoxNewRRUElectricInfo.PerformLayout();
            this.groupBoxNewRRUCompressor1.ResumeLayout(false);
            this.groupBoxNewRRUCompressor1.PerformLayout();
            this.groupBoxNewRRULineInfo.ResumeLayout(false);
            this.groupBoxNewRRULineInfo.PerformLayout();
            this.groupBoxNewRRUJobInfo.ResumeLayout(false);
            this.groupBoxNewRRUJobInfo.PerformLayout();
            this.menuStripRRUFile.ResumeLayout(false);
            this.menuStripRRUFile.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label labelNewRRUMsg;
        public System.Windows.Forms.Button buttonNewRRUPrint;
        public System.Windows.Forms.Button buttonNewRRUAccept;
        public System.Windows.Forms.Button buttonNewRRUCancel;
        private System.Windows.Forms.GroupBox groupBoxNewRRUFanInfo;
        public System.Windows.Forms.TextBox textBoxNewRRUFanOvrHP;
        private System.Windows.Forms.Label labelNewRRUFanOvrHP;
        public System.Windows.Forms.TextBox textBoxNewRRUFanOvrFLA;
        private System.Windows.Forms.Label labelNewRRUFanOvrFLA;
        public System.Windows.Forms.TextBox textBoxNewRRUFanOvrPH;
        private System.Windows.Forms.Label labelNewRRUFanOvrPH;
        public System.Windows.Forms.TextBox textBoxNewRRUFanOvrQty;
        private System.Windows.Forms.Label labelNewRRUFanOvrQty;
        public System.Windows.Forms.TextBox textBoxNewRRUFanStdHP;
        private System.Windows.Forms.Label labelNewRRUFanStdHP;
        public System.Windows.Forms.TextBox textBoxNewRRUFanStdFLA;
        private System.Windows.Forms.Label labelNewRRUFanStdFLA;
        public System.Windows.Forms.TextBox textBoxNewRRUFanStdPH;
        private System.Windows.Forms.Label labelNewRRUFanStdPH;
        public System.Windows.Forms.TextBox textBoxNewRRUFanStdQty;
        private System.Windows.Forms.Label labelNewRRUFanStdQty;
        public System.Windows.Forms.TextBox textBoxNewRRUFanCondHP;
        private System.Windows.Forms.Label labelNewRRUFanCondHP;
        public System.Windows.Forms.TextBox textBoxNewRRUFanCondFLA;
        private System.Windows.Forms.Label labelNewRRUFanCondFLA;
        public System.Windows.Forms.TextBox textBoxNewRRUFanCondPH;
        private System.Windows.Forms.Label labelNewRRUFanCondPH;
        public System.Windows.Forms.TextBox textBoxNewRRUFanCondQty;
        private System.Windows.Forms.Label labelNewRRUFanCondQty;
        public System.Windows.Forms.TextBox textBoxNewRRUComp2Charge;
        public System.Windows.Forms.TextBox textBoxNewRRUComp2LRA;
        public System.Windows.Forms.TextBox textBoxNewRRUComp2RLAVolts;
        public System.Windows.Forms.TextBox textBoxNewRRUComp2PH;
        public System.Windows.Forms.TextBox textBoxNewRRUComp2Qty;
        private System.Windows.Forms.GroupBox groupBoxNewRRUCompressor1;
        public System.Windows.Forms.TextBox textBoxNewRRUComp1Charge;
        private System.Windows.Forms.Label labelNewRRUComp1Charge;
        public System.Windows.Forms.TextBox textBoxNewRRUComp1LRA;
        private System.Windows.Forms.Label labelNewRRUComp1LRA;
        public System.Windows.Forms.TextBox textBoxNewRRUComp1RLAVolts;
        private System.Windows.Forms.Label labelNewRRUComp1RLAVolts;
        public System.Windows.Forms.TextBox textboxNewRRUComp1PH;
        private System.Windows.Forms.Label labelNewRRUComp1PH;
        public System.Windows.Forms.TextBox textBoxNewRRUComp1Qty;
        private System.Windows.Forms.Label labelNewRRUComp1Qty;
        private System.Windows.Forms.GroupBox groupBoxNewRRUElectricInfo;
        public System.Windows.Forms.TextBox textBoxNewRRUTestPressureLow;
        private System.Windows.Forms.Label labelNewRRUTestPressureLow;
        public System.Windows.Forms.TextBox textBoxNewRRUTestPressureHigh;
        private System.Windows.Forms.Label labelNewRRUTestPressureHigh;
        public System.Windows.Forms.TextBox textBoxNewRRUOperatingVolts;
        private System.Windows.Forms.Label labelNewRRUOperatingVolts;
        public System.Windows.Forms.TextBox textBoxNewRRUElectricalRating;
        private System.Windows.Forms.Label labelNewRRUElectricalRating;
        public System.Windows.Forms.TextBox textBoxNewRRUMOP;
        private System.Windows.Forms.Label labelNewRRUMOP;
        public System.Windows.Forms.TextBox textBoxNewRRUMFSMCB;
        private System.Windows.Forms.Label labelNewRRUMFSMCB;
        public System.Windows.Forms.TextBox textBoxNewRRUMinCKTAmp;
        private System.Windows.Forms.Label labelNewRRUMinCKTAmp;
        public System.Windows.Forms.GroupBox groupBoxNewRRULineInfo;
        public System.Windows.Forms.TextBox textBoxNewRRUVerifiedBy;
        public System.Windows.Forms.ComboBox comboBoxNewRRUModelNo;
        private System.Windows.Forms.Label labelNewRRUSerialNo;
        public System.Windows.Forms.TextBox textBoxNewRRUSerialNo;
        private System.Windows.Forms.Label labelNewRRUModelNum;
        private System.Windows.Forms.GroupBox groupBoxNewRRUJobInfo;
        public System.Windows.Forms.TextBox textBoxNewRRUReleaseNum;
        private System.Windows.Forms.Label labelNewRRUSlash;
        public System.Windows.Forms.TextBox textBoxNewRRUCustomer;
        public System.Windows.Forms.TextBox textBoxNewRRUOrderNum;
        private System.Windows.Forms.Label labelNewRRUCustomer;
        private System.Windows.Forms.Label labelNewRRUOrderNum;
        public System.Windows.Forms.TextBox textBoxNewRRULineItem;
        private System.Windows.Forms.Label labelNewRRULineItem;
        public System.Windows.Forms.TextBox textBoxNewRRUHeatingType;
        private System.Windows.Forms.Label labelNewRRUHeatingTypeInfo;
        public System.Windows.Forms.TextBox textBoxNewRRUHeatingInput;
        private System.Windows.Forms.Label labelNeRRUHeatingInput;
        private System.Windows.Forms.MenuStrip menuStripRRUFile;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItemRRU;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        public System.Windows.Forms.GroupBox groupBoxNewRRUIndirectFire;
        public System.Windows.Forms.TextBox textBoxNewRRUIFTempRise;
        private System.Windows.Forms.Label labelNewRRUIndFireTempRise;
        public System.Windows.Forms.TextBox textBoxNewRRUIFManifoldPressure;
        private System.Windows.Forms.Label labelNewRRUIndFireManifoldPres;
        public System.Windows.Forms.TextBox textBoxNewRRUIFMaxGasPressure;
        private System.Windows.Forms.Label labelNewRRUIndFireMaxGasPressure;
        public System.Windows.Forms.TextBox textBoxNewRRUIFMinGasPressure;
        private System.Windows.Forms.Label labelNewRRUIndFireMinGasPressure;
        public System.Windows.Forms.TextBox textBoxNewRRUIFMaxOutAirTemp;
        private System.Windows.Forms.Label labelNewRRUIndFireMaxOutAirTemp;
        public System.Windows.Forms.TextBox textBoxNewRRUIFMaxExt;
        private System.Windows.Forms.Label labelNewRRUIndFireMaxEst;
        public System.Windows.Forms.TextBox textBoxNewRRUIFMinInputBTU;
        private System.Windows.Forms.Label labelNewRRUIndFireMinHtgInput;
        public System.Windows.Forms.TextBox textBoxNewRRUIFHeatingOutput;
        private System.Windows.Forms.Label labelNewRRUIndFireHeatingOutputBTUH;
        public System.Windows.Forms.TextBox textBoxNewRRUIFHeatingInputBTUH;
        private System.Windows.Forms.Label labelNewRRUIndFireHHeatingInput;
        public System.Windows.Forms.TextBox textBoxNewRRUIFMaxHtgInputBTUH;
        private System.Windows.Forms.Label labelNewRRUIndirectFireMaxHtgInputBTUH;
        public System.Windows.Forms.TextBox textBoxNewRRUDFManifoldPressure;
        private System.Windows.Forms.Label labelNewRRUDFManifoldPressure;
        public System.Windows.Forms.TextBox textBoxNewRRUDFTempRise;
        private System.Windows.Forms.Label labelNewRRUDFTempRise;
        public System.Windows.Forms.TextBox textBoxNewRRUDFMinPressureDrop;
        private System.Windows.Forms.Label labelNewRRUDFMinPressureDrop;
        public System.Windows.Forms.TextBox textBoxNewRRUDFMinGasPressure;
        private System.Windows.Forms.Label labelNewRRUDFMinGasPressure;
        public System.Windows.Forms.TextBox textBoxNewRRUDFMaxPressureDrop;
        private System.Windows.Forms.Label labelNewRRUDFMaxPressureDrop;
        public System.Windows.Forms.TextBox textBoxNewRRUDFMaxGasPressure;
        private System.Windows.Forms.Label labelNewRRUDFMaxGasPressure;
        public System.Windows.Forms.TextBox textboxNewRRUDFMinHtgInput;
        private System.Windows.Forms.Label labelNewRRUDFMinHtgInput;
        public System.Windows.Forms.TextBox textBoxNewRRUDFMaxHtgInputBTUH;
        private System.Windows.Forms.Label labelNewRRUDFMaxHtgInputBTUH;
        public System.Windows.Forms.TextBox textBoxNewRRUDFStaticPressure;
        private System.Windows.Forms.Label labelNewRRUDFStaticPressure;
        public System.Windows.Forms.TextBox textBoxNewRRUDFEquiptedAirflow;
        private System.Windows.Forms.Label labelNewRRUDFEquiptedAirflow;
        public System.Windows.Forms.TextBox textBoxNewRRUDFCutOutTemp;
        private System.Windows.Forms.Label labelNewRRUDFCutOutTemp;
        public System.Windows.Forms.GroupBox groupBoxNewRRUDirectFire;
        private System.Windows.Forms.Label labelNewRRUPartNum;
        public System.Windows.Forms.TextBox textBoxNewRRUPartNum;
        public System.Windows.Forms.ComboBox comboBoxNewRRUHeatingType;
        private System.Windows.Forms.Label labelNewRRUHeatingType;
        public System.Windows.Forms.GroupBox groupBoxNewRRU;
        private System.Windows.Forms.GroupBox groupBoxNewRRUFridgeType;
        public System.Windows.Forms.RadioButton radioButtonNewRRUR410A;
        public System.Windows.Forms.Label labelMewRRUCreatedBy;
        public System.Windows.Forms.CheckBox checkBoxNewRRUFrench;
        public System.Windows.Forms.CheckBox checkBoxModelTypeOverride;
        public System.Windows.Forms.TextBox textBoxNewRRUEndConsumer;
        private System.Windows.Forms.Label labelNewRRUEndConsumer;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox comboBoxNewRRUIFHeatingFuelType;
        public System.Windows.Forms.TextBox textBoxNewRRUJobNum;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox textBoxNewRRUComp3Charge;
        public System.Windows.Forms.TextBox textBoxNewRRUComp3LRA;
        public System.Windows.Forms.TextBox textBoxNewRRUComp3RLA;
        public System.Windows.Forms.TextBox textBoxNewRRUComp3PH;
        public System.Windows.Forms.TextBox textBoxNewRRUComp3Qty;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;

    }
}