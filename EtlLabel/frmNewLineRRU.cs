using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using System.Configuration;

namespace KCC.OA.Etl
{
    public partial class frmNewLineRRU : Form
    {
        string HeatingInput = "";
        string EpicorServer = "";

        public frmNewLineRRU()
        {
            InitializeComponent();
            if (textBoxNewRRUHeatingInput.Text != "")
            {
                HeatingInput = textBoxNewRRUHeatingInput.Text;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonNewRRUCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBoxNewRRUHeatingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.groupBoxNewRRUDirectFire.Visible = false;
            this.groupBoxNewRRUIndirectFire.Visible = false;

            if (textBoxNewRRUHeatingInput.Text != "")
            {
                HeatingInput = textBoxNewRRUHeatingInput.Text;
            }

            if (this.comboBoxNewRRUHeatingType.SelectedIndex == 0)
            {                
                this.groupBoxNewRRUDirectFire.Location = new Point(6, 274);               
                this.groupBoxNewRRUDirectFire.Visible = true;
                this.groupBoxNewRRUDirectFire.BringToFront();
            }
            else if (this.comboBoxNewRRUHeatingType.SelectedIndex == 1)
            {                
                this.groupBoxNewRRUIndirectFire.Location = new Point(6, 274);
                this.groupBoxNewRRUIndirectFire.Visible = true;
                this.groupBoxNewRRUIndirectFire.BringToFront();
            }
            else if (this.comboBoxNewRRUHeatingType.SelectedIndex == 2)
            {
                this.textBoxNewRRUHeatingType.Text = "Electric";
                textBoxNewRRUHeatingInput.Text = HeatingInput;     
                      
            }
            else if (this.comboBoxNewRRUHeatingType.SelectedIndex == 3)
            {
                this.textBoxNewRRUHeatingType.Text = "NoHeat";
                textBoxNewRRUHeatingInput.Text = "";                   
            }
        }

        //private void comboBoxNewRRUVoltage_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    
        //}

        private void buttonNewRRUAccept_Click(object sender, EventArgs e)
        {

            if (GlobalModeType.ModeType == "New")
            {
                addNewLineOrderDtlRRU();
            }
            else
            {
                updateOrderDtlRRU();
            }
        }

        private void updateOrderDtlRRU()
        {
            string jobNumStr;
            int orderNumInt;
            int orderLineInt;
            int releaseNumInt;
            int slashLocInt;

            string frenchStr;
            string verifiedByStr;
            string heatFuelTypeStr;
            string heatInputTypeStr = "";

            VS_DataSetTableAdapters.EtlRRUTableAdapter etlRRUTA =
                                    new VS_DataSetTableAdapters.EtlRRUTableAdapter();

            etlRRUTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

            jobNumStr = this.textBoxNewRRUJobNum.Text;
            orderNumInt = Int32.Parse(this.textBoxNewRRUOrderNum.Text);
            orderLineInt = Int32.Parse(this.textBoxNewRRULineItem.Text);
            releaseNumInt = Int32.Parse(this.textBoxNewRRUReleaseNum.Text);

            if (this.textBoxNewRRUSerialNo.Text == "")
            {
                MessageBox.Show("ERROR - Serial No is a required field.");
                this.textBoxNewRRUSerialNo.Focus();
                this.textBoxNewRRUSerialNo.Select();
                return;
            }

            verifiedByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
            if (verifiedByStr != "")
            {
                slashLocInt = verifiedByStr.IndexOf('\\');
                if (slashLocInt != -1)
                {
                    ++slashLocInt;
                    verifiedByStr = verifiedByStr.Substring(slashLocInt, (verifiedByStr.Length - slashLocInt));
                }
            }

            if (this.checkBoxNewRRUFrench.Checked == true)
            {
                frenchStr = "True";

            }
            else
            {
                frenchStr = "False";
            }

           try
           {
               
               if (this.comboBoxNewRRUHeatingType.Text.Contains("Indirect"))
               {
                   heatInputTypeStr = "Indirect";
               }
               else if (this.comboBoxNewRRUHeatingType.Text.Contains("Direct"))
               {
                   heatInputTypeStr = "Direct";
               }
               else if (this.comboBoxNewRRUHeatingType.Text.Contains("Electric"))
               {
                   heatInputTypeStr = "Electric";
               }
               else
               {
                   heatInputTypeStr = "";
               }

               heatFuelTypeStr = heatInputTypeStr + "-" + this.comboBoxNewRRUIFHeatingFuelType.Text;
               
                etlRRUTA.UpdateByJobNum(
                     this.comboBoxNewRRUModelNo.Text,
                     this.textBoxNewRRUSerialNo.Text,
                     this.textBoxNewRRUPartNum.Text,
                     this.textBoxNewRRUMinCKTAmp.Text,
                     this.textBoxNewRRUMFSMCB.Text,
                     this.textBoxNewRRUMOP.Text,
                     heatFuelTypeStr,
                     //heatInputTypeStr,
                     this.textBoxNewRRUHeatingInput.Text,
                     this.textBoxNewRRUElectricalRating.Text,
                     this.textBoxNewRRUOperatingVolts.Text,
                     this.textBoxNewRRUTestPressureHigh.Text,
                     this.textBoxNewRRUTestPressureLow.Text,
                     this.textBoxNewRRUComp1Qty.Text,
                     this.textBoxNewRRUComp2Qty.Text,
                     this.textboxNewRRUComp1PH.Text,
                     this.textBoxNewRRUComp2PH.Text,
                     this.textBoxNewRRUComp1RLAVolts.Text,
                     this.textBoxNewRRUComp2RLAVolts.Text,
                     this.textBoxNewRRUComp1LRA.Text,
                     this.textBoxNewRRUComp2LRA.Text,
                     this.textBoxNewRRUComp1Charge.Text,
                     this.textBoxNewRRUComp2Charge.Text,
                     this.textBoxNewRRUFanCondQty.Text,
                     this.textBoxNewRRUFanStdQty.Text,
                     this.textBoxNewRRUFanOvrQty.Text,
                     this.textBoxNewRRUFanCondPH.Text,
                     this.textBoxNewRRUFanStdPH.Text,
                     this.textBoxNewRRUFanOvrPH.Text,
                     this.textBoxNewRRUFanCondFLA.Text,
                     this.textBoxNewRRUFanStdFLA.Text,
                     this.textBoxNewRRUFanOvrFLA.Text,
                     this.textBoxNewRRUFanCondHP.Text,
                     this.textBoxNewRRUFanStdHP.Text,
                     this.textBoxNewRRUFanOvrHP.Text,
                     this.textBoxNewRRUDFEquiptedAirflow.Text,
                     this.textBoxNewRRUDFStaticPressure.Text,
                     this.textBoxNewRRUDFMaxHtgInputBTUH.Text,
                     this.textboxNewRRUDFMinHtgInput.Text,
                     this.textBoxNewRRUDFTempRise.Text,
                     this.textBoxNewRRUDFMaxGasPressure.Text,
                     this.textBoxNewRRUDFMinGasPressure.Text,
                     this.textBoxNewRRUDFMaxPressureDrop.Text,
                     this.textBoxNewRRUDFMinPressureDrop.Text,
                     this.textBoxNewRRUDFManifoldPressure.Text,
                     this.textBoxNewRRUDFCutOutTemp.Text,
                     this.textBoxNewRRUIFHeatingInputBTUH.Text,
                     this.textBoxNewRRUIFHeatingOutput.Text,
                     this.textBoxNewRRUIFMinInputBTU.Text,
                     this.textBoxNewRRUIFMaxExt.Text,
                     this.textBoxNewRRUIFTempRise.Text,
                     this.textBoxNewRRUIFMaxOutAirTemp.Text,
                     this.textBoxNewRRUIFMaxGasPressure.Text,
                     this.textBoxNewRRUIFMinGasPressure.Text,
                     this.textBoxNewRRUIFManifoldPressure.Text,
                     this.textBoxNewRRUIFMaxHtgInputBTUH.Text,
                     verifiedByStr, 
                     frenchStr, 
                     this.textBoxNewRRUComp3Qty.Text,
                     this.textBoxNewRRUComp3PH.Text,
                     this.textBoxNewRRUComp3RLA.Text,
                     this.textBoxNewRRUComp3LRA.Text,
                     this.textBoxNewRRUComp3Charge.Text,                     
                     jobNumStr);

                MessageBox.Show("Job # " + jobNumStr + " updated successfully.");

                this.buttonNewRRUAccept.Text = "Update";
                GlobalModeType.ModeType = "Update";
                this.buttonNewRRUPrint.Enabled = true;
                this.labelNewRRUMsg.Text = jobNumStr +
                    " has been successfully updated in the EtlRRU table. " +
                    "Use the Update button for any additional modifications";
            }
            catch (Exception ex )
            {
                MessageBox.Show("ERROR - Updating data into EtlRRU --> " + ex);
            }            

        }

        private void addNewLineOrderDtlRRU()
        {
            string jobNumStr;
            int orderNumInt;
            int orderLineInt;
            int releaseNumInt;
            int slashLocInt;

            string verifiedByStr = "";
            string customerStr = "";
            string endConsumerNameStr = "";
            string frenchStr;
            string heatFuelTypeStr = "";

            jobNumStr = this.textBoxNewRRUJobNum.Text;
            orderNumInt = Int32.Parse(this.textBoxNewRRUOrderNum.Text);
            orderLineInt = Int32.Parse(this.textBoxNewRRULineItem.Text);
            releaseNumInt = Int32.Parse(this.textBoxNewRRUReleaseNum.Text);
            customerStr = this.textBoxNewRRUCustomer.Text;
            endConsumerNameStr = this.textBoxNewRRUEndConsumer.Text;

            if (this.comboBoxNewRRUModelNo.Text == "")
            {
                MessageBox.Show("ERROR - Model No is a required field.");
                this.textBoxNewRRUSerialNo.Focus();
                this.textBoxNewRRUSerialNo.Select();
                return;
            }

            if (this.textBoxNewRRUSerialNo.Text == "")
            {
                MessageBox.Show("ERROR - Serial No is a required field.");
                this.textBoxNewRRUSerialNo.Focus();
                this.textBoxNewRRUSerialNo.Select();
                return;
            }

            if (this.comboBoxNewRRUHeatingType.Text == "")
            {
                MessageBox.Show("ERROR - Heating Type is a required field.");
                this.textBoxNewRRUSerialNo.Focus();
                this.textBoxNewRRUSerialNo.Select();
                return;
            }

            VS_DataSet dsEtl = new VS_DataSet();


            // Write record to the VSOrderHed table.                
            try
            {
                // Create SqlDataAdapters for reading the VSOrderHed 
                // table using strongly typed datasets.
                VS_DataSetTableAdapters.EtlJobHeadTableAdapter etlJobHeadTA =
                    new VS_DataSetTableAdapters.EtlJobHeadTableAdapter();

                etlJobHeadTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                etlJobHeadTA.Fill(dsEtl.EtlJobHead);

                DataRow drJobHead = dsEtl.EtlJobHead.NewRow();

                drJobHead["JobNum"] = jobNumStr;
                drJobHead["OrderNum"] = orderNumInt.ToString();
                drJobHead["OrderLine"] = orderLineInt.ToString();
                drJobHead["ReleaseNum"] = releaseNumInt.ToString();
                drJobHead["UnitType"] = "RRU";
                drJobHead["Customer"] = customerStr;
                drJobHead["EndConsumerName"] = endConsumerNameStr;

                dsEtl.EtlJobHead.Rows.Add(drJobHead);

                etlJobHeadTA.Update(dsEtl.EtlJobHead);

                //vsOrderHedTA.Insert(orderNumInt.ToString(), customerStr, "RRU",
                //    orderLineInt.ToString(), releaseNumInt.ToString(), endConsumerNameStr);
            }
            catch
            {
                MessageBox.Show("Error occurred while inserting a row to the EtlJobHead table.");
                return;
            }


            verifiedByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            if (verifiedByStr != "")
            {
                slashLocInt = verifiedByStr.IndexOf('\\');
                if (slashLocInt != -1)
                {
                    ++slashLocInt;
                    verifiedByStr = verifiedByStr.Substring(slashLocInt, (verifiedByStr.Length - slashLocInt));
                }
            }

            if (this.checkBoxNewRRUFrench.Checked == true)
            {
                frenchStr = "True";

            }
            else
            {
                frenchStr = "False";
            }


            VS_DataSetTableAdapters.EtlRRUTableAdapter etlRRUTA =
                new VS_DataSetTableAdapters.EtlRRUTableAdapter();

            etlRRUTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

            //VS_DataSet dsEtlRRU = new VS_DataSet();

            heatFuelTypeStr = this.textBoxNewRRUHeatingType.Text + "-" + this.comboBoxNewRRUIFHeatingFuelType.Text;

            try
            {
                etlRRUTA.Fill(dsEtl.EtlRRU);

                DataRow drEtlRRU = dsEtl.EtlRRU.NewRow();

                drEtlRRU["JobNum"] = jobNumStr;
                drEtlRRU["OrderNum"] = orderNumInt.ToString();
                drEtlRRU["OrderLine"] = orderLineInt.ToString();
                drEtlRRU["ReleaseNum"] = releaseNumInt.ToString();
                drEtlRRU["ModelNo"] = this.comboBoxNewRRUModelNo.Text;
                drEtlRRU["SerialNo"] = this.textBoxNewRRUSerialNo.Text;
                drEtlRRU["PartNum"] = this.textBoxNewRRUPartNum.Text;
                drEtlRRU["MinCKTAmp"] = this.textBoxNewRRUMinCKTAmp.Text;
                drEtlRRU["MFSMCB"] = this.textBoxNewRRUMFSMCB.Text;
                drEtlRRU["MOP"] = this.textBoxNewRRUMOP.Text;
                drEtlRRU["ElecRating"] = this.textBoxNewRRUElectricalRating.Text;
                drEtlRRU["OperVoltage"] = this.textBoxNewRRUOperatingVolts.Text;
                drEtlRRU["HeatingType"] = heatFuelTypeStr;
                drEtlRRU["HeatingInput"] = this.textBoxNewRRUHeatingInput.Text;                    
                drEtlRRU["TestPressureHigh"] = this.textBoxNewRRUTestPressureHigh.Text;
                drEtlRRU["TestPressureLow"] = this.textBoxNewRRUTestPressureLow.Text;
                drEtlRRU["Compr1Qty"] = this.textBoxNewRRUComp1Qty.Text;
                drEtlRRU["Compr2Qty"] = this.textBoxNewRRUComp2Qty.Text;
                drEtlRRU["Compr1PH"] = this.textboxNewRRUComp1PH.Text;                
                drEtlRRU["Compr2PH"] = this.textBoxNewRRUComp2PH.Text;
                drEtlRRU["Compr1RLA_Volts"] = this.textBoxNewRRUComp1RLAVolts.Text;
                drEtlRRU["Compr2RLA_Volts"] = this.textBoxNewRRUComp2RLAVolts.Text;
                drEtlRRU["Compr1LRA"] = this.textBoxNewRRUComp1LRA.Text;                                                                               
                drEtlRRU["Compr2LRA"] = this.textBoxNewRRUComp2LRA.Text;
                drEtlRRU["Compr1Charge"] = this.textBoxNewRRUComp1Charge.Text;
                drEtlRRU["Compr2Charge"] = this.textBoxNewRRUComp2Charge.Text;
                drEtlRRU["FanCondQty"] = this.textBoxNewRRUFanCondQty.Text;
                drEtlRRU["FanStdQty"] = this.textBoxNewRRUFanStdQty.Text;
                drEtlRRU["FanOvrQty"] = this.textBoxNewRRUFanOvrQty.Text;
                drEtlRRU["FanCondPH"] = this.textBoxNewRRUFanCondPH.Text;
                drEtlRRU["FanStdPH"] = this.textBoxNewRRUFanStdPH.Text;
                drEtlRRU["FanOvrPH"] = this.textBoxNewRRUFanOvrPH.Text;
                drEtlRRU["FanCondFLA"] = this.textBoxNewRRUFanCondFLA.Text;
                drEtlRRU["FanStdFLA"] = this.textBoxNewRRUFanStdFLA.Text;
                drEtlRRU["FanOvrFLA"] = this.textBoxNewRRUFanOvrFLA.Text;
                drEtlRRU["FanCondHP"] = this.textBoxNewRRUFanCondHP.Text;
                drEtlRRU["FanStdHP"] = this.textBoxNewRRUFanStdHP.Text;
                drEtlRRU["FanOvrHP"] = this.textBoxNewRRUFanOvrHP.Text;
                drEtlRRU["DF_EquiptedAirFlow"] = this.textBoxNewRRUDFEquiptedAirflow.Text;
                drEtlRRU["DF_StaticPressure"] = this.textBoxNewRRUDFStaticPressure.Text;
                drEtlRRU["DF_MaxHtgInputBTUH"] = this.textBoxNewRRUDFMaxHtgInputBTUH.Text;
                drEtlRRU["DF_MinHtgInput"] = this.textboxNewRRUDFMinHtgInput.Text;
                drEtlRRU["DF_TempRise"] = this.textBoxNewRRUDFTempRise.Text;
                drEtlRRU["DF_MaxGasPressure"] = this.textBoxNewRRUDFMaxGasPressure.Text;
                drEtlRRU["DF_MinGasPressure"] = this.textBoxNewRRUDFMinGasPressure.Text;
                drEtlRRU["DF_MaxPressureDrop"] = this.textBoxNewRRUDFMaxPressureDrop.Text;
                drEtlRRU["DF_MinPressureDrop"] = this.textBoxNewRRUDFMinPressureDrop.Text;
                drEtlRRU["DF_ManifoldPressure"] = this.textBoxNewRRUDFManifoldPressure.Text;
                drEtlRRU["DF_CutOutTemp"] = this.textBoxNewRRUDFCutOutTemp.Text;
                drEtlRRU["IN_HtgInputBTUH"] = this.textBoxNewRRUIFHeatingInputBTUH.Text;
                drEtlRRU["IN_HtgOutput"] = this.textBoxNewRRUIFHeatingOutput.Text;
                drEtlRRU["IN_MinInputBTU"] = this.textBoxNewRRUIFMinInputBTU.Text;
                drEtlRRU["IN_MaxExt"] = this.textBoxNewRRUIFMaxExt.Text;
                drEtlRRU["IN_TempRise"] = this.textBoxNewRRUIFTempRise.Text;
                drEtlRRU["IN_MaxOutletAirTemp"] = this.textBoxNewRRUIFMaxOutAirTemp.Text;
                drEtlRRU["IN_MaxGasPressure"] = this.textBoxNewRRUIFMaxGasPressure.Text;
                drEtlRRU["IN_MinGasPressure"] = this.textBoxNewRRUIFMinGasPressure.Text;
                drEtlRRU["IN_ManifoldPressure"] = this.textBoxNewRRUIFManifoldPressure.Text;
                drEtlRRU["IN_MaxHtgInputBTUH"] = this.textBoxNewRRUIFMaxHtgInputBTUH.Text;                    
                drEtlRRU["VerifiedBy"] = verifiedByStr; 
                drEtlRRU["GTG"] = true;
                drEtlRRU["French"] = frenchStr;

                dsEtl.EtlRRU.Rows.Add(drEtlRRU);

                etlRRUTA.Update(dsEtl.EtlRRU);

                MessageBox.Show("Job # " + jobNumStr + " added successfully.");

                this.labelMewRRUCreatedBy.Visible = true;
                this.textBoxNewRRUVerifiedBy.Text = verifiedByStr;
                this.textBoxNewRRUVerifiedBy.Visible = true;

                this.buttonNewRRUAccept.Text = "Update";
                GlobalModeType.ModeType = "Update";
                this.buttonNewRRUPrint.Enabled = true;
                this.labelNewRRUMsg.Text = this.textBoxNewRRUSerialNo.Text +
                    " has been successfully written to the EtlRRU table. " +
                    "Use the Update button to modify this unit.";
            }
            catch
            {
                MessageBox.Show("ERROR inserting data into EtlRRU");
            }            
        }

        private void buttonNewRRUPrint_Click(object sender, EventArgs e)
        {
            string frenchStr = "False";
            string heatFuelTypeStr = "";

            frmPrintRRU frmPrint = new frmPrintRRU();            

            if (this.checkBoxNewRRUFrench.Checked == true)
            {
                frenchStr = "True";
            }           

            VS_DataSet ds = new VS_DataSet();

            heatFuelTypeStr = this.textBoxNewRRUHeatingType.Text + "-" + this.comboBoxNewRRUIFHeatingFuelType.Text;

            ds.RRUReportDataTable.Rows.Add(
                    this.textBoxNewRRUOrderNum.Text,
                    this.textBoxNewRRULineItem.Text,
                    this.textBoxNewRRUReleaseNum.Text,
                    this.comboBoxNewRRUModelNo.Text,
                    this.textBoxNewRRUSerialNo.Text,
                    this.textBoxNewRRUPartNum.Text,
                    this.textBoxNewRRUMinCKTAmp.Text,
                    this.textBoxNewRRUMFSMCB.Text,
                    this.textBoxNewRRUMOP.Text,
                    this.textBoxNewRRUElectricalRating.Text.Trim(),
                    this.textBoxNewRRUOperatingVolts.Text.Trim(),
                    heatFuelTypeStr,
                    this.textBoxNewRRUHeatingInput.Text.Trim(),
                    this.textBoxNewRRUTestPressureHigh.Text,
                    this.textBoxNewRRUTestPressureLow.Text,
                    this.textBoxNewRRUComp1Qty.Text,
                    this.textBoxNewRRUComp2Qty.Text,
                    this.textboxNewRRUComp1PH.Text,
                    this.textBoxNewRRUComp2PH.Text,
                    this.textBoxNewRRUComp1RLAVolts.Text,
                    this.textBoxNewRRUComp2RLAVolts.Text,
                    this.textBoxNewRRUComp1LRA.Text,
                    this.textBoxNewRRUComp2LRA.Text,
                    this.textBoxNewRRUComp1Charge.Text,
                    this.textBoxNewRRUComp2Charge.Text,
                    this.textBoxNewRRUFanCondQty.Text,
                    this.textBoxNewRRUFanStdQty.Text,
                    this.textBoxNewRRUFanOvrQty.Text,
                    this.textBoxNewRRUFanCondPH.Text,
                    this.textBoxNewRRUFanStdPH.Text,
                    this.textBoxNewRRUFanOvrPH.Text,
                    this.textBoxNewRRUFanCondFLA.Text,
                    this.textBoxNewRRUFanStdFLA.Text,
                    this.textBoxNewRRUFanOvrFLA.Text,
                    this.textBoxNewRRUFanCondHP.Text,
                    this.textBoxNewRRUFanStdHP.Text,
                    this.textBoxNewRRUFanOvrHP.Text,
                    this.textBoxNewRRUDFEquiptedAirflow.Text,
                    this.textBoxNewRRUDFStaticPressure.Text,
                    this.textBoxNewRRUDFMaxHtgInputBTUH.Text,
                    this.textboxNewRRUDFMinHtgInput.Text,
                    this.textBoxNewRRUDFTempRise.Text,
                    this.textBoxNewRRUDFMaxGasPressure.Text,
                    this.textBoxNewRRUDFMinGasPressure.Text,
                    this.textBoxNewRRUDFMaxPressureDrop.Text,
                    this.textBoxNewRRUDFMinPressureDrop.Text,
                    this.textBoxNewRRUDFManifoldPressure.Text,
                    this.textBoxNewRRUDFCutOutTemp.Text,
                    this.textBoxNewRRUIFHeatingInputBTUH.Text,
                    this.textBoxNewRRUIFHeatingOutput.Text,
                    this.textBoxNewRRUIFMinInputBTU.Text,
                    this.textBoxNewRRUIFMaxExt.Text,
                    this.textBoxNewRRUIFTempRise.Text,
                    this.textBoxNewRRUIFMaxOutAirTemp.Text,
                    this.textBoxNewRRUIFMaxGasPressure.Text,
                    this.textBoxNewRRUIFMinGasPressure.Text,
                    this.textBoxNewRRUIFManifoldPressure.Text,
                    this.textBoxNewRRUIFMaxHtgInputBTUH.Text, 
                    frenchStr,
                    this.textBoxNewRRUComp3Qty.Text,
                    this.textBoxNewRRUComp3PH.Text,
                    this.textBoxNewRRUComp3RLA.Text,
                    this.textBoxNewRRUComp3LRA.Text,
                    this.textBoxNewRRUComp3Charge.Text);

            ReportDocument cryRpt = new ReportDocument();

            EpicorServer = ConfigurationManager.AppSettings["EpicorAppServer"];

            string reportString = @"\\" + EpicorServer + "\\Apps\\OAU\\EtlLabel\\reports\\RRU_Label_v006.rpt";

            //string reportString = @"\\epicor905app\Apps\OAU\EtlLabel\reports\RRU_Label_v006.rpt"; 
            //string reportString = @"\\KCCWVPEPIC9APP\Apps\OAU\EtlLabel\reports\RRU_Label_v006.rpt"; 
            //string reportString = @"\\epicor905app\Apps\OAU\EtlLabel\reports\RRU_Label_v003_ThreeComp.rpt"; ;

#if DEBUG
                reportString = @"\\KCCWVTEPIC9APP2\Apps\OAU\EtlLabel\reports\RRU_Label_v006.rpt"; 
#endif

            cryRpt.Load(reportString);

            cryRpt.SetDataSource(ds.Tables["RRUReportDataTable"]);

            frmPrint.crystalReportViewer1.ReportSource = cryRpt;

            frmPrint.ShowDialog();
            frmPrint.crystalReportViewer1.Refresh();  
        }

        private void comboBoxNewRRUModelNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            string partNumStr = "";
            string htgTypeStr = "";                      

            if (GlobalModeType.ModeType == "New")
            {
                // Create SqlDataAdapters for reading the RRUTemplates table.
                VS_DataSetTableAdapters.EtlRRUTemplatesTableAdapter rruTA =
                        new VS_DataSetTableAdapters.EtlRRUTemplatesTableAdapter();

                rruTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;
                try
                {
                    partNumStr = this.comboBoxNewRRUModelNo.Text;
                    VS_DataSet.EtlRRUTemplatesDataTable rru2DT = rruTA.GetDataByModelNo(partNumStr);
                    if (rru2DT.Count > 0)
                    {
                        DataRow dr2 = rru2DT.Rows[0];
                        htgTypeStr = dr2["HeatingType"].ToString();

                        if (htgTypeStr.Equals("Direct", StringComparison.OrdinalIgnoreCase))
                        {
                            this.groupBoxNewRRUDirectFire.Location = new Point(6, 275);
                            this.groupBoxNewRRUDirectFire.Visible = true;
                            this.groupBoxNewRRUIndirectFire.Visible = false;
                            this.comboBoxNewRRUHeatingType.SelectedIndex = 0;
                            this.textBoxNewRRUDFEquiptedAirflow.Text = dr2["DF_EquiptedAirFlow"].ToString();
                            this.textBoxNewRRUDFStaticPressure.Text = dr2["DF_StaticPressure"].ToString();
                            this.textBoxNewRRUDFMaxHtgInputBTUH.Text = dr2["DF_MaxHtgInputBTUH"].ToString();
                            this.textboxNewRRUDFMinHtgInput.Text = dr2["DF_MinHtgInput"].ToString();
                            this.textBoxNewRRUDFTempRise.Text = dr2["DF_TempRise"].ToString();
                            this.textBoxNewRRUDFMaxGasPressure.Text = dr2["DF_MaxGasPressure"].ToString();
                            this.textBoxNewRRUDFMinGasPressure.Text = dr2["DF_MinGasPressure"].ToString();
                            this.textBoxNewRRUDFMaxPressureDrop.Text = dr2["DF_MaxPressureDrop"].ToString();
                            this.textBoxNewRRUDFMinPressureDrop.Text = dr2["DF_MinPressureDrop"].ToString();
                            this.textBoxNewRRUDFManifoldPressure.Text = dr2["DF_ManifoldPressure"].ToString();
                            this.textBoxNewRRUDFCutOutTemp.Text = dr2["DF_CutOutTemp"].ToString();

                        }
                        else if (htgTypeStr.Equals("Indirect", StringComparison.OrdinalIgnoreCase))
                        {
                            this.groupBoxNewRRUIndirectFire.Location = new Point(6, 275);
                            this.groupBoxNewRRUIndirectFire.Visible = true;
                            this.groupBoxNewRRUDirectFire.Visible = false;
                            this.comboBoxNewRRUHeatingType.SelectedIndex = 1;
                            this.textBoxNewRRUIFHeatingInputBTUH.Text = dr2["IN_HtgInputBTUH"].ToString();
                            this.textBoxNewRRUIFHeatingOutput.Text = dr2["IN_HtgOutputBTUH"].ToString();
                            this.textBoxNewRRUIFMinInputBTU.Text = dr2["IN_MinInputBTU"].ToString();
                            this.textBoxNewRRUIFMaxExt.Text = dr2["IN_MaxExt"].ToString();
                            this.textBoxNewRRUIFTempRise.Text = dr2["IN_TempRise"].ToString();
                            this.textBoxNewRRUIFMaxOutAirTemp.Text = dr2["IN_MaxOutletAirTemp"].ToString();
                            this.textBoxNewRRUIFMaxGasPressure.Text = dr2["IN_MaxGasPressure"].ToString();
                            this.textBoxNewRRUIFMinGasPressure.Text = dr2["IN_MinGaspressure"].ToString();
                            this.textBoxNewRRUIFManifoldPressure.Text = dr2["IN_ManifoldPressure"].ToString();
                            this.textBoxNewRRUIFMaxHtgInputBTUH.Text = dr2["IN_MaxHtgInputBTUH"].ToString();
                        }
                        else if (htgTypeStr.Equals("Electric", StringComparison.OrdinalIgnoreCase))
                        {
                            this.groupBoxNewRRUIndirectFire.Visible = false;
                            this.groupBoxNewRRUDirectFire.Visible = false;
                            this.comboBoxNewRRUHeatingType.SelectedIndex = 2;
                        }
                        else if (htgTypeStr.Equals("NoHeat", StringComparison.OrdinalIgnoreCase))
                        {
                            this.groupBoxNewRRUIndirectFire.Visible = false;
                            this.groupBoxNewRRUDirectFire.Visible = false;
                            this.comboBoxNewRRUHeatingType.SelectedIndex = 3;
                        }

                        this.textBoxNewRRUFanCondQty.Text = dr2["FanCondQty"].ToString();
                        this.textBoxNewRRUFanCondPH.Text = dr2["FanCondPH"].ToString();
                        this.textBoxNewRRUFanCondFLA.Text = dr2["FanCondFLA"].ToString();
                        this.textBoxNewRRUFanCondHP.Text = dr2["FanCondHP"].ToString();

                        this.textBoxNewRRUFanStdQty.Text = dr2["FanStdQty"].ToString();
                        this.textBoxNewRRUFanStdPH.Text = dr2["FanStdPH"].ToString();
                        this.textBoxNewRRUFanStdFLA.Text = dr2["FanStdFLA"].ToString();
                        this.textBoxNewRRUFanStdHP.Text = dr2["FanStdHP"].ToString();

                        this.textBoxNewRRUFanOvrQty.Text = dr2["FanOvrQty"].ToString();
                        this.textBoxNewRRUFanOvrPH.Text = dr2["FanOvrPH"].ToString();
                        this.textBoxNewRRUFanOvrFLA.Text = dr2["FanOvrFLA"].ToString();
                        this.textBoxNewRRUFanOvrHP.Text = dr2["FanOvrHP"].ToString();

                        this.textBoxNewRRUComp1Qty.Text = dr2["Compr1Qty"].ToString();
                        this.textboxNewRRUComp1PH.Text = dr2["Compr1PH"].ToString();
                        this.textBoxNewRRUComp1RLAVolts.Text = dr2["Compr1RLA_Volts"].ToString();
                        this.textBoxNewRRUComp1LRA.Text = dr2["Compr1LRA"].ToString();
                        this.textBoxNewRRUComp1Charge.Text = dr2["Compr1Charge"].ToString();

                        this.textBoxNewRRUComp2Qty.Text = dr2["Compr2Qty"].ToString();
                        this.textBoxNewRRUComp2PH.Text = dr2["Compr2PH"].ToString();
                        this.textBoxNewRRUComp2RLAVolts.Text = dr2["Compr2RLA_Volts"].ToString();
                        this.textBoxNewRRUComp2LRA.Text = dr2["Compr2LRA"].ToString();
                        this.textBoxNewRRUComp2Charge.Text = dr2["Compr2Charge"].ToString();

                        this.textBoxNewRRUComp3Qty.Text = dr2["Comp3Qty"].ToString() == "NA" ? "" : dr2["Comp3Qty"].ToString();
                        this.textBoxNewRRUComp3PH.Text = dr2["Comp3PH"].ToString() == "NA" ? "" : dr2["Comp3PH"].ToString();
                        this.textBoxNewRRUComp3RLA.Text = dr2["Comp3RLA_Volts"].ToString() == "NA" ? "" : dr2["Comp3RLA_Volts"].ToString();
                        this.textBoxNewRRUComp3LRA.Text = dr2["Comp3LRA"].ToString() == "NA" ? "" : dr2["Comp3LRA"].ToString();
                        this.textBoxNewRRUComp3Charge.Text = dr2["Comp3Charge"].ToString() == "NA" ? "" : dr2["Comp3Charge"].ToString();       

                        this.textBoxNewRRUMinCKTAmp.Text = dr2["MinCKTAmp"].ToString();
                        this.textBoxNewRRUMFSMCB.Text = dr2["MFSMCB"].ToString();
                        this.textBoxNewRRUMOP.Text = dr2["MOP"].ToString();
                        this.textBoxNewRRUElectricalRating.Text = dr2["ElecRating"].ToString();
                        this.textBoxNewRRUOperatingVolts.Text = dr2["OperVoltage"].ToString();
                        this.textBoxNewRRUHeatingType.Text = dr2["HeatingType"].ToString();
                        this.textBoxNewRRUHeatingInput.Text = dr2["HeatingInput"].ToString();
                        this.textBoxNewRRUTestPressureHigh.Text = dr2["TestPressureHigh"].ToString();
                        this.textBoxNewRRUTestPressureLow.Text = dr2["TestPressureLow"].ToString();
                        GlobalRRUModelNo.ModelNoSelInt = this.comboBoxNewRRUModelNo.SelectedIndex;
                        //GlobalModeType.ModeType = "Update";
                        this.checkBoxModelTypeOverride.Checked = false;
                        this.comboBoxNewRRUModelNo.Enabled = false;
                    }
                    else
                    {
                        MessageBox.Show("WARNING RRUTemplate for ModelNo - " + partNumStr + " Not found!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR searching RRUTemplates for ModelNo - " + partNumStr + " Exception = " + ex);
                }
            }           
        }        
        
        private void checkBoxModelTypeOverride_CheckedChanged(object sender, EventArgs e)
        {
            if (this.comboBoxNewRRUModelNo.Enabled == true)
            {
                this.comboBoxNewRRUModelNo.Enabled = false;
                GlobalModeType.ModeType = "Update";
            }
            else
            {
                this.buttonNewRRUPrint.Enabled = false;
                this.comboBoxNewRRUModelNo.Enabled = true;
                GlobalModeType.ModeType = "New";                
            }
        }

    }
}