﻿namespace KCC.OA.Etl
{
    partial class frmTradewinds
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpKCC = new System.Windows.Forms.TabPage();
            this.btnDisplay = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.txtVoltMax = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtkArmsSym = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtTypeOfUse = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtMaxOverProtection = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtMinCircuitAmp = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtFullLoadAmp = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtControlPower = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPhase = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtHertz = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtVoltage = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDateOfAsm = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSerialNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtModelNo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tpSemco = new System.Windows.Forms.TabPage();
            this.label20 = new System.Windows.Forms.Label();
            this.txtSemcoTEWheelMotorAmps = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtSemcoTEWheelMotorHP = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtSemcoExhFanMotorAmps = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtSemcoExhFanMotorHP = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.btnSemcoDisplay = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.txtSemcoSupFanMotorAmps = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtSemcoSupFanMotorHP = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtSemcoDateOfMfg = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtSemcoSerialNo = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtSemcoModelNo = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.tpTrane = new System.Windows.Forms.TabPage();
            this.label75 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.txtTraneDistBack = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.txtTraneDistTop = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.txtTraneDistDuck = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.txtTraneDistFront = new System.Windows.Forms.TextBox();
            this.txtTraneDistRSide = new System.Windows.Forms.TextBox();
            this.label73 = new System.Windows.Forms.Label();
            this.txtTraneDistLSide = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.txtTraneMaxEntStaticPres = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.txtTraneMaxDischargeTemp = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.txtTraneGasHeatMinHtgInput = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.txtTraneGasHeatMaxHtgInput = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.txtTraneCirc2Charge = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.txtTraneCirc1Charge = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.txtTraneEvapFanMtrHP = new System.Windows.Forms.TextBox();
            this.txtTraneEvapFanMtrFLAVolts = new System.Windows.Forms.TextBox();
            this.txtTraneEvapFanMtrHertz = new System.Windows.Forms.TextBox();
            this.txtTraneEvapFanMtrPhase = new System.Windows.Forms.TextBox();
            this.txtTraneEvapFanMtrQty = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.txtTraneCond2FanMtrHP = new System.Windows.Forms.TextBox();
            this.txtTraneCond2FanMtrFLAVolts = new System.Windows.Forms.TextBox();
            this.txtTraneCond2FanMtrHertz = new System.Windows.Forms.TextBox();
            this.txtTraneCond2FanMtrPhase = new System.Windows.Forms.TextBox();
            this.txtTraneCond2FanMtrQty = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.txtTraneCond1MtrHP = new System.Windows.Forms.TextBox();
            this.txtTraneCond1FanMtrFLAVolts = new System.Windows.Forms.TextBox();
            this.txtTraneCond1FanMtrHertz = new System.Windows.Forms.TextBox();
            this.txtTraneCond1FanMtrPhase = new System.Windows.Forms.TextBox();
            this.txtTraneCond1FanMtrQty = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtTraneComp2LRA = new System.Windows.Forms.TextBox();
            this.txtTraneComp2RLAVolts = new System.Windows.Forms.TextBox();
            this.txtTraneComp2Hertz = new System.Windows.Forms.TextBox();
            this.txtTraneComp2Phase = new System.Windows.Forms.TextBox();
            this.txtTraneComp2Qty = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.txtTraneComp1LRA = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.txtTraneComp1RLAVolts = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.btnTraneDisplay = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.txtTraneTestPresLow = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtTraneTestPresHigh = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtTraneRefrigType = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtTraneComp1Hertz = new System.Windows.Forms.TextBox();
            this.txtTraneComp1Phase = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.txtTraneComp1Qty = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.txtTraneDateOfMfg = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.txtTraneSerialNo = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtTraneModelNo = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.bthExit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tpKCC.SuspendLayout();
            this.tpSemco.SuspendLayout();
            this.tpTrane.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpKCC);
            this.tabControl1.Controls.Add(this.tpSemco);
            this.tabControl1.Controls.Add(this.tpTrane);
            this.tabControl1.Location = new System.Drawing.Point(13, 39);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(501, 675);
            this.tabControl1.TabIndex = 0;
            // 
            // tpKCC
            // 
            this.tpKCC.Controls.Add(this.btnDisplay);
            this.tpKCC.Controls.Add(this.label14);
            this.tpKCC.Controls.Add(this.txtVoltMax);
            this.tpKCC.Controls.Add(this.label15);
            this.tpKCC.Controls.Add(this.txtkArmsSym);
            this.tpKCC.Controls.Add(this.label16);
            this.tpKCC.Controls.Add(this.txtTypeOfUse);
            this.tpKCC.Controls.Add(this.label13);
            this.tpKCC.Controls.Add(this.txtMaxOverProtection);
            this.tpKCC.Controls.Add(this.label12);
            this.tpKCC.Controls.Add(this.txtMinCircuitAmp);
            this.tpKCC.Controls.Add(this.label11);
            this.tpKCC.Controls.Add(this.txtFullLoadAmp);
            this.tpKCC.Controls.Add(this.label10);
            this.tpKCC.Controls.Add(this.txtControlPower);
            this.tpKCC.Controls.Add(this.label9);
            this.tpKCC.Controls.Add(this.label8);
            this.tpKCC.Controls.Add(this.txtPhase);
            this.tpKCC.Controls.Add(this.label7);
            this.tpKCC.Controls.Add(this.txtHertz);
            this.tpKCC.Controls.Add(this.label6);
            this.tpKCC.Controls.Add(this.txtVoltage);
            this.tpKCC.Controls.Add(this.label5);
            this.tpKCC.Controls.Add(this.txtDateOfAsm);
            this.tpKCC.Controls.Add(this.label4);
            this.tpKCC.Controls.Add(this.txtSerialNo);
            this.tpKCC.Controls.Add(this.label3);
            this.tpKCC.Controls.Add(this.txtModelNo);
            this.tpKCC.Controls.Add(this.label2);
            this.tpKCC.ForeColor = System.Drawing.Color.Blue;
            this.tpKCC.Location = new System.Drawing.Point(4, 22);
            this.tpKCC.Name = "tpKCC";
            this.tpKCC.Padding = new System.Windows.Forms.Padding(3);
            this.tpKCC.Size = new System.Drawing.Size(493, 649);
            this.tpKCC.TabIndex = 0;
            this.tpKCC.Text = "KCC International";
            this.tpKCC.UseVisualStyleBackColor = true;
            // 
            // btnDisplay
            // 
            this.btnDisplay.ForeColor = System.Drawing.Color.Blue;
            this.btnDisplay.Location = new System.Drawing.Point(169, 509);
            this.btnDisplay.Name = "btnDisplay";
            this.btnDisplay.Size = new System.Drawing.Size(150, 50);
            this.btnDisplay.TabIndex = 14;
            this.btnDisplay.Text = "Display";
            this.btnDisplay.UseVisualStyleBackColor = true;
            this.btnDisplay.Click += new System.EventHandler(this.btnDisplay_Click);
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Blue;
            this.label14.Location = new System.Drawing.Point(384, 425);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 25);
            this.label14.TabIndex = 27;
            this.label14.Text = "V Maximum";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtVoltMax
            // 
            this.txtVoltMax.Location = new System.Drawing.Point(325, 425);
            this.txtVoltMax.Name = "txtVoltMax";
            this.txtVoltMax.Size = new System.Drawing.Size(53, 20);
            this.txtVoltMax.TabIndex = 13;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Blue;
            this.label15.Location = new System.Drawing.Point(213, 425);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(100, 25);
            this.label15.TabIndex = 25;
            this.label15.Text = "kArms Symmetrical";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtkArmsSym
            // 
            this.txtkArmsSym.Location = new System.Drawing.Point(154, 425);
            this.txtkArmsSym.Name = "txtkArmsSym";
            this.txtkArmsSym.Size = new System.Drawing.Size(53, 20);
            this.txtkArmsSym.TabIndex = 12;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Blue;
            this.label16.Location = new System.Drawing.Point(16, 421);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(131, 25);
            this.label16.TabIndex = 23;
            this.label16.Text = "Short Circuit Current:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTypeOfUse
            // 
            this.txtTypeOfUse.Location = new System.Drawing.Point(153, 385);
            this.txtTypeOfUse.Name = "txtTypeOfUse";
            this.txtTypeOfUse.Size = new System.Drawing.Size(295, 20);
            this.txtTypeOfUse.TabIndex = 11;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Blue;
            this.label13.Location = new System.Drawing.Point(15, 381);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(131, 25);
            this.label13.TabIndex = 21;
            this.label13.Text = "Type of Use:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMaxOverProtection
            // 
            this.txtMaxOverProtection.Location = new System.Drawing.Point(259, 345);
            this.txtMaxOverProtection.Name = "txtMaxOverProtection";
            this.txtMaxOverProtection.Size = new System.Drawing.Size(53, 20);
            this.txtMaxOverProtection.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Blue;
            this.label12.Location = new System.Drawing.Point(16, 342);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(237, 25);
            this.label12.TabIndex = 19;
            this.label12.Text = "Max Overcurrent Protection:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMinCircuitAmp
            // 
            this.txtMinCircuitAmp.Location = new System.Drawing.Point(259, 305);
            this.txtMinCircuitAmp.Name = "txtMinCircuitAmp";
            this.txtMinCircuitAmp.Size = new System.Drawing.Size(53, 20);
            this.txtMinCircuitAmp.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Blue;
            this.label11.Location = new System.Drawing.Point(16, 302);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(237, 25);
            this.label11.TabIndex = 17;
            this.label11.Text = "Minimum Circuit Amp:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtFullLoadAmp
            // 
            this.txtFullLoadAmp.Location = new System.Drawing.Point(259, 265);
            this.txtFullLoadAmp.Name = "txtFullLoadAmp";
            this.txtFullLoadAmp.Size = new System.Drawing.Size(53, 20);
            this.txtFullLoadAmp.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(16, 262);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(237, 25);
            this.label10.TabIndex = 15;
            this.label10.Text = "Full Load Amp:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtControlPower
            // 
            this.txtControlPower.Location = new System.Drawing.Point(259, 225);
            this.txtControlPower.Name = "txtControlPower";
            this.txtControlPower.Size = new System.Drawing.Size(53, 20);
            this.txtControlPower.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(16, 222);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(237, 25);
            this.label9.TabIndex = 13;
            this.label9.Text = "Control Power:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(426, 185);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 25);
            this.label8.TabIndex = 12;
            this.label8.Text = "Ph";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPhase
            // 
            this.txtPhase.Location = new System.Drawing.Point(367, 185);
            this.txtPhase.Name = "txtPhase";
            this.txtPhase.Size = new System.Drawing.Size(53, 20);
            this.txtPhase.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(318, 185);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 25);
            this.label7.TabIndex = 10;
            this.label7.Text = "Hz";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtHertz
            // 
            this.txtHertz.Location = new System.Drawing.Point(259, 185);
            this.txtHertz.Name = "txtHertz";
            this.txtHertz.Size = new System.Drawing.Size(53, 20);
            this.txtHertz.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(213, 185);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 25);
            this.label6.TabIndex = 8;
            this.label6.Text = "V";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtVoltage
            // 
            this.txtVoltage.Location = new System.Drawing.Point(154, 185);
            this.txtVoltage.Name = "txtVoltage";
            this.txtVoltage.Size = new System.Drawing.Size(53, 20);
            this.txtVoltage.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(16, 181);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(131, 25);
            this.label5.TabIndex = 6;
            this.label5.Text = "Electrical Rating:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDateOfAsm
            // 
            this.txtDateOfAsm.Location = new System.Drawing.Point(154, 145);
            this.txtDateOfAsm.Name = "txtDateOfAsm";
            this.txtDateOfAsm.Size = new System.Drawing.Size(295, 20);
            this.txtDateOfAsm.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(16, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 25);
            this.label4.TabIndex = 4;
            this.label4.Text = "Date of Assembly:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSerialNo
            // 
            this.txtSerialNo.Location = new System.Drawing.Point(154, 105);
            this.txtSerialNo.Name = "txtSerialNo";
            this.txtSerialNo.Size = new System.Drawing.Size(295, 20);
            this.txtSerialNo.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(16, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Serial Number:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtModelNo
            // 
            this.txtModelNo.Location = new System.Drawing.Point(154, 65);
            this.txtModelNo.Name = "txtModelNo";
            this.txtModelNo.Size = new System.Drawing.Size(295, 20);
            this.txtModelNo.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(16, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 25);
            this.label2.TabIndex = 0;
            this.label2.Text = "Model Number:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tpSemco
            // 
            this.tpSemco.Controls.Add(this.label20);
            this.tpSemco.Controls.Add(this.txtSemcoTEWheelMotorAmps);
            this.tpSemco.Controls.Add(this.label21);
            this.tpSemco.Controls.Add(this.txtSemcoTEWheelMotorHP);
            this.tpSemco.Controls.Add(this.label22);
            this.tpSemco.Controls.Add(this.label17);
            this.tpSemco.Controls.Add(this.txtSemcoExhFanMotorAmps);
            this.tpSemco.Controls.Add(this.label18);
            this.tpSemco.Controls.Add(this.txtSemcoExhFanMotorHP);
            this.tpSemco.Controls.Add(this.label19);
            this.tpSemco.Controls.Add(this.btnSemcoDisplay);
            this.tpSemco.Controls.Add(this.label26);
            this.tpSemco.Controls.Add(this.txtSemcoSupFanMotorAmps);
            this.tpSemco.Controls.Add(this.label27);
            this.tpSemco.Controls.Add(this.txtSemcoSupFanMotorHP);
            this.tpSemco.Controls.Add(this.label28);
            this.tpSemco.Controls.Add(this.txtSemcoDateOfMfg);
            this.tpSemco.Controls.Add(this.label29);
            this.tpSemco.Controls.Add(this.txtSemcoSerialNo);
            this.tpSemco.Controls.Add(this.label30);
            this.tpSemco.Controls.Add(this.txtSemcoModelNo);
            this.tpSemco.Controls.Add(this.label31);
            this.tpSemco.Location = new System.Drawing.Point(4, 22);
            this.tpSemco.Name = "tpSemco";
            this.tpSemco.Padding = new System.Windows.Forms.Padding(3);
            this.tpSemco.Size = new System.Drawing.Size(493, 649);
            this.tpSemco.TabIndex = 1;
            this.tpSemco.Text = "Semco";
            this.tpSemco.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Blue;
            this.label20.Location = new System.Drawing.Point(332, 265);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(22, 25);
            this.label20.TabIndex = 69;
            this.label20.Text = "A";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSemcoTEWheelMotorAmps
            // 
            this.txtSemcoTEWheelMotorAmps.Location = new System.Drawing.Point(273, 265);
            this.txtSemcoTEWheelMotorAmps.Name = "txtSemcoTEWheelMotorAmps";
            this.txtSemcoTEWheelMotorAmps.Size = new System.Drawing.Size(53, 20);
            this.txtSemcoTEWheelMotorAmps.TabIndex = 9;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Blue;
            this.label21.Location = new System.Drawing.Point(227, 265);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(25, 25);
            this.label21.TabIndex = 67;
            this.label21.Text = "HP";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSemcoTEWheelMotorHP
            // 
            this.txtSemcoTEWheelMotorHP.Location = new System.Drawing.Point(168, 265);
            this.txtSemcoTEWheelMotorHP.Name = "txtSemcoTEWheelMotorHP";
            this.txtSemcoTEWheelMotorHP.Size = new System.Drawing.Size(53, 20);
            this.txtSemcoTEWheelMotorHP.TabIndex = 8;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Blue;
            this.label22.Location = new System.Drawing.Point(30, 261);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(131, 25);
            this.label22.TabIndex = 65;
            this.label22.Text = "TE Wheel Motor:";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Blue;
            this.label17.Location = new System.Drawing.Point(332, 225);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(22, 25);
            this.label17.TabIndex = 64;
            this.label17.Text = "A";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSemcoExhFanMotorAmps
            // 
            this.txtSemcoExhFanMotorAmps.Location = new System.Drawing.Point(273, 225);
            this.txtSemcoExhFanMotorAmps.Name = "txtSemcoExhFanMotorAmps";
            this.txtSemcoExhFanMotorAmps.Size = new System.Drawing.Size(53, 20);
            this.txtSemcoExhFanMotorAmps.TabIndex = 7;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Blue;
            this.label18.Location = new System.Drawing.Point(227, 225);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(25, 25);
            this.label18.TabIndex = 62;
            this.label18.Text = "HP";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSemcoExhFanMotorHP
            // 
            this.txtSemcoExhFanMotorHP.Location = new System.Drawing.Point(168, 225);
            this.txtSemcoExhFanMotorHP.Name = "txtSemcoExhFanMotorHP";
            this.txtSemcoExhFanMotorHP.Size = new System.Drawing.Size(53, 20);
            this.txtSemcoExhFanMotorHP.TabIndex = 6;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Blue;
            this.label19.Location = new System.Drawing.Point(30, 221);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(131, 25);
            this.label19.TabIndex = 60;
            this.label19.Text = "Exhaust Fan Motor:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnSemcoDisplay
            // 
            this.btnSemcoDisplay.ForeColor = System.Drawing.Color.Blue;
            this.btnSemcoDisplay.Location = new System.Drawing.Point(166, 339);
            this.btnSemcoDisplay.Name = "btnSemcoDisplay";
            this.btnSemcoDisplay.Size = new System.Drawing.Size(150, 50);
            this.btnSemcoDisplay.TabIndex = 10;
            this.btnSemcoDisplay.Text = "Display";
            this.btnSemcoDisplay.UseVisualStyleBackColor = true;
            this.btnSemcoDisplay.Click += new System.EventHandler(this.btnSemcoDisplay_Click);
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Blue;
            this.label26.Location = new System.Drawing.Point(332, 185);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(22, 25);
            this.label26.TabIndex = 40;
            this.label26.Text = "A";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSemcoSupFanMotorAmps
            // 
            this.txtSemcoSupFanMotorAmps.Location = new System.Drawing.Point(273, 185);
            this.txtSemcoSupFanMotorAmps.Name = "txtSemcoSupFanMotorAmps";
            this.txtSemcoSupFanMotorAmps.Size = new System.Drawing.Size(53, 20);
            this.txtSemcoSupFanMotorAmps.TabIndex = 5;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Blue;
            this.label27.Location = new System.Drawing.Point(227, 185);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(25, 25);
            this.label27.TabIndex = 38;
            this.label27.Text = "HP";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSemcoSupFanMotorHP
            // 
            this.txtSemcoSupFanMotorHP.Location = new System.Drawing.Point(168, 185);
            this.txtSemcoSupFanMotorHP.Name = "txtSemcoSupFanMotorHP";
            this.txtSemcoSupFanMotorHP.Size = new System.Drawing.Size(53, 20);
            this.txtSemcoSupFanMotorHP.TabIndex = 4;
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Blue;
            this.label28.Location = new System.Drawing.Point(30, 181);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(131, 25);
            this.label28.TabIndex = 36;
            this.label28.Text = "Supply Fan Motor:";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSemcoDateOfMfg
            // 
            this.txtSemcoDateOfMfg.Location = new System.Drawing.Point(168, 145);
            this.txtSemcoDateOfMfg.Name = "txtSemcoDateOfMfg";
            this.txtSemcoDateOfMfg.Size = new System.Drawing.Size(295, 20);
            this.txtSemcoDateOfMfg.TabIndex = 3;
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Blue;
            this.label29.Location = new System.Drawing.Point(30, 141);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(131, 25);
            this.label29.TabIndex = 34;
            this.label29.Text = "Date of Manufacture:";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSemcoSerialNo
            // 
            this.txtSemcoSerialNo.Location = new System.Drawing.Point(168, 105);
            this.txtSemcoSerialNo.Name = "txtSemcoSerialNo";
            this.txtSemcoSerialNo.Size = new System.Drawing.Size(295, 20);
            this.txtSemcoSerialNo.TabIndex = 2;
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Blue;
            this.label30.Location = new System.Drawing.Point(30, 101);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(131, 25);
            this.label30.TabIndex = 32;
            this.label30.Text = "Serial Number:";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSemcoModelNo
            // 
            this.txtSemcoModelNo.Location = new System.Drawing.Point(168, 65);
            this.txtSemcoModelNo.Name = "txtSemcoModelNo";
            this.txtSemcoModelNo.Size = new System.Drawing.Size(295, 20);
            this.txtSemcoModelNo.TabIndex = 1;
            // 
            // label31
            // 
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Blue;
            this.label31.Location = new System.Drawing.Point(30, 61);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(131, 25);
            this.label31.TabIndex = 30;
            this.label31.Text = "Model Number:";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tpTrane
            // 
            this.tpTrane.Controls.Add(this.label75);
            this.tpTrane.Controls.Add(this.label68);
            this.tpTrane.Controls.Add(this.txtTraneDistBack);
            this.tpTrane.Controls.Add(this.label74);
            this.tpTrane.Controls.Add(this.txtTraneDistTop);
            this.tpTrane.Controls.Add(this.label69);
            this.tpTrane.Controls.Add(this.txtTraneDistDuck);
            this.tpTrane.Controls.Add(this.label70);
            this.tpTrane.Controls.Add(this.label71);
            this.tpTrane.Controls.Add(this.label72);
            this.tpTrane.Controls.Add(this.txtTraneDistFront);
            this.tpTrane.Controls.Add(this.txtTraneDistRSide);
            this.tpTrane.Controls.Add(this.label73);
            this.tpTrane.Controls.Add(this.txtTraneDistLSide);
            this.tpTrane.Controls.Add(this.label61);
            this.tpTrane.Controls.Add(this.txtTraneMaxEntStaticPres);
            this.tpTrane.Controls.Add(this.label62);
            this.tpTrane.Controls.Add(this.label66);
            this.tpTrane.Controls.Add(this.txtTraneMaxDischargeTemp);
            this.tpTrane.Controls.Add(this.label67);
            this.tpTrane.Controls.Add(this.label63);
            this.tpTrane.Controls.Add(this.txtTraneGasHeatMinHtgInput);
            this.tpTrane.Controls.Add(this.label64);
            this.tpTrane.Controls.Add(this.txtTraneGasHeatMaxHtgInput);
            this.tpTrane.Controls.Add(this.label65);
            this.tpTrane.Controls.Add(this.label60);
            this.tpTrane.Controls.Add(this.label59);
            this.tpTrane.Controls.Add(this.label58);
            this.tpTrane.Controls.Add(this.label57);
            this.tpTrane.Controls.Add(this.label55);
            this.tpTrane.Controls.Add(this.txtTraneCirc2Charge);
            this.tpTrane.Controls.Add(this.label56);
            this.tpTrane.Controls.Add(this.label54);
            this.tpTrane.Controls.Add(this.txtTraneCirc1Charge);
            this.tpTrane.Controls.Add(this.label53);
            this.tpTrane.Controls.Add(this.txtTraneEvapFanMtrHP);
            this.tpTrane.Controls.Add(this.txtTraneEvapFanMtrFLAVolts);
            this.tpTrane.Controls.Add(this.txtTraneEvapFanMtrHertz);
            this.tpTrane.Controls.Add(this.txtTraneEvapFanMtrPhase);
            this.tpTrane.Controls.Add(this.txtTraneEvapFanMtrQty);
            this.tpTrane.Controls.Add(this.label52);
            this.tpTrane.Controls.Add(this.txtTraneCond2FanMtrHP);
            this.tpTrane.Controls.Add(this.txtTraneCond2FanMtrFLAVolts);
            this.tpTrane.Controls.Add(this.txtTraneCond2FanMtrHertz);
            this.tpTrane.Controls.Add(this.txtTraneCond2FanMtrPhase);
            this.tpTrane.Controls.Add(this.txtTraneCond2FanMtrQty);
            this.tpTrane.Controls.Add(this.label51);
            this.tpTrane.Controls.Add(this.label35);
            this.tpTrane.Controls.Add(this.label36);
            this.tpTrane.Controls.Add(this.label47);
            this.tpTrane.Controls.Add(this.label48);
            this.tpTrane.Controls.Add(this.label49);
            this.tpTrane.Controls.Add(this.label50);
            this.tpTrane.Controls.Add(this.txtTraneCond1MtrHP);
            this.tpTrane.Controls.Add(this.txtTraneCond1FanMtrFLAVolts);
            this.tpTrane.Controls.Add(this.txtTraneCond1FanMtrHertz);
            this.tpTrane.Controls.Add(this.txtTraneCond1FanMtrPhase);
            this.tpTrane.Controls.Add(this.txtTraneCond1FanMtrQty);
            this.tpTrane.Controls.Add(this.label34);
            this.tpTrane.Controls.Add(this.txtTraneComp2LRA);
            this.tpTrane.Controls.Add(this.txtTraneComp2RLAVolts);
            this.tpTrane.Controls.Add(this.txtTraneComp2Hertz);
            this.tpTrane.Controls.Add(this.txtTraneComp2Phase);
            this.tpTrane.Controls.Add(this.txtTraneComp2Qty);
            this.tpTrane.Controls.Add(this.label33);
            this.tpTrane.Controls.Add(this.label46);
            this.tpTrane.Controls.Add(this.label45);
            this.tpTrane.Controls.Add(this.txtTraneComp1LRA);
            this.tpTrane.Controls.Add(this.label44);
            this.tpTrane.Controls.Add(this.txtTraneComp1RLAVolts);
            this.tpTrane.Controls.Add(this.label38);
            this.tpTrane.Controls.Add(this.label37);
            this.tpTrane.Controls.Add(this.btnTraneDisplay);
            this.tpTrane.Controls.Add(this.label23);
            this.tpTrane.Controls.Add(this.txtTraneTestPresLow);
            this.tpTrane.Controls.Add(this.label24);
            this.tpTrane.Controls.Add(this.txtTraneTestPresHigh);
            this.tpTrane.Controls.Add(this.label25);
            this.tpTrane.Controls.Add(this.txtTraneRefrigType);
            this.tpTrane.Controls.Add(this.label32);
            this.tpTrane.Controls.Add(this.txtTraneComp1Hertz);
            this.tpTrane.Controls.Add(this.txtTraneComp1Phase);
            this.tpTrane.Controls.Add(this.label39);
            this.tpTrane.Controls.Add(this.txtTraneComp1Qty);
            this.tpTrane.Controls.Add(this.label40);
            this.tpTrane.Controls.Add(this.txtTraneDateOfMfg);
            this.tpTrane.Controls.Add(this.label41);
            this.tpTrane.Controls.Add(this.txtTraneSerialNo);
            this.tpTrane.Controls.Add(this.label42);
            this.tpTrane.Controls.Add(this.txtTraneModelNo);
            this.tpTrane.Controls.Add(this.label43);
            this.tpTrane.Location = new System.Drawing.Point(4, 22);
            this.tpTrane.Name = "tpTrane";
            this.tpTrane.Size = new System.Drawing.Size(493, 649);
            this.tpTrane.TabIndex = 2;
            this.tpTrane.Text = "Trane";
            this.tpTrane.UseVisualStyleBackColor = true;
            // 
            // label75
            // 
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.Blue;
            this.label75.Location = new System.Drawing.Point(115, 500);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(311, 25);
            this.label75.TabIndex = 134;
            this.label75.Text = "Distance to combustible material w/wo electric Heat (INCHES):";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label68
            // 
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Blue;
            this.label68.Location = new System.Drawing.Point(323, 519);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(45, 25);
            this.label68.TabIndex = 133;
            this.label68.Text = "Back";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTraneDistBack
            // 
            this.txtTraneDistBack.Location = new System.Drawing.Point(324, 547);
            this.txtTraneDistBack.Name = "txtTraneDistBack";
            this.txtTraneDistBack.Size = new System.Drawing.Size(45, 20);
            this.txtTraneDistBack.TabIndex = 42;
            // 
            // label74
            // 
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.Blue;
            this.label74.Location = new System.Drawing.Point(114, 519);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(45, 25);
            this.label74.TabIndex = 131;
            this.label74.Text = "Top";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTraneDistTop
            // 
            this.txtTraneDistTop.Location = new System.Drawing.Point(115, 547);
            this.txtTraneDistTop.Name = "txtTraneDistTop";
            this.txtTraneDistTop.Size = new System.Drawing.Size(45, 20);
            this.txtTraneDistTop.TabIndex = 38;
            // 
            // label69
            // 
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.Blue;
            this.label69.Location = new System.Drawing.Point(376, 519);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(45, 25);
            this.label69.TabIndex = 128;
            this.label69.Text = "Duct";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTraneDistDuck
            // 
            this.txtTraneDistDuck.Location = new System.Drawing.Point(376, 547);
            this.txtTraneDistDuck.Name = "txtTraneDistDuck";
            this.txtTraneDistDuck.Size = new System.Drawing.Size(45, 20);
            this.txtTraneDistDuck.TabIndex = 43;
            // 
            // label70
            // 
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Blue;
            this.label70.Location = new System.Drawing.Point(348, 519);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(54, 25);
            this.label70.TabIndex = 126;
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label71
            // 
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.Blue;
            this.label71.Location = new System.Drawing.Point(271, 519);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(45, 25);
            this.label71.TabIndex = 124;
            this.label71.Text = "Front";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label72
            // 
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.Blue;
            this.label72.Location = new System.Drawing.Point(219, 519);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(45, 25);
            this.label72.TabIndex = 123;
            this.label72.Text = "R.Side";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTraneDistFront
            // 
            this.txtTraneDistFront.Location = new System.Drawing.Point(272, 547);
            this.txtTraneDistFront.Name = "txtTraneDistFront";
            this.txtTraneDistFront.Size = new System.Drawing.Size(45, 20);
            this.txtTraneDistFront.TabIndex = 41;
            // 
            // txtTraneDistRSide
            // 
            this.txtTraneDistRSide.Location = new System.Drawing.Point(220, 547);
            this.txtTraneDistRSide.Name = "txtTraneDistRSide";
            this.txtTraneDistRSide.Size = new System.Drawing.Size(45, 20);
            this.txtTraneDistRSide.TabIndex = 40;
            // 
            // label73
            // 
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Blue;
            this.label73.Location = new System.Drawing.Point(167, 519);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(45, 25);
            this.label73.TabIndex = 120;
            this.label73.Text = "L.Side";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTraneDistLSide
            // 
            this.txtTraneDistLSide.Location = new System.Drawing.Point(168, 547);
            this.txtTraneDistLSide.Name = "txtTraneDistLSide";
            this.txtTraneDistLSide.Size = new System.Drawing.Size(45, 20);
            this.txtTraneDistLSide.TabIndex = 39;
            // 
            // label61
            // 
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Blue;
            this.label61.Location = new System.Drawing.Point(411, 477);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(79, 25);
            this.label61.TabIndex = 117;
            this.label61.Text = "Inches W.C.";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTraneMaxEntStaticPres
            // 
            this.txtTraneMaxEntStaticPres.Location = new System.Drawing.Point(168, 477);
            this.txtTraneMaxEntStaticPres.Name = "txtTraneMaxEntStaticPres";
            this.txtTraneMaxEntStaticPres.Size = new System.Drawing.Size(240, 20);
            this.txtTraneMaxEntStaticPres.TabIndex = 37;
            // 
            // label62
            // 
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Blue;
            this.label62.Location = new System.Drawing.Point(3, 473);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(158, 25);
            this.label62.TabIndex = 115;
            this.label62.Text = "Max Ext Static Pressure:";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label66
            // 
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Blue;
            this.label66.Location = new System.Drawing.Point(412, 450);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(62, 25);
            this.label66.TabIndex = 114;
            this.label66.Text = "Deg  F";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTraneMaxDischargeTemp
            // 
            this.txtTraneMaxDischargeTemp.Location = new System.Drawing.Point(169, 450);
            this.txtTraneMaxDischargeTemp.Name = "txtTraneMaxDischargeTemp";
            this.txtTraneMaxDischargeTemp.Size = new System.Drawing.Size(240, 20);
            this.txtTraneMaxDischargeTemp.TabIndex = 36;
            // 
            // label67
            // 
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Blue;
            this.label67.Location = new System.Drawing.Point(4, 446);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(158, 25);
            this.label67.TabIndex = 112;
            this.label67.Text = "Max Discharge Temp:";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label63
            // 
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Blue;
            this.label63.Location = new System.Drawing.Point(419, 422);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(38, 25);
            this.label63.TabIndex = 111;
            this.label63.Text = "BTUH";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTraneGasHeatMinHtgInput
            // 
            this.txtTraneGasHeatMinHtgInput.Location = new System.Drawing.Point(330, 422);
            this.txtTraneGasHeatMinHtgInput.Name = "txtTraneGasHeatMinHtgInput";
            this.txtTraneGasHeatMinHtgInput.Size = new System.Drawing.Size(80, 20);
            this.txtTraneGasHeatMinHtgInput.TabIndex = 35;
            // 
            // label64
            // 
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.Blue;
            this.label64.Location = new System.Drawing.Point(274, 422);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(37, 25);
            this.label64.TabIndex = 109;
            this.label64.Text = "BTUH";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTraneGasHeatMaxHtgInput
            // 
            this.txtTraneGasHeatMaxHtgInput.Location = new System.Drawing.Point(185, 422);
            this.txtTraneGasHeatMaxHtgInput.Name = "txtTraneGasHeatMaxHtgInput";
            this.txtTraneGasHeatMaxHtgInput.Size = new System.Drawing.Size(80, 20);
            this.txtTraneGasHeatMaxHtgInput.TabIndex = 34;
            // 
            // label65
            // 
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Blue;
            this.label65.Location = new System.Drawing.Point(36, 418);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(131, 25);
            this.label65.TabIndex = 107;
            this.label65.Text = "Gas Heat:";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label60
            // 
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Blue;
            this.label60.Location = new System.Drawing.Point(317, 394);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(130, 25);
            this.label60.TabIndex = 106;
            this.label60.Text = "Minimum Heating Input";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label59
            // 
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Blue;
            this.label59.Location = new System.Drawing.Point(174, 394);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(130, 25);
            this.label59.TabIndex = 105;
            this.label59.Text = "Maximum Heating Input";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label58
            // 
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Blue;
            this.label58.Location = new System.Drawing.Point(311, 368);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(37, 25);
            this.label58.TabIndex = 104;
            this.label58.Text = "LOW";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label57
            // 
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Blue;
            this.label57.Location = new System.Drawing.Point(167, 368);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(37, 25);
            this.label57.TabIndex = 103;
            this.label57.Text = "HIGH";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label55
            // 
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Blue;
            this.label55.Location = new System.Drawing.Point(427, 339);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(45, 25);
            this.label55.TabIndex = 102;
            this.label55.Text = "LBS";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTraneCirc2Charge
            // 
            this.txtTraneCirc2Charge.Location = new System.Drawing.Point(167, 339);
            this.txtTraneCirc2Charge.Name = "txtTraneCirc2Charge";
            this.txtTraneCirc2Charge.Size = new System.Drawing.Size(254, 20);
            this.txtTraneCirc2Charge.TabIndex = 31;
            // 
            // label56
            // 
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Blue;
            this.label56.Location = new System.Drawing.Point(29, 335);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(131, 25);
            this.label56.TabIndex = 100;
            this.label56.Text = "Circuit 2 Charge:";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label54
            // 
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Blue;
            this.label54.Location = new System.Drawing.Point(428, 312);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(45, 25);
            this.label54.TabIndex = 99;
            this.label54.Text = "LBS";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTraneCirc1Charge
            // 
            this.txtTraneCirc1Charge.Location = new System.Drawing.Point(168, 312);
            this.txtTraneCirc1Charge.Name = "txtTraneCirc1Charge";
            this.txtTraneCirc1Charge.Size = new System.Drawing.Size(254, 20);
            this.txtTraneCirc1Charge.TabIndex = 30;
            // 
            // label53
            // 
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Blue;
            this.label53.Location = new System.Drawing.Point(30, 308);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(131, 25);
            this.label53.TabIndex = 97;
            this.label53.Text = "Circuit 1 Charge:";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTraneEvapFanMtrHP
            // 
            this.txtTraneEvapFanMtrHP.Location = new System.Drawing.Point(407, 253);
            this.txtTraneEvapFanMtrHP.Name = "txtTraneEvapFanMtrHP";
            this.txtTraneEvapFanMtrHP.Size = new System.Drawing.Size(45, 20);
            this.txtTraneEvapFanMtrHP.TabIndex = 28;
            // 
            // txtTraneEvapFanMtrFLAVolts
            // 
            this.txtTraneEvapFanMtrFLAVolts.Location = new System.Drawing.Point(321, 253);
            this.txtTraneEvapFanMtrFLAVolts.Name = "txtTraneEvapFanMtrFLAVolts";
            this.txtTraneEvapFanMtrFLAVolts.Size = new System.Drawing.Size(80, 20);
            this.txtTraneEvapFanMtrFLAVolts.TabIndex = 27;
            // 
            // txtTraneEvapFanMtrHertz
            // 
            this.txtTraneEvapFanMtrHertz.Location = new System.Drawing.Point(271, 253);
            this.txtTraneEvapFanMtrHertz.Name = "txtTraneEvapFanMtrHertz";
            this.txtTraneEvapFanMtrHertz.Size = new System.Drawing.Size(45, 20);
            this.txtTraneEvapFanMtrHertz.TabIndex = 26;
            // 
            // txtTraneEvapFanMtrPhase
            // 
            this.txtTraneEvapFanMtrPhase.Location = new System.Drawing.Point(219, 253);
            this.txtTraneEvapFanMtrPhase.Name = "txtTraneEvapFanMtrPhase";
            this.txtTraneEvapFanMtrPhase.Size = new System.Drawing.Size(45, 20);
            this.txtTraneEvapFanMtrPhase.TabIndex = 25;
            // 
            // txtTraneEvapFanMtrQty
            // 
            this.txtTraneEvapFanMtrQty.Location = new System.Drawing.Point(167, 253);
            this.txtTraneEvapFanMtrQty.Name = "txtTraneEvapFanMtrQty";
            this.txtTraneEvapFanMtrQty.Size = new System.Drawing.Size(45, 20);
            this.txtTraneEvapFanMtrQty.TabIndex = 24;
            // 
            // label52
            // 
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Blue;
            this.label52.Location = new System.Drawing.Point(29, 249);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(131, 25);
            this.label52.TabIndex = 91;
            this.label52.Text = "Evap Fan Motor:";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTraneCond2FanMtrHP
            // 
            this.txtTraneCond2FanMtrHP.Location = new System.Drawing.Point(407, 227);
            this.txtTraneCond2FanMtrHP.Name = "txtTraneCond2FanMtrHP";
            this.txtTraneCond2FanMtrHP.Size = new System.Drawing.Size(45, 20);
            this.txtTraneCond2FanMtrHP.TabIndex = 23;
            // 
            // txtTraneCond2FanMtrFLAVolts
            // 
            this.txtTraneCond2FanMtrFLAVolts.Location = new System.Drawing.Point(321, 227);
            this.txtTraneCond2FanMtrFLAVolts.Name = "txtTraneCond2FanMtrFLAVolts";
            this.txtTraneCond2FanMtrFLAVolts.Size = new System.Drawing.Size(80, 20);
            this.txtTraneCond2FanMtrFLAVolts.TabIndex = 22;
            // 
            // txtTraneCond2FanMtrHertz
            // 
            this.txtTraneCond2FanMtrHertz.Location = new System.Drawing.Point(271, 227);
            this.txtTraneCond2FanMtrHertz.Name = "txtTraneCond2FanMtrHertz";
            this.txtTraneCond2FanMtrHertz.Size = new System.Drawing.Size(45, 20);
            this.txtTraneCond2FanMtrHertz.TabIndex = 21;
            // 
            // txtTraneCond2FanMtrPhase
            // 
            this.txtTraneCond2FanMtrPhase.Location = new System.Drawing.Point(219, 227);
            this.txtTraneCond2FanMtrPhase.Name = "txtTraneCond2FanMtrPhase";
            this.txtTraneCond2FanMtrPhase.Size = new System.Drawing.Size(45, 20);
            this.txtTraneCond2FanMtrPhase.TabIndex = 20;
            // 
            // txtTraneCond2FanMtrQty
            // 
            this.txtTraneCond2FanMtrQty.Location = new System.Drawing.Point(167, 227);
            this.txtTraneCond2FanMtrQty.Name = "txtTraneCond2FanMtrQty";
            this.txtTraneCond2FanMtrQty.Size = new System.Drawing.Size(45, 20);
            this.txtTraneCond2FanMtrQty.TabIndex = 19;
            // 
            // label51
            // 
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Blue;
            this.label51.Location = new System.Drawing.Point(29, 223);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(131, 25);
            this.label51.TabIndex = 85;
            this.label51.Text = "Cond Fan Motor:";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label35
            // 
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Blue;
            this.label35.Location = new System.Drawing.Point(322, 174);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(80, 25);
            this.label35.TabIndex = 84;
            this.label35.Text = "FLA-Volts";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label36
            // 
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Blue;
            this.label36.Location = new System.Drawing.Point(408, 174);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(45, 25);
            this.label36.TabIndex = 83;
            this.label36.Text = "HP";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label47
            // 
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Blue;
            this.label47.Location = new System.Drawing.Point(348, 174);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(54, 25);
            this.label47.TabIndex = 82;
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label48
            // 
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Blue;
            this.label48.Location = new System.Drawing.Point(271, 174);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(45, 25);
            this.label48.TabIndex = 81;
            this.label48.Text = "Hz";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label49
            // 
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Blue;
            this.label49.Location = new System.Drawing.Point(219, 174);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(45, 25);
            this.label49.TabIndex = 80;
            this.label49.Text = "Ph";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label50
            // 
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Blue;
            this.label50.Location = new System.Drawing.Point(167, 174);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(45, 25);
            this.label50.TabIndex = 79;
            this.label50.Text = "Qty";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTraneCond1MtrHP
            // 
            this.txtTraneCond1MtrHP.Location = new System.Drawing.Point(408, 202);
            this.txtTraneCond1MtrHP.Name = "txtTraneCond1MtrHP";
            this.txtTraneCond1MtrHP.Size = new System.Drawing.Size(45, 20);
            this.txtTraneCond1MtrHP.TabIndex = 18;
            // 
            // txtTraneCond1FanMtrFLAVolts
            // 
            this.txtTraneCond1FanMtrFLAVolts.Location = new System.Drawing.Point(322, 202);
            this.txtTraneCond1FanMtrFLAVolts.Name = "txtTraneCond1FanMtrFLAVolts";
            this.txtTraneCond1FanMtrFLAVolts.Size = new System.Drawing.Size(80, 20);
            this.txtTraneCond1FanMtrFLAVolts.TabIndex = 17;
            // 
            // txtTraneCond1FanMtrHertz
            // 
            this.txtTraneCond1FanMtrHertz.Location = new System.Drawing.Point(272, 202);
            this.txtTraneCond1FanMtrHertz.Name = "txtTraneCond1FanMtrHertz";
            this.txtTraneCond1FanMtrHertz.Size = new System.Drawing.Size(45, 20);
            this.txtTraneCond1FanMtrHertz.TabIndex = 16;
            // 
            // txtTraneCond1FanMtrPhase
            // 
            this.txtTraneCond1FanMtrPhase.Location = new System.Drawing.Point(220, 202);
            this.txtTraneCond1FanMtrPhase.Name = "txtTraneCond1FanMtrPhase";
            this.txtTraneCond1FanMtrPhase.Size = new System.Drawing.Size(45, 20);
            this.txtTraneCond1FanMtrPhase.TabIndex = 15;
            // 
            // txtTraneCond1FanMtrQty
            // 
            this.txtTraneCond1FanMtrQty.Location = new System.Drawing.Point(168, 202);
            this.txtTraneCond1FanMtrQty.Name = "txtTraneCond1FanMtrQty";
            this.txtTraneCond1FanMtrQty.Size = new System.Drawing.Size(45, 20);
            this.txtTraneCond1FanMtrQty.TabIndex = 14;
            // 
            // label34
            // 
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Blue;
            this.label34.Location = new System.Drawing.Point(30, 198);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(131, 25);
            this.label34.TabIndex = 73;
            this.label34.Text = "Cond Fan Motor:";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTraneComp2LRA
            // 
            this.txtTraneComp2LRA.Location = new System.Drawing.Point(408, 151);
            this.txtTraneComp2LRA.Name = "txtTraneComp2LRA";
            this.txtTraneComp2LRA.Size = new System.Drawing.Size(45, 20);
            this.txtTraneComp2LRA.TabIndex = 13;
            // 
            // txtTraneComp2RLAVolts
            // 
            this.txtTraneComp2RLAVolts.Location = new System.Drawing.Point(322, 151);
            this.txtTraneComp2RLAVolts.Name = "txtTraneComp2RLAVolts";
            this.txtTraneComp2RLAVolts.Size = new System.Drawing.Size(80, 20);
            this.txtTraneComp2RLAVolts.TabIndex = 12;
            // 
            // txtTraneComp2Hertz
            // 
            this.txtTraneComp2Hertz.Location = new System.Drawing.Point(272, 151);
            this.txtTraneComp2Hertz.Name = "txtTraneComp2Hertz";
            this.txtTraneComp2Hertz.Size = new System.Drawing.Size(45, 20);
            this.txtTraneComp2Hertz.TabIndex = 11;
            // 
            // txtTraneComp2Phase
            // 
            this.txtTraneComp2Phase.Location = new System.Drawing.Point(220, 151);
            this.txtTraneComp2Phase.Name = "txtTraneComp2Phase";
            this.txtTraneComp2Phase.Size = new System.Drawing.Size(45, 20);
            this.txtTraneComp2Phase.TabIndex = 10;
            // 
            // txtTraneComp2Qty
            // 
            this.txtTraneComp2Qty.Location = new System.Drawing.Point(168, 151);
            this.txtTraneComp2Qty.Name = "txtTraneComp2Qty";
            this.txtTraneComp2Qty.Size = new System.Drawing.Size(45, 20);
            this.txtTraneComp2Qty.TabIndex = 9;
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Blue;
            this.label33.Location = new System.Drawing.Point(30, 147);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(131, 25);
            this.label33.TabIndex = 67;
            this.label33.Text = "Compressor #2:";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label46
            // 
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Blue;
            this.label46.Location = new System.Drawing.Point(322, 98);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(80, 25);
            this.label46.TabIndex = 66;
            this.label46.Text = "RLA-Volts";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label45
            // 
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Blue;
            this.label45.Location = new System.Drawing.Point(408, 98);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(45, 25);
            this.label45.TabIndex = 65;
            this.label45.Text = "LRA";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTraneComp1LRA
            // 
            this.txtTraneComp1LRA.Location = new System.Drawing.Point(408, 126);
            this.txtTraneComp1LRA.Name = "txtTraneComp1LRA";
            this.txtTraneComp1LRA.Size = new System.Drawing.Size(45, 20);
            this.txtTraneComp1LRA.TabIndex = 8;
            // 
            // label44
            // 
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Blue;
            this.label44.Location = new System.Drawing.Point(348, 98);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(54, 25);
            this.label44.TabIndex = 63;
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTraneComp1RLAVolts
            // 
            this.txtTraneComp1RLAVolts.Location = new System.Drawing.Point(322, 126);
            this.txtTraneComp1RLAVolts.Name = "txtTraneComp1RLAVolts";
            this.txtTraneComp1RLAVolts.Size = new System.Drawing.Size(80, 20);
            this.txtTraneComp1RLAVolts.TabIndex = 7;
            // 
            // label38
            // 
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Blue;
            this.label38.Location = new System.Drawing.Point(271, 98);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(45, 25);
            this.label38.TabIndex = 61;
            this.label38.Text = "Hz";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label37
            // 
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Blue;
            this.label37.Location = new System.Drawing.Point(219, 98);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(45, 25);
            this.label37.TabIndex = 60;
            this.label37.Text = "Ph";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnTraneDisplay
            // 
            this.btnTraneDisplay.ForeColor = System.Drawing.Color.Blue;
            this.btnTraneDisplay.Location = new System.Drawing.Point(185, 583);
            this.btnTraneDisplay.Name = "btnTraneDisplay";
            this.btnTraneDisplay.Size = new System.Drawing.Size(150, 50);
            this.btnTraneDisplay.TabIndex = 44;
            this.btnTraneDisplay.Text = "Display";
            this.btnTraneDisplay.UseVisualStyleBackColor = true;
            this.btnTraneDisplay.Click += new System.EventHandler(this.btnTraneDisplay_Click);
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Blue;
            this.label23.Location = new System.Drawing.Point(413, 368);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(38, 25);
            this.label23.TabIndex = 57;
            this.label23.Text = "PSIG";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTraneTestPresLow
            // 
            this.txtTraneTestPresLow.Location = new System.Drawing.Point(355, 368);
            this.txtTraneTestPresLow.Name = "txtTraneTestPresLow";
            this.txtTraneTestPresLow.Size = new System.Drawing.Size(53, 20);
            this.txtTraneTestPresLow.TabIndex = 33;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Blue;
            this.label24.Location = new System.Drawing.Point(268, 368);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(37, 25);
            this.label24.TabIndex = 55;
            this.label24.Text = "PSIG";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTraneTestPresHigh
            // 
            this.txtTraneTestPresHigh.Location = new System.Drawing.Point(209, 368);
            this.txtTraneTestPresHigh.Name = "txtTraneTestPresHigh";
            this.txtTraneTestPresHigh.Size = new System.Drawing.Size(53, 20);
            this.txtTraneTestPresHigh.TabIndex = 32;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Blue;
            this.label25.Location = new System.Drawing.Point(30, 364);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(131, 25);
            this.label25.TabIndex = 53;
            this.label25.Text = "Test  Pressure:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTraneRefrigType
            // 
            this.txtTraneRefrigType.Location = new System.Drawing.Point(168, 283);
            this.txtTraneRefrigType.Name = "txtTraneRefrigType";
            this.txtTraneRefrigType.Size = new System.Drawing.Size(295, 20);
            this.txtTraneRefrigType.TabIndex = 29;
            // 
            // label32
            // 
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Blue;
            this.label32.Location = new System.Drawing.Point(30, 279);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(131, 25);
            this.label32.TabIndex = 51;
            this.label32.Text = "Refrigerant Type:";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTraneComp1Hertz
            // 
            this.txtTraneComp1Hertz.Location = new System.Drawing.Point(272, 126);
            this.txtTraneComp1Hertz.Name = "txtTraneComp1Hertz";
            this.txtTraneComp1Hertz.Size = new System.Drawing.Size(45, 20);
            this.txtTraneComp1Hertz.TabIndex = 6;
            // 
            // txtTraneComp1Phase
            // 
            this.txtTraneComp1Phase.Location = new System.Drawing.Point(220, 126);
            this.txtTraneComp1Phase.Name = "txtTraneComp1Phase";
            this.txtTraneComp1Phase.Size = new System.Drawing.Size(45, 20);
            this.txtTraneComp1Phase.TabIndex = 5;
            // 
            // label39
            // 
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Blue;
            this.label39.Location = new System.Drawing.Point(167, 98);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(45, 25);
            this.label39.TabIndex = 38;
            this.label39.Text = "Qty";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTraneComp1Qty
            // 
            this.txtTraneComp1Qty.Location = new System.Drawing.Point(168, 126);
            this.txtTraneComp1Qty.Name = "txtTraneComp1Qty";
            this.txtTraneComp1Qty.Size = new System.Drawing.Size(45, 20);
            this.txtTraneComp1Qty.TabIndex = 4;
            // 
            // label40
            // 
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Blue;
            this.label40.Location = new System.Drawing.Point(30, 122);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(131, 25);
            this.label40.TabIndex = 36;
            this.label40.Text = "Compressor #1:";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTraneDateOfMfg
            // 
            this.txtTraneDateOfMfg.Location = new System.Drawing.Point(168, 71);
            this.txtTraneDateOfMfg.Name = "txtTraneDateOfMfg";
            this.txtTraneDateOfMfg.Size = new System.Drawing.Size(295, 20);
            this.txtTraneDateOfMfg.TabIndex = 3;
            // 
            // label41
            // 
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Blue;
            this.label41.Location = new System.Drawing.Point(30, 67);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(131, 25);
            this.label41.TabIndex = 34;
            this.label41.Text = "Date of Manufacture:";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTraneSerialNo
            // 
            this.txtTraneSerialNo.Location = new System.Drawing.Point(168, 42);
            this.txtTraneSerialNo.Name = "txtTraneSerialNo";
            this.txtTraneSerialNo.Size = new System.Drawing.Size(295, 20);
            this.txtTraneSerialNo.TabIndex = 2;
            // 
            // label42
            // 
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Blue;
            this.label42.Location = new System.Drawing.Point(30, 38);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(131, 25);
            this.label42.TabIndex = 32;
            this.label42.Text = "Serial Number:";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTraneModelNo
            // 
            this.txtTraneModelNo.Location = new System.Drawing.Point(168, 13);
            this.txtTraneModelNo.Name = "txtTraneModelNo";
            this.txtTraneModelNo.Size = new System.Drawing.Size(295, 20);
            this.txtTraneModelNo.TabIndex = 1;
            // 
            // label43
            // 
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Blue;
            this.label43.Location = new System.Drawing.Point(30, 9);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(131, 25);
            this.label43.TabIndex = 30;
            this.label43.Text = "Model Number:";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // bthExit
            // 
            this.bthExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bthExit.ForeColor = System.Drawing.Color.Red;
            this.bthExit.Location = new System.Drawing.Point(434, 9);
            this.bthExit.Name = "bthExit";
            this.bthExit.Size = new System.Drawing.Size(75, 23);
            this.bthExit.TabIndex = 2;
            this.bthExit.Text = "Exit";
            this.bthExit.UseVisualStyleBackColor = true;
            this.bthExit.Click += new System.EventHandler(this.bthExit_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(188, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Tradewinds";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmTradewinds
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(527, 721);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bthExit);
            this.Controls.Add(this.tabControl1);
            this.Name = "frmTradewinds";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tradewinds";
            this.tabControl1.ResumeLayout(false);
            this.tpKCC.ResumeLayout(false);
            this.tpKCC.PerformLayout();
            this.tpSemco.ResumeLayout(false);
            this.tpSemco.PerformLayout();
            this.tpTrane.ResumeLayout(false);
            this.tpTrane.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpKCC;
        private System.Windows.Forms.TabPage tpSemco;
        private System.Windows.Forms.TabPage tpTrane;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bthExit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnDisplay;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btnSemcoDisplay;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button btnTraneDisplay;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        public System.Windows.Forms.TextBox txtTraneEvapFanMtrHP;
        public System.Windows.Forms.TextBox txtTraneEvapFanMtrFLAVolts;
        public System.Windows.Forms.TextBox txtTraneEvapFanMtrHertz;
        public System.Windows.Forms.TextBox txtTraneEvapFanMtrPhase;
        public System.Windows.Forms.TextBox txtTraneEvapFanMtrQty;
        public System.Windows.Forms.TextBox txtTraneCond2FanMtrHP;
        public System.Windows.Forms.TextBox txtTraneCond2FanMtrFLAVolts;
        public System.Windows.Forms.TextBox txtTraneCond2FanMtrHertz;
        public System.Windows.Forms.TextBox txtTraneCond2FanMtrPhase;
        public System.Windows.Forms.TextBox txtTraneCond2FanMtrQty;
        public System.Windows.Forms.TextBox txtTraneCirc2Charge;
        public System.Windows.Forms.TextBox txtTraneCirc1Charge;
        public System.Windows.Forms.TextBox txtTraneDistBack;
        public System.Windows.Forms.TextBox txtTraneDistTop;
        public System.Windows.Forms.TextBox txtTraneDistDuck;
        public System.Windows.Forms.TextBox txtTraneDistFront;
        public System.Windows.Forms.TextBox txtTraneDistRSide;
        public System.Windows.Forms.TextBox txtTraneDistLSide;
        public System.Windows.Forms.TextBox txtTraneMaxEntStaticPres;
        public System.Windows.Forms.TextBox txtTraneMaxDischargeTemp;
        public System.Windows.Forms.TextBox txtTraneGasHeatMinHtgInput;
        public System.Windows.Forms.TextBox txtTraneGasHeatMaxHtgInput;
        public System.Windows.Forms.TextBox txtTraneCond1MtrHP;
        public System.Windows.Forms.TextBox txtTraneCond1FanMtrFLAVolts;
        public System.Windows.Forms.TextBox txtTraneCond1FanMtrHertz;
        public System.Windows.Forms.TextBox txtTraneCond1FanMtrPhase;
        public System.Windows.Forms.TextBox txtTraneCond1FanMtrQty;
        public System.Windows.Forms.TextBox txtTraneComp2LRA;
        public System.Windows.Forms.TextBox txtTraneComp2RLAVolts;
        public System.Windows.Forms.TextBox txtTraneComp2Hertz;
        public System.Windows.Forms.TextBox txtTraneComp2Phase;
        public System.Windows.Forms.TextBox txtTraneComp2Qty;
        public System.Windows.Forms.TextBox txtTraneComp1LRA;
        public System.Windows.Forms.TextBox txtTraneComp1RLAVolts;
        public System.Windows.Forms.TextBox txtTraneTestPresLow;
        public System.Windows.Forms.TextBox txtTraneTestPresHigh;
        public System.Windows.Forms.TextBox txtTraneRefrigType;
        public System.Windows.Forms.TextBox txtTraneComp1Hertz;
        public System.Windows.Forms.TextBox txtTraneComp1Phase;
        public System.Windows.Forms.TextBox txtTraneComp1Qty;
        public System.Windows.Forms.TextBox txtTraneDateOfMfg;
        public System.Windows.Forms.TextBox txtTraneSerialNo;
        public System.Windows.Forms.TextBox txtTraneModelNo;
        public System.Windows.Forms.TextBox txtSemcoExhFanMotorAmps;
        public System.Windows.Forms.TextBox txtSemcoExhFanMotorHP;
        public System.Windows.Forms.TextBox txtSemcoSupFanMotorAmps;
        public System.Windows.Forms.TextBox txtSemcoSupFanMotorHP;
        public System.Windows.Forms.TextBox txtSemcoDateOfMfg;
        public System.Windows.Forms.TextBox txtSemcoSerialNo;
        public System.Windows.Forms.TextBox txtSemcoModelNo;
        public System.Windows.Forms.TextBox txtSemcoTEWheelMotorAmps;
        public System.Windows.Forms.TextBox txtSemcoTEWheelMotorHP;
        public System.Windows.Forms.TextBox txtModelNo;
        public System.Windows.Forms.TextBox txtSerialNo;
        public System.Windows.Forms.TextBox txtVoltage;
        public System.Windows.Forms.TextBox txtDateOfAsm;
        public System.Windows.Forms.TextBox txtVoltMax;
        public System.Windows.Forms.TextBox txtkArmsSym;
        public System.Windows.Forms.TextBox txtMaxOverProtection;
        public System.Windows.Forms.TextBox txtMinCircuitAmp;
        public System.Windows.Forms.TextBox txtFullLoadAmp;
        public System.Windows.Forms.TextBox txtControlPower;
        public System.Windows.Forms.TextBox txtPhase;
        public System.Windows.Forms.TextBox txtHertz;
        public System.Windows.Forms.TextBox txtTypeOfUse;
    }
}