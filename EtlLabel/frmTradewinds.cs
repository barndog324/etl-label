﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KCC.OA.Etl
{
    public partial class frmTradewinds : Form
    {
        public frmTradewinds()
        {
            InitializeComponent();
        }

        private void btnDisplay_Click(object sender, EventArgs e)
        {
            frmPrintTradewinds frmPrint = new frmPrintTradewinds();

            VS_DataSet ds = new VS_DataSet();
            ds.TradewindsKCCDataTable.Rows.Add(txtModelNo.Text,
                                               txtSerialNo.Text,
                                               txtDateOfAsm.Text,
                                               txtVoltage.Text,
                                               txtHertz.Text,
                                               txtPhase.Text,
                                               txtControlPower.Text,
                                               txtFullLoadAmp.Text,
                                               txtMinCircuitAmp.Text,
                                               txtMaxOverProtection.Text,
                                               txtTypeOfUse.Text,
                                               txtkArmsSym.Text,
                                               txtVoltMax.Text);

            ReportDocument cryRpt = new ReportDocument();

            //reportString = @"F:\Projects\EtlLabel\reports\OA_Label_v008.rpt";

            string reportString = @"\\epicor905app\Apps\OAU\EtlLabel\reports\TradewindsKCC.v1.rpt";   // Default Crystal 

            cryRpt.Load(reportString);

            cryRpt.SetDataSource(ds.Tables["TradewindsKCCDataTable"]);

            frmPrint.crystalReportViewer1.ReportSource = cryRpt;

            frmPrint.ShowDialog();
            frmPrint.crystalReportViewer1.Refresh();  
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSemcoExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSemcoDisplay_Click(object sender, EventArgs e)
        {
            frmPrintTradewinds frmPrint = new frmPrintTradewinds();

            VS_DataSet ds = new VS_DataSet();
            ds.TradewindsSemcoDataTable.Rows.Add(txtSemcoModelNo.Text,
                                                 txtSemcoSerialNo.Text,
                                                 txtSemcoDateOfMfg.Text,
                                                 txtSemcoSupFanMotorHP.Text,
                                                 txtSemcoSupFanMotorAmps.Text,
                                                 txtSemcoExhFanMotorHP.Text,
                                                 txtSemcoExhFanMotorAmps.Text,
                                                 txtSemcoTEWheelMotorHP.Text,
                                                 txtSemcoTEWheelMotorAmps.Text);

            ReportDocument cryRpt = new ReportDocument();

            //reportString = @"F:\Projects\EtlLabel\reports\OA_Label_v008.rpt";

            string reportString = @"\\epicor905app\Apps\OAU\EtlLabel\reports\TradewindsSemco.v1.rpt";   // Default Crystal 

            cryRpt.Load(reportString);

            cryRpt.SetDataSource(ds.Tables["TradewindsSemcoDataTable"]);

            frmPrint.crystalReportViewer1.ReportSource = cryRpt;

            frmPrint.ShowDialog();
            frmPrint.crystalReportViewer1.Refresh();  
        }

        private void btnTraneExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTraneDisplay_Click(object sender, EventArgs e)
        {
            frmPrintTradewinds frmPrint = new frmPrintTradewinds();

            VS_DataSet ds = new VS_DataSet();
            ds.TradewindsTraneDataTable.Rows.Add(txtTraneModelNo.Text,
                                                 txtTraneSerialNo.Text,
                                                 txtTraneDateOfMfg.Text,
                                                 txtTraneComp1Qty.Text,
                                                 txtTraneComp1Phase.Text,
                                                 txtTraneComp1Hertz.Text,
                                                 txtTraneComp1RLAVolts.Text,
                                                 txtTraneComp1LRA.Text,
                                                 txtTraneComp2Qty.Text,
                                                 txtTraneComp2Phase.Text,
                                                 txtTraneComp2Hertz.Text,
                                                 txtTraneComp2RLAVolts.Text,
                                                 txtTraneComp2LRA.Text,
                                                 txtTraneCond1FanMtrQty.Text,
                                                 txtTraneCond1FanMtrPhase.Text,
                                                 txtTraneCond1FanMtrHertz.Text,
                                                 txtTraneCond1FanMtrFLAVolts.Text,
                                                 txtTraneCond1MtrHP.Text,
                                                 txtTraneCond2FanMtrQty.Text,
                                                 txtTraneCond2FanMtrPhase.Text,
                                                 txtTraneCond2FanMtrHertz.Text,
                                                 txtTraneCond2FanMtrFLAVolts.Text,
                                                 txtTraneCond2FanMtrHP.Text,
                                                 txtTraneEvapFanMtrQty.Text,
                                                 txtTraneEvapFanMtrPhase.Text,
                                                 txtTraneEvapFanMtrHertz.Text,
                                                 txtTraneEvapFanMtrFLAVolts.Text,
                                                 txtTraneEvapFanMtrHP.Text,
                                                 txtTraneRefrigType.Text,
                                                 txtTraneCirc1Charge.Text,
                                                 txtTraneCirc2Charge.Text,
                                                 txtTraneTestPresHigh.Text,
                                                 txtTraneTestPresLow.Text,
                                                 txtTraneGasHeatMaxHtgInput.Text,
                                                 txtTraneGasHeatMinHtgInput.Text,
                                                 txtTraneMaxDischargeTemp.Text,
                                                 txtTraneMaxEntStaticPres.Text,
                                                 txtTraneDistTop.Text,
                                                 txtTraneDistLSide.Text,
                                                 txtTraneDistRSide.Text,
                                                 txtTraneDistFront.Text,
                                                 txtTraneDistBack.Text,
                                                 txtTraneDistDuck.Text
                                        );

            ReportDocument cryRpt = new ReportDocument();

            //reportString = @"F:\Projects\EtlLabel\reports\OA_Label_v008.rpt";

            string reportString = @"\\epicor905app\Apps\OAU\EtlLabel\reports\TradewindsTrane.v1.rpt";   // Default Crystal 

            cryRpt.Load(reportString);

            cryRpt.SetDataSource(ds.Tables["TradewindsTraneDataTable"]);

            frmPrint.crystalReportViewer1.ReportSource = cryRpt;

            frmPrint.ShowDialog();
            frmPrint.crystalReportViewer1.Refresh();  
        }

        private void bthExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }      
       
    }
}
