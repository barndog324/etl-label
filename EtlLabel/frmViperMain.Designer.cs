namespace KCC.OA.Etl
{
    partial class frmViperMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonMainNew = new System.Windows.Forms.Button();
            this.buttonNewOpen = new System.Windows.Forms.Button();
            this.buttonNewExit = new System.Windows.Forms.Button();
            this.vS2_DataSet = new KCC.OA.Etl.VS2_DataSet();
            this.orderdtlBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.orderdtlTableAdapter = new KCC.OA.Etl.VS2_DataSetTableAdapters.orderdtlTableAdapter();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vS2_DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderdtlBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.orderToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(265, 29);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(52, 25);
            this.fileToolStripMenuItem.Text = "File";
            this.fileToolStripMenuItem.Click += new System.EventHandler(this.fileToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(112, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // orderToolStripMenuItem
            // 
            this.orderToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newOrderToolStripMenuItem,
            this.openOrderToolStripMenuItem,
            this.deleteOrderToolStripMenuItem});
            this.orderToolStripMenuItem.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orderToolStripMenuItem.Name = "orderToolStripMenuItem";
            this.orderToolStripMenuItem.Size = new System.Drawing.Size(70, 25);
            this.orderToolStripMenuItem.Text = "Order";
            // 
            // newOrderToolStripMenuItem
            // 
            this.newOrderToolStripMenuItem.Name = "newOrderToolStripMenuItem";
            this.newOrderToolStripMenuItem.Size = new System.Drawing.Size(188, 26);
            this.newOrderToolStripMenuItem.Text = "New Order";
            this.newOrderToolStripMenuItem.Click += new System.EventHandler(this.newOrderToolStripMenuItem_Click);
            // 
            // openOrderToolStripMenuItem
            // 
            this.openOrderToolStripMenuItem.Name = "openOrderToolStripMenuItem";
            this.openOrderToolStripMenuItem.Size = new System.Drawing.Size(188, 26);
            this.openOrderToolStripMenuItem.Text = "Open Order";
            this.openOrderToolStripMenuItem.Click += new System.EventHandler(this.openOrderToolStripMenuItem_Click);
            // 
            // deleteOrderToolStripMenuItem
            // 
            this.deleteOrderToolStripMenuItem.Enabled = false;
            this.deleteOrderToolStripMenuItem.Name = "deleteOrderToolStripMenuItem";
            this.deleteOrderToolStripMenuItem.Size = new System.Drawing.Size(188, 26);
            this.deleteOrderToolStripMenuItem.Text = "Delete Order";
            this.deleteOrderToolStripMenuItem.Click += new System.EventHandler(this.deleteOrderToolStripMenuItem_Click);
            // 
            // buttonMainNew
            // 
            this.buttonMainNew.ForeColor = System.Drawing.Color.Blue;
            this.buttonMainNew.Location = new System.Drawing.Point(54, 78);
            this.buttonMainNew.Name = "buttonMainNew";
            this.buttonMainNew.Size = new System.Drawing.Size(160, 44);
            this.buttonMainNew.TabIndex = 1;
            this.buttonMainNew.Text = "Create New ETL Job";
            this.buttonMainNew.UseVisualStyleBackColor = true;
            this.buttonMainNew.Click += new System.EventHandler(this.buttonMainNew_Click);
            // 
            // buttonNewOpen
            // 
            this.buttonNewOpen.ForeColor = System.Drawing.Color.Blue;
            this.buttonNewOpen.Location = new System.Drawing.Point(54, 141);
            this.buttonNewOpen.Name = "buttonNewOpen";
            this.buttonNewOpen.Size = new System.Drawing.Size(160, 43);
            this.buttonNewOpen.TabIndex = 2;
            this.buttonNewOpen.Text = "View Previous Jobs";
            this.buttonNewOpen.UseVisualStyleBackColor = true;
            this.buttonNewOpen.Click += new System.EventHandler(this.buttonNewOpen_Click);
            // 
            // buttonNewExit
            // 
            this.buttonNewExit.ForeColor = System.Drawing.Color.Red;
            this.buttonNewExit.Location = new System.Drawing.Point(54, 204);
            this.buttonNewExit.Name = "buttonNewExit";
            this.buttonNewExit.Size = new System.Drawing.Size(160, 43);
            this.buttonNewExit.TabIndex = 3;
            this.buttonNewExit.Text = "EXIT";
            this.buttonNewExit.UseVisualStyleBackColor = true;
            this.buttonNewExit.Click += new System.EventHandler(this.buttonNewExit_Click);
            // 
            // vS2_DataSet
            // 
            this.vS2_DataSet.DataSetName = "VS2_DataSet";
            this.vS2_DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // orderdtlBindingSource
            // 
            this.orderdtlBindingSource.DataMember = "orderdtl";
            this.orderdtlBindingSource.DataSource = this.vS2_DataSet;
            // 
            // orderdtlTableAdapter
            // 
            this.orderdtlTableAdapter.ClearBeforeFill = true;
            // 
            // frmViperMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(265, 322);
            this.Controls.Add(this.buttonNewExit);
            this.Controls.Add(this.buttonNewOpen);
            this.Controls.Add(this.buttonMainNew);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmViperMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ETL Sticker Main";
            this.Load += new System.EventHandler(this.frmViperMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vS2_DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderdtlBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openOrderToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem orderToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem deleteOrderToolStripMenuItem;
        private VS2_DataSet vS2_DataSet;
        private System.Windows.Forms.BindingSource orderdtlBindingSource;
        private KCC.OA.Etl.VS2_DataSetTableAdapters.orderdtlTableAdapter orderdtlTableAdapter;
        private System.Windows.Forms.Button buttonMainNew;
        private System.Windows.Forms.Button buttonNewOpen;
        private System.Windows.Forms.Button buttonNewExit;
    }
}

