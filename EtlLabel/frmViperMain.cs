using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace KCC.OA.Etl
{
    public partial class frmViperMain : Form
    {
        public frmViperMain()
        {
            InitializeComponent();                            
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void newOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmViperNew frmVSNew = new frmViperNew();
           
            frmVSNew.ShowDialog();
        }

        private void deleteOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmDeleteOAU frmVSDel = new frmDeleteOAU();
            
            frmVSDel.ShowDialog();
        }

        private void openOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {            
            frmViperOpen frmVSOpen = new frmViperOpen();
            GlobalModeType.ModeType = "Update";
            populateOrdersListview(frmVSOpen);
            frmVSOpen.ShowDialog();
        }

        private void populateOrdersListview(frmViperOpen frmVSOpen)
        {
            ListViewItem lviOpen;

            // Create SqlDataAdapters for reading the VSOrderHed 
            // table using strongly typed datasets.
            VS_DataSetTableAdapters.VSOrderHedTableAdapter vsOrderHedTA =
                new VS_DataSetTableAdapters.VSOrderHedTableAdapter();

            vsOrderHedTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

            // Read the VSOrderHed table and retrieve all the order numbers and their respective
            // line numbers that have been previously processed and display them on the screen
            // so the user can select which order theyed like to look at in detail.
            /*
            VS_DataSet.VSOrderHedDataTable vsOrdHedDT = vsOrderHedTA.GetDataByOrderByOrderNum();

            foreach (DataRow dr in vsOrdHedDT)
            {
                string tempStr;

                //lviOpen = new ListViewItem(dr["OrderNum"].ToString());
                lviOpen = new ListViewItem(dr["JobNum"].ToString());
                tempStr = dr["OrderNum"].ToString();
                lviOpen.SubItems.Add(tempStr.Trim());
                tempStr = dr["OrderLine"].ToString();
                lviOpen.SubItems.Add(tempStr.Trim());
                tempStr = dr["ReleaseNum"].ToString();
                lviOpen.SubItems.Add(tempStr.Trim());                
                lviOpen.SubItems.Add(dr["UnitType"].ToString());
                lviOpen.SubItems.Add(dr["EndConsumerName"].ToString());
                frmVSOpen.listViewOOOrderNums.Items.Add(lviOpen);
            }

            if (vsOrdHedDT.Count > 0)
            {               
                frmVSOpen.listViewOOOrderNums.Items[0].Selected = true;
                frmVSOpen.listViewOOOrderNums.Select();                
            }   
            */
            
            VS_DataSetTableAdapters.EtlJobHeadTableAdapter etlJobHeadTA =
                new VS_DataSetTableAdapters.EtlJobHeadTableAdapter();

            etlJobHeadTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;
             
            //VS_DataSet.VSOrderHedDataTable vsOrdHedDT = vsOrderHedTA.GetDataByOrderByOrderNum();
            VS_DataSet.EtlJobHeadDataTable etlJobHead = etlJobHeadTA.GetDataByOrderByJobNum();

            foreach (DataRow dr in etlJobHead)
            {
                string tempStr;

                //lviOpen = new ListViewItem(dr["OrderNum"].ToString());
                lviOpen = new ListViewItem(dr["JobNum"].ToString());
                tempStr = dr["OrderNum"].ToString();
                lviOpen.SubItems.Add(tempStr.Trim());
                tempStr = dr["OrderLine"].ToString();
                lviOpen.SubItems.Add(tempStr.Trim());
                tempStr = dr["ReleaseNum"].ToString();
                lviOpen.SubItems.Add(tempStr.Trim());                
                lviOpen.SubItems.Add(dr["UnitType"].ToString());
                lviOpen.SubItems.Add(dr["EndConsumerName"].ToString());
                frmVSOpen.listViewOOOrderNums.Items.Add(lviOpen);
            }

            if (etlJobHead.Count > 0)
            {               
                frmVSOpen.listViewOOOrderNums.Items[0].Selected = true;
                frmVSOpen.listViewOOOrderNums.Select();                
            }
        }

        private void frmViperMain_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'vS2_DataSet.orderdtl' table. You can move, or remove it, as needed.
            //this.orderdtlTableAdapter.Fill(this.vS2_DataSet.orderdtl);

        }

        private void buttonNewOpen_Click(object sender, EventArgs e)
        {
            frmViperOpen frmVSOpen = new frmViperOpen();
            GlobalModeType.ModeType = "Update";
            populateOrdersListview(frmVSOpen);
            frmVSOpen.ShowDialog();
        }

        private void buttonMainNew_Click(object sender, EventArgs e)
        {
            frmViperNew frmVSNew = new frmViperNew();
            frmVSNew.ShowDialog();
        }

        private void buttonNewExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}