namespace KCC.OA.Etl
{
    partial class frmViperNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxJobInfo = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxJobNum = new System.Windows.Forms.TextBox();
            this.textBoxNewVSReleaseNum = new System.Windows.Forms.TextBox();
            this.labelNewVSSlash = new System.Windows.Forms.Label();
            this.textBoxNewVSLineNum = new System.Windows.Forms.TextBox();
            this.labelNewVSLine_Release = new System.Windows.Forms.Label();
            this.buttonNVCancel = new System.Windows.Forms.Button();
            this.buttonNVOK = new System.Windows.Forms.Button();
            this.comboBoxNVUnitType = new System.Windows.Forms.ComboBox();
            this.textBoxNVOrderNum = new System.Windows.Forms.TextBox();
            this.labelNVUnitType = new System.Windows.Forms.Label();
            this.labelVNOrderNum = new System.Windows.Forms.Label();
            this.groupBoxJobInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxJobInfo
            // 
            this.groupBoxJobInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxJobInfo.Controls.Add(this.label1);
            this.groupBoxJobInfo.Controls.Add(this.textBoxJobNum);
            this.groupBoxJobInfo.Controls.Add(this.textBoxNewVSReleaseNum);
            this.groupBoxJobInfo.Controls.Add(this.labelNewVSSlash);
            this.groupBoxJobInfo.Controls.Add(this.textBoxNewVSLineNum);
            this.groupBoxJobInfo.Controls.Add(this.labelNewVSLine_Release);
            this.groupBoxJobInfo.Controls.Add(this.buttonNVCancel);
            this.groupBoxJobInfo.Controls.Add(this.buttonNVOK);
            this.groupBoxJobInfo.Controls.Add(this.comboBoxNVUnitType);
            this.groupBoxJobInfo.Controls.Add(this.textBoxNVOrderNum);
            this.groupBoxJobInfo.Controls.Add(this.labelNVUnitType);
            this.groupBoxJobInfo.Controls.Add(this.labelVNOrderNum);
            this.groupBoxJobInfo.Location = new System.Drawing.Point(16, 15);
            this.groupBoxJobInfo.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxJobInfo.Name = "groupBoxJobInfo";
            this.groupBoxJobInfo.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxJobInfo.Size = new System.Drawing.Size(233, 238);
            this.groupBoxJobInfo.TabIndex = 0;
            this.groupBoxJobInfo.TabStop = false;
            this.groupBoxJobInfo.Text = "Job Info";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 20);
            this.label1.TabIndex = 12;
            this.label1.Text = "Job #:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxJobNum
            // 
            this.textBoxJobNum.BackColor = System.Drawing.Color.White;
            this.textBoxJobNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxJobNum.Location = new System.Drawing.Point(124, 19);
            this.textBoxJobNum.Name = "textBoxJobNum";
            this.textBoxJobNum.Size = new System.Drawing.Size(84, 23);
            this.textBoxJobNum.TabIndex = 0;
            this.textBoxJobNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxNewVSReleaseNum
            // 
            this.textBoxNewVSReleaseNum.BackColor = System.Drawing.Color.White;
            this.textBoxNewVSReleaseNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewVSReleaseNum.Location = new System.Drawing.Point(172, 128);
            this.textBoxNewVSReleaseNum.Name = "textBoxNewVSReleaseNum";
            this.textBoxNewVSReleaseNum.Size = new System.Drawing.Size(36, 23);
            this.textBoxNewVSReleaseNum.TabIndex = 3;
            this.textBoxNewVSReleaseNum.TabStop = false;
            this.textBoxNewVSReleaseNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxNewVSReleaseNum.Visible = false;
            // 
            // labelNewVSSlash
            // 
            this.labelNewVSSlash.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewVSSlash.Location = new System.Drawing.Point(166, 132);
            this.labelNewVSSlash.Name = "labelNewVSSlash";
            this.labelNewVSSlash.Size = new System.Drawing.Size(11, 20);
            this.labelNewVSSlash.TabIndex = 10;
            this.labelNewVSSlash.Text = "/";
            this.labelNewVSSlash.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelNewVSSlash.Visible = false;
            // 
            // textBoxNewVSLineNum
            // 
            this.textBoxNewVSLineNum.BackColor = System.Drawing.Color.White;
            this.textBoxNewVSLineNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNewVSLineNum.Location = new System.Drawing.Point(124, 128);
            this.textBoxNewVSLineNum.Name = "textBoxNewVSLineNum";
            this.textBoxNewVSLineNum.Size = new System.Drawing.Size(36, 23);
            this.textBoxNewVSLineNum.TabIndex = 2;
            this.textBoxNewVSLineNum.TabStop = false;
            this.textBoxNewVSLineNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxNewVSLineNum.Visible = false;
            // 
            // labelNewVSLine_Release
            // 
            this.labelNewVSLine_Release.Location = new System.Drawing.Point(6, 129);
            this.labelNewVSLine_Release.Name = "labelNewVSLine_Release";
            this.labelNewVSLine_Release.Size = new System.Drawing.Size(115, 20);
            this.labelNewVSLine_Release.TabIndex = 8;
            this.labelNewVSLine_Release.Text = "Line #/Release:";
            this.labelNewVSLine_Release.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelNewVSLine_Release.Visible = false;
            // 
            // buttonNVCancel
            // 
            this.buttonNVCancel.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNVCancel.Location = new System.Drawing.Point(133, 186);
            this.buttonNVCancel.Name = "buttonNVCancel";
            this.buttonNVCancel.Size = new System.Drawing.Size(75, 31);
            this.buttonNVCancel.TabIndex = 3;
            this.buttonNVCancel.Text = "Cancel";
            this.buttonNVCancel.UseVisualStyleBackColor = true;
            this.buttonNVCancel.Click += new System.EventHandler(this.buttonNVCancel_Click);
            // 
            // buttonNVOK
            // 
            this.buttonNVOK.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNVOK.Location = new System.Drawing.Point(30, 186);
            this.buttonNVOK.Name = "buttonNVOK";
            this.buttonNVOK.Size = new System.Drawing.Size(75, 31);
            this.buttonNVOK.TabIndex = 2;
            this.buttonNVOK.Text = "OK";
            this.buttonNVOK.UseVisualStyleBackColor = true;
            this.buttonNVOK.Click += new System.EventHandler(this.buttonNVOK_Click);
            // 
            // comboBoxNVUnitType
            // 
            this.comboBoxNVUnitType.BackColor = System.Drawing.Color.White;
            this.comboBoxNVUnitType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.comboBoxNVUnitType.FormattingEnabled = true;
            this.comboBoxNVUnitType.Items.AddRange(new object[] {
            "FAN",
            "MON",
            "MSP",
            "MUA",
            "OAU",
            "PCO",
            "RRU",
            "VKG"});
            this.comboBoxNVUnitType.Location = new System.Drawing.Point(124, 54);
            this.comboBoxNVUnitType.Name = "comboBoxNVUnitType";
            this.comboBoxNVUnitType.Size = new System.Drawing.Size(84, 24);
            this.comboBoxNVUnitType.TabIndex = 1;
            // 
            // textBoxNVOrderNum
            // 
            this.textBoxNVOrderNum.BackColor = System.Drawing.Color.White;
            this.textBoxNVOrderNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBoxNVOrderNum.Location = new System.Drawing.Point(124, 90);
            this.textBoxNVOrderNum.Name = "textBoxNVOrderNum";
            this.textBoxNVOrderNum.Size = new System.Drawing.Size(84, 23);
            this.textBoxNVOrderNum.TabIndex = 1;
            this.textBoxNVOrderNum.TabStop = false;
            this.textBoxNVOrderNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxNVOrderNum.Visible = false;
            // 
            // labelNVUnitType
            // 
            this.labelNVUnitType.Location = new System.Drawing.Point(6, 55);
            this.labelNVUnitType.Name = "labelNVUnitType";
            this.labelNVUnitType.Size = new System.Drawing.Size(115, 20);
            this.labelNVUnitType.TabIndex = 2;
            this.labelNVUnitType.Text = "Unit Type:";
            this.labelNVUnitType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelVNOrderNum
            // 
            this.labelVNOrderNum.Location = new System.Drawing.Point(6, 93);
            this.labelVNOrderNum.Name = "labelVNOrderNum";
            this.labelVNOrderNum.Size = new System.Drawing.Size(115, 20);
            this.labelVNOrderNum.TabIndex = 0;
            this.labelVNOrderNum.Text = "Sales Order #:";
            this.labelVNOrderNum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelVNOrderNum.Visible = false;
            // 
            // frmViperNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(266, 274);
            this.Controls.Add(this.groupBoxJobInfo);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmViperNew";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "New ETL Sticker";
            this.groupBoxJobInfo.ResumeLayout(false);
            this.groupBoxJobInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxJobInfo;
        private System.Windows.Forms.Label labelNVUnitType;
        private System.Windows.Forms.Button buttonNVCancel;
        private System.Windows.Forms.Button buttonNVOK;
        public System.Windows.Forms.ComboBox comboBoxNVUnitType;
        private System.Windows.Forms.TextBox textBoxJobNum;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox textBoxNewVSReleaseNum;
        private System.Windows.Forms.Label labelNewVSSlash;
        public System.Windows.Forms.TextBox textBoxNewVSLineNum;
        private System.Windows.Forms.Label labelNewVSLine_Release;
        private System.Windows.Forms.TextBox textBoxNVOrderNum;
        private System.Windows.Forms.Label labelVNOrderNum;
    }
}