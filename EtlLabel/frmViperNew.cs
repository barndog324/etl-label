using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace KCC.OA.Etl
{
    public partial class frmViperNew : Form
    {
        MainBO objMain = new MainBO();

        public frmViperNew()
        {
            InitializeComponent();
        }

        private void buttonNVCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonNVOK_Click(object sender, EventArgs e)
        {
            string jobNumStr = "";
            string orderNumStr = "";
            string customerStr = "";                       
            //string lineNumStr = "";
            //string releaseNumStr = "";
            string endConsumerNameStr = "";
            string modelNoStr = "";
            string partNumStr = "";
        

            int orderNumInt;
            int custNumInt;

            jobNumStr = this.textBoxJobNum.Text.Trim();
            //orderNumStr = this.textBoxNVOrderNum.Text.Trim();
            //lineNumStr = this.textBoxNewVSLineNum.Text.Trim();
            //releaseNumStr = this.textBoxNewVSReleaseNum.Text.Trim();
            /*
            if (this.textBoxNVOrderNum.Text.Trim() == "")
            {
                MessageBox.Show("Sales Order Number is Required!");
                this.textBoxNVOrderNum.Focus();
                return;
            }
            */
            /*
            if (this.textBoxNewVSLineNum.Text.Trim() == "")
            {
                MessageBox.Show("Line # is Required!");
                this.textBoxNewVSLineNum.Focus();
                return;
            }
            */
            /*
            if (this.textBoxNewVSReleaseNum.Text.Trim() == "")
            {
                MessageBox.Show("Release Number is Required!");
                this.textBoxNewVSReleaseNum.Focus();
                return;
            }
            */

            if (this.textBoxJobNum.Text.Trim() == "")
            {
                MessageBox.Show("Job Number is Required!");
                this.textBoxJobNum.Focus();
                return;
            }

            if (this.comboBoxNVUnitType.SelectedIndex < 0)
            {
                MessageBox.Show("Please select a Unit Type!");
                this.comboBoxNVUnitType.Focus();
                return;
            }

            

            try
            {

                //MessageBox.Show(GlobalKCCConnectionString.KCCConnectString);
                //return;

                // Create SqlDataAdapters for reading the VSOrderHed 
                // table using strongly typed datasets.
                //VS_DataSetTableAdapters.VSOrderHedTableAdapter vsOrderHedTA =
                //    new VS_DataSetTableAdapters.VSOrderHedTableAdapter();

                VS_DataSetTableAdapters.EtlJobHeadTableAdapter etlJobHeadTA =
                    new VS_DataSetTableAdapters.EtlJobHeadTableAdapter();

                //vsOrderHedTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                etlJobHeadTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;
                             
                // Read the VSOrderHed table to see if the Sales Order Number entered by the
                // user exist in the table. If it does already exist display a message box
                // indicating that, Otherwise display the Unit Info form for the user to 
                // input the data in.
                //VS_DataSet.VSOrderHedDataTable vsOrdHedDT = 
                //    vsOrderHedTA.GetDataByOrderNumOrderLineReleaseNum(orderNumStr, lineNumStr, releaseNumStr);

                VS_DataSet.EtlJobHeadDataTable etlJobHeadDT =
                    etlJobHeadTA.GetDataByJobNum(jobNumStr);
                /*
                if (vsOrdHedDT.Count > 0)
                {
                    MessageBox.Show("Sales Order #/Line #/Release combination already exist.");
                    this.textBoxNVOrderNum.Focus();
                }
                else
                {
                    endConsumerNameStr = "";
                    
                    try
                    {
                        orderNumInt = Int32.Parse(orderNumStr);

                        // Read the orderhed table to retrieve the customer number.
                        VS2_DataSetTableAdapters.orderhedTableAdapter orderHedTA =
                            new VS2_DataSetTableAdapters.orderhedTableAdapter();

                        orderHedTA.Connection.ConnectionString = GlobalEpicorConnectionString.EpicorConnectString;

                        VS2_DataSet.orderhedDataTable orderHedDT = orderHedTA.GetDataByOrderNum(orderNumInt);
                        

                        if (orderHedDT.Count > 0)
                        {
                            DataRow dr = orderHedDT.Rows[0];
                            customerStr = dr["custnum"].ToString();
                           
                            custNumInt = Int32.Parse(customerStr);
                            try
                            {
                                // Read the the customer table to retrieve the customer name.
                                VS2_DataSetTableAdapters.customerTableAdapter custTA =
                                    new VS2_DataSetTableAdapters.customerTableAdapter();

                                VS2_DataSet.customerDataTable custDT = custTA.GetDataByCustNum(custNumInt);

                                if (custDT.Count > 0)
                                {
                                    DataRow cr = custDT.Rows[0];
                                    customerStr = cr["name"].ToString();
                                }
                                else
                                {
                                    customerStr = "";
                                }
                            }
                            catch (Exception f)
                            {
                                MessageBox.Show("ERROR reading custumer table for customer number - " 
                                                + customerStr + " - " + f);
                                this.textBoxNVOrderNum.Focus();
                            }                            
                        }
                    }
                    catch (Exception f)
                    {
                        MessageBox.Show("ERROR reading VSOrderHed table for order number - " + 
                                         orderNumStr + " - " + f);
                        this.textBoxNVOrderNum.Focus();
                    }
                

                    GlobalModeType.ModeType = "New";

                    if (this.comboBoxNVUnitType.SelectedIndex == 0)  // OUA
                    {
                        processNewOAU(customerStr, endConsumerNameStr);
                    }                 
                    else if (this.comboBoxNVUnitType.SelectedIndex == 1)  // RRU
                    {
                        processNewRRU(customerStr, endConsumerNameStr);
                    }
                    else if (this.comboBoxNVUnitType.SelectedIndex == 2)  // MUA
                    {
                        processNewMUA(customerStr, endConsumerNameStr);
                    }
                    else if (this.comboBoxNVUnitType.SelectedIndex == 3)  // FAN
                    {
                        processNewFAN(customerStr, endConsumerNameStr);
                    }
                    else if (this.comboBoxNVUnitType.SelectedIndex == 4)  // PCO
                    {
                        processNewPCO(customerStr, endConsumerNameStr);
                    }
                    
                }*/

                if (etlJobHeadDT.Count > 0)
                {
                    MessageBox.Show("Job already exists.");
                    this.textBoxJobNum.Focus();
                }
                else
                {
                    endConsumerNameStr = "";

                    DataRow drJobProd = null;

                    try
                    {
                        //orderNumInt = Int32.Parse(orderNumStr);

                        VS2_DataSetTableAdapters.jobprodTableAdapter jobProdTA =
                            new VS2_DataSetTableAdapters.jobprodTableAdapter();

                        jobProdTA.Connection.ConnectionString = GlobalEpicorConnectionString.EpicorConnectString;

                        VS2_DataSet.jobprodDataTable jobProdDT = jobProdTA.GetDataByJobNum(jobNumStr);

                        if (jobProdDT.Count == 0)
                        {
                            return;
                        }

                        drJobProd = jobProdDT.Rows[0];                        
                       
                        if (drJobProd["ordernum"].ToString() == "0")
                        {
                            customerStr = "STOCK";
                        }
                        else
                        {
                            orderNumInt = Int32.Parse(drJobProd["ordernum"].ToString());

                            // Read the orderhed table to retrieve the customer number.
                            VS2_DataSetTableAdapters.orderhedTableAdapter orderHedTA =
                                new VS2_DataSetTableAdapters.orderhedTableAdapter();

                            orderHedTA.Connection.ConnectionString = GlobalEpicorConnectionString.EpicorConnectString;

                            VS2_DataSet.orderhedDataTable orderHedDT = orderHedTA.GetDataByOrderNum(orderNumInt);


                            if (orderHedDT.Count > 0)
                            {
                                DataRow dr = orderHedDT.Rows[0];
                                customerStr = dr["custnum"].ToString();

                                custNumInt = Int32.Parse(customerStr);
                                try
                                {
                                    // Read the the customer table to retrieve the customer name.
                                    VS2_DataSetTableAdapters.customerTableAdapter custTA =
                                        new VS2_DataSetTableAdapters.customerTableAdapter();

                                    custTA.Connection.ConnectionString = GlobalEpicorConnectionString.EpicorConnectString;

                                    VS2_DataSet.customerDataTable custDT = custTA.GetDataByCustNum(custNumInt);

                                    if (custDT.Count > 0)
                                    {
                                        DataRow cr = custDT.Rows[0];
                                        customerStr = cr["name"].ToString();
                                    }
                                    else
                                    {
                                        customerStr = "";
                                    }
                                }
                                catch (Exception f)
                                {
                                    MessageBox.Show("ERROR reading custumer table for customer number - "
                                                    + customerStr + " - " + f);
                                    this.textBoxJobNum.Focus();
                                }
                            }
                        }                        
                    }
                    catch (Exception f)
                    {
                        MessageBox.Show("ERROR reading VSOrderHed table for order number - " +
                                         orderNumStr + " - " + f);
                        this.textBoxJobNum.Focus();
                    }

                    GlobalModeType.ModeType = "New";

                    if (this.comboBoxNVUnitType.SelectedIndex == 0)  // FAN
                    {
                        processNewFAN(customerStr, endConsumerNameStr, drJobProd);                       
                    }
                    else if (this.comboBoxNVUnitType.SelectedIndex == 1)  // MON
                    {
                        processNewMON(customerStr, endConsumerNameStr, drJobProd);                       
                    }
                    else if (this.comboBoxNVUnitType.SelectedIndex == 2)  // MSP
                    {
                        processNewMSP(customerStr, endConsumerNameStr, drJobProd);
                    }
                    else if (this.comboBoxNVUnitType.SelectedIndex == 3)  // MUA
                    {
                        processNewMUA(customerStr, endConsumerNameStr, drJobProd);
                    }
                    else if (this.comboBoxNVUnitType.SelectedIndex == 4)  // OAU
                    {
                        processNewOAU(customerStr, endConsumerNameStr, drJobProd);                        
                    }
                    else if (this.comboBoxNVUnitType.SelectedIndex == 5)  // PCO
                    {
                        processNewPCO(customerStr, endConsumerNameStr, drJobProd);
                    }
                    else if (this.comboBoxNVUnitType.SelectedIndex == 6)  //RRU
                    {
                        processNewRRU(customerStr, endConsumerNameStr, drJobProd);   
                    }
                }

            }
            catch (Exception f)
            {
                MessageBox.Show("Error occurred when accessing EtlJobHead table. - " + f);
                return;
            }
        } // End of Function

        private void processNewMUA(string customerStr, string endConsumerNameStr, DataRow drJobProd)
        {
            string jobNumStr = "";
            string orderNumStr = "";
            string partNumStr = "";
            string lineNumStr = "";
            string releaseNumStr = "";
            string orderQtyStr = "";

            //int orderNumInt;
            //int orderLineInt;

            jobNumStr = drJobProd["jobnum"].ToString();
            orderNumStr = drJobProd["ordernum"].ToString();
            lineNumStr = drJobProd["orderline"].ToString();
            releaseNumStr = drJobProd["orderrelnum"].ToString();
            partNumStr = drJobProd["partnum"].ToString();
            orderQtyStr = drJobProd["prodqty"].ToString();

            frmNewLineMUA frmNewMUA = new frmNewLineMUA();
            GlobalModeType.ModeType = "New";

            frmNewMUA.Text = "New MUA Job # " + jobNumStr;
            frmNewMUA.textBoxNewMUAOrderNum.Text = orderNumStr;            

            frmNewMUA.textBoxNewMUACustomer.Text = customerStr;
            frmNewMUA.textBoxNewMUACustomer.Enabled = false;

            frmNewMUA.textBoxNewMUAEndConsumer.Text = endConsumerNameStr;
            frmNewMUA.textBoxNewMUAEndConsumer.Focus();
            frmNewMUA.textBoxNewMUAEndConsumer.Select();
            
            frmNewMUA.textBoxNewMUALineItem.Text = lineNumStr;
            frmNewMUA.textBoxNewMUAReleaseNum.Text = releaseNumStr;

            frmNewMUA.labelNewMUAMsg.Text = "New MUA unit, please input the correct data and then press the";
            frmNewMUA.labelNewMUAMsg2.Text = "Accept button to save the information in the EtlMUA table.";
            frmNewMUA.ShowDialog();
        }

        private void processNewRRU(string customerStr, string endConsumerNameStr, DataRow drJobProd)
        {
            string jobNumStr = drJobProd["jobnum"].ToString();
            string orderNumStr = drJobProd["ordernum"].ToString();
            string partNumStr = drJobProd["partnum"].ToString();
            string lineNumStr = drJobProd["orderline"].ToString();
            string releaseNumStr = drJobProd["orderrelnum"].ToString();
            string htgTypeStr = "";

            //int orderNumInt;
            //int orderLineInt;
            int modelNoLocInt = -1;
            int i = 0;

            //orderNumStr = this.textBoxNVOrderNum.Text.Trim();
            //lineNumStr = this.textBoxNewVSLineNum.Text.Trim();
            //releaseNumStr = this.textBoxNewVSReleaseNum.Text.Trim();

            frmNewLineRRU frmNewRRU = new frmNewLineRRU();
            frmNewRRU.Size = new System.Drawing.Size(860, 710);
            frmNewRRU.groupBoxNewRRU.Size = new System.Drawing.Size(822, 633);
            GlobalModeType.ModeType = "New";

            frmNewRRU.Text = "New RRU Job # " + jobNumStr;
            frmNewRRU.textBoxNewRRUJobNum.Text = jobNumStr;
            frmNewRRU.textBoxNewRRUOrderNum.Text = orderNumStr;
            frmNewRRU.textBoxNewRRUPartNum.Text = partNumStr;

            frmNewRRU.textBoxNewRRUCustomer.Text = customerStr;
            frmNewRRU.textBoxNewRRUCustomer.Enabled = false;

            frmNewRRU.textBoxNewRRUEndConsumer.Text = endConsumerNameStr;
            frmNewRRU.textBoxNewRRUEndConsumer.Focus();
            frmNewRRU.textBoxNewRRUEndConsumer.Select();
            
            frmNewRRU.textBoxNewRRULineItem.Text = lineNumStr;
            frmNewRRU.textBoxNewRRUReleaseNum.Text = releaseNumStr;

            

            try
            {
                // Create SqlDataAdapters for reading the RRUTemplates table.
                //VS_DataSetTableAdapters.RRUTemplatesTableAdapter rruTA =
                //    new VS_DataSetTableAdapters.RRUTemplatesTableAdapter();

                VS_DataSetTableAdapters.EtlRRUTemplatesTableAdapter rruTA =
                    new VS_DataSetTableAdapters.EtlRRUTemplatesTableAdapter();

                rruTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                // Read the RRUTemplates table and retrieve all of the different ModelNo templates.

                //VS_DataSet.RRUTemplatesDataTable rruDT = rruTA.GetData();
                VS_DataSet.EtlRRUTemplatesDataTable rruDT = rruTA.GetData();

                foreach (DataRow dr in rruDT)
                {
                    frmNewRRU.comboBoxNewRRUModelNo.Items.Add(dr["ModelNo"].ToString());
                    if (partNumStr.Equals(dr["ModelNo"].ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                        modelNoLocInt = i;
                    }
                    ++i;
                }

                if (modelNoLocInt >= 0)
                {
                    frmNewRRU.comboBoxNewRRUModelNo.SelectedIndex = modelNoLocInt;
                }

                try
                {
                    VS_DataSet.EtlRRUTemplatesDataTable rru2DT = rruTA.GetDataByModelNo(partNumStr);
                    if (rru2DT.Count > 0)
                    {
                        DataRow dr2 = rru2DT.Rows[0];
                        htgTypeStr = dr2["HeatingType"].ToString();

                        if (htgTypeStr.Equals("Direct", StringComparison.OrdinalIgnoreCase))
                        {
                            frmNewRRU.groupBoxNewRRUDirectFire.Location = new Point(6, 275);
                            frmNewRRU.groupBoxNewRRUDirectFire.Visible = true;
                            frmNewRRU.groupBoxNewRRUIndirectFire.Visible = false;
                            frmNewRRU.comboBoxNewRRUHeatingType.SelectedIndex = 0;
                            frmNewRRU.textBoxNewRRUDFEquiptedAirflow.Text = dr2["DF_EquiptedAirFlow"].ToString();
                            frmNewRRU.textBoxNewRRUDFStaticPressure.Text = dr2["DF_StaticPressure"].ToString();
                            frmNewRRU.textBoxNewRRUDFMaxHtgInputBTUH.Text = dr2["DF_MaxHtgInputBTUH"].ToString();
                            frmNewRRU.textboxNewRRUDFMinHtgInput.Text = dr2["DF_MinHtgInput"].ToString();
                            frmNewRRU.textBoxNewRRUDFTempRise.Text = dr2["DF_TempRise"].ToString();
                            frmNewRRU.textBoxNewRRUDFMaxGasPressure.Text = dr2["DF_MaxGasPressure"].ToString();
                            frmNewRRU.textBoxNewRRUDFMinGasPressure.Text = dr2["DF_MinGasPressure"].ToString();
                            frmNewRRU.textBoxNewRRUDFMaxPressureDrop.Text = dr2["DF_MaxPressureDrop"].ToString();
                            frmNewRRU.textBoxNewRRUDFMinPressureDrop.Text = dr2["DF_MinPressureDrop"].ToString();
                            frmNewRRU.textBoxNewRRUDFManifoldPressure.Text = dr2["DF_ManifoldPressure"].ToString();
                            frmNewRRU.textBoxNewRRUDFCutOutTemp.Text = dr2["DF_CutOutTemp"].ToString();

                        }
                        else if (htgTypeStr.Equals("Indirect", StringComparison.OrdinalIgnoreCase))
                        {
                            frmNewRRU.groupBoxNewRRUIndirectFire.Location = new Point(6, 275);
                            frmNewRRU.groupBoxNewRRUIndirectFire.Visible = true;
                            frmNewRRU.groupBoxNewRRUDirectFire.Visible = false;
                            frmNewRRU.comboBoxNewRRUHeatingType.SelectedIndex = 1;
                            frmNewRRU.textBoxNewRRUIFHeatingInputBTUH.Text = dr2["IN_HtgInputBTUH"].ToString();
                            frmNewRRU.textBoxNewRRUIFHeatingOutput.Text = dr2["IN_HtgOutputBTUH"].ToString();
                            frmNewRRU.textBoxNewRRUIFMinInputBTU.Text = dr2["IN_MinInputBTU"].ToString();
                            frmNewRRU.textBoxNewRRUIFMaxExt.Text = dr2["IN_MaxExt"].ToString();
                            frmNewRRU.textBoxNewRRUIFTempRise.Text = dr2["IN_TempRise"].ToString();
                            frmNewRRU.textBoxNewRRUIFMaxOutAirTemp.Text = dr2["MaxOutletAirTemp"].ToString();
                            frmNewRRU.textBoxNewRRUIFMaxGasPressure.Text = dr2["IN_MaxGasPressure"].ToString();
                            frmNewRRU.textBoxNewRRUIFMinGasPressure.Text = dr2["IN_MinGaspressure"].ToString();
                            frmNewRRU.textBoxNewRRUIFManifoldPressure.Text = dr2["IN_ManifoldPressure"].ToString();
                            frmNewRRU.textBoxNewRRUIFMaxHtgInputBTUH.Text = dr2["IN_MaxHtgInputBTUH"].ToString();
                        }
                        frmNewRRU.textBoxNewRRUFanCondQty.Text = dr2["FanCondQty"].ToString();
                        frmNewRRU.textBoxNewRRUFanCondPH.Text = dr2["FanCondPH"].ToString();
                        frmNewRRU.textBoxNewRRUFanCondFLA.Text = dr2["FanCondFLA"].ToString();
                        frmNewRRU.textBoxNewRRUFanCondHP.Text = dr2["FanCondHP"].ToString();

                        frmNewRRU.textBoxNewRRUFanStdQty.Text = dr2["FanStdQty"].ToString();
                        frmNewRRU.textBoxNewRRUFanStdPH.Text = dr2["FanStdPH"].ToString();
                        frmNewRRU.textBoxNewRRUFanStdFLA.Text = dr2["FanStdFLA"].ToString();
                        frmNewRRU.textBoxNewRRUFanStdHP.Text = dr2["FanStdHP"].ToString();

                        frmNewRRU.textBoxNewRRUFanOvrQty.Text = dr2["FanOvrQty"].ToString();
                        frmNewRRU.textBoxNewRRUFanOvrPH.Text = dr2["FanOvrPH"].ToString();
                        frmNewRRU.textBoxNewRRUFanOvrFLA.Text = dr2["FanOvrFLA"].ToString();
                        frmNewRRU.textBoxNewRRUFanOvrHP.Text = dr2["FanOvrHP"].ToString();

                        frmNewRRU.textBoxNewRRUComp1Qty.Text = dr2["Compr1Qty"].ToString();
                        frmNewRRU.textboxNewRRUComp1PH.Text = dr2["Compr1PH"].ToString();
                        frmNewRRU.textBoxNewRRUComp1RLAVolts.Text = dr2["Compr1RLA_Volts"].ToString();
                        frmNewRRU.textBoxNewRRUComp1LRA.Text = dr2["Compr1LRA"].ToString();
                        frmNewRRU.textBoxNewRRUComp1Charge.Text = dr2["Compr1Charge"].ToString();

                        frmNewRRU.textBoxNewRRUComp2Qty.Text = dr2["Compr2Qty"].ToString();
                        frmNewRRU.textBoxNewRRUComp2PH.Text = dr2["Compr2PH"].ToString();
                        frmNewRRU.textBoxNewRRUComp2RLAVolts.Text = dr2["Compr2RLA_Volts"].ToString();
                        frmNewRRU.textBoxNewRRUComp2LRA.Text = dr2["Compr2LRA"].ToString();
                        frmNewRRU.textBoxNewRRUComp2Charge.Text = dr2["Compr2Charge"].ToString();

                        frmNewRRU.textBoxNewRRUComp3Qty.Text = dr2["Compr3Qty"].ToString() == "NA" ? "" : dr2["Compr3Qty"].ToString();
                        frmNewRRU.textBoxNewRRUComp3PH.Text = dr2["Compr3PH"].ToString() == "NA" ? "" : dr2["Compr3PH"].ToString();
                        frmNewRRU.textBoxNewRRUComp3RLA.Text = dr2["Compr3RLA_Volts"].ToString() == "NA" ? "" : dr2["Compr3RLA_Volts"].ToString();
                        frmNewRRU.textBoxNewRRUComp3LRA.Text = dr2["Compr3LRA"].ToString() == "NA" ? "" : dr2["Compr3LRA"].ToString();
                        frmNewRRU.textBoxNewRRUComp3Charge.Text = dr2["Compr3Charge"].ToString() == "NA" ? "" : dr2["Compr3Charge"].ToString();                      

                        frmNewRRU.textBoxNewRRUMinCKTAmp.Text = dr2["MinCKTAmp"].ToString();
                        frmNewRRU.textBoxNewRRUMFSMCB.Text = dr2["MFSMCB"].ToString();
                        frmNewRRU.textBoxNewRRUMOP.Text = dr2["MOP"].ToString();
                        frmNewRRU.textBoxNewRRUElectricalRating.Text = dr2["ElecRating"].ToString();
                        frmNewRRU.textBoxNewRRUOperatingVolts.Text = dr2["OperVoltage"].ToString();
                        frmNewRRU.textBoxNewRRUHeatingType.Text = dr2["HeatingType"].ToString();
                        frmNewRRU.textBoxNewRRUHeatingInput.Text = dr2["HeatingInput"].ToString();
                        frmNewRRU.textBoxNewRRUTestPressureHigh.Text = dr2["TestPressureHigh"].ToString();
                        frmNewRRU.textBoxNewRRUTestPressureLow.Text = dr2["TestPressureLow"].ToString();

                        frmNewRRU.radioButtonNewRRUR410A.Checked = true;

                    }
                    else 
                    {
                        MessageBox.Show("WARNING EtlRRUTemplate for ModelNo - " + partNumStr + " Not found " +
                            "therefore you'll have to choose a model type from the Model Type drop down box!");
                    }
                }
                catch
                {
                    MessageBox.Show("ERROR searching EtlRRUTemplates for ModelNo - " + partNumStr);   
                }
            }
            catch
            {
                MessageBox.Show("ERROR reading the EtlRRUTemplates table.");               
            }
           

            frmNewRRU.labelNewRRUMsg.Text = "New RRU, please input the correct data and then press the" +
                                    "Accept button to save the information in the EtlRRU table.";
            frmNewRRU.ShowDialog();
        }

        private void processNewFAN(string customerStr, string endConsumerNameStr, DataRow drJobProd)
        {
            
            string jobNumStr = drJobProd["jobnum"].ToString();
            string orderNumStr = drJobProd["ordernum"].ToString();
            string lineNumStr = drJobProd["orderline"].ToString();
            string releaseNumStr = drJobProd["orderrelnum"].ToString();
            string partNumStr = drJobProd["partnum"].ToString();

            frmNewLineFAN frmNewFAN = new frmNewLineFAN();
            GlobalModeType.ModeType = "New";

            frmNewFAN.Text = "New FAN Job # " + jobNumStr;
            frmNewFAN.textBoxNewFANJobNum.Text = jobNumStr;
            frmNewFAN.textBoxNewFANOrderNum.Text = orderNumStr;

            frmNewFAN.textBoxNewFANCustomer.Text = customerStr;
            frmNewFAN.textBoxNewFANCustomer.Enabled = false;

            frmNewFAN.textBoxNewFANEndConsumer.Text = endConsumerNameStr;
            frmNewFAN.textBoxNewFANEndConsumer.Focus();
            frmNewFAN.textBoxNewFANEndConsumer.Select();

            frmNewFAN.textBoxNewFANLineItem.Text = lineNumStr;
            frmNewFAN.textBoxNewFANReleaseNum.Text = releaseNumStr;

            frmNewFAN.textBoxNewFANPartNum.Text = partNumStr;
            
            frmNewFAN.labelNewFANMsg.Text = "New Fan, please input the correct data and then press the";
            frmNewFAN.labelNewFANMsg2.Text = "Accept button to save the information in the EtlFAN table.";
            frmNewFAN.ShowDialog();


        }

        private void processNewMON(string customerStr, string endConsumerNameStr, DataRow drJobProd)
        {
            string jobNumStr = "";
            string orderNumStr = "";
            string partNumStr = "";
            string lineNumStr = "";
            string releaseNumStr = "";
            string poNumStr = "";
            string commentStr = "";
            string dateOfMfgStr = "";

            int orderNumInt;
            int orderLineInt;

            DateTime DateOfMfg;

            jobNumStr = drJobProd["jobnum"].ToString();
            orderNumStr = drJobProd["ordernum"].ToString();
            lineNumStr = drJobProd["orderline"].ToString();
            releaseNumStr = drJobProd["orderrelnum"].ToString();

            frmNewLineMON frmNewMON = new frmNewLineMON();
            GlobalModeType.ModeType = "New";

            frmNewMON.Text = "New PCO Job # " + jobNumStr;
            frmNewMON.txtOrderNum.Text = orderNumStr;
            frmNewMON.txtLineItem.Text = lineNumStr;
            frmNewMON.txtReleaseNum.Text = releaseNumStr;

            frmNewMON.txtCustomer.Text = customerStr;
            frmNewMON.txtCustomer.Enabled = false;

            frmNewMON.txtEndConsumer.Text = endConsumerNameStr;
            frmNewMON.txtEndConsumer.Focus();
            frmNewMON.txtEndConsumer.Select();

            frmNewMON.txtJobNum.Text = jobNumStr;

            frmNewMON.lbNewMONMsg.Text = "New MON, add, verify & save data (Press Accept button to save).";
            frmNewMON.btnAccept.Text = "Accept";

            frmNewMON.ShowDialog();
        }

        private void processNewMSP(string customerStr, string endConsumerNameStr, DataRow drJobProd)
        {
            string jobNumStr = "";
            string orderNumStr = "";            
            string lineNumStr = "";
            string releaseNumStr = "";
            string modelNoStr = "";
            string custNameStr = "";
            string DHV_Str = "D,H'V";

            jobNumStr = drJobProd["jobnum"].ToString();
            orderNumStr = drJobProd["ordernum"].ToString();
            lineNumStr = drJobProd["orderline"].ToString();
            releaseNumStr = drJobProd["orderrelnum"].ToString();

            objMain.JobNum = jobNumStr;
            DataTable dtJobHead = objMain.GetJobHeadData();

            if (dtJobHead.Rows.Count > 0)
            {
                DataRow drJobHead = dtJobHead.Rows[0];
                modelNoStr = drJobHead["ModelNo"].ToString();
                if (endConsumerNameStr.Length == 0)
                {
                    endConsumerNameStr = drJobHead["CustomerName"].ToString();
                }
            }

            frmNewLineMSP frmNewMSP = new frmNewLineMSP();
            GlobalModeType.ModeType = "New";

            frmNewMSP.Text = "New MSP Job # " + jobNumStr;
            frmNewMSP.tbNewMSPOrderNum.Text = drJobProd["ordernum"].ToString();
            frmNewMSP.tbNewMSPLineItem.Text = drJobProd["orderline"].ToString();
            frmNewMSP.tbNewMSPReleaseNum.Text = drJobProd["orderrelnum"].ToString();
            frmNewMSP.tbNewMSPPartNum.Text = drJobProd["PartNum"].ToString();
            frmNewMSP.tbNewMSPModelNo.Text = modelNoStr;
          
            frmNewMSP.tbNewMSPCustomer.Text = customerStr;
            frmNewMSP.tbNewMSPCustomer.Enabled = false;

            frmNewMSP.tbNewMSPEndConsumer.Text = endConsumerNameStr;
            frmNewMSP.tbNewMSPEndConsumer.Focus();
            frmNewMSP.tbNewMSPEndConsumer.Select();
            frmNewMSP.tbNewMSP_MfgDate.Text = DateTime.Today.ToShortDateString();

            frmNewMSP.tbNewMSPJobNum.Text = jobNumStr;

            if (modelNoStr.Length > 39)
            {
                if (DHV_Str.Contains(modelNoStr.Substring(1, 1)) == true)  // Digit 2 equals 'D', 'H', or' 'V'
                {
                    // Digit 11 equals A or B then Unit Type = DV CW
                    frmNewMSP.gbNewMSPPostCooling.Enabled = true;
                    frmNewMSP.gbNewMSPPostHeating.Enabled = true;
                    frmNewMSP.gbNewMSPDehumidifier.Text = "Dehumidifier Rated Conditions";
                    frmNewMSP.lbNewFAN_VoltDraw.Text = "Draw:";
                    frmNewMSP.tbNewMSP_UnitType.Text = "DVCW";

                    if (modelNoStr.Substring(10, 1) == "C" || modelNoStr.Substring(10, 1) == "D")   // Digit 11 equals C or D then Unit Type = DV DX
                    {
                        frmNewMSP.tbNewMSPDehumidAmbient.Enabled = false;
                        frmNewMSP.tbNewMSPDehumidBTUH.Enabled = false;
                        frmNewMSP.tbNewMSPDehumidEWT.Enabled = false;
                        frmNewMSP.tbNewMSPDehumidGlycol.Enabled = false;
                        frmNewMSP.tbNewMSPDehumidLWT.Enabled = false;
                        frmNewMSP.tbNewMSPDehumidPD.Enabled = false;
                        frmNewMSP.lbDehumidGPM_SST.Text = "SST";
                        frmNewMSP.lbPostCoolingGPM_SST.Text = "SST";
                        frmNewMSP.tbNewMSP_UnitType.Text = "DVDX";
                    }
                }
                else // Digit2 equals 'U'
                {
                   
                    frmNewMSP.tbNewMSPDehumidGlycol.Enabled = false;
                    frmNewMSP.tbNewMSPDehumidLWT.Enabled = false;
                    frmNewMSP.tbNewMSPDehumidPD.Enabled = false;
                    frmNewMSP.tbNewMSPDehumidEWT.Enabled = false;
                    frmNewMSP.lbNewFAN_VoltDraw.Text = "Voltage:";

                    if (modelNoStr.Substring(10, 1) == "C" || modelNoStr.Substring(10, 1) == "D")  // Digit 11 equals C or D then Unit Type = DU DX
                    {
                        frmNewMSP.tbNewMSPDehumidCapacity.Enabled = false;                                                                      
                        frmNewMSP.lbDehumidGPM_SST.Text = "SST";
                        frmNewMSP.gbNewMSPDehumidifier.Text = "Summer Rated Conditions";
                        frmNewMSP.tbNewMSP_UnitType.Text = "DUDX";
                    }
                    else                                                                           // Digit 11 equals A or B then Unit Type = DU CW
                    {
                        frmNewMSP.gbNewMSPDehumidifier.Text = "Dehumidifier Rated Conditions";
                        frmNewMSP.lbDehumidGPM_SST.Text = "SST";
                        frmNewMSP.tbNewMSP_UnitType.Text = "DUCW";
                    }
                }
            }
            else
            {
                //frmNewMSP.gbNewMSPPostCooling.Enabled = false; //03/04/2020 - Temporarily commented out to allow Engineering to input values into the MSP Label.
                //frmNewMSP.gbNewMSPPostHeating.Enabled = false; //03/04/2020 - Temporarily commented out to allow Engineering to input values into the MSP Label.
            }

            frmNewMSP.lbNewMSPMsg.Text = "New MSP, add, verify & save data (Press Accept button to save).";
            frmNewMSP.btnNewMSPAccept.Text = "Accept";

            frmNewMSP.ShowDialog();
        }

        private void processNewPCO(string customerStr, string endConsumerNameStr, DataRow drJobProd)
        {
            string jobNumStr = "";
            string orderNumStr = "";
            string partNumStr = "";
            string lineNumStr = "";
            string releaseNumStr = "";
            string poNumStr = "";
            string commentStr = "";
            string dateOfMfgStr = "";

            int orderNumInt;
            int orderLineInt;

            DateTime DateOfMfg;

            jobNumStr = drJobProd["jobnum"].ToString();
            orderNumStr = drJobProd["ordernum"].ToString();
            lineNumStr = drJobProd["orderline"].ToString();
            releaseNumStr = drJobProd["orderrelnum"].ToString();

            frmNewLinePCO frmNewPCO = new frmNewLinePCO();
            GlobalModeType.ModeType = "New";

            frmNewPCO.Text = "New PCO Job # " + jobNumStr;
            frmNewPCO.textBoxNewPCOOrderNum.Text = orderNumStr;           

            frmNewPCO.textBoxNewPCOCustomer.Text = customerStr;
            frmNewPCO.textBoxNewPCOCustomer.Enabled = false;

            frmNewPCO.textBoxNewPCOEndConsumer.Text = endConsumerNameStr;
            frmNewPCO.textBoxNewPCOEndConsumer.Focus();
            frmNewPCO.textBoxNewPCOEndConsumer.Select();

            frmNewPCO.textBoxNewPCOLineItem.Text = lineNumStr;
            frmNewPCO.textBoxNewPCOReleaseNum.Text = releaseNumStr;
            frmNewPCO.textBoxNewPCOJobNo.Text = jobNumStr;

            try
            {
                // Create SqlDataAdapters for reading the OrderDtl
                // table using strongly typed datasets.
                VS2_DataSetTableAdapters.orderdtlTableAdapter ordDtlTA =
                    new VS2_DataSetTableAdapters.orderdtlTableAdapter();

                ordDtlTA.Connection.ConnectionString = GlobalEpicorConnectionString.EpicorConnectString;

                VS2_DataSetTableAdapters.orderhedTableAdapter ordHedTA = 
                    new KCC.OA.Etl.VS2_DataSetTableAdapters.orderhedTableAdapter();

                ordHedTA.Connection.ConnectionString = GlobalEpicorConnectionString.EpicorConnectString;

                VS2_DataSetTableAdapters.jobheadTableAdapter jobHeadTA = 
                    new KCC.OA.Etl.VS2_DataSetTableAdapters.jobheadTableAdapter();

                jobHeadTA.Connection.ConnectionString = GlobalEpicorConnectionString.EpicorConnectString;


                // Read the orderDtl table to see if the Sales Order Number entered by the
                // user exist in the table. If it does then retrieve the data associated  
                // with that sales order number.
                orderNumInt = Int32.Parse(orderNumStr);
                orderLineInt = Int32.Parse(lineNumStr);
                VS2_DataSet.orderdtlDataTable ordDtlDT =
                    ordDtlTA.GetDataByOrderNumOrderLine(orderNumInt, orderLineInt);

                if (ordDtlDT.Count > 0)
                {
                    DataRow ordDtlRow = ordDtlDT.Rows[0];

                    partNumStr = ordDtlRow["partnum"].ToString();
                    frmNewPCO.textBoxNewPCOModelNo.Text = partNumStr;

                    commentStr = ordDtlRow["ordercomment"].ToString();
                    frmNewPCO.textBoxNewPCOTag.Text = commentStr;
                }

                VS2_DataSet.orderhedDataTable ordHedDT = ordHedTA.GetDataByOrderNum(orderNumInt);
                if (ordHedDT.Count > 0)
                {
                    DataRow ordHedRow = ordHedDT.Rows[0];

                    poNumStr = ordHedRow["ponum"].ToString();
                    frmNewPCO.textBoxNewPCOPONum.Text = poNumStr;
                }

                jobNumStr = orderNumStr + "-" + lineNumStr + "-" + releaseNumStr;
                VS2_DataSet.jobheadDataTable jobHeadDT = jobHeadTA.GetDataByJobNo(jobNumStr);
                
                if (jobHeadDT.Count > 0)
                {
                    DataRow jobHeadRow = jobHeadDT.Rows[0];

                    dateOfMfgStr = jobHeadRow["jobcompletiondate"].ToString();
                    if (dateOfMfgStr != "")
                    {
                        DateOfMfg = Convert.ToDateTime(dateOfMfgStr);
                        dateOfMfgStr = DateOfMfg.ToShortDateString();
                    }

                    frmNewPCO.textBoxNewPCODateOfMfg.Text = dateOfMfgStr;
                }

            }
            catch
            {
                MessageBox.Show("Error occurred when trying to read the orderdtl table.");
                return;
            }
            frmNewPCO.labelNewPCOMsg.Text = "New PCO, verify & save data (Press Accept button to save).";
            
            frmNewPCO.ShowDialog();
        }

        private void processNewOAU(string customerStr, string endConsumerNameStr, DataRow drJobProd)
        {
            string jobNumStr = drJobProd["jobnum"].ToString();
            string orderNumStr = drJobProd["ordernum"].ToString();
            string modelNumStr = "";
            string modelNum2Str = "";
            string orderQtyStr = "";
            string lineNumStr = drJobProd["orderline"].ToString();
            string releaseNumStr = drJobProd["orderrelnum"].ToString();

            int orderNumInt;
            int orderLineInt;

            Boolean modelNumFoundBool = false;
            Boolean oldModelNumBool = false;           

            //orderNumStr = this.textBoxNVOrderNum.Text.Trim();
            //lineNumStr = this.textBoxNewVSLineNum.Text.Trim();
            //releaseNumStr = this.textBoxNewVSReleaseNum.Text.Trim();
            //jobNumStr = this.textBoxJobNum.Text.Trim();

            frmNewLineOAU frmNewOAU = new frmNewLineOAU();
            GlobalModeType.ModeType = "New";
            frmNewOAU.Text = "New OAU Job # " + jobNumStr;
            frmNewOAU.textBoxNewOAUOrderNum.Text = orderNumStr;
           
            frmNewOAU.textBoxNewOAUCustomer.Text = customerStr;
            frmNewOAU.textBoxNewOAUCustomer.Enabled = false;

            //frmNewOAU.cboSCCR.SelectedIndex = 0;
            //frmNewOAU.cboFanEvapHPKW.SelectedIndex = 0;
            //frmNewOAU.cboFanPwrHPKW.SelectedIndex = 0;

            frmNewOAU.textBoxNewOAUEndConsumer.Text = endConsumerNameStr;
            frmNewOAU.textBoxNewOAUEndConsumer.Focus();
            frmNewOAU.textBoxNewOAUEndConsumer.Select();
         
            frmNewOAU.textBoxNewOAULineItem.Text = lineNumStr;
            frmNewOAU.textBoxNewOAUReleaseNum.Text = releaseNumStr;

            frmNewOAU.textBoxNewOAUJobNum.Text = jobNumStr;

            try
            {
                // Create SqlDataAdapters for reading the OrderDtl
                // table using strongly typed datasets.
                //VS2_DataSetTableAdapters.orderdtlTableAdapter ordDtlTA =
                //    new VS2_DataSetTableAdapters.orderdtlTableAdapter();

                VS2_DataSetTableAdapters.jobheadTableAdapter jobHeadTA =
                    new VS2_DataSetTableAdapters.jobheadTableAdapter();

                //ordDtlTA.Connection.ConnectionString = GlobalEpicorConnectionString.EpicorConnectString;
                jobHeadTA.Connection.ConnectionString = GlobalEpicorConnectionString.EpicorConnectString;

                // Read the orderDtl table to see if the Sales Order Number entered by the
                // user exist in the table. If it does then retrieve the data associated  
                // with that sales order number.
                orderNumInt = Int32.Parse(orderNumStr);
                orderLineInt = Int32.Parse(lineNumStr);
                //VS2_DataSet.orderdtlDataTable ordDtlDT = 
                //    ordDtlTA.GetDataByOrderNumOrderLine(orderNumInt, orderLineInt);

                VS2_DataSet.jobheadDataTable jobHeadDT =
                    jobHeadTA.GetDataByJobNo(jobNumStr);
                

                if (jobHeadDT.Count > 0)
                {
                    //DataRow ordDtlRow = ordDtlDT.Rows[0];

                    DataRow jobHeadRow = jobHeadDT.Rows[0];
                                      
                    //modelNumStr = ordDtlRow["partnum"].ToString();
                    //modelNum2Str = ordDtlRow["linedesc"].ToString();

                    modelNumStr = jobHeadRow["partnum"].ToString();
                    modelNum2Str = jobHeadRow["partdescription"].ToString();
                    
                    if ((modelNumStr.Substring(0, 2) == "OA") && (modelNumStr.Length == 29))
                    {                                        
                        frmNewOAU.textBoxNewOAUModelNo.Text = modelNumStr;
                        modelNumFoundBool = true;
                        oldModelNumBool = true;
                    }
                    else if ((modelNum2Str.Substring(0, 2) == "OA") && (modelNum2Str.Length == 29))
                    {
                        frmNewOAU.textBoxNewOAUModelNo.Text = modelNum2Str;
                        modelNumStr = modelNum2Str;
                        modelNumFoundBool = true;
                        oldModelNumBool = true;
                    }
                    else if ((modelNumStr.Substring(0, 2) == "OA") && (modelNumStr.Length == 39))
                    {
                        frmNewOAU.textBoxNewOAUModelNo.Text = modelNumStr;
                        modelNumFoundBool = true;                       
                    }
                    else if ((modelNum2Str.Substring(0, 2) == "OA") && (modelNum2Str.Length == 39))
                    {
                        frmNewOAU.textBoxNewOAUModelNo.Text = modelNum2Str;
                        modelNumStr = modelNum2Str;
                        modelNumFoundBool = true;                        
                    }
 
                    //frmNewOAU.textBoxNewOAULineItem.Text = ordDtlRow["orderline"].ToString();
                    frmNewOAU.textBoxNewOAULineItem.Text = drJobProd["orderline"].ToString();

                    //orderQtyStr = ordDtlRow["orderqty"].ToString();
                    orderQtyStr = drJobProd["prodqty"].ToString();
                    if (float.Parse(orderQtyStr) < 10)
                    {
                        orderQtyStr = orderQtyStr.Substring(0, 1);
                    }
                    else
                    {
                        orderQtyStr = orderQtyStr.Substring(0, 2);
                    }

                    // Create the SerialNo by concatenating the following values.
                    frmNewOAU.textBoxNewOAUSerialNo.Text = "OA" + jobNumStr;

                    // Setup Dual Point Power
                    frmNewOAU.labelNewOAUDPP2.ForeColor = Color.Pink;
                    frmNewOAU.textBoxNewOAUMinCKTAmp2.BackColor = Color.Pink;
                    frmNewOAU.textBoxNewOAUMinCKTAmp2.ReadOnly = true;
                    frmNewOAU.textBoxNewOAUMFSMCB2.BackColor = Color.Pink;
                    frmNewOAU.textBoxNewOAUMFSMCB2.ReadOnly = true;
 
                    if (modelNumFoundBool)
                    {                        
                        modelNumStr = modelNumStr.ToUpper();                        

                        if (oldModelNumBool)   // Old Model Number
                        {
                            GlobalFunctions.parse29DigitModelNo(modelNumStr, frmNewOAU);                                                      
                        }
                        else   // New Model Number
                        {
                            //// Determine which Heating Type settings are used by passing the
                            //// 20th digit of the ModelNo to the getHeatingTypeNewModel function. NOTE:
                            //// When you assign the return value to the SelectedIndex parameter
                            //// this will cause the SelectedIndexChanged event for this combobox
                            //// to fire. This event can be found in the frmNewLineOAU code.
                            //frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex =
                            //    getHeatingTypeNewModel(modelNumStr.Substring(19, 1));                           

                            //// Copy the 27th digit of the model number. This value represents
                            //// Powered Exhaust.
                            //pwrExtDigitStr = modelNumStr.Substring(26, 1);

                            //// Copy the 31st digit of the model number. This value represents ERV.
                            //ervDigitStr = modelNumStr.Substring(30, 1);
                            GlobalFunctions.parse39DigitModelNo(modelNumStr, frmNewOAU);

                            if (modelNumStr.Substring(13, 1) == "3" || modelNumStr.Substring(13, 1) == "8")
                            {
                                frmNewOAU.groupBoxNewOAUWater_Glycol.Visible = true;
                            }
                        }

                    }
                    else
                    {
                        MessageBox.Show("WARNING - " + jobNumStr + " does not contain an OAU model number.");
                        frmNewOAU.groupBoxParseModelNo.Location = new Point(6, 152);
                        frmNewOAU.groupBoxParseModelNo.Visible = true;
                        //frmNewOAU.textBoxModifyModelNo.Focus();
                        //frmNewOAU.textBoxModifyModelNo.Select();
                        frmNewOAU.labelNewOAUMsg.Text = "Please enter Model No and press the Parse button.";
                        frmNewOAU.buttonNewOAUAccept.Enabled = false;
                        //frmNewOAU.buttonNewOAUCancel.Enabled = false;
                    }
                }
                else                     
                {
                    MessageBox.Show("WARNING - Job # " + jobNumStr + 
                                    " does NOT contain a Line# " +  orderLineInt.ToString());
                }
            }           
            catch (Exception f)
            {
                MessageBox.Show("Error occurred when trying to read the jobhead table. - " + f);
                return;
            }

            frmNewOAU.ShowDialog();                                                        
        }

        private void getFanHP(string hpIndStr, string pwrExtHPIndStr, frmNewLineOAU frmNewOAU)
        {
            string hpStr = "";

            if ((hpIndStr == "A") || (hpIndStr == "B"))
            {
                hpStr = "0.5";
            }
            else if ((hpIndStr == "C") || (hpIndStr == "D"))
            {
                hpStr = "0.75";
            }
            else if ((hpIndStr == "E") || (hpIndStr == "F"))
            {
                hpStr = "1.0";
            }
            else if ((hpIndStr == "G") || (hpIndStr == "H"))
            {
                hpStr = "1.5";
            }
            else if ((hpIndStr == "J") || (hpIndStr == "K"))
            {
                hpStr = "2.0";
            }
            else if ((hpIndStr == "L") || (hpIndStr == "M"))
            {
                hpStr = "3.0";
            }
            else if ((hpIndStr == "N") || (hpIndStr == "P"))
            {
                hpStr = "5.0";
            }
            else if ((hpIndStr == "R") || (hpIndStr == "S"))
            {
                hpStr = "7.5";
            }
            else if ((hpIndStr == "T") || (hpIndStr == "U"))
            {
                hpStr = "10.0";
            }
            else if ((hpIndStr == "V") || (hpIndStr == "W"))
            {
                hpStr = "15.0";
            }

            frmNewOAU.textBoxNewOAUFanEvapHP.Text = hpStr;

            if (pwrExtHPIndStr != "0")
            {
                frmNewOAU.textBoxNewOAUFanPwrHP.Text = hpStr;
            } 

        }

        private int getHeatingType(string digitStr)
        {
            int retValInt = -1;

            if (digitStr == "0")  // Indicates No Heat
            {
                retValInt = 2;
            }
            else if (digitStr == "A") // Indicates Indirect-Fired (IF)
            {
                retValInt = 0;
            }
            else if (digitStr == "B") // Indicates Direct-Fired (DF)
            {
                retValInt = 3;
            }
            else if (digitStr == "E") // Indicates Electric
            {
                retValInt = 1;
            }
            else if (digitStr == "J") // Indicates Hot Water
            {
                retValInt = 4;
            }

            return retValInt;
           
        }

        private int getHeatingTypeNewModel(string digitStr)
        {
            int retValInt = -1;

            if (digitStr == "0")  // Indicates No Heat
            {
                retValInt = 2;
            }
            else if (digitStr == "A") // Indicates Indirect-Fired (IF)
            {
                retValInt = 0;
            }
            else if (digitStr == "B") // Indicates Direct-Fired (DF)
            {
                retValInt = 3;
            }
            else if (digitStr == "C") // Indicates Electric 2 Stage   Not currently available
            {
                retValInt = -1;
            }
            else if (digitStr == "D") // Indicates Electric SCR Modulating
            {
                retValInt = 1;
            }
            else if (digitStr == "E") // Indicates Dual Fuel (PRI-DF/SEC-IF) Not currently available
            {
                retValInt = -1;
            }
            else if (digitStr == "F") // Indicates Dual Fuel (PRI-DF/SEC-ELEC)  Not currently available
            {
                retValInt = -1;
            }
            else if (digitStr == "G") // Indicates Dual Fuel (PRI-IF/SEC-ELEC)  Not currently available
            {
                retValInt = -1;
            }
            else if (digitStr == "H") // Indicates Dual Fuel (PRI-ELEC/SEC-ELEC)  Not currently available
            {
                retValInt = -1;
            }
            else if (digitStr == "J") // Indicates Hot Water   Not currently available
            {
                retValInt = 4;
            }
            else if (digitStr == "K") // Indicates Steam   Not currently available
            {
                retValInt = -1;
            }

            return retValInt;

        }

        private int getVoltage(string digitStr)
        {
            int retValInt = -1;

            if (digitStr == "3")  // Indicates 208/230/60/3 voltage
            {
                retValInt = 0;
            }
            else if (digitStr == "4") // Indicates 208/230/60/3 voltage
            {
                retValInt = 1;
            }
            else if (digitStr == "5") // Indicates 208/230/60/3 voltage
            {
                retValInt = 2;
            }

            return retValInt;

        }

    } // End of Class    
}

