namespace KCC.OA.Etl
{
    partial class frmViperOpen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBoxOpenOrders = new System.Windows.Forms.GroupBox();
            this.dgvJobNumList = new System.Windows.Forms.DataGridView();
            this.listViewOOOrderNums = new System.Windows.Forms.ListView();
            this.columnHeaderJobNum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderOrderNum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderLineNum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderReleaseNum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderUnitType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderCustomer = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.menuStripOOFileMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lCSystemsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tradewiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelOOStartAt = new System.Windows.Forms.Label();
            this.textBoxOOStartAt = new System.Windows.Forms.TextBox();
            this.buttonExit = new System.Windows.Forms.Button();
            this.btnNewLine = new System.Windows.Forms.Button();
            this.lbEnvironment = new System.Windows.Forms.Label();
            this.groupBoxOpenOrders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvJobNumList)).BeginInit();
            this.menuStripOOFileMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxOpenOrders
            // 
            this.groupBoxOpenOrders.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBoxOpenOrders.Controls.Add(this.dgvJobNumList);
            this.groupBoxOpenOrders.Controls.Add(this.listViewOOOrderNums);
            this.groupBoxOpenOrders.Location = new System.Drawing.Point(12, 70);
            this.groupBoxOpenOrders.Name = "groupBoxOpenOrders";
            this.groupBoxOpenOrders.Size = new System.Drawing.Size(932, 541);
            this.groupBoxOpenOrders.TabIndex = 0;
            this.groupBoxOpenOrders.TabStop = false;
            this.groupBoxOpenOrders.Text = "Orders";
            // 
            // dgvJobNumList
            // 
            this.dgvJobNumList.AllowUserToAddRows = false;
            this.dgvJobNumList.AllowUserToDeleteRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            this.dgvJobNumList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvJobNumList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvJobNumList.Location = new System.Drawing.Point(7, 23);
            this.dgvJobNumList.Name = "dgvJobNumList";
            this.dgvJobNumList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvJobNumList.Size = new System.Drawing.Size(919, 499);
            this.dgvJobNumList.TabIndex = 1;
            this.dgvJobNumList.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvJobNumList_CellMouseDoubleClick);
            // 
            // listViewOOOrderNums
            // 
            this.listViewOOOrderNums.BackColor = System.Drawing.Color.White;
            this.listViewOOOrderNums.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderJobNum,
            this.columnHeaderOrderNum,
            this.columnHeaderLineNum,
            this.columnHeaderReleaseNum,
            this.columnHeaderUnitType,
            this.columnHeaderCustomer});
            this.listViewOOOrderNums.ForeColor = System.Drawing.Color.Blue;
            this.listViewOOOrderNums.FullRowSelect = true;
            this.listViewOOOrderNums.GridLines = true;
            this.listViewOOOrderNums.Location = new System.Drawing.Point(6, 459);
            this.listViewOOOrderNums.MultiSelect = false;
            this.listViewOOOrderNums.Name = "listViewOOOrderNums";
            this.listViewOOOrderNums.Size = new System.Drawing.Size(920, 63);
            this.listViewOOOrderNums.TabIndex = 0;
            this.listViewOOOrderNums.UseCompatibleStateImageBehavior = false;
            this.listViewOOOrderNums.View = System.Windows.Forms.View.Details;
            this.listViewOOOrderNums.Visible = false;
            this.listViewOOOrderNums.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewOOOrderNums_MouseDoubleClick);
            // 
            // columnHeaderJobNum
            // 
            this.columnHeaderJobNum.Text = "Job #";
            this.columnHeaderJobNum.Width = 120;
            // 
            // columnHeaderOrderNum
            // 
            this.columnHeaderOrderNum.Text = "Order #";
            this.columnHeaderOrderNum.Width = 100;
            // 
            // columnHeaderLineNum
            // 
            this.columnHeaderLineNum.Text = "Line #";
            this.columnHeaderLineNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeaderLineNum.Width = 75;
            // 
            // columnHeaderReleaseNum
            // 
            this.columnHeaderReleaseNum.Text = "Rel #";
            this.columnHeaderReleaseNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeaderReleaseNum.Width = 75;
            // 
            // columnHeaderUnitType
            // 
            this.columnHeaderUnitType.Text = "Unit Type";
            this.columnHeaderUnitType.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeaderUnitType.Width = 100;
            // 
            // columnHeaderCustomer
            // 
            this.columnHeaderCustomer.Text = "Customer";
            this.columnHeaderCustomer.Width = 450;
            // 
            // menuStripOOFileMenu
            // 
            this.menuStripOOFileMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.menuStripOOFileMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStripOOFileMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStripOOFileMenu.Location = new System.Drawing.Point(0, 0);
            this.menuStripOOFileMenu.Name = "menuStripOOFileMenu";
            this.menuStripOOFileMenu.Size = new System.Drawing.Size(957, 24);
            this.menuStripOOFileMenu.TabIndex = 1;
            this.menuStripOOFileMenu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lCSystemsToolStripMenuItem,
            this.tradewiToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // lCSystemsToolStripMenuItem
            // 
            this.lCSystemsToolStripMenuItem.Name = "lCSystemsToolStripMenuItem";
            this.lCSystemsToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.lCSystemsToolStripMenuItem.Text = "LC Systems";
            this.lCSystemsToolStripMenuItem.Click += new System.EventHandler(this.lCSystemsToolStripMenuItem_Click);
            // 
            // tradewiToolStripMenuItem
            // 
            this.tradewiToolStripMenuItem.Name = "tradewiToolStripMenuItem";
            this.tradewiToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.tradewiToolStripMenuItem.Text = "Tradewinds";
            this.tradewiToolStripMenuItem.Click += new System.EventHandler(this.tradewiToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // labelOOStartAt
            // 
            this.labelOOStartAt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.labelOOStartAt.Location = new System.Drawing.Point(15, 37);
            this.labelOOStartAt.Name = "labelOOStartAt";
            this.labelOOStartAt.Size = new System.Drawing.Size(70, 21);
            this.labelOOStartAt.TabIndex = 2;
            this.labelOOStartAt.Text = "Start At:";
            this.labelOOStartAt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxOOStartAt
            // 
            this.textBoxOOStartAt.BackColor = System.Drawing.Color.White;
            this.textBoxOOStartAt.ForeColor = System.Drawing.Color.Black;
            this.textBoxOOStartAt.Location = new System.Drawing.Point(91, 35);
            this.textBoxOOStartAt.Name = "textBoxOOStartAt";
            this.textBoxOOStartAt.Size = new System.Drawing.Size(100, 23);
            this.textBoxOOStartAt.TabIndex = 3;
            this.textBoxOOStartAt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxOOStartAt_KeyDown);
            // 
            // buttonExit
            // 
            this.buttonExit.BackColor = System.Drawing.Color.White;
            this.buttonExit.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExit.ForeColor = System.Drawing.Color.Red;
            this.buttonExit.Location = new System.Drawing.Point(837, 37);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(108, 27);
            this.buttonExit.TabIndex = 4;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = false;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // btnNewLine
            // 
            this.btnNewLine.BackColor = System.Drawing.Color.White;
            this.btnNewLine.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewLine.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnNewLine.Location = new System.Drawing.Point(702, 37);
            this.btnNewLine.Name = "btnNewLine";
            this.btnNewLine.Size = new System.Drawing.Size(108, 27);
            this.btnNewLine.TabIndex = 5;
            this.btnNewLine.Text = "New Line";
            this.btnNewLine.UseVisualStyleBackColor = false;
            this.btnNewLine.Click += new System.EventHandler(this.btnNewLine_Click);
            // 
            // lbEnvironment
            // 
            this.lbEnvironment.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEnvironment.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lbEnvironment.Location = new System.Drawing.Point(339, 33);
            this.lbEnvironment.Name = "lbEnvironment";
            this.lbEnvironment.Size = new System.Drawing.Size(327, 27);
            this.lbEnvironment.TabIndex = 6;
            this.lbEnvironment.Text = "Production Environment";
            this.lbEnvironment.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmViperOpen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(957, 605);
            this.Controls.Add(this.lbEnvironment);
            this.Controls.Add(this.btnNewLine);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.textBoxOOStartAt);
            this.Controls.Add(this.labelOOStartAt);
            this.Controls.Add(this.groupBoxOpenOrders);
            this.Controls.Add(this.menuStripOOFileMenu);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.MainMenuStrip = this.menuStripOOFileMenu;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmViperOpen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ETL Sticker Order Numbers";
            this.groupBoxOpenOrders.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvJobNumList)).EndInit();
            this.menuStripOOFileMenu.ResumeLayout(false);
            this.menuStripOOFileMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxOpenOrders;
        private System.Windows.Forms.MenuStrip menuStripOOFileMenu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        public System.Windows.Forms.ListView listViewOOOrderNums;
        public System.Windows.Forms.ColumnHeader columnHeaderOrderNum;
        public System.Windows.Forms.ColumnHeader columnHeaderLineNum;
        public System.Windows.Forms.ColumnHeader columnHeaderReleaseNum;
        public System.Windows.Forms.ColumnHeader columnHeaderUnitType;
        public System.Windows.Forms.ColumnHeader columnHeaderCustomer;
        private System.Windows.Forms.Label labelOOStartAt;
        public System.Windows.Forms.TextBox textBoxOOStartAt;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.ColumnHeader columnHeaderJobNum;
        private System.Windows.Forms.Button btnNewLine;
        private System.Windows.Forms.ToolStripMenuItem lCSystemsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tradewiToolStripMenuItem;
        private System.Windows.Forms.DataGridView dgvJobNumList;
        private System.Windows.Forms.Label lbEnvironment;
    }
}