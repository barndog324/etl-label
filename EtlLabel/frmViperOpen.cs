using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace KCC.OA.Etl
{
    public partial class frmViperOpen : Form
    {
        MainBO objMain = new MainBO();

        public frmViperOpen()
        {
            InitializeComponent();
                      
            GlobalEpicorConnectionString.EpicorServer = ConfigurationManager.AppSettings["EpicorAppServer"];
           
            if (GlobalEpicorConnectionString.EpicorServer.ToLower().Contains("epicor905app") == true)
            {
                GlobalEpicorConnectionString.EnvColor = Color.Snow;
                GlobalEpicorConnectionString.EnvironmentStr = "Production Environment";
                GlobalEpicorConnectionString.DisplayMode = "Production";

                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["ProdDatabase.Properties.Settings.epicor905ConnectionString"].ConnectionString = "Data Source=EPICOR905SQL\\EPICOR905;Initial Catalog=epicor905;Integrated Security=True";
                config.ConnectionStrings.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString = "Data Source=EPICOR905SQL\\EPICOR905;Initial Catalog=KCC;Integrated Security=True";
                config.Save(ConfigurationSaveMode.Modified);

                GlobalKCCConnectionString.KCCConnectString = config.ConnectionStrings.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;
                GlobalEpicorConnectionString.EpicorConnectString = config.ConnectionStrings.ConnectionStrings["ProdDatabase.Properties.Settings.epicor905ConnectionString"].ConnectionString;

                ConfigurationManager.RefreshSection("connectionStrings");
            }
            else if (GlobalEpicorConnectionString.EpicorServer.ToLower().Contains("kccwvpepic9app") == true)
            {
                GlobalEpicorConnectionString.EnvColor = Color.Gainsboro;
                GlobalEpicorConnectionString.EnvironmentStr = "New Production Environment";
                GlobalEpicorConnectionString.DisplayMode = "Production";
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["ProdDatabase.Properties.Settings.epicor905ConnectionString"].ConnectionString = "Data Source=kccwvpepic9sql;Initial Catalog=epicor905;Integrated Security=True";
                config.ConnectionStrings.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString = "Data Source=kccwvpepic9sql;Initial Catalog=KCC;Integrated Security=True";
                config.Save(ConfigurationSaveMode.Modified);

                GlobalKCCConnectionString.KCCConnectString = config.ConnectionStrings.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;
                GlobalEpicorConnectionString.EpicorConnectString = config.ConnectionStrings.ConnectionStrings["ProdDatabase.Properties.Settings.epicor905ConnectionString"].ConnectionString;

                ConfigurationManager.RefreshSection("connectionStrings");
            }
            else if (GlobalEpicorConnectionString.EpicorServer.Contains("kccwvpkntcapp01") == true)
            {
                GlobalEpicorConnectionString.EnvColor = Color.Salmon;
                GlobalEpicorConnectionString.EnvironmentStr = "Kinetic Production Environment";
                GlobalEpicorConnectionString.DisplayMode = "Production";

                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["ProdDatabase.Properties.Settings.epicor905ConnectionString"].ConnectionString = "Data Source=kccwvpkntcsql01;Initial Catalog=epicor905;Integrated Security=True";
                config.ConnectionStrings.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString = "Data Source=kccwvpkntcsql01;Initial Catalog=KCC;Integrated Security=True";
                config.Save(ConfigurationSaveMode.Modified);

                GlobalKCCConnectionString.KCCConnectString = config.ConnectionStrings.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;
                GlobalEpicorConnectionString.EpicorConnectString = config.ConnectionStrings.ConnectionStrings["ProdDatabase.Properties.Settings.epicor905ConnectionString"].ConnectionString;

                ConfigurationManager.RefreshSection("connectionStrings");
            }
            else
            {
                GlobalEpicorConnectionString.EnvColor = Color.SpringGreen;
                GlobalEpicorConnectionString.EnvironmentStr = "Test 2 Environment";
                GlobalEpicorConnectionString.DisplayMode = "Test";

                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["ProdDatabase.Properties.Settings.epicor905ConnectionString"].ConnectionString = "Data Source=KCCWVTEPICSQL01;Initial Catalog=epicor905;Integrated Security=True";
                config.ConnectionStrings.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString = "Data Source=KCCWVTEPICSQL01;Initial Catalog=KCC;Integrated Security=True";
                config.Save(ConfigurationSaveMode.Modified);

                GlobalKCCConnectionString.KCCConnectString = config.ConnectionStrings.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;
                GlobalEpicorConnectionString.EpicorConnectString = config.ConnectionStrings.ConnectionStrings["ProdDatabase.Properties.Settings.epicor905ConnectionString"].ConnectionString;

                ConfigurationManager.RefreshSection("connectionStrings");
            }



//#if DEBUG
//              try           // If in the debug environment then set the Database connection string to the test database.
//              {
//                  this.BackColor = Color.LightCoral;
//                  lbEnvironment.Text = "Development Environment";
                  
//                  Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
//                  config.ConnectionStrings.ConnectionStrings["ProdDatabase.Properties.Settings.KCC905ConnectionString"].ConnectionString = "Data Source=DEVSVR01;Initial Catalog=KCC;Integrated Security=True";
//                  config.Save(ConfigurationSaveMode.Modified);

//                  ConfigurationManager.RefreshSection("connectionStrings");
//              }
//              catch (Exception ex)
//              {
//                  //MessageBox.Show(ConfigurationManager.ConnectionStrings["con"].ToString() + ".This is invalid connection", "Incorrect server/Database - " + ex);
//              }
//#if TEST2
//              try           // If in the debug environment then set the Database connection string to the test database.
//              {
//                  this.BackColor = Color.SpringGreen;
//                  lbEnvironment.Text = "Test 2 Environment";
                  
//                  Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
//                  config.ConnectionStrings.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString = "Data Source=KCCWVTEPICSQL01;Initial Catalog=KCC;Integrated Security=True";
//                  config.Save(ConfigurationSaveMode.Modified);


//                  ConfigurationManager.RefreshSection("connectionStrings");
//              }
//              catch (Exception ex)
//              {
//                  //MessageBox.Show(ConfigurationManager.ConnectionStrings["con"].ToString() + ".This is invalid connection", "Incorrect server/Database - " + ex);
//              }
//#endif
//#endif


            this.BackColor = GlobalEpicorConnectionString.EnvColor;
            lbEnvironment.Text = GlobalEpicorConnectionString.EnvironmentStr;
            
            GlobalModeType.ModeType = "Update";
            populateOrdersListview(this);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GlobalModeType.ModeType = "";
            this.Close();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            GlobalModeType.ModeType = "";
            this.Close();
        }       
        

        private void listViewOOOrderNums_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            string jobNumStr;
            string orderNumStr;
            string orderLineStr;
            string releaseNumStr;
            string unitTypeStr;
            string customerStr = "";
            string endConsumerStr = "";

            int orderNumInt;
            int orderLineInt;
            int releaseNumInt;
            int itemSelectedInt = 0;

            databaseFunctions dbfuncs = new databaseFunctions();

            // Go through all the Items to determine which one the user selected.
            for (int i = 0; i <= this.listViewOOOrderNums.Items.Count - 1; i++)
            {
                if (this.listViewOOOrderNums.Items[i].Selected == true)
                {
                    itemSelectedInt = i;
                }
            }

            jobNumStr = (string)this.listViewOOOrderNums.Items[itemSelectedInt].Text;
            orderNumStr = (string)this.listViewOOOrderNums.Items[itemSelectedInt].SubItems[1].Text;
            orderLineStr = this.listViewOOOrderNums.Items[itemSelectedInt].SubItems[2].Text.ToString();
            releaseNumStr = this.listViewOOOrderNums.Items[itemSelectedInt].SubItems[3].Text.ToString();
            unitTypeStr = this.listViewOOOrderNums.Items[itemSelectedInt].SubItems[4].Text.ToString();
            endConsumerStr = this.listViewOOOrderNums.Items[itemSelectedInt].SubItems[5].Text.ToString();

            orderNumInt = Int32.Parse(orderNumStr);
            orderLineInt = Int32.Parse(orderLineStr);
            releaseNumInt = Int32.Parse(releaseNumStr);

            customerStr = dbfuncs.getCustomerName(orderNumInt);

            if (unitTypeStr.Equals("OAU", StringComparison.OrdinalIgnoreCase))
            {
                try
                {
                    VS_DataSetTableAdapters.EtlOAUTableAdapter etlOAUTA =
                        new VS_DataSetTableAdapters.EtlOAUTableAdapter();

                    etlOAUTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                    // Read the orderDtlOAU table and retrieve the specified OrderNum/OrderLine/ReleaseNum
                    // info and then use the NewLineOAU form to display the item.
                    VS_DataSet.EtlOAUDataTable etlOAUDT =
                        etlOAUTA.GetDataByJobNum(jobNumStr);

                    if (etlOAUDT.Rows.Count == 0)
                    {
                        MessageBox.Show("No ETL Label has been created for Job # - " + jobNumStr);
                    }
                    else
                    {
                        //populate_frmNewLineOAU(etlOAUDT, endConsumerStr, customerStr); No longer need to pass data table
                        populate_frmNewLineOAU(jobNumStr, endConsumerStr, customerStr, true);
                    }                   
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR reading the EtlOAU table for Job # " + jobNumStr + "Exception: " + ex);
                }
            }
            else if (unitTypeStr.Equals("FAN", StringComparison.OrdinalIgnoreCase))
            {
                try
                {
                    //VS_DataSetTableAdapters.OrderDtlFANTableAdapter ordDtlFANTA =
                    //    new VS_DataSetTableAdapters.OrderDtlFANTableAdapter();

                    VS_DataSetTableAdapters.EtlFANTableAdapter etlFANTA =
                        new VS_DataSetTableAdapters.EtlFANTableAdapter();

                    //ordDtlFANTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                    etlFANTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                    // Read the orderDtlFAN table and retrieve the specified OrderNum/OrderLine/ReleaseNum
                    // info and then use the NewLineFAN form to display the item.
                    //VS_DataSet.OrderDtlFANDataTable ordDtlFANDT =
                    //    ordDtlFANTA.GetDataByOrderNumOrderLineReleaseNum(orderNumInt, orderLineInt, releaseNumInt);

                    VS_DataSet.EtlFANDataTable etlFANDT =
                        etlFANTA.GetDataByJobNum(jobNumStr);

                    if (etlFANDT.Rows.Count == 0)
                    {
                        MessageBox.Show("No ETL Label has been created for Job # - " + jobNumStr);
                    }
                    else
                    {
                        populate_frmNewLineFAN(etlFANDT, endConsumerStr, customerStr);
                    }                   
                }
                catch
                {
                    MessageBox.Show("ERROR reading the EtlFAN table for Job # " + jobNumStr);
                }
            }
            else if (unitTypeStr.Equals("RRU", StringComparison.OrdinalIgnoreCase))
            {
                try
                {
                    VS_DataSetTableAdapters.EtlRRUTableAdapter etlRRUTA =
                        new VS_DataSetTableAdapters.EtlRRUTableAdapter();

                    etlRRUTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                    // Read the orderDtlRRU table and retrieve the specified OrderNum/OrderLine/ReleaseNum
                    // info and then use the NewLineRRU form to display the item.
                    VS_DataSet.EtlRRUDataTable etlRRUDT =
                        //etlRRUTA.GetDataByOrderNumOrderLineReleaseNum(orderNumInt, orderLineInt, releaseNumInt);
                        etlRRUTA.GetDataByJobNum(jobNumStr);

                    if (etlRRUDT.Rows.Count == 0)
                    {
                        MessageBox.Show("No ETL Label has been created for Job # - " + jobNumStr);
                    }
                    else
                    {
                        populate_frmNewLineRRU(etlRRUDT, endConsumerStr, customerStr);
                    }
                }
                catch
                {
                    MessageBox.Show("ERROR reading the EtlRRU table for Job # - " + jobNumStr);
                }
            }
            else if (unitTypeStr.Equals("MUA", StringComparison.OrdinalIgnoreCase))
            {
                try
                {
                    //VS_DataSetTableAdapters.OrderDtlMUATableAdapter ordDtlMUATA =
                    //    new VS_DataSetTableAdapters.OrderDtlMUATableAdapter();

                    VS_DataSetTableAdapters.EtlMUATableAdapter etlMUATA =
                        new VS_DataSetTableAdapters.EtlMUATableAdapter();

                    //ordDtlMUATA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                    etlMUATA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                    // Read the orderDtlMUA table and retrieve the specified OrderNum/OrderLine/ReleaseNum
                    // info and then use the NewLineMUA form to display the item.
                    //VS_DataSet.OrderDtlMUADataTable ordDtlMUADT =
                    //    ordDtlMUATA.GetDataByOrderNumOrderLineReleaseNum(orderNumInt, orderLineInt, releaseNumInt);

                    VS_DataSet.EtlMUADataTable etlMUADT =
                        etlMUATA.GetDataByJobNum(jobNumStr);

                    if (etlMUADT.Rows.Count == 0)
                    {
                        MessageBox.Show("No ETL Label has been created for Job # - " + jobNumStr);
                    }
                    else
                    {
                        populate_frmNewLineMUA(etlMUADT, endConsumerStr, customerStr);
                    }                    
                }
                catch
                {
                    MessageBox.Show("ERROR reading the EtlMUA table for JobNum # " + jobNumStr);
                }
            }
            else if (unitTypeStr.Equals("PCO", StringComparison.OrdinalIgnoreCase))
            {
                try
                {
                    //VS_DataSetTableAdapters.OrderDtlPCOTableAdapter ordDtlPCOTA =
                    //    new VS_DataSetTableAdapters.OrderDtlPCOTableAdapter();

                    VS_DataSetTableAdapters.EtlPCOTableAdapter etlPCOTA =
                        new VS_DataSetTableAdapters.EtlPCOTableAdapter();

                    //ordDtlPCOTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                    etlPCOTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                    // Read the orderDtlPCO table and retrieve the specified OrderNum/OrderLine/ReleaseNum
                    // info and then use the NewLinePCO form to display the item.
                    //VS_DataSet.OrderDtlPCODataTable ordDtlPCODT =
                    //    ordDtlPCOTA.GetDataByOrderNumOrderLineReleaseNum(orderNumInt, orderLineInt, releaseNumInt);

                    VS_DataSet.EtlPCODataTable etlPCODT =
                        etlPCOTA.GetDataByJobNum(jobNumStr);


                    if (etlPCODT.Rows.Count == 0)
                    {
                        MessageBox.Show("No ETL Label has been created for Job # - " + jobNumStr);
                    }
                    else
                    {
                        populate_frmNewLinePCO(etlPCODT, endConsumerStr, customerStr);
                    }                                        
                }
                catch (Exception f)
                {
                    MessageBox.Show("JobNum " + f);
                    //MessageBox.Show("ERROR reading the orderDtlPCO table for OrderNum/OrderLine/ReleaseNum - "
                    //               + orderNumStr + "/" + orderLineStr + "/" + releaseNumStr);
                }
            }
            else if (unitTypeStr.Equals("MON", StringComparison.OrdinalIgnoreCase))
            {
                try
                {
                    objMain.JobNum = jobNumStr;
                    DataTable dt = objMain.GetMonitorEtlData();

                    if (objMain.JobNum != "")
                    {
                        MessageBox.Show("No ETL Label has been created for Job # - " + jobNumStr);
                    }
                    else
                    {
                        populate_frmNewLineMON(endConsumerStr, customerStr, dt);
                    }
                }
                catch (Exception f)
                {
                    MessageBox.Show("JobNum " + f);
                    //MessageBox.Show("ERROR reading the orderDtlPCO table for OrderNum/OrderLine/ReleaseNum - "
                    //               + orderNumStr + "/" + orderLineStr + "/" + releaseNumStr);
                }
            }

        }

        private void populateOrdersListview(frmViperOpen frmVSOpen)
        {
            ListViewItem lviOpen;

            // Create SqlDataAdapters for reading the VSOrderHed 
            // table using strongly typed datasets.
            VS_DataSetTableAdapters.VSOrderHedTableAdapter vsOrderHedTA =
                new VS_DataSetTableAdapters.VSOrderHedTableAdapter();

            vsOrderHedTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

            // Read the VSOrderHed table and retrieve all the order numbers and their respective
            // line numbers that have been previously processed and display them on the screen
            // so the user can select which order theyed like to look at in detail.
            /*
            VS_DataSet.VSOrderHedDataTable vsOrdHedDT = vsOrderHedTA.GetDataByOrderByOrderNum();

            foreach (DataRow dr in vsOrdHedDT)
            {
                string tempStr;

                //lviOpen = new ListViewItem(objMain.OrderNum"].ToString());
                lviOpen = new ListViewItem(objMain.JobNum"].ToString());
                tempStr = objMain.OrderNum;
                lviOpen.SubItems.Add(tempStr.Trim());
                tempStr = objMain.OrderLine;
                lviOpen.SubItems.Add(tempStr.Trim());
                tempStr = objMain.ReleaseNum;
                lviOpen.SubItems.Add(tempStr.Trim());                
                lviOpen.SubItems.Add(objMain.UnitType"].ToString());
                lviOpen.SubItems.Add(objMain.EndConsumerName"].ToString());
                frmVSOpen.listViewOOOrderNums.Items.Add(lviOpen);
            }

            if (vsOrdHedDT.Count > 0)
            {               
                frmVSOpen.listViewOOOrderNums.Items[0].Selected = true;
                frmVSOpen.listViewOOOrderNums.Select();                
            }   
            */

            //VS_DataSetTableAdapters.EtlJobHeadTableAdapter etlJobHeadTA =
            //    new VS_DataSetTableAdapters.EtlJobHeadTableAdapter();

            //etlJobHeadTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

            ////VS_DataSet.VSOrderHedDataTable vsOrdHedDT = vsOrderHedTA.GetDataByOrderByOrderNum();
            //VS_DataSet.EtlJobHeadDataTable etlJobHead = etlJobHeadTA.GetDataByOrderByJobNum();

            //foreach (DataRow dr in etlJobHead)
            //{
            //    string tempStr;

            //    //lviOpen = new ListViewItem(objMain.OrderNum"].ToString());
            //    lviOpen = new ListViewItem(dr["JobNum"].ToString());
            //    tempStr = dr["OrderNum"].ToString();
            //    lviOpen.SubItems.Add(tempStr.Trim());
            //    tempStr = dr["OrderLine"].ToString();
            //    lviOpen.SubItems.Add(tempStr.Trim());                
            //    tempStr = dr["ReleaseNum"].ToString();
            //    lviOpen.SubItems.Add(tempStr.Trim());
            //    lviOpen.SubItems.Add(dr["UnitType"].ToString());
            //    lviOpen.SubItems.Add(dr["EndConsumerName"].ToString());
            //    frmVSOpen.listViewOOOrderNums.Items.Add(lviOpen);
            //}

            DataTable dt = objMain.GetJobNumberList();

            dgvJobNumList.DataSource = dt;
            
            dgvJobNumList.Columns["JobNum"].Width = 125;            // JobNum
            dgvJobNumList.Columns["JobNum"].HeaderText = "Job Number";
            dgvJobNumList.Columns["JobNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvJobNumList.Columns["JobNum"].DisplayIndex = 0;
            dgvJobNumList.Columns["OrderNum"].Width = 75;            // OrderNum
            dgvJobNumList.Columns["OrderNum"].HeaderText = "OrderNum";
            dgvJobNumList.Columns["OrderNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvJobNumList.Columns["OrderNum"].DisplayIndex = 1;
            dgvJobNumList.Columns["Customer"].Width = 200;           // Customer
            dgvJobNumList.Columns["Customer"].HeaderText = "Customer";
            dgvJobNumList.Columns["Customer"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvJobNumList.Columns["Customer"].DisplayIndex = 2;
            dgvJobNumList.Columns["UnitType"].Width = 75;           //Unit Type
            dgvJobNumList.Columns["UnitType"].HeaderText = "Unit Type";
            dgvJobNumList.Columns["UnitType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvJobNumList.Columns["UnitType"].DisplayIndex = 3;
            dgvJobNumList.Columns["OrderLine"].Width = 60;            //OrderLine
            dgvJobNumList.Columns["OrderLine"].HeaderText = "Order\nLine";
            dgvJobNumList.Columns["OrderLine"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvJobNumList.Columns["OrderLine"].DisplayIndex = 4;
            dgvJobNumList.Columns["ReleaseNum"].Width = 60;            //ReleaseNum
            dgvJobNumList.Columns["ReleaseNum"].HeaderText = "Rel\nNum";
            dgvJobNumList.Columns["ReleaseNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvJobNumList.Columns["ReleaseNum"].DisplayIndex = 5;
            dgvJobNumList.Columns["EndConsumerName"].Width = 250;           // EndConsumerName
            dgvJobNumList.Columns["EndConsumerName"].HeaderText = "EndConsumerName";
            dgvJobNumList.Columns["EndConsumerName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvJobNumList.Columns["EndConsumerName"].DisplayIndex = 6;
            
        }


        private void populate_frmNewLinePCO(VS_DataSet.EtlPCODataTable etlPCODT, 
                                            string endConsumerStr, string customerNameStr)
        {         
            frmNewLinePCO frmNewPCO = new frmNewLinePCO();

            DataRow dr = etlPCODT.Rows[0];

            GlobalModeType.ModeType = "Update";

            frmNewPCO.Text = "Update PCO Job # " + dr["JobNum"].ToString();

            // Fill in all of the various info on the form.
            frmNewPCO.textBoxNewPCOOrderNum.Text = dr["OrderNum"].ToString();

            frmNewPCO.textBoxNewPCOEndConsumer.Text = endConsumerStr;
            frmNewPCO.textBoxNewPCOCustomer.Text = customerNameStr;
            frmNewPCO.textBoxNewPCOCustomer.Enabled = false;            
            
            frmNewPCO.textBoxNewPCOLineItem.Text = dr["OrderLine"].ToString();
            frmNewPCO.textBoxNewPCOReleaseNum.Text = dr["ReleaseNum"].ToString();
            frmNewPCO.textBoxNewPCOPONum.Text = dr["PONum"].ToString();
            frmNewPCO.textBoxNewPCOJobNo.Text = dr["JobNum"].ToString();
            frmNewPCO.textBoxNewPCOModelNo.Text = dr["ModelNum"].ToString();
            frmNewPCO.textBoxNewPCODateOfMfg.Text = dr["DateOfMfg"].ToString();
            frmNewPCO.textBoxNewPCOTag.Text = dr["Tag"].ToString();
            frmNewPCO.textBoxNewPCOBulbPartNum.Text = dr["BulbPartNum"].ToString();

            frmNewPCO.buttonNewPCOAccept.Text = "Update";
            frmNewPCO.buttonNewPCOPrint.Enabled = true;
            frmNewPCO.buttonNewPCOPrint.Focus();
            frmNewPCO.buttonNewPCOPrint.Select();            
            frmNewPCO.textBoxNewPCOPONum.Enabled = true;

            frmNewPCO.labelNewPCOMsg.Text = "Existing PCO order, make updates then press the Update button to save.";
            GlobalModeType.ModeType = "Update";
            frmNewPCO.ShowDialog();


        }

        private void populate_frmNewLineMUA(VS_DataSet.EtlMUADataTable etlMUADT, 
                                            string endConsumerStr, string customerNameStr)
        {
            string frenchStr;

            frmNewLineMUA frmNewMUA = new frmNewLineMUA();

            DataRow dr = etlMUADT.Rows[0];

            GlobalModeType.ModeType = "Update";

            frmNewMUA.Text = "Update MUA Job # " + dr["JobNum"].ToString();

            // Fill in all of the various info on the form.
            frmNewMUA.textBoxNewMUAJobNum.Text = dr["JobNum"].ToString();
            frmNewMUA.textBoxNewMUAOrderNum.Text = dr["OrderNum"].ToString();

            frmNewMUA.textBoxNewMUAEndConsumer.Text = endConsumerStr;
            frmNewMUA.textBoxNewMUAEndConsumer.Focus();
            frmNewMUA.textBoxNewMUAEndConsumer.Select();

            frmNewMUA.textBoxNewMUACustomer.Text = customerNameStr;
            frmNewMUA.textBoxNewMUACustomer.Enabled = false;
            
            frmNewMUA.textBoxNewMUALineItem.Text = dr["OrderLine"].ToString();
            frmNewMUA.textBoxNewMUAReleaseNum.Text = dr["ReleaseNum"].ToString();
            frmNewMUA.textBoxNewMUAModelNo.Text = dr["ModelNum"].ToString();
            frmNewMUA.textBoxNewMUAModelNo.Enabled = false;
            frmNewMUA.textBoxNewMUAPartNum.Text = dr["PartNum"].ToString();
            frmNewMUA.textBoxNewMUASerialNo.Text = dr["SerialNum"].ToString();
            frmNewMUA.textBoxNewMUATag.Text = dr["Tag"].ToString();
            frmNewMUA.textBoxNewMUAElecBlowerFan.Text = dr["ElecBlowerFan"].ToString();
            frmNewMUA.textBoxNewMUAElecVolts.Text = dr["ElecVolts"].ToString();
            frmNewMUA.textBoxNewMUAElecPhase.Text = dr["ElecPhase"].ToString();
            frmNewMUA.textBoxNewMUAElecHertz.Text = dr["ElecHertz"].ToString();
            frmNewMUA.textBoxNewMUAElecFLA.Text = dr["ElecFLA"].ToString();
            frmNewMUA.textBoxNewMUAElecMinVoltage.Text = dr["ElecMinVoltage"].ToString();
            frmNewMUA.textBoxNewMUAElecMaxVoltage.Text = dr["ElecMaxVoltage"].ToString();
            frmNewMUA.textBoxNewMUAElecMaxVoltageHertz.Text = dr["ElecMaxVoltageHertz"].ToString();
            frmNewMUA.textBoxNewMUAAirSCFM.Text = dr["AirSCFM"].ToString();
            frmNewMUA.textBoxNewMUAAirMaxTotalPressure.Text = dr["AirMaxTotalPressure"].ToString();
            frmNewMUA.textBoxNewMUAAirMaxExtPressure.Text = dr["AirMaxExtPressure"].ToString();
            frmNewMUA.textBoxNewMUAAirTempRise.Text = dr["AirTempRise"].ToString();
            frmNewMUA.textBoxNewMUAGasInput.Text = dr["GasInput"].ToString();
            frmNewMUA.textBoxNewMUAGasOutput.Text = dr["GasOutput"].ToString();
            frmNewMUA.textBoxNewMUAVerifiedBy.Text = dr["VerifiedBy"].ToString();
            frmNewMUA.textBoxNewMUAVerifiedBy.Visible = true;
            frmNewMUA.labelNewMUAVerifiedBy.Visible = true;
            frenchStr = dr["French"].ToString();

            if (frenchStr.Equals("True", StringComparison.OrdinalIgnoreCase))
            {
                frmNewMUA.checkBoxNewMUAFrench.Checked = true;
            }

            frmNewMUA.buttonNewMUAAccept.Text = "Update";
            frmNewMUA.buttonNewMUAPrint.Enabled = true;

            frmNewMUA.labelNewMUAMsg.Text = "Existing MUA order, if any updates are made then press the";
            frmNewMUA.labelNewMUAMsg2.Text = "Update button to save the information.";
            GlobalModeType.ModeType = "UpdateRRU";
            frmNewMUA.ShowDialog();
            

        }

        private void populate_frmNewLineRRU(VS_DataSet.EtlRRUDataTable etlRRUDT, 
                                            string endConsumerStr, string customerNameStr)
        {
            string modelNoStr;
            string htgTypeStr;
            string frenchStr;
            string heatFuelTypeStr;
            //string fuelTypeStr;

            int modelNoLocInt = -1;
            int i = 0;

            frmNewLineRRU frmNewRRU = new frmNewLineRRU();

            DataRow dr = etlRRUDT.Rows[0];

            GlobalModeType.ModeType = "Update";
            frmNewRRU.Size = new System.Drawing.Size(860, 710);
            frmNewRRU.groupBoxNewRRU.Size = new System.Drawing.Size(822, 633);


            frmNewRRU.Text = "Update RRU Job # " + dr["JobNum"].ToString();

            // Fill in all of the various info on the form.
            frmNewRRU.textBoxNewRRUJobNum.Text = dr["JobNum"].ToString();
            frmNewRRU.textBoxNewRRUOrderNum.Text = dr["OrderNum"].ToString();

            frmNewRRU.textBoxNewRRUEndConsumer.Text = endConsumerStr;
            frmNewRRU.textBoxNewRRUEndConsumer.Focus();
            frmNewRRU.textBoxNewRRUEndConsumer.Select();

            frmNewRRU.textBoxNewRRUCustomer.Text = customerNameStr;
            frmNewRRU.textBoxNewRRUCustomer.Enabled = false;
            
            frmNewRRU.textBoxNewRRULineItem.Text = dr["OrderLine"].ToString();
            frmNewRRU.textBoxNewRRUReleaseNum.Text = dr["ReleaseNum"].ToString();

            try
            {
                // Create SqlDataAdapters for reading the RRUTemplates table.
                VS_DataSetTableAdapters.EtlRRUTemplatesTableAdapter rruTA =
                    new VS_DataSetTableAdapters.EtlRRUTemplatesTableAdapter();

                rruTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                // Read the RRUTemplates table and retrieve all of the different ModelNo templates.

                VS_DataSet.EtlRRUTemplatesDataTable rruDT = rruTA.GetData();

                modelNoStr = dr["ModelNo"].ToString();

                foreach (DataRow dr2 in rruDT)
                {
                    frmNewRRU.comboBoxNewRRUModelNo.Items.Add(dr2["ModelNo"].ToString());
                    if (modelNoStr.Equals(dr2["ModelNo"].ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                        modelNoLocInt = i;
                    }
                    ++i;
                }

                if (modelNoLocInt >= 0)
                {
                    frmNewRRU.comboBoxNewRRUModelNo.SelectedIndex = modelNoLocInt;
                    frmNewRRU.comboBoxNewRRUModelNo.Enabled = false;
                }
                else
                {
                    frmNewRRU.comboBoxNewRRUModelNo.Text = modelNoStr;
                }

                frmNewRRU.textBoxNewRRUPartNum.Text = dr["PartNum"].ToString();
                frmNewRRU.textBoxNewRRUSerialNo.Text = dr["SerialNo"].ToString();
                frmNewRRU.labelMewRRUCreatedBy.Visible = true;
                frmNewRRU.textBoxNewRRUVerifiedBy.Visible = true;
                frmNewRRU.textBoxNewRRUVerifiedBy.Text = dr["VerifiedBy"].ToString();
                
                htgTypeStr = dr["HeatingType"].ToString();
                if (htgTypeStr.Contains("Indirect"))
                {
                    htgTypeStr = "Indirect";
                }
                else if (htgTypeStr.Contains("Direct")) 
                {
                    htgTypeStr = "Direct";
                }
                else if (htgTypeStr.Contains("Electric"))
                {
                    htgTypeStr = "Electric";
                }
                else
                {
                    htgTypeStr = "";
                }

                if (htgTypeStr.Contains("Direct"))
                {
                    frmNewRRU.groupBoxNewRRUDirectFire.Location = new Point(6, 275);
                    frmNewRRU.groupBoxNewRRUDirectFire.Visible = true;
                    frmNewRRU.groupBoxNewRRUIndirectFire.Visible = false;
                    frmNewRRU.comboBoxNewRRUHeatingType.SelectedIndex = 0;
                    frmNewRRU.textBoxNewRRUDFEquiptedAirflow.Text = dr["DF_EquiptedAirFlow"].ToString();
                    frmNewRRU.textBoxNewRRUDFStaticPressure.Text = dr["DF_StaticPressure"].ToString();
                    frmNewRRU.textBoxNewRRUDFMaxHtgInputBTUH.Text = dr["DF_MaxHtgInputBTUH"].ToString();
                    frmNewRRU.textboxNewRRUDFMinHtgInput.Text = dr["DF_MinHtgInput"].ToString();
                    frmNewRRU.textBoxNewRRUDFTempRise.Text = dr["DF_TempRise"].ToString();
                    frmNewRRU.textBoxNewRRUDFMaxGasPressure.Text = dr["DF_MaxGasPressure"].ToString();
                    frmNewRRU.textBoxNewRRUDFMinGasPressure.Text = dr["DF_MinGasPressure"].ToString();
                    frmNewRRU.textBoxNewRRUDFMaxPressureDrop.Text = dr["DF_MaxPressureDrop"].ToString();
                    frmNewRRU.textBoxNewRRUDFMinPressureDrop.Text = dr["DF_MinPressureDrop"].ToString();
                    frmNewRRU.textBoxNewRRUDFManifoldPressure.Text = dr["DF_ManifoldPressure"].ToString();
                    frmNewRRU.textBoxNewRRUDFCutOutTemp.Text = dr["DF_CutOutTemp"].ToString();

                }
                else if (htgTypeStr.Contains("Indirect"))
                {
                    frmNewRRU.groupBoxNewRRUIndirectFire.Location = new Point(6, 275);
                    frmNewRRU.groupBoxNewRRUIndirectFire.Visible = true;
                    frmNewRRU.groupBoxNewRRUDirectFire.Visible = false;
                    frmNewRRU.comboBoxNewRRUHeatingType.SelectedIndex = 1;

                    heatFuelTypeStr = dr["HeatingType"].ToString();
                    if (heatFuelTypeStr.IndexOf("Liquid Propane") > 0)
                    {
                        frmNewRRU.comboBoxNewRRUIFHeatingFuelType.Text = "Liquid Propane";
                    }
                    else if (heatFuelTypeStr.IndexOf("Natural Gas") > 0)
                    {
                        frmNewRRU.comboBoxNewRRUIFHeatingFuelType.Text = "Natural Gas";
                    }

                    frmNewRRU.textBoxNewRRUIFHeatingInputBTUH.Text = dr["IN_HtgInputBTUH"].ToString();
                    frmNewRRU.textBoxNewRRUIFHeatingOutput.Text = dr["IN_HtgOutput"].ToString();
                    frmNewRRU.textBoxNewRRUIFMinInputBTU.Text = dr["IN_MinInputBTU"].ToString();
                    frmNewRRU.textBoxNewRRUIFMaxExt.Text = dr["IN_MaxExt"].ToString();
                    frmNewRRU.textBoxNewRRUIFTempRise.Text = dr["IN_TempRise"].ToString();
                    frmNewRRU.textBoxNewRRUIFMaxOutAirTemp.Text = dr["IN_MaxOutletAirTemp"].ToString();
                    frmNewRRU.textBoxNewRRUIFMaxGasPressure.Text = dr["IN_MaxGasPressure"].ToString();
                    frmNewRRU.textBoxNewRRUIFMinGasPressure.Text = dr["IN_MinGasPressure"].ToString();
                    frmNewRRU.textBoxNewRRUIFManifoldPressure.Text = dr["IN_ManifoldPressure"].ToString();
                    frmNewRRU.textBoxNewRRUIFMaxHtgInputBTUH.Text = dr["IN_MaxHtgInputBTUH"].ToString();
                }
                else if (htgTypeStr.Contains("Electric"))
                {
                    frmNewRRU.comboBoxNewRRUHeatingType.SelectedIndex = 2;
                }

                frmNewRRU.textBoxNewRRUFanCondQty.Text = dr["FanCondQty"].ToString();
                frmNewRRU.textBoxNewRRUFanCondPH.Text = dr["FanCondPH"].ToString();
                frmNewRRU.textBoxNewRRUFanCondFLA.Text = dr["FanCondFLA"].ToString();
                frmNewRRU.textBoxNewRRUFanCondHP.Text = dr["FanCondHP"].ToString();

                frmNewRRU.textBoxNewRRUFanStdQty.Text = dr["FanStdQty"].ToString();
                frmNewRRU.textBoxNewRRUFanStdPH.Text = dr["FanStdPH"].ToString();
                frmNewRRU.textBoxNewRRUFanStdFLA.Text = dr["FanStdFLA"].ToString();
                frmNewRRU.textBoxNewRRUFanStdHP.Text = dr["FanStdHP"].ToString();

                frmNewRRU.textBoxNewRRUFanOvrQty.Text = dr["FanOvrQty"].ToString();
                frmNewRRU.textBoxNewRRUFanOvrPH.Text = dr["FanOvrPH"].ToString();
                frmNewRRU.textBoxNewRRUFanOvrFLA.Text = dr["FanOvrFLA"].ToString();
                frmNewRRU.textBoxNewRRUFanOvrHP.Text = dr["FanOvrHP"].ToString();

                frmNewRRU.textBoxNewRRUComp1Qty.Text = dr["Compr1Qty"].ToString();
                frmNewRRU.textboxNewRRUComp1PH.Text = dr["Compr1PH"].ToString();
                frmNewRRU.textBoxNewRRUComp1RLAVolts.Text = dr["Compr1RLA_Volts"].ToString();
                frmNewRRU.textBoxNewRRUComp1LRA.Text = dr["Compr1LRA"].ToString();
                frmNewRRU.textBoxNewRRUComp1Charge.Text = dr["Compr1Charge"].ToString();

                frmNewRRU.textBoxNewRRUComp2Qty.Text = dr["Compr2Qty"].ToString();
                frmNewRRU.textBoxNewRRUComp2PH.Text = dr["Compr2PH"].ToString();
                frmNewRRU.textBoxNewRRUComp2RLAVolts.Text = dr["Compr2RLA_Volts"].ToString();
                frmNewRRU.textBoxNewRRUComp2LRA.Text = dr["Compr2LRA"].ToString();
                frmNewRRU.textBoxNewRRUComp2Charge.Text = dr["Compr2Charge"].ToString();

                frmNewRRU.textBoxNewRRUComp3Qty.Text = dr["Comp3Qty"].ToString() == "NA" ? "" : dr["Comp3Qty"].ToString();
                frmNewRRU.textBoxNewRRUComp3PH.Text = dr["Comp3PH"].ToString() == "NA" ? "" : dr["Comp3PH"].ToString();
                frmNewRRU.textBoxNewRRUComp3RLA.Text = dr["Comp3RLA_Volts"].ToString() == "NA" ? "" : dr["Comp3RLA_Volts"].ToString();
                frmNewRRU.textBoxNewRRUComp3LRA.Text = dr["Comp3LRA"].ToString() == "NA" ? "" : dr["Comp3LRA"].ToString();
                frmNewRRU.textBoxNewRRUComp3Charge.Text = dr["Comp3Charge"].ToString() == "NA" ? "" : dr["Comp3Charge"].ToString();       

                frmNewRRU.textBoxNewRRUMinCKTAmp.Text = dr["MinCKTAmp"].ToString();
                frmNewRRU.textBoxNewRRUMFSMCB.Text = dr["MFSMCB"].ToString();
                frmNewRRU.textBoxNewRRUMOP.Text = dr["MOP"].ToString();
                frmNewRRU.textBoxNewRRUElectricalRating.Text = dr["ElecRating"].ToString();
                frmNewRRU.textBoxNewRRUOperatingVolts.Text = dr["OperVoltage"].ToString();
                frmNewRRU.textBoxNewRRUHeatingType.Text = htgTypeStr;
                frmNewRRU.textBoxNewRRUHeatingInput.Text = dr["HeatingInput"].ToString();
                frmNewRRU.textBoxNewRRUTestPressureHigh.Text = dr["TestPressureHigh"].ToString();
                frmNewRRU.textBoxNewRRUTestPressureLow.Text = dr["TestPressureLow"].ToString();

                frmNewRRU.radioButtonNewRRUR410A.Checked = true;

                frenchStr = dr["French"].ToString();
                if (frenchStr.Equals("True", StringComparison.OrdinalIgnoreCase))
                {
                    frmNewRRU.checkBoxNewRRUFrench.Checked = true;
                }

                frmNewRRU.buttonNewRRUAccept.Text = "Update";
                frmNewRRU.buttonNewRRUPrint.Enabled = true;

                frmNewRRU.labelNewRRUMsg.Text = "Existing RRU order, if any updates are made then press the" +
                     "Update button to save the information.";
                
                frmNewRRU.ShowDialog();

            }
            catch
            {
                MessageBox.Show("ERROR reading the RRUTemplates table.");
            }

        }

        private void readOrderDtlRRU_PopRRUForm(VS_DataSet.OrderDtlRRUDataTable ordDtlRRUDT, 
                                                string endConsumerStr, string customerNameStr)
        {
            string modelNoStr;
            string htgTypeStr;
            string frenchStr;

            int modelNoLocInt = -1;
            int i = 0;

            frmNewLineRRU frmNewRRU = new frmNewLineRRU();

            DataRow dr = ordDtlRRUDT.Rows[0];

            GlobalModeType.ModeType = "Update";
            frmNewRRU.Size = new System.Drawing.Size(860, 710);
            frmNewRRU.groupBoxNewRRU.Size = new System.Drawing.Size(822, 633);

            frmNewRRU.Text = "Update RRU Sales Order# " + dr["OrderNum"].ToString();

            // Fill in all of the various info on the form.
            frmNewRRU.textBoxNewRRUOrderNum.Text = dr["OrderNum"].ToString();
          
            frmNewRRU.textBoxNewRRUEndConsumer.Text = endConsumerStr;
            frmNewRRU.textBoxNewRRUEndConsumer.Focus();
            frmNewRRU.textBoxNewRRUEndConsumer.Select();

            frmNewRRU.textBoxNewRRUCustomer.Text = customerNameStr;               
            frmNewRRU.textBoxNewRRUCustomer.Enabled = false;
            
            frmNewRRU.textBoxNewRRULineItem.Text = dr["OrderLine"].ToString();
            frmNewRRU.textBoxNewRRUReleaseNum.Text = dr["ReleaseNum"].ToString();

            try
            {
                // Create SqlDataAdapters for reading the RRUTemplates table.
                VS_DataSetTableAdapters.RRUTemplatesTableAdapter rruTA =
                    new VS_DataSetTableAdapters.RRUTemplatesTableAdapter();

                rruTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                // Read the RRUTemplates table and retrieve all of the different ModelNo templates.

                VS_DataSet.RRUTemplatesDataTable rruDT = rruTA.GetData();

                modelNoStr = dr["ModelNo"].ToString();

                foreach (DataRow dr2 in rruDT)
                {
                    frmNewRRU.comboBoxNewRRUModelNo.Items.Add(dr2["ModelNo"].ToString());
                    if (modelNoStr.Equals(dr["ModelNo"].ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                        modelNoLocInt = i;
                    }
                    ++i;
                }

                if (modelNoLocInt >= 0)
                {
                    frmNewRRU.comboBoxNewRRUModelNo.SelectedIndex = modelNoLocInt;
                    frmNewRRU.comboBoxNewRRUModelNo.Enabled = false;
                }

                frmNewRRU.textBoxNewRRUPartNum.Text = dr["PartNum"].ToString();
                frmNewRRU.textBoxNewRRUSerialNo.Text = dr["SerialNo"].ToString();
                frmNewRRU.labelMewRRUCreatedBy.Visible = true;
                frmNewRRU.textBoxNewRRUVerifiedBy.Visible = true;
                frmNewRRU.textBoxNewRRUVerifiedBy.Text = dr["VerifiedBy"].ToString();

                htgTypeStr = dr["HeatingType"].ToString();

                if (htgTypeStr.Equals("Direct", StringComparison.OrdinalIgnoreCase))
                {
                    frmNewRRU.groupBoxNewRRUDirectFire.Location = new Point(6, 275);
                    frmNewRRU.groupBoxNewRRUDirectFire.Visible = true;
                    frmNewRRU.groupBoxNewRRUIndirectFire.Visible = false;
                    frmNewRRU.comboBoxNewRRUHeatingType.SelectedIndex = 0;
                    frmNewRRU.textBoxNewRRUDFEquiptedAirflow.Text = dr["DF_EquiptedAirFlow"].ToString();
                    frmNewRRU.textBoxNewRRUDFStaticPressure.Text = dr["DF_StaticPressure"].ToString();
                    frmNewRRU.textBoxNewRRUDFMaxHtgInputBTUH.Text = dr["DF_MaxHtgInputBTUH"].ToString();
                    frmNewRRU.textboxNewRRUDFMinHtgInput.Text = dr["DF_MinHtgInput"].ToString();
                    frmNewRRU.textBoxNewRRUDFTempRise.Text = dr["DF_TempRise"].ToString();
                    frmNewRRU.textBoxNewRRUDFMaxGasPressure.Text = dr["DF_MaxGasPressure"].ToString();
                    frmNewRRU.textBoxNewRRUDFMinGasPressure.Text = dr["DF_MinGasPressure"].ToString();
                    frmNewRRU.textBoxNewRRUDFMaxPressureDrop.Text = dr["DF_MaxPressureDrop"].ToString();
                    frmNewRRU.textBoxNewRRUDFMinPressureDrop.Text = dr["DF_MinPressureDrop"].ToString();
                    frmNewRRU.textBoxNewRRUDFManifoldPressure.Text = dr["DF_ManifoldPressure"].ToString();
                    frmNewRRU.textBoxNewRRUDFCutOutTemp.Text = dr["DF_CutOutTemp"].ToString();

                }
                else if (htgTypeStr.Equals("Indirect", StringComparison.OrdinalIgnoreCase))
                {
                    frmNewRRU.groupBoxNewRRUIndirectFire.Location = new Point(6, 275);
                    frmNewRRU.groupBoxNewRRUIndirectFire.Visible = true;
                    frmNewRRU.groupBoxNewRRUDirectFire.Visible = false;
                    frmNewRRU.comboBoxNewRRUHeatingType.SelectedIndex = 1;
                    frmNewRRU.textBoxNewRRUIFHeatingInputBTUH.Text = dr["IN_HtgInputBTUH"].ToString();
                    frmNewRRU.textBoxNewRRUIFHeatingOutput.Text = dr["IN_HtgOutput"].ToString();
                    frmNewRRU.textBoxNewRRUIFMinInputBTU.Text = dr["IN_MinInputBTU"].ToString();
                    frmNewRRU.textBoxNewRRUIFMaxExt.Text = dr["IN_MaxExt"].ToString();
                    frmNewRRU.textBoxNewRRUIFTempRise.Text = dr["IN_TempRise"].ToString();
                    frmNewRRU.textBoxNewRRUIFMaxOutAirTemp.Text = dr["IN_MaxOutletAirTemp"].ToString();
                    frmNewRRU.textBoxNewRRUIFMaxGasPressure.Text = dr["IN_MaxGasPressure"].ToString();
                    frmNewRRU.textBoxNewRRUIFMinGasPressure.Text = dr["IN_MinGasPressure"].ToString();
                    frmNewRRU.textBoxNewRRUIFManifoldPressure.Text = dr["IN_ManifoldPressure"].ToString();
                    frmNewRRU.textBoxNewRRUIFMaxHtgInputBTUH.Text = dr["IN_MaxHtgInputBTUH"].ToString();
                }
                frmNewRRU.textBoxNewRRUFanCondQty.Text = dr["FanCondQty"].ToString();
                frmNewRRU.textBoxNewRRUFanCondPH.Text = dr["FanCondPH"].ToString();
                frmNewRRU.textBoxNewRRUFanCondFLA.Text = dr["FanCondFLA"].ToString();
                frmNewRRU.textBoxNewRRUFanCondHP.Text = dr["FanCondHP"].ToString();

                frmNewRRU.textBoxNewRRUFanStdQty.Text = dr["FanStdQty"].ToString();
                frmNewRRU.textBoxNewRRUFanStdPH.Text = dr["FanStdPH"].ToString();
                frmNewRRU.textBoxNewRRUFanStdFLA.Text = dr["FanStdFLA"].ToString();
                frmNewRRU.textBoxNewRRUFanStdHP.Text = dr["FanStdHP"].ToString();

                frmNewRRU.textBoxNewRRUFanOvrQty.Text = dr["FanOvrQty"].ToString();
                frmNewRRU.textBoxNewRRUFanOvrPH.Text = dr["FanOvrPH"].ToString();
                frmNewRRU.textBoxNewRRUFanOvrFLA.Text = dr["FanOvrFLA"].ToString();
                frmNewRRU.textBoxNewRRUFanOvrHP.Text = dr["FanOvrHP"].ToString();

                frmNewRRU.textBoxNewRRUComp1Qty.Text = dr["Comp1Qty"].ToString();
                frmNewRRU.textboxNewRRUComp1PH.Text = dr["Comp1PH"].ToString();
                frmNewRRU.textBoxNewRRUComp1RLAVolts.Text = dr["Comp1RLA_Volts"].ToString();
                frmNewRRU.textBoxNewRRUComp1LRA.Text = dr["Comp1LRA"].ToString();
                frmNewRRU.textBoxNewRRUComp1Charge.Text = dr["Comp1Charge"].ToString();

                frmNewRRU.textBoxNewRRUComp2Qty.Text = dr["Comp2Qty"].ToString();
                frmNewRRU.textBoxNewRRUComp2PH.Text = dr["Comp2PH"].ToString();
                frmNewRRU.textBoxNewRRUComp2RLAVolts.Text = dr["Comp2RLA_Volts"].ToString();
                frmNewRRU.textBoxNewRRUComp2LRA.Text = dr["Comp2LRA"].ToString();
                frmNewRRU.textBoxNewRRUComp2Charge.Text = dr["Comp2Charge"].ToString();

                frmNewRRU.textBoxNewRRUMinCKTAmp.Text = dr["MinCKTAmp"].ToString();
                frmNewRRU.textBoxNewRRUMFSMCB.Text = dr["MFSMCB"].ToString();
                frmNewRRU.textBoxNewRRUMOP.Text = dr["MOP"].ToString();
                frmNewRRU.textBoxNewRRUElectricalRating.Text = dr["ElecRating"].ToString();
                frmNewRRU.textBoxNewRRUOperatingVolts.Text = dr["OperVoltage"].ToString();
                frmNewRRU.textBoxNewRRUHeatingType.Text = dr["HeatingType"].ToString();
                frmNewRRU.textBoxNewRRUHeatingInput.Text = dr["HeatingInput"].ToString();
                frmNewRRU.textBoxNewRRUTestPressureHigh.Text = dr["TestPressureHigh"].ToString();
                frmNewRRU.textBoxNewRRUTestPressureLow.Text = dr["TestPressureLow"].ToString();

                frmNewRRU.radioButtonNewRRUR410A.Checked = true;

                frenchStr = dr["French"].ToString();
                if (frenchStr.Equals("True", StringComparison.OrdinalIgnoreCase))
                {
                    frmNewRRU.checkBoxNewRRUFrench.Checked = true;
                }

                frmNewRRU.buttonNewRRUAccept.Text = "Update";
                frmNewRRU.buttonNewRRUPrint.Enabled = true;

                frmNewRRU.labelNewRRUMsg.Text = "Existing RRU order, if any updates are made then press the" +
                     "Update button to save the information.";

                frmNewRRU.ShowDialog();

            }
            catch
            {
                MessageBox.Show("ERROR reading the RRUTemplates table.");
            }

        }

        private void populate_frmNewLineFAN(VS_DataSet.EtlFANDataTable etlFANDT, 
                                            string endConsumerStr, string customerNameStr)
        {
            frmNewLineFAN frmNewFAN = new frmNewLineFAN();

            DataRow dr = etlFANDT.Rows[0];

            GlobalModeType.ModeType = "Update";

            frmNewFAN.Text = "Update FAN Job # " + dr["JobNum"].ToString();

            // Fill in all of the various info on the form.
            frmNewFAN.textBoxNewFANJobNum.Text = dr["JobNum"].ToString();
            frmNewFAN.textBoxNewFANOrderNum.Text = dr["OrderNum"].ToString();

            frmNewFAN.textBoxNewFANEndConsumer.Text = endConsumerStr;
            frmNewFAN.textBoxNewFANEndConsumer.Focus();
            frmNewFAN.textBoxNewFANEndConsumer.Select();

            frmNewFAN.textBoxNewFANCustomer.Text = customerNameStr;
            frmNewFAN.textBoxNewFANCustomer.Enabled = false;            

            frmNewFAN.textBoxNewFANLineItem.Text = dr["OrderLine"].ToString();
            frmNewFAN.textBoxNewFANReleaseNum.Text = dr["ReleaseNum"].ToString();
            frmNewFAN.textBoxNewFANModelNo.Text = dr["ModelNum"].ToString();
            frmNewFAN.textBoxNewFANModelNo.Enabled = false;
            frmNewFAN.textBoxNewFANPartNum.Text = dr["PartNum"].ToString();
            frmNewFAN.textBoxNewFANSerialNo.Text = dr["SerialNum"].ToString();

            frmNewFAN.txtTagNum.Text = dr["TagNum"].ToString();
            frmNewFAN.txtTag.Text = dr["Tag"].ToString();

            frmNewFAN.textBoxNewFANElecBlowerFan.Text = dr["ElecBlowerFan"].ToString();
            frmNewFAN.textBoxNewFANElecVolts.Text = dr["ElecVolts"].ToString();
            frmNewFAN.textBoxNewFANElecPhase.Text = dr["ElecPhase"].ToString();
            frmNewFAN.textBoxNewFANElecHertz.Text = dr["ElecHertz"].ToString();
            frmNewFAN.textBoxNewFANElecFLA.Text = dr["ElecFLA"].ToString();
            frmNewFAN.textBoxNewFANElecMinVoltage.Text = dr["ElecMinVoltage"].ToString();
            frmNewFAN.textBoxNewFANElecMaxVoltage.Text = dr["ElecMaxVoltage"].ToString();
            frmNewFAN.textBoxNewFANElecMaxVoltageHertz.Text = dr["ElecMaxVoltageHertz"].ToString();
            frmNewFAN.textBoxNewFANAirCFM.Text = dr["AirCFM"].ToString();
            frmNewFAN.textBoxNewFANAirESP.Text = dr["AirESP"].ToString();
            frmNewFAN.textBoxNewFANAirRPM.Text = dr["AirRPM"].ToString();

            frmNewFAN.buttonNewFANAccept.Text = "Update";
            frmNewFAN.buttonNewFANPrint.Enabled = true;

            frmNewFAN.labelNewFANMsg.Text = "Existing Fan job, if any updates are made then press the";
            frmNewFAN.labelNewFANMsg2.Text = "Update button to save the information.";
            frmNewFAN.ShowDialog();

        }

        private void populate_frmNewLineOAU(string jobNumStr, string endConsumerStr, string customerNameStr, bool jobFound)
        {
            //string heatingTypeStr;
            string voltageStr;           
            string fuelTypeStr = "";
            string mfgDateStr = "";
            
            //bool jobFound = false;

            DateTime mfgDateDate;

            frmNewLineOAU frmNewOAU = new frmNewLineOAU();
            
            objMain.JobNum = jobNumStr;

            //try
            //{
            //    jobFound = objMain.GetEtlOAUData();                
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}

            GlobalModeType.ModeType = "PreUpdate";

            string modelNum = objMain.ModelNo;

            if (jobFound)
            {
                frmNewOAU.Text = "Update OAU Job # " + objMain.JobNum;

                // Fill in all of the various info on the form.
                frmNewOAU.textBoxNewOAUJobNum.Text = objMain.JobNum;
                frmNewOAU.textBoxNewOAUOrderNum.Text = objMain.OrderNum;

                frmNewOAU.textBoxNewOAUEndConsumer.Text = endConsumerStr;
                frmNewOAU.textBoxNewOAUEndConsumer.Focus();
                frmNewOAU.textBoxNewOAUEndConsumer.Select();
                frmNewOAU.textBoxNewOAUCustomer.Text = customerNameStr;
                frmNewOAU.textBoxNewOAUCustomer.Enabled = false;

                frmNewOAU.textBoxNewOAULineItem.Text = objMain.OrderLine;
                frmNewOAU.textBoxNewOAUReleaseNum.Text = objMain.ReleaseNum;
                frmNewOAU.textBoxNewOAUModelNo.Text = objMain.ModelNo;
                frmNewOAU.textBoxNewOAUSerialNo.Text = objMain.SerialNo;
                frmNewOAU.textBoxNewOAUModelNo.Enabled = false;


                 if (objMain.ModelNo.StartsWith("HA") == true)
                 {
                     //frmNewOAU.textBoxNewOAUModelNo.Enabled = true;
                     DataTable dtSrl = objMain.GetUnitSerialNoData();
                     if (dtSrl.Rows.Count > 0)
                     {
                         DataRow drSrl = dtSrl.Rows[0];
                         frmNewOAU.textBoxNewOAUSerialNo.Text = drSrl["SerialNumber"].ToString();
                     }
                     else
                     {
                         frmNewOAU.textBoxNewOAUSerialNo.Text = "HA" + objMain.JobNum;
                     }
                     frmNewOAU.textBoxNewOAUSerialNo.Enabled = true;
                 }
                
               
                
                frmNewOAU.textBoxNewOAUVerifiedBy.Visible = true;
                frmNewOAU.checkBoxNewOAUVerified.Visible = true;
                frmNewOAU.checkBoxNewOAUVerified.Checked = true;
                frmNewOAU.textBoxNewOAUVerifiedBy.Text = objMain.ModelNoVerifiedBy;

                //heatingTypeStr = objMain.HeatingType;

                frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex = ETLHelper.GetHeatingTypeIndex(modelNum);

                /*
                if (heatingTypeStr.Equals("Indirect", StringComparison.OrdinalIgnoreCase))
                {
                    frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex = 0;
                }
                else if (heatingTypeStr.Equals("Electric", StringComparison.OrdinalIgnoreCase))
                {
                    frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex = 1;
                }
                else if (heatingTypeStr.Equals("NoHeat", StringComparison.OrdinalIgnoreCase))
                {
                    frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex = 2;
                }
                else if (heatingTypeStr.Equals("Direct", StringComparison.OrdinalIgnoreCase))
                {
                    frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex = 3;
                }
                else // Heating Type equals HotWater
                {
                    frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex = 4;
                }
                */

                voltageStr = objMain.Voltage;

                if (voltageStr == "208")
                {
                    frmNewOAU.comboBoxNewOAUVoltage.SelectedIndex = 0;
                }
                if (voltageStr == "230")
                {
                    frmNewOAU.comboBoxNewOAUVoltage.SelectedIndex = 1;
                }
                else if (voltageStr == "460")
                {
                    frmNewOAU.comboBoxNewOAUVoltage.SelectedIndex = 2;
                }
                else // Voltage equals 575.
                {
                    frmNewOAU.comboBoxNewOAUVoltage.SelectedIndex = 3;
                }

                if (frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex == 0 || frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex == 6) // Indirect Fired or Dual Fuel (Indirect Fired/Electric)
                {
                    frmNewOAU.textBoxNewOAUIndFireMaxHtgInput.Text = objMain.IN_MaxHtgInputBTUH;
                    frmNewOAU.textBoxNewOAUIndFireHeatingOutput.Text = objMain.IN_HeatingOutputBTUH;
                    frmNewOAU.textBoxNewOauIndFireMinInputBTU.Text = objMain.IN_MinInputBTU;
                    frmNewOAU.textBoxNewOauIndFireMaxExt.Text = objMain.IN_MaxExt;
                    frmNewOAU.textBoxNewOAUIndFireTempRise.Text = objMain.IN_TempRise;
                    frmNewOAU.textBoxNewOAUIndFireMaxOutAirTemp.Text = objMain.IN_MaxOutAirTemp;
                    frmNewOAU.textBoxNewOAUIndFireMaxGasPressure.Text = objMain.IN_MaxGasPressure;
                    frmNewOAU.textBoxNewOAUIndFireMinGasPressure.Text = objMain.IN_MinGasPressure;
                    frmNewOAU.textBoxNewOAUIndFireManifoldPressure.Text = objMain.IN_ManifoldPressure;

                    fuelTypeStr = objMain.FuelType;

                    if (fuelTypeStr == "Natural Gas")
                    {
                        frmNewOAU.comboBoxNewOAUIFFuelType.SelectedIndex = 1;
                    }
                    else if (fuelTypeStr.Contains("Propane"))
                    {
                        frmNewOAU.comboBoxNewOAUIFFuelType.SelectedIndex = 2;
                    }
                    else
                    {
                        frmNewOAU.comboBoxNewOAUIFFuelType.SelectedIndex = 0;
                    }

                    if (frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex == 6)
                    {
                        frmNewOAU.textBoxNewOAUIndFireSecHtgInput.Text = objMain.SecondaryHtgInput;
                        frmNewOAU.textBoxNewOAUIndFireSecHtgInput.Visible = true;
                        frmNewOAU.lblNewOAUIndFireHtgInput.Visible = true;
                    }
                }
                else if (frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex == 1 || frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex == 7) // Electric, Electric SCR  or Dual Fuel (Electric/Electric)
                {
                    frmNewOAU.textBoxNewOAUElecHeatingInputElec.Text = objMain.HeatingInputElectric;
                    frmNewOAU.textBoxNewOAUElecMaxAirOutletTemp.Text = objMain.IN_MaxOutAirTemp;

                    if (frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex == 7)
                    {
                        frmNewOAU.txtNewOAUElecSecHtgInput.Text = objMain.SecondaryHtgInput;
                        frmNewOAU.txtNewOAUElecSecHtgInput.Visible = true;
                        frmNewOAU.lblNewOAUElecSecHtgInput.Visible = true;
                    }
                    frmNewOAU.groupBoxNewOAUElectric.Location = new Point(6, 346);
                    frmNewOAU.groupBoxNewOAUElectric.Visible = true;
                    frmNewOAU.groupBoxNewOAUElectric.BringToFront();
                }
                else if (frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex == 3) // Direct Fired
                {
                    frmNewOAU.txtNewOAU_DF_MaxHtgInputBTU.Text = objMain.DF_MaxHtgInputBTUH;
                    frmNewOAU.txtNewOAU_DF_MinHtgInputBTU.Text = objMain.DF_MinHeatingInput;
                    frmNewOAU.txtNewOAU_DF_TempRise.Text = objMain.DF_TempRise;
                    frmNewOAU.txtNewOAU_DF_MaxGasPressure.Text = objMain.DF_MaxGasPressure;
                    frmNewOAU.txtNewOAU_DF_MinGasPressure.Text = objMain.DF_MinGasPressure;
                    frmNewOAU.txtNewOAU_DF_AirFlowRate.Text = objMain.FlowRate;
                    frmNewOAU.txtNewOAU_DF_StaticPressure.Text = objMain.DF_StaticPressure;
                    frmNewOAU.txtNewOAU_DF_ManifoldPressure.Text = objMain.DF_ManifoldPressure;
                    frmNewOAU.txtNewOAU_DF_MaxPressureDrop.Text = objMain.DF_MaxPressureDrop;
                    frmNewOAU.txtNewOAU_DF_MinPressureDrop.Text = objMain.DF_MinPressureDrop;
                    frmNewOAU.txtNewOAU_DF_CutoutTemp.Text = objMain.DF_CutoutTemp;

                    fuelTypeStr = objMain.HeatingInputElectric;
                    if (fuelTypeStr == "Natural Gas")
                    {
                        frmNewOAU.comboBoxNewOAUIFFuelType.SelectedIndex = 1;
                    }
                    else if (fuelTypeStr == "Liquid Propane")
                    {
                        frmNewOAU.comboBoxNewOAUIFFuelType.SelectedIndex = 2;
                    }
                    else
                    {
                        frmNewOAU.comboBoxNewOAUIFFuelType.SelectedIndex = 0;
                    }
                }
                else if (frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex == 4 || frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex == 9) // Hot Water or Dual Fuel (HotWater/Elec)
                {
                    if (frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex == 9)
                    {
                        frmNewOAU.textBoxNewOAUElecHeatingInputElec.Visible = false;
                        frmNewOAU.labelNewOAUElecHeatingInput.Visible = false;
                        frmNewOAU.txtNewOAUElecSecHtgInput.Text = objMain.SecondaryHtgInput;
                        frmNewOAU.txtNewOAUElecSecHtgInput.Visible = true;
                        frmNewOAU.lblNewOAUElecSecHtgInput.Visible = true;

                        frmNewOAU.groupBoxNewOAUElectric.Location = new Point(6, 436);
                        frmNewOAU.groupBoxNewOAUElectric.Visible = true;
                        frmNewOAU.groupBoxNewOAUElectric.BringToFront();
                    }

                    frmNewOAU.textBoxHotWaterEnteringWaterTemp.Text = objMain.EnteringTemp;
                    frmNewOAU.textBoxHotWaterFlowRate.Text = objMain.FlowRate;
                    frmNewOAU.groupBoxNewOAUHotWater.Visible = true;
                    frmNewOAU.groupBoxNewOAUHotWater.BringToFront();
                    frmNewOAU.textBoxHotWaterFlowRate.Focus();
                    frmNewOAU.textBoxHotWaterFlowRate.Select();
                }
                else if (frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex == 5) // Steam
                {
                    frmNewOAU.textBoxNewOAUSteamOperatingPressure.Text = objMain.OperatingPressure;
                    frmNewOAU.textBoxNewOAUSteamCondensateFlowRate.Text = objMain.FlowRate;
                    frmNewOAU.groupBoxNewOAUSteam.Visible = true;
                    frmNewOAU.groupBoxNewOAUSteam.BringToFront();
                }
                else if (frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex == 6) // Steam
                {
                    //frmNewOAU.textBoxNewOAUSteamOperatingPressure.Text = objMain.OperatingPressure;
                    //frmNewOAU.textBoxNewOAUSteamCondensateFlowRate.Text = objMain.FlowRate;
                    //frmNewOAU.groupBoxNewOAUSteam.Visible = true;
                    //frmNewOAU.groupBoxNewOAUSteam.BringToFront();
                }
                else if (frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex == 7) // Steam
                {
                    //frmNewOAU.textBoxNewOAUSteamOperatingPressure.Text = objMain.OperatingPressure;
                    //frmNewOAU.textBoxNewOAUSteamCondensateFlowRate.Text = objMain.FlowRate;
                    //frmNewOAU.groupBoxNewOAUSteam.Visible = true;
                    //frmNewOAU.groupBoxNewOAUSteam.BringToFront();
                }
                else if (frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex == 8) // Steam
                {
                    //frmNewOAU.textBoxNewOAUSteamOperatingPressure.Text = objMain.OperatingPressure;
                    //frmNewOAU.textBoxNewOAUSteamCondensateFlowRate.Text = objMain.FlowRate;
                    //frmNewOAU.groupBoxNewOAUSteam.Visible = true;
                    //frmNewOAU.groupBoxNewOAUSteam.BringToFront();
                }
                else if (frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex == 9) // Steam
                {
                    //frmNewOAU.textBoxNewOAUSteamOperatingPressure.Text = objMain.OperatingPressure;
                    //frmNewOAU.textBoxNewOAUSteamCondensateFlowRate.Text = objMain.FlowRate;
                    //frmNewOAU.groupBoxNewOAUSteam.Visible = true;
                    //frmNewOAU.groupBoxNewOAUSteam.BringToFront();
                }
                else if (frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex == 10) // Steam
                {
                    //frmNewOAU.textBoxNewOAUSteamOperatingPressure.Text = objMain.OperatingPressure;
                    //frmNewOAU.textBoxNewOAUSteamCondensateFlowRate.Text = objMain.FlowRate;
                    frmNewOAU.groupBoxNewOAUElectric.Visible = true;
                    frmNewOAU.groupBoxNewOAUElectric.BringToFront();
                    frmNewOAU.labelNewOAUElecHeatingInput.Visible = false;
                    frmNewOAU.textBoxNewOAUElecHeatingInputElec.Visible = false;
                    frmNewOAU.lblNewOAUElecSecHtgInput.Visible = true;
                    frmNewOAU.txtNewOAUElecSecHtgInput.Visible = true;
                    frmNewOAU.txtNewOAUElecSecHtgInput.Text = objMain.SecondaryHtgInput;
                }
                else if (frmNewOAU.comboBoxNewOAUHeatingType.SelectedIndex == 2) // NoHeat
                {
                    frmNewOAU.textBoxNewOauIndFireMaxExt.Text = objMain.IN_MaxExt;
                }

                if (modelNum.Substring(13, 1) == "3" || modelNum.Substring(13, 1) == "8")
                {
                    frmNewOAU.groupBoxNewOAUWater_Glycol.Visible = true;

                    if (objMain.WaterGlycol == "Water")
                    {
                        frmNewOAU.radioButtonWater.Checked = true;
                    }
                    else if (objMain.WaterGlycol == "Glycol")
                    {
                        frmNewOAU.radioButtonGlycol.Checked = true;
                    }
                }

                // Compressors and charge data
                frmNewOAU.txtComp1Qty.Text = objMain.Comp1Qty;
                frmNewOAU.txtComp1Phase.Text = objMain.Comp1Phase;
                frmNewOAU.txtComp1RLA_Volts.Text = objMain.Comp1RLA_Volts;
                frmNewOAU.txtComp1LRA.Text = objMain.Comp1LRA;
                frmNewOAU.txtCircuit1Charge.Text = objMain.Circuit1Charge;

                frmNewOAU.txtComp2Qty.Text = objMain.Comp2Qty;
                frmNewOAU.txtComp2Phase.Text = objMain.Comp2Phase;
                frmNewOAU.txtComp2RLA_Volts.Text = objMain.Comp2RLA_Volts;
                frmNewOAU.txtComp2LRA.Text = objMain.Comp2LRA;

                frmNewOAU.txtComp3Qty.Text = objMain.Comp3Qty;
                frmNewOAU.txtComp3Phase.Text = objMain.Comp3Phase;
                frmNewOAU.txtComp3RLA_Volts.Text = objMain.Comp3RLA_Volts;
                frmNewOAU.txtComp3LRA.Text = objMain.Comp3LRA;

                frmNewOAU.txtComp4Qty.Text = objMain.Comp4Qty;
                frmNewOAU.txtComp4Phase.Text = objMain.Comp4Phase;
                frmNewOAU.txtComp4RLA_Volts.Text = objMain.Comp4RLA_Volts;
                frmNewOAU.txtComp4LRA.Text = objMain.Comp4LRA;
                frmNewOAU.txtCircuit2Charge.Text = objMain.Circuit2Charge;

                frmNewOAU.txtComp5Qty.Text = objMain.Comp5Qty;
                frmNewOAU.txtComp5Phase.Text = objMain.Comp5Phase;
                frmNewOAU.txtComp5RLA_Volts.Text = objMain.Comp5RLA_Volts;
                frmNewOAU.txtComp5LRA.Text = objMain.Comp5LRA;

                frmNewOAU.txtComp6Qty.Text = objMain.Comp6Qty;
                frmNewOAU.txtComp6Phase.Text = objMain.Comp6Phase;
                frmNewOAU.txtComp6RLA_Volts.Text = objMain.Comp6RLA_Volts;
                frmNewOAU.txtComp6LRA.Text = objMain.Comp6LRA;

                if (objMain.FanCondQty != "" && Int32.Parse(objMain.FanCondQty) > 0)
                {
                    frmNewOAU.textBoxNewOAUFanCondQty.Text = objMain.FanCondQty;
                    frmNewOAU.textBoxNewOAUFanCondPH.Text = objMain.FanCondPhase;
                    frmNewOAU.textBoxNewOAUFanCondFLA.Text = objMain.FanCondFLA;
                    frmNewOAU.textBoxNewOAUFanCondHP.Text = objMain.FanCondHP;
                }
                else
                {
                    frmNewOAU.textBoxNewOAUFanCondQty.Text = "";
                    frmNewOAU.textBoxNewOAUFanCondPH.Text = "";
                    frmNewOAU.textBoxNewOAUFanCondFLA.Text = "";
                    frmNewOAU.textBoxNewOAUFanCondHP.Text = "";
                }

                if (objMain.FanErvQty != "" && Int32.Parse(objMain.FanErvQty) > 0)
                {
                    frmNewOAU.textBoxNewOAUFanEnergyQty.Text = objMain.FanErvQty;
                    frmNewOAU.textBoxNewOAUFanEnergyPH.Text = objMain.FanErvPhase;
                    frmNewOAU.textBoxNewOAUFanEnergyFLA.Text = objMain.FanErvFLA;
                    frmNewOAU.textBoxNewOAUFanEnergyHP.Text = objMain.FanErvHP;
                }
                else
                {
                    frmNewOAU.textBoxNewOAUFanEnergyQty.Text = "";
                    frmNewOAU.textBoxNewOAUFanEnergyPH.Text = "";
                    frmNewOAU.textBoxNewOAUFanEnergyFLA.Text = "";
                    frmNewOAU.textBoxNewOAUFanEnergyHP.Text = "";
                }

                if (objMain.FanEvapQty != "" && Int32.Parse(objMain.FanEvapQty) > 0)
                {
                    frmNewOAU.textBoxNewOAUFanEvapQty.Text = objMain.FanEvapQty;
                    frmNewOAU.textBoxNewOAUFanEvapPH.Text = objMain.FanEvapPhase;
                    frmNewOAU.textBoxNewOAUFanEvapFLA.Text = objMain.FanEvapFLA;
                    frmNewOAU.textBoxNewOAUFanEvapHP.Text = objMain.FanEvapHP;
                }
                else
                {
                    frmNewOAU.textBoxNewOAUFanEvapQty.Text = "";
                    frmNewOAU.textBoxNewOAUFanEvapPH.Text = "";
                    frmNewOAU.textBoxNewOAUFanEvapFLA.Text = "";
                    frmNewOAU.textBoxNewOAUFanEvapHP.Text = "";
                }

                if (objMain.FanPwrExhQty != "" && Int32.Parse(objMain.FanPwrExhQty) > 0)
                {
                    frmNewOAU.textBoxNewOAUFanPwrQty.Text = objMain.FanPwrExhQty;
                    frmNewOAU.textBoxNewOAUFanPwrPH.Text = objMain.FanPwrExhPhase;
                    frmNewOAU.textBoxNewOAUFanPwrFLA.Text = objMain.FanPwrExhFLA;
                    frmNewOAU.textBoxNewOAUFanPwrHP.Text = objMain.FanPwrExhHP;
                }
                else
                {
                    frmNewOAU.textBoxNewOAUFanPwrQty.Text = "";
                    frmNewOAU.textBoxNewOAUFanPwrPH.Text = "";
                    frmNewOAU.textBoxNewOAUFanPwrFLA.Text = "";
                    frmNewOAU.textBoxNewOAUFanPwrHP.Text = "";
                }

                frmNewOAU.textBoxNewOAUMinCKTAmp2.Text = objMain.MinCKTAmp2;
                frmNewOAU.textBoxNewOAUMFSMCB2.Text = objMain.MFSMCB2;

                if (objMain.DualPointPower == "1")
                {
                    frmNewOAU.checkBoxNewOAUDPP.Checked = true;
                }
                else
                {
                    frmNewOAU.checkBoxNewOAUDPP.Checked = false;

                    frmNewOAU.labelNewOAUDPP2.ForeColor = Color.Pink;

                    frmNewOAU.textBoxNewOAUMinCKTAmp2.BackColor = Color.Pink;
                    frmNewOAU.textBoxNewOAUMFSMCB2.BackColor = Color.Pink;

                    frmNewOAU.textBoxNewOAUMinCKTAmp2.ReadOnly = true;
                    frmNewOAU.textBoxNewOAUMFSMCB2.ReadOnly = true;
                }

                frmNewOAU.textBoxNewOAUMinCKTAmp.Text = objMain.MinCKTAmp;
                frmNewOAU.textBoxNewOAUMFSMCB.Text = objMain.MFSMCB;
                frmNewOAU.textBoxNewOAUMOP.Text = objMain.MOP;

                //if (objMain.SCCR == "5" || objMain.SCCR == "65")
                //    frmNewOAU.cboSCCR.Text = objMain.SCCR;
                //else
                //    frmNewOAU.cboSCCR.Text = "";           

                if (modelNum.Length == 39)
                {
                    if (modelNum.Substring(35, 1) == "X")
                    {
                        frmNewOAU.txtSCCR.Enabled = true;
                    }
                    else
                    {
                        if (modelNum.Substring(0, 3) == "OAB" || modelNum.Substring(0, 3) == "OAG")
                        {
                            if (modelNum.Substring(35, 1) == "G" || modelNum.Substring(35, 1) == "H" ||
                                modelNum.Substring(35, 1) == "N")
                            {
                                frmNewOAU.txtSCCR.Text = "65";
                            }
                            else
                            {
                                frmNewOAU.txtSCCR.Text = "5";
                            }
                        }
                        else
                        {
                            if (modelNum.Substring(35, 1) == "F" || modelNum.Substring(35, 1) == "G" ||
                                modelNum.Substring(35, 1) == "M")
                            {
                                frmNewOAU.txtSCCR.Text = "65";
                            }
                            else
                            {
                                frmNewOAU.txtSCCR.Text = "5";
                            }
                        }
                    }
                }
                else
                {
                    if (modelNum.Substring(40, 1) == "X")
                    {
                        frmNewOAU.txtSCCR.Enabled = true;
                    }
                    else
                    {
                        if (modelNum.Substring(40, 1) == "C" || modelNum.Substring(40, 1) == "D" ||
                            modelNum.Substring(40, 1) == "J")
                        {
                            frmNewOAU.txtSCCR.Text = "65";
                        }
                        else
                        {
                            frmNewOAU.txtSCCR.Text = "5";
                        }
                    }
                }

                frmNewOAU.textBoxNewOAUElectricalRating.Text = objMain.ElecRating;
                frmNewOAU.textBoxNewOAUOperatingVolts.Text = objMain.OperVoltage;
                frmNewOAU.textBoxNewOAUTestPressureHigh.Text = objMain.TestPressureHigh;
                frmNewOAU.textBoxNewOAUTestPressureLow.Text = objMain.TestPressureLow;

                mfgDateStr = objMain.MfgDate;
                if (mfgDateStr != "")
                {
                    mfgDateDate = Convert.ToDateTime(mfgDateStr);
                    frmNewOAU.dateTimePickerOAUMfgDate.Value = mfgDateDate;
                }

                if (objMain.French == "True")
                {
                    frmNewOAU.checkBoxLangFrench.Checked = true;
                }

                if (objMain.WhseCode == "LWH2")
                {
                    frmNewOAU.cbWhseCode.SelectedIndex = 0;
                }
                else
                {
                    frmNewOAU.cbWhseCode.SelectedIndex = 1;
                }

                frmNewOAU.buttonNewOAUAccept.Text = "Update";
                frmNewOAU.buttonNewOAUPrint.Enabled = true;
                frmNewOAU.labelNewOAUMsg.Text = "UPDATE MODE - Changes made to the data on this screen will " +
                    "overwrite the data for - " + frmNewOAU.textBoxNewOAUJobNum.Text;
                GlobalModeType.ModeType = "Update";

                frmNewOAU.ShowDialog();
            }
            else
            {
                MessageBox.Show("No ETL data found for this Job.");
            }
            
        }

        private void populate_frmNewLineMON(string endConsumerStr, string customerNameStr, DataTable dtMON)
        {           
            frmNewLineMON frmNewMON = new frmNewLineMON();
            frmNewMON.btnAccept.Text = "Update";       
           
            DataRow dr = dtMON.Rows[0];

            GlobalModeType.ModeType = "Update";            

            frmNewMON.Text = "Update Monitor Job # " + dr["JobNum"].ToString();
            frmNewMON.txtOrderNum.Text = dr["OrderNum"].ToString();
            frmNewMON.txtLineItem.Text = dr["OrderLine"].ToString();
            frmNewMON.txtReleaseNum.Text = dr["ReleaseNum"].ToString();
            frmNewMON.txtJobNum.Text = dr["JobNum"].ToString();
            frmNewMON.txtCustomer.Text = customerNameStr;
            frmNewMON.txtEndConsumer.Text = endConsumerStr;          

            frmNewMON.txtModelNo.Text = dr["ModelNo"].ToString();
            frmNewMON.txtSerialNo.Text = dr["SerialNo"].ToString();
            frmNewMON.txtPartNum.Text = frmNewMON.txtModelNo.Text.Substring(0, 7);
            frmNewMON.txtVerifiedBy.Text = dr["ModelNoVerifiedBy"].ToString();
            frmNewMON.txtHeatType.Text = dr["HeatingType"].ToString();
            frmNewMON.txtMinCKTAmp.Text = dr["MinCKTAmp"].ToString();
            frmNewMON.txtMFSMCB.Text = dr["MOP"].ToString();
            frmNewMON.txtRawMOP.Text = dr["RawMOP"].ToString();
            frmNewMON.txtElectricalRating.Text = dr["ElecRating"].ToString();
            frmNewMON.txtOperatingVolts.Text = dr["OperVoltage"].ToString(); 
            frmNewMON.txtFuelType.Text = dr["FuelType"].ToString();
            frmNewMON.txtMaxOutAirTemp.Text = dr["MaxOutletTemp"].ToString();
            frmNewMON.txtModelNo.Text = dr["ModelNo"].ToString();
            frmNewMON.txtVoltage.Text = dr["Voltage"].ToString();
            frmNewMON.txtHeatingInputElectric.Text = dr["ElectricHeatFLA"].ToString();
            //frmNewMON.txtCircuit1Charge.Text = dr["Circuit1Charge"].ToString();
            //frmNewMON.txtCircuit2Charge.Text = dr["Circuit2Charge"].ToString();

            //frmNewMON.txtDD_SupplyFanStdQty.Text = dr["DD_SupplyFanStdQty"].ToString();
            //frmNewMON.txtDD_SupplyFanStdPhase.Text = dr["DD_SupplyFanStdPhase"].ToString();
            //frmNewMON.txtDD_SupplyFanStdFLA.Text = dr["DD_SupplyFanStdFLA"].ToString();
            //frmNewMON.txtDD_SupplyFanStdHP.Text = dr["DD_SupplyFanStdHP"].ToString();
            
            //frmNewMON.txtDD_SupplyFanOvrQty.Text = dr["DD_SupplyFanOvrQty"].ToString();
            //frmNewMON.txtDD_SupplyFanOvrPhase.Text = dr["DD_SupplyFanOvrPhase"].ToString();
            //frmNewMON.txtDD_SupplyFanOvrFLA.Text = dr["DD_SupplyFanOvrFLA"].ToString();
            //frmNewMON.txtDD_SupplyFanOvrHP.Text = dr["DD_SupplyFanOvrHP"].ToString();
           
            //frmNewMON.txtBelt_SupplyFanStdQty.Text = dr["BeltSupplyFanStdQty"].ToString();
            //frmNewMON.txtBelt_SupplyFanStdPhase.Text = dr["BeltSupplyFanStdPhase"].ToString();
            //frmNewMON.txtBelt_SupplyFanStdFLA.Text = dr["BeltSupplyFanStdFLA"].ToString();
            //frmNewMON.txtBelt_SupplyFanStdHP.Text = dr["BeltSupplyFanStdHP"].ToString();
           
            //frmNewMON.txtBeltSupplyFanOvrQty.Text = dr["BeltSupplyFanOvrQty"].ToString();
            //frmNewMON.txtBeltSupplyFanOvrPhase.Text = dr["BeltSupplyFanOvrPhase"].ToString();
            //frmNewMON.txtBeltSupplyFanOvrFLA.Text = dr["BeltSupplyFanOvrFLA"].ToString();
            //frmNewMON.txtBeltSupplyFanOvrHP.Text = dr["BeltSupplyFanOvrHP"].ToString();
            
            //frmNewMON.txtCondFanStdQty.Text = dr["CondFanStdQty"].ToString();
            //frmNewMON.txtCondFanStdPhase.Text = dr["CondFanStdPhase"].ToString();
            //frmNewMON.txtCondFanStdFLA.Text = dr["CondFanStdFLA"].ToString();
            //frmNewMON.txtCondFanStdHP.Text = dr["CondFanStdHP"].ToString();
           
            //frmNewMON.txtCondFanHighQty.Text = dr["CondFanHighQty"].ToString();
            //frmNewMON.txtCondFanHighPhase.Text = dr["CondFanHighPhase"].ToString();
            //frmNewMON.txtCondFanHighFLA.Text = dr["CondFanHighFLA"].ToString();
            //frmNewMON.txtCondFanHighHP.Text = dr["CondFanHighHP"].ToString();
           
            frmNewMON.txtExhaustFanQty.Text = dr["ExhaustFanQty"].ToString();
            frmNewMON.txtExhaustFanPhase.Text = dr["ExhaustFanPhase"].ToString();
            frmNewMON.txtExhaustFanFLA.Text = dr["ExhaustFanFLA"].ToString();
            frmNewMON.txtExhaustFanHP.Text = dr["ExhaustFanHP"].ToString();
            
            frmNewMON.txtSemcoWheelQty.Text = dr["SemcoWheelQty"].ToString();
            frmNewMON.txtSemcoWheelPhase.Text = dr["SemcoWheelPhase"].ToString();
            frmNewMON.txtSemcoWheelFLA.Text = dr["SemcoWheelFLA"].ToString();
            frmNewMON.txtSemcoWheelHP.Text = dr["SemcoWheelHP"].ToString();
            
            frmNewMON.txtAirXchangeWheelQty.Text = dr["AirXchangeWheelQty"].ToString();
            frmNewMON.txtAirXchangeWheelPhase.Text = dr["AirXchangeWheelPhase"].ToString();
            frmNewMON.txtAirXchangeWheelFLA.Text = dr["AirXchangeWheelFLA"].ToString();
            frmNewMON.txtAirXchangeWheelHP.Text = dr["AirXchangeWheelHP"].ToString();           
                                     
            try
            {
                frmNewMON.lbNewMONMsg.Text = "Existing Monitor order, if any updates are made then press the" +
                     "Update button to save the information.";

                frmNewMON.ShowDialog();

            }
            catch
            {
                MessageBox.Show("ERROR displaying MON data.");
            }
        }

        private void populate_frmNewLineMSP(string endConsumerStr, string customerNameStr, DataTable dtMSP)
        {            
            frmNewLineMSP frmNewMSP = new frmNewLineMSP();
            frmNewMSP.btnNewMSPAccept.Text = "Update";

            DataRow dr = dtMSP.Rows[0];

            string DHV_Str = "D,H'V";
            string modelNoStr = dr["ModelNo"].ToString();

            GlobalModeType.ModeType = "Update";

            if (modelNoStr.StartsWith("DV") == true || modelNoStr.StartsWith("DU") == true )
            {
                if (DHV_Str.Contains(modelNoStr.Substring(1, 1)) == true)  // Digit 2 equals 'D', 'H', or' 'V'
                {
                    // Digit 11 equals A or B then Unit Type = DV CW
                    frmNewMSP.gbNewMSPPostCooling.Enabled = true;
                    frmNewMSP.gbNewMSPPostHeating.Enabled = true;
                    frmNewMSP.gbNewMSPDehumidifier.Text = "Dehumidifier Rated Conditions";
                    frmNewMSP.lbNewFAN_VoltDraw.Text = "Draw:";
                    frmNewMSP.tbNewMSPFanVoltageDraw.Text = dr["FanDraw"].ToString();
                    frmNewMSP.tbNewMSPDehumidGPM_SST.Text = dr["Dehumid_SummerCond_GPM"].ToString();
                    frmNewMSP.tbNewMSPPostCoolingGPM_SST.Text = dr["PostCooling_GPM"].ToString();
                    frmNewMSP.tbNewMSP_UnitType.Text = "DVCW";

                    if (modelNoStr.Substring(10, 1) == "C" || modelNoStr.Substring(10, 1) == "D")   // Digit 11 equals C or D then Unit Type = DV DX
                    {
                        frmNewMSP.tbNewMSPDehumidAmbient.Enabled = false;
                        //frmNewMSP.tbNewMSPDehumidBTUH.Enabled = false;
                        frmNewMSP.tbNewMSPDehumidEWT.Enabled = false;
                        frmNewMSP.tbNewMSPDehumidGlycol.Enabled = false;
                        frmNewMSP.tbNewMSPDehumidLWT.Enabled = false;
                        frmNewMSP.tbNewMSPDehumidPD.Enabled = false;
                        frmNewMSP.lbDehumidGPM_SST.Text = "SST";
                        frmNewMSP.tbNewMSPDehumidGPM_SST.Text = dr["Dehumid_SummerCond_SST"].ToString();
                        frmNewMSP.lbPostCoolingGPM_SST.Text = "SST";
                        frmNewMSP.tbNewMSPPostCoolingGPM_SST.Text = dr["PostCooling_SST"].ToString();
                        frmNewMSP.tbNewMSP_UnitType.Text = "DVDX";
                    }
                }
                else // Digit2 equals 'U'
                {

                    frmNewMSP.tbNewMSPDehumidGlycol.Enabled = false;
                    frmNewMSP.tbNewMSPDehumidLWT.Enabled = false;
                    frmNewMSP.tbNewMSPDehumidPD.Enabled = false;
                    frmNewMSP.tbNewMSPDehumidEWT.Enabled = false;
                    frmNewMSP.lbNewFAN_VoltDraw.Text = "Voltage:";
                    frmNewMSP.tbNewMSPFanVoltageDraw.Text = dr["FanVoltage"].ToString();
                    frmNewMSP.lbDehumidGPM_SST.Text = "SST";
                    frmNewMSP.tbNewMSPDehumidGPM_SST.Text = dr["Dehumid_SummerCond_SST"].ToString();
                    frmNewMSP.gbNewMSPPostCooling.Enabled = false;
                    frmNewMSP.gbNewMSPPostHeating.Enabled = false;

                    if (modelNoStr.Substring(10, 1) == "C" || modelNoStr.Substring(10, 1) == "D")  // Digit 11 equals C or D then Unit Type = DU DX
                    {
                        frmNewMSP.tbNewMSPDehumidCapacity.Enabled = false;
                        
                        frmNewMSP.gbNewMSPDehumidifier.Text = "Summer Rated Conditions";                        
                        frmNewMSP.tbNewMSP_UnitType.Text = "DUDX";
                    }
                    else                                                                           // Digit 11 equals A or B then Unit Type = DU CW
                    {
                        frmNewMSP.gbNewMSPDehumidifier.Text = "Dehumidifier Rated Conditions";                        
                        frmNewMSP.tbNewMSP_UnitType.Text = "DUCW";
                    }
                }
            }
            else
            {
                //frmNewMSP.gbNewMSPPostCooling.Enabled = false;  //03/04/2020 - Temporarily commented out to allow Engineering to input values into the MSP Label.
                //frmNewMSP.gbNewMSPPostHeating.Enabled = false;  //03/04/2020 - Temporarily commented out to allow Engineering to input values into the MSP Label.
            }

            frmNewMSP.Text = "Update MSP Job # " + dr["JobNum"].ToString();
            frmNewMSP.tbNewMSPOrderNum.Text = dr["OrderNum"].ToString();
            frmNewMSP.tbNewMSPLineItem.Text = dr["OrderLine"].ToString();
            frmNewMSP.tbNewMSPReleaseNum.Text = dr["ReleaseNum"].ToString();
            frmNewMSP.tbNewMSPJobNum.Text = dr["JobNum"].ToString();
            frmNewMSP.tbNewMSPCustomer.Text = customerNameStr;
            frmNewMSP.tbNewMSPEndConsumer.Text = endConsumerStr;

            frmNewMSP.tbNewMSPModelNo.Text = modelNoStr;
            frmNewMSP.tbNewMSPSerialNo.Text = dr["SerialNum"].ToString();
            frmNewMSP.tbNewMSPPartNum.Text = dr["PartNum"].ToString();
            frmNewMSP.tbNewMSP_MfgDate.Text = dr["MfgDate"].ToString();
            frmNewMSP.tbNewMSPVerifiedBy.Text = dr["VerifiedBy"].ToString();  
          
            frmNewMSP.tbNewMSPDesignation.Text = dr["Designation"].ToString();
            frmNewMSP.tbNewMSPSerialNo.Text = dr["SerialNum"].ToString();
            frmNewMSP.tbNewMSPFanFilter.Text = dr["Filter"].ToString();          

            frmNewMSP.tbNewMSP_Power.Text = dr["FanPower"].ToString();

            frmNewMSP.tbNewMSPFanAirVolume.Text = dr["FanAirVolume"].ToString();
            frmNewMSP.tbNewMSPFanExternalSP.Text = dr["FanExternalSP"].ToString();
            frmNewMSP.tbNewMSPFanTotalSP.Text = dr["FanTotalSP"].ToString();
            frmNewMSP.tbNewMSPRefrigerant.Text = dr["Refrigerant"].ToString();

            frmNewMSP.tbNewMSPDehumidBTUH.Text = dr["Dehumid_SummerCond_BTUH"].ToString();
            frmNewMSP.tbNewMSPDehumidEWT.Text = dr["Dehumid_SummerCond_EWT"].ToString();
            frmNewMSP.tbNewMSPDehumidLWT.Text = dr["Dehumid_SummerCond_LWT"].ToString();

            //if (dr["Dehumid_SummerCond_GPM"] != null )
            //{
            //    frmNewMSP.lbDehumidGPM_SST.Text = "GPM";
            //    frmNewMSP.tbNewMSPDehumidGPM_SST.Text = dr["Dehumid_SummerCond_GPM"].ToString();
            //}
            //else
            //{
            //    frmNewMSP.lbDehumidGPM_SST.Text = "SST";
            //    frmNewMSP.tbNewMSPDehumidGPM_SST.Text = dr["Dehumid_SummerCond_SST"].ToString();
            //}
            
            frmNewMSP.tbNewMSPDehumidPD.Text = dr["Dehumid_SummerCond_PD"].ToString();
            frmNewMSP.tbNewMSPDehumidGlycol.Text = dr["Dehumid_SummerCond_Glycol"].ToString();
            frmNewMSP.tbNewMSPDehumidEAT.Text = dr["Dehumid_SummerCond_EAT"].ToString();
            frmNewMSP.tbNewMSPDehumidLAT.Text = dr["Dehumid_SummerCond_LAT"].ToString();
            frmNewMSP.tbNewMSPDehumidAmbient.Text = dr["AmbientTemp"].ToString();
            frmNewMSP.tbNewMSPDehumidCapacity.Text = dr["Dehumid_SummerCond_CAPACITY"].ToString();           

            frmNewMSP.tbNewMSPPostCoolingBTUH.Text = dr["PostCooling_BTUH"].ToString();
            frmNewMSP.tbNewMSPPostCoolingEWT.Text = dr["PostCooling_EWT"].ToString();
            frmNewMSP.tbNewMSPPostCoolingLWT.Text = dr["PostCooling_LWT"].ToString();

            //if (dr["PostCooling_GPM"] != null)
            //{
            //    frmNewMSP.lbPostCoolingGPM_SST.Text = "GPM";
            //    frmNewMSP.tbNewMSPPostCoolingGPM_SST.Text = dr["PostCooling_GPM"].ToString();
            //}
            //else
            //{
            //    frmNewMSP.lbPostCoolingGPM_SST.Text = "SST";
            //    frmNewMSP.tbNewMSPPostCoolingGPM_SST.Text = dr["PostCooling_SST"].ToString();
            //}
            //frmNewMSP.tbNewMSPPostCoolingGPM_SST.Text = dr["PostCooling_GPM"].ToString();
            frmNewMSP.tbNewMSPPostCoolingPD.Text = dr["PostCooling_PD"].ToString();
            frmNewMSP.tbNewMSPPostCoolingGlycol.Text = dr["PostCooling_Glycol"].ToString();
            frmNewMSP.tbNewMSPPostCoolingEAT.Text = dr["PostCooling_EAT"].ToString();
            frmNewMSP.tbNewMSPPostCoolingLAT.Text = dr["PostCooling_LAT"].ToString();

            frmNewMSP.tbNewMSPPostHeatingBTUH.Text = dr["PostHeating_BTUH"].ToString();
            frmNewMSP.tbNewMSPPostHeatingEWT.Text = dr["PostHeating_EWT"].ToString();
            frmNewMSP.tbNewMSPPostHeatingLWT.Text = dr["PostHeating_LWT"].ToString();
            frmNewMSP.tbNewMSPPostHeatingGPM.Text = dr["PostHeating_GPM"].ToString();
            frmNewMSP.tbNewMSPPostHeatingPD.Text = dr["PostHeating_PD"].ToString();
            frmNewMSP.tbNewMSPPostHeatingGlycol.Text = dr["PostHeating_Glycol"].ToString();
            frmNewMSP.tbNewMSPPostHeatingEAT.Text = dr["PostHeating_EAT"].ToString();
            frmNewMSP.tbNewMSPPostHeatingLAT.Text = dr["PostHeating_LAT"].ToString();           

            try
            {
                frmNewMSP.lbNewMSPMsg.Text = "Existing MSP order, if any updates are made then press the" +
                     "Update button to save the information.";
                frmNewMSP.btnNewMSPPrint.Enabled = true;
                frmNewMSP.ShowDialog();

            }
            catch
            {
                MessageBox.Show("ERROR displaying MON data.");
            }
        }

        private void textBoxOOStartAt_KeyDown(object sender, KeyEventArgs e)
        {
            string tmpStr = "";

            int rowIdx = 0;


            // KeyValue 35 -> End Key        KeyValue 36 -> Home Key          KeyValue 37 -> Left Arrow Key
            // KeyValue 38 -> Up Arrow Key   KeyValue 39 -> Right Arrow Key   KeyValue 40 -> Down Arrow Key
            // KeyValue 46 -> Delete Key     KeyValue 144 -> Num Lock Key
            if ((e.KeyValue > 34 && e.KeyValue < 41) || (e.KeyValue == 46) || (e.KeyValue == 144))
            {
                return;
            }
            // KeyValue 0 -> 48 --- KeyValue 9 -> 57 OR Numeric KeyPad KeyValue 0 -> 96 --- KeyValue 9 -> 105 
            else if ((e.KeyValue > 47 && e.KeyValue < 58) || (e.KeyValue > 95 && e.KeyValue < 106) ||
                     (e.KeyValue == 8) || e.KeyValue == 109 || e.KeyValue == 189)
            {
                //this.listViewOOOrderNums.Items.Clear();             

                try
                {
                    // Because the key value has not been added to the StartAt textbox
                    // at this point it must be added in order to search properly.
                    tmpStr = this.textBoxOOStartAt.Text + convertKeyValue(e.KeyValue);                                            

                    if ((e.KeyValue == 8) && (tmpStr.Length > 0))
                    {
                        tmpStr = tmpStr.Substring(1, (tmpStr.Length - 1));                      
                    }


                    foreach (DataGridViewRow dgr in dgvJobNumList.Rows)
                    {
                        if (dgr.Cells["JobNum"].Value != null)
                        {
                            if (dgr.Cells["JobNum"].Value.ToString().StartsWith(tmpStr) || dgr.Cells["JobNum"].Value.ToString() == tmpStr)
                            {
                                rowIdx = dgr.Index;
                                dgvJobNumList.ClearSelection();  // This code clears the current selected row and then select the first meeting the search criteria and scrolls forward to that row.
                                dgvJobNumList.Rows[rowIdx].Selected = true;
                                dgvJobNumList.FirstDisplayedScrollingRowIndex = rowIdx;                                                               
                                break;
                            }
                        }
                    }                    

                    //// Retrieve all Order number greater to or equal to Start At value.
                    //VS_DataSetTableAdapters.EtlJobHeadTableAdapter etlJobHeadTA = 
                    //                        new VS_DataSetTableAdapters.EtlJobHeadTableAdapter();

                    //etlJobHeadTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                    //VS_DataSet.EtlJobHeadDataTable etlJobHeadDT = etlJobHeadTA.GetDataByGTJobNum(tmpStr);
                    
                    //ListViewItem lviOpen;
           
                    //foreach (DataRow dr in etlJobHeadDT)
                    //{
                    //    string tempStr;

                    //    lviOpen = new ListViewItem(objMain.JobNum);
                    //    tempStr = objMain.OrderNum;
                    //    lviOpen.SubItems.Add(tempStr.Trim());
                    //    tempStr = objMain.OrderLine;
                    //    lviOpen.SubItems.Add(tempStr.Trim());
                    //    tempStr = objMain.ReleaseNum;
                    //    lviOpen.SubItems.Add(tempStr.Trim());                
                    //    lviOpen.SubItems.Add(objMain.UnitType);
                    //    lviOpen.SubItems.Add(objMain.Customer);
                    //    this.listViewOOOrderNums.Items.Add(lviOpen);
                    //}                                       
                }
                catch
                {
                     MessageBox.Show("ERROR - Reading EtlJobHead table.");
                }
            }
            else //Any other KeyValue will trigger an Error Message.
            {
                tmpStr = this.textBoxOOStartAt.Text;
                MessageBox.Show("ERROR - Numeric values only.");
                this.textBoxOOStartAt.Text = tmpStr;
                this.textBoxOOStartAt.SelectionStart = tmpStr.Length;                
            }
        }

        private string convertKeyValue(int keyValue)
        {
            // This function converts the integer KeyValue passed into it to its string equivalent.

            if ((keyValue == 48) || (keyValue == 96))
            {
                return "0";
            }
            else if ((keyValue == 49) || (keyValue == 97))
            {
                return "1";
            }
            else if ((keyValue == 50) || (keyValue == 98))
            {
                return "2";
            }
            else if ((keyValue == 51) || (keyValue == 99))
            {
                return "3";
            }
            else if ((keyValue == 52) || (keyValue == 100))
            {
                return "4";
            }
            else if ((keyValue == 53) || (keyValue == 101))
            {
                return "5";
            }
            else if ((keyValue == 54) || (keyValue == 102))
            {
                return "6";
            }
            else if ((keyValue == 55) || (keyValue == 103))
            {
                return "7";
            }
            else if ((keyValue == 56) || (keyValue == 104))
            {
                return "8";
            }
            else if ((keyValue == 57) || (keyValue == 105))
            {
                return "9";
            }

            return "";
        }

        private void btnNewLine_Click(object sender, EventArgs e)
        {
            frmViperNew frmVSNew = new frmViperNew();
            frmVSNew.ShowDialog();
        }

        private void lCSystemsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmLCSystemMain frmLC = new frmLCSystemMain();
            frmLC.ShowDialog();
        }

        private void tradewiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTradewinds frmTrade = new frmTradewinds();
            frmTrade.txtModelNo.Text = "TW1150G4SFVTE3";
            frmTrade.txtDateOfAsm.Text = "DECMBER,2017";
            frmTrade.txtVoltage.Text = "460";
            frmTrade.txtHertz.Text = "60";
            frmTrade.txtPhase.Text = "3";
            frmTrade.txtControlPower.Text = "24";
            frmTrade.txtFullLoadAmp.Text = "34.9";
            frmTrade.txtMinCircuitAmp.Text = "38.0";
            frmTrade.txtMaxOverProtection.Text = "50";
            frmTrade.txtTypeOfUse.Text = "FOR OUTDOOR USE";
            frmTrade.txtkArmsSym.Text = "5";
            frmTrade.txtVoltMax.Text = "460";

            frmTrade.txtSemcoModelNo.Text = "FVT-3000H+E";
            frmTrade.txtSemcoDateOfMfg.Text = "OCTOBER,2017";
            frmTrade.txtSemcoSupFanMotorHP.Text = "0.75";
            frmTrade.txtSemcoSupFanMotorAmps.Text = "1.6";
            frmTrade.txtSemcoExhFanMotorHP.Text = "3.0";
            frmTrade.txtSemcoExhFanMotorAmps.Text = "4.8";
            frmTrade.txtSemcoTEWheelMotorHP.Text = "1/6";
            frmTrade.txtSemcoTEWheelMotorAmps.Text = "0.31";

            frmTrade.txtTraneModelNo.Text = "YSD150G4RZA0000C1A1B6000H000000000000000";
            frmTrade.txtTraneDateOfMfg.Text = "SEPTEMBER,2017";
            frmTrade.txtTraneComp1Qty.Text = "1";
            frmTrade.txtTraneComp1Phase.Text = "3";
            frmTrade.txtTraneComp1Hertz.Text = "60";
            frmTrade.txtTraneComp1RLAVolts.Text = "12.2-460";
            frmTrade.txtTraneComp1LRA.Text = "100";

            frmTrade.txtTraneComp2Qty.Text = "1";
            frmTrade.txtTraneComp2Phase.Text = "3";
            frmTrade.txtTraneComp2Hertz.Text = "60";
            frmTrade.txtTraneComp2RLAVolts.Text = "6.2-460";
            frmTrade.txtTraneComp2LRA.Text = "41";

            frmTrade.txtTraneCond1FanMtrQty.Text = "1";
            frmTrade.txtTraneCond1FanMtrPhase.Text = "3";
            frmTrade.txtTraneCond1FanMtrHertz.Text = "60";
            frmTrade.txtTraneCond1FanMtrFLAVolts.Text = "6.2-460";
            frmTrade.txtTraneCond1MtrHP.Text = "0.5";

            frmTrade.txtTraneEvapFanMtrQty.Text = "1";
            frmTrade.txtTraneEvapFanMtrPhase.Text = "3";
            frmTrade.txtTraneEvapFanMtrHertz.Text = "60";
            frmTrade.txtTraneEvapFanMtrFLAVolts.Text = "4.8-460";
            frmTrade.txtTraneEvapFanMtrHP.Text = "3.00";

            frmTrade.txtTraneRefrigType.Text = "R410A FACTORY CHARGED";
            frmTrade.txtTraneCirc1Charge.Text = "8.10";
            frmTrade.txtTraneCirc2Charge.Text = "5.10";

            frmTrade.txtTraneTestPresHigh.Text = "449";
            frmTrade.txtTraneTestPresLow.Text = "238";

            frmTrade.txtTraneGasHeatMaxHtgInput.Text = "250000";
            frmTrade.txtTraneGasHeatMinHtgInput.Text = "175000";

            frmTrade.txtTraneMaxDischargeTemp.Text = "175";
            frmTrade.txtTraneMaxEntStaticPres.Text = "0.7";

            frmTrade.txtTraneDistTop.Text = "NO OBS";
            frmTrade.txtTraneDistLSide.Text = "24 IN.";
            frmTrade.txtTraneDistRSide.Text = "24 IN.";
            frmTrade.txtTraneDistFront.Text = "24 IN.";
            frmTrade.txtTraneDistBack.Text = "18 IN.";
            frmTrade.txtTraneDistDuck.Text = "N/A";

            frmTrade.txtSerialNo.Focus();
            frmTrade.txtSerialNo.Select();

            frmTrade.ShowDialog();
        }

        private void dgvJobNumList_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string jobNumStr;
            string orderNumStr;
            string orderLineStr;
            string releaseNumStr;
            string unitTypeStr;
            string customerStr = "";
            string endConsumerStr = "";

            int orderNumInt;
            int orderLineInt;
            int releaseNumInt;
            int itemSelectedInt = 0;

            databaseFunctions dbfuncs = new databaseFunctions();

            // Go through all the Items to determine which one the user selected.
            //for (int i = 0; i <= this.listViewOOOrderNums.Items.Count - 1; i++)
            //{
            //    if (this.listViewOOOrderNums.Items[i].Selected == true)
            //    {
            //        itemSelectedInt = i;
            //    }
            //}
            
            jobNumStr = dgvJobNumList.Rows[e.RowIndex].Cells["JobNum"].Value.ToString();
            orderNumStr = dgvJobNumList.Rows[e.RowIndex].Cells["OrderNum"].Value.ToString();
            orderLineStr = dgvJobNumList.Rows[e.RowIndex].Cells["OrderLine"].Value.ToString();
            releaseNumStr = dgvJobNumList.Rows[e.RowIndex].Cells["ReleaseNum"].Value.ToString();
            unitTypeStr = dgvJobNumList.Rows[e.RowIndex].Cells["UnitType"].Value.ToString();
            endConsumerStr = dgvJobNumList.Rows[e.RowIndex].Cells["EndConsumerName"].Value.ToString(); 

            orderNumInt = Int32.Parse(orderNumStr);
            orderLineInt = Int32.Parse(orderLineStr);
            releaseNumInt = Int32.Parse(releaseNumStr);

            customerStr = dbfuncs.getCustomerName(orderNumInt);

            if (unitTypeStr.StartsWith("OA") == true || unitTypeStr.StartsWith("VKG") == true)
            {
                try
                {
                    //VS_DataSetTableAdapters.EtlOAUTableAdapter etlOAUTA =
                    //    new VS_DataSetTableAdapters.EtlOAUTableAdapter();

                    //etlOAUTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                    // Read the orderDtlOAU table and retrieve the specified OrderNum/OrderLine/ReleaseNum
                    // info and then use the NewLineOAU form to display the item.
                    //VS_DataSet.EtlOAUDataTable etlOAUDT =
                    //    etlOAUTA.GetDataByJobNum(jobNumStr);

                    objMain.JobNum = jobNumStr;
                    bool jobFound = objMain.GetEtlOAUData();

                    if (jobFound == false)
                    {
                        MessageBox.Show("No ETL Label has been created for Job # - " + jobNumStr);
                    }
                    else
                    {
                        //populate_frmNewLineOAU(etlOAUDT, endConsumerStr, customerStr); No longer need to pass data table
                        populate_frmNewLineOAU(jobNumStr, endConsumerStr, customerStr, true);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR reading the EtlOAU table for Job # " + jobNumStr + "Exception: " + ex);
                }
            }
            else if (unitTypeStr.Equals("FAN", StringComparison.OrdinalIgnoreCase))
            {
                try
                {
                    //VS_DataSetTableAdapters.OrderDtlFANTableAdapter ordDtlFANTA =
                    //    new VS_DataSetTableAdapters.OrderDtlFANTableAdapter();

                    VS_DataSetTableAdapters.EtlFANTableAdapter etlFANTA =
                        new VS_DataSetTableAdapters.EtlFANTableAdapter();

                    //ordDtlFANTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                    etlFANTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                    // Read the orderDtlFAN table and retrieve the specified OrderNum/OrderLine/ReleaseNum
                    // info and then use the NewLineFAN form to display the item.
                    //VS_DataSet.OrderDtlFANDataTable ordDtlFANDT =
                    //    ordDtlFANTA.GetDataByOrderNumOrderLineReleaseNum(orderNumInt, orderLineInt, releaseNumInt);

                    VS_DataSet.EtlFANDataTable etlFANDT =
                        etlFANTA.GetDataByJobNum(jobNumStr);

                    if (etlFANDT.Rows.Count == 0)
                    {
                        MessageBox.Show("No ETL Label has been created for Job # - " + jobNumStr);
                    }
                    else
                    {
                        populate_frmNewLineFAN(etlFANDT, endConsumerStr, customerStr);
                    }
                }
                catch
                {
                    MessageBox.Show("ERROR reading the EtlFAN table for Job # " + jobNumStr);
                }
            }
            else if (unitTypeStr.Equals("RRU", StringComparison.OrdinalIgnoreCase))
            {
                try
                {
                    VS_DataSetTableAdapters.EtlRRUTableAdapter etlRRUTA =
                        new VS_DataSetTableAdapters.EtlRRUTableAdapter();

                    etlRRUTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                    // Read the orderDtlRRU table and retrieve the specified OrderNum/OrderLine/ReleaseNum
                    // info and then use the NewLineRRU form to display the item.
                    VS_DataSet.EtlRRUDataTable etlRRUDT =
                        //etlRRUTA.GetDataByOrderNumOrderLineReleaseNum(orderNumInt, orderLineInt, releaseNumInt);
                        etlRRUTA.GetDataByJobNum(jobNumStr);

                    if (etlRRUDT.Rows.Count == 0)
                    {
                        MessageBox.Show("No ETL Label has been created for Job # - " + jobNumStr);
                    }
                    else
                    {
                        populate_frmNewLineRRU(etlRRUDT, endConsumerStr, customerStr);
                    }
                }
                catch
                {
                    MessageBox.Show("ERROR reading the EtlRRU table for Job # - " + jobNumStr);
                }
            }
            else if (unitTypeStr.Equals("MUA", StringComparison.OrdinalIgnoreCase))
            {
                try
                {
                    //VS_DataSetTableAdapters.OrderDtlMUATableAdapter ordDtlMUATA =
                    //    new VS_DataSetTableAdapters.OrderDtlMUATableAdapter();

                    VS_DataSetTableAdapters.EtlMUATableAdapter etlMUATA =
                        new VS_DataSetTableAdapters.EtlMUATableAdapter();

                    //ordDtlMUATA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                    etlMUATA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                    // Read the orderDtlMUA table and retrieve the specified OrderNum/OrderLine/ReleaseNum
                    // info and then use the NewLineMUA form to display the item.
                    //VS_DataSet.OrderDtlMUADataTable ordDtlMUADT =
                    //    ordDtlMUATA.GetDataByOrderNumOrderLineReleaseNum(orderNumInt, orderLineInt, releaseNumInt);

                    VS_DataSet.EtlMUADataTable etlMUADT =
                        etlMUATA.GetDataByJobNum(jobNumStr);

                    if (etlMUADT.Rows.Count == 0)
                    {
                        MessageBox.Show("No ETL Label has been created for Job # - " + jobNumStr);
                    }
                    else
                    {
                        populate_frmNewLineMUA(etlMUADT, endConsumerStr, customerStr);
                    }
                }
                catch
                {
                    MessageBox.Show("ERROR reading the EtlMUA table for JobNum # " + jobNumStr);
                }
            }
            else if (unitTypeStr.Equals("PCO", StringComparison.OrdinalIgnoreCase))
            {
                try
                {
                    //VS_DataSetTableAdapters.OrderDtlPCOTableAdapter ordDtlPCOTA =
                    //    new VS_DataSetTableAdapters.OrderDtlPCOTableAdapter();

                    VS_DataSetTableAdapters.EtlPCOTableAdapter etlPCOTA =
                        new VS_DataSetTableAdapters.EtlPCOTableAdapter();

                    //ordDtlPCOTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                    etlPCOTA.Connection.ConnectionString = GlobalKCCConnectionString.KCCConnectString;

                    // Read the orderDtlPCO table and retrieve the specified OrderNum/OrderLine/ReleaseNum
                    // info and then use the NewLinePCO form to display the item.
                    //VS_DataSet.OrderDtlPCODataTable ordDtlPCODT =
                    //    ordDtlPCOTA.GetDataByOrderNumOrderLineReleaseNum(orderNumInt, orderLineInt, releaseNumInt);

                    VS_DataSet.EtlPCODataTable etlPCODT =
                        etlPCOTA.GetDataByJobNum(jobNumStr);


                    if (etlPCODT.Rows.Count == 0)
                    {
                        MessageBox.Show("No ETL Label has been created for Job # - " + jobNumStr);
                    }
                    else
                    {
                        populate_frmNewLinePCO(etlPCODT, endConsumerStr, customerStr);
                    }
                }
                catch (Exception f)
                {
                    MessageBox.Show("JobNum " + f);
                    //MessageBox.Show("ERROR reading the orderDtlPCO table for OrderNum/OrderLine/ReleaseNum - "
                    //               + orderNumStr + "/" + orderLineStr + "/" + releaseNumStr);
                }
            }
            else if (unitTypeStr.Equals("MON", StringComparison.OrdinalIgnoreCase))
            {
                try
                {
                    objMain.JobNum = jobNumStr;
                    DataTable dtMON = objMain.GetMonitorEtlData();

                    if (dtMON.Rows.Count == 0)
                    {
                        MessageBox.Show("No ETL Label has been created for Job # - " + jobNumStr);
                    }
                    else
                    {
                        populate_frmNewLineMON(endConsumerStr, customerStr, dtMON);
                    }
                }
                catch (Exception f)
                {
                    MessageBox.Show("JobNum " + f);
                    //MessageBox.Show("ERROR reading the orderDtlPCO table for OrderNum/OrderLine/ReleaseNum - "
                    //               + orderNumStr + "/" + orderLineStr + "/" + releaseNumStr);
                }
            }
            else if (unitTypeStr.Equals("MSP", StringComparison.OrdinalIgnoreCase))
            {
                try
                {
                    objMain.JobNum = jobNumStr;
                    DataTable dtMON = objMain.MSP_GetEtlData();

                    if (dtMON.Rows.Count == 0)
                    {
                        MessageBox.Show("No ETL Label has been created for Job # - " + jobNumStr);
                    }
                    else
                    {
                        populate_frmNewLineMSP(endConsumerStr, customerStr, dtMON);
                    }
                }
                catch (Exception f)
                {
                    MessageBox.Show("JobNum " + f);
                    //MessageBox.Show("ERROR reading the orderDtlPCO table for OrderNum/OrderLine/ReleaseNum - "
                    //               + orderNumStr + "/" + orderLineStr + "/" + releaseNumStr);
                }
            }
        }                    

    }
}